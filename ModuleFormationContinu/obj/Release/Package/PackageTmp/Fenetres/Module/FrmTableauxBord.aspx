﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmTableauxBord.aspx.vb" Inherits="Virtualia.Net.FrmTableauxBord" 
    MasterPageFile="~/PagesMaitre/VirtualiaMain.master" Culture="auto:fr-FR" UICulture="auto:fr-FR" MaintainScrollPositionOnPostback="True" 
    EnableEventValidation="false" Async="true" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlGestionTableauBord.ascx" TagName="VTableau" TagPrefix="Formation" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreVues" runat="server" Width="1150px" HorizontalAlign="Center" CellSpacing="2" Style="margin-top: 1px"
                              BackColor="#5E9598" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                              BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
        <asp:TableRow>
           <asp:TableCell HorizontalAlign="Center">
              <Formation:VTableau ID="CtlTableaux" runat="server" />
           </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
