﻿<%@ Page Language="vb" 
    AutoEventWireup="false" 
    CodeBehind="FrmPlanDeFormation.aspx.vb" 
    Inherits="Virtualia.Net.FrmPlanDeFormation" 
    MasterPageFile="~/PagesMaitre/VirtualiaMain.master" 
    Culture="auto:fr-FR" 
    UICulture="auto:fr-FR" 
    MaintainScrollPositionOnPostback="True" 
    EnableEventValidation="false"
    Async="true" 
    %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master" %>

<%@ Register Src="~/Controles/REF/Formation/CtlPlanningFormation.ascx" TagName="VPlanningStd" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControleGeneric/ArbreSelection.ascx" TagName="CTL_ARBRE" TagPrefix="Generic" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/CtrlDetailStage.ascx" TagName="CTL_DETAIL" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControleGeneric/BoutonNouveau.ascx" TagName="BTN_NOUVEAU" TagPrefix="Generic" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreVues" runat="server" Width="1150px" HorizontalAlign="Center" CellSpacing="2" Style="margin-top: 1px" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
        <asp:TableRow ID="RCmdNouv">
            <asp:TableCell HorizontalAlign="Right">
                <Generic:BTN_NOUVEAU ID="BtnNouveau" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="ConteneurVues" VerticalAlign="Top" HorizontalAlign="Center">
                <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueSelection" runat="server">
                        <Generic:CTL_ARBRE ID="CtrlArbre" runat="server" SiFiltrePlanVisible="True" Width="1150px" Height="540px" />
                    </asp:View>
                    <asp:View ID="VueCalendrier" runat="server">
                        <Virtualia:VPlanningStd ID="CtrlPlanning" runat="server" IDAppelant="FOR" />
                    </asp:View>
                    <asp:View ID="VueDetail" runat="server">
                        <Virtualia:CTL_DETAIL ID="CtrlDetail" runat="server" />
                    </asp:View>
                    <asp:View ID="VueDemandes" runat="server">
                        <Virtualia:VPlanningStd ID="CtrlDemandes" runat="server" IDAppelant="PER" />
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
