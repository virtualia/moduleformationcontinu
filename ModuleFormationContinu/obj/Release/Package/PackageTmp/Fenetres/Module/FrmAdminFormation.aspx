﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmAdminFormation.aspx.vb" Inherits="Virtualia.Net.FrmAdminFormation" 
    MasterPageFile="~/PagesMaitre/VirtualiaMain.Master" MaintainScrollPositionOnPostback="True" Culture="auto:fr-FR" UICulture="auto:fr-FR" 
    Async="true" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.Master" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/VReferentielFormation.ascx" tagname="VRefW" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Organisme/Organisme.ascx" tagname="VEnt" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/TablesGenerales/TAB_LISTE.ascx" tagname="VTabGen" tagprefix="Virtualia" %>

<asp:Content ID="CadreAdmin" runat="server" ContentPlaceHolderID="Principal">
   <asp:Table ID="CadreVues" runat="server" Height="700px" Width="1150px" HorizontalAlign="Center" BackColor="#5E9598" CellSpacing="2" Style="margin-top: 0px">
        <asp:TableRow>
            <asp:TableCell ID="CellRetour" HorizontalAlign="Left" VerticalAlign="Top" Visible="false">
                <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" 
                        BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" 
                        ImageAlign="Middle" style="margin-left: 1px;">
                </asp:ImageButton>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="ConteneurVues" VerticalAlign="Top" HorizontalAlign="Center">
                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueReferentiel" runat="server">
                        <Virtualia:VRefW ID="MajReferentiel" runat="server" SiListeMenuVisible="true" />
                    </asp:View>
                    <asp:View ID="VueTabGen" runat="server">
                        <Virtualia:VTabGen ID="MajTabGen" runat="server" />
                    </asp:View>
                    <asp:View ID="VueOrganisme" runat="server">
                        <Virtualia:VEnt ID="MajOrganisme" runat="server" />
                    </asp:View>
                    <asp:View ID="VueMessage" runat="server">
                        <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
