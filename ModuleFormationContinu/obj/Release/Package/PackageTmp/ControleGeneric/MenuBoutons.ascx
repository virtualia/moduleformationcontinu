﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MenuBoutons.ascx.vb" Inherits="Virtualia.Net.MenuBoutons" %>

<asp:Table ID="CadreMenus" runat="server" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true">
    <asp:TableRow VerticalAlign="Middle">
        <asp:TableCell>
            <asp:Menu ID="MenuChoix" runat="server" DynamicHorizontalOffset="10" BackColor="Transparent" Font-Italic="false" ForeColor="White" Height="32px" Orientation="Horizontal" StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="2px" StaticEnableDefaultPopOutImage="False" BorderColor="#B0E0D7" BorderStyle="None">
                <StaticSelectedStyle BackColor="#B0E0D7" BorderStyle="Solid" Font-Bold="True" ForeColor="#000066" />
                <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="24px" BackColor="#0E5F5C" BorderColor="#B0E0D7" BorderStyle="Solid" Font-Bold="False" ForeColor="White" />
                <StaticHoverStyle BackColor="#7EC8BE" ForeColor="White" BorderColor="#B0E0D7" BorderStyle="Solid" />
            </asp:Menu>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
