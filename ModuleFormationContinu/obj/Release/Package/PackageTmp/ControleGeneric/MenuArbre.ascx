﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MenuArbre.ascx.vb" Inherits="Virtualia.Net.MenuArbre" %>


<asp:Table ID="CadreMenu" runat="server" BorderColor="#B0E0D7" BorderWidth="2px" BorderStyle="Ridge" BackColor="White" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Font-Bold="false" Style="margin-top: 5px; background-attachment: inherit; display: table-cell;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="PanelTree" runat="server" Width="320px" Height="450px" ScrollBars="Auto" BackColor="Snow" Wrap="true" Style="text-align: left">
                <asp:TreeView ID="TreeListeMenu" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="Snow" ForeColor="#142425" ShowCheckBoxes="None"  NodeIndent="10" BackColor="Snow" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-ImageUrl="~/Images/Armoire/FicheBleue.bmp" RootNodeStyle-Font-Italic="False" Style="overflow: auto; width:100%; height:100%">
                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" ForeColor="White" />
                </asp:TreeView>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
