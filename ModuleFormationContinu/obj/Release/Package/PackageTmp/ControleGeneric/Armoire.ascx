﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Armoire.ascx.vb" Inherits="Virtualia.Net.Armoire" %>

<asp:Panel ID="pnl1" runat="server" Width="650px">
    <asp:Table ID="CadreArmoire" runat="server" HorizontalAlign="Center" Width="100%" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Font-Bold="false" BackColor="#173434">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell Height="5px"/>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="EtiInfo" runat="server" Text="# TITRE #" Height="20px" Width="90%" BackColor="Transparent" ForeColor="White" Font-Italic="true" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="CelluleSelection" Visible="false" VerticalAlign="Top">
                <asp:Table ID="Table1" runat="server" Width="90%" Height="25px" HorizontalAlign="Center" BorderStyle="None">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" Width="50%">
                            <asp:DropDownList ID="cboselection" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboselection_SelectedIndexChanged" Height="22px" Width="100%" BackColor="#A8BBB8" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left">
                                <asp:ListItem Text="" Value="NEANT" />
                                <asp:ListItem Text="Tout sélectionner" Value="TOUT" />
                                <asp:ListItem Text="Ne rien sélectionner" Value="RIEN" />
                                <asp:ListItem Text="Inverser la sélection" Value="INV" />
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell ID="CelluleLettre">
                <asp:Table ID="CadreLettres" runat="server" Width="95%" Height="50px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Table ID="CadreLettre" runat="server" Height="20px" HorizontalAlign="Center">
                                <asp:TableRow ID="RowLettres" runat="server" HorizontalAlign="Center">
                                    <asp:TableCell ID="CellZero">
                                        <asp:ImageButton Width="20px" ID="Button0" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/0.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonA" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/A.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonB" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonC" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonD" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonE" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonF" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonG" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonH" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonI" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonJ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonK" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonL" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonM" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonN" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonO" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonP" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonQ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonR" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonS" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonT" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonU" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonV" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonW" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonX" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonY" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonZ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton Width="20px" ID="ButtonArobase" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Arobase_Sel.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ID="CelluleFiltre" HorizontalAlign="Center" BackColor="#6D9092" Width="50%">
                            <asp:Table ID="CadreFiltre" runat="server" HorizontalAlign="Center" Height="22px" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Center" Width="50%">
                                        <asp:Label Width="90%" ID="lblRecherche" runat="server" Text="Recherche par nom (Commençant par)" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" BorderStyle="None" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left" Width="50%">
                                        <asp:TextBox Width="90%" ID="txtRecherche" runat="server" Text="" AutoPostBack="true" BackColor="White" BorderColor="#B0E0D7" BorderStyle="InSet" BorderWidth="2px" ForeColor="#124545" Height="16px" MaxLength="35" OnTextChanged="txtRecherche_TextChanged" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                <asp:Panel ID="PanelTree" runat="server" ScrollBars="Auto" BackColor="Snow" Wrap="true" Style="text-align: left" Width="100%" Height="440px" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7">
                    <asp:TreeView ID="LstArmoire" runat="server" MaxDataBindDepth="2" ForeColor="#142425" NodeIndent="10" LeafNodeStyle-HorizontalPadding="8px" Style="overflow: auto; width: 100%; height: 100%" OnSelectedNodeChanged="LstArmoire_SelectedChanged">
                        <SelectedNodeStyle BackColor="#6C9690" BorderColor="Snow" BorderStyle="Solid" ForeColor="#D7FAF3" Font-Bold="True" />
                    </asp:TreeView>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow VerticalAlign="Bottom">
            <asp:TableCell HorizontalAlign="Center" Height="22px">
                <asp:Table ID="CadreStatut" runat="server" Width="100px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="lblNbStatut" runat="server" Height="20px" Width="90px" BackColor="Transparent" ForeColor="White" BorderStyle="None" Text="0 / 0" Font-Italic="true" Style="text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="HButtonLettre" runat="server" Value="ButtonArobase" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Panel>

 