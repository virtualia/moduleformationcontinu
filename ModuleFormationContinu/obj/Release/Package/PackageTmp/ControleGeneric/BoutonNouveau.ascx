﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BoutonNouveau.ascx.vb" Inherits="Virtualia.Net.BoutonNouveau" %>

<asp:Table ID="TbNouveau" runat="server" Width="90px" BackImageUrl="~/Images/Boutons/NouvelleFiche_Std.bmp" BackColor="#FF7D9F99" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell ID="CellBtn1" HorizontalAlign="Center">
            <asp:Button ID="Btn1" runat="server" Text="Nouveau" Width="100%" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="cursor: pointer; margin-left:5px" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
