﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_Message.ascx.vb" Inherits="Virtualia.Net.V_Message" ClassName="V_Message" %>


<asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
        <%--BackImageUrl="~/Images/Fonds/Presentation.jpg"--%>
        <asp:Table ID="MessageVirtualia" runat="server" CellPadding="0" CellSpacing="4" BackColor="#8DA8A3" BorderColor="#1C5150" BorderStyle="NotSet" BorderWidth="2px" Width="730px"  HorizontalAlign="Center">

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Height="80px" ColumnSpan="3">
                    <asp:ImageButton ID="CmdLogo" runat="server" Width="64px" Height="64px" BorderStyle="Solid" BorderColor="#B0E0D7" BorderWidth="1px" ImageUrl="~/Images/General/VirImag_BleuVertGris.jpg" ImageAlign="Middle" />
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Height="80px" ColumnSpan="3">
                    <asp:Label ID="EtiTitre" runat="server" Height="40px" Width="600px" BorderColor="#1C5150" BorderStyle="Notset" BorderWidth="1px" BackColor="#B0E0D7" ForeColor="#124545" Font-Italic="true" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Text="" Style="text-align: center" />
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" Height="180px" ColumnSpan="3">
                    <asp:TextBox ID="EtiMsg" runat="server" Height="150px" Width="550px" BorderColor="#1C5150" BorderStyle="Notset" BorderWidth="1px" TextMode="MultiLine" BackColor="#B0E0D7" ForeColor="#124545" Font-Italic="true" ReadOnly="true" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Text="" Style="text-indent: 5px; text-align: left" />
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow VerticalAlign="Bottom">
                <asp:TableCell HorizontalAlign="Right">
                    <asp:Button ID="CmdOui" runat="server" Text="Oui" Height="26px" Width="110px" ForeColor="#D7FAF3" Font-Italic="true" BackColor="#0E5F5C" BorderWidth="2px" BorderStyle="Outset" BorderColor="#C9F0E8" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" TabIndex="1" ToolTip="Je confirme l'opération" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Button ID="CmdNon" runat="server" Text="Non" Height="26px" Width="110px" ForeColor="#D7FAF3" Font-Italic="true" BackColor="#0E5F5C" BorderWidth="2px" BorderStyle="Outset" BorderColor="#C9F0E8" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" TabIndex="2" ToolTip="Je vais corriger ma demande" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <asp:Button ID="CmdCancel" runat="server" Text="Annuler" Height="26px" Width="110px" ForeColor="#D7FAF3" Font-Italic="true" BackColor="#0E5F5C" BorderWidth="2px" BorderStyle="Outset" BorderColor="#C9F0E8" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" TabIndex="2" ToolTip="J'annule l'opération en cours" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Width="700px" Height="2px" ColumnSpan="3" />
            </asp:TableRow>
        </asp:Table>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="CmdOui" />
        <asp:PostBackTrigger ControlID="CmdNon" />
        <asp:PostBackTrigger ControlID="CmdCancel" />
    </Triggers>
</asp:UpdatePanel>















