﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlBilanPerso.ascx.vb" Inherits="Virtualia.Net.CtrlBilanPerso" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/ControlesFormations/Personnel/Bilan/PopupResultatPerso.ascx" TagName="CTL_POPUPRESULTAT" TagPrefix="Formation" %>

<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1150px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
    <asp:TableRow>
        <asp:TableCell  HorizontalAlign="Center">
            <asp:Label ID="lblTitre" runat="server" Text="Bilan individuel" Width="500px" 
                BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" 
                ForeColor="#D7FAF3" Font-Bold="True" Font-Italic="true" Style="text-align: center;" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="right">
            <asp:Table ID="CadreCommandeExcel" runat="server" Height="20px" Width="150px" BackImageUrl="~/Images/General/Cmd_Std.bmp" Visible="true" 
                BorderWidth="2px" BorderStyle="Outset" BackColor="#FF0E5E5C" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell ID="CellBoutonExcel" HorizontalAlign="Center">
                        <asp:Button ID="CmdExcel" runat="server" Text="Fichier Excel" Width="125px" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" 
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" 
                            BorderStyle="None" Style="cursor:pointer" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2">
            <asp:Table ID="TbGenerale" runat="server" HorizontalAlign="Center" Visible="true" Width="830px" 
                BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                <asp:TableRow>
                    <asp:TableCell Height="5px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreAnnee" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Label ID="EtiAnnee" runat="server" Text="Année" Height="20px" Width="60px" 
                                        BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" 
                                        ForeColor="#B0E0D7" Style="font-style: oblique; text-indent:5px; text-align:center;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="ListeAnnees" runat="server" Height="20px" Width="110px" 
                                        BackColor="#EEECFD" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" 
                                        OnSelectedIndexChanged="LstAnnee_SelectedIndexChanged" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreBilan" runat="server" CellPadding="0" CellSpacing="0" ForeColor="Black">
                            <asp:TableRow ForeColor="Snow" Font-Bold="True">
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label1" runat="server" Text="TOTAUX" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label2" runat="server" Text="Nb actions de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Font-Italic="true" Font-Bold="false">
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label8" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Button ID="Tot_Nb_Action_Formation" runat="server" BackColor="Transparent" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor: pointer" OnClick="Tot_Nb_Action_Formation_Click"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow ForeColor="Snow" Font-Bold="True">
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label11" runat="server" Text="DUREES" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label12" runat="server" Text="Nb heures de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label13" runat="server" Text="Nb jours de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Font-Italic="true" Font-Bold="false">
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label16" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Button ID="Tot_Nb_Heure" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor: pointer"  OnClick="Tot_Nb_Heure_Click"/>
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Button ID="Tot_Nb_Jour" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor: pointer"  OnClick="Tot_Nb_Jour_Click"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow ForeColor="Snow" Font-Bold="True">
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label29" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label22" runat="server" Text="Durée moyenne action de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Font-Italic="true" Font-Bold="false">
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label5" runat="server" Text="en heures" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Duree_Moyenne_Action_Formation_h" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Font-Italic="true" Font-Bold="false">
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label7" runat="server" Text="en jours" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Duree_Moyenne_Action_Formation_j" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow ForeColor="Snow" Font-Bold="True">
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label14" runat="server" Text="COUTS" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label15" runat="server" Text="Total coûts" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label21" runat="server" Text="Coûts pédagogiques" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label26" runat="server" Text="Coûts frais de mission" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Font-Italic="true" Font-Bold="false">
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label30" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Button ID="Cout_Tot" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor: pointer"  OnClick="Cout_Tot_Click"/>
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Button ID="Cout_Pedagogique" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor: pointer" OnClick="Cout_Pedagogique_Click"/> 
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Button ID="Cout_Frais_Mission" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor: pointer" OnClick="Cout_Frais_Mission_Click" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow ForeColor="Snow" Font-Bold="True">
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label31" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="#19968D" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Label6" runat="server" Text="Coût moyen par action de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Font-Italic="true" Font-Bold="false">
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                                    <asp:Label ID="Label32" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell BackColor="Snow" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                                    <asp:Label ID="Cout_Moyen_Action_Formation" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-right: 5px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <ajaxToolkit:ModalPopupExtender ID="PopupPopupResultat" runat="server" TargetControlID="HidenPopupResultat" PopupControlID="PanelResultatPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelResultatPopup" runat="server">
                <Formation:CTL_POPUPRESULTAT ID="CtrlPopupResultat" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HidenPopupResultat" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

