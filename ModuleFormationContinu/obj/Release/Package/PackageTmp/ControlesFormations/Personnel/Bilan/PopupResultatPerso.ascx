﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PopupResultatPerso.ascx.vb" Inherits="Virtualia.Net.PopupResultatPerso" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
        <asp:Table ID="tb" runat="server" Width="1100px" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>

            <asp:TableRow Height="40px">
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Table ID="Table1" runat="server" Width="200px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:Label ID="Label2" runat="server" Height="18px" Width="500px" Text="Détail" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:Label ID="lblContexte" runat="server" Height="18px" Width="900px" Text="Détail" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>
            <asp:TableRow Height="40px">
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Table ID="Table5" runat="server" Width="780px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:Label ID="lblTitre" runat="server" Height="18px" Width="750px" Text="XXXXXX" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>

            <asp:TableRow ID="R_Page">
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Table ID="Table2" runat="server" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:ImageButton ID="BtnPgPrec" runat="server" Width="32px" Height="16px" BorderStyle="None" ImageUrl="~/Images/Boutons/PagePrecedente.bmp" ImageAlign="Middle" ToolTip="Page précedente" OnClick="BtnPgPrec_Click" />
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" Width="105px">
                                <asp:DropDownList ID="CboPage" runat="server" Height="25px" Width="100px" BackColor="Snow" AutoPostBack="true" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="CboPage_SelectedIndexChanged" />
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="lblIndicPage" runat="server" Text="XXXXXX" ForeColor="White" BorderStyle="None" Style="text-align: right; margin-left: 2px" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:ImageButton ID="BtnPgSuiv" runat="server" Width="32px" Height="16px" BorderStyle="None" ImageUrl="~/Images/Boutons/PageSuivante.bmp" ImageAlign="Middle" ToolTip="Page suivante" OnClick="BtnPgSuiv_Click" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell>
                    <asp:Table ID="TBExport" runat="server" Style="height: auto; width: 100%">
                        <asp:TableRow ID="RowTitre" Height="40px" Visible="false">
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="Table3" runat="server" Width="780px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                                    <asp:TableRow Height="25px">
                                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                            <asp:Label ID="LblTitreExport" runat="server" Height="18px" Width="750px" Text="XXXXXX" ForeColor="White" BorderStyle="None" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Panel ID="Panel1" runat="server" Width="98%" Style="height: 25px;" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="Transparent" Font-Size="85%">
                                    <asp:Table ID="TBEntete" runat="server" Style="height: auto; width: 100%">
                                        <asp:TableRow>
                                            <asp:TableCell Width="43%" BackColor="#124545">
                                                <asp:Label ID="Label1" runat="server" Width="100%" Text="Nom" ForeColor="Snow" BackColor="Transparent" Style="text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="43%">
                                                <asp:Label ID="Label3" runat="server" Width="100%" Text="Prénom" ForeColor="Snow" BackColor="#124545" Style="text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="14%">
                                                <asp:Label ID="Label4" runat="server" Width="100%" Text="Naiss." ForeColor="Snow" BackColor="#124545" Style="text-align: center;" />
                                            </asp:TableCell>
                                            <%--<asp:TableCell Width="2%" />--%>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Panel ID="PnlDetail" runat="server" Width="98%" Style="height: auto; min-height: 200px; max-height: 400px" ScrollBars="Vertical" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="Transparent" Font-Size="85%">
                                    <asp:Table ID="TB_Donnees" runat="server" Height="250px" Style="height: auto; width: 100%">
                                        <asp:TableRow>
                                            <asp:TableCell Width="43%">
                                                <asp:Label ID="Label6" runat="server" Width="100%" Text="Dilly" ForeColor="#101010" BackColor="Snow" Style="margin-left: 2px; text-align: left;" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="43%">
                                                <asp:Label ID="Label7" runat="server" Width="100%" Text="Laurent" ForeColor="#101010" BackColor="Snow" Style="margin-left: 2px; text-align: left;" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="14%">
                                                <asp:Label ID="Label8" runat="server" Width="100%" Text="08/09/1971" ForeColor="#101010" BackColor="Snow" Style="text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Panel ID="Panel2" runat="server" Width="98%" Style="height: auto;" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="Transparent" Font-Size="85%">
                                    <asp:Table ID="Table6" runat="server" Style="height: auto; width: 100%">
                                        <%--<asp:TableRow ID="R_TotPage">
                                            <asp:TableCell Width="86%" HorizontalAlign="Right">
                                                <asp:Label ID="lblLibTotPage" runat="server" Width="100%" Text="Total dans la page :" ForeColor="Snow" Style="text-align: right; margin-right: 2px" BackColor="#124545" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="12%">
                                                <asp:Label ID="lblTotPage" runat="server" Width="100%" Text="200" ForeColor="Snow" Style="text-align: right;" BackColor="#124545" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="2%" />
                                        </asp:TableRow>--%>

                                        <asp:TableRow>
                                            <asp:TableCell Width="86%" HorizontalAlign="Right">
                                                <asp:Label ID="Label10" runat="server" Width="100%" Text="Total :" ForeColor="Snow" Style="text-align: right; margin-right: 2px" BackColor="#124545" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="12%">
                                                <asp:Label ID="LblToTGene" runat="server" Width="100%" Text="200" ForeColor="Snow" Style="text-align: right;" BackColor="#124545" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="2%" />
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    

                    <asp:Table ID="Table7" runat="server" Width="780px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                                <Generic:BTN_GENE ID="BtnQuitter" runat="server" Text="Quitter" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>
        </asp:Table>
    </ContentTemplate>
    <Triggers>
<%--        <asp:PostBackTrigger ControlID="BtnCsv" />
        <asp:PostBackTrigger ControlID="BtnTxt" />--%>
        <%--<asp:PostBackTrigger ControlID="BtnPdf" />--%>

        <asp:AsyncPostBackTrigger ControlID="BtnQuitter" />
        <asp:AsyncPostBackTrigger ControlID="BtnPgPrec" />
        <asp:AsyncPostBackTrigger ControlID="CboPage" />
        <asp:AsyncPostBackTrigger ControlID="BtnPgSuiv" />
    </Triggers>
</asp:UpdatePanel>