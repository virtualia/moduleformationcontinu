﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlEditionInscription.ascx.vb" Inherits="Virtualia.Net.CtrlEditionInscription" %>

<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/CtrlMailConvocation.ascx" TagName="NewVConvoc" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/CtrlConvocations.ascx" TagName="VConvoc" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/CtrlAttestations.ascx" TagName="VAttest" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/CtrlEmargements.ascx" TagName="VEmarge" TagPrefix="Virtualia" %>

<asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Width="1050px">
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="xxxxx" Height="22px" Width="400px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Right" Height="22px" Width="300px">
            <asp:Table ID="CadreCommandes" runat="server" Height="20px" Width="150px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                            <asp:Table ID="CadreCommandePDF" runat="server" Height="20px" Width="150px" BackImageUrl="~/Images/General/Cmd_Std.bmp" 
                            BorderWidth="2px" BorderStyle="Outset" BackColor="#FF0E5E5C" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell ID="CellBoutonPDF" HorizontalAlign="Center">
                                    <asp:Button ID="CmdPDF" runat="server" Text="Fichier Pdf" Width="125px" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" 
                                        BorderStyle="None" Style="cursor:pointer" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false" ColumnSpan="2">
            <asp:UpdateProgress ID="UpdateAttente" runat="server">
                <ProgressTemplate>
                    <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                            </asp:TableCell>
                            <asp:TableCell Width="10px" Height="10px" />
                            <asp:TableCell>
                                <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreApercu" runat="server" Visible="true" Width="1050px" BackColor="White">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:MultiView ID="MultiVues" runat="server">
                            <asp:View ID="VueConvocations" runat="server">
                                <Virtualia:VConvoc ID="Convocation" runat="server"></Virtualia:VConvoc>
                            </asp:View>
                            <asp:View ID="VueAttestations" runat="server">
                                <Virtualia:VAttest ID="Attestation" runat="server"></Virtualia:VAttest>
                            </asp:View>
                            <asp:View ID="VueEmargements" runat="server">
                                <Virtualia:VEmarge ID="Emargement" runat="server"></Virtualia:VEmarge>
                            </asp:View>
                            <asp:View ID="VueEmailConvocation" runat="server">
                                <Virtualia:NewVConvoc ID="NewConvocation" runat="server"></Virtualia:NewVConvoc>
                            </asp:View>
                            <asp:View ID="VueMessage" runat="server" >
                                <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                            </asp:View>
                        </asp:MultiView>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
</asp:Table>
