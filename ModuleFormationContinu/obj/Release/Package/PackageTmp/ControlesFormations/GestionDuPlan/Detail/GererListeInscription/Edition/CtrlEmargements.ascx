﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlEmargements.ascx.vb" Inherits="Virtualia.Net.CtrlEmargements" %>

<%@ Register src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/VListeEmargement.ascx" tagname="VListeEma" tagprefix="Virtualia" %>

<asp:Table ID="Cadre_Edition" runat="server" HorizontalAlign="Center" Width="1050px">
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_001" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_002" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_003" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_004" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_005" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_006" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_007" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_008" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_009" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_010" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_011" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_012" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_013" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_014" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_015" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_016" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_017" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_018" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_019" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeEma ID="Emargement_020" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

