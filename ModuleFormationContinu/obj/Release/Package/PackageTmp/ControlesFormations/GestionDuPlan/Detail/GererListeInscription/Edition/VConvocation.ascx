﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VConvocation.ascx.vb" Inherits="Virtualia.Net.VConvocation" %>

<style type="text/css">
.EtiquetteLeft
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:20px;
        margin-top:2px;
        text-align:left;
        text-indent:0px;
        width:450px;
    }
.EtiquetteRight
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:20px;
        margin-top:2px;
        text-align:right;
        text-indent:0px;
        width:450px;
    }
.CellCalendrier
    {
        background-color:lightgray;
        border-style:solid;
        border-width:1px;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        height:40px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:100px;
    }
.EtiCalendrier
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        height:30px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:90px;
    }
</style>

<asp:Table ID="CadreConvocation" runat="server" Width="750px" HorizontalAlign="Center">
    <asp:TableRow> 
        <asp:TableCell Height="50px" HorizontalAlign="left">
            <asp:Table ID="CadreEtablissement" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiTitre" runat="server" Text="Formation continue et concours" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtablissement" runat="server" Text="" CssClass="EtiquetteLeft" Font-Bold="true" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtaNoEtNomRue" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtaVille" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtaTelephone" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="right" VerticalAlign="Top" Height="100px">
            <asp:Table ID="CadreIndividu" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDateEdition" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"/>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIndNomPrenom" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIndNiveau1" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIndNiveau2" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="60px" HorizontalAlign="Center">
            <asp:Label ID="EtiConvocation" runat="server" Text="CONVOCATION A UNE ACTION DE FORMATION CONTINUE" Width="400px" 
                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Italic="False"
                BorderStyle="None" style="text-align:center" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreStage" runat="server" Width="700px" Height="60px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle1" runat="server" CssClass="EtiquetteLeft" Text="Nous vous informons que la formation :" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiFormation" runat="server" Text="" CssClass="EtiquetteLeft" Font-Size="Medium" 
                            Font-Bold="true" style="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle2" runat="server" CssClass="EtiquetteLeft" 
                            Text="se déroulera selon le calendrier ci-dessous :"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="200px" VerticalAlign="Top" HorizontalAlign="Center">
            <asp:Table ID="CadreCalendrier" runat="server" Width="740px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell CssClass="CellCalendrier">
                        <asp:Label ID="EtiDateDebut" runat="server" Text="Date de début" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier">
                        <asp:Label ID="EtiDateFin" runat="server" Text="Date de fin" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier">
                        <asp:Label ID="EtiHeureDebutMatin" runat="server" Text="de" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier">
                        <asp:Label ID="EtiHeureFinMatin" runat="server" Text="à" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier">
                        <asp:Label ID="EtiHeureDebutAM" runat="server" Text="de" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier">
                        <asp:Label ID="EtiHeureFinAM" runat="server" Text="à" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell CssClass="CellCalendrier" Height="80px">
                        <asp:Label ID="L1_DonDateDebut" runat="server" Text="" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Height="80px">
                        <asp:Label ID="L1_DonDateFin" runat="server" Text="" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Height="80px">
                        <asp:Label ID="L1_DonHeureDebutMatin" runat="server" Text="" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Height="80px">
                        <asp:Label ID="L1_DonHeureFinMatin" runat="server" Text="" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Height="80px">
                        <asp:Label ID="L1_DonHeureDebutAM" runat="server" Text="" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Height="80px">
                        <asp:Label ID="L1_DonHeureFinAM" runat="server" Text="" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreLieuFormation" Width="700px" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle3" runat="server" CssClass="EtiquetteLeft" 
                            Text="ADRESSE DE CONVOCATION" Font-Size="Medium" Font-Bold="true" style="text-align:left;margin-left:10px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLieuFormation" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="100%" Height="15px">
                        <asp:Label ID="EtiAdresseFormation" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="100%" Height="15px">
                        <asp:Label ID="EtiVilleFormation" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadrePolitesse" runat="server" Width="700px" Height="60px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle4" runat="server" CssClass="EtiquetteLeft" Text="Vous devez confirmer votre participation auprés de votre chef de service." />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle5" runat="server" CssClass="EtiquetteLeft" Text="En cas d'impossibilité d'assister à tout ou partie de ce stage, merci d'en informer le service des ressources humaines dans les meilleurs délais afin de libérer une place en liste d'attente." />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow> 
</asp:Table>
