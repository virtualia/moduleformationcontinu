﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlDetailStage.ascx.vb" Inherits="Virtualia.Net.CtrlDetailStage" %>

<%@ Register Src="~/Controles/PER/CtlSituationW.ascx" TagName="VGestion" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/REF/Formation/CtlPVueFOR.ascx" TagName="VFormation" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/REF/Formation/CtlValidationDemandes.ascx" TagName="VDemande" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererStage/CtrlGestionStage.ascx" TagName="CTL_GESTIONSTAGE" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlGestionListeInscription.ascx" TagName="CTL_GESTIONLISTEINSC" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlGestionMultiInscription.ascx" TagName="CTL_GESTIONMULTIINSC" TagPrefix="Formation" %>
<%@ Register Src="~/ControleGeneric/MenuBoutons.ascx" TagName="VMenus" TagPrefix="Generic" %>

<asp:Table ID="CadreVues" runat="server" Width="1150px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell ID="CellMenus">
            <asp:Table ID="CadreMenus" runat="server" Width="1150px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <Generic:VMenus ID="CtrlMenus" runat="server" HorizontalAlignement="Left" Width="1100px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="ConteneurVues" Width="1150px" VerticalAlign="Top" HorizontalAlign="Center">
            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueDetailStage" runat="server">
                    <Virtualia:VFormation ID="CtrlFormation" runat="server" />
                </asp:View>
                <asp:View ID="VueGestionStage" runat="server">
                    <Formation:CTL_GESTIONSTAGE ID="CtrlGestStage" runat="server" />
                </asp:View>
                <asp:View ID="VueGestionInscription" runat="server">
                    <Formation:CTL_GESTIONLISTEINSC ID="CtrlListInsc" runat="server" />
                </asp:View>
                <asp:View ID="VueGestionMultiInsc" runat="server">
                    <Formation:CTL_GESTIONMULTIINSC ID="CtrlMultiInsc" runat="server" />
                </asp:View>
                <asp:View ID="VueValidation" runat="server">
                    <Virtualia:VDemande ID="CtrlValidation" runat="server" />
                </asp:View>
                <asp:View ID="VueMessage" runat="server" >
                    <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                </asp:View>
                <asp:View ID="VueDossier" runat="server" >
                    <Virtualia:VGestion ID="CtrlIndividuel" runat="server" V_BackColor="#5E9598" />
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>





