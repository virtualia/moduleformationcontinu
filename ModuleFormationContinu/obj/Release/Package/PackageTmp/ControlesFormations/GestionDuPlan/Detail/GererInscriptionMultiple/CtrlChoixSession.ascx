﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlChoixSession.ascx.vb" Inherits="Virtualia.Net.CtrlChoixSession" %>

<%--<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlSessionRecurrente.ascx" TagName="CTL_GENESESSION" TagPrefix="Formation" %>--%>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlListeSession.ascx" TagName="CTL_LISTESESSION" TagPrefix="Formation" %>

<asp:Table ID="TbGenerale" runat="server" Visible="true" Width="1100px" Height="500px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Table ID="TbEntete" runat="server" Width="1000px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px" />
                    <asp:TableCell HorizontalAlign="Center" Width="800px">
                        <asp:Label ID="EtiTitre" runat="server" Text="ETAPE 1 - Sélection des sessions" Height="22px" Width="700px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="100px" >
                        <asp:ImageButton ID="CommandeSuiv" runat="server" Width="32px" Height="16px" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ImageUrl="~/Images/Boutons/PageSuivante.bmp" ImageAlign="Middle" ToolTip="ETAPE 2 - Sélection des stagiaires" Visible="true" OnClick="CommandeSuiv_Click"  />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" ColumnSpan="2" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
<%--            <asp:MultiView ID="MultiOnglet" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">--%>
                    <Formation:CTL_LISTESESSION ID="ctrlLstSession" runat="server" />
                <%--</asp:View>--%>
                <%--<asp:View ID="VueGeneSession" runat="server">
                    <Formation:CTL_GENESESSION ID="CtrlGeneSession" runat="server" />
                </asp:View>--%>
            <%--</asp:MultiView>--%>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
