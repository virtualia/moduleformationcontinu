﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlListeVue3.ascx.vb" Inherits="Virtualia.Net.CtrlListeVue3" %>

<asp:Table ID="TbGenerale" runat="server" Width="1100px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
    <asp:TableRow>
        <asp:TableCell>
            <%--    --%>
            <asp:Panel ID="Panel1" runat="server" Width="1100px" ScrollBars="Auto" Style="min-height: 200px">
                <asp:Table ID="TbDetail" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" BackColor="#A8BBB8" Width="100%" Font-Size="80%">
                    <asp:TableRow ID="RDetail" VerticalAlign="Top" ForeColor="White" BackColor="#A8BBB8">
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:CheckBox ID="ChkSelection" runat="server" Width="65px" Height="20px" AutoPostBack="true" OnCheckedChanged="ChkSelection_CheckedChanged" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="60px" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label23" runat="server" Text="Dossier" Width="60px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="25%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label24" runat="server" Text="Stagiaire" Width="230px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label25" runat="server" Text="Contexte" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label26" runat="server" Text="Cursus" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label27" runat="server" Text="Date inscription" Width="80px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label28" runat="server" Text="Demande d'hébergement" Width="80px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label29" runat="server" Text="Priorité" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label30" runat="server" Text="Présence" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label31" runat="server" Text="Filière" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label32" runat="server" Text="Module" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label33" runat="server" Text="Nb. d'heures de déplacement" Width="80px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label34" runat="server" Text="Organisme" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label35" runat="server" Text="Organisme payeur des coûts pédagogiques" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label36" runat="server" Text="Organisme payeur des autres coûts" Width="215px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                    </asp:TableRow>



                    <asp:TableRow Height="20px" BackColor="#E2E2E2">
                        <asp:TableCell Width="65px" HorizontalAlign="Center">
                            <asp:CheckBox ID="CheckBox1" runat="server" Width="65px" />
                        </asp:TableCell>

                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Table ID="Table2" runat="server" Width="60px" CellPadding="0" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Button ID="Button1" runat="server" Width="30px" Height="20px" BorderStyle="NotSet" ToolTip="Cliquer pour voir le détail" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>

                        <asp:TableCell Height="20px" Width="25%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label9" runat="server" Text="Stagiaire" Width="230px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label10" runat="server" Text="Contexte" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label11" runat="server" Text="Cursus" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label12" runat="server" Text="Date inscription" Width="80px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label13" runat="server" Text="Demande d'hébergement" Width="80px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label14" runat="server" Text="Au titre DIF" Width="80px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label38" runat="server" Text="Au titre DIF hors temps de travail" Width="80px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label39" runat="server" Text="Filière" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label40" runat="server" Text="Module" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label41" runat="server" Text="Nb. d'heures de déplacement" Width="80px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label42" runat="server" Text="Organisme" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label43" runat="server" Text="Organisme payeur des coûts pédagogiques" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label44" runat="server" Text="Organisme payeur des autres coûts" Width="215px" Style="text-align: left;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="Panel2" runat="server" Width="1100px">
                <asp:Table ID="Table3" runat="server" BorderStyle="NotSet" BorderWidth="2px" BorderColor="Transparent" Width="100%" Font-Size="80%">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell ColumnSpan="2" Width="130px" VerticalAlign="Middle">
                            <asp:Label ID="Label21" runat="server" Width="128px" Text="Pour la session" Font-Bold="true" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="100%" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotDossier" runat="server" Text="" Width="200px" Font-Bold="true" Style="text-align: left;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
