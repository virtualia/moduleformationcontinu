﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlListeVue2.ascx.vb" Inherits="Virtualia.Net.CtrlListeVue2" %>

<asp:Table ID="TbGenerale" runat="server" Visible="true" Width="1100px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnl" runat="server" Width="1100px">
                <asp:Table ID="tb" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" ForeColor="White" BackColor="#A8BBB8" Width="100%" Font-Size="80%">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:CheckBox ID="ChkSelection" runat="server" Width="65px" AutoPostBack="true" OnCheckedChanged="ChkSelection_CheckedChanged" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="60px" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label4" runat="server" Text="Dossier" Width="60px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="40%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label2" runat="server" Text="Stagiaire" Width="370px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label3" runat="server" Text="Nbr. d'heures" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label1" runat="server" Text="Nbr. de jours" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label6" runat="server" Text="Coûts pédagogiques" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label7" runat="server" Text="Coûts annexes" Width="80px" Height="30px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label8" runat="server" Text="Coût du déplacement" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label15" runat="server" Text="Coût de la restauration" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label16" runat="server" Text="Coût de l'hébergement" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <%--   --%>
            <asp:Panel ID="Panel1" runat="server" Width="1100px" ScrollBars="Auto" Style="min-height: 200px">
                <asp:Table ID="TbDetail" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" BackColor="#A8BBB8" Width="100%" Font-Size="80%">
                </asp:Table>

            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="Panel2" runat="server" Width="1100px">
                <asp:Table ID="Table3" runat="server" BorderStyle="NotSet" BorderWidth="2px" BorderColor="Transparent" Width="100%" Font-Size="80%">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell ColumnSpan="2" Width="130px" VerticalAlign="Middle">
                            <asp:Label ID="Label21" runat="server" Width="128px" Text="Pour la session" Font-Bold="true" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="40%" HorizontalAlign="Left" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotDossier" runat="server" Text="" Width="370px" Font-Bold="true" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotHeure" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotJour" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotPedagogique" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotForfaitaire" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotDeplacement" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotRestauration" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotHebergement" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
