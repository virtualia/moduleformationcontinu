﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlConvocations.ascx.vb" Inherits="Virtualia.Net.CtrlConvocations" %>

<%@ Register src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/VConvocation.ascx" tagname="VConvoc" tagprefix="Virtualia" %>

<asp:Table ID="Cadre_Edition" runat="server" HorizontalAlign="Center" Width="1050px">
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_001" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_002" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_003" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_004" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_005" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_006" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_007" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_008" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_009" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_010" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_011" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_012" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_013" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
<asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_014" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_015" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_016" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_017" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_018" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_019" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_020" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_021" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_022" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_023" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_024" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_025" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_026" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_027" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_028" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_029" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_030" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_031" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_032" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_033" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_034" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_035" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_036" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_037" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_038" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_039" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_040" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_041" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_042" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_043" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_044" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_045" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_046" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_047" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_048" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_049" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_050" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_051" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_052" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_053" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_054" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_055" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_056" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_057" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_058" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_059" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_060" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_061" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_062" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_063" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_064" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_065" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_066" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_067" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_068" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_069" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_070" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_071" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_072" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_073" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_074" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_075" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_076" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_077" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_078" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_079" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_080" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_081" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_082" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_083" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_084" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_085" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_086" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_087" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_088" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_089" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_090" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_091" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_092" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_093" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
<asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_094" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_095" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_096" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_097" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_098" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_099" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VConvoc ID="Convocation_100" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
