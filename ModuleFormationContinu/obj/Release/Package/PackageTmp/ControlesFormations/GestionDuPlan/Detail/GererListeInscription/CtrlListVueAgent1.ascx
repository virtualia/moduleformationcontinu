﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlListVueAgent1.ascx.vb" Inherits="Virtualia.Net.CtrlListVueAgent1" %>

<asp:Table ID="TbGenerale" runat="server" Width="1100px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnl" runat="server" Width="1100px">
                <asp:Table ID="tb" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" ForeColor="White" BackColor="#A8BBB8" Width="100%" Font-Size="80%">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:CheckBox ID="ChkSelection" runat="server" Width="65px" Height="20px" AutoPostBack="true" OnCheckedChanged="ChkSelection_CheckedChanged" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="60px" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label4" runat="server" Text="Dossier" Width="60px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="25%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label2" runat="server" Text="Session" Width="230px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label3" runat="server" Text="Suivi" Width="150px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="15%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label5" runat="server" Text="Motif" Width="150px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="20%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label8" runat="server" Text="Action" Width="235px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label6" runat="server" Text="Au titre du DIF" Width="80px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="20%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label7" runat="server" Text="Au titre du DIF hors temps de travail" Width="80px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="Panel1" runat="server" Width="1100px" ScrollBars="Auto" Style="min-height: 200px">
                <asp:Table ID="TbDetail" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" BackColor="#A8BBB8" Width="100%" Font-Size="80%">
                    <asp:TableRow Height="20px" BackColor="#E2E2E2">
                        <asp:TableCell Width="65px" HorizontalAlign="Center">
                            <asp:CheckBox ID="CheckBox1" runat="server" Width="65px" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Table ID="Table2" runat="server" Width="60px" CellPadding="0" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Button ID="Button1" runat="server" Width="30px" Height="20px" BackColor="Gray" BorderStyle="NotSet" ToolTip="Cliquer pour voir le détail" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>

                        <asp:TableCell Height="20px" Width="25%" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label9" runat="server" Text="Du 01/01/2014 au 01/01/2014" Width="100%" Style="text-align: left; text-indent: 2px" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%">
                            <asp:Label ID="Label10" runat="server" Text="Inscrit" Width="80px" Height="20px" Style="text-align: left; text-indent: 2px" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="15%">
                            <asp:Label ID="Label11" runat="server" Text="" Width="90px" Height="20px" Style="text-align: left; text-indent: 2px" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="10%">
                            <asp:Label ID="Label12" runat="server" Text="P1" Width="80px" Height="20px" Style="text-align: left; text-indent: 2px" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="20%">
                            <asp:Label ID="Label13" runat="server" Text="" Width="100px" Height="20px" Style="text-align: left; text-indent: 2px" />
                        </asp:TableCell>
                        <asp:TableCell Width="20%">
                            <asp:Label ID="Label14" runat="server" Text="aaaaa" Width="100px" Height="20px" Style="text-align: left; text-indent: 2px" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="Panel2" runat="server" Width="1100px">
                <asp:Table ID="Table3" runat="server" BorderStyle="NotSet" BorderWidth="2px" BorderColor="Transparent" Width="100%" Font-Size="80%">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell ColumnSpan="2" Width="130px" VerticalAlign="Middle">
                            <asp:Label ID="Label21" runat="server" Width="128px" Text="Pour le stagiaire" Font-Bold="true" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="100%" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotDossier" runat="server" Text="" Width="200px" Font-Bold="true" Style="text-align: left;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
