﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlGestionStage.ascx.vb" Inherits="Virtualia.Net.CtrlGestionStage" %>

<%@ Register Src="~/ControleGeneric/MenuArbre.ascx" TagName="V_MenuArbre" TagPrefix="Generic" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererStage/CtrlSuppression.ascx" TagName="CTL_SUPPRESSION" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererStage/CtrlVerification.ascx" TagName="CTL_VERIF" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererStage/CtrlDuplication.ascx" TagName="CTL_DUPLIC" TagPrefix="Formation" %>

<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1150px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Tb1" runat="server" Width="1150px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="200px">
                        <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="lblTitre" runat="server" Text="Gérer - XXXXXXXXXXXXXXX" Height="22px" Width="900px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="200px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="tb2" runat="server">
                <asp:TableRow>
                    <asp:TableCell ID="CellMenuArbre" Width="400px" HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Table ID="Tb3" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8" Width="390px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Generic:V_MenuArbre ID="CtrlMenus" runat="server" Width="390px" Height="500px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell Width="600px" HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Table ID="Tb4" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8" Width="590px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VueVerif" runat="server">
                                            <Formation:CTL_VERIF ID="CtrlVerif" runat="server" />
                                        </asp:View>
                                        <asp:View ID="VueDuplic" runat="server">
                                            <Formation:CTL_DUPLIC ID="CtrlDuplic" runat="server" />
                                        </asp:View>
                                        <asp:View ID="VueSuppression" runat="server">
                                            <Formation:CTL_SUPPRESSION ID="CtrlSuppression" runat="server" />
                                        </asp:View>
                                    </asp:MultiView>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

