﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlChoixStagiaire.ascx.vb" Inherits="Virtualia.Net.CtrlChoixStagiaire" %>

<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register Src="~/ControleGeneric/Armoire.ascx" TagName="CTL_ARMOIRE" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonSupprimer.ascx" TagName="BTN_SUPPR" TagPrefix="Generic" %>

<asp:Table ID="TbGenerale" runat="server" Visible="true" Width="1100px" Height="500px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Table ID="TbEntete" runat="server" Width="1000px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px">
                        <asp:ImageButton ID="CommandePrec" runat="server" Width="32px" Height="16px" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ImageUrl="~/Images/Boutons/PagePrecedente.bmp" ImageAlign="Middle" ToolTip="ETAPE 1 - Sélection des sessions" OnClick="CommandePrec_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="800px">
                        <asp:Label ID="EtiTitre" runat="server" Text="ETAPE 2 - Sélection des stagiaires" Height="22px" Width="700px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="100px">
                        <asp:ImageButton ID="CommandeSuiv" runat="server" Width="32px" Height="16px" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ImageUrl="~/Images/Boutons/PageSuivante.bmp" ImageAlign="Middle" ToolTip="ETAPE 3 - Modalités des inscriptions" Visible="true" OnClick="CommandeSuiv_Click" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Tb2" runat="server" Width="1050px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ColumnSpan="2">
                        <asp:Table ID="tb3" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                                    <asp:Label ID="EtiSousTitre" runat="server" Width="220px" Text="Sélection des stagiaires à partir de" ForeColor="White" BorderStyle="None" />
                                </asp:TableCell>
                                <asp:TableCell Width="5px" />
                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                    <asp:DropDownList ID="CboSource" runat="server" Height="25px" Width="700px" BackColor="Snow" AutoPostBack="true" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="CboSource_SelectedIndexChanged" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                        <Generic:CTL_ARMOIRE ID="CtrlArmoireStagiaire" runat="server" Titre="Dossiers" Height="227px" LettreDefaut="A" SiFiltre0="false" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                        <Generic:CTL_ARMOIRE ID="CtrlArmoireCible" runat="server" Titre="Stagiaires sélectionnés" Width="300px" Height="280px" LettreDefaut="_TOUS" BackColor="#6C9690" SiFiltre="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                   <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                       <asp:Table ID="CadreSelection" runat="server" BackColor="#124545">
                           <asp:TableRow>
                               <asp:TableCell>
                                   <asp:Label ID="EtiNomSelection" runat="server" Width="610px" height="25px" Text="" BackColor="#124545" 
                                        ForeColor="Snow" Font-Bold="true" Font-Italic="true" BorderStyle="None" style="text-align:center" />
                               </asp:TableCell>
                               <asp:TableCell>
                                   <asp:ImageButton ID="CmdDossier" runat="server" ImageUrl="~/Images/General/Fenetre.bmp" ImageAlign="NotSet" Tooltip="Accéder au dossier"
                                        BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="1px"/>
                               </asp:TableCell>
                           </asp:TableRow>
                       </asp:Table>
                   </asp:TableCell>
                   <asp:TableCell Width="300px" HorizontalAlign="Right" VerticalAlign="Top">
                        <Generic:BTN_SUPPR ID="BtnSupprimer" runat="server" />
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ID="CellDemandes" HorizontalAlign="Center" VerticalAlign="Top" ColumnSpan="2">
                      <asp:Label ID="EtiDEmande" runat="server" Width="850px" height="25px" Text="" BackColor="#124545" ForeColor="Snow" BorderStyle="None" />
                      <Virtualia:VListeGrid ID="ListeDemandes" runat="server" CadreWidth="850px" SiColonneSelect="false" SiCaseAcocher="false" />
                   </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ID="CellHisto" HorizontalAlign="Center" VerticalAlign="Top" ColumnSpan="2">
                      <asp:Label ID="EtiHisto" runat="server" Width="850px" height="25px" Text="" BackColor="#124545" ForeColor="Snow" BorderStyle="None" />
                      <Virtualia:VListeGrid ID="ListeHisto" runat="server" CadreWidth="850px" SiColonneSelect="false" SiCaseAcocher="false" />
                   </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
