﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlGestionListeInscription.ascx.vb" Inherits="Virtualia.Net.CtrlGestionListeInscription" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/ControleGeneric/V_ChoixSurListe.ascx" TagName="ListeReferences" TagPrefix="Generic" %>
<%@ Register Src="~/ControlesGeneriques/V_Message.ascx" TagName="VMessage" TagPrefix="Generic" %>
<%@ Register Src="~/ControlesGeneriques/V_GridSelection.ascx" TagName="VDataGrid" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/MenuBoutons.ascx" TagName="VMenus" TagPrefix="Generic" %>

<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlListeVue1.ascx" TagName="CTL_LISTE1" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlListeVue2.ascx" TagName="CTL_LISTE2" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlListeVue3.ascx" TagName="CTL_LISTE3" TagPrefix="Formation" %>

<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlListVueAgent1.ascx" TagName="CTL_LISTEAGT1" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlListVueAgent2.ascx" TagName="CTL_LISTEAGT2" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlListVueAgent3.ascx" TagName="CTL_LISTEAGT3" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/CtrlEditionInscription.ascx" TagName="CTL_EDITION" TagPrefix="Formation" %>

<%@ Register Src="~/Controles/REF/Formation/FOR_INSCRIPTION.ascx" TagName="FOR_INSCRIPTION" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlSuppressionInscriptions.ascx" TagName="CTL_SUPPR" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/CtrlModifInscription.ascx" TagName="CTL_MODIF" TagPrefix="Formation" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" HorizontalAlign="Center" Width="1150px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Tb1" runat="server" Width="1150px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="200px">
                        <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="lblTitre" runat="server" Text="Inscriptions - XXXXXXXXXXXXXXX" Height="22px" Width="900px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="200px">
                        <Generic:BTN_GENE ID="btnVue" runat="server" Text="Vue par stagiaire" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell ID="CellDataGrid" HorizontalAlign="Center">
            <asp:MultiView ID="MultiVueGrille" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueGrdSession" runat="server">
                    <Generic:VDataGrid ID="ListeGrilleSession" runat="server" Width="850px" SiPagination="true" TaillePage="8" />
                </asp:View>
                <asp:View ID="VueGrdAgt" runat="server">
                    <Generic:VDataGrid ID="ListeGrilleAgt" runat="server" Width="850px" SiPagination="true" TaillePage="8" />
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="tbMenus" runat="server" Width="1100px">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell>
                        <asp:Table ID="Table2" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Button ID="BtnINSC" runat="server" Text="Inscriptions" Width="120px" BackColor="#0E5F5C" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor: pointer" OnClick="BtnTab_Click" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Button ID="BtnCOUT" runat="server" Text="Coûts et durées" Width="120px" BackColor="#137A75" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor: pointer" OnClick="BtnTab_Click" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Button ID="BtnINFO" runat="server" Text="Compléments" Width="120px" BackColor="#19968F" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor: pointer;" OnClick="BtnTab_Click" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" Width="260px">
                        <asp:Label ID="lbl" runat="server" Width="250px" BackColor="Transparent" ForeColor="White" BorderStyle="None" BorderColor="Transparent" Text="Actions possibles sur la sélection" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="text-align: right;" />
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" Width="450px">
                        <asp:DropDownList ID="cboAction" runat="server" Width="440px" AutoPostBack="true" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="cboAction_SelectedIndexChanged" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TbEdition" runat="server" Width="1100px">
                <asp:TableRow HorizontalAlign="Center">
                    <asp:TableCell Width="378px" />
                    <asp:TableCell VerticalAlign="Middle" Width="260px">
                        <asp:Label ID="EtiLstEdition" runat="server" Width="250px" BackColor="Transparent" ForeColor="White" BorderStyle="None" BorderColor="Transparent" Text="Editions possibles" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="text-align: right" />
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" Width="450px">
                        <asp:DropDownList ID="LstChoixEdition" runat="server" Width="440px" AutoPostBack="true" ForeColor="#124545" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="CadreTelecharger" Visible="false">
                    <asp:TableCell Width="638px" ColumnSpan="2" />
                    <asp:TableCell HorizontalAlign="Left" Width="440px">
                           <asp:Button ID="CmdTelecharger" runat="server" Text="" Width="440px" BackColor="#B0E0D7" CommandName="Telecharger" OnCommand="CmdTelecharger_Commande"
                               BorderColor="#124545" BorderStyle="Outset" BorderWidth="1px" ForeColor="Blue" Style="cursor: pointer"
                               Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Font-Underline="true"/>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow >
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false" ColumnSpan="2">
            <asp:UpdateProgress ID="UpdateAttente" runat="server">
                <ProgressTemplate>
                    <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                            </asp:TableCell>
                            <asp:TableCell Width="10px" Height="10px" />
                            <asp:TableCell>
                                <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table1" runat="server" Width="1150px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="LblTitreSelection" runat="server" Text="Session du XXXXXXX" Width="700px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="center">
            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                <%--0--%>
                <asp:View ID="Vue1" runat="server">
                    <Formation:CTL_LISTE1 ID="CtrlListe1" runat="server" />
                </asp:View>
                <%--1--%>
                <asp:View ID="Vue2" runat="server">
                    <Formation:CTL_LISTE2 ID="CtrlListe2" runat="server" />
                </asp:View>
                <%--2--%>
                <asp:View ID="VueAgt1" runat="server">
                    <Formation:CTL_LISTEAGT1 ID="CtrlListeAgt1" runat="server" />
                </asp:View>
                <%--3--%>
                <asp:View ID="VueAgt2" runat="server">
                    <Formation:CTL_LISTEAGT2 ID="CtrlListeAgt2" runat="server" />
                </asp:View>
                <%--4--%>
                <asp:View ID="VueForInsc" runat="server">
                    <Formation:FOR_INSCRIPTION ID="CtrlGestInscription" runat="server" />
                </asp:View>
                <%--5--%>
                <asp:View ID="VueSuppr" runat="server">
                    <Formation:CTL_SUPPR ID="CtrlSuppression" runat="server" />
                </asp:View>
                <%--6--%>
                <asp:View ID="VueModif" runat="server">
                    <Formation:CTL_MODIF ID="CtrlModification" runat="server" />
                </asp:View>
                <%--7--%>
                <asp:View ID="VueSysRef" runat="server">
                    <Generic:ListeReferences ID="Referentiel" runat="server" />
                </asp:View>
                <%--8--%>
                <asp:View ID="Vue3" runat="server">
                    <Formation:CTL_LISTE3 ID="CtrlListe3" runat="server" />
                </asp:View>
                <%--9--%>
                <asp:View ID="View1" runat="server">
                    <Formation:CTL_LISTEAGT3 ID="CtrlListeAgt3" runat="server" />
                </asp:View>
                <%--10--%>
                <asp:View ID="VueEdition" runat="server">
                    <Formation:CTL_EDITION ID="CtrlGestionEdition" runat="server" />
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ID="CellTotaux">
            <asp:Panel ID="Panel2" runat="server" Width="1100px">
                <asp:Table ID="Table3" runat="server" Width="800px" Font-Size="80%" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px" RowSpan="2">
                            <asp:Label ID="Label1" runat="server" Text="Pour le stage" Width="150px" Font-Bold="true" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="223px">
                            <asp:Label ID="Label2" runat="server" Text="Nombre de stagiaires" Width="223px" Style="font-style: oblique; text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label3" runat="server" Text="Nbr. d'heures" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label4" runat="server" Text="Nbr. de jours" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label6" runat="server" Text="Coûts forfaitaires" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%">
                            <asp:Label ID="Label7" runat="server" Text="Coûts pédagogiques" Width="80px" Height="30px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label8" runat="server" Text="Coût du déplacement" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label15" runat="server" Text="Coût de la restauration" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="Label16" runat="server" Text="Coût de l'hébergement" Width="80px" Style="font-style: oblique; text-align: center;" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="223px" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotDossier" runat="server" Text="" Width="223px" Font-Bold="true" Style="text-align: left;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotHeure" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotJour" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotForfaitaire" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotPedagogique" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotDeplacement" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotRestauration" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                        <asp:TableCell Height="20px" Width="16%" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#E2E2E2">
                            <asp:Label ID="LblTotHebergement" runat="server" Text="" Width="80px" Font-Bold="true" Style="text-align: right;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HidenPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Generic:VMessage ID="CtlMsg" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HidenPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

