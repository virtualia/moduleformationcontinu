﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlListeSession.ascx.vb" Inherits="Virtualia.Net.CtrlListeSession" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/Armoire.ascx" TagName="CTL_ARMOIRE" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonSupprimer.ascx" TagName="BTN_SUPPR" TagPrefix="Generic" %>

<asp:Table ID="tb1" runat="server">

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow Height="50px" VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Label ID="Label1" runat="server" Height="20px" Width="350px" Text="Sessions" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
            <asp:Table ID="TbGenerale" runat="server" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="Snow">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Panel ID="pnl" runat="server" Width="700px">
                            <asp:Table ID="tb" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" ForeColor="White" BackColor="#A8BBB8" Width="100%" Font-Size="85%">
                                <asp:TableRow VerticalAlign="Top">
                                    <asp:TableCell Height="20px" Width="65px" HorizontalAlign="Center" VerticalAlign="Middle">
                                        <asp:Label ID="Label3" runat="server" Text="" Width="65px" Style="font-style: oblique; text-align: center;" />
                                    </asp:TableCell>
                                    <asp:TableCell Height="20px" Width="100%" HorizontalAlign="Left" VerticalAlign="Middle">
                                        <asp:Label ID="Label2" runat="server" Text="Session" Width="100%" Style="font-style: oblique; text-align: left;" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Panel ID="Panel1" runat="server" Width="700px" ScrollBars="Auto" Style="height: auto; min-height: 150px">
                            <asp:Table ID="TbDetail" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="Transparent" BackColor="Snow" Width="100%" Font-Size="85%">
                            </asp:Table>
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Generic:BTN_GENE ID="BtnGeneSessionRecc" runat="server" ToolTip="Choisir des dates récurrentes" Text="Dates récurrentes" Width="Normal" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TBreccurente" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Height="2px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TBkkk" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="Table1" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" Width="450px" Height="300px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" Height="20px">
                                                <asp:Label ID="Label5" runat="server" Height="20px" Width="250px" Text="Sélection des dates récurrentes" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="font-style: oblique; text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Table ID="Table2" runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                                                            <asp:Label ID="Label6" runat="server" Width="50px" Text="Année" ForeColor="White" BorderStyle="None" />
                                                        </asp:TableCell>
                                                        <asp:TableCell Width="5px" />
                                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                                            <asp:DropDownList ID="CboAnnee" runat="server" Height="25px" Width="100px" BackColor="Snow" AutoPostBack="true" ForeColor="#124545" OnSelectedIndexChanged="CboAnnee_SelectedIndexChanged" />
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                                                            <asp:Label ID="Label7" runat="server" Width="50px" Text="Mois" ForeColor="White" BorderStyle="None" />
                                                        </asp:TableCell>
                                                        <asp:TableCell Width="5px" />
                                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                                            <asp:DropDownList ID="CboMois" runat="server" Height="25px" Width="200px" BackColor="Snow" AutoPostBack="true" ForeColor="#124545" OnSelectedIndexChanged="CboMois_SelectedIndexChanged" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Table ID="TbBoutonsCalendrier" runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Label8" runat="server" Width="50px" Text="lu" ForeColor="White" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Label9" runat="server" Width="50px" Text="ma" ForeColor="White" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Label10" runat="server" Width="50px" Text="me" ForeColor="White" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Label11" runat="server" Width="50px" Text="je" ForeColor="White" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Label12" runat="server" Width="50px" Text="ve" ForeColor="White" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Label13" runat="server" Width="50px" Text="sa" ForeColor="White" Style="text-align: center;" BackColor="Gray" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Label ID="Labelfffff4" runat="server" Width="50px" Text="di" ForeColor="White" Style="text-align: center;" BackColor="Gray" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="R1">
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_1_lu" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_1_ma" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_1_me" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_1_je" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_1_ve" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell RowSpan="6" BackColor="Gray" />
                                                        <asp:TableCell RowSpan="6" BackColor="Gray" />
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="R2">
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_2_lu" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_2_ma" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_2_me" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_2_je" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_2_ve" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <%--<asp:TableCell/>--%>
                                                        <%--<asp:TableCell/>--%>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="R3">
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_3_lu" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_3_ma" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_3_me" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_3_je" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_3_ve" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="R4">
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_4_lu" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_4_ma" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_4_me" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_4_je" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_4_ve" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="R5">
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_5_lu" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_5_ma" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_5_me" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_5_je" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_5_ve" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow ID="R6">
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_6_lu" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_6_ma" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_6_me" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_6_je" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:Button ID="Btn_6_ve" runat="server" Width="50px" Style="text-align: center;" />
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>

                                <asp:TableCell VerticalAlign="Top">
                                    <asp:Table ID="Table5" runat="server" Width="350px" Height="300px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Height="20px" ColumnSpan="4">
                                                <asp:Label ID="Label20" runat="server" Height="20px" Width="200px" Text="Dates choisies" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="font-style: oblique; text-align: center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="Label14" runat="server" Width="150px" Text="Nb Heures" ForeColor="White" BorderStyle="None" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="5px" />
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="Label22" runat="server" Width="150px" Text="Nb Jours" ForeColor="White" BorderStyle="None" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="5px" />
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" ColumnSpan="4">
                                                <Generic:CTL_ARMOIRE ID="CtrlArmoireRecurrente" runat="server" Titre="" Width="320px" Height="200px" LettreDefaut="_TOUS" BackColor="#6C9690" SiFiltre="false" VAppelant="FOR" />
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow Height="40px">
                                            <asp:TableCell Width="1000px" HorizontalAlign="Right" ColumnSpan="2">
                                                <Generic:BTN_SUPPR ID="BtnSuppr" runat="server" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
