﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VAttestation.ascx.vb" Inherits="Virtualia.Net.VAttestation" %>

<style type="text/css">
.EtiquetteLeft
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:20px;
        margin-top:2px;
        text-align:left;
        text-indent:0px;
        width:450px;
    }
.EtiquetteRight
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:20px;
        margin-top:2px;
        text-align:right;
        text-indent:0px;
        width:450px;
    }
.CellCalendrier
    {
        background-color:lightgray;
        border-style:solid;
        border-width:1px;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        height:40px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:100px;
    }
.EtiCalendrier
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        height:30px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:90px;
    }
</style>

<asp:Table ID="CadreCovocation" runat="server" Width="750px" HorizontalAlign="Center">
    <asp:TableRow> 
        <asp:TableCell Height="50px" HorizontalAlign="left">
            <asp:Table ID="CadreEtablissement" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiTitre" runat="server" Text="Formation continue et concours" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtablissement" runat="server" Text="" CssClass="EtiquetteLeft" Font-Bold="true" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtaNoEtNomRue" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtaVille" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiEtaTelephone" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="right" VerticalAlign="Top" Height="100px">
            <asp:Table ID="CadreIndividu" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDateEdition" runat="server" Text="Le 3 décembre 2014" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"/>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIndNomPrenom" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIndNiveau1" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIndNiveau2" runat="server" Text="" CssClass="EtiquetteRight" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="60px" HorizontalAlign="Center">
            <asp:Label ID="EtiConvocation" runat="server" Text="ATTESTATION DE STAGE" Width="400px" 
                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Italic="False"
                BorderStyle="None" style="text-align:center" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreStage" runat="server" Width="700px" Height="60px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle1" runat="server" CssClass="EtiquetteLeft" Text="Je soussigné(e), certifie que" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiStagiaire" runat="server" Text="" CssClass="EtiquetteLeft" Font-Size="Medium" 
                            Font-Bold="true" style="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle2" runat="server" CssClass="EtiquetteLeft" 
                            Text="a participé au stage de formation :"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiFormation" runat="server" Text="" CssClass="EtiquetteLeft" Font-Size="Medium" 
                            Font-Bold="true" style="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle3" runat="server" CssClass="EtiquetteLeft" 
                            Text="qui s'est déroulé :"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDates" runat="server" CssClass="EtiquetteLeft" Font-Size="Medium" 
                            Font-Bold="true" style="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDuree" runat="server" CssClass="EtiquetteLeft" Font-Size="Medium" 
                            Font-Bold="true" style="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreLieuFormation" Width="700px" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle4" runat="server" CssClass="EtiquetteLeft" 
                            Text="LIEU" Font-Size="Medium" Font-Bold="true" style="text-align:left;margin-left:10px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLieuFormation" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="100%" Height="15px">
                        <asp:Label ID="EtiAdresseFormation" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="100%" Height="15px">
                        <asp:Label ID="EtiVilleFormation" runat="server" Text="" CssClass="EtiquetteLeft" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreSignature" runat="server" Width="700px" Height="60px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLibelle5" runat="server" CssClass="EtiquetteLeft" Text="Le responsable du service" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow> 
</asp:Table>

