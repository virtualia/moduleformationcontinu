﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlConfirmation.ascx.vb" Inherits="Virtualia.Net.CtrlConfirmation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/ControleGeneric/ArbreSelection.ascx" TagName="CTL_ARBRE" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/PopupConfirmInsc.ascx" TagName="CTL_CONFIRM" TagPrefix="Formation" %>

<asp:Table ID="TbGenerale" runat="server" Visible="true" Width="1100px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TbEntete" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px">
                        <asp:ImageButton ID="CommandePrec" runat="server" Width="32px" Height="16px" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ImageUrl="~/Images/Boutons/PagePrecedente.bmp" ImageAlign="Middle" ToolTip="ETAPE 3 - Modalités des inscriptions" OnClick="CommandePrec_Click" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="800px">
                        <asp:Label ID="EtiTitre" runat="server" Text="ETAPE 4 - Confirmation des inscriptions" Height="22px" Width="700px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="100px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" >
            <%----%>
            <asp:Table ID="tb1" runat="server" Width="1100px" >
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" >
                        <Generic:CTL_ARBRE ID="CtrlArbreResultat" runat="server" SiFiltrePlanVisible="False" Width="400px" Height="490px" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="500px">
                        <asp:Table ID="tb2" runat="server">
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:UpdatePanel ID="pnlTrait" runat="server">
                                        <ContentTemplate>
                                            <asp:Table ID="Table1" runat="server" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px" Width="400px">
                                                <asp:TableRow Height="40px">
                                                    <asp:TableCell HorizontalAlign="Center">
                                                        <Generic:BTN_GENE ID="BtnTrait" runat="server" Text="Exécuter" ToolTip="Confirmer les inscriptions"  />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:UpdatePanel ID="UpPnlMessage" runat="server">
                                        <ContentTemplate>
                                            <asp:Timer ID="HorlogeTraitement" runat="server" Interval="50" Enabled="false" />
                                            <asp:Table ID="TbMessage" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table ID="Table" runat="server" HorizontalAlign="Center">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="TxtNbTot" runat="server" Height="20px" Width="150px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: center;" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="TxtNbTrait" runat="server" Height="20px" Width="150px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: center;" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="TxtNbAnomalie" runat="server" Height="20px" Width="150px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: center;" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Center">
                                                        <asp:TextBox ID="TxtStatut" runat="server" Height="20px" Width="500px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Center">
                                                        <asp:TextBox ID="txtMsg" runat="server" Height="330px" Width="500px" TextMode="MultiLine" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Size="95%" Font-Bold="True" Style="text-align: left;" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow Height="50px">
                                                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false">
                                                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                                            <ProgressTemplate>
                                                                <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                                                                    <asp:TableRow>
                                                                        <asp:TableCell>
                                                                            <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell Width="10px" Height="10px" />
                                                                        <asp:TableCell>
                                                                            <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                                                        </asp:TableCell>
                                                                    </asp:TableRow>
                                                                </asp:Table>
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="BtnTrait" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HidenPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Formation:CTL_CONFIRM ID="CtrlConfirm" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HidenPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
