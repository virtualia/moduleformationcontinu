﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlDuplication.ascx.vb" Inherits="Virtualia.Net.CtrlDuplication" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Width="590px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Label ID="EtiExplic1" runat="server" Text="Virtualia va dupliquer ce stage. Vous devez sélectionner un plan cible et indiquer le nouvel intitulé de stage." Width="450px" ForeColor="White"  Font-Italic="true" Font-Bold="True" Style="text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow Height="40px">
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:UpdatePanel ID="upd1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnl1" runat="server" Width="580px">
                        <asp:Table ID="Table1" runat="server" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px" Width="100%">
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Width="33%">
                                    <asp:Label ID="Label2" runat="server" Height="18px" Width="100%" Text="Sélection du plan cible" ForeColor="White" BorderStyle="None" />
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" Width="67%">
                                    <asp:DropDownList ID="CboPlanCible" runat="server" AutoPostBack="true" Height="25px" Width="97%" BackColor="Snow" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="CboPlanCible_SelectedIndexChanged" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="BtnTrait" />
                    <asp:AsyncPostBackTrigger ControlID="txtNouvelleValeur" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow Height="40px">
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:UpdatePanel ID="upd2" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnl2" runat="server" Width="580px">
                        <asp:Table ID="Table3" runat="server" Width="100%" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                            <asp:TableRow Height="25px">
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:Label ID="Label3" runat="server" Height="18px" Width="100%" Text="Nouvel intitulé (120 caractères maximum)" ForeColor="White" BorderStyle="None" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                    <asp:TextBox ID="txtNouvelleValeur" runat="server" Height="16px" Width="97%" MaxLength="113" BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" ForeColor="Black" AutoPostBack="true" Style="text-align: left" TextMode="SingleLine" Font-Size="75%" OnTextChanged="txtNouvelleValeur_TextChanged" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="BtnTrait" />
                    <asp:AsyncPostBackTrigger ControlID="CboPlanCible" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow Height="40px">
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:UpdatePanel ID="upd3" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnl3" runat="server" Width="400px">
                        <asp:Table ID="Table2" runat="server" Width="100%" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                            <asp:TableRow Height="40px">
                                <asp:TableCell Width="100%" HorizontalAlign="Center">
                                    <Generic:BTN_GENE ID="BtnTrait" runat="server" Text="Exécuter" Width="Grand" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="CboPlanCible" />
                    <asp:AsyncPostBackTrigger ControlID="txtNouvelleValeur" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:UpdatePanel ID="UpPnlMessage" runat="server">
                <ContentTemplate>
                    <asp:Timer ID="TimerTraitement" runat="server" Interval="50" Enabled="false" />
                    <asp:Table ID="TbMessage" runat="server" Visible="true" Width="580px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                        <asp:TableRow>
                            <asp:TableCell Height="5px" />
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:TextBox ID="TxtStatut" runat="server" Height="20px" Width="540px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:TextBox ID="txtMsg" runat="server" Height="270px" Width="540px" TextMode="MultiLine" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="50px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false">
                                <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                    <ProgressTemplate>
                                        <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                                </asp:TableCell>
                                                <asp:TableCell Width="10px" Height="10px" />
                                                <asp:TableCell>
                                                    <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="BtnTrait" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
