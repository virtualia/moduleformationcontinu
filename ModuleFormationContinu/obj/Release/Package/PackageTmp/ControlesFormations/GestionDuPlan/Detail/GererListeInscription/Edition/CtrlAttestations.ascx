﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlAttestations.ascx.vb" Inherits="Virtualia.Net.CtrlAttestations" %>

<%@ Register src="~/ControlesFormations/GestionDuPlan/Detail/GererListeInscription/Edition/VAttestation.ascx" tagname="VAttest" tagprefix="Virtualia" %>

<asp:Table ID="Cadre_Edition" runat="server" HorizontalAlign="Center" Width="1050px">
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_001" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_002" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_003" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_004" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_005" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_006" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_007" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_008" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_009" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_010" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_011" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_012" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_013" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
<asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_014" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_015" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_016" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_017" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_018" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_019" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_020" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_021" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_022" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_023" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_024" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_025" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_026" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_027" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_028" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_029" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_030" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_031" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_032" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_033" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_034" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_035" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_036" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_037" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_038" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_039" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_040" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_041" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_042" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_043" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_044" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_045" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_046" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_047" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_048" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_049" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_050" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_051" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_052" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_053" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_054" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_055" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_056" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_057" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_058" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_059" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_060" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_061" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_062" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_063" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_064" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_065" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_066" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_067" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_068" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_069" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_070" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_071" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_072" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_073" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_074" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_075" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_076" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_077" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_078" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_079" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_080" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_081" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_082" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_083" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_084" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_085" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_086" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_087" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_088" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_089" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_090" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_091" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_092" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_093" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
<asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_094" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_095" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_096" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_097" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_098" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_099" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VAttest ID="Attestation_100" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

