﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlExportGraphique2.ascx.vb" Inherits="Virtualia.Net.CtrlExportGraphique2" %>

<asp:Table ID="TB1" runat="server" Width="768px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="White">

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table2" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="Label3" runat="server" Text="Répartition par thème" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table3" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell ID="Theme_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Theme_NB" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Theme_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Theme_Cout" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Theme_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Theme_Jour" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table4" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="Label1" runat="server" Text="Répartition par type d'action" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table5" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell ID="TypeAction_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_TypeAction_NB" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="TypeAction_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_TypeAction_Cout" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="TypeAction_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_TypeAction_Jour" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="Label2" runat="server" Text="Répartition par niveau 1" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table6" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell ID="Niveau1_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Niveau1_NB" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Niveau1_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Niveau1_Cout" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Niveau1_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Niveau1_Jour" runat="server" Height="200px" Width="300px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Bottom">
        <asp:TableCell HorizontalAlign="Right">
            <asp:Label ID="lblNumPage" runat="server" Text="Virtualia.net Page " Width="500px" BackColor="White" BorderStyle="None" ForeColor="LightGray" Style="font-style: oblique; text-indent: 5px; text-align: right; margin-right: 10px" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
