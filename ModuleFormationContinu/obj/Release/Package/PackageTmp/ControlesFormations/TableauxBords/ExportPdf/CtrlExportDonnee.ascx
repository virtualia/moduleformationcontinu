﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlExportDonnee.ascx.vb" Inherits="Virtualia.Net.CtrlExportDonnee" %>

<asp:Table ID="TbGenerale" runat="server" Width="768px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="White">

    <asp:TableRow ID="rTitre" VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="TABLEAU" Height="22px" Width="600px" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell Height="10px" />
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center" ID="CellDonnee"/>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Bottom">
        <asp:TableCell HorizontalAlign="Right">
            <asp:Label ID="EtiNumPage" runat="server" Text="Virtualia.net Page 1" Width="500px" BorderStyle="None" ForeColor="LightGray" Style="font-style: oblique; text-indent: 5px; text-align: right; margin-right: 10px" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell >
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
