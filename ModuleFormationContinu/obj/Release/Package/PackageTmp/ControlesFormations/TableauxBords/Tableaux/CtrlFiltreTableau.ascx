﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlFiltreTableau.ascx.vb" Inherits="Virtualia.Net.CtrlFiltreTableau" %>

<asp:Table ID="CadreControle" runat="server">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Timer ID="TimerAttente" runat="server" Interval="10" Enabled="false" />
            <asp:Table ID="CadreTB" runat="server">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreFiltre" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="CadreAnnee" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="EtiAnnee" runat="server" Text="Année" Height="20px" Width="60px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ListeAnnee" runat="server" Height="20px" Width="110px" BackColor="#EEECFD" 
                                                    BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" OnSelectedIndexChanged="cboAnnee_SelectedIndexChanged" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="CadrePlan" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="EtiPlan" runat="server" Text="Plan de formation" Height="20px" Width="220px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ListePlan" runat="server" Height="20px" Width="390px" BackColor="#EEECFD" 
                                                    BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" OnSelectedIndexChanged="CboPlan_SelectedIndexChanged" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell />
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="CadreEtab" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="EtiEtab" runat="server" Text="Etablissement" Height="20px" Width="220px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ListeEtab" runat="server" Height="20px" Width="390px" BackColor="#EEECFD" BorderColor="#B0E0D7" 
                                                    BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" OnSelectedIndexChanged="CboEtab_SelectedIndexChanged" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Table ID="CadreCmd" runat="server" Height="20px" Width="150px" BackImageUrl="~/Images/General/Cmd_Std.bmp" 
                                        BorderWidth="2px" BorderStyle="Outset" BackColor="#FF0E5E5C" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Button ID="CmdExecuter" runat="server" Text="Exécuter" Width="125px" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" 
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="cursor:pointer" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
