﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlVueTableauBord.ascx.vb" Inherits="Virtualia.Net.CtrlVueTableauBord" %>

<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlChiffreBrut.ascx" TagName="CTL_CHIFFRES_BRUTS" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlTableau.ascx" TagName="CTL_TABLEAU" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlGraphique.ascx" TagName="CTL_GRAPH" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlApercuDossier.ascx" TagName="CTL_APERCU" TagPrefix="Formation" %>

<asp:Table ID="CadreControle" runat="server" BackColor="#5E9598">
    <asp:TableRow ID="RowDonneeFiltre">
        <asp:TableCell>
            <asp:Table ID="CadreEntete" runat="server" Width="1050px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px">
                        <asp:ImageButton ID="CmdRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" ToolTip="Retour vers le filtre" OnClick="BtnRetour_Click" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Width="1000px" BorderWidth="1px" BorderStyle="NotSet" BorderColor="Snow">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiAnnee" runat="server" Text="Année : XXXX" Height="22px" Width="130px" ForeColor="Snow" Style="text-align: left;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiEtab" runat="server" Text="Etablissement : XXXX" Height="22px" Width="400px" ForeColor="Snow" Style="text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="32px" />
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiPlan" runat="server" Text="Plan de formation : XXXX" Height="22px" Width="400px" ForeColor="Snow" Style="text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TBDONNEE" runat="server" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TbVueAction" runat="server" HorizontalAlign="Center" Width="1050px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Table ID="CadreResultat" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:Label ID="EtiResultat" runat="server" Text="Résultats" Height="20px" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboVuPar" runat="server" Height="20px" Width="250px" BackColor="#EEECFD" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" OnSelectedIndexChanged="cboVuPar_SelectedIndexChanged">
                                                    <asp:ListItem Text="Chiffres bruts" Value="0" />
                                                    <asp:ListItem Text="Tableaux de synthèse" Value="1" />
                                                    <asp:ListItem Text="Graphiques" Value="2" />
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:UpdatePanel ID="UpdatePanelExport" runat="server">
                                        <ContentTemplate>
                                            <asp:Table ID="CadreRestitution" runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Label ID="EtiRestitution" runat="server" Text="Restitutions possibles" Height="20px" Width="180px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="CboAction" runat="server" Height="20px" Width="400px" BackColor="#EEECFD" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" OnSelectedIndexChanged="CboAction_SelectedIndexChanged" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="CboAction" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="2px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="center">
                        <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                            <asp:View ID="Vue1" runat="server">
                                <Formation:CTL_CHIFFRES_BRUTS ID="CtrlChiffresBruts" runat="server" />
                            </asp:View>
                            <asp:View ID="Vue2" runat="server">
                                <Formation:CTL_TABLEAU ID="CtrlTableauSynthese" runat="server" />
                            </asp:View>
                            <asp:View ID="Vue3" runat="server">
                                <Formation:CTL_GRAPH ID="CtrlGraph" runat="server" />
                            </asp:View>
                            <asp:View ID="VueApercu" runat="server">
                                <Formation:CTL_APERCU ID="CtrlApercu" runat="server" />
                            </asp:View>
                        </asp:MultiView>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
