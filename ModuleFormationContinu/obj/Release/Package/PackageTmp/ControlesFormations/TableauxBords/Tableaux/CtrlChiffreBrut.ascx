﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlChiffreBrut.ascx.vb" Inherits="Virtualia.Net.CtrlChiffreBrut" %>

<asp:Table ID="TbGenerale" runat="server" HorizontalAlign="Center" Visible="true" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="Chiffres bruts" Height="22px" Width="500px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="ROWCONTEXTE" Visible="false">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiContexte" runat="server" Text="Chiffres bruts" Width="500px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreChiffres" runat="server" CellPadding="0" CellSpacing="0" ForeColor="Black">
                <asp:TableRow BackColor="#19968D" ForeColor="Snow" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label1" runat="server" Text="TOTAUX" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label2" runat="server" Text="Nb actions de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label3" runat="server" Text="Nb bénéficiaires" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label4" runat="server" Text="Nb stagiaires" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="EtiEffectif" runat="server" Text="Effectif au XX/XX/XXXX" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label8" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Tot_Nb_Action_Formation" runat="server" BackColor="Transparent" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Tot_Nb_Action_Formation_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Tot_Nb_Benef" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Tot_Nb_Benef_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Tot_Nb_Stagiaire" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Tot_Nb_Stagiaire_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Tot_Nb_Effectif" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Tot_Nb_Effectif_Click" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#19968D" ForeColor="Snow" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label11" runat="server" Text="DUREES" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label12" runat="server" Text="Nb heures de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label13" runat="server" Text="Nb jours de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label9" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Width="170px">
                        <asp:Label ID="Label10" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label16" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Tot_Nb_Heure" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Tot_Nb_Heure_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Tot_Nb_Jour" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Tot_Nb_Jour_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label17" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label28" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#19968D" ForeColor="Snow" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px" BackColor="Snow">
                        <asp:Label ID="Label29" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label22" runat="server" Text="Durée moyenne action de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label23" runat="server" Text="Durée moyenne par personne formée" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label24" runat="server" Text="Durée moyenne par stagiaire" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label25" runat="server" Text="Durée moyenne par personne à l'effectif" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label5" runat="server" Text="en heures" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Action_Formation_h" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Beneficiaire_h" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Stagiaire_h" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Effectif_h" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label7" runat="server" Text="en jours" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Action_Formation_j" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Beneficiaire_j" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Stagiaire_j" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Duree_Moyenne_Effectif_j" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#19968D" ForeColor="Snow" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label14" runat="server" Text="COUTS" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label15" runat="server" Text="Total coûts" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label21" runat="server" Text="Coûts pédagogiques" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label26" runat="server" Text="Coûts frais de mission" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label27" runat="server" Text="Coûts masse salariale" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label30" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Cout_Tot" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Cout_Tot_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Cout_Pedagogique" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Cout_Pedagogique_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Button ID="Cout_Frais_Mission" runat="server" Text="" BackColor="Transparent" Width="170px" BorderStyle="None" BorderWidth="0px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Font-Italic="true" Style="text-align: center; cursor:pointer" OnClick="Cout_Frais_Mission_Click" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Cout_Masse_Salarial" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: right; margin-right: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#19968D" ForeColor="Snow" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px" BackColor="Snow">
                        <asp:Label ID="Label31" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label6" runat="server" Text="Coût moyen par action de formation" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label18" runat="server" Text="Coût moyen par personne formée" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label19" runat="server" Text="Coût moyen par stagiaire" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label20" runat="server" Text="Coût moyen par personne à l'effectif" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label32" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Cout_Moyen_Action_Formation" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-right: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Cout_Moyen_Beneficiaire" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-right: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Cout_Moyen_Stagiaires" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-right: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Cout_Moyen_Effectif" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-right: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#19968D" ForeColor="Snow" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label40" runat="server" Text="EFFORT DE FORMATION" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label41" runat="server" Text="Pourcentage de l'effectif formé" Width="170px" BorderStyle="None" Font-Bold="true" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label42" runat="server" Text="Pourcentage de l'effectif non formé" Width="170px" BorderStyle="None" Font-Bold="true" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label33" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Width="170px">
                        <asp:Label ID="Label34" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="Snow" Font-Italic="true" Font-Bold="false">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label35" runat="server" Text="" Width="110px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Pourcent_Effectif_Forme" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Pourcent_Effectif_Non_Forme" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label36" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label37" runat="server" Text="" Width="170px" BorderStyle="None" BorderWidth="0px" Style="text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

</asp:Table>
