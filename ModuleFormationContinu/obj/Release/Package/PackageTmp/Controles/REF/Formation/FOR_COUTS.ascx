﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_COUTS" Codebehind="FOR_COUTS.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="470px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" >
  <asp:TableRow> 
    <asp:TableCell> 
      <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="460px" SiCaseAcocher="false" SiColonneSelect="true" />
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow> 
    <asp:TableCell> 
        <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
          BorderColor="#FFEBC8" Height="530px" Width="450px" HorizontalAlign="Center">
          <asp:TableRow> 
            <asp:TableCell> 
              <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
                    CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell  HorizontalAlign="Center">
                            <asp:Label ID="Etiquette" runat="server" Text="Coûts du stage" Height="20px" Width="300px"
                                BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#FFF2DB"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                                font-style: oblique; text-indent: 5px; text-align: center;" >
                            </asp:Label>          
                        </asp:TableCell>      
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreDate" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px" >
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard"
                               V_PointdeVue="9" V_Objet="6" V_Information="0" V_SiDonneeDico="true"
                               EtiWidth="65px" DonTabIndex="1" />
                        </asp:TableCell>    
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left"  >
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="12" V_SiDonneeDico="true"
                               EtiWidth="190px" DonWidth="160px" DonTabIndex="2" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="12px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>   
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreCouts" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="300px" >
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="1" V_SiDonneeDico="true"
                               EtiWidth="190px" DonWidth="70px" DonTabIndex="3" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro1" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="13" V_SiDonneeDico="true"
                               EtiWidth="190px" DonWidth="70px" DonTabIndex="4" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro13" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="4px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreAutreCouts" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="340px" >
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="2" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="5"
                               Etistyle="margin-left: 40px;" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro2" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="5" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="6"
                               Etistyle="margin-left: 40px;" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro5" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="6" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="7"
                               Etistyle="margin-left: 40px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro6" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="14" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="8"
                               Etistyle="margin-left: 40px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro14" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="4px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="3" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="9"
                               Etistyle="margin-left: 40px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro3" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="4" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="10"
                               Etistyle="margin-left: 40px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro4" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="11" V_SiDonneeDico="true"
                               EtiWidth="230px" DonWidth="70px" DonTabIndex="11"
                               Etistyle="margin-left: 40px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro11" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauPriseEnCharge" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px" >
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left"  >
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server"
                                V_PointdeVue="9" V_Objet="6" V_Information="7" V_SiDonneeDico="true"
                                EtiWidth="225px" DonWidth="205px" DonTabIndex="12"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>   
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauMontant" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="340px" >
                  <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                               V_PointdeVue="9" V_Objet="6" V_Information="8" V_SiDonneeDico="true"
                               EtiWidth="80px" DonWidth="70px" DonTabIndex="13"
                               Etistyle="margin-left: 149px;" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro8" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                        <asp:TableCell Height="4px" Columnspan="2"></asp:TableCell>
                  </asp:TableRow>   
                </asp:Table>   
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauOrganismes" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px" >   
                   <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                                V_PointdeVue="9" V_Objet="6" V_Information="9" V_SiDonneeDico="true"
                                EtiWidth="225px" DonWidth="205px" DonTabIndex="14"/>
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server"
                                V_PointdeVue="9" V_Objet="6" V_Information="10" V_SiDonneeDico="true"
                                EtiWidth="225px" DonWidth="205px" DonTabIndex="15"/>
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                        <asp:TableCell Height="15px"></asp:TableCell>
                   </asp:TableRow>  
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
        </asp:Table>  
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>
 
