﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_SESSION" Codebehind="FOR_SESSION.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="550px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" >
  <asp:TableRow> 
    <asp:TableCell> 
      <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="500px" SiCaseAcocher="false" SiColonneSelect="true"/>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow> 
    <asp:TableCell> 
        <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#FFEBC8" Height="670px" Width="550px" HorizontalAlign="Center">
          <asp:TableRow>
           <asp:TableCell>
             <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
                    CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="Etiquette" runat="server" Text="Session de formation" Height="20px" Width="300px"
                                BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#FFF2DB"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                                font-style: oblique; text-indent: 5px; text-align: center;">
                            </asp:Label>          
                        </asp:TableCell>      
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreDates" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px" >
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                               V_PointdeVue="9" V_Objet="5" V_Information="0" V_SiDonneeDico="true" DonTabIndex="1"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="InfoD15" runat="server" TypeCalendrier="Standard" EtiWidth="120px" SiDateFin="True" SiDureeVisible="True"
                               V_PointdeVue="9" V_Objet="5" V_Information="15" V_SiDonneeDico="true" DonTabIndex="2"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="5px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreDuree" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="350px">
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_SiEnLectureSeule="true"
                               V_PointdeVue="9" V_Objet="5" V_Information="9" V_SiDonneeDico="true"
                               EtiWidth="220px" DonWidth="30px" DonTabIndex="3"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelJourOuvre" runat="server" Text="jours" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;">
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreDureeHoraire" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="455px" >
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="10" V_SiDonneeDico="true"
                               EtiWidth="220px" DonWidth="30px" DonTabIndex="4"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelHeures" runat="server" Text="heures" Height="20px" Width="50px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;">
                            </asp:Label> 
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_SiEnLectureSeule="true"
                               V_PointdeVue="9" V_Objet="5" V_Information="3" V_SiDonneeDico="true"
                               EtiWidth="60px" DonWidth="30px" DonTabIndex="5"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelJourHor" runat="server" Text="jours" Height="20px" Width="25px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;">
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="3px" Columnspan="4"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreInscriptions" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="429px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelInscrits" runat="server" Text="Inscriptions prévues" Height="20px" Width="150px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 20px; text-align: left;">
                            </asp:Label> 
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="1" V_SiDonneeDico="true"
                               EtiWidth="70px" DonWidth="30px" DonTabIndex="6"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="2" V_SiDonneeDico="true"
                               EtiWidth="70px" DonWidth="30px" DonTabIndex="7"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px" Columnspan="3"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreSession" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="530px">
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="18" V_SiDonneeDico="true"
                               EtiWidth="130px" DonWidth="400px" DonTabIndex="8"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="19" V_SiDonneeDico="true"
                               EtiWidth="90px" DonWidth="120px" DonTabIndex="9"
                               Etistyle="margin-left: 44px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab20" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="20" V_SiDonneeDico="true"
                               EtiWidth="90px" DonWidth="250px" DonTabIndex="10"
                               Etistyle="margin-left: 44px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab21" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="21" V_SiDonneeDico="true"
                               EtiWidth="90px" DonWidth="250px" DonTabIndex="11"
                               Etistyle="margin-left: 44px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="TableauLieu" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="530px"> 
                   <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="DonTab25" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="25" V_SiDonneeDico="true"
                               Etivisible="True" EtiWidth="40px" EtiText="Lieu" DonWidth="396px" DonTabIndex="12" 
                               Donstyle="margin-top: -1px;" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="TableauAdresse" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px">
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="7" V_SiDonneeDico="true"
                               EtiVisible="false" DonWidth="390px" DonTabIndex="13" Donstyle="margin-left: 48px; margin-top: -1px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="23" V_SiDonneeDico="true"
                               EtiVisible="false" DonWidth="60px" DonTabIndex="14"
                               Donstyle="margin-left: 48px; margin-top: -1px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH24" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="24" V_SiDonneeDico="true"
                               EtiVisible="false" DonWidth="319px" DonTabIndex="15" 
                               Donstyle="margin-top: -1px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="2px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="TableauHorairesSession" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="455px">
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="11" V_SiDonneeDico="true"
                               EtiWidth="30px" DonWidth="40px" DonTabIndex="16"
                               Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="12" V_SiDonneeDico="true"
                               EtiWidth="20px" DonWidth="40px" DonTabIndex="17"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelTiret" runat="server" Text="-" Height="20px" Width="20px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 7px; text-align: left;">
                            </asp:Label> 
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="13" V_SiDonneeDico="true"
                               EtiWidth="30px" DonWidth="40px" DonTabIndex="18"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="14" V_SiDonneeDico="true"
                               EtiWidth="20px" DonWidth="40px" DonTabIndex="19"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px" Columnspan="5"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="TableauSalle" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="530px"> 
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab22" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="22" V_SiDonneeDico="true"
                               EtiWidth="90px" DonWidth="350px" DonTabIndex="20"
                               Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="6" V_SiDonneeDico="true"
                               EtiWidth="90px" DonWidth="290px" DonTabIndex="21"
                               Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="4px"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab16" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="16" V_SiDonneeDico="true"
                               EtiWidth="140px" DonWidth="225px" DonTabIndex="22"
                               Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="17" V_SiDonneeDico="true"
                               EtiWidth="140px" DonWidth="219px" DonTabIndex="23"
                               Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="4px"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                             <Virtualia:VCocheSimple ID="Coche26" runat="server"
                               V_PointdeVue="9" V_Objet="5" V_Information="26" V_SiDonneeDico="true"
                               V_Width="150px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="TableauObservation" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="540px">
                    <asp:TableRow> 
                         <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server" DonTextMode="true"
                              V_PointdeVue="9" V_Objet="5" V_Information="4" V_SiDonneeDico="true"
                              EtiWidth="535px" DonWidth="533px" DonHeight="50px" DonTabIndex="25"
                              Etistyle="margin-left: 2px; text-align:center;" Donstyle="margin-left: 2px;"/>
                         </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="15px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
          </asp:TableRow>   
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>
 
