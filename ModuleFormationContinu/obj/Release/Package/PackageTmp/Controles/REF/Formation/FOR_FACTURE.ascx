﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_FACTURE" Codebehind="FOR_FACTURE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="450px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
  <asp:TableRow> 
    <asp:TableCell> 
      <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="460px" SiCaseAcocher="false" SiColonneSelect="true"/>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow> 
    <asp:TableCell> 
        <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
          BorderColor="#FFEBC8" Height="480px" Width="450px" HorizontalAlign="Center">
          <asp:TableRow> 
            <asp:TableCell> 
              <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell> 
                    <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
                        CellSpacing="0" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell  HorizontalAlign="Center">
                                <asp:Label ID="Etiquette" runat="server" Text="Suivi de la facturation" Height="20px" Width="300px"
                                    BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                                    BorderWidth="2px" ForeColor="#FFF2DB"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                                    font-style: oblique; text-indent: 5px; text-align: center;" >
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                    </asp:Table>  
                </asp:TableCell>
          </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell> 
                     <asp:Table ID="CadreDate" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px">
                       <asp:TableRow> 
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="125px"
                                            V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="7" V_Information="0" DonTabIndex="1"/>
                            </asp:TableCell>    
                       </asp:TableRow>
                       <asp:TableRow>
                            <asp:TableCell Height="12px"></asp:TableCell>
                       </asp:TableRow>
                     </asp:Table> 
                </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreFacture" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="300px" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="1" V_SiDonneeDico="true"
                               EtiWidth="100px" DonWidth="124px" DonTabIndex="2"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="4" V_SiDonneeDico="true"
                               EtiWidth="100px" DonWidth="70px" DonTabIndex="3"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro4" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="2" V_SiDonneeDico="true"
                               EtiWidth="120px" DonWidth="130px" DonTabIndex="4"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="5" V_SiDonneeDico="true"
                               EtiWidth="100px" DonWidth="70px" DonTabIndex="5"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro5" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;">
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="3" V_SiDonneeDico="true"
                               EtiWidth="100px" DonWidth="150px" DonTabIndex="6"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="6" V_SiDonneeDico="true"
                               EtiWidth="100px" DonWidth="70px" DonTabIndex="7"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">  
                            <asp:Label ID="LabelEuro6" runat="server" Text="€" Height="20px" Width="30px"
                                BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                font-style: oblique; text-indent: 1px; text-align: left;" >
                            </asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px" Columnspan="3"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauPeriode" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="400px" >
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="InfoD10" runat="server" TypeCalendrier="Standard" EtiWidth="150px"
                                            V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="7" V_Information="10" DonTabIndex="8"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="InfoD11" runat="server" TypeCalendrier="Standard" EtiWidth="40px" SiDateFin="true"
                                            V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="7" V_Information="11" DonTabIndex="9"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauComptaFacture" runat="server" Height="20px" CellPadding="0" CellSpacing="0" Width="400px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="InfoD12" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                            V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="7" V_Information="12" DonTabIndex="10" Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="InfoD13" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                            V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="7" V_Information="13" DonTabIndex="11" Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="14" V_SiDonneeDico="true"
                               EtiWidth="170px" DonWidth="150px" DonTabIndex="12"
                               Etistyle="margin-left: 50px;"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="4px"></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="8" V_SiDonneeDico="true"
                               EtiWidth="170px" DonWidth="250px" DonTabIndex="13" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                               V_PointdeVue="9" V_Objet="7" V_Information="9" V_SiDonneeDico="true"
                               EtiWidth="170px" DonWidth="250px" DonTabIndex="14" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="8px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauObservation" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="450px">
                    <asp:TableRow> 
                         <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV07" runat="server" DonTextMode="true"
                              V_PointdeVue="9" V_Objet="7" V_Information="7" V_SiDonneeDico="true"
                              EtiWidth="437px" DonWidth="435px" DonHeight="50px" DonTabIndex="15"
                              Etistyle="margin-left: 5px; text-align:center;" Donstyle="margin-left: 5px;"/>
                         </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="15px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
        </asp:Table>  
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>
 
