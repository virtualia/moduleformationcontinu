﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_INTERVENANT" Codebehind="FOR_INTERVENANT.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="510px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" >
  <asp:TableRow> 
    <asp:TableCell> 
      <Virtualia:VListeGrid ID="ListeGrille" runat="server"  CadreWidth="520px" SiColonneSelect="true" SiCaseAcocher="false"/> 
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow> 
    <asp:TableCell> 
        <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
          BorderColor="#FFEBC8" Height="425px" Width="510px" HorizontalAlign="Center">
          <asp:TableRow> 
            <asp:TableCell> 
              <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche" >
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
                    CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="Etiquette" runat="server" Text="Intervenants" Height="20px" Width="300px"
                                BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#FFF2DB"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                                font-style: oblique; text-indent: 5px; text-align: center;" >
                            </asp:Label>          
                        </asp:TableCell>      
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="CadreDon" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="510px" >
                    <asp:TableRow> 
                         <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="1" V_SiDonneeDico="true"
                               EtiWidth="106px" DonWidth="382px" DonTabIndex="1" />
                         </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                         <asp:TableCell Height="8px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>   
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauTelephone" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="350px">   
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="7" V_SiDonneeDico="true"
                               EtiWidth="80px" DonWidth="130px" DonTabIndex="2" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="8" V_SiDonneeDico="true"
                               EtiWidth="130px" DonWidth="130px" DonTabIndex="3" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauMail" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="350px">  
                   <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="9" V_SiDonneeDico="true"
                               EtiWidth="50px" DonWidth="350px" DonTabIndex="4" />
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                        <asp:TableCell Height="10px"></asp:TableCell>
                   </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauOrganisme" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="510px" > 
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="2" V_SiDonneeDico="true"
                               EtiWidth="110px" DonWidth="306px" DonTabIndex="5"
                               Etistyle="margin-left: 40px;" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="1px"></asp:TableCell>
                    </asp:TableRow>  
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="3" V_SiDonneeDico="true"
                               EtiWidth="110px" DonWidth="300px" DonTabIndex="6"
                               Etistyle="margin-left: 40px;" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="4" V_SiDonneeDico="true"
                               EtiVisible="false" DonWidth="300px" DonTabIndex="7"
                               Donstyle="margin-left: 154px;" />
                        </asp:TableCell>
                    </asp:TableRow>  
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauAdresse" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="300px">   
                    <asp:TableRow> 
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="5" V_SiDonneeDico="true"
                               EtiVisible="false" DonWidth="60px" DonTabIndex="8" Donstyle="margin-left: 154px;"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                               V_PointdeVue="9" V_Objet="9" V_Information="6" V_SiDonneeDico="true"
                               EtiVisible="false" DonWidth="230px" DonTabIndex="9"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell> 
                <asp:Table ID="TableauObservation" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="510px">
                    <asp:TableRow> 
                         <asp:TableCell HorizontalAlign="Left" >
                            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV10" runat="server" DonTextMode="true"
                              V_PointdeVue="9" V_Objet="9" V_Information="10" V_SiDonneeDico="true"
                              EtiWidth="500px" DonWidth="498px" DonHeight="90px" DonTabIndex="10"
                              Etistyle="margin-left: 2px; text-align:center;" Donstyle="margin-left: 2px;"/>
                         </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                         <asp:TableCell Height="15px"></asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
            </asp:TableCell>
          </asp:TableRow>    
        </asp:Table>  
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>
 
