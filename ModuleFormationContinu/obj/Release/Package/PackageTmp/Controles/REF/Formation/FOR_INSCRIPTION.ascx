﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FOR_INSCRIPTION.ascx.vb" Inherits="Virtualia.Net.FOR_INSCRIPTION" %>

<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" Width="700px" HorizontalAlign="Center" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px" >
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                         <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Détail de l'inscription" Height="20px" Width="500px" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="TbDetail" runat="server" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Inscription personnalisée--%>
                                    <Virtualia:VCocheSimple ID="Coche28" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="28" V_SiDonneeDico="true"
                                         V_Width="250px" V_Text="Inscription personnalisée" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Date de début de la formation--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="14" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="1" V_SiEnLectureSeule="false" DonStyle="text-align: center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Date de fin de la formation--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="15" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="2" V_SiEnLectureSeule="false" DonStyle="text-align: center;" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Date d'inscription--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="2" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Référence de la session--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="16" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="2" V_SiEnLectureSeule="false" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Nombre d'heures de formation--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="12" V_SiDonneeDico="true" 
                                        DonWidth="100px" EtiText="Nombre d'heures" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Height="2px" VerticalAlign="Top" />
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="TbStagiaire" runat="server" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px" Width="700px">
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="Label2" runat="server" Height="20px" Width="350px" Text="Stagiaire" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Appartenance du stagiaire au personnel--%>
                                    <Virtualia:VCocheSimple ID="Coche07" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="7" V_SiDonneeDico="true" V_Width="250px" V_Text="Appartenance au personnel" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Nom du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="3" V_SiDonneeDico="true" 
                                        DonWidth="400px" DonTabIndex="3" EtiText="Nom" EtiWidth="250px" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Prénom du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="4" V_SiDonneeDico="true" 
                                        DonWidth="400px" DonTabIndex="3" EtiText="Prénom" EtiWidth="250px" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Date de naissance du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="5" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="3" EtiText="Date de naissance" EtiWidth="250px" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Etablissement du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="6" V_SiDonneeDico="true" 
                                        DonWidth="400px" DonTabIndex="3" EtiText="Etablissement" EtiWidth="250px" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Direction--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH29" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="29" V_SiDonneeDico="true" 
                                        DonWidth="400px" DonTabIndex="3" EtiText="Direction" EtiWidth="250px" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Service--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH30" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="30" V_SiDonneeDico="true" 
                                        DonWidth="400px" DonTabIndex="3" EtiText="Service" EtiWidth="250px" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Height="2px" VerticalAlign="Top" />
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="TbFormation" runat="server" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px" Width="700px">
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="Label5" runat="server" Height="20px" Width="350px" Text="Formation" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Action de formation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab31" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="31" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiText="Action" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Organisme de formation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab32" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="32" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiText="Organisme" V_SiEnLectureSeule="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Contexte de formation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab33" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="33" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiText="Contexte" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Filière de formation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab34" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="34" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiText="Filière" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Cursus de formation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab26" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="26" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiText="Cursus" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Module de formation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab27" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="27" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiText="Module" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Height="2px" VerticalAlign="Top" />
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="TbSuivi" runat="server" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px" Width="700px">
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="Label6" runat="server" Height="20px" Width="350px" Text="Suivi" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Suivi de l'inscription--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab17" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="17" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Motif de non participation--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab18" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="18" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Suivi de la présence au stage--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab19" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="19" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Ordre de priorité à l'inscription--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab20" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="20" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="400px" EtiWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Au titre du DIF--%>
                                    <Virtualia:VCocheSimple ID="Coche35" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="35" V_SiDonneeDico="true" 
                                        V_Width="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Hors temps de travail--%>
                                    <Virtualia:VCocheSimple ID="Coche36" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="36" V_SiDonneeDico="true" 
                                        V_Width="250px" />
                                </asp:TableCell>
                            </asp:TableRow>

                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Height="2px" VerticalAlign="Top" />
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="TbCouts" runat="server" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px" Width="700px">
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="Label3" runat="server" Height="20px" Width="350px" Text="Coûts" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Coût forfaitaire stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="25" V_SiDonneeDico="true"
                                         DonWidth="100px" DonTabIndex="3" EtiText="Forfaitaires" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Coût pédagogique du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="8" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="3" EtiText="Pédagogiques" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Nombre d'heures de déplacement--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="13" V_SiDonneeDico="true"
                                         DonWidth="100px" DonTabIndex="3" EtiText="Nb heures de déplacement" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Coût déplacement du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="9" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="3" EtiText="De déplacement" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Coût restauration--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="10" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="3" EtiText="De restauration" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <%--Coût hébergement du stagiaire--%>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH111" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="11" V_SiDonneeDico="true" 
                                        DonWidth="100px" DonTabIndex="3" EtiText="D'hébergement" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Organisme payeur des coûts pédagogiques--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab23" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="23" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="380px" EtiWidth="300px" EtiHeight="40px" EtiText="Organisme payeur des coûts pédagogiques" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <%--Organisme payeur des autres coûts--%>
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab24" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="24" V_SiDonneeDico="true" 
                                        DonTabIndex="7" DonWidth="380px" EtiWidth="300px" EtiHeight="40px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell Height="2px" VerticalAlign="Top" />
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="TbCommentaire" runat="server" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px" Width="700px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2" Height="170px">
                                    <%--Commentaire--%>
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV22" runat="server" DonTextMode="true" V_PointdeVue="9" V_Objet="8" V_Information="22" V_SiDonneeDico="true" 
                                        EtiWidth="500px" DonWidth="498px" DonHeight="120px" DonTabIndex="1" EtiStyle="margin-left: 2px; text-align:center;" DonStyle="margin-left: 2px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>



