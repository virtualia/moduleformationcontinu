﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_CARACTERISTIC" Codebehind="FOR_CARACTERISTIC.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#FFEBC8" Height="470px" Width="680px" HorizontalAlign="Center">
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px" >
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                        BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Caractéristiques du stage" Height="20px" Width="300px"
                        BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#FFF2DB"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;" >
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreDon" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" >
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="1" V_SiDonneeDico="true"
                       EtiWidth="100px" DonWidth="300px" DonTabIndex="1" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="10" V_SiDonneeDico="true"
                       EtiWidth="90px" DonWidth="125px" DonTabIndex="2" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="9" V_SiDonneeDico="true"
                       EtiWidth="100px" DonWidth="300px" DonTabIndex="3" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="11" V_SiDonneeDico="true"
                       EtiWidth="90px" DonWidth="125px" DonTabIndex="4" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" Columnspan="2"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauAffectation" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" > 
          <asp:TableRow> 
            <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server"
                    V_PointdeVue="9" V_Objet="3" V_Information="14" V_SiDonneeDico="true"
                    EtiWidth="240px" DonWidth="350px" DonTabIndex="5"
                    Etistyle="margin-left: 50px;"/>
            </asp:TableCell>
          </asp:TableRow>  
          <asp:TableRow> 
            <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server"
                    V_PointdeVue="9" V_Objet="3" V_Information="6" V_SiDonneeDico="true"
                    EtiWidth="240px" DonWidth="350px" DonTabIndex="6"
                    Etistyle="margin-left: 50px;"/>
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
            <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VDuoEtiquetteCommande ID="Dontab13" runat="server"
                    V_PointdeVue="9" V_Objet="3" V_Information="13" V_SiDonneeDico="true"
                    EtiWidth="240px" DonWidth="350px" DonTabIndex="7"
                    Etistyle="margin-left: 50px;"/>
            </asp:TableCell>
          </asp:TableRow>
          <asp:TableRow>
            <asp:TableCell Height="15px"></asp:TableCell>
          </asp:TableRow>  
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreBudget" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" >
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="8" V_SiDonneeDico="true"
                       EtiWidth="160px" DonWidth="170px" DonTabIndex="8" />
                 </asp:TableCell>
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="17" V_SiDonneeDico="true"
                       EtiWidth="180px" DonWidth="110px" DonTabIndex="9" />
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDate ID="InfoD15" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                               V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="3" V_Information="15" DonTabIndex="10"/>
                 </asp:TableCell> 
                 <asp:TableCell HorizontalAlign="Left"  >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab16" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="16" V_SiDonneeDico="true"
                       EtiWidth="80px" DonWidth="216px" DonTabIndex="11" />
                 </asp:TableCell>   
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="15px" Columnspan="2"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauNiveauQualif" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="600px" >
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left"  >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server"
                      V_PointdeVue="9" V_Objet="3" V_Information="12" V_SiDonneeDico="true"
                      EtiWidth="330px" DonWidth="328px" DonTabIndex="12"
                      Etistyle="margin-left: 20px; text-align:center;"/>
                 </asp:TableCell>    
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left"  >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                      V_PointdeVue="9" V_Objet="3" V_Information="2" V_SiDonneeDico="true"
                      EtiWidth="200px" DonWidth="198px" DonTabIndex="11"
                      Etistyle="margin-left: 20px; text-align:center;"/>
                 </asp:TableCell>             
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left"  >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server"
                      V_PointdeVue="9" V_Objet="3" V_Information="3" V_SiDonneeDico="true"
                      EtiWidth="330px" DonWidth="328px" DonTabIndex="14"
                      Etistyle="margin-left: 20px; text-align:center;"/>
                 </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell HorizontalAlign="Left"  >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server"
                      V_PointdeVue="9" V_Objet="3" V_Information="7" V_SiDonneeDico="true"
                      EtiWidth="200px" DonWidth="198px" DonTabIndex="15"
                      Etistyle="margin-left: 20px; text-align:center;"/>
                 </asp:TableCell> 
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="15px"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauMoyens" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" > 
          <asp:TableRow> 
             <asp:TableCell HorizontalAlign="Left" >
               <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server"
                  V_PointdeVue="9" V_Objet="3" V_Information="4" V_SiDonneeDico="true"
                  EtiWidth="150px" DonWidth="386px" DonTabIndex="16" />
             </asp:TableCell>
          </asp:TableRow> 
          <asp:TableRow>
             <asp:TableCell Height="15px"></asp:TableCell>
          </asp:TableRow> 
          <asp:TableRow>
             <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server"
                       V_PointdeVue="9" V_Objet="3" V_Information="5" V_SiDonneeDico="true"
                       EtiWidth="150px" EtiText="Gestionnaire interne" DonWidth="450px" DonTabIndex="17" />
                </asp:TableCell> 
          </asp:TableRow> 
          <asp:TableRow>
             <asp:TableCell Height="15px"></asp:TableCell>
          </asp:TableRow>  
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>   
</asp:Table>