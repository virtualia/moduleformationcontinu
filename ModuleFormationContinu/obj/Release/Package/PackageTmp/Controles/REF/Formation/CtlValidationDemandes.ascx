﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlValidationDemandes.ascx.vb" Inherits="Virtualia.Net.CtlValidationDemandes" %>

<asp:Table ID="CadreValidation" runat="server" HorizontalAlign="Center" BackColor="#5E9598" BorderStyle="NotSet" BorderWidth="2px" BorderColor="#B0E0D7">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left" Width="200px">
              <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CelluleGauche" runat="server" Visible="true" HorizontalAlign="Center" ColumnSpan="2">
            <div style="margin-top: 215px; width: 300px; height: 360px; border: inset 4px #B0E0D7; background-color: White; 
                          vertical-align: top; display: table-cell; overflow: auto">
                <asp:TreeView ID="TreeListeSessions" runat="server" BorderStyle="None" NodeIndent="5" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" 
                    Font-Size="Medium" ForeColor="#121245" MaxDataBindDepth="2" Height="340px" Width="290px" ImageSet="BulletedList4" 
                    RootNodeStyle-ImageUrl="~/Images/Armoire/GrisFonceFermer16.bmp" Style="vertical-align: top; margin-top: 10px; margin-bottom: 10px;" 
                    RootNodeStyle-BorderStyle="Solid" RootNodeStyle-BorderWidth="1" RootNodeStyle-BackColor="#6C9690" RootNodeStyle-HorizontalPadding="5" LeafNodeStyle-HorizontalPadding="5" ParentNodeStyle-HorizontalPadding="5">
                    <SelectedNodeStyle BackColor="#B0E0D7" BorderColor="#6C9690" BorderStyle="Solid" BorderWidth="1" ForeColor="#124545" Font-Overline="true" />
                </asp:TreeView>
            </div>
        </asp:TableCell>
        <asp:TableCell ID="CelluleMilieu" BackColor="#8DA8A3" BorderColor="#1C5150" BorderStyle="Ridge" BorderWidth="4px" 
            Font-Names="Trebuchet MS" Font-Italic="true" Font-Size="Small" HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Table ID="CadreMilieu" runat="server" Width="310px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiTitre" runat="server" Height="66px" Width="300px" BackColor="#124545" Visible="true" BorderColor="#1C5150" BorderStyle="Inset" BorderWidth="2px" ForeColor="#B0E0D7" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Text="" Style="margin-top: 10px; font-style: oblique; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiSousTitre" runat="server" Height="22px" Width="300px" BackColor="#124545" Visible="true" BorderColor="#1C5150" BorderStyle="Inset" BorderWidth="2px" ForeColor="#B0E0D7" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Text="" Style="margin-top: 10px; font-style: oblique; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Table ID="CadreRadio" runat="server" HorizontalAlign="Center">
                            <asp:TableRow Height="25px">
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:RadioButton ID="RadioV0" runat="server" Text="Les demandes en attente de décision" Visible="true" AutoPostBack="true" GroupName="OptionV" ForeColor="#124545" Height="18px" Width="280px" BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" BorderWidth="2px" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small" Checked="true" Style="margin-top: 0px; font-style: oblique; vertical-align: middle; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:RadioButton ID="RadioV1" runat="server" Text="Les demandes accordées" AutoPostBack="true" GroupName="OptionV" ForeColor="#124545" Height="18px" Width="280px" BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; font-style: oblique; vertical-align: middle; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:RadioButton ID="RadioV2" runat="server" Text="Les demandes refusées" AutoPostBack="true" GroupName="OptionV" ForeColor="#124545" Height="18px" Width="280px" BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; font-style: oblique; vertical-align: middle; text-indent: 5px; text-align: left" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:CheckBox ID="CocheAll" runat="server" AutoPostBack="true" Height="22px" Width="22px" BackColor="Transparent" Style="margin-left:5px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="360px" Width="350px" BackColor="Snow" HorizontalAlign="Left" VerticalAlign="Top" ColumnSpan="2">
                        <asp:CheckBoxList ID="ListeValidation" runat="server" BackColor="Snow" ForeColor="#124545" 
                                Font-Names="Trebuchet MS" Font-Bold="False" Font-Size="Small" Font-Italic="false"
                                CellPadding="2" CellSpacing="2" RepeatLayout="Flow" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell  VerticalAlign="Top">
            <asp:Table ID="CadreDroite" runat="server">
                <asp:TableRow>
                    <asp:TableCell Height="140px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                            Width="70px" HorizontalAlign="Center" style="margin-top: 3px; margin-right:3px" >
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:RadioButton ID="RadioAccord" runat="server" Text="ACCORDER" AutoPostBack="true" GroupName="Valider" Checked="True" 
                            ForeColor="#124545" Height="18px" Width="120px" BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" 
                            BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"  Font-Italic="false"
                            Style="margin-top: 0px; margin-left: 1px; vertical-align: middle; text-indent: 5px; text-align: left" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:RadioButton ID="RadioRefus" runat="server" Text="REFUSER" AutoPostBack="true" GroupName="Valider" 
                            ForeColor="#124545" Height="18px" Width="120px" BackColor="#7D9F99" BorderColor="#FFEBC8" BorderStyle="inset" 
                            BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"  Font-Italic="false"
                            Style="margin-top: 0px; margin-left: 1px; vertical-align: middle; text-indent: 5px; text-align: left" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>