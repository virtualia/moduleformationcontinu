﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VReferentielFormation.ascx.vb" Inherits="Virtualia.Net.VReferentielFormation" %>

<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<asp:Table ID="SystemeReference" runat="server">
    <asp:TableRow>
        <asp:TableCell ID="MenuRef" runat="server" Visible="true">
            <asp:Table ID="Recherche" runat="server" CellPadding="1" CellSpacing="0" BorderWidth="4px" BorderStyle="Ridge" BorderColor="#B0E0D7">
                <asp:TableRow BackColor="#6C9690">
                    <asp:TableCell VerticalAlign="Top" Width="80px" HorizontalAlign="Left">
                        <asp:Label ID="EtiAllerA" runat="server" Height="18px" Width="80px" Text="Aller à" BackColor="#6C9690" Visible="true" 
                            BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" ForeColor="#FFF2DB" Font-Bold="False" 
                            Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 1px; text-indent: 5px; text-align: center" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="VAllerA" runat="server" BackColor="Snow" BorderColor="#FFEBC8" BorderStyle="Inset" Height="16px" Width="200px" 
                            Font-Bold="false" ForeColor="#142425" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:ImageButton ID="CmdAllerA" runat="server" BorderStyle="None" Height="20px" ImageAlign="Middle" 
                            ImageUrl="~/Images/Boutons/Executer_Gris.bmp" ToolTip="Accés direct à une préoccupation" Style="margin-bottom: 2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell BackColor="Snow" ColumnSpan="3">
                        <asp:Panel ID="PanelTreeDico" runat="server" Height="680px" Width="360px" ScrollBars="Auto" style="text-align:left;">
                            <asp:TreeView ID="TreeDicoRef" runat="server" BorderStyle="None" NodeIndent="15" Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" 
                                Font-Size="Medium" ForeColor="#121245" MaxDataBindDepth="2" Height="660px" Width="340px" ImageSet="BulletedList4" 
                                RootNodeStyle-ImageUrl="~/Images/Armoire/GrisFonceFermer16.bmp" Style="vertical-align:top;margin-top:10px;margin-bottom:10px;" 
                                RootNodeStyle-BorderStyle="Solid" RootNodeStyle-BorderWidth="1" RootNodeStyle-BackColor="#6C9690" RootNodeStyle-HorizontalPadding="5" LeafNodeStyle-HorizontalPadding="5" ParentNodeStyle-HorizontalPadding="5">
                            <SelectedNodeStyle BackColor="#B0E0D7" BorderColor="#6C9690" BorderStyle="Solid" BorderWidth="1" ForeColor="#124545" Font-Overline="true" />
                                <Nodes>
                                    <asp:TreeNode PopulateOnDemand="true" Text="Référentiel lié aux plans" SelectAction="Expand" Value="Plan" />
                                    <asp:TreeNode PopulateOnDemand="true" Text="Référentiel lié aux stages" SelectAction="Expand" Value="Stage" />
                                    <asp:TreeNode PopulateOnDemand="true" Text="Référentiel lié aux inscriptions" SelectAction="Expand" Value="Inscription" />
                                    <asp:TreeNode PopulateOnDemand="true" Text="Référentiel lié aux factures" SelectAction="Expand" Value="Facture" />
                                    <asp:TreeNode PopulateOnDemand="true" Text="Référentiel lié aux organismes" SelectAction="Expand" Value="Organisme" />
                                    <asp:TreeNode PopulateOnDemand="true" Text="Organismes" SelectAction="Select" Value="Entreprise" />
                                </Nodes>
                            </asp:TreeView>
                        </asp:Panel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell ID="CelluleDroite" VerticalAlign="Top">
            <Virtualia:VReference ID="RefVirtualia" runat="server" SiCommandesVisible="true" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>