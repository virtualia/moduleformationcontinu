﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VCoupleEtiDate.ascx.vb" Inherits="Virtualia.Net.VCoupleEtiDate" ClassName="VCoupleEtiDate" EnableViewState="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Saisies/VSaisieCalendrier.ascx" tagname="VCalend" tagprefix="Virtualia" %>

<asp:Table ID="VSaisieDate" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell Height="22px">
            <asp:Table ID="VStandardDate" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell ID="CellEtiquette">
                        <asp:Label ID="Etiquette" runat="server" Height="20px" Width="150px" Text="" BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                BorderWidth="2px" ForeColor="#142425" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="CadreDonnee" runat="server" BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px">
                            <asp:TableRow>
                                <asp:TableCell ID="CellCommande">
                                    <asp:ImageButton ID="CmdCalendrier" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Boutons/CalendarIcon.bmp" >
                                    </asp:ImageButton>     
                                </asp:TableCell>
                                <asp:TableCell>
                                        <asp:TextBox ID="Donnee" runat="server" Height="16px" Width="90px" MaxLength="10" BorderStyle="None"
                                            ForeColor="Black" Visible="true" AutoPostBack="true"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
          <asp:TableCell ID="CellCalPopup" Visible="false">
               <ajaxToolkit:ModalPopupExtender ID="PopupCal" runat="server" TargetControlID="HPopupCal" PopupControlID="PanelCalPopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelCalPopup" runat="server">
                     <Virtualia:VCalend ID="CalendrierMensuel" runat="server" TypeCalendrier="SD" />
                </asp:Panel>
          </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
               <asp:HiddenField ID="HPopupCal" runat="server" Value="0" />       
          </asp:TableCell>
    </asp:TableRow>      
</asp:Table> 

