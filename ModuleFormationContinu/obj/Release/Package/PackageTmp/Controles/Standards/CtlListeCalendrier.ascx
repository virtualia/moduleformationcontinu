﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlListeCalendrier.ascx.vb" Inherits="Virtualia.Net.CtlListeCalendrier" %>

<asp:Table ID="CadreCalendrier" runat="server" Width="1100px" HorizontalAlign="Center" Font-Names="Trebuchet MS" 
    Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreChoix" runat="server">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" Width="150px">
                        <asp:Label ID="EtiAnnee" runat="server" Text="Année" Height="20px" Width="100px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="150px">
                        <asp:DropDownList ID="ListeChoixAnnee" runat="server" Height="25px" Width="80px" AutoPostBack="true" BackColor="Snow" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="150px">
                        <asp:Label ID="EtiMois" runat="server" Text="Mois" Height="20px" Width="100px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="250px">
                        <asp:DropDownList ID="ListeChoixMois" runat="server" AutoPostBack="true" Height="25px" Width="150px" BackColor="Snow" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center" Width="120px">
                        <asp:Label Width="120px" ID="EtiFiltreN1" runat="server" Text="Filtre sur Plan" Height="18px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" Width="250px">
                        <asp:DropDownList ID="ListeChoixFiltreN1" runat="server" AutoPostBack="true" Height="25px" Width="250px" BackColor="Snow" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left"/>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center" Width="80px">
                        <asp:Label Width="80px" ID="EtiFiltreN2" runat="server" Text="Lieu" Height="18px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" Width="250px">
                        <asp:DropDownList ID="ListeChoixFiltreN2" runat="server" AutoPostBack="true" Height="25px" Width="250px" BackColor="Snow" ForeColor="#124545" Style="border-spacing: 2px; text-indent: 5px; text-align: left"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="3px" />
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="PanelEnteteJour" runat="server" Style="height: auto;" BorderStyle="NotSet" BorderWidth="2px" BorderColor="Transparent" BackColor="Transparent">
                <asp:Table ID="CadreEnteteJour" runat="server" Style="height: auto" HorizontalAlign="Left" Font-Size="80%">
                    <asp:TableRow>
                        <asp:TableCell Width="330px" BorderWidth="1" BorderStyle="None">
                            <asp:Label ID="EtiNeutreGauche" Width="330px" runat="server" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour01" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour01" runat="server" Width="17px" Text="1" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour02" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour02" runat="server" Width="17px" Text="2" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour03" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour03" runat="server" Width="17px" Text="3" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour04" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour04" runat="server" Width="17px" Text="4" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour05" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour05" runat="server" Width="17px" Text="5" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour06" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour06" runat="server" Width="17px" Text="6" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour07"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour07" runat="server" Width="17px" Text="7" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour08" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour08" runat="server" Width="17px" Text="8" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour09" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour09" runat="server" Width="17px" Text="9" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour10" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour10" runat="server" Width="17px" Text="10" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour11"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour11" runat="server" Width="17px" Text="11" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour12"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour12" runat="server" Width="17px" Text="12" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour13" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour13" runat="server" Width="17px" Text="13" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour14"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour14" runat="server" Width="17px" Text="14" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour15" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour15" runat="server" Width="17px" Text="15" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour16" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour16" runat="server" Width="17px" Text="16" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour17"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour17" runat="server" Width="17px" Text="17" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour18" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour18" runat="server" Width="17px" Text="18" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour19" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour19" runat="server" Width="17px" Text="19" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour20" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour20" runat="server" Width="17px" Text="20" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour21"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour21" runat="server" Width="17px" Text="21" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour22" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour22" runat="server" Width="17px" Text="22" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour23" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour23" runat="server" Width="17px" Text="23" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour24"  Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour24" runat="server" Width="17px" Text="24" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour25" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour25" runat="server" Width="17px" Text="25" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour26" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour26" runat="server" Width="17px" Text="26" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour27" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour27" runat="server" Width="17px" Text="27" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell  ID="CellJour28" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour28" runat="server" Width="17px" Text="28" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour29" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour29" runat="server" Width="17px" Text="29" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour30" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour30" runat="server" Width="17px" Text="30" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellJour31" Width="17px" BorderWidth="1" BorderColor="Black" BorderStyle="NotSet">
                            <asp:Label ID="EtiJour31" runat="server" Width="17px" Text="31" Style="text-align: center;" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiNeutreDroite" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="PanelLignes" runat="server" Style="height: auto;  min-height: 150px;" ScrollBars="Auto" BorderStyle="NotSet" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                <asp:Table ID="CadreLignes" runat="server" Style="display: table-cell; height: auto" HorizontalAlign="Left" Font-Size="80%">
                    <asp:TableRow ID="RowGabarit">
                        <asp:TableCell Width="330px" BorderWidth="1" BorderStyle="None">
                            <asp:Label ID="EtiSep01" Width="330px" runat="server" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep02" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep03" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep04" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep05" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep06" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep07" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep08" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep09" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep10" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep11" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep12" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep13" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep14" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep15" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep16" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep17" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep18" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep19" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep20" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep21" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep22" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep23" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep24" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep25" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep26" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep27" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep28" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep29" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep30" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep31" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep32" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                        <asp:TableCell Width="17px" BorderWidth="1" BorderColor="Transparent" BorderStyle="NotSet">
                            <asp:Label ID="EtiSep33" runat="server" Width="17px" Height="0px" BorderStyle="None" BorderColor="Transparent" BackColor="Transparent" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
