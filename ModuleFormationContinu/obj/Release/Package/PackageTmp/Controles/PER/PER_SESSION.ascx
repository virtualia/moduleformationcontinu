﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_SESSION" Codebehind="PER_SESSION.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VFiltreListe.ascx" tagname="VFiltreListe" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="1050px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow>
       <asp:TableCell HorizontalAlign="right">
            <asp:Table ID="CadreCommandeExcel" runat="server" Height="20px" Width="150px" BackImageUrl="~/Images/General/Cmd_Std.bmp" Visible="true" 
                BorderWidth="2px" BorderStyle="Outset" BackColor="#FF0E5E5C" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell ID="CellBoutonExcel" HorizontalAlign="Center">
                        <asp:Button ID="CmdExcel" runat="server" Text="Fichier Excel" Width="125px" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" 
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" 
                            BorderStyle="None" Style="cursor:pointer" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow> 
     <asp:TableCell HorizontalAlign="Center"> 
       <Virtualia:VFiltreListe ID="ListeGrille" runat="server" CadreWidth="1000px" SiColonneSelect="true" V_NumColonne_Filtre="3" 
            EtiTextFiltre="Suite donnée" EtiTextWidth="200px"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="1050px" Width="700px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Formation continue" Height="20px" Width="300px" BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Italic="true" Style="text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px" >
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard"
                                             V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="29" V_Information="0" DonTabIndex="1"/>
                                    </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDate ID="InfoD11" runat="server" TypeCalendrier="Standard" SiDateFin="true" SiDureeVisible="true" EtiStyle="margin-left:2px"
                                             V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="29" V_Information="11" DonTabIndex="2"/>
                                    </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="8" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="3" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="10" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="4" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>

                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="Label2" runat="server" Height="20px" Width="350px" Text="Stage" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab04" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="4" V_SiDonneeDico="true" DonTabIndex="5" DonWidth="480px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="25" V_SiDonneeDico="true" 
                                        DonWidth="200px" DonTabIndex="6" EtiText="Référence du stage" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab29" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="29" V_SiDonneeDico="true" DonTabIndex="6" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab02" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="2" V_SiDonneeDico="true" DonTabIndex="7" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab03" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="3" V_SiDonneeDico="true" DonTabIndex="8" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab35" runat="server" EtiText="Sous-thème" V_PointdeVue="1" V_Objet="29" V_Information="35" V_SiDonneeDico="true" DonTabIndex="9" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab01" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="1" V_SiDonneeDico="true" DonTabIndex="10" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab09" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="9" V_SiDonneeDico="true" DonTabIndex="11" DonWidth="400px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiSuivi" runat="server" Height="20px" Width="350px" Text="Déroulement" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche33" runat="server" 
                                         V_PointdeVue="1" V_Objet="29" V_Information="33" V_SiDonneeDico="true" DonTabIndex="12" 
                                         V_Width="268px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCocheSimple ID="Coche34" runat="server"
                                        V_PointdeVue="1" V_Objet="29" V_Information="34" V_SiDonneeDico="true" DonTabIndex="13" 
                                        V_Width="268px" /> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="5" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="100px" DonTabIndex="14" EtiStyle="margin-left:2px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab23" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="23" V_SiDonneeDico="true" DonTabIndex="15" DonWidth="150px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab06" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="6" V_SiDonneeDico="true" DonTabIndex="16" DonWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab18" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="18" V_SiDonneeDico="true" DonTabIndex="17" DonWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DonTab07" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="7" V_SiDonneeDico="true" DonTabIndex="18" DonWidth="350px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiCouts" runat="server" Height="20px" Width="350px" Text="Coûts et frais" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="12" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="19" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="14" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="20" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH30" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="30" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="21" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="16" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="22" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="15" V_SiDonneeDico="true" DonWidth="120px" DonTabIndex="23" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" EtiText="Frais de déplacement" V_PointdeVue="1" V_Objet="29" V_Information="13" V_SiDonneeDico="true" DonWidth="80px" DonTabIndex="24" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiComplements" runat="server" Height="20px" Width="350px" Text="Compléments d'information" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="tb2" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab19" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="19" V_SiDonneeDico="true" DonTabIndex="25" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab20" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="20" V_SiDonneeDico="true" DonTabIndex="26" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" 
                                                V_PointdeVue="1" V_Objet="29" V_Information="22" V_SiDonneeDico="true" DonWidth="70px" DonTabIndex="27" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab24" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="24" V_SiDonneeDico="true" DonTabIndex="28" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab17" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="17" 
                                                    V_SiDonneeDico="true" DonTabIndex="26" DonWidth="200px" EtiText="Référence de la session" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab26" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="26" V_SiDonneeDico="true" DonTabIndex="29" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab27" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="27" V_SiDonneeDico="true" DonTabIndex="30" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab28" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="28" V_SiDonneeDico="true" DonTabIndex="31" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab31" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="31" V_SiDonneeDico="true" DonTabIndex="32" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DonTab32" runat="server" V_PointdeVue="1" V_Objet="29" V_Information="32" V_SiDonneeDico="true" DonTabIndex="33" DonWidth="400px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
