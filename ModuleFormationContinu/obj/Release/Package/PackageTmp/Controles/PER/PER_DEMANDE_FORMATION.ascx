﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DEMANDE_FORMATION" Codebehind="PER_DEMANDE_FORMATION.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="620px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiCaseAcocher="false" />
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="600px" Width="700px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0" Visible="false"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Demandes de formation" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="300px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="0" V_SiDonneeDico="true"
                           DonWidth="100px" DonTabIndex="1"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="300px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="6" V_SiDonneeDico="true"
                           EtiWidth="80px" DonWidth="100px" DonTabIndex="2" EtiStyle="margin-left:2px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="1" V_SiDonneeDico="true"
                           DonTabIndex="3" DonWidth="490px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" Width="300px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="2" V_SiDonneeDico="true"
                           EtiWidth="220px" DonWidth="100px" DonTabIndex="4"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="3" V_SiDonneeDico="true"
                           DonTabIndex="5" EtiWidth="120px" DonWidth="150px"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="4" V_SiDonneeDico="true"
                           DonTabIndex="6" EtiWidth="80px" DonWidth="215px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="5" V_SiDonneeDico="true"
                           DonTabIndex="7" EtiWidth="250px" DonWidth="300px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="9" V_SiDonneeDico="true"
                           DonTabIndex="8" EtiWidth="200px" DonWidth="350px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV07" runat="server" DonTextMode="true" V_PointdeVue="1" V_Objet="81" V_Information="7" V_SiDonneeDico="true"
                           EtiWidth="652px" DonWidth="650px" DonHeight="100px" DonTabIndex="9" /> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="8px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCocheSimple ID="Coche08" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="8" V_SiDonneeDico="true"
                           V_Width="250px" V_Style="margin-left:10px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="Coche10" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="10" V_SiDonneeDico="true"
                           V_Width="250px" V_Style="margin-left:10px"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="Coche11" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="11" V_SiDonneeDico="true"
                           V_Width="250px" V_Style="margin-left:10px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="40px">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="EtiIntranet" runat="server" Height="20px" Width="350px"
                           Text="Intranet - Processus de validation"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="Intranet" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiValideur" runat="server" Height="20px" Width="328px"
                            Text="Intranet - Valideur" 
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-left:3px">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiDate" runat="server" Height="20px" Width="121px"
                            Text="Date"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiDecision" runat="server" Height="20px" Width="151px"
                            Text="Décision"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="12" V_SiDonneeDico="true"
                            EtiWidth="120px" DonWidth="200px" DonTabIndex="10"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="13" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="120px" DonTabIndex="11"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="14" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="150px" DonTabIndex="12"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="15" V_SiDonneeDico="true"
                            EtiWidth="120px" DonWidth="200px" DonTabIndex="13"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="16" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="120px" DonTabIndex="14"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="17" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="150px" DonTabIndex="15"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="18" V_SiDonneeDico="true"
                            EtiWidth="120px" DonWidth="200px" DonTabIndex="16"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="19" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="120px" DonTabIndex="17"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" V_PointdeVue="1" V_Objet="81" V_Information="20" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="150px" DonTabIndex="18"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" Columnspan="3"></asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>       
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>