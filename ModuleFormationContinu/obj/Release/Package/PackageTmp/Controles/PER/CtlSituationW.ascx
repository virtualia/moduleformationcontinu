﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlSituationW.ascx.vb" Inherits="Virtualia.Net.CtlSituationW" %>

<%@ Register Src="~/Controles/PER/CtlPvuePER.ascx" TagName="MenuPER" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/PER/PER_SESSION.ascx" TagName="PER_SESSION" TagPrefix="Virtualia" %>
<%@ Register Src="~/ControlesFormations/Personnel/Bilan/CtrlBilanPerso.ascx" TagName="CTL_STATINDIV" TagPrefix="Formation" %>
<%@ Register Src="~/Controles/PER/PER_DIF.ascx" TagName="PER_DIF" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>

<asp:Table ID="CadreGeneral" runat="server" BorderStyle="None" BorderWidth="2px" BackColor="Transparent"
    BorderColor="#B0E0D7" Width="1140px" HorizontalAlign="Center" style="margin-top:1px;">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" 
                BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" 
                ImageAlign="Middle" style="margin-left: 1px;">
            </asp:ImageButton>
        </asp:TableCell>
        <asp:TableCell Height="30px">
            <asp:Label ID="EtiIdentite" runat="server" Text="" Height="25px" Width="800px"
                        BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="White"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
              </asp:Label>     
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Height="35px">
            <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="1100px" HorizontalAlign="Left"
               CellPadding="0" CellSpacing="0" BackColor="Transparent" style="margin-top: 2px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Menu ID="MenuChoix" runat="server" Width="1000px"
                            DynamicHorizontalOffset="10" Font-Names="Trebuchet MS" Font-Size="Small"
                            BackColor="Transparent" Font-Italic="false" ForeColor="#124545" Height="32px" Orientation="Horizontal" 
                            StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="2px" style="margin-left: 1px;" 
                            StaticEnableDefaultPopOutImage="False" BorderColor="#124545" BorderStyle="None">
                            <StaticSelectedStyle BackColor="#124545" BorderStyle="Solid" Font-Bold="True" 
                                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#B0E0D7" />
                            <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="24px"
                                BackColor="#B0E0D7" BorderColor="#124545" BorderStyle="Solid" Font-Bold="False" 
                                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" />
                            <StaticHoverStyle BackColor="#124545" ForeColor="White" BorderColor="#124545" 
                                BorderStyle="Solid" />
                            <Items>
                                <asp:MenuItem Text="Parcours de formation" Value="FORMATION" ToolTip="Visualisation des formations continues"/>
                                <asp:MenuItem Text="Bilan individuel" Value="BILAN" ToolTip = "Bilan individuel de formation"/>
                                <asp:MenuItem Text="D I F" Value="DIF" ToolTip = "Droit individuel à la formation"/>
                                <asp:MenuItem Text="Situation de gestion" Value="GESTION" ToolTip = "Situation dans la base de gestion Virtualia"/>
                            </Items>
                        </asp:Menu>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2">
            <asp:Table ID="CadreVues" runat="server" CellPadding="0" CellSpacing="0" Width="1100px" HorizontalAlign="center" BorderColor="#124545" BorderStyle="None" BorderWidth="2px">
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>      
              </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                            <asp:View ID="VuePerSession" runat="server">
                                <Virtualia:PER_SESSION ID="PER_SESSION_29" V_SiEnLectureSeule="true" runat="server" />
                            </asp:View>
                            <asp:View ID="VueStatIndiv" runat="server">
                                <Formation:CTL_STATINDIV ID="CtrlStatIndiv" runat="server" />
                            </asp:View>
                            <asp:View ID="VuePerDif" runat="server">
                                <Virtualia:PER_DIF ID="PER_DIF_93" runat="server" />
                            </asp:View>
                            <asp:View ID="VueGestion" runat="server">
                                <Virtualia:MenuPER ID="CtrlVuesPER" runat="server" />
                            </asp:View>
                            <asp:View ID="VueMessage" runat="server">
                                <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                        </asp:View>
                         </asp:MultiView>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>