﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPvuePER.ascx.vb" Inherits="Virtualia.Net.CtlPvuePER" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VMenuCommun.ascx" tagname="VMenu" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VPlanningAnnuel.ascx" tagname="VPlanning" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_DIPLOME.ascx" tagname="PER_DIPLOME" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_BREVETPRO.ascx" tagname="PER_BREVETPRO" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_SPECIALITE.ascx" tagname="PER_SPECIALITE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_ABSENCE.ascx" tagname="PER_ABSENCE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_COLLECTIVITE.ascx" tagname="PER_COLLECTIVITE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_AFFECTATION.ascx" tagname="PER_AFFECTATION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_STATUT.ascx" tagname="PER_STATUT" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_POSITION.ascx" tagname="PER_POSITION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_GRADE.ascx" tagname="PER_GRADE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_ADRESSEPRO.ascx" tagname="PER_ADRESSEPRO" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/PER/PER_DEMANDE_FORMATION.ascx" tagname="PER_DEMANDE_FORMATION" tagprefix="Virtualia" %>

<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1140px">
        <asp:TableRow>
        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="SITUATION EN GESTION" Height="22px" Width="700px"
                        BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                        style="font-style: normal; text-indent: 5px; text-align: center;">
                </asp:Label>     
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <Virtualia:VMenu ID="PER_MENU" runat="server" V_Appelant="PER"></Virtualia:VMenu>
        </asp:TableCell>
        <asp:TableCell ID="CellulePER" runat="server" Width="800px" VerticalAlign="Top">
            <asp:MultiView ID="MultiPER" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueDemande" runat="server">
                    <Virtualia:PER_DEMANDE_FORMATION ID="PER_DEMANDE_81" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_DEMANDE_FORMATION>
                </asp:View>
                <asp:View ID="VueDiplome" runat="server">
                    <Virtualia:PER_DIPLOME ID="PER_DIPLOME_6" runat="server"></Virtualia:PER_DIPLOME>
                </asp:View>
                <asp:View ID="VueQualification" runat="server">
                    <Virtualia:PER_BREVETPRO ID="PER_BREVETPRO_34" runat="server"></Virtualia:PER_BREVETPRO>
                </asp:View>
                <asp:View ID="VueSpecialite" runat="server">
                    <Virtualia:PER_SPECIALITE ID="PER_SPECIALITE_53" runat="server"></Virtualia:PER_SPECIALITE>
                </asp:View>
                <asp:View ID="VueAffectation" runat="server">
                    <Virtualia:PER_AFFECTATION ID="PER_AFFECTATION_17" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_AFFECTATION>
                </asp:View>
                <asp:View ID="VuePlanning" runat="server">
                    <Virtualia:VPlanning ID="PER_PLANNING" runat="server"></Virtualia:VPlanning>
                </asp:View>
                <asp:View ID="VueAbsence" runat="server">
                    <Virtualia:PER_ABSENCE ID="PER_ABSENCE_15" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_ABSENCE>
                </asp:View>
                <asp:View ID="VueStatut" runat="server">
                    <Virtualia:PER_STATUT ID="PER_STATUT_12" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_STATUT>
                </asp:View>
                <asp:View ID="VuePosition" runat="server">
                    <Virtualia:PER_POSITION ID="PER_POSITION_13" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_POSITION>
                </asp:View>
                <asp:View ID="VueGrade" runat="server">
                    <Virtualia:PER_GRADE ID="PER_GRADE_14" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_GRADE>
                </asp:View>
                <asp:View ID="VueAdrPro" runat="server">
                    <Virtualia:PER_ADRESSEPRO ID="PER_ADRESSEPRO_24" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_ADRESSEPRO>
                </asp:View>
                <asp:View ID="VueEtablissement" runat="server">
                    <Virtualia:PER_COLLECTIVITE ID="PER_COLLECTIVITE_26" runat="server" V_SiEnLectureSeule="true"></Virtualia:PER_COLLECTIVITE>
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellMessage" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Virtualia:VMessage id="MsgVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell>
              <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
          </asp:TableCell>
      </asp:TableRow>

</asp:Table>
    