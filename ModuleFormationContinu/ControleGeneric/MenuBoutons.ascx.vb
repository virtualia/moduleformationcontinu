﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.ObjetBaseStructure.Formation

Public Class MenuBoutons
    Inherits UserControl
    Implements IMenuBouton

    Private WsAct_ClickMenu As Action(Of CacheClickMenu)

    Public Overloads WriteOnly Property Width As Unit
        Set(value As Unit)
            CadreMenus.Width = value
        End Set
    End Property

    Public Property MarqueSelection As Boolean
        Get
            If Not (ViewState.KeyExiste("MarqueSelection")) Then
                ViewState.AjouteValeur("MarqueSelection", True)
            End If
            Return DirectCast(ViewState("MarqueSelection"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("MarqueSelection", value)
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IMenuBouton.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Public WriteOnly Property HorizontalAlignement As HorizontalAlign
        Set(value As HorizontalAlign)
            CadreMenus.HorizontalAlign = value
        End Set
    End Property

    Public Overloads Property Visible As Boolean
        Get
            If Not (ViewState.KeyExiste("ESTVisible")) Then
                ViewState.AjouteValeur("ESTVisible", True)
            End If
            Return DirectCast(ViewState("ESTVisible"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("ESTVisible", value)
        End Set
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        CadreMenus.Visible = Visible
    End Sub

    Public WriteOnly Property Act_ClickMenu As Action(Of CacheClickMenu) Implements IMenuBouton.Act_ClickMenu
        Set(value As Action(Of CacheClickMenu))
            WsAct_ClickMenu = value
        End Set
    End Property

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        Dim lst As IList(Of MenuItem) = TryCast(items, IList(Of MenuItem))
        If (lst Is Nothing) Then
            Return
        End If
        MenuChoix.Items.Clear()
        For Each mnu As MenuItem In lst
            MenuChoix.Items.Add(mnu)
        Next
    End Sub

    Public Sub ReInit(ByVal SiReinit As Boolean) Implements IMenuBouton.ReInit
        If SiReinit = False Then
            Exit Sub
        End If
        If MenuChoix.Items.Count <= 0 Then
            Exit Sub
        End If
        For Each mnu As MenuItem In MenuChoix.Items
            mnu.Selected = False
        Next
        MenuChoix.Items(0).Selected = True
    End Sub

    Public Sub EndInit() Implements IMenuBouton.EndInit
        If ((Not IsPostBack) AndAlso MarqueSelection) Then
            ReInit(True)
        End If
        If WsAct_ClickMenu Is Nothing Then
            Exit Sub
        End If
        AddHandler MenuChoix.MenuItemClick, AddressOf Me.ClickMenu
    End Sub

    Private Sub ClickMenu(sender As Object, ev As MenuEventArgs)
        Dim cache As CacheClickMenu = New CacheClickMenu()
        cache.TypeSource = TypeSourceMenu.V_Menu

        If (ev.Item.Value = "RET") Then
            cache.TypeClick = TypeClickMenu.RETOUR
            WsAct_ClickMenu(cache)
            ev.Item.Selected = MarqueSelection
            Return
        End If

        cache.TypeClick = TypeClickMenu.VUE
        cache.Value = ev.Item.Value
        WsAct_ClickMenu(cache)
        ev.Item.Selected = MarqueSelection
    End Sub

    Public Sub SelectionneBouton(ByVal ValBouton As String)
        For Each mn As MenuItem In MenuChoix.Items
            If mn.Value = ValBouton Then
                mn.Selected = True
            Else
                mn.Selected = False
            End If
        Next
    End Sub

    Public Sub SupprimerMenu(ByVal ValBouton As String)
        For Each mn As MenuItem In MenuChoix.Items
            If mn.Value = ValBouton Then
                MenuChoix.Items.Remove(mn)
                Exit Sub
            End If
        Next
    End Sub

    Public ReadOnly Property NombreMenus As Integer
        Get
            Return MenuChoix.Items.Count
        End Get
    End Property
End Class