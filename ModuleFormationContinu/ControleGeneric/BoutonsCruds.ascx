﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BoutonsCruds.ascx.vb" Inherits="Virtualia.Net.BoutonsCruds" %>

<%@ Register Src="~/ControleGeneric/BoutonSupprimer.ascx" TagName="BTN_SUPPR" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonNouveau.ascx" TagName="BTN_NOUV" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonValider.ascx" TagName="BTN_VALID" TagPrefix="Generic" %>

<asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
        <asp:Table ID="CadreCommandes" runat="server">
            <asp:TableRow>
                <asp:TableCell ID="CellNouveau1">
                    <Generic:BTN_NOUV ID="BtnNouveau1" runat="server" />
                </asp:TableCell>
                <asp:TableCell ID="CellSupprimer1">
                    <Generic:BTN_SUPPR ID="BtnSupprimer1" runat="server" />
                </asp:TableCell>
                <asp:TableCell ID="CellValider1">
                    <Generic:BTN_VALID ID="BtnValider1" runat="server" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="BtnNouveau1" />
        <asp:PostBackTrigger ControlID="BtnSupprimer1" />
        <asp:PostBackTrigger ControlID="BtnValider1" />
    </Triggers>
</asp:UpdatePanel>
