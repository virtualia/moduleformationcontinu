﻿Imports Virtualia.ObjetBaseStructure.Formation
Public Class BoutonGeneral
    Inherits BoutonBase

    Public Enum Taille
        Petit
        Normal
        Grand
        TresGrand
    End Enum

    Protected Overrides ReadOnly Property Btn As Button
        Get
            Return Btn1
        End Get
    End Property

    Protected Overrides ReadOnly Property CellBtn As TableCell
        Get
            Return CellBtn1
        End Get
    End Property
    
    Public WriteOnly Property Width As Taille
        Set(value As Taille)
            GereTaille(value)
        End Set
    End Property

    Public Property Text As String
        Set(value As String)
            Btn.Text = value
        End Set
        Get
            Return Btn.Text
        End Get
    End Property

    Private Sub GereTaille(value As Taille)
        Dim simg As String = "~/Images/General/Cmd_Std"
        Dim w As Unit = Unit.Pixel(150)
        Dim wb As Unit = Unit.Pixel(125)

        Select Case value
            Case Taille.Petit
                simg = simg & "_Petit"
                w = Unit.Pixel(90)
                wb = Unit.Pixel(80)
            Case Taille.Grand
                simg = simg & "_long"
                w = Unit.Pixel(200)
                wb = Unit.Pixel(180)
            Case Taille.TresGrand
                simg = simg & "_long_long"
                w = Unit.Pixel(400)
                wb = Unit.Pixel(380)
        End Select

        TbGene.Width = w
        TbGene.BackImageUrl = simg & ".bmp"
        Btn.Width = wb
    End Sub

End Class