﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_ChoixSurListe.ascx.vb" Inherits="Virtualia.Net.V_ChoixSurListe" %>

<%@ Register Src="~/ControleGeneric/ListeSelection.ascx" TagName="CTL_Armoire" TagPrefix="Generic" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:Table ID="CadreVues" runat="server" Height="600px" Width="700px" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table ID="CadreTitre" runat="server" Width="600px" Height="40px" HorizontalAlign="Center">
                <asp:TableRow VerticalAlign="Middle">
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Label ID="LblTitre" runat="server" Text="XXXXX" Height="22px" Width="500px" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table ID="Table1" runat="server" Width="700px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Table ID="TBArmoire" runat="server">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4">
                                    <Generic:CTL_Armoire ID="CtrlArmoire" runat="server" Titre="Sélectionner une valeur" Width="650px" Height="400px" SiSelection="false" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="120px" />
                                <asp:TableCell Width="180px" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Generic:BTN_GENE ID="BtnMettreABlanc" runat="server" Text="Mettre à blanc"  />
                                </asp:TableCell>
                                <asp:TableCell Width="180px" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Generic:BTN_GENE ID="BtnRetour" runat="server" Text="Retour" />
                                </asp:TableCell>
                                <asp:TableCell Width="120px" />
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
