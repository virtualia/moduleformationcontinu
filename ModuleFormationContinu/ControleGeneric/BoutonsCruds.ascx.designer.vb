﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class BoutonsCruds

    '''<summary>
    '''Contrôle upd.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents upd As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Contrôle CadreCommandes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCommandes As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CellNouveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellNouveau1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle BtnNouveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnNouveau1 As Global.Virtualia.Net.BoutonNouveau

    '''<summary>
    '''Contrôle CellSupprimer1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSupprimer1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle BtnSupprimer1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnSupprimer1 As Global.Virtualia.Net.BoutonSupprimer

    '''<summary>
    '''Contrôle CellValider1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellValider1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle BtnValider1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnValider1 As Global.Virtualia.Net.BoutonValider
End Class
