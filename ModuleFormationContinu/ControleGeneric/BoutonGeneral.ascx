﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BoutonGeneral.ascx.vb" Inherits="Virtualia.Net.BoutonGeneral" %>

<asp:Table ID="TbGene" runat="server" Height="20px" Width="150px" BackImageUrl="~/Images/General/Cmd_Std.bmp" 
    BorderWidth="2px" BorderStyle="Outset" BackColor="#FF0E5E5C" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell ID="CellBtn1" HorizontalAlign="Center">
            <asp:Button ID="Btn1" runat="server" Text="Bouton" Width="125px" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" 
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="cursor:pointer" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
