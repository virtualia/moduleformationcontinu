﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBaseStructure.Formation

Public Class MenuArbre
    Inherits UserControl
    Implements IMenuArbre

#Region "Proprietes"
    Private WsCmdRetour As ImageButton
    Private WsAct_ClickMenu As Action(Of CacheClickMenu)

    Public WriteOnly Property CmdRetour As ImageButton Implements IMenuArbre.CmdRetour
        Set(value As ImageButton)
            WsCmdRetour = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IMenuArbre.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Public WriteOnly Property Act_ClickMenu As Action(Of CacheClickMenu) Implements IMenuArbre.Act_ClickMenu
        Set(value As Action(Of CacheClickMenu))
            WsAct_ClickMenu = value
        End Set
    End Property

    Public ReadOnly Property StyleNoeuds As ListStyleTreeNodeCollection
        Get
            If Not (Me.ViewState.KeyExiste("STYLENOEUD")) Then
                Me.ViewState.AjouteValeur("STYLENOEUD", New ListStyleTreeNodeCollection())
            End If
            Return Me.ViewState.GetValeur(Of ListStyleTreeNodeCollection)("STYLENOEUD")
        End Get
    End Property

    Public WriteOnly Property Width As Unit
        Set(value As Unit)
            PanelTree.Width = value
        End Set
    End Property

    Public WriteOnly Property Height As Unit
        Set(value As Unit)
            PanelTree.Height = value
        End Set
    End Property

    Public Property ExpandRacine As Boolean
        Private Get
            If Not (ViewState.KeyExiste("ExpandRacine")) Then
                ViewState.AjouteValeur("ExpandRacine", True)
            End If
            Return DirectCast(ViewState("ExpandRacine"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("ExpandRacine", value)
        End Set
    End Property

    Public Sub Charge(source As Object) Implements IMenuArbre.Charge
        If StyleNoeuds.Count <= 0 Then
            Dim st As StyleTreeNode = New StyleTreeNode()
            st.Expanded = ExpandRacine
            st.PopulateOnDemand = False
            st.SelectAction = TreeNodeSelectAction.SelectExpand
            st.ShowCheckBox = False
            st.UrlImage = "~/Images/Armoire/BleuFermer16.bmp"
            StyleNoeuds.Add(st)

            st = New StyleTreeNode()
            st.PopulateOnDemand = False
            st.SelectAction = TreeNodeSelectAction.SelectExpand
            st.ShowCheckBox = False
            st.UrlImage = "~/Images/Armoire/FicheBleue.bmp"
            StyleNoeuds.Add(st)
        End If
        Dim SiPremier As Boolean = True
        For Each st As StyleTreeNode In StyleNoeuds
            If SiPremier = True Then
                st.Propriete_Liee = "Groupe"
                SiPremier = False
            Else
                st.Propriete_Liee = "LibelleMenu"
            End If
        Next
        Dim Elements As IEnumerable(Of ItemDicoVueBase) = TryCast(source, IEnumerable(Of ItemDicoVueBase))
        If Elements Is Nothing Then
            Exit Sub
        End If

        Dim generateur As GenerationVisuArbre(Of ItemDicoVueBase) = New GenerationVisuArbre(Of ItemDicoVueBase)()

        generateur.StyleNoeuds.AddRange(StyleNoeuds)

        Dim listprops As List(Of String) = New List(Of String)()
        listprops.Add("Groupe")
        listprops.Add("LibelleMenu")

        Dim lst As List(Of ItemDicoVueBase) = New List(Of ItemDicoVueBase)(Elements)

        TreeListeMenu.Nodes.AddRange(generateur.GetArbre(lst, listprops))
    End Sub

    Public Sub EndInit() Implements IMenuArbre.EndInit
        If WsAct_ClickMenu Is Nothing Then
            Return
        End If

        'On affecte l'evenement click
        AddHandler TreeListeMenu.SelectedNodeChanged, AddressOf TreeListe_SelectedNodeChanged

        'On affecte le click du bouton retour
        If WsCmdRetour IsNot Nothing Then
            AddHandler WsCmdRetour.Click, AddressOf Me.ClickRetour
        End If
        ReInit(True)

    End Sub

    Public Sub ReInit(ByVal SiReinit As Boolean) Implements IMenuArbre.ReInit
        If SiReinit = False Then
            Exit Sub
        End If
        Dim SiPremier As Boolean = True
        Dim SiPremierMenu As TreeNode = Nothing

        For Each Noeud As TreeNode In TreeListeMenu.Nodes
            Noeud.Selected = False
            If Noeud.ChildNodes.Count > 0 Then
                If ExpandRacine = True Then
                    Noeud.Expand()
                Else
                    Noeud.Collapse()
                End If
                If Not (Noeud.Expanded) And SiPremier Then
                    Noeud.Expand()
                End If
                For Each nf As TreeNode In Noeud.ChildNodes
                    If SiPremier = True Then
                        SiPremierMenu = nf
                        SiPremier = False
                    End If
                    nf.Selected = False
                Next
            End If
        Next
        If SiPremierMenu Is Nothing Then
            Return
        End If
        SiPremierMenu.Selected = True
        TreeListe_SelectedNodeChanged(TreeListeMenu, Nothing)
    End Sub

    Private Sub TreeListe_SelectedNodeChanged(sender As Object, e As EventArgs)
        Dim treemenu As TreeView = DirectCast(sender, TreeView)
        If (treemenu.SelectedNode Is Nothing) Then
            Exit Sub
        End If

        Dim tn As TreeNode = treemenu.SelectedNode

        If (tn.Parent Is Nothing) Then
            If Not (ExpandRacine) Then
                DirectCast(sender, TreeView).CollapseAll()
            End If
            'On est sur une racine
            If Not (tn.Expanded) Then
                tn.Expand()
            End If
            Exit Sub
        End If

        If WsAct_ClickMenu Is Nothing Then
            Exit Sub
        End If

        Dim cacheclick As CacheClickMenu = New CacheClickMenu()
        cacheclick.TypeClick = TypeClickMenu.VUE
        cacheclick.TypeSource = TypeSourceMenu.V_MenuArbre
        cacheclick.Value = tn.Parent.Text & VI.SigneBarre & tn.Text

        WsAct_ClickMenu(cacheclick)

    End Sub

    Private Sub ClickRetour(sender As Object, e As EventArgs)
        If WsAct_ClickMenu Is Nothing Then
            Return
        End If
        Dim cacheclick As CacheClickMenu = New CacheClickMenu()
        cacheclick.TypeClick = TypeClickMenu.RETOUR
        WsAct_ClickMenu(cacheclick)
    End Sub
#End Region

End Class