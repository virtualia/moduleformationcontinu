﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation

Public Class V_ChoixSurListe
    Inherits ControlCslBase

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        BtnMettreABlanc.Act = Sub(btn)
                                  Act_MiseABlanc()()
                              End Sub

        BtnRetour.Act = Sub(btn)
                            Act_Retour()()
                        End Sub
    End Sub

    Protected Overrides ReadOnly Property CtlArmoire As IListeSelection
        Get
            Return CtrlArmoire
        End Get
    End Property

    Public WriteOnly Property LettreDefaut As ValeurDefaut
        Set(value As ValeurDefaut)
            CtrlArmoire.LettreDefaut = value
        End Set
    End Property

    Protected Overrides WriteOnly Property Titre As String
        Set(value As String)
            LblTitre.Text = value
        End Set
    End Property

    Public Sub ReInit()
        CtrlArmoire.Reinit()
    End Sub
End Class