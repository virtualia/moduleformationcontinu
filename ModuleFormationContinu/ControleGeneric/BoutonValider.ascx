﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BoutonValider.ascx.vb" Inherits="Virtualia.Net.BoutonValider" %>

<asp:Table ID="TbValider" runat="server" BackColor="#FF7D9F99" Width="90px" BackImageUrl="~/Images/Boutons/OKFiche_Std.bmp" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" CellPadding="0" CellSpacing="0">
	<asp:TableRow>
		<asp:TableCell ID="CellBtn1" HorizontalAlign="Center">
			<asp:Button ID="Btn1" runat="server" Text="Valider" Width="100%" Height="20px" BackColor="Transparent" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" style="cursor:pointer; margin-left:5px"/>
		</asp:TableCell>
	</asp:TableRow>
</asp:Table>
