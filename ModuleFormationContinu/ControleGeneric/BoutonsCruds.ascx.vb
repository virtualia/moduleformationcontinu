﻿Imports Virtualia.ObjetBaseStructure.Formation

Public Class BoutonsCruds
    Inherits BoutonCrudBase

    Protected Overrides ReadOnly Property BtnNouveau As BoutonBase
        Get
            Return BtnNouveau1
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSupprimer As BoutonBase
        Get
            Return BtnSupprimer1
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnValider As BoutonBase
        Get
            Return BtnValider1
        End Get
    End Property

    Protected Overrides ReadOnly Property CellNouveau As TableCell
        Get
            Return CellNouveau1
        End Get
    End Property

    Protected Overrides ReadOnly Property CellSupprimer As TableCell
        Get
            Return CellSupprimer1
        End Get
    End Property

    Protected Overrides ReadOnly Property CellValider As TableCell
        Get
            Return CellValider1
        End Get
    End Property

End Class