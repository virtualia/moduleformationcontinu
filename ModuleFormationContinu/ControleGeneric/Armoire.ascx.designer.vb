﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Armoire

    '''<summary>
    '''Contrôle pnl1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents pnl1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle CadreArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreArmoire As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiInfo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CelluleSelection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CelluleSelection As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Table1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Table1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle cboselection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents cboselection As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Contrôle CelluleLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CelluleLettre As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle CadreLettres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLettres As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLettre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle RowLettres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RowLettres As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''Contrôle CellZero.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellZero As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Button0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Button0 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonA As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonB As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonC As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonD As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonE As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonF As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonG.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonG As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonH.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonH As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonI As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonJ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonJ As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonK As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonL.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonL As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonM As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonN.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonN As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonO.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonO As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonP As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonQ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonQ As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonR.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonR As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonS As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonT As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonU As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonV.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonV As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonW.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonW As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonX.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonX As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonY.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonY As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonZ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonZ As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle ButtonArobase.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonArobase As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Contrôle CelluleFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CelluleFiltre As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle CadreFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFiltre As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle lblRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents lblRecherche As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle txtRecherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents txtRecherche As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Contrôle PanelTree.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelTree As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle LstArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LstArmoire As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''Contrôle CadreStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStatut As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle lblNbStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents lblNbStatut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle HButtonLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HButtonLettre As Global.System.Web.UI.WebControls.HiddenField
End Class
