﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ArbreSelection.ascx.vb" Inherits="Virtualia.Net.ArbreSelection" %>


<asp:Panel ID="PanelGlobal" runat="server" HorizontalAlign="Center" Width="1000px" Style="min-width: 500px;">
    <asp:Table ID="CadreArmoire" runat="server" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" Style="width: 100%; height: 100%">

        <asp:TableRow>
            <asp:TableCell ID="CellTitre" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#6D9092" Height="40px" Width="100%">
                <%--ici--%>
                <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Width="80%">
                    <asp:TableRow ID="RCbo">
                        <asp:TableCell ID="CellEtiPan" VerticalAlign="Middle" HorizontalAlign="Right" Height="30px">
                            <asp:Label ID="EtiPlan" runat="server" Text="Filtre sur Plan" Width="150px" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Font-Italic="true" Style="margin-left: 10px" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellDonPan" VerticalAlign="Middle" HorizontalAlign="Left" Height="30px">
                            <asp:DropDownList ID="CboFiltrePlan" runat="server" Height="22px" Width="250px" AutoPostBack="True" BackColor="#A8BBB8" ForeColor="#124545" Style="text-align: left; margin-left: 10px" OnSelectedIndexChanged="CboFiltrePlan_SelectedIndexChanged" />
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" Height="30px"  Width="120px">
                            <asp:Label ID="EtiTypeArmoire" runat="server" Text="Affichage" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Font-Italic="true" Style="margin-left: 10px" />
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" Height="30px" Width="630px">
                            <asp:DropDownList ID="CboTypeArmoire" runat="server" Height="22px" Width="610px" AutoPostBack="True" BackColor="#A8BBB8" ForeColor="#124545" Style="text-align: left;margin-left:10px;" OnSelectedIndexChanged="CboTypeArmoire_SelectedIndexChanged" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="RTitre">
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="center" Height="30px" Width="100%" ColumnSpan="2">
                            <asp:Label ID="lblTitre" runat="server" Height="22px" Width="300px" Text="" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Font-Italic="true" Style="text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell ID="CelluleLettre" runat="server" Visible="true" HorizontalAlign="Center" VerticalAlign="Top" Width="100%">
                <%--ici--%>
                <asp:Table ID="CadreLettre" runat="server" Height="25px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Width="100%">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Table ID="tb1" runat="server">
                                <asp:TableRow ID="RowLettres">
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonA" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/A.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonB" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonC" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonD" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonE" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonF" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonG" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonH" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonI" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonJ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonK" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonL" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonM" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonN" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonO" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonP" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonQ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonR" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonS" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonT" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonU" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonV" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonW" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonX" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonY" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonZ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:ImageButton Width="20px" ID="ButtonArobase" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/arobase.bmp" OnClick="ButtonLettre_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>

                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow>
                        <asp:TableCell Width="100%" HorizontalAlign="Center">
                            <asp:Table ID="tb2" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                                        <asp:Label ID="lblRecherche" runat="server" Text="Contient" Height="16px" Width="200px" BackColor="Transparent" ForeColor="#D7FAF3" BorderStyle="None" Style="font-style: oblique; text-indent: 5px; text-align: right;" />
                                    </asp:TableCell>
                                    <asp:TableCell Width="10px" />
                                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="Middle">
                                        <asp:TextBox ID="txtRecherche" runat="server" Text="" Visible="true" AutoPostBack="true" BackColor="White" BorderColor="#B0E0D7" BorderStyle="InSet" BorderWidth="2px" ForeColor="#124545" Height="16px" Width="200px" MaxLength="35" Style="font-style: normal; text-indent: 1px; text-align: left" OnTextChanged="txtRecherche_TextChanged" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Height="100%" Width="100%" BackColor="Snow">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Table ID="TbArbre" runat="server" Width="900px" Height="100%">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top" Width="100%" Height="100%">
                                    <asp:Panel ID="PanelTree" runat="server" ScrollBars="Auto" Style="text-align: left; min-height: 200px" Width="100%">
                                        <%--<asp:TreeView ID="TreeView1" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="Snow" ForeColor="#142425" NodeIndent="10" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-ImageUrl="~/Images/Icones/DatabaseBleu.bmp" Style="overflow: auto; width: 100%; height: 100%">--%>
                                        <asp:TreeView ID="TreeListe" runat="server" BorderStyle="None" ForeColor="#142425" NodeIndent="10" LeafNodeStyle-HorizontalPadding="8px" Style="overflow: auto; width: 100%; height: 100%">
                                            <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" Font-Bold="true" />
                                        </asp:TreeView>
                                    </asp:Panel>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="CboTypeArmoire" />
                        <asp:PostBackTrigger ControlID="TreeListe" />
                        <asp:PostBackTrigger ControlID="txtRecherche" />
                        <asp:PostBackTrigger ControlID="ButtonA" />
                        <asp:PostBackTrigger ControlID="ButtonB" />
                        <asp:PostBackTrigger ControlID="ButtonC" />
                        <asp:PostBackTrigger ControlID="ButtonD" />
                        <asp:PostBackTrigger ControlID="ButtonE" />
                        <asp:PostBackTrigger ControlID="ButtonF" />
                        <asp:PostBackTrigger ControlID="ButtonG" />
                        <asp:PostBackTrigger ControlID="ButtonH" />
                        <asp:PostBackTrigger ControlID="ButtonI" />
                        <asp:PostBackTrigger ControlID="ButtonJ" />
                        <asp:PostBackTrigger ControlID="ButtonK" />
                        <asp:PostBackTrigger ControlID="ButtonL" />
                        <asp:PostBackTrigger ControlID="ButtonM" />
                        <asp:PostBackTrigger ControlID="ButtonN" />
                        <asp:PostBackTrigger ControlID="ButtonO" />
                        <asp:PostBackTrigger ControlID="ButtonP" />
                        <asp:PostBackTrigger ControlID="ButtonQ" />
                        <asp:PostBackTrigger ControlID="ButtonR" />
                        <asp:PostBackTrigger ControlID="ButtonS" />
                        <asp:PostBackTrigger ControlID="ButtonT" />
                        <asp:PostBackTrigger ControlID="ButtonU" />
                        <asp:PostBackTrigger ControlID="ButtonV" />
                        <asp:PostBackTrigger ControlID="ButtonW" />
                        <asp:PostBackTrigger ControlID="ButtonX" />
                        <asp:PostBackTrigger ControlID="ButtonY" />
                        <asp:PostBackTrigger ControlID="ButtonZ" />
                        <asp:PostBackTrigger ControlID="ButtonArobase" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="lblNb" runat="server" Width="100%" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" ForeColor="White" Font-Italic="true" Style="text-align: center" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell>
                <asp:HiddenField ID="HButtonLettre" runat="server" Value="" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Panel>
