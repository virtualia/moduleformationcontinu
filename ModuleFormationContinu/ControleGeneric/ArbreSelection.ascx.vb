﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports System.Reflection
Imports Virtualia.ObjetBaseStructure.Formation

Public Class ArbreSelection
    Inherits UserControl

    Private WsSiInhib As Boolean = False
    Private Const WsTailleScrollBar As Integer = 18
    Private Const WsTailleTitre As Integer = 80
    Private Const WsTailleFiltre As Integer = 40

    Private WsFunc_DataSource As Func(Of IList)
    Private WsFunc_CalculNbTotal As Func(Of IList, String, Func(Of Object, Boolean), String)
    Private WsAct_NoeudChange As Action(Of List(Of TreeNode), String)
    Private WsAct_ItemArmoireChange As Action(Of String)
    '**RP
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsAct_ItemPlanChange As Action(Of String)

    Public Property SiFiltrePlanVisible As Boolean
        Get
            Return CellEtiPan.Visible
        End Get
        Set(value As Boolean)
            CellEtiPan.Visible = value
            CellDonPan.Visible = value
        End Set
    End Property
    Public WriteOnly Property Titre As String
        Set(value As String)
            lblTitre.Text = value
        End Set
    End Property

    Private ReadOnly Property V_Cache As CacheArbreSelection
        Get
            If Not (ViewState.KeyExiste(CacheArbreSelection.KeyState)) Then
                ViewState.AjouteValeur(CacheArbreSelection.KeyState, New CacheArbreSelection())
            End If
            Return ViewState.GetValeur(Of CacheArbreSelection)(CacheArbreSelection.KeyState)
        End Get
    End Property

    Public WriteOnly Property Width As Unit
        Set(value As Unit)
            If value.Type = UnitType.Percentage Then
                PanelGlobal.Width = value
                TbArbre.Width = Unit.Percentage(100)
                PanelTree.Width = Unit.Percentage(98)
                Return
            End If
            If value.Value < 500 Then
                PanelGlobal.Width = Unit.Pixel(500)
            Else
                PanelGlobal.Width = value
            End If
        End Set
    End Property

    Public WriteOnly Property Height As Unit
        Set(value As Unit)
            PanelGlobal.Height = value
        End Set
    End Property

    Public Property ArmoireItems As ListItemArmoireCollection
        Private Get
            If Not (ViewState.KeyExiste("ArmoireItems")) Then
                ViewState.AjouteValeur("ArmoireItems", New ListItemArmoireCollection())
            End If
            Return ViewState.GetValeur(Of ListItemArmoireCollection)("ArmoireItems")
        End Get
        Set(value As ListItemArmoireCollection)
            ViewState.AjouteValeur("ArmoireItems", value)
        End Set
    End Property

    Public Property ShowCheckBoxes As Boolean
        Private Get
            If Not (ViewState.KeyExiste("ShowCheckBoxes")) Then
                ViewState.AjouteValeur("ShowCheckBoxes", False)
            End If
            Return DirectCast(ViewState("ShowCheckBoxes"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.AjouteValeur("ShowCheckBoxes", value)
        End Set
    End Property

    Public Property ProprieteDeRecherche As String
        Private Get
            Return V_Cache.ProprieteDeRecherche
        End Get
        Set(value As String)
            V_Cache.ProprieteDeRecherche = value
            V_Cache.Sauve(ViewState)
        End Set
    End Property

    Public Property StyleNoeuds As ListStyleTreeNodeCollection
        Private Get
            If Not (ViewState.KeyExiste("StyleNoeuds")) Then
                ViewState.AjouteValeur("StyleNoeuds", New ListStyleTreeNodeCollection())
            End If
            Return ViewState.GetValeur(Of ListStyleTreeNodeCollection)("StyleNoeuds")
        End Get
        Set(value As ListStyleTreeNodeCollection)
            ViewState.AjouteValeur("StyleNoeuds", value)
        End Set
    End Property

    Public Property SiMoNoSelection As Boolean
        Private Get
            If Not (ViewState.KeyExiste("MoNoSelection")) Then
                ViewState.AjouteValeur("MoNoSelection", True)
            End If
            Return DirectCast(ViewState("MoNoSelection"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.AjouteValeur("MoNoSelection", value)
        End Set
    End Property

    Public WriteOnly Property Func_DataSource As Func(Of IList)
        Set(value As Func(Of IList))
            WsFunc_DataSource = value
        End Set
    End Property

    Public WriteOnly Property Func_CalculNbTotal As Func(Of IList, String, Func(Of Object, Boolean), String)
        Set(value As Func(Of IList, String, Func(Of Object, Boolean), String))
            WsFunc_CalculNbTotal = value
        End Set
    End Property

    Public WriteOnly Property Act_NoeudChange As Action(Of List(Of TreeNode), String)
        Set(value As Action(Of List(Of TreeNode), String))
            WsAct_NoeudChange = value
        End Set
    End Property

    Public WriteOnly Property Act_ItemArmoireChange As Action(Of String)
        Set(value As Action(Of String))
            WsAct_ItemArmoireChange = value
        End Set
    End Property

    Public WriteOnly Property Act_ItemPlanChange As Action(Of String)
        Set(value As Action(Of String))
            WsAct_ItemPlanChange = value
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        AddHandler TreeListe.TreeNodePopulate, Sub(s, ev)
                                                   ConstruitArbre(ev.Node)
                                               End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)
        '**RP
        If CboFiltrePlan.Items.Count = 0 Then
            Call ChargerFiltrePlan()
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        CboFiltrePlan.Text = V_Cache.FiltrePlan
        '**
        If PanelGlobal.Width.Type = UnitType.Percentage Then
            TbArbre.Width = Unit.Percentage(100)
            PanelTree.Width = Unit.Percentage(98)
        Else
            TbArbre.Width = Unit.Pixel((PanelGlobal.Width.Value - WsTailleScrollBar))
            PanelTree.Width = Unit.Pixel((PanelGlobal.Width.Value - WsTailleScrollBar))
        End If
        If PanelGlobal.Height.Type <> UnitType.Pixel Then
            Return
        End If
        Dim taille As Double = PanelGlobal.Height.Value

        'On enleve la taille du titre
        taille = taille - WsTailleTitre

        If ItemArmoire.GetMontreFiltre(CboTypeArmoire.SelectedItem) Then
            taille = taille - WsTailleFiltre
        End If

        taille = Math.Round(taille, 0)

        PanelTree.Height = New Unit(taille.ToString("F0") & "px")
    End Sub

    Public Sub Charge()
        ConstruitArbre()
    End Sub

    Private Sub ConstruitArbre(Optional Noeud As TreeNode = Nothing)
        If Noeud Is Nothing Then
            TreeListe.Nodes.Clear()
        End If
        If WsFunc_DataSource Is Nothing Then
            Return
        End If
        Dim Donnees As IList = WsFunc_DataSource()
        If Donnees Is Nothing OrElse Donnees.Count <= 0 Then
            lblNb.Text = "0"
            Return
        End If
        Dim val As String = ItemArmoire.GetValue(CboTypeArmoire.SelectedItem)
        Dim props As List(Of String) = V_Cache.LstItemsCboArmoire(val).Proprietes
        Dim generateur As GenerateurArbre = New GenerateurArbre()
        Dim temoin As Object = Donnees(0)
        Dim Styles_De_Objet As List(Of StyleTreeNode) = GetStyleDeTypeArmoire()

        For Each st As StyleTreeNode In Styles_De_Objet
            If ShowCheckBoxes = True Then
                st.ShowCheckBox = True
            End If
            generateur.StyleNoeuds.Add(st)
        Next

        Dim predicat As Func(Of Object, Boolean) = Nothing
        Dim filtre As ItemFiltreChaine = V_Cache.GetItemFiltre()

        If filtre IsNot Nothing Then
            Dim p As PropertyInfo = GestObjetBaseHelper.GetPropertyInfo(temoin, V_Cache.ProprieteDeRecherche)
            If Not (p Is Nothing) Then
                predicat = Function(it)
                               Dim valeur As String = p.GetValue(it, Nothing).ToString()

                               Dim CRetour As Boolean = filtre.Evalue(valeur)

                               Return CRetour
                           End Function

                If Noeud Is Nothing AndAlso WsFunc_CalculNbTotal Is Nothing Then
                    lblNb.Text = "" & (From it In Donnees Where predicat(it) Select 1).Sum() & " / " & Donnees.Count
                End If
            End If
        Else
            If Noeud Is Nothing Then
                lblNb.Text = Donnees.Count
            End If
        End If

        Dim noeuds As List(Of TreeNode) = Nothing
        Dim noeudscollection As TreeNodeCollection = Nothing

        If Noeud Is Nothing Then
            If WsFunc_CalculNbTotal IsNot Nothing Then
                lblNb.Text = WsFunc_CalculNbTotal(Donnees, CboTypeArmoire.SelectedItem.Value, predicat)
            End If
            noeuds = generateur.GetArbre(Donnees, props, predicat)
            noeudscollection = TreeListe.Nodes
        Else
            noeuds = generateur.GetSousArbre(Donnees, props, Noeud, predicat)
            noeudscollection = Noeud.ChildNodes
        End If
        If (noeuds Is Nothing OrElse noeuds.Count <= 0) Then
            Return
        End If
        noeudscollection.AddRange(noeuds)
    End Sub

    Public Sub EndInit()
        If IsPostBack = False Then
            V_Cache.LstStyleNoeud = StyleNoeuds
            V_Cache.LstItemsCboArmoire = ArmoireItems
            V_Cache.ProprieteDeRecherche = ProprieteDeRecherche
            V_Cache.TypeOperateur = TypeOperateurChaine.CONTIENT

            RCbo.Visible = (V_Cache.LstItemsCboArmoire.Count > 1)
            RTitre.Visible = Not (V_Cache.LstItemsCboArmoire.Count > 1)

            If V_Cache.LstItemsCboArmoire.Count > 0 Then
                V_Cache.LstItemsCboArmoire.ForEach(Sub(ita)
                                                       CboTypeArmoire.Items.Add(ItemArmoire.GetListItem(ita))
                                                   End Sub)
            End If

            If V_Cache.LstItemsCboArmoire.Count <= 0 Then
                CelluleLettre.Visible = False
                V_Cache.FiltrePlan = ""
                V_Cache.ClickLettre = False
                V_Cache.Sauve(ViewState)
                Exit Sub
            End If

            Dim it As ItemArmoire = V_Cache.LstItemsCboArmoire.First()
            Dim lettre As String
            If it.AfficheFitre = True Then
                CelluleLettre.Visible = True
                If it.FiltreDefaut = ValeurDefaut._TOUS Then
                    lettre = "Arobase"
                    V_Cache.LibFiltre = ""
                    V_Cache.ClickLettre = False
                Else
                    lettre = [Enum].GetName(GetType(ValeurDefaut), it.FiltreDefaut)
                    V_Cache.LibFiltre = lettre
                    V_Cache.ClickLettre = True
                End If
            Else
                CelluleLettre.Visible = False
                V_Cache.LibFiltre = ""
                V_Cache.ClickLettre = False
            End If
            V_Cache.Sauve(ViewState)
        End If
        InitEvenements()
    End Sub

    Public Sub SelectionneTypeArmoire(ByVal Clef As String)
        CboTypeArmoire.SelectedItem.Selected = False
        CboFiltrePlan.Items.Clear()
        Dim KeyItem As String
        For Each it In CboTypeArmoire.Items
            KeyItem = ItemArmoire.GetValue(DirectCast(it, ListItem))
            If KeyItem = Clef Then
                DirectCast(it, ListItem).Selected = True
                Exit For
            End If
        Next
        WsSiInhib = True
        CboTypeArmoire_SelectedIndexChanged(CboTypeArmoire, Nothing)
    End Sub

    Public Sub Selectionne(ByVal Valeur As String, Optional ByVal SiEstSelection As Boolean = True)
        Dim SiEstFiltreVisible As Boolean = ItemArmoire.GetMontreFiltre(CboTypeArmoire.SelectedItem)
        If SiEstFiltreVisible Then
            VisuHelper.DeselectionneTousLesBoutons(RowLettres, "~/Images/Lettres/")
            SelOuNon_BoutonLettre("ButtonArobase", "Arobase_Sel")
            txtRecherche.Text = Valeur
            InitFiltre(txtRecherche.Text, False)
        Else
            InitFiltre("", False)
        End If
        If TreeListe.SelectedNode IsNot Nothing Then
            TreeListe.SelectedNode.Selected = False
        End If
        TreeListe.CollapseAll()
        Dim func_recherche As Func(Of TreeNode, TreeNode) = Function(n As TreeNode)
                                                                Dim CRetour As TreeNode = Nothing
                                                                If (n Is Nothing) Then
                                                                    For Each rac In TreeListe.Nodes
                                                                        CRetour = func_recherche(rac)
                                                                        If CRetour IsNot Nothing Then
                                                                            Return CRetour
                                                                        End If
                                                                    Next
                                                                    Return CRetour
                                                                End If
                                                                If (n.Text = Valeur) Then
                                                                    Return n
                                                                End If
                                                                If (n.ChildNodes.Count > 0) Then
                                                                    For Each nf In n.ChildNodes
                                                                        CRetour = func_recherche(nf)
                                                                        If Not (CRetour Is Nothing) Then
                                                                            Return CRetour
                                                                        End If
                                                                    Next
                                                                End If
                                                                Return CRetour
                                                            End Function

        Dim noeudtrouve As TreeNode = func_recherche(Nothing)

        If noeudtrouve IsNot Nothing Then
            noeudtrouve.Selected = SiEstSelection
            Dim Act_expand As Action(Of TreeNode) = Sub(n)
                                                        If (n.Parent Is Nothing) Then
                                                            Return
                                                        End If
                                                        n.Parent.Expanded = True
                                                        Act_expand(n.Parent)
                                                    End Sub
            Act_expand(noeudtrouve)
        End If
    End Sub

    Public Sub CollapseTousLesNoeuds()
        TreeListe.CollapseAll()
    End Sub

    Public Sub CollapseAutreNoeud(ByVal Noeud As TreeNode)
        If Noeud Is Nothing Then
            Exit Sub
        End If
        Dim Collection_Noeuds As TreeNodeCollection = Nothing

        If Noeud.Depth = 0 Then
            Collection_Noeuds = TreeListe.Nodes
        Else
            Collection_Noeuds = Noeud.Parent.ChildNodes
        End If
        For Each nd In ( _
                            From n In Collection_Noeuds _
                            Where Not DirectCast(n, TreeNode).Equals(Noeud) _
                            Select DirectCast(n, TreeNode) _
                        )
            nd.CollapseAll()
        Next
        CollapseAutreNoeud(Noeud.Parent)
    End Sub

    Private Sub InitFiltre(ByVal Valeur As String, Optional ByVal SiClicklettre As Boolean = False)
        Dim tmp As CacheArbreSelection = V_Cache
        V_Cache.LibFiltre = Valeur
        V_Cache.ClickLettre = SiClicklettre
        V_Cache.Sauve(ViewState)
        Charge()
    End Sub

    Private Sub SelOuNon_BoutonLettre(ByVal Id As String, ByVal TypeImage As String)
        Dim Bouton As ImageButton = VisuHelper.GetImageBoutonLettre(RowLettres, Id)
        If Bouton Is Nothing Then
            Return
        End If
        Bouton.ImageUrl = "~/Images/Lettres/" & TypeImage & ".bmp"
    End Sub

    Private Sub InitEvenements()
        If WsAct_NoeudChange IsNot Nothing Then
            AddHandler TreeListe.SelectedNodeChanged, AddressOf TreeListe_SelectedNodeChanged
        End If
    End Sub

    Private Function GetStyleDeTypeArmoire() As List(Of StyleTreeNode)
        Dim lstprops As List(Of String) = (From it In V_Cache.LstItemsCboArmoire Where it.Key = CboTypeArmoire.SelectedItem.Value.Split("|"c)(0) Select it.Proprietes).First()
        Dim LstResultat As List(Of StyleTreeNode) = (From s In lstprops _
                                             Join st In V_Cache.LstStyleNoeud On s.ToLower() Equals st.Propriete_Liee.ToLower() _
                                             Select st).ToList()
        Return LstResultat
    End Function

    Private Sub TreeListe_SelectedNodeChanged(sender As Object, e As EventArgs)
        If WsAct_NoeudChange Is Nothing Then
            Return
        End If
        Dim arbre As TreeView = DirectCast(sender, TreeView)
        Dim lstnoeuds As List(Of TreeNode) = New List(Of TreeNode)()

        If ShowCheckBoxes = False Then
            lstnoeuds.Add(arbre.SelectedNode)
        Else
            If SiMoNoSelection = True Then
                Dim noeuds As List(Of TreeNode) = New List(Of TreeNode)()
                For Each n As TreeNode In arbre.CheckedNodes
                    noeuds.Add(n)
                Next
                noeuds.ForEach(Sub(it)
                                   it.Checked = False
                               End Sub)
            End If

            Dim n_selectionne As TreeNode = arbre.SelectedNode
            If Not (n_selectionne.Checked) Then
                n_selectionne.Checked = True
            End If

            For Each n As TreeNode In arbre.CheckedNodes
                lstnoeuds.Add(n)
            Next

        End If

        WsAct_NoeudChange(lstnoeuds, ItemArmoire.GetValue(CboTypeArmoire.SelectedItem))
    End Sub

    Protected Sub CboTypeArmoire_SelectedIndexChanged(sender As Object, e As EventArgs)
        TreeListe.Nodes.Clear()
        If WsSiInhib = False Then
            If WsAct_ItemArmoireChange IsNot Nothing Then
                WsAct_ItemArmoireChange(ItemArmoire.GetValue(DirectCast(sender, DropDownList).SelectedItem))
                Exit Sub
            End If
        End If

        Dim SiEstFiltreVisible As Boolean = ItemArmoire.GetMontreFiltre(DirectCast(sender, DropDownList).SelectedItem)

        If SiEstFiltreVisible Then
            If WsSiInhib = True Then
                'On vient de "SelectionneTypeArmoire"
                VisuHelper.DeselectionneTousLesBoutons(RowLettres, "~/Images/Lettres/")

                Dim ValeurDefaut As String = [Enum].GetName(GetType(ValeurDefaut), (From it In V_Cache.LstItemsCboArmoire Where it.Key = CboTypeArmoire.SelectedItem.Value.Split("|"c)(0) Select it.FiltreDefaut).First())
                If ValeurDefaut = "_TOUS" Then
                    HButtonLettre.Value = "ButtonArobase"
                Else
                    HButtonLettre.Value = "Button" & ValeurDefaut
                End If
            End If
            If HButtonLettre.Value = "" Then
                ButtonLettre_Click(ButtonA, Nothing)
            Else
                Dim idbutton As String = HButtonLettre.Value
                HButtonLettre.Value = ""
                ButtonLettre_Click(VisuHelper.GetImageBoutonLettre(RowLettres, idbutton), Nothing)
            End If
        Else
            InitFiltre("")
        End If
        CelluleLettre.Visible = SiEstFiltreVisible
        WsSiInhib = False
    End Sub

    Protected Sub ButtonLettre_Click(sender As Object, e As ImageClickEventArgs)
        txtRecherche.Text = ""
        Dim Button As ImageButton = DirectCast(sender, ImageButton)
        Dim id As String = Button.ID

        If HButtonLettre.Value = id Then
            Return
        End If
        Dim Lettre As String = (id.ToLower().Split(New String() {"button"}, StringSplitOptions.RemoveEmptyEntries))(0).ToUpper()
        If Lettre = "AROBASE" Then
            InitFiltre("")
        Else
            InitFiltre(Lettre, True)
        End If
        SelOuNon_BoutonLettre(id, Lettre + "_Sel")
        If HButtonLettre.Value.Trim() <> "" Then
            Dim Lettre_Anc = (HButtonLettre.Value.ToLower().Split(New String() {"button"}, StringSplitOptions.RemoveEmptyEntries))(0).ToUpper()
            SelOuNon_BoutonLettre(HButtonLettre.Value, Lettre_Anc)
        End If
        HButtonLettre.Value = id
    End Sub

    Protected Sub txtRecherche_TextChanged(sender As Object, e As EventArgs)
        If V_Cache.ClickLettre = True Then
            If V_Cache.LibFiltre <> "" Then
                SelOuNon_BoutonLettre("Button" & V_Cache.LibFiltre.ToUpper(), V_Cache.LibFiltre.ToUpper())
                HButtonLettre.Value = "ButtonArobase"
            End If
            V_Cache.ClickLettre = False
            SelOuNon_BoutonLettre("ButtonArobase", "Arobase_Sel")
        End If

        If DirectCast(sender, TextBox).Text.Trim() = "" Then
            SelOuNon_BoutonLettre("ButtonArobase", "Arobase")
            Dim it As ItemArmoire = (From ita In V_Cache.LstItemsCboArmoire Where ita.Key = CboTypeArmoire.SelectedItem.Value.Split("|"c)(0) Select ita).First()
            HButtonLettre.Value = ""
            If it.FiltreDefaut = ValeurDefaut._TOUS Then
                ButtonLettre_Click(ButtonArobase, ImageClickEventArgs.Empty)
                Exit Sub
            End If

            Dim bouton As ImageButton = VisuHelper.GetImageBoutonLettre(RowLettres, "Button" & [Enum].GetName(GetType(ValeurDefaut), it.FiltreDefaut))
            ButtonLettre_Click(bouton, Nothing)
            Exit Sub

        End If
        InitFiltre(DirectCast(sender, TextBox).Text)
    End Sub

    '** Ajouts RP
    Protected Sub CboFiltrePlan_SelectedIndexChanged(sender As Object, e As EventArgs)
        V_Cache.FiltrePlan = CType(sender, DropDownList).SelectedItem.Text
        V_Cache.Sauve(ViewState)
        If WsAct_ItemPlanChange IsNot Nothing Then
            WsAct_ItemPlanChange(DirectCast(sender, DropDownList).SelectedItem.Text)
        End If
    End Sub

    Private Sub ChargerFiltrePlan()
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Dim LstPlans As List(Of ListItem)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        CboFiltrePlan.Items.Clear()
        CboFiltrePlan.Items.Add("")
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        LstPlans = Requeteur.ListePlanOuStagePourFiltre("", 10)
        If LstPlans Is Nothing OrElse LstPlans.Count = 0 Then
            Exit Sub
        End If
        For Each Element As ListItem In LstPlans
            If Element.Text <> "" AndAlso Strings.Left(Element.Text, 1) <> "(" Then
                CboFiltrePlan.Items.Add(Element)
            End If
        Next
    End Sub
End Class
