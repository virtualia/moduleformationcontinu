﻿Imports Virtualia.ObjetBaseStructure.Formation

Public Class BoutonSupprimer
    Inherits BoutonBase

    Protected Overrides ReadOnly Property Btn As Button
        Get
            Return Btn1
        End Get
    End Property

    Protected Overrides ReadOnly Property CellBtn As TableCell
        Get
            Return CellBtn1
        End Get
    End Property

End Class