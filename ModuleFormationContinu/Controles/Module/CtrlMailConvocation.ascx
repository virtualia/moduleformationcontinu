﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlMailConvocation.ascx.vb" Inherits="Virtualia.Net.CtrlMailConvocation" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BackColor="#8DA8A3" BorderColor="#1C5150" Height="600px" Width="780px" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2">
            <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
                CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="True"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px" >
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                         <asp:Button ID="CommandeOK" runat="server" Text="Envoyer" Width="65px" Height="20px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top" ColumnSpan="2">
            <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Convocations par email à une session de formation" Height="20px" Width="400px"
                            BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="White"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;" >
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreInscrits" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:CheckBox ID="CocheAll" runat="server" AutoPostBack="true" Height="22px" Width="22px" BackColor="Transparent" Style="margin-left:5px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="400px" Width="250px" BackColor="Snow" HorizontalAlign="Left" VerticalAlign="Top">
                        <asp:CheckBoxList ID="ListeValidation" runat="server" BackColor="Snow" ForeColor="#124545" 
                                Font-Names="Trebuchet MS" Font-Bold="False" Font-Size="Small" Font-Italic="false"
                                CellPadding="2" CellSpacing="2" RepeatLayout="Flow" AutoPostBack="true">
                        </asp:CheckBoxList>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow> 
                     <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VCoupleEtiDonnee ID="MailSujet" runat="server" V_SiDonneeDico="false" V_SiAutoPostBack="true"
                                    EtiWidth="150px" EtiText="Objet du courriel" DonWidth="350px" DonTabIndex="1"/>
                     </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Center">
                         <Virtualia:VCoupleVerticalEtiDonnee ID="MailContenu" runat="server" V_SiDonneeDico="false" DonTextMode="true" V_SiAutoPostBack="true"
                                    EtiWidth="500px" EtiText="Contenu du courriel" DonWidth="500px" DonHeight="250px" DonTabIndex="2"/>
                     </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Center">
                         <Virtualia:VCoupleVerticalEtiDonnee ID="MailSignature" runat="server" V_SiDonneeDico="false" DonTextMode="true" V_SiAutoPostBack="true"
                                    EtiWidth="500px" EtiText="Signature du courriel" DonWidth="500px" DonHeight="150px" DonTabIndex="3"/>
                     </asp:TableCell>      
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
  </asp:TableRow>
</asp:Table>