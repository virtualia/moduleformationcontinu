﻿Public Class CtrlMailConvocation
    Inherits System.Web.UI.UserControl
    Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageDialogue As MsgDialog_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.CacheValidationDemande
    Private WsNomStateCache As String = "Convocations"

    Protected Overridable Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageDialogue(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.CacheValidationDemande
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.CacheValidationDemande)
            End If
            Dim NewCache As Virtualia.Net.CacheValidationDemande
            NewCache = New Virtualia.Net.CacheValidationDemande
            Return NewCache
        End Get
        Set(value As Virtualia.Net.CacheValidationDemande)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property V_CocheAll As Boolean
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.SiCocheAllCochee
        End Get
        Set(value As Boolean)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.SiCocheAllCochee = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Private Property V_ListeIdentifiants As List(Of Integer)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.ListeIdentifiantsPER
        End Get
        Set(ByVal value As List(Of Integer))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.ListeIdentifiantsPER = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_DonneesEdition As Virtualia.Net.DonneeEditionInfo
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition
        End Get
        Set(value As Virtualia.Net.DonneeEditionInfo)
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            ListeValidation.Items.Clear()
            DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition = value
        End Set
    End Property

    Private Sub CocheAll_CheckedChanged(sender As Object, e As EventArgs) Handles CocheAll.CheckedChanged
        V_CocheAll = CocheAll.Checked
        For Each element As ListItem In ListeValidation.Items
            element.Selected = CocheAll.Checked
        Next
    End Sub

    Private Sub CtrlMailConvocation_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim LstDonnees As Virtualia.Net.DonneeEditionInfo
        Dim Chaine As System.Text.StringBuilder
        Dim IntituleSession As String = ""
        Dim Domaine As String = ""
        Dim DateSession As String = ""
        Dim HeureSession As String = ""
        Dim LieuFormation As String = ""
        Dim LstIdePER As List(Of Integer)

        LstDonnees = V_DonneesEdition
        If LstDonnees Is Nothing OrElse LstDonnees.ListeStagiaires.Count = 0 Then
            ListeValidation.Items.Clear()
            Exit Sub
        End If
        If ListeValidation.Items.Count = 0 Then
            LstIdePER = New List(Of Integer)
            For Each Inscrit In LstDonnees.ListeStagiaires
                ListeValidation.Items.Add(New ListItem(Inscrit.NomPrenom, Inscrit.Identifiant))
                LstIdePER.Add(Inscrit.Identifiant)
            Next
            V_ListeIdentifiants = LstIdePER
            WsCtl_Cache = CacheVirControle
            For Each element As ListItem In ListeValidation.Items
                element.Selected = WsCtl_Cache.SiCocheAllCochee
            Next
        End If

        IntituleSession = LstDonnees.Stage.Intitule
        Domaine = LstDonnees.Stage.Domaine
        DateSession = LstDonnees.Stage.DateDeb
        If LstDonnees.Stage.H_AM_Deb <> "" Then
            HeureSession = LstDonnees.Stage.H_PM_Deb
        Else
            HeureSession = LstDonnees.Stage.H_AM_Deb
        End If
        LieuFormation = LstDonnees.LieuFormation.SLieu

        If MailSujet.DonText = "" Then
            MailSujet.DonText = "Convocation à une formation"
        End If
        If MailContenu.DonText = "" Then
            Chaine = New System.Text.StringBuilder
            Chaine.Append("Bonjour," & vbCrLf & vbCrLf)
            Chaine.Append("A la suite de votre demande d'inscription à la formation " & IntituleSession)
            Chaine.Append(" du " & DateSession)
            If HeureSession <> "" Then
                Chaine.Append(" à " & HeureSession)
            End If
            Chaine.Append(", je vous confirme qu'une place vous y a été réservée. ")
            If LieuFormation <> "" Then
                Chaine.Append("Cette session se déroulera " & LieuFormation & ".")
            End If
            Chaine.Append(vbCrLf & vbCrLf)
            Chaine.Append("Afin d'assurer le suivi de vos formations, je vous rappelle qu'il est nécessaire d'émarger au début de chaque journée de stage." & vbCrLf)
                Chaine.Append("Enfin je vous précise que ce courriel vaut convocation." & vbCrLf)
            If Domaine <> "" Then
                Chaine.Append("Pour information du service comptable,  cette formation relève du domaine de formation : " & Domaine & "." & vbCrLf)
            End If
            Chaine.Append("Je vous souhaite une bonne formation et demeure à votre disposition pour toute information complémentaire." & vbCrLf & vbCrLf)
            Chaine.Append("Bien cordialement")
                MailContenu.DonText = Chaine.ToString
            End If
            If MailSignature.DonText = "" Then
            Chaine = New System.Text.StringBuilder
            Chaine.Append(vbCrLf)
            Chaine.Append("Direction des ressources humaines" & vbCrLf)
            Chaine.Append("Département de la formation" & vbCrLf)
            MailSignature.DonText = Chaine.ToString
        End If
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        If ListeValidation.Items.Count = 0 Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        WsCtl_Cache = CacheVirControle
        Dim Cretour As Boolean
        Dim LstIdeOK As List(Of Integer)
        Dim IdeChef As Integer
        Dim ObjetMail As Virtualia.Net.Session.ObjetMessagerie
        Dim TexteMsg As String
        Dim LstCrendu As List(Of String) = New List(Of String)

        For Each Element As ListItem In ListeValidation.Items
            If Element.Selected = True Then
                LstIdeOK = New List(Of Integer)
                LstIdeOK.Add(CInt(Element.Value))
                IdeChef = WebFct.PointeurGlobal.VirIdentifiantManager(CInt(Element.Value), 0)
                If IdeChef > 0 Then
                    LstIdeOK.Add(IdeChef)
                End If
                TexteMsg = "A l'attention de " & Element.Text & vbCrLf & MailContenu.DonText & vbCrLf & MailSignature.DonText
                ObjetMail = New Virtualia.Net.Session.ObjetMessagerie(WebFct.PointeurGlobal, "", LstIdeOK, "Convocation formation")
                Cretour = ObjetMail.EnvoyerEMail(WebFct.PointeurUtilisateur.V_NomdeConnexion, MailSujet.DonText, TexteMsg, True)
                If Cretour = True Then
                    LstCrendu.Add(" - un email a été envoyé à " & Element.Text & " et son responsable hiérarchique")
                Else
                    LstCrendu.Add(" - un email n'a pu être envoyé à " & Element.Text & " et son responsable hiérarchique")
                End If
            End If
        Next
        If LstCrendu.Count = 0 Then
            Exit Sub
        End If
        TexteMsg = ""
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Dim I As Integer
        For I = 0 To LstCrendu.Count - 1
            TexteMsg &= LstCrendu.Item(I) & vbCrLf
        Next I
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Convocation", 0, "", "OK", "Envoi des convocations", TexteMsg)
        V_MessageDialog(Evenement)
    End Sub
End Class