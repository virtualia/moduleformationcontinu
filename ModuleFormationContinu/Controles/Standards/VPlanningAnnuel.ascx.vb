﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements

Public Class VPlanningAnnuel
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheCalendrier
    Private WsNomStateCache As String = "VPlanningIde"

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public Property Identifiant() As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Ide_Dossier
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.Ide_Dossier = value Then
                Exit Property
            End If
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Ide_Dossier = value
            CacheVirControle = WsCtl_Cache
            HIndex.Value = "0"
        End Set
    End Property

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheCalendrier
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheCalendrier)
            End If
            Dim TableauW(0) As String
            Dim NewCache As Virtualia.Net.VCaches.CacheCalendrier
            NewCache = New Virtualia.Net.VCaches.CacheCalendrier
            NewCache.Date_Valeur = V_WebFonction.ViRhDates.DateduJour
            If LibelJours.SelectionLibelMois <> "" Then
                TableauW = Strings.Split(LibelJours.SelectionLibelMois, Strings.Space(1), -1)
                NewCache.Annee_Selection = CInt(TableauW(1))
                NewCache.Mois_Selection = V_WebFonction.ViRhDates.IndexduMois(TableauW(0))
            Else
                NewCache.Annee_Selection = System.DateTime.Now.Year
                NewCache.Mois_Selection = System.DateTime.Now.Month
            End If
            If LibelJours.SelectionNbMois <> "" Then
                TableauW = Strings.Split(LibelJours.SelectionNbMois, Strings.Space(1), -1)
                NewCache.Coche_Butoir = TableauW(0)
            Else
                NewCache.Coche_Butoir = "12"
            End If
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheCalendrier)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub VPlanningAnnuel_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim UtiSession As Virtualia.Net.Session.ObjetSession = V_WebFonction.PointeurUtilisateur
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        '****** Initialisations *******************
        If CInt(HIndex.Value) = 0 Then
            Call InitialiserPage()
            Exit Sub
        End If
        '******** Fin traitement **********************************
        Dim NumIndex As Integer = CInt(HIndex.Value)
        If Date_Valeur(NumIndex - 1) = "" Then
            Call TerminerPage()
            TimerPlanning.Enabled = False
            Exit Sub
        End If
        '****************** Traitement par groupe de 18 ********************
        HIndex.Value = CStr(TraiterAsynchrone(NumIndex).Result)
    End Sub

    Private Sub InitialiserPage()
        Dim IndiceK As Integer
        Dim Ctl As System.Web.UI.Control
        Dim Ide_Dossier As Integer = Identifiant
        Dim DateDebut As String = Date_Valeur(0)
        Dim DateFin As String = Date_Valeur(0, True)

        For IndiceK = 0 To 17
            Ctl = V_WebFonction.VirWebControle(Me.CadrePlanning, "PlanningM", IndiceK)
            If Ctl Is Nothing Then
                Exit For
            End If
            Ctl.Visible = False
        Next IndiceK
        HIndex.Value = "1"
        TimerPlanning.Enabled = True
    End Sub

    Private Sub TerminerPage()
        Dim IndiceK As Integer
        Dim Ctl As System.Web.UI.Control
        For IndiceK = 0 To 17
            Ctl = V_WebFonction.VirWebControle(Me.CadrePlanning, "PlanningM", IndiceK)
            If Ctl Is Nothing Then
                Exit For
            End If
            If Ctl.Visible = False Then
                Exit For
            End If
        Next IndiceK
    End Sub

    Private Async Function TraiterAsynchrone(ByVal Index As Integer) As System.Threading.Tasks.Task(Of Integer)
        Dim Traitement As Func(Of Integer, Integer) = Function(NoEtape As Integer)
                                                          Return TraiterGroupe(Index)
                                                      End Function
        Return Await System.Threading.Tasks.Task.FromResult(Of Integer)(Traitement(Index))
    End Function

    Private Function TraiterGroupe(ByVal NoEtape As Integer) As Integer
        Dim Ctl As System.Web.UI.Control
        Dim VirControle As VPlanningMensuel
        Dim IndiceK As Integer
        Dim DateValeur As String = Date_Valeur(NoEtape - 1)

        '****************** Traitement par groupe de 18 ********************
        For IndiceK = NoEtape - 1 To NoEtape + 17
            DateValeur = Date_Valeur(IndiceK)
            If DateValeur = "" Then
                Return IndiceK + 1
            End If
            Ctl = V_WebFonction.VirWebControle(Me.CadrePlanning, "PlanningM", IndiceK)
            If Ctl Is Nothing Then
                Return IndiceK + 1
            End If
            VirControle = CType(Ctl, VPlanningMensuel)
            VirControle.Identifiant(DateValeur) = Identifiant
            VirControle.Visible = True
        Next IndiceK
        Return NoEtape + 18
    End Function

    Private Sub LibelJours_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles LibelJours.ValeurChange
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Nbmois As Integer = 12
        Dim TableauW(0) As String

        WsCtl_Cache = CacheVirControle
        Annee = WsCtl_Cache.Annee_Selection
        Mois = WsCtl_Cache.Mois_Selection
        If IsNumeric(WsCtl_Cache.Coche_Butoir) Then
            Nbmois = CInt(WsCtl_Cache.Coche_Butoir)
        End If
        TableauW = Strings.Split(e.Valeur, Strings.Space(1), -1)
        Select Case e.Parametre
            Case Is = "Mois"
                Annee = CInt(TableauW(1))
                Mois = V_WebFonction.ViRhDates.IndexduMois(TableauW(0))
            Case Is = "Nombre"
                Nbmois = CInt(TableauW(0))
        End Select
        WsCtl_Cache.Annee_Selection = Annee
        WsCtl_Cache.Mois_Selection = Mois
        WsCtl_Cache.Coche_Butoir = CStr(Nbmois)
        CacheVirControle = WsCtl_Cache

        Dim UtiSession As Virtualia.Net.Session.ObjetSession = V_WebFonction.PointeurUtilisateur
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        HIndex.Value = "0"
        TimerPlanning.Enabled = True
    End Sub

    Private ReadOnly Property Date_Valeur(ByVal Index As Integer, Optional ByVal Sidatefin As Boolean = False) As String
        Get
            Dim Mois As Integer
            Dim Annee As Integer
            Dim NbMois As Integer = 12
            Dim IndiceM As Integer

            WsCtl_Cache = CacheVirControle
            Annee = WsCtl_Cache.Annee_Selection
            Mois = WsCtl_Cache.Mois_Selection
            If IsNumeric(WsCtl_Cache.Coche_Butoir) Then
                NbMois = CInt(WsCtl_Cache.Coche_Butoir)
            End If

            If Sidatefin = True Then
                IndiceM = (NbMois - 1) + Mois
            Else
                IndiceM = Mois + Index
                If IndiceM > (NbMois - 1) + Mois Or Index > 17 Then
                    Return ""
                End If
            End If
            If IndiceM > 12 Then
                Do
                    IndiceM -= 12
                    Annee += 1
                    If IndiceM <= 12 Then
                        Exit Do
                    End If
                Loop
            End If
            If Sidatefin = True Then
                Return V_WebFonction.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(IndiceM, "00") & "/" & Annee)
            Else
                Return "01/" & Strings.Format(IndiceM, "00") & "/" & Annee
            End If
        End Get
    End Property
End Class