﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VMenuCommun
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event Menu_Click As Dossier_ClickEventHandler
    Private WsNomState As String = "TreeMenu"
    Private WsListeMenu As List(Of Virtualia.Net.Controles.ItemMenuCommun)
    Private WsAppelant As String

    Protected Overridable Sub VMenu_Click(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent Menu_Click(Me, e)
    End Sub

    Public Property V_Appelant As String
        Get
            Return WsAppelant
        End Get
        Set(ByVal value As String)
            WsAppelant = value
        End Set
    End Property

    Private Property V_DataPathItemSel As String
        Get
            Dim CacheArmoire As ArrayList

            CacheArmoire = CType(Me.ViewState(WsNomState), ArrayList)
            If CacheArmoire Is Nothing Then
                Return ""
            End If
            Return CacheArmoire(0).ToString
        End Get
        Set(ByVal value As String)
            Dim CacheArmoire As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            CacheArmoire = New ArrayList
            CacheArmoire.Add(value)
            Me.ViewState.Add(WsNomState, CacheArmoire)
        End Set
    End Property

    Private Sub FaireListeOrganisee()
        Dim IndiceA As Integer
        Dim RuptureN1 As String = ""
        Dim UrlImageN1 = "~/Images/Armoire/BleuFermer16.bmp"
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        Dim NewNoeudN1 As TreeNode = Nothing
        Dim NewNoeudN2 As TreeNode = Nothing

        TreeListeMenu.Nodes.Clear()
        TreeListeMenu.LevelStyles.Clear()

        Call ChargerMenu()

        For IndiceA = 0 To WsListeMenu.Count - 1
            Select Case WsListeMenu.Item(IndiceA).Groupe
                Case Is <> RuptureN1
                    RuptureN1 = WsListeMenu.Item(IndiceA).Groupe
                    NewNoeudN2 = Nothing

                    NewNoeudN1 = New TreeNode(RuptureN1, "N1" & VI.Tild & IndiceA.ToString)
                    NewNoeudN1.ImageUrl = UrlImageN1
                    NewNoeudN1.PopulateOnDemand = False
                    NewNoeudN1.SelectAction = TreeNodeSelectAction.SelectExpand
                    TreeListeMenu.Nodes.Add(NewNoeudN1)
            End Select

            If NewNoeudN1 IsNot Nothing Then
                NewNoeudN2 = New TreeNode(WsListeMenu.Item(IndiceA).Intitule, CStr(WsListeMenu.Item(IndiceA).Index_Vue))
                NewNoeudN2.ImageUrl = UrlImagePer
                NewNoeudN2.PopulateOnDemand = False
                NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                NewNoeudN1.ChildNodes.Add(NewNoeudN2)
            End If

        Next IndiceA

        Call StylerlArmoire(2)

        RuptureN1 = V_DataPathItemSel
        If RuptureN1 <> "" Then
            Try
                NewNoeudN1 = TreeListeMenu.FindNode(RuptureN1)
                NewNoeudN1.Selected = True
            Catch ex As Exception
                NewNoeudN1 = Nothing
            End Try
        End If
        TreeListeMenu.ExpandAll()

    End Sub

    Private Sub StylerlArmoire(ByVal Profondeur As Integer)
        Dim Vstyle As System.Web.UI.WebControls.TreeNodeStyle
        TreeListeMenu.LevelStyles.Clear()

        If Profondeur = 1 Then

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Italic = True
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(1)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#142425")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 2 Then

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#1C5151")
        Vstyle.ForeColor = Drawing.Color.White

        TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 3 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#137A76")
        Vstyle.ForeColor = Drawing.Color.White

        TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 4 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeMenu.LevelStyles.Add(Vstyle)
        End If

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListeOrganisee()
    End Sub

    Private Sub ChargerMenu()
        Dim Chaine As String
        Dim Menu As Virtualia.Net.Controles.ItemMenuCommun
        WsListeMenu = New List(Of Virtualia.Net.Controles.ItemMenuCommun)

        Select Case WsAppelant
            Case "FOR"
                Chaine = "IDENTIFICATION"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Définition"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 1
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueIdentification"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Caractéristiques"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 3
                Menu.Index_Vue = 1
                Menu.ID_Vue = "VueCaracteristique"
                WsListeMenu.Add(Menu)

                Chaine = "DESCRIPTIF / INTERVENANTS"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Descriptif"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 2
                Menu.Index_Vue = 2
                Menu.ID_Vue = "VueDescriptif"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Intervenants"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 9
                Menu.Index_Vue = 3
                Menu.ID_Vue = "VueIntervenant"
                WsListeMenu.Add(Menu)

                Chaine = "SESSIONS"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Session"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 5
                Menu.Index_Vue = 4
                Menu.ID_Vue = "VueSession"
                WsListeMenu.Add(Menu)

                Chaine = "COUTS / FACTURES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Coûts"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 6
                Menu.Index_Vue = 5
                Menu.ID_Vue = "VueCout"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Factures"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 7
                Menu.Index_Vue = 6
                Menu.ID_Vue = "VueFacture"
                WsListeMenu.Add(Menu)

                Chaine = "EVALUATION"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Evaluation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueFormation
                Menu.Numero_Objet = 4
                Menu.Index_Vue = 7
                Menu.ID_Vue = "VueEvaluation"
                WsListeMenu.Add(Menu)

            Case "PER"
                Chaine = "DEMANDES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Demande de formation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaDemandeFormation
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueDemande"
                WsListeMenu.Add(Menu)

                Chaine = "DIPLOMES ET QUALIFICATIONS"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Diplômes"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaDiplome
                Menu.Index_Vue = 1
                Menu.ID_Vue = "VueDiplome"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Qualifications"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCompetence
                Menu.Index_Vue = 2
                Menu.ID_Vue = "VueQualification"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Spécialités"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaSpecialite
                Menu.Index_Vue = 3
                Menu.ID_Vue = "VueSpecialite"
                WsListeMenu.Add(Menu)

                Chaine = "CONGES ET ABSENCES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Planning"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaPresence
                Menu.Index_Vue = 5
                Menu.ID_Vue = "VueAbsence"
                WsListeMenu.Add(Menu)

                Chaine = "CONGES ET ABSENCES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Congés et absences"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAbsence
                Menu.Index_Vue = 6
                Menu.ID_Vue = "VueAbsence"
                WsListeMenu.Add(Menu)

                Chaine = "SITUATION ADMINISTRATIVE"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Administration / Etablissement"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaSociete
                Menu.Index_Vue = 11
                Menu.ID_Vue = "VueEtablissement"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affectations fonctionnelles"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaOrganigramme
                Menu.Index_Vue = 4
                Menu.ID_Vue = "VueAffectation"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Statut"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaStatut
                Menu.Index_Vue = 7
                Menu.ID_Vue = "VueStatut"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Position"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaActivite
                Menu.Index_Vue = 8
                Menu.ID_Vue = "VuePosition"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Grade"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaGrade
                Menu.Index_Vue = 9
                Menu.ID_Vue = "VueGrade"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Adresse professionnelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdrPro
                Menu.Index_Vue = 10
                Menu.ID_Vue = "VueAdrPro"
                WsListeMenu.Add(Menu)
        End Select

    End Sub

    Private Sub TreeListeMenu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeMenu.SelectedNodeChanged
        V_DataPathItemSel = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.ValuePath

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs = Nothing
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, _
                                                            CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text)
        VMenu_Click(Evenement)
    End Sub

End Class