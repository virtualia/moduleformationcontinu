﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VListeCombo
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsNomStateCache As String = "VListeCombo"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheControleListe
    Private WsSiLigneBlanche As Boolean = False

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheControleListe
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheControleListe)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheControleListe
            NewCache = New Virtualia.Net.VCaches.CacheControleListe
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheControleListe)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property V_NomTable As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.NomTableGenerale
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.NomTableGenerale = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_CritereListe As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Critere
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Critere = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property SiLigneBlanche As Boolean
        Get
            Return WsSiLigneBlanche
        End Get
        Set(ByVal value As Boolean)
            WsSiLigneBlanche = value
        End Set
    End Property

    Public Property V_Liste() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Liste_Valeurs
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Liste_Valeurs = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_Libel() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Caption
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Caption = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property EtiText As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property LstStyle As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                VComboBox.Style.Remove(Strings.Trim(TableauW(0)))
                VComboBox.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property LstText As String
        Get
            Return VComboBox.Text
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Valeur_ItemSelectionne = value
            CacheVirControle = WsCtl_Cache
            Try
                VComboBox.Items.FindByText(value).Selected = True
                VComboBox.Text = value
            Catch ex As Exception
                Exit Property
            End Try
        End Set
    End Property

    Public Property LstHeight As System.Web.UI.WebControls.Unit
        Get
            Return VComboBox.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VComboBox.Height = value
        End Set
    End Property

    Public Property LstWidth As System.Web.UI.WebControls.Unit
        Get
            Return VComboBox.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VComboBox.Width = value
        End Set
    End Property

    Public Property LstBackColor As System.Drawing.Color
        Get
            Return VComboBox.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VComboBox.BackColor = value
        End Set
    End Property

    Public Property LstForeColor As System.Drawing.Color
        Get
            Return VComboBox.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VComboBox.ForeColor = value
        End Set
    End Property

    Public Property LstBorderColor As System.Drawing.Color
        Get
            Return VComboBox.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            VComboBox.BorderColor = value
        End Set
    End Property

    Public Property LstBorderWidth As System.Web.UI.WebControls.Unit
        Get
            Return VComboBox.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            VComboBox.BorderWidth = value
        End Set
    End Property

    Public Property LstBorderStyle As System.Web.UI.WebControls.BorderStyle
        Get
            Return VComboBox.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            VComboBox.BorderStyle = value
        End Set
    End Property

    Public Property LstVisible As Boolean
        Get
            Return VComboBox.Visible
        End Get
        Set(ByVal value As Boolean)
            VComboBox.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListe()
    End Sub

    Private Sub FaireListe()
        Dim IndiceI As Integer
        Dim TableauData(0) As String
        WsCtl_Cache = CacheVirControle

        VComboBox.Items.Clear()
        If WsCtl_Cache.Liste_Valeurs IsNot Nothing Then
            For IndiceI = 0 To WsCtl_Cache.Liste_Valeurs.Count - 1
                If WsCtl_Cache.Liste_Valeurs.Item(IndiceI) = "" Then
                    If SiLigneBlanche = False Then
                        Exit For
                    End If
                End If
                VComboBox.Items.Add(WsCtl_Cache.Liste_Valeurs.Item(IndiceI))
            Next IndiceI
        End If
        If EtiText = "" Then
            Select Case VComboBox.Items.Count
                Case Is = 0
                    Etiquette.Text = WsCtl_Cache.Libelles_Caption.Item(0)
                Case Is = 1
                    Etiquette.Text = WsCtl_Cache.Libelles_Caption.Item(1)
                Case Else
                    Etiquette.Text = VComboBox.Items.Count.ToString & Space(1) & WsCtl_Cache.Libelles_Caption.Item(2)
            End Select
        End If
        If WsCtl_Cache.Valeur_ItemSelectionne = "" Then
            VComboBox.SelectedIndex = VComboBox.Items.Count - 1
        Else
            Try
                VComboBox.Items.FindByText(WsCtl_Cache.Valeur_ItemSelectionne).Selected = True
                VComboBox.Text = WsCtl_Cache.Valeur_ItemSelectionne
            Catch ex As Exception
                Exit Try
            End Try
        End If
    End Sub

    Public ReadOnly Property VCount As Integer
        Get
            Return VComboBox.Items.Count
        End Get
    End Property

    Protected Sub VComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VComboBox.SelectedIndexChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Valeur_ItemSelectionne = VComboBox.SelectedItem.Text
        CacheVirControle = WsCtl_Cache

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CStr(VComboBox.SelectedIndex), VComboBox.SelectedValue)
        Saisie_Change(Evenement)
    End Sub

End Class