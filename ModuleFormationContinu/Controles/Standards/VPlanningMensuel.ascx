﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VPlanningMensuel.ascx.vb" Inherits="Virtualia.Net.VPlanningMensuel" %>

<asp:Table ID="VPlanningMois" runat="server" Width="825px" CellPadding="0" CellSpacing="0" HorizontalAlign="Left">
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="VMois" runat="server" Height="36px" Width="65px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" GridLines="None"
                        style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiMois" runat="server" Height="17px" Width="65px" BackColor="#8DA8A3"
                                ForeColor="#E9FDF9" BorderStyle="None" Text=""
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-left: 1px; margin-top: 1px; text-indent:1px; text-align: center; 
                                       vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiAnnee" runat="server" Height="17px" Width="65px" BackColor="#8DA8A3" 
                                ForeColor="#E9FDF9" BorderStyle="None" Text=""
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-left: 1px; margin-bottom: 1px; text-indent:1px; text-align: center; 
                                       vertical-align: top" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <div style="width: 5px"></div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour00" runat="server" Height="36px" Width="20px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM00" runat="server" Height="18px" Width="18px" BackColor="LightGray"
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM00" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top;
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour01" runat="server" Height="36px" Width="20px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM01" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM01" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour02" runat="server" Height="36px" Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM02" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM02" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour03" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM03" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM03" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour04" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM04" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM04" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour05" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM05" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom;
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM05" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour06" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM06" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM06" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <div style="width: 3px"></div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour07" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM07" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM07" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour08" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM08" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM08" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour09" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM09" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM09" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour10" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM10" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM10" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour11" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM11" runat="server" Height="18px" Width="18px" BackColor="LightGray"
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM11" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour12" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM12" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM12" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour13" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM13" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM13" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <div style="width: 3px"></div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour14" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM14" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM14" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour15" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM15" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM15" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour16" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM16" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM16" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table> 
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour17" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM17" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM17" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour18" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM18" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM18" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour19" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM19" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM19" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour20" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM20" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM20" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <div style="width: 3px"></div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour21" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM21" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM21" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour22" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM22" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM22" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
         <asp:TableCell>
            <asp:Table ID="VJour23" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM23" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM23" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour24" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM24" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM24" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
         <asp:TableCell>
            <asp:Table ID="VJour25" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM25" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM25" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour26" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM26" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM26" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour27" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM27" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM27" runat="server" Height="18px" Width="18px" BackColor="LightGray"
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <div style="width: 3px"></div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour28" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM28" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM28" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour29" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM29" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM29" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
         <asp:TableCell>
            <asp:Table ID="VJour30" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM30" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM30" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour31" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM31" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM31" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
         <asp:TableCell>
            <asp:Table ID="VJour32" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM32" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM32" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour33" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM33" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM33" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour34" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM34" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM34" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <div style="width: 3px"></div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour35" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM35" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM35" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Table ID="VJour36" runat="server" Height="36px"  Width="20px" CellPadding="0" CellSpacing="0"
                        BorderStyle="None" BorderColor="#216B68" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalAM36" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: bottom; 
                                border-bottom-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CalPM36" runat="server" Height="18px" Width="18px" BackColor="LightGray" 
                                ForeColor="#064F4D" BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; 
                                border-top-style: none" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
