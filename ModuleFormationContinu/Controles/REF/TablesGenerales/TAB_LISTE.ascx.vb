﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class TAB_LISTE
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNbZones As Integer = 5
    Private WsNomStateIde As String = "IdeFiche"
    Private WsNomStateListe As String = "ListeFiches"

    Public Property V_NomTable As String
        Get
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateIde), String)
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIde), String) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)

            ComboMvts.Items.Clear()
            MsgOperation.Text = ""
            Call PersonnaliserSaisie()
        End Set
    End Property

    Private ReadOnly Property WebCtlZone(ByVal Index As Integer) As Virtualia.Net.VZoneSaisie
        Get
            Dim IndiceI As Integer
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VZoneSaisie

            IndiceI = 0
            Do
                Ctl = WebFct.VirWebControle(Me.Saisie, "Zone", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.VZoneSaisie)
                If IsNumeric(Strings.Right(VirControle.ID, 2)) Then
                    If CInt(Strings.Right(VirControle.ID, 2)) = Index Then
                        Return VirControle
                    End If
                End If
                IndiceI += 1
            Loop
            Return Nothing
        End Get
    End Property

    Private Sub AfficherValeurSel(ByVal Index As Integer, ByVal Valeur As String)
        Dim LstTxt As List(Of String) = ListeValeurs(Index)
        If LstTxt Is Nothing Then
            Exit Sub
        End If
        For Each Chaine In LstTxt
            If Chaine = Valeur Then
                Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
                If VirControle IsNot Nothing Then
                    VirControle.ValeurSelectionnee = Chaine
                    Exit For
                End If
            End If
        Next
    End Sub

    Public Property ListeValeurs(ByVal Index As Integer) As List(Of String)
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.ValeursCombo
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As List(Of String))
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.ValeursCombo = value
            End If
        End Set
    End Property

    Private Sub FaireListe()
        Dim TableGEN As Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION = Nothing
        Dim ListeGEN As List(Of Virtualia.TablesObjet.ShemaREF.TAB_LISTE)
        Dim VCache As List(Of String)
        Dim IndiceI As Integer

        If Me.ViewState(WsNomStateListe) IsNot Nothing Then
            Me.ViewState.Remove(WsNomStateListe)
        End If
        TableGEN = WebFct.PointeurReferentiel.PointeurTablesGenerales.Fiche_Table(V_NomTable)
        If TableGEN Is Nothing Then
            ComboMvts.Items.Add("Aucune valeur")
            Exit Sub
        End If
        ListeGEN = TableGEN.ListedesValeurs
        If ListeGEN Is Nothing Then
            ComboMvts.Items.Add("Aucune valeur")
            Exit Sub
        End If
        Select Case ListeGEN.Count
            Case Is = 0
                ComboMvts.Items.Add("Aucune valeur")
            Case Is = 1
                ComboMvts.Items.Add("Une valeur")
            Case Else
                ComboMvts.Items.Add(ListeGEN.Count.ToString & " valeurs")
        End Select
        For Each Fiche In ListeGEN
            ComboMvts.Items.Add(Fiche.Valeur)
        Next

        VCache = New List(Of String)
        VCache.Add("")
        For IndiceI = 0 To ListeGEN.Count - 1
            VCache.Add(ListeGEN.Item(IndiceI).V_DonneeGrid & VI.Tild & ListeGEN.Item(IndiceI).Valeur & VI.Tild)
        Next
        Me.ViewState.Add(WsNomStateListe, VCache)

        If ListeGEN.Count = 1 Then
            V_FicheSelectionnee = ComboMvts.Items.Item(1).Text
        End If
    End Sub

    Public ReadOnly Property V_Fiche(ByVal Index As Integer) As String
        Get
            Dim VCache As List(Of String)

            VCache = CType(Me.ViewState(WsNomStateListe), List(Of String))
            If VCache Is Nothing Then
                Return ""
            End If
            If Index < VCache.Count Then
                Return VCache.Item(Index)
            Else
                Return ""
            End If
        End Get
    End Property

    Public WriteOnly Property V_FicheSelectionnee As String
        Set(ByVal value As String)
            Try
                ComboMvts.Text = value
            Catch ex As Exception
                Exit Property
            End Try
            Dim Chaine As String = V_Fiche(ComboMvts.SelectedIndex)
            If Chaine = "" Then
                Exit Property
            End If
            Dim IndiceI As Integer
            Dim TableauData(0) As String
            TableauData = Strings.Split(Chaine, VI.Tild, -1)
            For IndiceI = 0 To TableauData.Count - 3
                Donnee(IndiceI) = TableauData(IndiceI)
                If NatureDonnee(IndiceI) = "Table" Then
                    Call AfficherValeurSel(IndiceI, Donnee(IndiceI))
                End If
            Next IndiceI
            MsgOperation.Text = ""
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub TAB_LISTE_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If ComboMvts.Items.Count = 0 Then
            Call FaireListe()
        End If
    End Sub

    Public Property NombreZones As Integer
        Get
            Return WsNbZones
        End Get
        Set(value As Integer)
            WsNbZones = value
        End Set
    End Property

    Public Property V_Titre As String
        Get
            Return EtiTitre.Text
        End Get
        Set(ByVal value As String)
            EtiTitre.Text = value
        End Set
    End Property

    Public Property SiCommandesVisible As Boolean
        Get
            Return CadreCmdNewSupp.Visible
        End Get
        Set(ByVal value As Boolean)
            CadreCmdNewSupp.Visible = value
            CadreCmdOK.Visible = value
        End Set
    End Property

    Public Property SiVisible As Boolean
        Get
            Return Saisie.Visible
        End Get
        Set(ByVal value As Boolean)
            Saisie.Visible = value
        End Set
    End Property

    Public Property SiZoneVisible(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiVisible
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiVisible = value
            End If
        End Set
    End Property

    Public Property SiZoneComboEnable(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiComboEnable
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiComboEnable = value
            End If
        End Set
    End Property

    Public Property SiZoneComboVisible(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiComboVisible
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiComboVisible = value
            End If
        End Set
    End Property

    Public Property SiZoneNonSaisissable(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiNonSaisissable
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiNonSaisissable = value
            End If
        End Set
    End Property

    Public Property Etiquette(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.Etiquette
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.Etiquette = value
            End If
        End Set
    End Property

    Public Property Donnee(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.TexteSaisi
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.TexteSaisi = value
            End If
        End Set
    End Property

    Public Property NatureDonnee(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.NatureDonnee
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.NatureDonnee = value
            End If
        End Set
    End Property

    Public Property FormatDonnee(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.FormatDonnee
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.FormatDonnee = value
            End If
        End Set
    End Property

    Public Property LongueurDonnee(ByVal Index As Integer) As Integer
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.Longueur
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.Longueur = value
            End If
        End Set
    End Property

    Private Sub Zone_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles Zone00.ValeurChange, _
        Zone01.ValeurChange, Zone02.ValeurChange, Zone03.ValeurChange, Zone04.ValeurChange

        Dim IndiceK As Integer = CInt(Strings.Right(CType(sender, Virtualia.Net.VZoneSaisie).ID, 2))
        Donnee(IndiceK) = e.Valeur

    End Sub

    Private Sub ComboMvts_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ComboMvts.SelectedIndexChanged
        MsgOperation.Text = ""
        Dim Chaine As String = V_Fiche(ComboMvts.SelectedIndex)
        If Chaine = "" Then
            Exit Sub
        End If
        Dim IndiceI As Integer
        Dim TableauData(0) As String
        TableauData = Strings.Split(Chaine, VI.Tild, -1)
        For IndiceI = 0 To TableauData.Count - 3
            Donnee(IndiceI) = TableauData(IndiceI)
            If NatureDonnee(IndiceI) = "Table" Then
                Call AfficherValeurSel(IndiceI, Donnee(IndiceI))
            End If
        Next IndiceI
    End Sub

    Private Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim Chaine As System.Text.StringBuilder
        Dim IndiceI As Integer
        Dim Cretour As Boolean
        Dim CodeMaj As String = ""
        Dim ValeursLues As String = ""
        Dim TableGEN As Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION = Nothing
        Dim ListeGEN As List(Of Virtualia.TablesObjet.ShemaREF.TAB_LISTE) = Nothing

        TableGEN = WebFct.PointeurReferentiel.PointeurTablesGenerales.Fiche_Table(V_NomTable)
        If TableGEN IsNot Nothing Then
            ListeGEN = TableGEN.ListedesValeurs
        End If
        Chaine = New System.Text.StringBuilder
        Chaine.Append(VI.Tild)
        For IndiceI = 0 To NombreZones - 1
            Chaine.Append(Donnee(IndiceI) & VI.Tild)
        Next IndiceI
        If ComboMvts.SelectedIndex >= 0 Then
            If ListeGEN IsNot Nothing Then
                For Each FicheREF In ListeGEN
                    If FicheREF.Valeur = ComboMvts.SelectedItem.Text Then
                        ValeursLues = FicheREF.ContenuTable
                        CodeMaj = "M"
                        Exit For
                    End If
                Next
            End If
        End If
        If TableGEN Is Nothing Then
            TableGEN = New Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION
            TableGEN.Ide_Dossier = WebFct.PointeurGlobal.NouvelIdentifiant("TAB_DESCRIPTION")
            TableGEN.Intitule = V_NomTable
            Cretour = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurUtilisateur.V_NomdeConnexion, VI.PointdeVue.PVueGeneral, 1, TableGEN.Ide_Dossier, "C", "", TableGEN.ContenuTable)
            If Cretour = False Then
                MsgOperation.Text = "Incident au moment de la mise à jour."
                Exit Sub
            End If
        End If
        If CodeMaj = "M" Then
            Cretour = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurUtilisateur.V_NomdeConnexion, VI.PointdeVue.PVueGeneral, 2, TableGEN.Ide_Dossier, "S", ValeursLues, Chaine.ToString)
        End If
        Cretour = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurUtilisateur.V_NomdeConnexion, VI.PointdeVue.PVueGeneral, 2, TableGEN.Ide_Dossier, "C", "", Chaine.ToString)
        If Cretour = True Then
            Call WebFct.PointeurReferentiel.PointeurTablesGenerales.SupprimerItem(TableGEN.Ide_Dossier)
            Call WebFct.PointeurReferentiel.PointeurTablesGenerales.AjouterItem(TableGEN.Ide_Dossier)

            Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
            Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(V_NomTable, "")
            Call WebFct.PointeurContexte.SysRef_Listes.RemoveAll(AddressOf Predicat.RechercherTable)
        End If
        For IndiceI = 0 To NombreZones - 1
            Donnee(IndiceI) = ""
        Next IndiceI
        If Cretour = True Then
            MsgOperation.Text = "Validation effectuée"
        Else
            MsgOperation.Text = "Incident au moment de la mise à jour."
        End If

        ComboMvts.Items.Clear()
    End Sub

    Private Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Dim IndiceI As Integer
        V_FicheSelectionnee = ""
        ComboMvts.SelectedItem.Text = ""
        MsgOperation.Text = ""
        For IndiceI = 0 To NombreZones - 1
            Donnee(IndiceI) = ""
            If NatureDonnee(IndiceI) = "Table" Then
                Call AfficherValeurSel(IndiceI, Donnee(IndiceI))
            End If
        Next IndiceI
    End Sub

    Private Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Dim TableGEN As Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION = Nothing
        Dim ListeGEN As List(Of Virtualia.TablesObjet.ShemaREF.TAB_LISTE)
        Dim IndiceI As Integer
        Dim Cretour As Boolean
        Dim ValeursLues As String = ""

        If ComboMvts.SelectedItem.Text = "" Then
            Exit Sub
        End If
        TableGEN = WebFct.PointeurReferentiel.PointeurTablesGenerales.Fiche_Table(V_NomTable)
        If TableGEN Is Nothing Then
            MsgOperation.Text = ""
            Exit Sub
        End If
        ListeGEN = TableGEN.ListedesValeurs
        If ListeGEN Is Nothing Then
            MsgOperation.Text = ""
            Exit Sub
        End If
        For IndiceI = 0 To ListeGEN.Count - 1
            If ListeGEN.Item(IndiceI).Valeur = ComboMvts.SelectedItem.Text Then
                ValeursLues = ListeGEN.Item(IndiceI).ContenuTable
                Exit For
            End If
        Next IndiceI
        If ValeursLues = "" Then
            MsgOperation.Text = ""
            Exit Sub
        End If
        Cretour = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurUtilisateur.V_NomdeConnexion, VI.PointdeVue.PVueGeneral, 2, TableGEN.Ide_Dossier, "S", ValeursLues, "")
        For IndiceI = 0 To NombreZones - 1
            Donnee(IndiceI) = ""
        Next IndiceI
        If Cretour = True Then
            Call WebFct.PointeurReferentiel.PointeurTablesGenerales.SupprimerItem(TableGEN.Ide_Dossier)
            Call WebFct.PointeurReferentiel.PointeurTablesGenerales.AjouterItem(TableGEN.Ide_Dossier)

            Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
            Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(V_NomTable, "")
            Call WebFct.PointeurContexte.SysRef_Listes.RemoveAll(AddressOf Predicat.RechercherTable)

            MsgOperation.Text = "Suppression effectuée"
        End If
        ComboMvts.Items.Clear()
    End Sub

    Private Sub PersonnaliserSaisie()
        Dim UtiSession As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim Dictionnaire As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel) = Nothing
        Dim FicheGen As Virtualia.TablesObjet.ShemaREF.TAB_LISTE
        Dim LstValeurs As List(Of String) = Nothing
        Dim IndiceI As Integer
        Dim IndiceK As Integer

        FicheGen = New Virtualia.TablesObjet.ShemaREF.TAB_LISTE
        Dictionnaire = FicheGen.ListeDico

        For IndiceI = 0 To NombreZones - 1
            If IndiceI < Dictionnaire.Count Then
                Donnee(IndiceI) = ""
                SiZoneVisible(IndiceI) = Dictionnaire.Item(IndiceI).SiVisible
                SiZoneNonSaisissable(IndiceI) = False
                Etiquette(IndiceI) = Dictionnaire.Item(IndiceI).Etiquette
                NatureDonnee(IndiceI) = Dictionnaire.Item(IndiceI).NatureDonnee
                FormatDonnee(IndiceI) = Dictionnaire.Item(IndiceI).FormatDonnee
                If Strings.Left(Dictionnaire.Item(IndiceI).FormatDonnee, 5) = "Decim" Then
                    LongueurDonnee(IndiceI) = Dictionnaire.Item(IndiceI).Longueur + 1
                Else
                    If Dictionnaire.Item(IndiceI).NatureDonnee = "Date" Then
                        LongueurDonnee(IndiceI) = 10
                    Else
                        LongueurDonnee(IndiceI) = Dictionnaire.Item(IndiceI).Longueur
                    End If
                End If
                If Dictionnaire.Item(IndiceI).SiNonSaisissable = True Then
                    SiZoneNonSaisissable(IndiceI) = True
                End If
                If Dictionnaire.Item(IndiceI).NatureDonnee = "Table" Then
                    Select Case Dictionnaire.Item(IndiceI).PointdeVue
                        Case VI.PointdeVue.PVueGeneral 'Tables générales
                            If Dictionnaire.Item(IndiceI).NomTable <> "" Then
                                Dim TableGen As Virtualia.TablesObjet.ShemaREF.TAB_DESCRIPTION
                                Dim ListeGen As List(Of Virtualia.TablesObjet.ShemaREF.TAB_LISTE) = Nothing
                                TableGen = WebFct.PointeurReferentiel.PointeurTablesGenerales.Fiche_Table(Dictionnaire.Item(IndiceI).NomTable)
                                If TableGen IsNot Nothing Then
                                    ListeGen = TableGen.ListedesValeurs
                                End If
                                If ListeGen IsNot Nothing Then
                                    LstValeurs = New List(Of String)
                                    LstValeurs.Add("")
                                    For IndiceK = 0 To ListeGen.Count - 1
                                        LstValeurs.Add(ListeGen.Item(IndiceK).Valeur)
                                    Next IndiceK
                                    ListeValeurs(IndiceI) = LstValeurs
                                End If
                            End If
                        Case VI.PointdeVue.PVueEtablissement
                            Dim ListeETA As List(Of Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE)
                            ListeETA = WebFct.PointeurReferentiel.PointeurEtablissements.ListeDesEtablissements
                            If ListeETA IsNot Nothing Then
                                LstValeurs = New List(Of String)
                                LstValeurs.Add("")
                                For IndiceK = 0 To ListeETA.Count - 1
                                    LstValeurs.Add(ListeETA.Item(IndiceK).Denomination)
                                Next IndiceK
                                ListeValeurs(IndiceI) = LstValeurs
                            End If
                    End Select
                End If
                '**** Si en lecture seule
                SiZoneNonSaisissable(IndiceI) = UtiSession.SiEnLectureV3
                SiZoneComboEnable(IndiceI) = Not (UtiSession.SiEnLectureV3)
                '*****
            Else
                SiZoneVisible(IndiceI) = False
                NatureDonnee(IndiceI) = ""
            End If
        Next IndiceI

        NombreZones = Dictionnaire.Count
        SiCommandesVisible = True
    End Sub

End Class