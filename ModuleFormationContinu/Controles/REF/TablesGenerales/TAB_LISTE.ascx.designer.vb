﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class TAB_LISTE

    '''<summary>
    '''Contrôle Saisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Saisie As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreCommandes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCommandes As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle ComboMvts.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ComboMvts As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Contrôle CadreCmdNewSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdNewSupp As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeNew.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeNew As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CommandeSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeSupp As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Zone00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Zone00 As Global.Virtualia.Net.VZoneSaisie

    '''<summary>
    '''Contrôle Zone01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Zone01 As Global.Virtualia.Net.VZoneSaisie

    '''<summary>
    '''Contrôle Zone02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Zone02 As Global.Virtualia.Net.VZoneSaisie

    '''<summary>
    '''Contrôle Zone03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Zone03 As Global.Virtualia.Net.VZoneSaisie

    '''<summary>
    '''Contrôle Zone04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Zone04 As Global.Virtualia.Net.VZoneSaisie

    '''<summary>
    '''Contrôle MsgOperation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgOperation As Global.System.Web.UI.WebControls.TextBox
End Class
