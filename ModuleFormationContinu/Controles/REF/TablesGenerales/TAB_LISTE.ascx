﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TAB_LISTE.ascx.vb" Inherits="Virtualia.Net.TAB_LISTE" %>

<%@ Register src="~/Controles/Saisies/VZoneSaisie.ascx" tagname="VZone" tagprefix="Virtualia" %>

<asp:Table ID="Saisie" runat="server" CellPadding="1" CellSpacing="0" Width="700px" BorderStyle="None" 
           BorderWidth="1px" BorderColor="#B0E0D7" BackColor="#CAEBE4" style="margin-top: 20px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="30px" CellPadding="0" CellSpacing="0"
                  Width="604px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px" 
                  BorderWidth="1px" BorderStyle="NotSet">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Middle">
                            <asp:DropDownList ID="ComboMvts" runat="server" Height="22px" Width="360px"
                                AutoPostBack="true" BackColor="#E2F5F1" ForeColor="#142425"
                                style="margin-top: 2px; border-color: #7EC8BE; border-width: 2px; border-style: inset;
                                     display: table-cell; font-style: normal; ">
                            </asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Middle">
                            <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                Width="160px" CellPadding="0" CellSpacing="0"
                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                            Tooltip="Nouvelle fiche" >
                                        </asp:Button>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                            Tooltip="Supprimer la fiche" >
                                        </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>    
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Middle">
                            <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                CellPadding="0" CellSpacing="0" Visible="true" >
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                         </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>  
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>  
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Label ID="EtiTitre" runat="server" Text="" Visible="true"
                BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="3px"
                ForeColor="White" Height="25px" Width="678px"  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="margin-top: 10px; margin-left: 0px; font-style: normal;
                text-indent: 5px; text-align: center"/>   
        </asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone00" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone01" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone02" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone03" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone04" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:TextBox ID="MsgOperation" runat="server" Text="" Visible="true" AutoPostBack="false" ReadOnly="true"
                BackColor="#1C5151" ForeColor="#FF9849" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="3px"
                Height="25px" Width="678px" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="margin-top: 2px; margin-left: 0px; font-style: normal;
                text-indent: 5px; text-align: center"/>   
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>