﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VReferentielFormation
    
    '''<summary>
    '''Contrôle SystemeReference.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SystemeReference As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle MenuRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MenuRef As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Recherche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Recherche As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiAllerA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAllerA As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VAllerA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VAllerA As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CmdAllerA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdAllerA As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle PanelTreeDico.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelTreeDico As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle TreeDicoRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TreeDicoRef As Global.System.Web.UI.WebControls.TreeView
    
    '''<summary>
    '''Contrôle CelluleDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CelluleDroite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle RefVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefVirtualia As Global.Virtualia.Net.VReferentiel
End Class
