﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_Formation
    Inherits System.Web.UI.UserControl
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            If value = -1 Then
                FOR_IDENTIFICATION_1.Identifiant = 0
                Call FOR_IDENTIFICATION_1.V_CommandeNewDossier(VI.PointdeVue.PVueFormation)
                value = 0
            Else
                FOR_IDENTIFICATION_1.Identifiant = value
            End If
            HSelIde.Value = value.ToString
            FOR_DESCRIPTIF_2.Identifiant = value
            FOR_CARACTERISTIC_3.Identifiant = value
            FOR_EVALUATION_4.Identifiant = value
            FOR_SESSION_5.Identifiant = value
            FOR_COUTS_6.Identifiant = value
            FOR_FACTURE_7.Identifiant = value
            FOR_INTERVENANT_9.Identifiant = value
        End Set
    End Property

    Protected Sub Onglet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BoutonN1.Click, BoutonN2.Click, _
    BoutonN3.Click, BoutonN4.Click, BoutonN5.Click

        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        ConteneurVues.Width = New Unit(1150)
        Select Case CType(sender, Button).ID
            Case Is = "BoutonN1"
                MultiOnglets.SetActiveView(VueCaracteristique)
            Case Is = "BoutonN2"
                MultiOnglets.SetActiveView(VueDescriptif)
            Case Is = "BoutonN3"
                MultiOnglets.SetActiveView(VueSession)
            Case Is = "BoutonN4"
                MultiOnglets.SetActiveView(VueCout)
            Case Is = "BoutonN5"
                MultiOnglets.SetActiveView(VueEvaluation)
        End Select
        Call Initialiser()
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        ConteneurVues.Width = New Unit(1150)
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueCaracteristique"
                MultiOnglets.SetActiveView(VueCaracteristique)
            Case "VueDescriptif"
                MultiOnglets.SetActiveView(VueDescriptif)
            Case "VueSession"
                MultiOnglets.SetActiveView(VueSession)
            Case "VueCout"
                MultiOnglets.SetActiveView(VueCout)
            Case "VueEvaluation"
                MultiOnglets.SetActiveView(VueEvaluation)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(0) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        ConteneurVues.Width = New Unit(1150)
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueCaracteristique"
                Select Case e.ObjetAppelant
                    Case 1
                        FOR_IDENTIFICATION_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case 3
                        FOR_CARACTERISTIC_3.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueCaracteristique)
            Case "VueDescriptif"
                Select Case e.ObjetAppelant
                    Case 9
                        FOR_INTERVENANT_9.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueDescriptif)
            Case "VueSession"
                Select Case e.ObjetAppelant
                    Case 5
                        FOR_SESSION_5.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueSession)
            Case "VueCout"
                Select Case e.ObjetAppelant
                    Case 6
                        FOR_COUTS_6.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case 7
                        FOR_FACTURE_7.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueCout)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(0) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles FOR_IDENTIFICATION_1.AppelTable, FOR_CARACTERISTIC_3.AppelTable, FOR_SESSION_5.AppelTable, _
    FOR_COUTS_6.AppelTable, FOR_FACTURE_7.AppelTable, FOR_INTERVENANT_9.AppelTable

        Select Case e.ObjetAppelant
            Case 1, 3
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueCaracteristique.ID
            Case 9
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDescriptif.ID
            Case 5
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSession.ID
            Case 6, 7
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueCout.ID
            Case Else
                Exit Sub
        End Select

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#BFB8AB")
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Private Sub Initialiser()
        Dim Ctl As Control
        Dim BtnControle As System.Web.UI.WebControls.Button
        Dim IndiceI As Integer = 0
        Dim K As Integer = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreOnglets, "Bouton", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            BtnControle = CType(Ctl, System.Web.UI.WebControls.Button)
            K = CInt(Strings.Right(BtnControle.ID, 1)) - 1
            If K = MultiOnglets.ActiveViewIndex Then
                BtnControle.Font.Bold = True
                BtnControle.Height = New Unit(24)
                BtnControle.Width = New Unit(135)
                BtnControle.BackColor = WebFct.ConvertCouleur("#7D9F99")
                BtnControle.ForeColor = System.Drawing.Color.White
            Else
                BtnControle.Font.Bold = False
                BtnControle.Height = New Unit(33)
                BtnControle.Width = New Unit(148)
                BtnControle.BackColor = System.Drawing.Color.Transparent
                BtnControle.ForeColor = WebFct.ConvertCouleur("#142425")
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
        End If
    End Sub

    Protected Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        ConteneurVues.Width = New Unit(1150)
        MultiOnglets.SetActiveView(VueCaracteristique)
        Call Initialiser()
        RaiseEvent RetourEventHandler(Me, e)
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles FOR_IDENTIFICATION_1.MessageDialogue, FOR_COUTS_6.MessageDialogue, FOR_FACTURE_7.MessageDialogue, FOR_INTERVENANT_9.MessageDialogue, _
    FOR_SESSION_5.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        ConteneurVues.Width = New Unit(1150)
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        PopupMsg.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case Is = "MajDossier"
                If e.NumeroObjet = 1 And e.ReponseMsg = "OK" Then
                    MultiOnglets.SetActiveView(VueDescriptif)
                    RaiseEvent RetourEventHandler(Me, e)
                End If
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case 5
                        FOR_SESSION_5.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiOnglets.SetActiveView(VueSession)
                    Case 6
                        FOR_COUTS_6.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiOnglets.SetActiveView(VueCout)
                    Case 7
                        FOR_FACTURE_7.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiOnglets.SetActiveView(VueCout)
                    Case 9
                        FOR_INTERVENANT_9.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiOnglets.SetActiveView(VueDescriptif)
                End Select
        End Select
        ConteneurVues.Width = New Unit(1150)
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
    End Sub

End Class
