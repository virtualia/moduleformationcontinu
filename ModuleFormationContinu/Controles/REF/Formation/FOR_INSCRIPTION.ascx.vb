﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes

Public Class FOR_INSCRIPTION
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsPvue As Integer = VI.PointdeVue.PVueFormation
    Private WsNumObjet As Integer = 8
    Private WsNomTable As String = "FOR_INSCRIPTIONS"
    Private WsFiche As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS

    Private Const PERSONALISATION As Integer = 28
    Private Const COUT_PEDA As Integer = 8
    Private Const COUT_DEPLA As Integer = 9
    Private Const COUT_RESTAU As Integer = 10
    Private Const COUT_HEBER As Integer = 11
    Private Const NB_HEURE As Integer = 12
    Private Const NB_HEURE_DEPLA As Integer = 13
    Private Const COUT_FORFAIT As Integer = 25
    Private Const SUIVI As Integer = 17
    Private Const MOTIF As Integer = 18

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant(ByVal RangInscription As Integer) As Integer
        Set(ByVal value As Integer)
            Call InitialiserControles()
            Dim ColHisto As New List(Of Integer)
            ColHisto.Add(1)
            ColHisto.Add(14)
            V_CacheColHisto = ColHisto
            V_Identifiant = value
            V_ActualiserObjet(value)
            V_Occurence = CStr(RangInscription)
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirDonneeTable.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
            Else
                If CacheDonnee.Item(NumInfo).ToLower = "oui" Then
                    VirCoche.V_Check = True
                Else
                    VirCoche.V_Check = False
                End If
            End If
            If NumInfo = 28 Then
                VirCoche.V_SiEnLectureSeule = Not (CacheDonnee.Item(NumInfo).ToLower = "oui") 'Inscription personnalisee
            Else
                VirCoche.V_SiEnLectureSeule = True
            End If
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirVertical.DonText = ""
            Else
                VirVertical.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirVertical.V_SiEnLectureSeule = True
            VirVertical.V_SiAutoPostBack = False
            IndiceI += 1
        Loop
    End Sub

    Public Sub GereRetourMessage(ByVal RetourMsg As Virtualia.OutilsVisu.Formation.CacheRetourMessage)
        If RetourMsg.TypeMessage <> Virtualia.OutilsVisu.Formation.TypeCommandeMessage.OUI Then
            Exit Sub
        End If
        If RetourMsg.Commande = Virtualia.OutilsVisu.Formation.TypeCommandeMessage.OUI Then
            Call V_ValeurChange(28, "Non")
            Coche28.V_SiEnLectureSeule = True
            Call V_MajFiche()
            Call ImpacterInscriptions()
        End If
    End Sub

    Private Sub Coche28_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles Coche28.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim LstMsg As List(Of String) = New List(Of String)
        LstMsg.Add("La dépersonnalisation va entrainer : " & vbCrLf & vbTab & "- Le recalcul des coûts," & vbCrLf & vbTab & "- La mise à jour de l'action de formation," & vbCrLf & vbTab & "- La mise à jour du nombre d'heures," & vbCrLf & vbTab & "- La mise à jour de la filière" & vbCrLf & vbCrLf & "Voulez-vous continuer ?")
        Call V_MessageDialog(V_WebFonction.Evt_MessageConfirmation(WsNumObjet, "Dépersonnalisation", LstMsg))
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_PointdeVue = WsPvue
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        If Me.V_SiEnLectureSeule = True Then
            CadreCmdOK.Visible = False
            Call InitialiserControles()
        End If
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            VirVertical.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Sub ImpacterInscriptions()
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Dim IntituleAncien As String = ""
        Dim LstSessions As List(Of Virtualia.TablesObjet.ShemaREF.FOR_SESSION)

        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier IsNot Nothing Then
            If Dossier.SiadesInscriptions = False Then
                Exit Sub
            End If
        End If
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        LstSessions = Dossier.ListeDesFORSessions
        If LstSessions Is Nothing Then
            Exit Sub
        End If
        For Each FicheFOR In LstSessions
            Call Dossier.VerifierInscriptions("", FicheFOR.Date_de_Valeur, FicheFOR.Rang)
        Next
    End Sub
End Class

