﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Public Class CtlPlanningFormation
    Inherits System.Web.UI.UserControl
    Private WsActionArmoireChange As Action(Of String)
    Private WsActionIdeChange As Action(Of Integer)
    Public Property IDAppelant As String
        Get
            Return CalendrierDyna.IDAppelant
        End Get
        Set(value As String)
            CalendrierDyna.IDAppelant = value
        End Set
    End Property
    Private Sub CtlPlanningFormation_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Call ChargerListeChoix()
        End If
    End Sub
    Private Sub CtlPlanningFormation_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Select Case IDAppelant
            Case "FOR"
                CboTypeArmoire.SelectedValue = "CALENDRIER"
            Case "PER"
                CboTypeArmoire.SelectedValue = "DEMANDES"
        End Select
    End Sub
    Public WriteOnly Property ArmoireChange As Action(Of String)
        Set(value As Action(Of String))
            WsActionArmoireChange = value
        End Set
    End Property
    Public WriteOnly Property IdeChange As Action(Of Integer)
        Set(value As Action(Of Integer))
            WsActionIdeChange = value
        End Set
    End Property
    Protected Sub CboTypeArmoire_SelectedIndexChanged(sender As Object, e As EventArgs)
        If WsActionArmoireChange IsNot Nothing Then
            WsActionArmoireChange(DirectCast(sender, DropDownList).SelectedValue)
        End If
    End Sub
    Private Sub CalendrierDyna_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles CalendrierDyna.ValeurChange
        If WsActionIdeChange IsNot Nothing Then
            WsActionIdeChange(CInt(e.Valeur))
        End If
    End Sub

    Private Sub ChargerListeChoix()
        If CboTypeArmoire.Items.Count > 0 Then
            Exit Sub
        End If

        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste des stages pour lesquels les demandes de formation validées ne sont pas traitées", "STAGE_DEMANDES_VALIDEES"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par plan, domaine, thème des stages des 3 dernières années", "PLAN_DERNIERE_N_ANNEE"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par plan, domaine, thème de tous les stages", "PLAN_TOUS"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par établissement, plan, domaine, thème des stages des 3 dernières années", "ETAB_PLAN_TOUS"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste alphabétique de tous les stages", "STAGE_TOUS"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par plan des stages ayant des sessions à venir", "PLAN_STAGE_SESSION_A_VENIR"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par plan, domaine, thème des stages sans session", "PLAN_STAGE_SANS_SESSION"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par plan des stages sans inscrit", "PLAN_STAGE_SANS_INSCRIT"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Liste organisée par plan des stages pour lesquels les inscrits n'ont pas été convoqués", "PLAN_STAGE_SANS_CONVOCATION"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Le calendrier des demandes de formation", "DEMANDES"))
        CboTypeArmoire.Items.Add(New System.Web.UI.WebControls.ListItem("Le calendrier des sessions", "CALENDRIER"))

    End Sub

End Class