﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPvueFOR.ascx.vb" Inherits="Virtualia.Net.CtlPvueFOR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VMenuCommun.ascx" tagname="VMenu" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_IDENTIFICATION.ascx" tagname="FOR_IDENTIFICATION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_DESCRIPTIF.ascx" tagname="FOR_DESCRIPTIF" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_CARACTERISTIC.ascx" tagname="FOR_CARACTERISTIC" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_SESSION.ascx" tagname="FOR_SESSION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_COUTS.ascx" tagname="FOR_COUTS" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_FACTURE.ascx" tagname="FOR_FACTURE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_INTERVENANT.ascx" tagname="FOR_INTERVENANT" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_EVALUATION.ascx" tagname="FOR_EVALUATION" tagprefix="Virtualia" %>

<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1140px" BackColor="#5E9598">
        <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left" Width="200px">
                <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="SITUATION EN GESTION" Height="44px" Width="700px"
                        BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                        style="font-style: normal; text-indent: 5px; text-align: center;">
                </asp:Label>     
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <Virtualia:VMenu ID="FOR_MENU" runat="server" V_Appelant="FOR"></Virtualia:VMenu>
        </asp:TableCell>
        <asp:TableCell ID="CelluleFOR" runat="server" Width="800px" VerticalAlign="Top">
            <asp:MultiView ID="MultiFOR" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueIdentification" runat="server">
                    <Virtualia:FOR_IDENTIFICATION ID="FOR_IDENTIFICATION_1" runat="server" CadreStyle="margin-top: 10px"  /> 
                </asp:View>
                <asp:View ID="VueCaracteristique" runat="server">
                    <Virtualia:FOR_CARACTERISTIC ID="FOR_CARACTERISTIC_3" runat="server" CadreStyle="margin-top: 10px"  />  
                </asp:View>
                <asp:View ID="VueDescriptif" runat="server">
                    <Virtualia:FOR_DESCRIPTIF ID="FOR_DESCRIPTIF_2" runat="server" CadreStyle="margin-top: 10px"  />
                </asp:View>
                <asp:View ID="VueIntervenant" runat="server">
                    <Virtualia:FOR_INTERVENANT ID="FOR_INTERVENANT_9" runat="server" CadreStyle="margin-top: 10px"  />
                </asp:View>
                <asp:View ID="VueSession" runat="server">
                    <Virtualia:FOR_SESSION ID="FOR_SESSION_5" runat="server" CadreStyle="margin-top: 10px" />  
                </asp:View>
                <asp:View ID="VueCout" runat="server">
                    <Virtualia:FOR_COUTS ID="FOR_COUTS_6" runat="server" CadreStyle="margin-top: 10px" />
                </asp:View>
                <asp:View ID="VueFacture" runat="server">
                    <Virtualia:FOR_FACTURE ID="FOR_FACTURE_7" runat="server" CadreStyle="margin-top: 10px" /> 
                </asp:View>
                <asp:View ID="VueEvaluation" runat="server">
                    <Virtualia:FOR_EVALUATION ID="FOR_EVALUATION_4" runat="server"  CadreStyle="margin-top: 10px" />  
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellMessage" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Virtualia:VMessage id="MsgVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell>
              <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
          </asp:TableCell>
      </asp:TableRow>

</asp:Table>
    