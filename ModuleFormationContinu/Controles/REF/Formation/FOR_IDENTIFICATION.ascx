﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_IDENTIFICATION" Codebehind="FOR_IDENTIFICATION.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
  BorderColor="#FFEBC8" Height="410px" Width="680px" HorizontalAlign="Center">
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px" >
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                     <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                         BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                         Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                         BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                     </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Identification du stage" Height="20px" Width="300px"
                        BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#FFF2DB"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;" >
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreDon" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" >
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="1" V_SiDonneeDico="true"
                       EtiWidth="100px" DonWidth="550px" DonTabIndex="1"/>
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="15px"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauReference" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="650px" > 
            <asp:TableRow > 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="6" V_SiDonneeDico="true"
                       EtiWidth="100px" DonWidth="130px" DonTabIndex="2" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="12" V_SiDonneeDico="true"
                       EtiWidth="130px" DonWidth="60px" DonTabIndex="3" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" >  
                    <asp:Label ID="LabelHeures" runat="server" Text="heures" Height="20px" Width="50px"
                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                        ForeColor="#A67F3B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style: oblique; text-indent: 2px; text-align: left;" >
                    </asp:Label> 
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDate ID="InfoD08" runat="server" TypeCalendrier="Standard" EtiWidth="103px"
                               V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="1" V_Information="8" DonTabIndex="4"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" Columnspan="4"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadrePlan" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" >
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="5" V_SiDonneeDico="true"
                       EtiWidth="130px" DonWidth="450px" DonTabIndex="5"
                       Etistyle="margin-left: 20px;" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="3" V_SiDonneeDico="true"
                       EtiWidth="130px" DonWidth="450px" DonTabIndex="6"
                       Etistyle="margin-left: 20px;" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="4" V_SiDonneeDico="true"
                       EtiWidth="130px" DonWidth="450px" DonTabIndex="7" 
                       Etistyle="margin-left: 20px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab16" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="16" V_SiDonneeDico="true"
                       EtiWidth="130px" EtiText="Sous-thème" DonWidth="450px" DonTabIndex="8" 
                       Etistyle="margin-left: 20px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="10px"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauOrganisme" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="668px" > 
            <asp:TableRow > 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="2" V_SiDonneeDico="true"
                       EtiWidth="80px" DonWidth="280px" DonTabIndex="9" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDate ID="InfoD07" runat="server" TypeCalendrier="Standard" EtiWidth="140px"
                               V_SiDonneeDico="true" V_PointdeVue="9" V_Objet="1" V_Information="7" DonTabIndex="10"/>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="13" V_SiDonneeDico="true"
                       EtiWidth="45px" DonWidth="35px" DonTabIndex="11" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="10px" Columnspan="3"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauCursus" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="530px" >
            <asp:TableRow > 
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="9" V_SiDonneeDico="true"
                       EtiWidth="100px" DonWidth="350px" DonTabIndex="12"
                       Etistyle="margin-left: 20px;" />
                 </asp:TableCell>
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="10" V_SiDonneeDico="true"
                       EtiWidth="25px" DonWidth="25px" DonTabIndex="13" />
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left" Columnspan="2">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="11" V_SiDonneeDico="true"
                       EtiWidth="100px" DonWidth="350px" DonTabIndex="14"
                       Etistyle="margin-left: 20px;" />
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="15px" Columnspan="2"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreURL" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="680px" >
            <asp:TableRow>
                <asp:TableCell Height="5px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="14" V_SiDonneeDico="true"
                       EtiWidth="60px" DonWidth="500px" DonTabIndex="15" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="2px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab15" runat="server"
                       V_PointdeVue="9" V_Objet="1" V_Information="15" V_SiDonneeDico="true"
                       EtiWidth="200px" DonWidth="280px" DonTabIndex="16" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>