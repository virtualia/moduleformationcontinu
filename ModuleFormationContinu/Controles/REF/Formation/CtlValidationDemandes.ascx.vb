﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlValidationDemandes
    Inherits System.Web.UI.UserControl
    Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageDialogue As MsgDialog_MsgEventHandler
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.CacheValidationDemande
    Private WsNomStateCache As String = "ValiderDemande"

    Protected Overridable Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageDialogue(Me, e)
    End Sub
    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub
    Private Property CacheVirControle As Virtualia.Net.CacheValidationDemande
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.CacheValidationDemande)
            End If
            Dim NewCache As Virtualia.Net.CacheValidationDemande
            NewCache = New Virtualia.Net.CacheValidationDemande
            Return NewCache
        End Get
        Set(value As Virtualia.Net.CacheValidationDemande)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property V_Identifiant As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.IdentifiantStage
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.IdentifiantStage = value Then
                Exit Property
            End If
            WsCtl_Cache.IdentifiantStage = value
            CacheVirControle = WsCtl_Cache
            '*** Lecture du dossier complet
            Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
            Dim Dossier As Virtualia.Metier.Formation.DossierStage
            Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
            Ensemble.Identifiant = value
            Dossier = Ensemble.ItemDossier(value)
            If Dossier IsNot Nothing Then
                EtiTitre.Text = Dossier.Intitule_Stage
            Else
                EtiTitre.Text = ""
            End If
            '**
            EtiSousTitre.Text = ""
            Call Charger_Sessions()
        End Set
    End Property

    Private Property V_ListeIdentifiants_PER As List(Of Integer)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.ListeIdentifiantsPER
        End Get
        Set(ByVal value As List(Of Integer))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.ListeIdentifiantsPER = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Private ReadOnly Property V_ChoixListe As String
        Get
            WsCtl_Cache = CacheVirControle
            Select Case WsCtl_Cache.Type_Liste
                Case 0
                    Return ""
                Case 1
                    Return "Accord"
                Case Else
                    Return "Refus"
            End Select
        End Get
    End Property
    Private Property V_DateNoeud As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.DateSession
        End Get
        Set(value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.DateSession = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_CocheAll As Boolean
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.SiCocheAllCochee
        End Get
        Set(value As Boolean)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.SiCocheAllCochee = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub CocheAll_CheckedChanged(sender As Object, e As EventArgs) Handles CocheAll.CheckedChanged
        V_CocheAll = CocheAll.Checked
        For Each element As ListItem In ListeValidation.Items
            element.Selected = CocheAll.Checked
        Next
    End Sub

    Private Sub Charger_Sessions()
        Dim Ide_Dossier As Integer = V_Identifiant
        If Ide_Dossier = 0 Then
            Exit Sub
        End If
        Dim LstIntranet As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION) = Nothing
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Dim IntituleStage As String = ""
        Dim Rupture As String = "Aucun"
        Dim NewNoeud As TreeNode
        Dim VImage As String = ""
        Dim LstIdePER As List(Of Integer)
        Dim SuiteTestee As String = ""

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        TreeListeSessions.Nodes.Clear()
        TreeListeSessions.MaxDataBindDepth = 1
        TreeListeSessions.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.BulletedList4
        ListeValidation.Items.Clear()
        LstIntranet = ListeDesDemandesdeFormation(Dossier.Intitule_Stage)
        If LstIntranet Is Nothing Then
            Exit Sub
        End If
        LstIdePER = New List(Of Integer)
        For Each FichePER In LstIntranet
            If FichePER.Date_Session <> Rupture Then
                VImage = "~/Images/Armoire/BleuStandardFermer16.bmp"
                Select Case FichePER.Date_Session
                    Case ""
                        NewNoeud = New TreeNode("Demandes formulées hors session", "09/09/1999")
                    Case Else
                        NewNoeud = New TreeNode("Session du " & FichePER.Date_Session, FichePER.Date_Session)
                End Select
                NewNoeud.ImageUrl = VImage
                NewNoeud.PopulateOnDemand = False
                NewNoeud.SelectAction = TreeNodeSelectAction.Select
                TreeListeSessions.Nodes.Add(NewNoeud)
                Rupture = FichePER.Date_Session
            End If
        Next
        NewNoeud = Nothing
        If V_DateNoeud = "" Then
            NewNoeud = TreeListeSessions.Nodes.Item(0)
            NewNoeud.Selected = True
            V_DateNoeud = NewNoeud.Value
        Else
            For Each Noeud As TreeNode In TreeListeSessions.Nodes
                If Noeud.Value = V_DateNoeud Then
                    NewNoeud = Noeud
                    Exit For
                End If
            Next
        End If
        If NewNoeud Is Nothing Then
            Exit Sub
        End If
        LstIdePER = New List(Of Integer)
        For Each FichePER In LstIntranet
            If FichePER.Date_Session = NewNoeud.Value Or NewNoeud.Value = "09/09/1999" Then
                SuiteTestee = FichePER.SuiteDonnee
                Select Case SuiteTestee
                    Case "Accord", "Refus"
                        Exit Select
                    Case Else
                        SuiteTestee = ""
                End Select
                If SuiteTestee = V_ChoixListe Then
                    LstIdePER.Add(FichePER.Ide_Dossier)
                End If
            End If
        Next
        V_ListeIdentifiants_PER = LstIdePER
        If LstIdePER Is Nothing OrElse LstIdePER.Count = 0 Then
            Exit Sub
        End If
        Call FaireListeAgents(NewNoeud, LstIdePER)
    End Sub

    Private Function ListeDesDemandesdeFormation(ByVal IntituleStage As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
        Dim LstRequete As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim LstResultat As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
        Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
        Dim InfoSel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
        Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)()
        Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim Rupture As String = ""
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Dim LstFiltres As List(Of String)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        LstFiltres = Requeteur.ListeConfidentialite

        ObjetStructure.SiForcerClauseDistinct = False
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.SiHistoriquedeSituation = True
        ObjetStructure.SiSelectMin_PlutotQue_Max = True
        ObjetStructure.SiPasdeTriSurIdeDossier = True

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 1)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
        InfoSel.Valeurs_AComparer = (New String() {IntituleStage}).ToList()
        LstSel.Add(InfoSel)

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 14)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
        InfoSel.Valeurs_AComparer = (New String() {"Avis favorable"}).ToList()
        LstSel.Add(InfoSel)

        For Each Info In WebFct.PointeurGlobal.VirListeInfosDico
            If Info.PointdeVue = VI.PointdeVue.PVueApplicatif And Info.Objet = VI.ObjetPer.ObaDemandeFormation Then
                Select Case Info.Information
                    Case 2
                        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 1))
                    Case Else
                        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 0))
                End Select
            End If
        Next

        Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WebFct.PointeurGlobal.VirModele, WebFct.PointeurGlobal.VirInstanceBd)
        Constructeur.StructureRequete = ObjetStructure
        Constructeur.ListeInfosASelectionner = LstSel
        Constructeur.ListeInfosAExtraire = LstExt
        If LstFiltres IsNot Nothing AndAlso LstFiltres.Count > 0 Then
            For Each Filtre In LstFiltres
                Constructeur.ClauseFiltreIN = Filtre
            Next
        End If

        LstRequete = WebFct.PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WebFct.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDemandeFormation,
                                                                                  Constructeur.OrdreSqlDynamique)
        If LstRequete Is Nothing Then
            Return Nothing
        End If
        LstResultat = New List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
        For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION In LstRequete
            LstResultat.Add(Fiche)
        Next
        If LstResultat.Count = 0 Then
            Return Nothing
        End If
        Return LstResultat
    End Function

    Private Function UneDemandedeFormation(ByVal Ide As Integer, ByVal IntituleStage As String, ByVal DateSession As String) As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION
        Dim LstRequete As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim LstResultat As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
        Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
        Dim InfoSel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
        Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)()
        Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim Rupture As String = ""

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If

        ObjetStructure.SiForcerClauseDistinct = False
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.SiHistoriquedeSituation = True
        ObjetStructure.SiSelectMin_PlutotQue_Max = True
        ObjetStructure.SiPasdeTriSurIdeDossier = True
        ObjetStructure.IdentifiantDossier = Ide

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 1)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
        InfoSel.Valeurs_AComparer = (New String() {IntituleStage}).ToList()
        LstSel.Add(InfoSel)

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 14)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
        InfoSel.Valeurs_AComparer = (New String() {"Avis favorable"}).ToList()
        LstSel.Add(InfoSel)

        For Each Info In WebFct.PointeurGlobal.VirListeInfosDico
            If Info.PointdeVue = VI.PointdeVue.PVueApplicatif And Info.Objet = VI.ObjetPer.ObaDemandeFormation Then
                Select Case Info.Information
                    Case 2
                        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 1))
                    Case Else
                        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 0))
                End Select
            End If
        Next

        Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WebFct.PointeurGlobal.VirModele, WebFct.PointeurGlobal.VirInstanceBd)
        Constructeur.StructureRequete = ObjetStructure
        Constructeur.ListeInfosASelectionner = LstSel
        Constructeur.ListeInfosAExtraire = LstExt

        LstRequete = WebFct.PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WebFct.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDemandeFormation,
                                                                                  Constructeur.OrdreSqlDynamique)
        If LstRequete Is Nothing Then
            Return Nothing
        End If
        LstResultat = New List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
        For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION In LstRequete
            Select Case DateSession
                Case ""
                    Return Fiche
                Case Else
                    If Fiche.Date_Session = DateSession Then
                        Return Fiche
                    End If
            End Select
            LstResultat.Add(Fiche)
        Next
        Return Nothing
    End Function

    Private Function ListeAgentsViaIntranet(ByVal LstIde As List(Of Integer)) As List(Of Virtualia.Metier.Formation.IdentitePersonne)
        Dim LstResultat As List(Of ServiceServeur.VirRequeteType)
        Dim LstAgents As List(Of Virtualia.Metier.Formation.IdentitePersonne) = Nothing
        Dim AgentDemandeur As Virtualia.Metier.Formation.IdentitePersonne
        Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
        Dim InfoSel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
        Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)()
        Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim Rupture As String = ""

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaCivil, 2)
        LstSel.Add(InfoSel)
        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 0))
        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 0))
        ObjetStructure.SiForcerClauseDistinct = False
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.SiHistoriquedeSituation = False
        ObjetStructure.SiPasdeTriSurIdeDossier = False

        Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WebFct.PointeurGlobal.VirModele, WebFct.PointeurGlobal.VirInstanceBd)
        Constructeur.StructureRequete = ObjetStructure
        Constructeur.ListeInfosASelectionner = LstSel
        Constructeur.ListeInfosAExtraire = LstExt
        Constructeur.PreselectiondIdentifiants = LstIde

        LstResultat = WebFct.PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WebFct.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDemandeFormation,
                                                                                  Constructeur.OrdreSqlDynamique)
        If LstResultat Is Nothing Then
            Return Nothing
        End If
        LstAgents = New List(Of Virtualia.Metier.Formation.IdentitePersonne)
        For Each Resultat In LstResultat
            AgentDemandeur = New Virtualia.Metier.Formation.IdentitePersonne
            AgentDemandeur.Ide = Resultat.Ide_Dossier
            AgentDemandeur.Nom = Resultat.Valeurs(0)
            AgentDemandeur.Prenom = Resultat.Valeurs(1)
            LstAgents.Add(AgentDemandeur)
        Next
        Return LstAgents
    End Function

    Private Sub FaireListeAgents(ByVal Noeud As TreeNode, ByVal Optional LstIdePER As List(Of Integer) = Nothing)
        Dim LstAgents As List(Of Virtualia.Metier.Formation.IdentitePersonne)
        ListeValidation.Items.Clear()
        EtiSousTitre.Text = Noeud.Text
        If LstIdePER Is Nothing Then
            LstIdePER = V_ListeIdentifiants_PER
        End If
        If LstIdePER Is Nothing Then
            Exit Sub
        End If
        LstAgents = ListeAgentsViaIntranet(LstIdePER)
        If LstAgents Is Nothing Then
            Exit Sub
        End If
        For Each Agent In LstAgents
            ListeValidation.Items.Add(New ListItem(Agent.Nom & Strings.Space(1) & Agent.Prenom, CStr(Agent.Ide)))
        Next
        WsCtl_Cache = CacheVirControle
        For Each element As ListItem In ListeValidation.Items
            element.Selected = WsCtl_Cache.SiCocheAllCochee
        Next
    End Sub
    Private Sub TreeListeSessions_SelectedNodeChanged(sender As Object, e As EventArgs) Handles TreeListeSessions.SelectedNodeChanged
        V_DateNoeud = CType(sender, TreeView).SelectedValue
        Call Charger_Sessions()
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        If ListeValidation.Items.Count = 0 Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        WsCtl_Cache = CacheVirControle
        Dim Cretour As Boolean
        Dim SiCRC As Boolean = False
        Dim DemandePER As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION
        Dim LstIdeOK As List(Of Integer) = New List(Of Integer)
        Dim IdeChef As Integer
        Dim IdeNPlusUn As Integer
        Dim ObjetMail As Virtualia.Net.Session.ObjetMessagerie
        Dim TexteMsg As String = ""
        Dim LstCrendu As List(Of String) = New List(Of String)

        SiCRC = False
        If WebFct.PointeurGlobal.VirModele.InstanceProduit.NumeroLicence = "9908004" Then 'Cour des Comptes
            SiCRC = WebFct.PointeurUtilisateur.Etablissement.StartsWith("CRC")
        End If
        For Each Element As ListItem In ListeValidation.Items
            If Element.Selected = True Then
                DemandePER = UneDemandedeFormation(CInt(Element.Value), EtiTitre.Text, V_DateNoeud)
                If DemandePER IsNot Nothing Then
                    DemandePER.SuiteDonnee = WsCtl_Cache.SuiteDonnee
                    Cretour = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                      VI.ObjetPer.ObaDemandeFormation, DemandePER.Ide_Dossier, "M", DemandePER.FicheLue, DemandePER.ContenuTable)
                    If Cretour = True Then
                        LstIdeOK = New List(Of Integer)
                        LstIdeOK.Add(DemandePER.Ide_Dossier)
                        IdeChef = WebFct.PointeurGlobal.VirIdentifiantManager(DemandePER.Ide_Dossier, 0)
                        If IdeChef > 0 Then
                            LstIdeOK.Add(IdeChef)
                        End If
                        If SiCRC = False Then
                            IdeNPlusUn = WebFct.PointeurGlobal.VirIdentifiantManager(IdeChef, 0)
                            If IdeNPlusUn > 0 Then
                                LstIdeOK.Add(IdeNPlusUn)
                            End If
                        End If
                        TexteMsg = "A l'attention de " & Element.Text & vbCrLf
                        TexteMsg &= "Bonjour, " & vbCrLf
                        TexteMsg &= "Votre demande d'inscription au stage : " & EtiTitre.Text & " du " & WsCtl_Cache.DateSession & " a reçu une réponse "
                        Select Case WsCtl_Cache.SuiteDonnee
                            Case "Accord"
                                TexteMsg &= "favorable de votre correspondant formation."
                                TexteMsg &= vbCrLf & "En conséquence votre demande a été transmise à l'organisateur de la formation."
                                TexteMsg &= vbCrLf & "Attention : ce présent courriel ne vaut pas inscription. Celle-ci vous sera confirmée par l'envoi d'une convocation."
                            Case "Refus"
                                TexteMsg &= "défavorable par le correspondant formation."
                        End Select
                        ObjetMail = New Virtualia.Net.Session.ObjetMessagerie(WebFct.PointeurGlobal, "", LstIdeOK, "Demande de formation")
                        Cretour = ObjetMail.EnvoyerEMail(WebFct.PointeurUtilisateur.V_NomdeConnexion, "Demande d'inscription à une formation", TexteMsg, SiCRC)
                        If Cretour = True Then
                                LstCrendu.Add(" - un email a été envoyé à " & Element.Text & " et son responsable hiérarchique")
                            Else
                                LstCrendu.Add(" - un email n'a pu être envoyé à " & Element.Text & " et son responsable hiérarchique")
                            End If
                        End If
                    End If
            End If
        Next
        Call Charger_Sessions()
        If LstCrendu.Count = 0 Then
            Exit Sub
        End If
        TexteMsg = ""
        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Dim I As Integer
        For I = 0 To LstCrendu.Count - 1
            TexteMsg &= LstCrendu.Item(I) & vbCrLf
        Next I
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Demandes", 0, "", "OK", "Validation des demandes de formation", TexteMsg)
        V_MessageDialog(Evenement)
    End Sub

    Private Sub SuiteDonnee_CheckedChanged(sender As Object, e As EventArgs) Handles RadioAccord.CheckedChanged, RadioRefus.CheckedChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.SuiteDonnee = CType(sender, RadioButton).ID.Replace("Radio", "")
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub TypeListe_CheckedChanged(sender As Object, e As EventArgs) Handles RadioV0.CheckedChanged, RadioV1.CheckedChanged, RadioV2.CheckedChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Type_Liste = CInt(Strings.Right(CType(sender, RadioButton).ID, 1))
        CacheVirControle = WsCtl_Cache
        Call Charger_Sessions()
    End Sub

    Private Sub ListeValidation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeValidation.SelectedIndexChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.SiCocheAllCochee = False
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("STAGE", 0, "", "")
        ReponseRetour(Evenement)
    End Sub
End Class