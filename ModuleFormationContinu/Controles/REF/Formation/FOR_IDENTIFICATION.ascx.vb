﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_FOR_IDENTIFICATION
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsPvue As Integer = VI.PointdeVue.PVueFormation
    Private WsNumObjet As Integer = 1
    Private WsNomTable As String = "FOR_IDENTIFICATION"
    Private WsFiche As Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                Call InitialiserControles()
                V_Identifiant = value
                '*** Lecture du dossier complet
                Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
                Dim Dossier As Virtualia.Metier.Formation.DossierStage
                Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
                Ensemble.Identifiant = V_Identifiant
                Dossier = Ensemble.ItemDossier(value)
                '**
            End If
        End Set
    End Property
    Public ReadOnly Property IntituleStage As String
        Get
            Return V_CacheMaj(1)
        End Get
    End Property
    Public ReadOnly Property ReferenceDuStage As String
        Get
            Return V_CacheMaj(6)
        End Get
    End Property
    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = Not (CommandeOK.Visible)
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_PointdeVue = WsPvue
        V_Objet(0) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        If Me.V_SiEnLectureSeule = True Then
            CadreCmdOK.Visible = False
            Call InitialiserControles()
        End If
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelHeures.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD07.ValeurChange, InfoD08.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange,
    InfoH06.ValeurChange, InfoH10.ValeurChange, InfoH12.ValeurChange,
    InfoH13.ValeurChange, InfoH14.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab02.AppelTable,
    Dontab03.AppelTable, Dontab04.AppelTable, Dontab05.AppelTable, Dontab09.AppelTable, Dontab11.AppelTable, Dontab15.AppelTable, Dontab16.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee(NumInfo).ToString <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim LstErreurs As List(Of String) = SiMajPossible()
        Dim TitreMsg As String

        If LstErreurs IsNot Nothing Then
            Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Mise à jour impossible", LstErreurs))
            Exit Sub
        End If
        If V_CacheMaj.Item(15) = "" And V_WebFonction.PointeurUtilisateur.Etablissement <> "" Then
            Call V_ValeurChange(15, V_WebFonction.PointeurUtilisateur.Etablissement)
        End If
        Call V_MajFiche()
        CadreCmdOK.Visible = False
        Dim TabOK As New List(Of String)
        TabOK.Add("Le dossier ayant la valeur = " & V_CacheMaj.Item(1))
        If V_SiCreation = True Then
            Dim FicheCAR As Virtualia.TablesObjet.ShemaREF.FOR_CARACTERISTIC
            Dim Cretour As Boolean
            FicheCAR = New Virtualia.TablesObjet.ShemaREF.FOR_CARACTERISTIC
            FicheCAR.Actionformation = "(Non affecté)"
            Cretour = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueFormation, 3, V_Identifiant, "C", "", FicheCAR.ContenuTable)
            TabOK.Add(" a été créé.")
            TitreMsg = "Nouveau dossier"
            Call V_MessageDialog(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabOK, V_Identifiant))
        Else
            TabOK.Add(" a été mis à jour.")
            If ImpacterInscriptions() = True Then
                TabOK.Add("Les inscriptions liées ont été également mises à jour.")
            End If
        End If
        Call CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).InitialiserListesLD()
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Function SiMajPossible() As List(Of String)
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim TableauErreur As List(Of String) = Nothing

        If CacheDonnee.Item(17) = "" Then
            CacheDonnee.Item(17) = V_WebFonction.ViRhDates.DateduJour
        End If
        If CacheDonnee.Item(1).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie de l'intitulé du stage est obligatoire.")
        End If
        If CacheDonnee.Item(5).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie du plan est obligatoire.")
        End If
        If CacheDonnee.Item(7).Trim <> "" And CacheDonnee.Item(2).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie de l'organisme de formation est obligatoire dès lors qu'une date de convention est saisie.")
        End If
        If CacheDonnee.Item(4).Trim <> "" And CacheDonnee.Item(3).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie du domaine est obligatoire dès lors qu'un thème est saisi.")
        End If
        If CacheDonnee.Item(16).Trim <> "" And CacheDonnee.Item(4).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie du thème est obligatoire dès lors qu'un sous-thème est saisi.")
        End If
        If V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(10)) > 0 AndAlso CacheDonnee.Item(9).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie du cursus de formation est obligatoire dès lors qu'un ordonnancement est saisi.")
        End If
        If CacheDonnee.Item(1).Trim <> "" Then
            Select Case V_Identifiant
                Case = 0
                    Dim LstDoublons As List(Of Integer) = V_WebFonction.PointeurGlobal.ControleDoublon(WsPvue, 0, 1, CacheDonnee.Item(1).Trim)
                    If LstDoublons IsNot Nothing AndAlso LstDoublons.Count > 0 Then
                        If TableauErreur Is Nothing Then
                            TableauErreur = New List(Of String)
                        End If
                        TableauErreur.Add("La création de cet intitulé du stage n'est pas possible car il existe déjà un stage identifié comme cela.")
                    End If
                Case Else
                    Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
                    Dim Dossier As Virtualia.Metier.Formation.DossierStage
                    Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
                    Dossier = Ensemble.ItemDossier(V_Identifiant)
                    If Dossier IsNot Nothing AndAlso CacheDonnee.Item(1).Trim <> Dossier.Intitule_Stage Then
                        If Dossier.ListeDesFORSessions IsNot Nothing Then
                            If TableauErreur Is Nothing Then
                                TableauErreur = New List(Of String)
                            End If
                            TableauErreur.Add("La modification de l'intitulé du stage n'est pas possible car il existe des sessions.")
                        End If
                    End If
            End Select
        End If
        Return TableauErreur
    End Function

    Private Function ImpacterInscriptions() As Boolean
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Dim IntituleAncien As String = ""
        Dim LstSessions As List(Of Virtualia.TablesObjet.ShemaREF.FOR_SESSION)

        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier IsNot Nothing Then
            IntituleAncien = Dossier.Intitule_Stage
            If Dossier.SiadesInscriptions = False Then
                Return False
            End If
        End If
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        LstSessions = Dossier.ListeDesFORSessions
        If LstSessions Is Nothing Then
            Return False
        End If
        For Each FicheFOR In LstSessions
            Call Dossier.VerifierInscriptions(IntituleAncien, FicheFOR.Date_de_Valeur, FicheFOR.Rang)
        Next
        Return True
    End Function
End Class
