﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPlanningFormation.ascx.vb" Inherits="Virtualia.Net.CtlPlanningFormation" %>

<%@ Register src="~/Controles/Standards/CtlListeCalendrier.ascx" tagname="VListeCal" tagprefix="Virtualia" %>

<asp:Table ID="CadreArmoire" runat="server" Width="1150px" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false">
    <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#6D9092" Height="40px"  Width="100%">
                <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Width="80%" >
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" Height="30px"  Width="30%">
                            <asp:Label ID="EtiTypeArmoire" runat="server" Text="Affichage" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Font-Italic="true" Style="margin-right: 5px" />
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" Height="30px" Width="70%">
                            <asp:DropDownList ID="CboTypeArmoire" runat="server" Height="22px" Width="100%" AutoPostBack="True" BackColor="#A8BBB8" ForeColor="#124545" Style="text-align: left; margin-right: 40px" OnSelectedIndexChanged="CboTypeArmoire_SelectedIndexChanged" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="5px"/>
    </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell  Width="1150px" Height="550px">
            <Virtualia:VListeCal ID="CalendrierDyna" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>