﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_FOR_SESSION
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsPvue As Integer = VI.PointdeVue.PVueFormation
    Private WsNumObjet As Integer = 5
    Private WsNomTable As String = "FOR_SESSION"
    Private WsFiche As Virtualia.TablesObjet.ShemaREF.FOR_SESSION
    Private WsNomStateIntitule As String = "MenuFORIntitule"

    Public Property V_IntituleStage As String
        Get
            If Me.ViewState(WsNomStateIntitule) Is Nothing Then
                Return ""
            End If
            Return CType(Me.ViewState(WsNomStateIntitule), String)
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIntitule) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIntitule), String) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIntitule)
            End If
            Me.ViewState.Add(WsNomStateIntitule, value)
        End Set
    End Property

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(15)
                ColHisto.Add(18)
                ColHisto.Add(3)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Public Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune session")
        LstLibels.Add("Une session")
        LstLibels.Add("sessions")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("du")
        LstColonnes.Add("au")
        LstColonnes.Add("intitulé")
        LstColonnes.Add("durée")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Call V_CommandeNewFiche()
                CadreCmdOK.Visible = True
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = Not (CommandeOK.Visible)
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirVertical.DonText = ""
            Else
                VirVertical.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirVertical.V_SiAutoPostBack = Not (CommandeOK.Visible)
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            VirCoche.V_SiAutoPostBack = Not (CommandeOK.Visible)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
            Else
                If CacheDonnee(NumInfo).ToString = "Oui" Then
                    VirCoche.V_Check = True
                Else
                    VirCoche.V_Check = False
                End If
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NoRang As Integer = 0
        If IsNumeric(CacheDonnee.Item(28)) Then
            NoRang = CInt(CacheDonnee.Item(28))
        End If
        Dim LstInscriptions As List(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)
        LstInscriptions = Dossier.ListeDesInscriptions(Strings.Left(CacheDonnee.Item(0), 10), NoRang)
        If LstInscriptions IsNot Nothing AndAlso LstInscriptions.Count > 0 Then
            Dim LstErreurs As List(Of String) = New List(Of String)
            LstErreurs.Add("Il y a " & LstInscriptions.Count & " inscription(s) pour cette session")
            Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Suppression impossible", LstErreurs))
            Exit Sub
        End If
        LstInscriptions = Dossier.ListeDesInscriptions_Recurrentes
        If LstInscriptions IsNot Nothing AndAlso LstInscriptions.Count > 0 Then
            Dim LstErreurs As List(Of String) = New List(Of String)
            LstErreurs.Add("Il y a " & LstInscriptions.Count & " inscription(s) récurrentes pour cette session")
            Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Suppression impossible", LstErreurs))
            Exit Sub
        End If
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim LstErreurs As List(Of String) = SiMajPossible()
        If LstErreurs Is Nothing Then
            If V_CacheMaj(18) = "" Then
                V_CacheMaj(18) = V_IntituleStage
            End If
            '** Si 2 sessions le même jour et contenu différent alors rang = 0 ou 1  **
            If V_Fiche IsNot Nothing AndAlso V_Fiche.CodeMiseAjour = "C" Then
                V_CacheMaj(28) = InitialiserRang()
                Call V_ValeurChange(0, Strings.Left(V_CacheMaj(0), 10) & Strings.Space(1) & "0" & V_CacheMaj(28) & ":00:00")
            End If
            If V_CacheMaj(0) <> "" And V_CacheMaj(15) <> "" Then
                Dim NbOuvres As Integer = 0
                Dim DateDebut As Date = V_WebFonction.ViRhDates.DateTypee(V_CacheMaj(0))
                Dim DateFin As Date = V_WebFonction.ViRhDates.DateTypee(V_CacheMaj(15))
                Dim IndiceI As Integer = 0
                Dim DateWork As Date = DateDebut
                Do
                    If V_WebFonction.ViRhDates.SiJourOuvre(DateWork.ToShortDateString, True) = True Then
                        NbOuvres += 1
                    End If
                    IndiceI += 1
                    DateWork = DateDebut.AddDays(IndiceI)
                    If DateWork > DateFin Then
                        Exit Do
                    End If
                Loop
                V_CacheMaj(9) = CStr(NbOuvres)
            End If
            If V_CacheMaj(10) <> "" Then
                V_CacheMaj(3) = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).CalculerPoidsJournee(V_CacheMaj(10))
            End If
            '**
            Call V_MajFiche()
            Call ImpacterInscriptions()
            Call ActualiserListe()
            CadreCmdOK.Visible = False
            Exit Sub
        End If
        Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Mise à jour impossible", LstErreurs))
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoD00.ValeurChange, InfoD15.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange,
        InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH06.ValeurChange, InfoH07.ValeurChange,
        InfoH09.ValeurChange, InfoH10.ValeurChange, InfoH11.ValeurChange, InfoH12.ValeurChange, InfoH13.ValeurChange,
        InfoH14.ValeurChange, InfoH17.ValeurChange, InfoH18.ValeurChange, InfoH19.ValeurChange,
        InfoH23.ValeurChange, InfoH24.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV04.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Coche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Coche26.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab16.AppelTable,
    Dontab20.AppelTable, Dontab21.AppelTable, Dontab22.AppelTable, DonTab25.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj
            Dim SiAdresseAfaire As Boolean = False

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                    If NumInfo = 25 Then
                        SiAdresseAfaire = True
                    End If
                Else
                    If CacheDonnee(NumInfo).ToString <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                        If NumInfo = 25 Then
                            SiAdresseAfaire = True
                        End If
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
            If SiAdresseAfaire = False Then
                Exit Property
            End If
            Dim LstENT As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(V_WebFonction.PointeurGlobal.VirModele, V_WebFonction.PointeurGlobal.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueEntreprise, "", "", VI.Operateurs.ET) = 1
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = CacheDonnee(25)
            For NumInfo = 1 To 14
                Constructeur.InfoExtraite(NumInfo - 1, 1, 0) = NumInfo
            Next NumInfo
            Constructeur.SiPasdeTriSurIdeDossier = False
            LstENT = V_WebFonction.PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueEntreprise, 1, Constructeur.OrdreSqlDynamique)
            If LstENT Is Nothing OrElse LstENT.Count = 0 Then
                Exit Property
            End If
            Call V_ValeurChange(7, CType(LstENT.Item(0), Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).NumeroetRue)
            Call V_ValeurChange(23, CType(LstENT.Item(0), Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).Codepostal)
            Call V_ValeurChange(24, CType(LstENT.Item(0), Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).Bureaudistributeur)
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_PointdeVue = WsPvue
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 0
        ListeGrille.Centrage_Colonne(2) = 0
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelJourOuvre.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelHeures.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelJourHor.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelInscrits.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Donnee_BorderkColor")
        LabelTiret.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")

        ListeGrille.SiColonneSelect = True
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            VirVertical.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Function SiMajPossible() As List(Of String)
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim TableauErreur As List(Of String) = Nothing

        If (CacheDonnee.Item(0).Trim <> "" And CacheDonnee.Item(15).Trim = "") Or (CacheDonnee.Item(0).Trim = "" And CacheDonnee.Item(15).Trim <> "") Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie des dates de début et de fin de session est obligatoire.")
        End If
        If CacheDonnee.Item(0).Trim <> "" And CacheDonnee.Item(15).Trim <> "" Then
            If V_WebFonction.ViRhDates.DateTypee(Strings.Left(CacheDonnee.Item(0), 10)) > V_WebFonction.ViRhDates.DateTypee(CacheDonnee.Item(15)) Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("La date de fin de la session doit être supérieure à la date de début.")
            End If
            If V_Fiche IsNot Nothing AndAlso V_Fiche.CodeMiseAjour = "C" Then
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = V_ListeFiches(CacheDonnee.Item(0).Trim, "")
                If LstFiches IsNot Nothing And LstFiches.Count > 2 Then
                    If TableauErreur Is Nothing Then
                        TableauErreur = New List(Of String)
                    End If
                    TableauErreur.Add("Il ne peut y avoir plus de 2 sessions le même jour.")
                End If
            End If
        End If
        If V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(10)) = 0 Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie de la durée de la session en heures est obligatoire.")
        End If
        If V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(2)) > 0 Then
            If V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(1)) > V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(2)) Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("Le nombre maximum d'inscrits doit être supérieur au nombre minimum.")
            End If
        End If

        If (CacheDonnee.Item(19).Trim <> "" Or CacheDonnee.Item(20).Trim <> "" Or CacheDonnee.Item(21).Trim <> "") And CacheDonnee.Item(18).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie d'un intitulé de session est obligatoire dès lors qu'un cursus, un module ou une référence ont été saisis.")
        End If

        '*** Contrôle des horaires 
        Dim Heure_1330 As DateTime = V_WebFonction.ViRhDates.HeureTypee(V_WebFonction.ViRhDates.DateduJour, "13:30")
        Dim HMatin_Debut As DateTime = V_WebFonction.ViRhDates.HeureTypee(V_WebFonction.ViRhDates.DateduJour, CacheDonnee.Item(11))
        Dim HMatin_Fin As DateTime = V_WebFonction.ViRhDates.HeureTypee(V_WebFonction.ViRhDates.DateduJour, CacheDonnee.Item(12))
        Dim HSoir_Debut As DateTime = V_WebFonction.ViRhDates.HeureTypee(V_WebFonction.ViRhDates.DateduJour, CacheDonnee.Item(13))
        Dim HSoir_Fin As DateTime = V_WebFonction.ViRhDates.HeureTypee(V_WebFonction.ViRhDates.DateduJour, CacheDonnee.Item(14))

        If CacheDonnee.Item(11) <> "" And CacheDonnee.Item(12) <> "" Then
            If V_WebFonction.ViRhDates.ComparerHeures(CacheDonnee.Item(11), CacheDonnee.Item(12)) = VI.ComparaisonDates.PlusGrand Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("Les horaires de matinée saisis sont incohérents.")
            End If
            If V_WebFonction.ViRhDates.ComparerHeures(CacheDonnee.Item(12), "13:30") = VI.ComparaisonDates.PlusGrand Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("Les horaires de matinée saisis semblent être ceux de l'aprés-midi.")
            End If
        End If
        If CacheDonnee.Item(13) <> "" And CacheDonnee.Item(14) <> "" Then
            If V_WebFonction.ViRhDates.ComparerHeures(CacheDonnee.Item(13), CacheDonnee.Item(14)) = VI.ComparaisonDates.PlusGrand Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("Les horaires d'aprés-midi saisis sont incohérents.")
            End If
            If V_WebFonction.ViRhDates.ComparerHeures("13:30", CacheDonnee.Item(13)) = VI.ComparaisonDates.PlusGrand Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("Les horaires d'après-midi saisis semblent être ceux de la matinée.")
            End If
        End If
        If CacheDonnee.Item(11) <> "" And CacheDonnee.Item(14) <> "" Then
            If V_WebFonction.ViRhDates.ComparerHeures(CacheDonnee.Item(11), CacheDonnee.Item(14)) = VI.ComparaisonDates.PlusGrand Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("Les horaires saisis sont incohérents.")
            End If
        End If
        '**
        '** Contrôle de la salle de formation
        If CacheDonnee.Item(22) <> "" And CacheDonnee.Item(0).Trim <> "" And CacheDonnee.Item(15).Trim <> "" Then
            Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
            Dim MsgRetour As String
            Dim TypeJour As Integer = Type_PlageHoraire(CacheDonnee)
            Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
            MsgRetour = Ensemble.Controler_Utilisation_Salle(CacheDonnee.Item(22), Strings.Left(CacheDonnee.Item(0), 10), CacheDonnee.Item(15).Trim, TypeJour, V_Identifiant)
            If MsgRetour <> "OK" Then
                If TableauErreur Is Nothing Then
                    TableauErreur = New List(Of String)
                End If
                TableauErreur.Add("La salle est déjà utilisée pour une autre formation : " & MsgRetour)
            End If
        End If
        '**
        Return TableauErreur
    End Function

    Private Function InitialiserRang() As String
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim LstSessions As List(Of Virtualia.TablesObjet.ShemaREF.FOR_SESSION)
        Dim ChaineHorOri As String
        Dim ChaineHorNew As String

        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier Is Nothing Then
            Return "0"
        End If
        LstSessions = Dossier.ListeDesFORSessions
        If LstSessions Is Nothing Then
            Return "0"
        End If
        For Each FicheFOR In LstSessions
            If FicheFOR.Date_Valeur_ToDate = CDate(CacheDonnee(0)) Then
                ChaineHorOri = FicheFOR.HeureDebut_Matin & FicheFOR.HeureFin_Matin & FicheFOR.HeureDebut_ApresMidi & FicheFOR.HeureFin_ApresMidi
                ChaineHorNew = CacheDonnee(11) & CacheDonnee(12) & CacheDonnee(13) & CacheDonnee(14)
                If FicheFOR.IntituleSession <> CacheDonnee(18) OrElse ChaineHorOri <> ChaineHorNew Then
                    Select Case FicheFOR.Rang
                        Case 0
                            Return "1"
                        Case 1
                            Return "0"
                    End Select
                End If
                Return CStr(FicheFOR.Rang)
            End If
        Next
        Return "0"
    End Function

    Private Sub ImpacterInscriptions()
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NoRang As Integer = 0

        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        If IsNumeric(CacheDonnee.Item(28)) Then
            NoRang = CInt(CacheDonnee.Item(28))
        End If
        Call Dossier.VerifierInscriptions("", Strings.Left(CacheDonnee.Item(0), 10), NoRang)
    End Sub

    Private Sub ListeGrille_PreRender(sender As Object, e As EventArgs) Handles ListeGrille.PreRender
        ListeGrille.SiColonneSelect = True
    End Sub

    Private ReadOnly Property Type_PlageHoraire(ByVal CacheCourant As List(Of String)) As Integer
        Get
            Dim CloneFiche As Virtualia.TablesObjet.ShemaREF.FOR_SESSION
            CloneFiche = New Virtualia.TablesObjet.ShemaREF.FOR_SESSION
            CloneFiche.Date_de_Valeur = CacheCourant.Item(0)
            CloneFiche.Date_de_Fin = CacheCourant.Item(9)
            CloneFiche.HeureDebut_Matin = CacheCourant.Item(11)
            CloneFiche.HeureFin_Matin = CacheCourant.Item(12)
            CloneFiche.HeureDebut_ApresMidi = CacheCourant.Item(13)
            CloneFiche.HeureFin_ApresMidi = CacheCourant.Item(14)
            Return CloneFiche.VTypeJour
        End Get
    End Property
End Class
