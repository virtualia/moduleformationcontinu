﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_Formation" Codebehind="Formation.ascx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_IDENTIFICATION.ascx" tagname="FOR_IDENTIFICATION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_DESCRIPTIF.ascx" tagname="FOR_DESCRIPTIF" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_CARACTERISTIC.ascx" tagname="FOR_CARACTERISTIC" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_SESSION.ascx" tagname="FOR_SESSION" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_COUTS.ascx" tagname="FOR_COUTS" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_FACTURE.ascx" tagname="FOR_FACTURE" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_INTERVENANT.ascx" tagname="FOR_INTERVENANT" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Formation/FOR_EVALUATION.ascx" tagname="FOR_EVALUATION" tagprefix="Virtualia" %>

<asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1150px" HorizontalAlign="Center"
                     style="margin-top: 0px " >
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="750px" HorizontalAlign="Left"
               CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Onglets/Onglet_2bords_SysRef.bmp"
               style="margin-top: 10px; border-top-color: #6C9690;
                border-bottom-color: #B0E0D7; border-bottom-style: groove" >
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="150px">
                        <asp:Button ID="BoutonN1" runat="server" Height="24px" Width="135px" 
                            ForeColor="White" Text="Caractéristiques" Font-Bold="true" Font-Italic="true"
                            Font-Names="Trebuchet MS" BackColor="#6C9690" BorderStyle="None"
                            style="padding-left: 1px; margin-left: 0px; margin-bottom: 2px; text-align: center" />     
                    </asp:TableCell>
                   <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="150px">
                        <asp:Button ID="BoutonN2" runat="server" Height="33px" Width="148px" 
                            ForeColor="#142425" Text="Descriptif" Font-Bold="false" Font-Italic="true"
                            Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None"
                            style="padding-left: 1px; margin-left: 0px; margin-bottom: 2px; text-align: center" />  
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="150px">
                        <asp:Button ID="BoutonN3" runat="server" Height="33px" Width="148px" 
                            ForeColor="#142425" Text="Sessions" Font-Bold="false" Font-Italic="true"
                            Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                            style="padding-left: 1px; margin-left: 0px; margin-bottom: 2px; text-align: center" />  
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="150px">
                        <asp:Button ID="BoutonN4" runat="server" Height="33px" Width="148px" 
                            ForeColor="#142425" Text="Coûts" Font-Bold="false" Font-Italic="true"
                            Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                            style="padding-left: 1px; margin-left: 0px; margin-bottom: 2px; text-align: center" />   
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign ="Center" VerticalAlign="Bottom" Width="150px">
                        <asp:Button ID="BoutonN5" runat="server" Height="33px" Width="148px" 
                            ForeColor="#142425" Text="Evaluation" Font-Bold="false" Font-Italic="true"
                            Font-Names="Trebuchet MS" BackColor="Transparent" BorderStyle="None" 
                            style="padding-left: 1px; margin-left: 0px; margin-bottom: 2px; text-align: center" />   
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Bottom">
            <asp:Table ID="CadreRetour" runat="server" Height="22px" CellPadding="0" 
                 CellSpacing="0" BackImageUrl="~/Images/Boutons/Retour_Std.bmp" Visible="true"
                 BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                 Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px" >
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                         <asp:Button ID="CommandeRetour" runat="server" Text="Retour" Width="65px" Height="20px"
                             BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                             Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                             BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                         </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
     </asp:TableRow>
   </asp:Table>
   <asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
             BackColor="#A8BBB8" CellSpacing="2" style="margin-top: 0px" >
       <asp:TableRow>
           <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
               <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                  <asp:View ID="VueCaracteristique" runat="server">  
                     <Virtualia:FOR_IDENTIFICATION ID="FOR_IDENTIFICATION_1" runat="server"
                       CadreStyle="margin-top: 10px"  /> 
                     <Virtualia:FOR_CARACTERISTIC ID="FOR_CARACTERISTIC_3" runat="server"
                       CadreStyle="margin-top: 20px"  />    
                   </asp:View>  
                  <asp:View ID="VueDescriptif" runat="server">
                     <Virtualia:FOR_DESCRIPTIF ID="FOR_DESCRIPTIF_2" runat="server"
                       CadreStyle="margin-top: 10px"  /> 
                     <Virtualia:FOR_INTERVENANT ID="FOR_INTERVENANT_9" runat="server"
                       CadreStyle="margin-top: 20px"  />  
                  </asp:View>
                  <asp:View ID="VueSession" runat="server">  
                      <Virtualia:FOR_SESSION ID="FOR_SESSION_5" runat="server"
                       CadreStyle="margin-top: 10px" />  
                  </asp:View> 
                  <asp:View ID="VueCout" runat="server">
                      <Virtualia:FOR_COUTS ID="FOR_COUTS_6" runat="server"
                       CadreStyle="margin-top: 10px" />
                      <Virtualia:FOR_FACTURE ID="FOR_FACTURE_7" runat="server"
                       CadreStyle="margin-top: 0px" /> 
                  </asp:View>
                  <asp:View ID="VueEvaluation" runat="server">  
                      <Virtualia:FOR_EVALUATION ID="FOR_EVALUATION_4" runat="server"
                       CadreStyle="margin-top: 10px" />  
                  </asp:View>
              </asp:MultiView>
          </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
        <asp:TableCell ID="CellMessage" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Virtualia:VMessage id="MsgVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell>
              <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
          </asp:TableCell>
      </asp:TableRow>
  </asp:Table>