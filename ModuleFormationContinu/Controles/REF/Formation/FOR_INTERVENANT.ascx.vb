﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_FOR_INTERVENANT
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsPvue As Integer = VI.PointdeVue.PVueFormation
    Private WsNumObjet As Integer = 9
    Private WsNomTable As String = "FOR_INTERVENANT"
    Private WsFiche As Virtualia.TablesObjet.ShemaREF.FOR_INTERVENANT

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(1)
                ColHisto.Add(2)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Public Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucun intervenant")
        LstLibels.Add("Un intervenant")
        LstLibels.Add("intervenants")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("nom et prénom")
        LstColonnes.Add("Organisme")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If

    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = Not (CommandeOK.Visible)
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirVertical.DonText = ""
            Else
                VirVertical.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirVertical.V_SiAutoPostBack = Not (CommandeOK.Visible)
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim LstErreurs As List(Of String) = SiMajPossible()
        If LstErreurs Is Nothing Then
            Call V_MajFiche()
            Call ActualiserListe()
            CadreCmdOK.Visible = False
            Exit Sub
        End If
        Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Mise à jour impossible", LstErreurs))
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange, _
    InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange, InfoH06.ValeurChange, InfoH07.ValeurChange, _
    InfoH08.ValeurChange, InfoH09.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV10.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab02.AppelTable
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj
            Dim SiAdresseAfaire As Boolean = False

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                    SiAdresseAfaire = True
                Else
                    If CacheDonnee(NumInfo).ToString <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                        SiAdresseAfaire = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
            If SiAdresseAfaire = False Then
                Exit Property
            End If
            Dim LstENT As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(V_WebFonction.PointeurGlobal.VirModele, V_WebFonction.PointeurGlobal.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueEntreprise, "", "", VI.Operateurs.ET) = 1
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = CacheDonnee(2)
            For NumInfo = 1 To 14
                Constructeur.InfoExtraite(NumInfo - 1, 1, 0) = NumInfo
            Next NumInfo
            Constructeur.SiPasdeTriSurIdeDossier = False
            LstENT = V_WebFonction.PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueEntreprise, 1, Constructeur.OrdreSqlDynamique)
            If LstENT Is Nothing OrElse LstENT.Count = 0 Then
                Exit Property
            End If
            Call V_ValeurChange(3, CType(LstENT.Item(0), Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).NumeroetRue)
            Call V_ValeurChange(5, CType(LstENT.Item(0), Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).Codepostal)
            Call V_ValeurChange(6, CType(LstENT.Item(0), Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).Bureaudistributeur)
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_PointdeVue = WsPvue
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 0
        ListeGrille.Centrage_Colonne(2) = 0
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            VirVertical.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Function SiMajPossible() As List(Of String)
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim TableauErreur As List(Of String) = Nothing

        If CacheDonnee.Item(1).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie des nom et prénom de l'intervenant est obligatoire.")
        End If
        
        Return TableauErreur
    End Function

    Private Sub ListeGrille_PreRender(sender As Object, e As EventArgs) Handles ListeGrille.PreRender
        ListeGrille.SiColonneSelect = True
    End Sub
End Class
