﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_FOR_FACTURE
    
    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ListeGrille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeGrille As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreCommandes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCommandes As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreCmdNewSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdNewSupp As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeNew.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeNew As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeSupp As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDate As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoD00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle CadreFacture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFacture As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH01 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle LabelEuro4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEuro4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dontab02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab02 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH05 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle LabelEuro5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEuro5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Dontab03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab03 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH06 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle LabelEuro6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelEuro6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableauPeriode.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauPeriode As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoD10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD10 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoD11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD11 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle TableauComptaFacture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauComptaFacture As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoD12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD12 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoD13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD13 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Dontab14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab14 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle Dontab08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab08 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle Dontab09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab09 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle TableauObservation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauObservation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoV07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoV07 As Global.Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
End Class
