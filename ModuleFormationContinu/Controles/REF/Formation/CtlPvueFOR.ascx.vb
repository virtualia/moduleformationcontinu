﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlPvueFOR
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheWebFenetre
    Private WsNomStateCache As String = "MenuFORIde"
    Private WsActRetour As System.Action(Of Virtualia.Systeme.Evenements.MessageRetourEventArgs)

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheWebFenetre
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheWebFenetre)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheWebFenetre
            NewCache = New Virtualia.Net.VCaches.CacheWebFenetre
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheWebFenetre)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property V_Identifiant As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Ide_Dossier
        End Get
        Set(ByVal value As Integer)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Ide_Dossier = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_SiEnLectureSeule As Boolean
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.SiReadOnly
        End Get
        Set(ByVal value As Boolean)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.SiReadOnly = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public WriteOnly Property Act_Retour As Action(Of Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        Set(value As System.Action(Of Virtualia.Systeme.Evenements.MessageRetourEventArgs))
            WsActRetour = value
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
            Call AfficherFenetreFOR()
        End If
    End Sub

    Private Sub AfficherFenetreFOR()
        Dim ChaineRef As String = ""
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.Ide_Dossier = 0 Then
            EtiTitre.Text = ""
            FOR_IDENTIFICATION_1.Identifiant = 0
            FOR_CARACTERISTIC_3.Identifiant = 0
            FOR_DESCRIPTIF_2.Identifiant = 0
            FOR_INTERVENANT_9.Identifiant = 0
            FOR_SESSION_5.Identifiant = 0
            FOR_COUTS_6.Identifiant = 0
            FOR_FACTURE_7.Identifiant = 0
            FOR_EVALUATION_4.Identifiant = 0
            Exit Sub
        End If
        FOR_IDENTIFICATION_1.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
        FOR_IDENTIFICATION_1.Identifiant = WsCtl_Cache.Ide_Dossier
        If FOR_IDENTIFICATION_1.ReferenceDuStage <> "" Then
            ChaineRef = " (" & FOR_IDENTIFICATION_1.ReferenceDuStage & ")"
        End If
        EtiTitre.Text = FOR_IDENTIFICATION_1.IntituleStage & ChaineRef
        Select Case MultiFOR.ActiveViewIndex
            Case 1
                FOR_CARACTERISTIC_3.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_CARACTERISTIC_3.Identifiant = WsCtl_Cache.Ide_Dossier
            Case 2
                FOR_DESCRIPTIF_2.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_DESCRIPTIF_2.Identifiant = WsCtl_Cache.Ide_Dossier
            Case 3
                FOR_INTERVENANT_9.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_INTERVENANT_9.Identifiant = WsCtl_Cache.Ide_Dossier
            Case 4
                FOR_SESSION_5.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_SESSION_5.Identifiant = WsCtl_Cache.Ide_Dossier
                FOR_SESSION_5.V_IntituleStage = FOR_IDENTIFICATION_1.IntituleStage
            Case 5
                FOR_COUTS_6.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_COUTS_6.Identifiant = WsCtl_Cache.Ide_Dossier
            Case 6
                FOR_FACTURE_7.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_FACTURE_7.Identifiant = WsCtl_Cache.Ide_Dossier
            Case 7
                FOR_EVALUATION_4.V_SiEnLectureSeule = WsCtl_Cache.SiReadOnly
                FOR_EVALUATION_4.Identifiant = WsCtl_Cache.Ide_Dossier
        End Select
    End Sub

    Private Sub FOR_MENU_Menu_Click(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles FOR_MENU.Menu_Click
        If IsNumeric(e.Parametre) Then
            MultiFOR.ActiveViewIndex = CInt(e.Parametre)
        End If
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueCaracteristique"
                MultiFOR.SetActiveView(VueCaracteristique)
            Case "VueCout"
                MultiFOR.SetActiveView(VueCout)
            Case "VueDescriptif"
                MultiFOR.SetActiveView(VueDescriptif)
            Case "VueEvaluation"
                MultiFOR.SetActiveView(VueEvaluation)
            Case "VueFacture"
                MultiFOR.SetActiveView(VueFacture)
            Case "VueIdentification"
                MultiFOR.SetActiveView(VueIdentification)
            Case "VueIntervenant"
                MultiFOR.SetActiveView(VueIntervenant)
            Case "VueSession"
                MultiFOR.SetActiveView(VueSession)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiFOR.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueCaracteristique"
                FOR_CARACTERISTIC_3.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiFOR.SetActiveView(VueCaracteristique)
            Case "VueCout"
                FOR_COUTS_6.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiFOR.SetActiveView(VueCout)
            Case "VueFacture"
                FOR_FACTURE_7.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiFOR.SetActiveView(VueFacture)
            Case "VueIdentification"
                FOR_IDENTIFICATION_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiFOR.SetActiveView(VueIdentification)
            Case "VueIntervenant"
                FOR_INTERVENANT_9.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiFOR.SetActiveView(VueIntervenant)
            Case "VueSession"
                FOR_SESSION_5.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiFOR.SetActiveView(VueSession)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiFOR.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles FOR_CARACTERISTIC_3.AppelTable, FOR_COUTS_6.AppelTable, FOR_FACTURE_7.AppelTable, FOR_IDENTIFICATION_1.AppelTable, _
        FOR_INTERVENANT_9.AppelTable, FOR_SESSION_5.AppelTable

        Select Case e.ObjetAppelant
            Case 1
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueIdentification.ID
            Case 3
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueCaracteristique.ID
            Case 5
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSession.ID
            Case 6
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueCout.ID
            Case 7
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueFacture.ID
            Case 9
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueIntervenant.ID
            Case Else
                Exit Sub
        End Select

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case 5
                        FOR_SESSION_5.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueSession)
                    Case 6
                        FOR_COUTS_6.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueCout)
                    Case 7
                        FOR_FACTURE_7.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueFacture)
                    Case 9
                        FOR_INTERVENANT_9.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueIntervenant)
                End Select
            Case Is = "SuppDossier"
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case 1 'Création
                        If IsNumeric(e.ChaineDatas) Then
                            V_Identifiant = CInt(e.ChaineDatas)
                        End If
                        FOR_IDENTIFICATION_1.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueIdentification)
                    Case 2
                        FOR_DESCRIPTIF_2.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueDescriptif)
                    Case 3
                        FOR_CARACTERISTIC_3.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueCaracteristique)
                    Case 4
                        FOR_EVALUATION_4.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueEvaluation)
                    Case 5
                        FOR_SESSION_5.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueSession)
                    Case 6
                        FOR_COUTS_6.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueCout)
                    Case 7
                        FOR_FACTURE_7.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueFacture)
                    Case 9
                        FOR_INTERVENANT_9.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiFOR.SetActiveView(VueIntervenant)
                End Select
        End Select
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles FOR_IDENTIFICATION_1.MessageDialogue, FOR_DESCRIPTIF_2.MessageDialogue, FOR_CARACTERISTIC_3.MessageDialogue, FOR_EVALUATION_4.MessageDialogue, _
    FOR_SESSION_5.MessageDialogue, FOR_COUTS_6.MessageDialogue, FOR_FACTURE_7.MessageDialogue, FOR_INTERVENANT_9.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles FOR_IDENTIFICATION_1.MessageSaisie, FOR_DESCRIPTIF_2.MessageSaisie, FOR_CARACTERISTIC_3.MessageSaisie, FOR_EVALUATION_4.MessageSaisie,
    FOR_SESSION_5.MessageSaisie, FOR_COUTS_6.MessageSaisie, FOR_FACTURE_7.MessageSaisie, FOR_INTERVENANT_9.MessageSaisie
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Exit Sub
    End Sub

    Private Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("STAGE", 0, "", "")
        If WsActRetour IsNot Nothing Then
            WsActRetour(Evenement)
        Else
            ReponseRetour(Evenement)
        End If
    End Sub
End Class