﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_FOR_DESCRIPTIF" Codebehind="FOR_DESCRIPTIF.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#FFEBC8" Height="570px" Width="600px" HorizontalAlign="Center">
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px" >
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                        BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>                        
    </asp:TableCell>      
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Descriptif du stage" Height="20px" Width="300px"
                        BackColor="#A67F3B" BorderColor="#FFEBC8"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#FFF2DB"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;" >
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>                        
    </asp:TableCell>      
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauDescriptif" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="600px" >
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV02" runat="server" DonTextMode="true"
                      V_PointdeVue="9" V_Objet="2" V_Information="2" V_SiDonneeDico="true"
                      EtiWidth="590px" DonWidth="588px" DonHeight="120px" DonTabIndex="1"
                      Etistyle="margin-left: 2px; text-align:center;" Donstyle="margin-left: 2px;" />
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="4px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left" >
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV01" runat="server" DonTextMode="true"
                      V_PointdeVue="9" V_Objet="2" V_Information="1" V_SiDonneeDico="true"
                      EtiWidth="590px" DonWidth="588px" DonHeight="120px" DonTabIndex="2"
                      Etistyle="margin-left: 2px; text-align:center;" Donstyle="margin-left: 2px;" />
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="4px"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>                        
    </asp:TableCell>      
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
        <asp:Table ID="TableauObservation" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="600px" >   
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" DonTextMode="true"
                      V_PointdeVue="9" V_Objet="2" V_Information="3" V_SiDonneeDico="true"
                      EtiWidth="290px" DonWidth="288px" DonHeight="165px" DonTabIndex="3"
                      Etistyle="margin-left: 2px; text-align:center;" Donstyle="margin-left: 2px;" />
                 </asp:TableCell>
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server" DonTextMode="true"
                      V_PointdeVue="9" V_Objet="2" V_Information="4" V_SiDonneeDico="true"
                      EtiWidth="290px" DonWidth="288px" DonHeight="165px" DonTabIndex="4"
                      Etistyle="margin-left: 0px; text-align:center;" Donstyle="margin-left: 0px;"/>
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell Height="15px" Columnspan="2"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>                        
    </asp:TableCell>      
  </asp:TableRow> 
</asp:Table>