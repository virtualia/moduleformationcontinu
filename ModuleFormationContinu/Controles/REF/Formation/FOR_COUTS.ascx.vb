﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_FOR_COUTS
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsPvue As Integer = VI.PointdeVue.PVueFormation
    Private WsNumObjet As Integer = 6
    Private WsNomTable As String = "FOR_COUTS"
    Private WsFiche As Virtualia.TablesObjet.ShemaREF.FOR_COUTS

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(12)
                V_CacheColHisto = ColHisto
                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Public Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune situation")
        LstLibels.Add("Une situation")
        LstLibels.Add("situations")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("date de valeur")
        LstColonnes.Add("coût pédagogique")
        LstColonnes.Add("modalité")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiAutoPostBack = Not (CommandeOK.Visible)
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        If V_ListeFiches.Count > 1 Then
            Try
                Call V_CommandeSuppFiche()
                Exit Sub
            Catch ex As Exception
                Exit Sub
            End Try
        End If
        Dim NbInscrits = NombreInscriptions
        If NbInscrits = 0 Then
            Try
                Call V_CommandeSuppFiche()
                Exit Sub
            Catch ex As Exception
                Exit Sub
            End Try
        End If
        Dim LstErreurs As List(Of String) = New List(Of String)
        LstErreurs.Add("Il y a " & NbInscrits & " inscription(s) pour ce stage")
        Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Suppression impossible", LstErreurs))
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim LstErreurs As List(Of String) = SiMajPossible()
        If LstErreurs Is Nothing Then
            Call V_MajFiche()
            Call ImpacterInscriptions()
            Call ActualiserListe()
            CadreCmdOK.Visible = False
            Exit Sub
        End If
        Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Mise à jour impossible", LstErreurs))
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD00.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange,
        InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange,
        InfoH06.ValeurChange, InfoH08.ValeurChange, InfoH11.ValeurChange, InfoH13.ValeurChange, InfoH14.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab07.AppelTable, _
    Dontab09.AppelTable, Dontab10.AppelTable, Dontab12.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee(NumInfo).ToString <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_PointdeVue = WsPvue
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelEuro1.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro2.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro3.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro4.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro5.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro6.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro8.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro11.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro13.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelEuro14.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")

        ListeGrille.SiColonneSelect = True
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Function SiMajPossible() As List(Of String)
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim TableauErreur As List(Of String) = Nothing
        Dim ZCalc As Double = 0
        Dim IndiceI As Integer

        If CacheDonnee.Item(0).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie de la date de valeur est obligatoire.")
        End If
        If CacheDonnee.Item(12).Trim = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie de la modalité de calcul est obligatoire.")
        ElseIf V_Fiche IsNot Nothing AndAlso CType(V_Fiche, Virtualia.TablesObjet.ShemaREF.FOR_COUTS).Modalitecalcul_couts <> "" Then
            If CacheDonnee.Item(12).Trim <> CType(V_Fiche, Virtualia.TablesObjet.ShemaREF.FOR_COUTS).Modalitecalcul_couts Then
                Dim NbInscrits = NombreInscriptions
                If NbInscrits > 0 Then
                    If TableauErreur Is Nothing Then
                        TableauErreur = New List(Of String)
                    End If
                    TableauErreur.Add("Modification de la modalité impossible. Il y a " & NbInscrits & " inscription(s) pour ce stage.")
                End If
            End If
        End If
        If CacheDonnee.Item(7).Trim <> "" And V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(8)) <= 0 Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie d'un montant est obligatoire dès lors que la prise en charge financière est saisie.")
        End If
        If CacheDonnee.Item(7).Trim = "" And V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(8)) > 0 Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie de la pris en charge financière est obligatoire dès lors qu'un montant est saisi.")
        End If
        For IndiceI = 1 To 14
            Select Case IndiceI
                Case 1, 2, 24, 5, 6, 13, 14
                    ZCalc = V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(IndiceI))
                    If ZCalc > 0 Then
                        Exit For
                    End If
            End Select
        Next IndiceI
        If ZCalc <= 0 Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie d'au moins un coût est obligatoire.")
        End If

        Return TableauErreur
    End Function

    Private Sub ImpacterInscriptions()
        Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
        Dim Dossier As Virtualia.Metier.Formation.DossierStage
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim LstSessions As List(Of Virtualia.TablesObjet.ShemaREF.FOR_SESSION)

        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        LstSessions = Dossier.ListeDesFORSessions
        If LstSessions Is Nothing Then
            Exit Sub
        End If
        For Each FicheFOR In LstSessions
            If CDate(FicheFOR.DateSession) >= CDate(CacheDonnee(0)) Then
                Call Dossier.VerifierInscriptions("", Strings.Left(FicheFOR.DateSession, 10), FicheFOR.Rang)
            End If
        Next
    End Sub

    Private Sub ListeGrille_PreRender(sender As Object, e As EventArgs) Handles ListeGrille.PreRender
        ListeGrille.SiColonneSelect = True
    End Sub

    Private ReadOnly Property NombreInscriptions As Integer
        Get
            Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
            Dim Dossier As Virtualia.Metier.Formation.DossierStage
            Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
            Ensemble.Identifiant = V_Identifiant
            Dossier = Ensemble.ItemDossier(V_Identifiant)
            If Dossier Is Nothing Then
                Return 0
            End If
            Dim CacheDonnee As List(Of String) = V_CacheMaj
            Dim LstInscriptions As List(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)
            LstInscriptions = Dossier.ListeDesInscriptions
            If LstInscriptions IsNot Nothing OrElse LstInscriptions.Count > 0 Then
                Return LstInscriptions.Count
            End If
            Return 0
        End Get
    End Property


End Class
