﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_ENT_ORGANISME" Codebehind="ENT_ORGANISME.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#FFEBC8" Height="445px" Width="550px" HorizontalAlign="Center">
    <asp:TableRow>
      <asp:TableCell>
         <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
               <asp:TableCell VerticalAlign="Bottom">
                 <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                 </asp:Button>
               </asp:TableCell>
            </asp:TableRow>
         </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px"
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Entreprise ou Organisme" Height="20px" Width="280px"
                        BackColor="#A67F3B" BorderColor="#FFEBC8" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#FFF2DB"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreDon" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="550px">
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="1" V_SiDonneeDico="true"
                       EtiWidth="105px" DonWidth="425px" DonTabIndex="1"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab13" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="13" V_SiDonneeDico="true"
                       EtiWidth="125px" DonWidth="355px" DonTabIndex="2"
                       EtiStyle="margin-left: 20px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="9" V_SiDonneeDico="true"
                       EtiWidth="125px" DonWidth="355px" DonTabIndex="3"
                       EtiStyle="margin-left: 20px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="10" V_SiDonneeDico="true"
                       EtiWidth="125px" DonWidth="355px" DonTabIndex="4"
                       EtiStyle="margin-left: 20px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="8px"></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreImmatriculation" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="550px">
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="11" V_SiDonneeDico="true"
                       EtiWidth="90px" DonWidth="100px" DonTabIndex="5"
                       EtiStyle="margin-left: 20px;"/>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="7" V_SiDonneeDico="true"
                       EtiWidth="120px" DonWidth="120px" DonTabIndex="6"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="8" V_SiDonneeDico="true"
                       EtiWidth="90px" DonWidth="60px" DonTabIndex="7"
                       EtiStyle="margin-left: 20px;"/>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="12" V_SiDonneeDico="true"
                       EtiWidth="120px" DonWidth="126px" DonTabIndex="8"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreAdresse1" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="480px">
            <asp:TableRow> 
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV02" runat="server"
                      V_PointdeVue="10" V_Objet="1" V_Information="2" V_SiDonneeDico="true"
                      EtiWidth="472px" DonWidth="470px" DonHeight="20px" DonTabIndex="9"
                      EtiStyle="text-align:center; margin-left: 20px;" Donstyle="margin-left: 20px;"/>
                 </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="1px"></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreAdresse2" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="480px" HorizontalAlign="Left">
            <asp:TableRow>
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server"
                      V_PointdeVue="10" V_Objet="1" V_Information="3" V_SiDonneeDico="true"
                      EtiWidth="78px" DonWidth="76px" DonHeight="20px" DonTabIndex="10"
                      EtiStyle="text-align:Left; margin-left: 20px;" Donstyle="margin-left: 20px;"/>
                 </asp:TableCell> 
                 <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server"
                      V_PointdeVue="10" V_Objet="1" V_Information="4" V_SiDonneeDico="true"
                      EtiWidth="388px" DonWidth="386px" DonHeight="20px" DonTabIndex="11"
                      EtiStyle="text-align:center; margin-left: 2px;" Donstyle="margin-left: 2px;"/>
                 </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="2px" ColumnSpan="2" ></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreContact" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="480px">
            <asp:TableRow> 
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="5" V_SiDonneeDico="true"
                       etiWidth="78px" DonWidth="130px" DonTabIndex="12"
                       EtiStyle="margin-left: 19px;"/>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="6" V_SiDonneeDico="true"
                       EtiWidth="82px" DonWidth="130px" DonTabIndex="13"
                       EtiStyle="margin-left: 30px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreURL" runat="server" Height="20px" CellPadding="0" CellSpacing="0" 
            Width="550px" HorizontalAlign="Left">
            <asp:TableRow>
               <asp:TableCell HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                       V_PointdeVue="10" V_Objet="1" V_Information="14" V_SiDonneeDico="true"
                       EtiWidth="40px" DonWidth="475px" DonTabIndex="14"
                       EtiStyle="margin-left: 19px;"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px"></asp:TableCell>
            </asp:TableRow> 
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
</asp:Table>