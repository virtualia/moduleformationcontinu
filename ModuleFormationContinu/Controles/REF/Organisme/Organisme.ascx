﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_Organisme" Codebehind="Organisme.ascx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Organisme/ENT_ORGANISME.ascx" tagname="ENT_ORGANISME" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/REF/Organisme/ENT_INTERLOCUTEUR.ascx" tagname="ENT_INTERLOCUTEUR" tagprefix="Virtualia" %>

<asp:Table ID="CadreSaisie" runat="server" Height="1220px" Width="1150px" HorizontalAlign="Center"
            BackColor="#A8BBB8" CellSpacing="2" style="margin-top: 0px" >
    <asp:TableRow>
        <asp:TableCell ID="ConteneurVues" Height="1220px" Width="820px" VerticalAlign="Top" HorizontalAlign="Center">
            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueDefinition" runat="server">  
                    <Virtualia:ENT_ORGANISME ID="ENT_ORGANISME_1" runat="server" CadreStyle="margin-top: 10px"  />
                    <Virtualia:ENT_INTERLOCUTEUR ID="ENT_INTERLOCUTEUR_2" runat="server" CadreStyle="margin-top: 10px"  /> 
                </asp:View>  
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell ID="CellMessage" Visible="false">
        <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="PanelMsgPopup" runat="server">
            <Virtualia:VMessage id="MsgVirtualia" runat="server" />
        </asp:Panel>
        <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellReference" Visible="false">
        <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="PanelRefPopup" runat="server">
            <Virtualia:VReference ID="RefVirtualia" runat="server" />
        </asp:Panel>
        <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>