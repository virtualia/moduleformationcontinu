﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_Organisme
    Inherits System.Web.UI.UserControl
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public Property Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            If value = -1 Then
                ENT_ORGANISME_1.Identifiant = 0
                Call ENT_ORGANISME_1.V_CommandeNewDossier(VI.PointdeVue.PVueEntreprise)
                value = 0
            Else
                ENT_ORGANISME_1.Identifiant = value
            End If
            HSelIde.Value = value.ToString
            ENT_INTERLOCUTEUR_2.Identifiant = value
        End Set
    End Property

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        ConteneurVues.Width = New Unit(1150)
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueDefinition"
                MultiOnglets.SetActiveView(VueDefinition)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(0) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        ConteneurVues.Width = New Unit(1150)
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueDefinition"
                Select Case e.ObjetAppelant
                    Case 1
                        ENT_ORGANISME_1.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                    Case 2
                        ENT_INTERLOCUTEUR_2.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                End Select
                MultiOnglets.SetActiveView(VueDefinition)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(0) = MultiOnglets.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles ENT_ORGANISME_1.AppelTable, ENT_INTERLOCUTEUR_2.AppelTable

        Select Case e.ObjetAppelant
            Case 1, 2
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDefinition.ID
            Case Else
                Exit Sub
        End Select
        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#BFB8AB")
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
        End If
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles ENT_ORGANISME_1.MessageDialogue, ENT_INTERLOCUTEUR_2.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        ConteneurVues.Width = New Unit(1150)
        CadreSaisie.BackImageUrl = ""
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#216B68")
        PopupMsg.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case Is = "MajDossier"
                If e.NumeroObjet = 1 And e.ReponseMsg = "OK" Then
                    RaiseEvent RetourEventHandler(Me, e)
                End If
        End Select
        ConteneurVues.Width = New Unit(1150)
        CadreSaisie.BackColor = WebFct.ConvertCouleur("#A8BBB8")
        MultiOnglets.SetActiveView(VueDefinition)
    End Sub

End Class
