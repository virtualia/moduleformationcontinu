﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele

Public Class VReferentielFormation
    Inherits Virtualia.Net.Controles.SystemeReferences
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Private WsActSelection As System.Action(Of Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)

    Public Property SiListeMenuVisible As Boolean
        Get
            Return V_SiMajMenuVisible
        End Get
        Set(ByVal value As Boolean)
            MenuRef.Visible = value
            RefVirtualia.SiCommandesVisible = value
            If value = False Then
                For Each Noeud As TreeNode In TreeDicoRef.Nodes
                    Noeud.PopulateOnDemand = False
                Next
            End If
            V_SiMajMenuVisible = value
        End Set
    End Property

    Public WriteOnly Property Act_Selection As Action(Of Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)
        Set(value As System.Action(Of Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs))
            WsActSelection = value
        End Set
    End Property

    Public Sub Initialiser()
        RefVirtualia.V_PointdeVue = Me.V_PointdeVue
        RefVirtualia.V_NomTable = Me.V_NomTable
        If V_PointdeVue <> VI.PointdeVue.PVueGeneral Then
            RefVirtualia.InitialiserPointdeVue(Me.V_PointdeVue)
        End If
    End Sub

    Private Sub ChargerMenu_GeneralFOR(ByVal Noeud As TreeNode)
        Dim NewNoeud As TreeNode
        Dim VImage = "~/Images/Armoire/FicheBleue.bmp"
        Select Case Noeud.Value
            Case "Entreprise"
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Liste des organismes de formation", VI.PointdeVue.PVueEntreprise & VI.Tild)
            Case "Facture"
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Affectation budgétaire", VI.PointdeVue.PVueGeneral & VI.Tild & "Affectation budgétaire" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Nature de facture", VI.PointdeVue.PVueGeneral & VI.Tild & "Nature de facture" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Prise en charge financière", VI.PointdeVue.PVueGeneral & VI.Tild & "Prise en charge financière" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Statut de la commande", VI.PointdeVue.PVueGeneral & VI.Tild & "Statut de la commande" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Statut de la créance", VI.PointdeVue.PVueGeneral & VI.Tild & "Statut de la créance" & VI.Tild)
            Case "Inscription"
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Avis", VI.PointdeVue.PVueGeneral & VI.Tild & "Avis" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Inscriptions - comptabilisation", VI.PointdeVue.PVueGeneral & VI.Tild & "Inscriptions - comptabilisation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Inscriptions - facturation", VI.PointdeVue.PVueGeneral & VI.Tild & "Inscriptions - facturation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Inscriptions - validation", VI.PointdeVue.PVueGeneral & VI.Tild & "Inscriptions - validation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Motif de non participation au stage", VI.PointdeVue.PVueGeneral & VI.Tild & "Motif de non participation au stage" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Ordre de priorité à l'inscription", VI.PointdeVue.PVueGeneral & VI.Tild & "Ordre de priorité à l'inscription" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Suivi présence au stage", VI.PointdeVue.PVueGeneral & VI.Tild & "Suivi présence au stage" & VI.Tild)
            Case "Organisme"
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Secteur d'activité", VI.PointdeVue.PVueGeneral & VI.Tild & "Secteur d'activité" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Tiers comptable", VI.PointdeVue.PVueGeneral & VI.Tild & "Tiers comptable" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Tranche d'effectif", VI.PointdeVue.PVueGeneral & VI.Tild & "Tranche d'effectif" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Type d'organisme", VI.PointdeVue.PVueGeneral & VI.Tild & "Type d'organisme" & VI.Tild)
            Case "Plan"
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Plan de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Plan de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Domaine de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Domaine de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Thème", VI.PointdeVue.PVueGeneral & VI.Tild & "Thème" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Sous-Thème", VI.PointdeVue.PVueGeneral & VI.Tild & "Sous-Thème" & VI.Tild)
            Case "Stage"
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Action de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Action de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Classement du stage", VI.PointdeVue.PVueGeneral & VI.Tild & "Classement du stage" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Contexte du stage", VI.PointdeVue.PVueGeneral & VI.Tild & "Contexte du stage" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Cursus de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Cursus de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Filière de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Filière de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Lieu de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Lieu de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Méthode pédagogique", VI.PointdeVue.PVueGeneral & VI.Tild & "Méthode pédagogique" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Mode calcul coût formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Mode calcul coût formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Module de formation", VI.PointdeVue.PVueGeneral & VI.Tild & "Module de formation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Niveau", VI.PointdeVue.PVueGeneral & VI.Tild & "Niveau" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Niveau de stage", VI.PointdeVue.PVueGeneral & VI.Tild & "Niveau de stage" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Qualification", VI.PointdeVue.PVueGeneral & VI.Tild & "Qualification" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Type évaluation", VI.PointdeVue.PVueGeneral & VI.Tild & "Type évaluation" & VI.Tild)
                NewNoeud = V_AjouterNoeudValeur(TreeDicoRef, Noeud, "Type de stage", VI.PointdeVue.PVueGeneral & VI.Tild & "Type de stage" & VI.Tild)
        End Select
        For Each NewNoeud In Noeud.ChildNodes
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            NewNoeud.ImageUrl = VImage
        Next
    End Sub

    Protected Sub CmdAllerA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CmdAllerA.Click
        If VAllerA.Text = "" Or VAllerA.Text = "*" Then
            Exit Sub
        End If
        Dim Noeud As TreeNode
        Dim ValCourante As String = V_RechercheCourante
        Dim NoCourant As Integer = V_IndexCourant

        If ValCourante <> VAllerA.Text Then
            NoCourant = -1
        End If
        NoCourant += 1
        V_RechercheCourante = VAllerA.Text
        V_IndexCourant = NoCourant
        Noeud = V_AlleraInfoDico(TreeDicoRef, NoCourant, False, VAllerA.Text)
        If Noeud IsNot Nothing Then
            Noeud.Select()
            Call V_SelectSurDicoRef(TreeDicoRef.FindNode(Noeud.ValuePath))
            RefVirtualia.V_PointdeVue = Me.V_PointdeVue
            RefVirtualia.V_NomTable = Me.V_NomTable
        End If
    End Sub

    Protected Sub TreeDicoRef_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles TreeDicoRef.TreeNodePopulate
        If V_SiMajMenuVisible = False Then
            Exit Sub
        End If
        TreeDicoRef.MaxDataBindDepth = 2
        TreeDicoRef.ImageSet = System.Web.UI.WebControls.TreeViewImageSet.BulletedList4
        If e.Node.ChildNodes.Count = 0 Then
            Select Case e.Node.Depth
                Case Is = 0
                    Call ChargerMenu_GeneralFOR(e.Node)
                    e.Node.Expand()
            End Select
        End If
    End Sub

    Protected Sub TreeDicoRef_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeDicoRef.SelectedNodeChanged
        Call V_SelectSurDicoRef(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode)
        RefVirtualia.V_PointdeVue = Me.V_PointdeVue
        RefVirtualia.V_NomTable = Me.V_NomTable
    End Sub

    Private Sub RefVirtualia_ValeurSelectionnee(sender As Object, e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        If WsActSelection IsNot Nothing Then
            WsActSelection(e)
        End If
    End Sub

    Private Sub RefVirtualia_RetourEventHandler(sender As Object, e As EventArgs) Handles RefVirtualia.RetourEventHandler
        If WsActSelection IsNot Nothing Then
            WsActSelection(Nothing)
        End If
    End Sub
End Class