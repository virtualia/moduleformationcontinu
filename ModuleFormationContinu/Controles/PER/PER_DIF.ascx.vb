﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_DIF
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = 93
    Private WsNomTable As String = "PER_DIF"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_DIF
    Private WsLstFichesDIF As List(Of Virtualia.TablesObjet.ShemaPER.PER_DIF)

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(2)
                ColHisto.Add(4)
                ColHisto.Add(5)
                ColHisto.Add(3)
                ColHisto.Add(8)
                ColHisto.Add(9)
                ColHisto.Add(7)
                V_CacheColHisto = ColHisto

                Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
                Dossier = V_WebFonction.PointeurDossier(value)
                If Dossier IsNot Nothing Then
                    Dossier.V_ActualiserUnObjet(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, "PER_DIF") = WsNumObjet
                End If
                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune situation")
        LstLibels.Add("Une situation")
        LstLibels.Add("situations")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("Année")
        LstColonnes.Add("DIF annuel")
        LstColonnes.Add("Heures effectuées")
        LstColonnes.Add("Sur temps de travail")
        LstColonnes.Add("Hors temps de travail")
        LstColonnes.Add("Solde annuel")
        LstColonnes.Add("Cumul sur temps de travail")
        LstColonnes.Add("Cumul hors temps de travail")
        LstColonnes.Add("Solde du DIF")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            WsLstFichesDIF = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_DIF)(V_ListeFiches("", ""))
            If WsLstFichesDIF IsNot Nothing AndAlso WsLstFichesDIF.Count > 0 Then
                For Each FicheDIF In WsLstFichesDIF
                    Call VerifierDIF(FicheDIF)
                Next
            End If
            ListeGrille.V_ValeurNulle = "0 h 00"
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiAutoPostBack = Not CadreCmdOK.Visible
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserDIF()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim LstErreurs As List(Of String) = SiMajPossible()
        If LstErreurs Is Nothing Then
            Call V_MajFiche()
            Call ActualiserDIF()
            Exit Sub
        End If
        Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Mise à jour impossible", LstErreurs))
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD00.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange,
        InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))

        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim IndiceI As Integer

        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        For IndiceI = 0 To 9
            ListeGrille.Centrage_Colonne(IndiceI) = 1
        Next IndiceI
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiAnnuel.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiAnnuel.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiAnnuel.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiCumul.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiCumul.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiCumul.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            If IndiceI > 1 Then
                VirControle.V_SiEnLectureSeule = True
            End If
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Sub ActualiserDIF()
        Dim Ensemble As Virtualia.Metier.Formation.EnsembleDossiers
        Dim Dossier As Virtualia.Metier.Formation.DossierFormation
        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsemblePER
        Ensemble.Identifiant = V_Identifiant
        Dossier = Ensemble.ItemDossier(V_Identifiant)
        If Dossier IsNot Nothing Then
            Call Dossier.VerifierEtMajDIF(V_CacheMaj.Item(0))
        End If
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Private Sub VerifierDIF(ByVal Fiche As Virtualia.TablesObjet.ShemaPER.PER_DIF)
        Dim TotalDIF As Double = 0
        Dim NbEffectuées As Double = 0
        Dim SoldeDIF As String = ""
        Dim NbTempsTravail As Double = 0
        Dim NbHorsTravail As Double = 0
        Dim DateDIF As Double = 0
        Dim Maxi120H As Double = 7200

        For Each FichePER In WsLstFichesDIF
            If FichePER.Date_Valeur_ToDate.Year <= Fiche.Date_Valeur_ToDate.Year Then
                TotalDIF += Val(V_WebFonction.ViRhDates.CalcHeure(FichePER.DIF, "", 1))
                NbEffectuées += Val(V_WebFonction.ViRhDates.CalcHeure(FichePER.NombreHeures_Effectuees, "", 1))
                NbTempsTravail += Val(V_WebFonction.ViRhDates.CalcHeure(FichePER.NombreHeures_Sur_TempsTravail, "", 1))
                NbHorsTravail += Val(V_WebFonction.ViRhDates.CalcHeure(FichePER.NombreHeures_Hors_TempsTravail, "", 1))
            End If
        Next
        Select Case TotalDIF - NbEffectuées
            Case Is > Maxi120H
                SoldeDIF = V_WebFonction.ViRhDates.CalcHeure(LTrim(Str(Maxi120H)), "", 2)
            Case Else
                Select Case TotalDIF - NbEffectuées
                    Case Is >= 0
                        SoldeDIF = V_WebFonction.ViRhDates.CalcHeure(CStr(TotalDIF - NbEffectuées), "", 2)
                    Case Is < 0
                        SoldeDIF = "-" & V_WebFonction.ViRhDates.CalcHeure(CStr(NbEffectuées - TotalDIF), "", 2)
                End Select
        End Select
        Fiche.Solde_DIF = SoldeDIF
        Select Case NbEffectuées
            Case Is >= 0
                Fiche.NombreHeures_Cumulees = V_WebFonction.ViRhDates.CalcHeure(CStr(NbEffectuées), "", 2)
            Case Else
                Fiche.NombreHeures_Cumulees = "-" & V_WebFonction.ViRhDates.CalcHeure(CStr(-NbEffectuées), "", 2)
        End Select
        Select Case NbTempsTravail
            Case Is >= 0
                Fiche.Cumul_Sur_TempsTravail = V_WebFonction.ViRhDates.CalcHeure(CStr(NbTempsTravail), "", 2)
            Case Else
                Fiche.Cumul_Sur_TempsTravail = "-" & V_WebFonction.ViRhDates.CalcHeure(CStr(-NbTempsTravail), "", 2)
        End Select
        Select Case NbHorsTravail
            Case Is >= 0
                Fiche.Cumul_Hors_TempsTravail = V_WebFonction.ViRhDates.CalcHeure(CStr(NbHorsTravail), "", 2)
            Case Else
                Fiche.Cumul_Hors_TempsTravail = "-" & V_WebFonction.ViRhDates.CalcHeure(CStr(-NbHorsTravail), "", 2)
        End Select

        TotalDIF = Val(V_WebFonction.ViRhDates.CalcHeure(Fiche.DIF, "", 1)) - Val(V_WebFonction.ViRhDates.CalcHeure(Fiche.NombreHeures_Effectuees, "", 1))
        Select Case TotalDIF
            Case >= 0
                Fiche.SoldeAnnuel = V_WebFonction.ViRhDates.CalcHeure(CStr(TotalDIF), "", 2)
            Case Else
                Fiche.SoldeAnnuel = "-" & V_WebFonction.ViRhDates.CalcHeure(CStr(-TotalDIF), "", 2)
        End Select

    End Sub

    Private Function SiMajPossible() As List(Of String)
        WsLstFichesDIF = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_DIF)(V_ListeFiches("", ""))
        If WsLstFichesDIF Is Nothing OrElse WsLstFichesDIF.Count = 0 Then
            Return Nothing
        End If
        For Each FicheDIF In WsLstFichesDIF
            Call VerifierDIF(FicheDIF)
        Next
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim TableauErreur As List(Of String) = Nothing
        Dim NbSaisie As Double = 0
        NbSaisie = Val(V_WebFonction.ViRhDates.CalcHeure(CacheDonnee(1), "", 1))
        If V_SiCreation = False Then
            NbSaisie -= Val(V_WebFonction.ViRhDates.CalcHeure(WsLstFichesDIF.Item(0).DIF, "", 1))
        End If
        Dim TotalDIF As Double = Val(V_WebFonction.ViRhDates.CalcHeure(WsLstFichesDIF.Item(0).Solde_DIF, "", 1))
        Dim Maxi120H As Double = 7200
        If TotalDIF + NbSaisie > Maxi120H Then
            TableauErreur = New List(Of String)
            TableauErreur.Add("Contrôle préalable à l'enregistrement du DIF")
            TableauErreur.Add("Le total du DIF est actuellement de " & WsLstFichesDIF.Item(0).Solde_DIF)
            TableauErreur.Add("Vous ne devez pas dépasser un solde de plus de 120 h 00")
            Return TableauErreur
        End If
        Return Nothing
    End Function

End Class

