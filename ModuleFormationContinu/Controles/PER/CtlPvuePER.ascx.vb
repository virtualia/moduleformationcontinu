﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlPvuePER
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateIde As String = "MenuPERIde"

    Public Property V_Identifiant As Integer
        Get
            If Me.ViewState(WsNomStateIde) Is Nothing Then
                Return 0
            End If
            Return CType(Me.ViewState(WsNomStateIde), Integer)
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIde), Integer) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
            Call AfficherFenetrePER()
        End If
    End Sub

    Private Sub AfficherFenetrePER()
        Dim Ide_Dossier As Integer = V_Identifiant
        If Ide_Dossier = 0 Then
            Exit Sub
        End If
        Select Case MultiPER.ActiveViewIndex
            Case 0
                PER_DEMANDE_81.Identifiant = Ide_Dossier
            Case 1
                PER_DIPLOME_6.Identifiant = Ide_Dossier
            Case 2
                PER_BREVETPRO_34.Identifiant = Ide_Dossier
            Case 3
                PER_SPECIALITE_53.Identifiant = Ide_Dossier
            Case 4
                PER_AFFECTATION_17.Identifiant = Ide_Dossier
            Case 5
                PER_PLANNING.Identifiant = Ide_Dossier
            Case 6
                PER_ABSENCE_15.Identifiant = Ide_Dossier
            Case 7
                PER_STATUT_12.Identifiant = Ide_Dossier
            Case 8
                PER_POSITION_13.Identifiant = Ide_Dossier
            Case 9
                PER_GRADE_14.Identifiant = Ide_Dossier
            Case 10
                PER_ADRESSEPRO_24.Identifiant = Ide_Dossier
            Case 11
                PER_COLLECTIVITE_26.Identifiant = Ide_Dossier
        End Select
    End Sub

    Private Sub PER_MENU_Menu_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles PER_MENU.Menu_Click
        If IsNumeric(e.Parametre) Then
            MultiPER.ActiveViewIndex = CInt(e.Parametre)
        End If
    End Sub

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueAffectation"
                MultiPER.SetActiveView(VueAffectation)
            Case "VueAbsence"
                MultiPER.SetActiveView(VueAbsence)
            Case "VueAdrPro"
                MultiPER.SetActiveView(VueAdrPro)
            Case "VueDemande"
                MultiPER.SetActiveView(VueDemande)
            Case "VueDiplome"
                MultiPER.SetActiveView(VueDiplome)
            Case "VueEtablissement"
                MultiPER.SetActiveView(VueEtablissement)
            Case "VueGrade"
                MultiPER.SetActiveView(VueGrade)
            Case "VuePosition"
                MultiPER.SetActiveView(VuePosition)
            Case "VueQualification"
                MultiPER.SetActiveView(VueQualification)
            Case "VueSpecialite"
                MultiPER.SetActiveView(VueSpecialite)
            Case "VueStatut"
                MultiPER.SetActiveView(VueStatut)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiPER.ActiveViewIndex
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Select Case WebFct.PointeurContexte.SysRef_IDVueRetour
            Case "VueAffectation"
                PER_AFFECTATION_17.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAffectation)
            Case "VueAbsence"
                PER_ABSENCE_15.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAbsence)
            Case "VueAdrPro"
                PER_ADRESSEPRO_24.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueAdrPro)
            Case "VueDemande"
                PER_DEMANDE_81.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueDemande)
            Case "VueDiplome"
                PER_DIPLOME_6.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueDiplome)
            Case "VueEtablissement"
                PER_COLLECTIVITE_26.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueEtablissement)
            Case "VueGrade"
                PER_GRADE_14.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueGrade)
            Case "VuePosition"
                PER_POSITION_13.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VuePosition)
            Case "VueQualification"
                PER_BREVETPRO_34.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueQualification)
            Case "VueStatut"
                PER_STATUT_12.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueStatut)
            Case "VueSpecialite"
                PER_SPECIALITE_53.Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
                MultiPER.SetActiveView(VueSpecialite)
        End Select
        WebFct.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiPER.ActiveViewIndex
    End Sub

    Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    Handles PER_AFFECTATION_17.AppelTable, PER_ABSENCE_15.AppelTable, PER_BREVETPRO_34.AppelTable, PER_DIPLOME_6.AppelTable, _
        PER_STATUT_12.AppelTable, PER_POSITION_13.AppelTable, PER_GRADE_14.AppelTable, PER_SPECIALITE_53.AppelTable, PER_DEMANDE_81.AppelTable

        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaOrganigramme
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAffectation.ID
            Case VI.ObjetPer.ObaAbsence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAbsence.ID
            Case VI.ObjetPer.ObaStatut
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueStatut.ID
            Case VI.ObjetPer.ObaActivite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VuePosition.ID
            Case VI.ObjetPer.ObaGrade
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueGrade.ID
            Case VI.ObjetPer.ObaSociete
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueEtablissement.ID
            Case VI.ObjetPer.ObaDiplome
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDiplome.ID
            Case VI.ObjetPer.ObaAdrPro
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueAdrPro.ID
            Case VI.ObjetPer.ObaCompetence
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueQualification.ID
            Case VI.ObjetPer.ObaSpecialite
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueSpecialite.ID
            Case VI.ObjetPer.ObaDemandeFormation
                WebFct.PointeurContexte.SysRef_IDVueRetour = VueDemande.ID
            Case Else
                Exit Sub
        End Select

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        Select Case e.ObjetAppelant
            Case VI.ObjetPer.ObaCivil
                If e.ControleAppelant = "Dontab06" Then
                    RefVirtualia.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
                End If
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
            Case VI.ObjetPer.ObaGrade
                WebFct.PointeurContexte.TsTampon_ArrayList = Nothing
                RefVirtualia.V_Appelant(e.ObjetAppelant, e.DatedeValeur) = e.ControleAppelant
            Case Else
                RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        End Select
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatut)
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePosition)
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGrade)
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAbsence)
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation)
                    Case VI.ObjetPer.ObaDiplome
                        PER_DIPLOME_6.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueDiplome)
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEtablissement)
                    Case VI.ObjetPer.ObaCompetence
                        PER_BREVETPRO_34.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueQualification)
                    Case VI.ObjetPer.ObaSpecialite
                        PER_SPECIALITE_53.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueSpecialite)
                    Case VI.ObjetPer.ObaDemandeFormation
                        PER_DEMANDE_81.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueDemande)
                End Select
            Case Is = "SuppDossier"
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaStatut
                        PER_STATUT_12.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueStatut)
                    Case VI.ObjetPer.ObaActivite
                        PER_POSITION_13.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VuePosition)
                    Case VI.ObjetPer.ObaGrade
                        PER_GRADE_14.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueGrade)
                    Case VI.ObjetPer.ObaAbsence
                        PER_ABSENCE_15.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAbsence)
                    Case VI.ObjetPer.ObaOrganigramme
                        PER_AFFECTATION_17.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueAffectation)
                    Case VI.ObjetPer.ObaDiplome
                        PER_DIPLOME_6.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueDiplome)
                    Case VI.ObjetPer.ObaSociete
                        PER_COLLECTIVITE_26.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueEtablissement)
                    Case VI.ObjetPer.ObaCompetence
                        PER_BREVETPRO_34.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueQualification)
                    Case VI.ObjetPer.ObaSpecialite
                        PER_SPECIALITE_53.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueSpecialite)
                    Case VI.ObjetPer.ObaDemandeFormation
                        PER_DEMANDE_81.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiPER.SetActiveView(VueDemande)
                End Select
        End Select
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_STATUT_12.MessageDialogue, PER_POSITION_13.MessageDialogue, PER_GRADE_14.MessageDialogue, PER_ABSENCE_15.MessageDialogue,
    PER_AFFECTATION_17.MessageDialogue, PER_COLLECTIVITE_26.MessageDialogue, PER_BREVETPRO_34.MessageDialogue, PER_DIPLOME_6.MessageDialogue,
    PER_SPECIALITE_53.MessageDialogue, PER_DEMANDE_81.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_STATUT_12.MessageSaisie, PER_POSITION_13.MessageSaisie, PER_GRADE_14.MessageSaisie, PER_ABSENCE_15.MessageSaisie,
    PER_AFFECTATION_17.MessageSaisie, PER_COLLECTIVITE_26.MessageSaisie, PER_BREVETPRO_34.MessageSaisie, PER_DIPLOME_6.MessageSaisie, PER_SPECIALITE_53.MessageSaisie,
    PER_DEMANDE_81.MessageSaisie
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
    End Sub

End Class