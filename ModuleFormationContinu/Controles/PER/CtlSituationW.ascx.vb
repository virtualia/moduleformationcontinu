﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlSituationW
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomState As String = "Situation"
    Private Const IFormation As Integer = 0
    Private Const IBilan As Integer = 1
    Private Const IDIF As Integer = 2
    Private Const IGestionPER As Integer = 3

    Public ReadOnly Property V_Webfonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property
    Public Property V_Identifiant As Integer
        Get
            Dim VCache As Integer
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), Integer)
                Return VCache
            End If
            Return 0
        End Get
        Set(ByVal value As Integer)
            Dim VCache As Integer
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), Integer)
                If VCache = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomState)
            Else
                VCache = value
            End If
            VCache = value
            EtiIdentite.Text = ""
            Me.ViewState.Add(WsNomState, VCache)
            Dim Ensemble As Virtualia.Metier.Formation.EnsembleDossiers
            Dim Dossier As Virtualia.Metier.Formation.DossierFormation
            Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsemblePER
            Dossier = Ensemble.ItemDossier(value)
            If Dossier Is Nothing Then
                Ensemble.Identifiant = value
                Dossier = Ensemble.ItemDossier(value)
            End If
            If Dossier IsNot Nothing Then
                EtiIdentite.Text = Dossier.Qualite & Strings.Space(1) & Dossier.Nom & Strings.Space(1) & Dossier.Prenom & " (Ide V : " & Dossier.Identifiant & ")"
            End If
        End Set
    End Property

    Public Property V_BackColor() As System.Drawing.Color
        Get
            Return CadreGeneral.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            CadreGeneral.BackColor = value
        End Set
    End Property

    Public Property V_Emetteur As String
        Get
            Return V_Webfonction.PointeurContexte.ModuleActif
        End Get
        Set(value As String)
            V_Webfonction.PointeurContexte.ModuleActif = value
        End Set
    End Property
    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Select Case MultiVues.ActiveViewIndex
            Case IFormation
                PER_SESSION_29.Identifiant = V_Identifiant
            Case IBilan
                CtrlStatIndiv.Identifiant = V_Identifiant
            Case IDIF
                PER_DIF_93.Identifiant = V_Identifiant
            Case IGestionPER
                CtrlVuesPER.V_Identifiant = V_Identifiant
        End Select
    End Sub

    Private Sub MenuChoix_MenuItemClick(sender As Object, e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuChoix.MenuItemClick
        CadreVues.BackColor = V_Webfonction.ConvertCouleur("#5E9598")
        Select Case e.Item.Value
            Case "FORMATION"
                MultiVues.SetActiveView(VuePerSession)
            Case "BILAN"
                MultiVues.SetActiveView(VueStatIndiv)
            Case "DIF"
                MultiVues.SetActiveView(VuePerDif)
            Case "GESTION"
                MultiVues.SetActiveView(VueGestion)
        End Select
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs(V_Emetteur, 0, "", "")
        ReponseRetour(Evenement)
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        Select Case e.Emetteur
            Case Is = "SuppFiche"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaDIF
                        PER_DIF_93.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiVues.SetActiveView(VuePerDif)
                End Select
            Case Is = "SuppDossier"
                Exit Sub
            Case Is = "MajDossier"
                Select Case e.NumeroObjet
                    Case VI.ObjetPer.ObaDIF
                        PER_DIF_93.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiVues.SetActiveView(VuePerDif)
                End Select
        End Select
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    Handles PER_DIF_93.MessageDialogue
        MsgVirtualia.AfficherMessage = e
        MultiVues.SetActiveView(VueMessage)
        V_Webfonction.PointeurContexte.Fenetre_VueActive(VI.CategorieRH.InfosPersonnelles) = MultiVues.ActiveViewIndex
    End Sub

End Class