﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_DIF" Codebehind="PER_DIF.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="1050px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="1000px" SiColonneSelect="true" SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="420px" Width="600px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                    Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Bottom">
                            <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                        Width="160px" CellPadding="0" CellSpacing="0"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                Tooltip="Nouvelle fiche" >
                                            </asp:Button>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                            <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                Tooltip="Supprimer la fiche" >
                                            </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>    
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Bottom">
                            <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        CellPadding="0" CellSpacing="0" Visible="false" >
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                            </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>    
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="600px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Droit individuel à la formation / DIF" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="600px">
               <asp:TableRow>
                   <asp:TableCell>
                       <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="93" V_Information="0" DonTabIndex="1" />
                   </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="1" V_SiDonneeDico="true"
                           DonWidth="120px" DonTabIndex="2"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow Height="40px">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiAnnuel" runat="server" Height="20px" Width="300px"
                           Text="Heures annuelles"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="2" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="3"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="4" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="4"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="5" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="5"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="6"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow Height="40px">
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiCumul" runat="server" Height="20px" Width="300px"
                           Text="Heures cumulées"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="6" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="7"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="8" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="8"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="9" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="9"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                           V_PointdeVue="1" V_Objet="93" V_Information="7" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="10"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
               </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>       
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>