﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_SESSION
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = VI.ObjetPer.ObaFormation
    Private WsNomTable As String = "PER_SESSION"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_SESSION

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(11)   'Date de fin
                ColHisto.Add(4)    'Intitulé
                ColHisto.Add(6)    'Suite donnée
                ColHisto.Add(2)     'Domaine
                ColHisto.Add(8)     'Durée en heures
                V_CacheColHisto = ColHisto

                Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
                Dossier = V_WebFonction.PointeurDossier(value)
                If Dossier IsNot Nothing Then
                    Dossier.V_ActualiserUnObjet(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, "PER_SESSION") = WsNumObjet
                End If
                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune session effecuée")
        LstLibels.Add("Une session effectuée")
        LstLibels.Add("sessions effectuées")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("du")
        LstColonnes.Add("au")
        LstColonnes.Add("intitulé du stage")
        LstColonnes.Add("suivi")
        LstColonnes.Add("domaine")
        LstColonnes.Add("jours")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION) = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION)(V_ListeFiches("", ""))
            Dim LstSuivis As List(Of String) = Nothing
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                LstSuivis = (From instance In LstFiches Order By instance.Suite_donnee Select instance.Suite_donnee).Distinct.ToList
                If LstSuivis IsNot Nothing Then
                    LstSuivis.Add("")
                End If
            End If
            ListeGrille.V_Filtres(LstLibels) = LstSuivis
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            VirCoche.V_SiAutoPostBack = Not (CadreCmdOK.Visible)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
            Else
                If CacheDonnee.Item(NumInfo).ToLower = "oui" Then
                    VirCoche.V_Check = True
                Else
                    VirCoche.V_Check = False
                End If
            End If
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD00.ValeurChange, InfoD11.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH05.ValeurChange,
    InfoH08.ValeurChange, InfoH10.ValeurChange, InfoH12.ValeurChange, InfoH13.ValeurChange, InfoH14.ValeurChange, InfoH15.ValeurChange, InfoH16.ValeurChange,
    InfoH22.ValeurChange, InfoH25.ValeurChange, InfoH30.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles DonTab01.AppelTable, DonTab02.AppelTable, _
            DonTab03.AppelTable, DonTab04.AppelTable, DonTab06.AppelTable, DonTab07.AppelTable, DonTab09.AppelTable, _
            DonTab18.AppelTable, DonTab19.AppelTable, DonTab20.AppelTable, DonTab23.AppelTable, _
            DonTab24.AppelTable, DonTab26.AppelTable, DonTab27.AppelTable, _
            DonTab28.AppelTable, DonTab29.AppelTable, DonTab31.AppelTable, DonTab32.AppelTable, DonTab35.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee.Item(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                Else
                    If CacheDonnee.Item(NumInfo) <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Sub Coche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Coche33.ValeurChange, Coche34.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 0
        ListeGrille.Centrage_Colonne(3) = 0
        ListeGrille.Centrage_Colonne(4) = 0
        ListeGrille.Centrage_Colonne(5) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiComplements.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiComplements.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiComplements.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiCouts.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiCouts.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiCouts.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiSuivi.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        EtiSuivi.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiSuivi.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = MyBase.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = MyBase.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirExperte As Controles_VCoupleEtiquetteExperte
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "ExprH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirExperte = CType(Ctl, Controles_VCoupleEtiquetteExperte)
            VirExperte.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private Sub CmdExcel_Click(sender As Object, e As EventArgs) Handles CmdExcel.Click
        Dim FichierCsv As String
        Dim FluxTeleChargement As Byte()
        Dim LstFiches As List(Of Systeme.MetaModele.VIR_FICHE)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim Chaine As System.Text.StringBuilder

        FichierCsv = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirRepertoireTemporaire
        FichierCsv &= "\" & V_WebFonction.PointeurUtilisateur.V_NomdeConnexion & ".csv"

        LstFiches = V_ListeFiches("", "")
        If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
            Exit Sub
        End If
        FicStream = New System.IO.FileStream(FichierCsv, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        Chaine = New System.Text.StringBuilder
        Chaine.Append("Du" & VI.PointVirgule)
        Chaine.Append("Au" & VI.PointVirgule)
        Chaine.Append("Intitulé du stage" & VI.PointVirgule)
        Chaine.Append("Suivi" & VI.PointVirgule)
        Chaine.Append("Non participation" & VI.PointVirgule)
        Chaine.Append("Domaine" & VI.PointVirgule)
        Chaine.Append("Thème" & VI.PointVirgule)
        Chaine.Append("Nb Jours" & VI.PointVirgule)
        Chaine.Append("Nb heures")
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()

        For Each FichePer In LstFiches
            WsFiche = CType(FichePer, Virtualia.TablesObjet.ShemaPER.PER_SESSION)
            Chaine.Append(WsFiche.Date_du_stage & VI.PointVirgule)
            Chaine.Append(WsFiche.Date_de_Fin & VI.PointVirgule)
            If WsFiche.Intitule_de_la_session <> "" Then
                Chaine.Append(WsFiche.Intitule_de_la_session & VI.PointVirgule)
            Else
                Chaine.Append(WsFiche.Intitule & VI.PointVirgule)
            End If
            Chaine.Append(WsFiche.Suite_donnee & VI.PointVirgule)
            Chaine.Append(WsFiche.Motif_de_non_participation & VI.PointVirgule)
            Chaine.Append(WsFiche.Domaine & VI.PointVirgule)
            Chaine.Append(WsFiche.Theme & VI.PointVirgule)
            Chaine.Append(WsFiche.Duree & VI.PointVirgule)
            Chaine.Append(WsFiche.Duree_en_heures)
            FicWriter.WriteLine(Chaine.ToString)
            Chaine.Clear()
        Next
        FicWriter.Flush()
        FicWriter.Close()

        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(FichierCsv)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & V_WebFonction.PointeurUtilisateur.V_NomdeConnexion & ".csv" & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

End Class

