﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlPvuePER
    
    '''<summary>
    '''Contrôle CadreGlobal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGlobal As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PER_MENU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_MENU As Global.Virtualia.Net.VMenuCommun
    
    '''<summary>
    '''Contrôle CellulePER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellulePER As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle MultiPER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiPER As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle VueDemande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDemande As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DEMANDE_81.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DEMANDE_81 As Global.Virtualia.Net.Fenetre_PER_DEMANDE_FORMATION
    
    '''<summary>
    '''Contrôle VueDiplome.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDiplome As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DIPLOME_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DIPLOME_6 As Global.Virtualia.Net.Fenetre_PER_DIPLOME
    
    '''<summary>
    '''Contrôle VueQualification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueQualification As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_BREVETPRO_34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_BREVETPRO_34 As Global.Virtualia.Net.Fenetre_PER_BREVETPRO
    
    '''<summary>
    '''Contrôle VueSpecialite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSpecialite As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_SPECIALITE_53.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_SPECIALITE_53 As Global.Virtualia.Net.Fenetre_PER_SPECIALITE
    
    '''<summary>
    '''Contrôle VueAffectation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION_17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION_17 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION
    
    '''<summary>
    '''Contrôle VuePlanning.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePlanning As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_PLANNING.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_PLANNING As Global.Virtualia.Net.VPlanningAnnuel
    
    '''<summary>
    '''Contrôle VueAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAbsence As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ABSENCE_15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ABSENCE_15 As Global.Virtualia.Net.Fenetre_PER_ABSENCE
    
    '''<summary>
    '''Contrôle VueStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueStatut As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_STATUT_12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_STATUT_12 As Global.Virtualia.Net.Fenetre_PER_STATUT
    
    '''<summary>
    '''Contrôle VuePosition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePosition As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_POSITION_13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_POSITION_13 As Global.Virtualia.Net.Fenetre_PER_POSITION
    
    '''<summary>
    '''Contrôle VueGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueGrade As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_GRADE_14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_GRADE_14 As Global.Virtualia.Net.Fenetre_PER_GRADE
    
    '''<summary>
    '''Contrôle VueAdrPro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdrPro As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ADRESSEPRO_24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ADRESSEPRO_24 As Global.Virtualia.Net.Fenetre_PER_ADRESSEPRO
    
    '''<summary>
    '''Contrôle VueEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEtablissement As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_COLLECTIVITE_26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_COLLECTIVITE_26 As Global.Virtualia.Net.Fenetre_PER_COLLECTIVITE
    
    '''<summary>
    '''Contrôle CellMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMessage As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupMsg As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelMsgPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelMsgPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle MsgVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgVirtualia As Global.Virtualia.Net.Controles_VMessage
    
    '''<summary>
    '''Contrôle HPopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupMsg As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle CellReference.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellReference As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupReferentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupReferentiel As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelRefPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelRefPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle RefVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefVirtualia As Global.Virtualia.Net.VReferentiel
    
    '''<summary>
    '''Contrôle HPopupRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupRef As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle HSelIde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelIde As Global.System.Web.UI.WebControls.HiddenField
End Class
