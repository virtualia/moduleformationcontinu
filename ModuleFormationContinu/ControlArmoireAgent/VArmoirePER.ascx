﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VArmoirePER.ascx.vb" Inherits="Virtualia.Net.VArmoirePER" %>

<asp:Panel ID="PanelArmoire" runat="server" BackColor="#1C5151" HorizontalAlign="Center" Width="1050px" 
    BorderStyle="None" Font-Names="Trebuchet MS" Font-Italic="true" Style="margin-top:5px; margin-bottom:5px">
    <asp:Table ID="CadreArmoire" runat="server">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table ID="CadreListe" runat="server" Width="640px" Style="background-attachment:inherit; display:table-cell;">
                    <asp:TableRow>
                        <asp:TableCell Width="640px" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#6D9092">
                            <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Height="40px" Width="440px">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Label ID="EtiTypeArmoire" runat="server" Height="22px" Width="100px" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Text="Type d'armoire" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="margin-top: 1px; text-align: center" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:DropDownList ID="DropDownArmoire" runat="server" AutoPostBack="True" Height="22px" Width="510px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ID="CelluleStage" Width="640px" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#6D9092" Visible="false">
                            <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Height="40px" Width="440px">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Label ID="Label1" runat="server" Height="22px" Width="100px" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Text="Plan" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="margin-top: 1px; text-align: center" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:DropDownList ID="CboPlan" runat="server" AutoPostBack="True" Height="22px" Width="510px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="CboPlan_SelectedIndexChanged"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Label ID="Label2" runat="server" Height="22px" Width="100px" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Text="Stage" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="margin-top: 1px; text-align: center" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:DropDownList ID="CboStage" runat="server" AutoPostBack="True" Height="22px" Width="510px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="CboStage_SelectedIndexChanged" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow>
                        <asp:TableCell ID="CelluleLettre" runat="server" Width="650px" Visible="false">
                            <asp:Table ID="CadreLettre" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Left" Width="640px">
                                <asp:TableRow ID="RowButtonLettre">
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonA" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/A_Sel.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonB" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonC" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonD" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonE" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonF" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonG" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonH" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonI" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonJ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonK" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonL" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonM" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonN" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonO" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonP" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonQ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonR" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonS" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonT" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonU" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonV" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonW" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonX" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonY" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonZ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonAll" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/arobase.bmp" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="RowRechNom">
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="19">
                                        <asp:Label ID="EtiRecherche" runat="server" Text="Recherche par nom (Commençant par)" Height="16px" Width="420px" BackColor="Transparent" ForeColor="#D7FAF3" BorderStyle="None" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-right: 5px; font-style: oblique; text-indent: 5px; text-align: right;">
                                        </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="8">
                                        <asp:TextBox ID="DonRecherche" runat="server" Text="" Visible="true" AutoPostBack="true" BackColor="White" BorderColor="#B0E0D7" BorderStyle="InSet" BorderWidth="2px" ForeColor="#124545" Height="16px" Width="220px" MaxLength="35" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; font-style: normal; text-indent: 1px; text-align: left" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="PanelTree" runat="server" Width="685px" Height="600px" BackColor="Snow" ScrollBars="Auto" BorderStyle="None" Style="text-align: left; min-height: 200px">
                                <asp:TreeView ID="TreeListeDossier" runat="server" MaxDataBindDepth="2" ForeColor="#142425" ShowCheckBoxes="All"  Font-Bold="True" NodeIndent="10" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True" RootNodeStyle-ImageUrl="~/Images/Armoire/FicheBleue.bmp" RootNodeStyle-Font-Italic="False" style="width:100%; height:100%">
                                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                                </asp:TreeView>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="620px" HorizontalAlign="Center">
                            <asp:Label ID="EtiStatus1" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table ID="CadrePER" runat="server" Width="300px" HorizontalAlign="Left" Style="margin-top: 2px; background-attachment: inherit; display: table-cell;">
                    <asp:TableRow>
                        <asp:TableCell Width="298px" HorizontalAlign="Center" VerticalAlign="Middle" Height="40px" BackColor="#6D9092">
                            <asp:Label ID="EtiSelection" runat="server" Height="22px" Width="250px" BackColor="Transparent" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <%--### LISTE ###--%>
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" BorderColor="#CCFFFF" BorderWidth="2px" BorderStyle="Inset" Height="600px"  Width="300px" BackColor="Snow" Wrap="true" Style="margin-top: 1px; vertical-align: top; overflow: hidden; text-align: left">
                                <asp:ListBox ID="VListePer" runat="server" AutoPostBack="true" BackColor="Snow" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="display: table-cell; font-style: normal; width:100%; height:100%" />
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="300px" HorizontalAlign="Center">
                            <asp:Label ID="EtiStatus2" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" VerticalAlign="Top" HorizontalAlign="Center" Height="20px">
                <asp:Table ID="CadreBas" runat="server" HorizontalAlign="Left" Style="margin-top: 3px;" Width="300px" Visible="false">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="PanelCmdBas" runat="server" Width="200px" Height="25px" >
                                <asp:Table ID="CadreCmdCsv" runat="server" Height="20px" CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/General/Cmd_Std.bmp" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3" Width="150px" HorizontalAlign="Left" Style="margin-top: 3px;">
                                    <asp:TableRow VerticalAlign="Top">
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeCsv" runat="server" Text="Fichier Csv" Width="125px" Height="20px" BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="margin-left: 15px; text-align: left;" ToolTip="" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:HiddenField ID="HSelLettre" runat="server" Value="A" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>


