﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Systeme.Evenements
Imports Virtualia.Metier.Formation
Imports System.IO
Imports Virtualia.Net.ServiceServeur

Public Class VArmoirePER
    Inherits UserControl
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As DossierClickEventArgs)
    Public Event Dossier_Click As Dossier_ClickEventHandler
    Public Delegate Sub Selection_TypeArmoireEventHandler(ByVal sender As Object, ByVal e As SelectionTypeArmoireEventArgs)
    Public Event Selection_TypeArmoire As Selection_TypeArmoireEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private Const IDX_ALPHA_ACTIVITE As Integer = 0
    Private Const IDX_ALPHA_TOUS As Integer = 1
    Private Const IDX_ALPHA_STAGE As Integer = 2
    Private Const IDX_ALPHA_FORMATION As Integer = 3
    Private Const IDX_ALPHA_FORMATION_3 As Integer = 4
    Private Const IDX_AFFECTATION As Integer = 5
    Private Const IDX_SITADM As Integer = 6

    Public Class SelectionTypeArmoireEventArgs
        Inherits EventArgs
        Public Property TypeArmoire As String

        Public Sub New(ByVal stypearmoire As String)
            TypeArmoire = stypearmoire
        End Sub
    End Class

    Public ReadOnly Property V_Cache As CacheArmoire
        Get
            If ViewState.KeyExiste(CacheArmoire.KeyState) = False Then
                ViewState.AjouteValeur(CacheArmoire.KeyState, New CacheArmoire())
            End If
            Return ViewState.GetValeur(Of CacheArmoire)(CacheArmoire.KeyState)
        End Get
    End Property

    Private ReadOnly Property ControllerArmoire As SelectionAgent
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).Lecteur_Agent
        End Get
    End Property

    Protected Sub VDossier_Click(ByVal e As DossierClickEventArgs)
        RaiseEvent Dossier_Click(Me, e)
    End Sub

    Protected Overridable Sub VSelection_TypeArmoire(ByVal e As SelectionTypeArmoireEventArgs)
        RaiseEvent Selection_TypeArmoire(Me, e)
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        Call Remplir_ComboChoix()
        Call Remplir_ComboPlan()
        TreeListeDossier.ShowCheckBoxes = TreeNodeTypes.None
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
        MyBase.OnPreRender(e)
        If V_Cache.DoitCharger = True Then
            V_Cache.DoitCharger = False
            V_Cache.Sauve(ViewState)
            FaireListe(V_Cache.ListeSel)
        End If
    End Sub

    Public Sub InitialiserVariables()
        VListePer.Items.Clear()

        V_Cache.IdeSel = 0
        V_Cache.DataPathItemSel = ""
        V_Cache.CriteresSel.Clear()
        V_Cache.ListeIdeSel.Clear()
        V_Cache.Sauve(ViewState)

        EtiSelection.Text = ""
        EtiSelection.ToolTip = ""
        EtiStatus2.Text = ""
    End Sub

    Private Sub FaireListe(ByVal Index As Integer)
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur

        DonRecherche.Text = HSelLettre.Value
        Select Case Index
            Case IDX_ALPHA_ACTIVITE, IDX_ALPHA_TOUS, IDX_ALPHA_FORMATION, IDX_ALPHA_FORMATION_3, IDX_ALPHA_STAGE
                V_Cache.ObjetSel = ""
            Case IDX_AFFECTATION
                V_Cache.ObjetSel = "Organigramme"
            Case IDX_SITADM
                V_Cache.ObjetSel = "Carrieres"
        End Select

        V_Cache.Sauve(ViewState)

        CadrePER.Visible = (V_Cache.ObjetSel <> "")
        CelluleLettre.Visible = Not (V_Cache.ObjetSel <> "")

        If V_Cache.ObjetSel <> "" Then
            PanelArmoire.Width = New Unit(1010)
        Else
            PanelArmoire.Width = New Unit(700)
        End If

        ControllerArmoire.PointeurListeCourante = Nothing

        Select Case Index
            Case IDX_ALPHA_ACTIVITE 'Les dossiers en activité
                ControllerArmoire.PointeurListeCourante = ControllerArmoire.ObjetArmoire.ListeAlphabetique(HSelLettre.Value, True)
            Case IDX_ALPHA_TOUS 'Tous les dossiers
                ControllerArmoire.PointeurListeCourante = ControllerArmoire.ObjetArmoire.ListeAlphabetique(HSelLettre.Value, False)
            Case IDX_ALPHA_STAGE 'Tous les dossiers
                If CboPlan.SelectedIndex > 0 AndAlso CboStage.SelectedIndex > 0 Then
                    ControllerArmoire.PointeurListeCourante = Requeteur.LesPersonnesEnFormation(HSelLettre.Value, -1, CboPlan.SelectedItem.Text, CboStage.SelectedItem.Text)
                End If
            Case IDX_ALPHA_FORMATION 'Formation de l'année
                ControllerArmoire.PointeurListeCourante = Requeteur.LesPersonnesEnFormation(HSelLettre.Value, 0, "", "")
            Case IDX_ALPHA_FORMATION_3 'Formation les 3 dernieres annees
                ControllerArmoire.PointeurListeCourante = Requeteur.LesPersonnesEnFormation(HSelLettre.Value, 2, "", "")
            Case IDX_AFFECTATION 'Organigramme
                ControllerArmoire.PointeurListeCourante = ControllerArmoire.ObjetArmoire.AffectationsFonctionnelles("", "", "", "", "", "", "")
            Case IDX_SITADM 'Carrieres
                ControllerArmoire.PointeurListeCourante = ControllerArmoire.ObjetArmoire.SituationsAdministratives("", "", "", "", "")
        End Select

        If (Index = IDX_ALPHA_ACTIVITE OrElse Index = IDX_ALPHA_FORMATION OrElse Index = IDX_ALPHA_FORMATION_3 OrElse Index = IDX_ALPHA_TOUS OrElse Index = IDX_ALPHA_STAGE) Then
            Call FaireListeAlphabetique()
            Exit Sub
        End If
        Call FaireListeOrganisee(Index)
    End Sub

    Private Sub FaireListeOrganisee(ByVal Index As Integer)
        Dim LstVuesDyna As List(Of VirRequeteType)
        Dim IndiceA As Integer
        Dim RuptureN1 As String = ""
        Dim RuptureN2 As String = ""
        Dim RuptureN3 As String = ""
        Dim RuptureN4 As String = ""
        Dim RuptureN5 As String = ""
        Dim RuptureN6 As String = ""
        Dim RuptureN7 As String = ""
        Dim UrlImageN1 = "~/Images/Armoire/BleuFermer16.bmp"
        Dim UrlImageN2 = "~/Images/Armoire/JauneFermer16.bmp"
        Dim UrlImageN3 = "~/Images/Armoire/VertFonceFermer16.bmp"
        Dim UrlImageN4 = "~/Images/Armoire/GrisClairFermer16.bmp"
        Dim UrlImageN5 = "~/Images/Armoire/OrangeFonceFermer16.bmp"
        Dim UrlImageN6 = "~/Images/Armoire/RougeFermer16.bmp"
        Dim UrlImageN7 = "~/Images/Armoire/BleuFonceFermer16.bmp"
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        Dim NewNoeudN1 As TreeNode = Nothing
        Dim NewNoeudN2 As TreeNode = Nothing
        Dim NewNoeudN3 As TreeNode = Nothing
        Dim NewNoeudN4 As TreeNode = Nothing
        Dim NewNoeudN5 As TreeNode = Nothing
        Dim NewNoeudN6 As TreeNode = Nothing
        Dim NewNoeudN7 As TreeNode = Nothing
        Dim CptN1 As Integer = 0
        Dim CptN2 As Integer = 0
        Dim CptN3 As Integer = 0
        Dim CptN4 As Integer = 0
        Dim CptN5 As Integer = 0
        Dim CptN6 As Integer = 0
        Dim SiAFaire As Boolean
        Dim Etablissement As String = ControllerArmoire.Param_Etablissement

        If Etablissement = "(Tous)" Then
            Etablissement = ""
        End If
        TreeListeDossier.Nodes.Clear()
        EtiStatus1.Text = ""
        EtiStatus2.Text = ""

        LstVuesDyna = ControllerArmoire.PointeurListeCourante
        If LstVuesDyna Is Nothing OrElse LstVuesDyna.Count = 0 Then
            Return
        End If
        TreeListeDossier.LevelStyles.Clear()

        For IndiceA = 0 To LstVuesDyna.Count - 1
            SiAFaire = True
            If Etablissement <> "" Then
                If LstVuesDyna.Item(IndiceA).Valeurs(0) <> Etablissement Then
                    SiAFaire = False
                End If
            End If
            If SiAFaire = True Then
                Select Case LstVuesDyna.Item(IndiceA).Valeurs(0)
                    Case Is <> RuptureN1
                        If NewNoeudN7 IsNot Nothing Then
                            If NewNoeudN7.ChildNodes.Count > 1 Then
                                NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                            Else
                                NewNoeudN7.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN6 IsNot Nothing Then
                            If CptN6 > 1 Then
                                NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                            Else
                                NewNoeudN6.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN5 IsNot Nothing Then
                            If CptN5 > 1 Then
                                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                            Else
                                NewNoeudN5.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN4 IsNot Nothing Then
                            If CptN4 > 1 Then
                                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                            Else
                                NewNoeudN4.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN3 IsNot Nothing Then
                            If CptN3 > 1 Then
                                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                            Else
                                NewNoeudN3.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN2 IsNot Nothing Then
                            If CptN2 > 1 Then
                                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
                            Else
                                NewNoeudN2.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN1 IsNot Nothing Then
                            If CptN1 > 1 Then
                                NewNoeudN1.ToolTip = CptN1.ToString & " dossiers"
                            Else
                                NewNoeudN1.ToolTip = "Un dossier"
                            End If
                        End If
                        RuptureN1 = LstVuesDyna.Item(IndiceA).Valeurs(0)
                        RuptureN2 = ""
                        RuptureN3 = ""
                        RuptureN4 = ""
                        RuptureN5 = ""
                        RuptureN6 = ""
                        RuptureN7 = ""
                        NewNoeudN2 = Nothing
                        NewNoeudN3 = Nothing
                        NewNoeudN4 = Nothing
                        NewNoeudN5 = Nothing
                        NewNoeudN6 = Nothing
                        NewNoeudN7 = Nothing
                        CptN1 = 0
                        CptN2 = 0
                        CptN3 = 0
                        CptN4 = 0
                        CptN5 = 0
                        CptN6 = 0

                        NewNoeudN1 = New TreeNode(RuptureN1, "N1" & VI.Tild & IndiceA.ToString)
                        NewNoeudN1.ImageUrl = UrlImageN1

                        NewNoeudN1.PopulateOnDemand = False
                        NewNoeudN1.ShowCheckBox = False
                        NewNoeudN1.SelectAction = TreeNodeSelectAction.SelectExpand
                        TreeListeDossier.Nodes.Add(NewNoeudN1)
                End Select

                Select Case LstVuesDyna.Item(IndiceA).Valeurs(1)
                    Case Is <> RuptureN2
                        If NewNoeudN7 IsNot Nothing Then
                            If NewNoeudN7.ChildNodes.Count > 1 Then
                                NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                            Else
                                NewNoeudN7.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN6 IsNot Nothing Then
                            If CptN6 > 1 Then
                                NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                            Else
                                NewNoeudN6.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN5 IsNot Nothing Then
                            If CptN5 > 1 Then
                                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                            Else
                                NewNoeudN5.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN4 IsNot Nothing Then
                            If CptN4 > 1 Then
                                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                            Else
                                NewNoeudN4.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN3 IsNot Nothing Then
                            If CptN3 > 1 Then
                                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                            Else
                                NewNoeudN3.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN2 IsNot Nothing Then
                            If CptN2 > 1 Then
                                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
                            Else
                                NewNoeudN2.ToolTip = "Un dossier"
                            End If
                        End If
                        RuptureN2 = LstVuesDyna.Item(IndiceA).Valeurs(1)
                        RuptureN3 = ""
                        RuptureN4 = ""
                        RuptureN5 = ""
                        RuptureN6 = ""
                        RuptureN7 = ""
                        NewNoeudN3 = Nothing
                        NewNoeudN4 = Nothing
                        NewNoeudN5 = Nothing
                        NewNoeudN6 = Nothing
                        NewNoeudN7 = Nothing
                        CptN2 = 0
                        CptN3 = 0
                        CptN4 = 0
                        CptN5 = 0
                        CptN6 = 0

                        If NewNoeudN1 IsNot Nothing Then
                            NewNoeudN2 = New TreeNode(RuptureN2, "N2" & VI.Tild & RuptureN1 & VI.Tild & IndiceA.ToString)
                            NewNoeudN2.ImageUrl = UrlImageN2
                            NewNoeudN2.PopulateOnDemand = False
                            NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                            NewNoeudN1.ChildNodes.Add(NewNoeudN2)
                        End If
                End Select

                If LstVuesDyna.Item(IndiceA).Valeurs.Count > 2 Then
                    If LstVuesDyna.Item(IndiceA).Valeurs(2) <> "" Then
                        Select Case LstVuesDyna.Item(IndiceA).Valeurs(2)
                            Case Is <> RuptureN3
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN5 IsNot Nothing Then
                                    If CptN5 > 1 Then
                                        NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                    Else
                                        NewNoeudN5.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN4 IsNot Nothing Then
                                    If CptN4 > 1 Then
                                        NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                    Else
                                        NewNoeudN4.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN3 IsNot Nothing Then
                                    If CptN3 > 1 Then
                                        NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                                    Else
                                        NewNoeudN3.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN3 = LstVuesDyna.Item(IndiceA).Valeurs(2)
                                RuptureN4 = ""
                                RuptureN5 = ""
                                RuptureN6 = ""
                                RuptureN7 = ""
                                NewNoeudN4 = Nothing
                                NewNoeudN5 = Nothing
                                NewNoeudN6 = Nothing
                                NewNoeudN7 = Nothing
                                CptN3 = 0
                                CptN4 = 0
                                CptN5 = 0
                                CptN6 = 0

                                If NewNoeudN2 IsNot Nothing Then
                                    NewNoeudN3 = New TreeNode(RuptureN3, "N3" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild & IndiceA.ToString)
                                    NewNoeudN3.ImageUrl = UrlImageN3
                                    NewNoeudN3.PopulateOnDemand = False
                                    NewNoeudN3.SelectAction = TreeNodeSelectAction.SelectExpand
                                    NewNoeudN2.ChildNodes.Add(NewNoeudN3)
                                End If
                        End Select
                    End If
                End If

                If LstVuesDyna.Item(IndiceA).Valeurs.Count > 3 Then
                    If LstVuesDyna.Item(IndiceA).Valeurs(3) <> "" Then
                        Select Case LstVuesDyna.Item(IndiceA).Valeurs(3)
                            Case Is <> RuptureN4
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN5 IsNot Nothing Then
                                    If CptN5 > 1 Then
                                        NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                    Else
                                        NewNoeudN5.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN4 IsNot Nothing Then
                                    If CptN4 > 1 Then
                                        NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                    Else
                                        NewNoeudN4.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN4 = LstVuesDyna.Item(IndiceA).Valeurs(3)
                                RuptureN5 = ""
                                RuptureN6 = ""
                                RuptureN7 = ""
                                NewNoeudN5 = Nothing
                                NewNoeudN6 = Nothing
                                NewNoeudN7 = Nothing
                                CptN4 = 0
                                CptN5 = 0
                                CptN6 = 0

                                NewNoeudN4 = New TreeNode(RuptureN4, "N4" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & IndiceA.ToString)
                                NewNoeudN4.ImageUrl = UrlImageN4
                                NewNoeudN4.PopulateOnDemand = False
                                NewNoeudN4.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN3 Is Nothing Then
                                    If NewNoeudN2 IsNot Nothing Then
                                        NewNoeudN2.ChildNodes.Add(NewNoeudN4)
                                    End If
                                Else
                                    NewNoeudN3.ChildNodes.Add(NewNoeudN4)
                                End If
                        End Select
                    End If
                End If

                If LstVuesDyna.Item(IndiceA).Valeurs.Count > 4 Then
                    If LstVuesDyna.Item(IndiceA).Valeurs(4) <> "" Then
                        Select Case LstVuesDyna.Item(IndiceA).Valeurs(4)
                            Case Is <> RuptureN5
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN5 IsNot Nothing Then
                                    If CptN5 > 1 Then
                                        NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                    Else
                                        NewNoeudN5.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN5 = LstVuesDyna.Item(IndiceA).Valeurs(4)
                                RuptureN6 = ""
                                RuptureN7 = ""
                                NewNoeudN6 = Nothing
                                NewNoeudN7 = Nothing
                                CptN5 = 0
                                CptN6 = 0

                                NewNoeudN5 = New TreeNode(RuptureN5, "N5" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & RuptureN4 & VI.Tild & IndiceA.ToString)
                                NewNoeudN5.ImageUrl = UrlImageN5
                                NewNoeudN5.PopulateOnDemand = False
                                NewNoeudN5.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN4 Is Nothing Then
                                    If NewNoeudN3 Is Nothing Then
                                        If NewNoeudN2 IsNot Nothing Then
                                            NewNoeudN2.ChildNodes.Add(NewNoeudN5)
                                        End If
                                    Else
                                        NewNoeudN3.ChildNodes.Add(NewNoeudN5)
                                    End If
                                Else
                                    NewNoeudN4.ChildNodes.Add(NewNoeudN5)
                                End If
                        End Select
                    End If
                End If

                If LstVuesDyna.Item(IndiceA).Valeurs.Count > 5 And Index = 2 Then
                    If LstVuesDyna.Item(IndiceA).Valeurs(5) <> "" Then
                        Select Case LstVuesDyna.Item(IndiceA).Valeurs(5)
                            Case Is <> RuptureN6
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                If NewNoeudN6 IsNot Nothing Then
                                    If CptN6 > 1 Then
                                        NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
                                    Else
                                        NewNoeudN6.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN6 = LstVuesDyna.Item(IndiceA).Valeurs(5)
                                RuptureN7 = ""
                                NewNoeudN7 = Nothing
                                CptN6 = 0

                                NewNoeudN6 = New TreeNode(RuptureN6, "N6" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & RuptureN4 & VI.Tild & RuptureN5 & VI.Tild & IndiceA.ToString)
                                NewNoeudN6.ImageUrl = UrlImageN6
                                NewNoeudN6.PopulateOnDemand = False
                                NewNoeudN6.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN5 Is Nothing Then
                                    If NewNoeudN4 Is Nothing Then
                                        If NewNoeudN3 Is Nothing Then
                                            If NewNoeudN2 IsNot Nothing Then
                                                NewNoeudN2.ChildNodes.Add(NewNoeudN6)
                                            End If
                                        Else
                                            NewNoeudN3.ChildNodes.Add(NewNoeudN6)
                                        End If
                                    Else
                                        NewNoeudN4.ChildNodes.Add(NewNoeudN6)
                                    End If
                                Else
                                    NewNoeudN5.ChildNodes.Add(NewNoeudN6)
                                End If
                        End Select
                    End If
                End If

                If LstVuesDyna.Item(IndiceA).Valeurs.Count > 6 And Index = 2 Then
                    If LstVuesDyna.Item(IndiceA).Valeurs(6) <> "" Then
                        Select Case LstVuesDyna.Item(IndiceA).Valeurs(6)
                            Case Is <> RuptureN7
                                If NewNoeudN7 IsNot Nothing Then
                                    If NewNoeudN7.ChildNodes.Count > 1 Then
                                        NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
                                    Else
                                        NewNoeudN7.ToolTip = "Un dossier"
                                    End If
                                End If
                                RuptureN7 = LstVuesDyna.Item(IndiceA).Valeurs(6)

                                NewNoeudN7 = New TreeNode(RuptureN7, "N7" & VI.Tild & RuptureN1 & VI.Tild & RuptureN2 & VI.Tild _
                                                          & RuptureN3 & VI.Tild & RuptureN4 & VI.Tild & RuptureN5 & VI.Tild _
                                                          & RuptureN6 & VI.Tild & IndiceA.ToString)
                                NewNoeudN7.ImageUrl = UrlImageN7
                                NewNoeudN7.PopulateOnDemand = False
                                NewNoeudN7.SelectAction = TreeNodeSelectAction.SelectExpand
                                If NewNoeudN6 Is Nothing Then
                                    If NewNoeudN5 Is Nothing Then
                                        If NewNoeudN4 Is Nothing Then
                                            If NewNoeudN3 Is Nothing Then
                                                If NewNoeudN2 IsNot Nothing Then
                                                    NewNoeudN2.ChildNodes.Add(NewNoeudN7)
                                                End If
                                            Else
                                                NewNoeudN3.ChildNodes.Add(NewNoeudN7)
                                            End If
                                        Else
                                            NewNoeudN4.ChildNodes.Add(NewNoeudN7)
                                        End If
                                    Else
                                        NewNoeudN5.ChildNodes.Add(NewNoeudN7)
                                    End If
                                Else
                                    NewNoeudN6.ChildNodes.Add(NewNoeudN7)
                                End If
                        End Select
                    End If
                    CptN1 += 1
                    CptN2 += 1
                    CptN3 += 1
                    CptN4 += 1
                    CptN5 += 1
                    CptN6 += 1
                End If
            End If
        Next IndiceA

        If NewNoeudN7 IsNot Nothing Then
            If NewNoeudN7.ChildNodes.Count > 1 Then
                NewNoeudN7.ToolTip = NewNoeudN7.ChildNodes.Count.ToString & " dossiers"
            Else
                NewNoeudN7.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN6 IsNot Nothing Then
            If CptN6 > 1 Then
                NewNoeudN6.ToolTip = CptN6.ToString & " dossiers"
            Else
                NewNoeudN6.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN5 IsNot Nothing Then
            If CptN5 > 1 Then
                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
            Else
                NewNoeudN5.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN4 IsNot Nothing Then
            If CptN4 > 1 Then
                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
            Else
                NewNoeudN4.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN3 IsNot Nothing Then
            If CptN3 > 1 Then
                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
            Else
                NewNoeudN3.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN2 IsNot Nothing Then
            If CptN2 > 1 Then
                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
            Else
                NewNoeudN2.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN1 IsNot Nothing Then
            If CptN1 > 1 Then
                NewNoeudN1.ToolTip = CptN1.ToString & " dossiers"
            Else
                NewNoeudN1.ToolTip = "Un dossier"
            End If
        End If

        EtiStatus1.Text = "" & LstVuesDyna.Count
        EtiStatus2.Text = "" & VListePer.Items.Count

        RuptureN1 = V_Cache.DataPathItemSel
        If RuptureN1 <> "" Then
            Try
                NewNoeudN1 = TreeListeDossier.FindNode(RuptureN1)
            Catch ex As Exception
                NewNoeudN1 = Nothing
            End Try
            If NewNoeudN1 IsNot Nothing Then
                NewNoeudN1.Checked = True
                Select Case NewNoeudN1.Depth
                    Case Is = 0
                        NewNoeudN1.ExpandAll()
                    Case Is = 1
                        NewNoeudN1.Parent.ExpandAll()
                    Case Is = 2
                        NewNoeudN1.Parent.Parent.ExpandAll()
                    Case Is = 3
                        NewNoeudN1.Parent.Parent.Parent.ExpandAll()
                    Case Is = 4
                        NewNoeudN1.Parent.Parent.Parent.Parent.ExpandAll()
                    Case Is = 5
                        NewNoeudN1.Parent.Parent.Parent.Parent.Parent.ExpandAll()
                End Select
            Else
                TreeListeDossier.CollapseAll()
            End If
        Else
            TreeListeDossier.CollapseAll()
        End If

        TreeListeDossier.CheckedNodes.Clear()
        For Each Noeud As TreeNode In TreeListeDossier.Nodes
            If Noeud.Text <> ControllerArmoire.Param_Etablissement Then
                Continue For
            End If
            Noeud.Checked = True
            If Index <> IDX_AFFECTATION Then
                Noeud.Expanded = True
                Exit For
            End If
            If ControllerArmoire.Param_Niveau1 = "" Then
                Noeud.Expanded = True
                Exit For
            End If
            For Each SousNoeud As TreeNode In Noeud.ChildNodes
                If SousNoeud.Text <> ControllerArmoire.Param_Niveau1 Then
                    Continue For
                End If
                SousNoeud.Checked = True
                If SousNoeud.Depth <= 0 Then
                    Continue For
                End If
                SousNoeud.Parent.Checked = False
                SousNoeud.Parent.Expanded = True
                Exit For
            Next
        Next
    End Sub

    Private Sub FaireListeAlphabetique()
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        TreeListeDossier.Nodes.Clear()
        EtiStatus1.Text = ""
        EtiStatus2.Text = ""
        If ControllerArmoire.PointeurListeCourante Is Nothing OrElse ControllerArmoire.PointeurListeCourante.Count <= 0 Then
            EtiStatus1.Text = "0"
            Exit Sub
        End If
        Call StylerlArmoire(1)

        Dim NewNoeudPER As TreeNode

        ControllerArmoire.PointeurListeCourante.ForEach(Sub(it)
                                                            NewNoeudPER = New TreeNode(it.Valeurs(0) & Strings.Space(1) & it.Valeurs(1), it.Ide_Dossier.ToString)
                                                            NewNoeudPER.ImageUrl = UrlImagePer
                                                            NewNoeudPER.PopulateOnDemand = False
                                                            NewNoeudPER.SelectAction = TreeNodeSelectAction.Select
                                                            TreeListeDossier.Nodes.Add(NewNoeudPER)
                                                        End Sub)

        EtiStatus1.Text = "" & ControllerArmoire.PointeurListeCourante.Count
        EtiStatus2.Text = ""
    End Sub

    Private Sub StylerlArmoire(ByVal Profondeur As Integer)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim Vstyle As TreeNodeStyle
        TreeListeDossier.LevelStyles.Clear()

        If Profondeur = 1 Then
            Vstyle = New TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White
            TreeListeDossier.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Italic = True
        Vstyle.Font.Size = FontUnit.Small
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(1)
        Vstyle.BorderStyle = BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#142425")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        TreeListeDossier.LevelStyles.Add(Vstyle)

        If Profondeur = 2 Then
            Vstyle = New TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeDossier.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#1C5151")
        Vstyle.ForeColor = Drawing.Color.White

        TreeListeDossier.LevelStyles.Add(Vstyle)

        If Profondeur = 3 Then
            Vstyle = New TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeDossier.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#137A76")
        Vstyle.ForeColor = Drawing.Color.White

        TreeListeDossier.LevelStyles.Add(Vstyle)

        If Profondeur = 4 Then
            Vstyle = New TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = BorderStyle.None
            Vstyle.BackColor = Drawing.Color.White

            TreeListeDossier.LevelStyles.Add(Vstyle)
        End If

    End Sub

    Private Sub Remplir_ComboChoix()
        If IsPostBack = True Then
            Return
        End If
        DropDownArmoire.Items.Add(New ListItem("Liste alphabétique des dossiers en activité") With {.Selected = True})
        DropDownArmoire.Items.Add(New ListItem("Liste alphabétique de tous les dossiers"))
        DropDownArmoire.Items.Add(New ListItem("Liste alphabétique des dossiers ayant suivi une formation précise"))
        DropDownArmoire.Items.Add(New ListItem("Liste alphabétique des dossiers ayant suivi une formation dans l'année " & DateTime.Now.Year))
        V_Cache.ListeSel = IDX_ALPHA_ACTIVITE
        V_Cache.Sauve(ViewState)

        DropDownArmoire.Items.Add(New ListItem("Liste alphabétique des dossiers ayant suivi une formation les 3 dernières années"))
        DropDownArmoire.Items.Add(New ListItem("Liste des dossiers par établissement et affectation fonctionnelle"))
        DropDownArmoire.Items.Add(New ListItem("Liste des dossiers par établissement et situation administrative"))

    End Sub

    Private Sub Remplir_ComboPlan()
        If IsPostBack = True Then
            Return
        End If
        CboStage.Items.Clear()
        CboStage.Items.Add("Selectionner...")
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur

        Dim lstplan As List(Of ListItem) = Requeteur.ListePlanOuStagePourFiltre("")
        If lstplan.Count <= 0 Then
            Return
        End If
        CboPlan.Items.Add("Selectionner...")
        lstplan.ForEach(Sub(pl)
                            CboPlan.Items.Add(pl)
                        End Sub)

    End Sub

    Private Sub InitialiserBoutonsLettre()
        Dim lstboutonlettre As List(Of ImageButton) = VisuHelper.GetImageBoutonLettreTous(RowButtonLettre)
        lstboutonlettre.ForEach(Sub(Bouton)
                                    If Bouton.ID = "ButtonAll" Then
                                        Bouton.ImageUrl = "~/Images/Lettres/Arobase.bmp"
                                        Return
                                    End If
                                    Bouton.ImageUrl = "~/Images/Lettres/" & Strings.Right(Bouton.ID, 1) & ".bmp"
                                End Sub)
    End Sub

    Private Sub TreeListeDossier_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TreeListeDossier.SelectedNodeChanged
        Dim Tableaudata As String()
        Dim NoeudSelect As TreeNode = DirectCast(sender, TreeView).SelectedNode

        If V_Cache.ObjetSel = "" Then 'Liste Alphabétique
            Dim SelIde As Integer = 0
            If IsNumeric(NoeudSelect.Value) Then
                SelIde = Integer.Parse(NoeudSelect.Value)
            Else
                If Not NoeudSelect.Value.Contains("-") Then
                    Exit Sub
                End If
                Tableaudata = Strings.Split(NoeudSelect.Value, "-", -1)
                If IsNumeric(Tableaudata.Last()) Then
                    SelIde = Integer.Parse(Tableaudata.Last())
                End If
            End If
            V_Cache.IdeSel = SelIde
            V_Cache.Sauve(ViewState)
            If SelIde = 0 Then
                Exit Sub
            End If
            VDossier_Click(New DossierClickEventArgs(V_Cache.IdeSel, ""))
            Return
        End If

        Dim LstVuesDyna As List(Of VirRequeteType) = Nothing
        Dim IndiceI As Integer
        Dim ChaineSel As String = NoeudSelect.Text
        Dim Etablissement As String = ""
        Dim Sel1 As String = ""
        Dim Sel2 As String = ""
        Dim Sel3 As String = ""
        Dim Sel4 As String = ""
        Dim Sel5 As String = ""
        Dim Sel6 As String = ""
        Dim LstCriteres As List(Of String)
        Tableaudata = Strings.Split(NoeudSelect.Value, VI.Tild, -1)
        LstCriteres = New List(Of String)
        LstCriteres.Add(V_Cache.ObjetSel)
        Select Case Tableaudata(0)
            Case "N1"
                Etablissement = ChaineSel
                LstCriteres.Add(ChaineSel)
            Case "N2"
                Etablissement = Tableaudata(1)
                Sel1 = ChaineSel
                LstCriteres.Add(Etablissement)
                LstCriteres.Add(ChaineSel)
            Case "N3"
                Etablissement = Tableaudata(1)
                Sel1 = Tableaudata(2)
                Sel2 = ChaineSel
                LstCriteres.Add(Etablissement)
                LstCriteres.Add(Tableaudata(2))
                LstCriteres.Add(ChaineSel)
            Case "N4"
                Etablissement = Tableaudata(1)
                Sel1 = Tableaudata(2)
                Sel2 = Tableaudata(3)
                Sel3 = ChaineSel
                LstCriteres.Add(Etablissement)
                For IndiceI = 2 To 3
                    LstCriteres.Add(Tableaudata(IndiceI))
                Next IndiceI
                LstCriteres.Add(ChaineSel)
            Case "N5"
                Etablissement = Tableaudata(1)
                Sel1 = Tableaudata(2)
                Sel2 = Tableaudata(3)
                Sel3 = Tableaudata(4)
                Sel4 = ChaineSel
                LstCriteres.Add(Etablissement)
                For IndiceI = 2 To 4
                    LstCriteres.Add(Tableaudata(IndiceI))
                Next IndiceI
                LstCriteres.Add(ChaineSel)
            Case "N6"
                Etablissement = Tableaudata(1)
                Sel1 = Tableaudata(2)
                Sel2 = Tableaudata(3)
                Sel3 = Tableaudata(4)
                Sel4 = Tableaudata(5)
                Sel5 = ChaineSel
                LstCriteres.Add(Etablissement)
                For IndiceI = 2 To 5
                    LstCriteres.Add(Tableaudata(IndiceI))
                Next IndiceI
                LstCriteres.Add(ChaineSel)
            Case "N7"
                Etablissement = Tableaudata(1)
                Sel1 = Tableaudata(2)
                Sel2 = Tableaudata(3)
                Sel3 = Tableaudata(4)
                Sel4 = Tableaudata(5)
                Sel5 = Tableaudata(6)
                Sel6 = ChaineSel
                LstCriteres.Add(Etablissement)
                For IndiceI = 2 To 6
                    LstCriteres.Add(Tableaudata(IndiceI))
                Next IndiceI
                LstCriteres.Add(ChaineSel)
        End Select

        Select Case V_Cache.ObjetSel
            Case "Organigramme"
                LstVuesDyna = ControllerArmoire.ObjetArmoire.AffectationsFonctionnelles(Etablissement, Sel1, Sel2, Sel3, Sel4, Sel5, Sel6)
            Case "Carrieres"
                LstVuesDyna = ControllerArmoire.ObjetArmoire.SituationsAdministratives(Etablissement, Sel1, Sel2, Sel3, Sel4)
        End Select

        If LstVuesDyna Is Nothing Then
            Exit Sub
        End If

        Call InitialiserVariables()
        EtiSelection.Text = NoeudSelect.Text
        EtiSelection.ToolTip = NoeudSelect.Value
        EtiStatus2.Text = ""

        Dim ItemPer As ListItem
        Dim LstPERBrut As New List(Of ListItem)

        LstVuesDyna.ForEach(Sub(rt)
                                Try
                                    If VListePer.Items.FindByValue(rt.Ide_Dossier.ToString()) Is Nothing Then
                                        ItemPer = New ListItem()
                                        ItemPer.Text = rt.Valeurs(rt.Valeurs.Count - 2) & Strings.Space(2) & rt.Valeurs(rt.Valeurs.Count - 1) 'Nom et Prénom
                                        ItemPer.Value = "" & rt.Ide_Dossier
                                        LstPERBrut.Add(ItemPer)
                                    End If
                                Catch ex As Exception
                                    Exit Try
                                End Try
                            End Sub)

        If LstPERBrut.Count <= 0 Then
            Return
        End If

        Dim LstIde As List(Of Integer) = New List(Of Integer)

        For Each el In (From it In LstPERBrut Order By it.Text).ToList()
            VListePer.Items.Add(el)
            LstIde.Add(Integer.Parse(el.Value))
        Next
        EtiStatus2.Text = "" & VListePer.Items.Count

        V_Cache.DataPathItemSel = NoeudSelect.ValuePath
        V_Cache.CriteresSel = LstCriteres
        V_Cache.ListeIdeSel = LstIde
        V_Cache.Sauve(ViewState)

    End Sub

    Private Sub VListePer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VListePer.SelectedIndexChanged
        Dim SelIde As Integer = 0
        Try
            SelIde = CInt(CType(sender, ListBox).SelectedItem.Value)
        Catch ex As Exception
            Exit Try
        End Try
        V_Cache.IdeSel = SelIde
        V_Cache.Sauve(ViewState)
        VDossier_Click(New DossierClickEventArgs(V_Cache.IdeSel, ""))
    End Sub

    Private Sub DropDownArmoire_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DropDownArmoire.SelectedIndexChanged
        Call InitialiserVariables()
        Dim SelIndex As Integer = CType(sender, DropDownList).SelectedIndex

        CelluleStage.Visible = (SelIndex = IDX_ALPHA_STAGE)
        RowRechNom.Visible = (SelIndex <> IDX_ALPHA_STAGE)

        V_Cache.ListeSel = SelIndex
        V_Cache.DoitCharger = True
        V_Cache.Sauve(ViewState)
        Select Case SelIndex
            Case IDX_ALPHA_ACTIVITE, IDX_ALPHA_FORMATION, IDX_ALPHA_FORMATION_3, IDX_ALPHA_TOUS
                If HSelLettre.Value = "" Then
                    InitialiserBoutonsLettre()
                    ButtonA.ImageUrl = "~/Images/Lettres/A_Sel.bmp"
                    If (ControllerArmoire.Etablissement <> "" And ControllerArmoire.Etablissement <> "(Tous)") Or ControllerArmoire.FiltreUti <> "" Then
                        HSelLettre.Value = ""
                    Else
                        HSelLettre.Value = "A"
                    End If
                End If
            Case IDX_AFFECTATION, IDX_SITADM
                If (ControllerArmoire.Etablissement <> "" And ControllerArmoire.Etablissement <> "(Tous)") Or ControllerArmoire.FiltreUti <> "" Then
                    HSelLettre.Value = ""
                End If
        End Select
        VSelection_TypeArmoire(New SelectionTypeArmoireEventArgs(DropDownArmoire.SelectedItem.Text))
    End Sub

    Private Sub ButtonA_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles ButtonA.Click, _
            ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click, _
            ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click, _
            ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click, _
            ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click, ButtonAll.Click

        V_Cache.DoitCharger = True
        V_Cache.Sauve(ViewState)
        InitialiserBoutonsLettre()
        If CType(sender, ImageButton).ID = "ButtonAll" Then
            HSelLettre.Value = ""
            CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & "Arobase_Sel.bmp"
        Else
            HSelLettre.Value = Strings.Right(CType(sender, ImageButton).ID, 1)
            CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(sender, ImageButton).ID, 1) & "_Sel.bmp"
        End If
    End Sub

    Private Sub CommandeCsv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeCsv.Click
        If ControllerArmoire.PointeurListeCourante Is Nothing OrElse ControllerArmoire.PointeurListeCourante.Count <= 0 Then
            Return
        End If
        Dim CarSep As String = Strings.Chr(9)
        Dim Suffixe As String = "." & Strings.Right(CType(sender, Button).ID, 3)
        Dim CodeIso As Encoding = Encoding.UTF8
        Dim CodeUnicode As Encoding = Encoding.Unicode
        Dim Chaine As String

        Dim ChemindeBase As String = Request.PhysicalApplicationPath
        If Not (ChemindeBase.EndsWith("\")) Then
            ChemindeBase &= "\"
        End If
        Dim NomRepertoire As String = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirRepertoireTemporaire
        Dim NomFichier As String = "Armoire_" & ControllerArmoire.Param_Annee & "_" & ObjetGlobalBase.RhFonctions.ChaineSansAccent(ControllerArmoire.Param_Etablissement, False) & Suffixe

        Dim FicStream As FileStream = New FileStream(NomRepertoire & "\" & NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
        Dim FicWriter As StreamWriter = New StreamWriter(FicStream, CodeUnicode)

        ControllerArmoire.PointeurListeCourante.ForEach(Sub(rt)
                                                            Chaine = ""
                                                            rt.Valeurs.ForEach(Sub(s)
                                                                                   Chaine &= s & CarSep
                                                                               End Sub)
                                                            FicWriter.WriteLine(Chaine)
                                                        End Sub)

        FicWriter.Flush()
        FicWriter.Close()
        Dim FluxTeleChargement As Byte() = My.Computer.FileSystem.ReadAllBytes(NomRepertoire & "\" & NomFichier)

        If FluxTeleChargement IsNot Nothing Then
            Dim response As HttpResponse = HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub DonRecherche_TextChanged(sender As Object, e As System.EventArgs) Handles DonRecherche.TextChanged
        HSelLettre.Value = DonRecherche.Text.ToUpper
        InitialiserBoutonsLettre()
        V_Cache.DoitCharger = True
        V_Cache.Sauve(ViewState)
    End Sub

    Protected Sub CboPlan_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        CboStage.Items.Clear()
        CboStage.Items.Add("Selectionner...")
        HSelLettre.Value = ""
        V_Cache.DoitCharger = True
        V_Cache.Sauve(ViewState)
        If (CboPlan.SelectedIndex <= 0) Then
            Exit Sub
        End If
        Dim lstStage As List(Of ListItem) = Requeteur.ListePlanOuStagePourFiltre(CboPlan.SelectedItem.Text)
        If lstStage.Count <= 0 Then
            Exit Sub
        End If
        lstStage.ForEach(Sub(pl)
                             CboStage.Items.Add(pl)
                         End Sub)
    End Sub

    Protected Sub CboStage_SelectedIndexChanged(sender As Object, e As EventArgs)

        InitialiserBoutonsLettre()
        ButtonAll.ImageUrl = "~/Images/Lettres/Arobase_Sel.bmp"
        HSelLettre.Value = ""

        V_Cache.DoitCharger = True
        V_Cache.Sauve(ViewState)

        VSelection_TypeArmoire(New SelectionTypeArmoireEventArgs(DropDownArmoire.SelectedItem.Text))
    End Sub
End Class

<Serializable>
Public Class CacheArmoire
    Public Const KeyState As String = "ArmoirePER"
    Public Property ListeSel As Integer
    Public Property ObjetSel As String
    Public Property IdeSel As Integer
    Public Property DataPathItemSel As String
    Public Property CriteresSel As List(Of String)
    Public Property ListeIdeSel As List(Of Integer)
    Public Property DoitCharger As Boolean

    Public Sub New()
        DoitCharger = True
        ListeSel = 0
        ObjetSel = "Etablissement"
        IdeSel = 0
        DataPathItemSel = ""
        CriteresSel = New List(Of String)()
        ListeIdeSel = New List(Of Integer)()
    End Sub

    Public Sub Sauve(ByVal vs As StateBag)
        vs.AjouteValeur(KeyState, Me)
    End Sub
End Class
