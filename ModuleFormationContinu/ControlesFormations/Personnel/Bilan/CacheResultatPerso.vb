﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.Metier.Formation
Imports System.Reflection
Imports System.Xml.Serialization
Imports Virtualia.Structure.Formation

<Serializable>
Public Class CacheResultatPerso
    Public Property Titre As String = ""
    Public Property NomAgent As String = ""
    Public Property ProprietesValeurs As PropValeurInfoCollection = New PropValeurInfoCollection()

    Public Function GetResultat(Of T)(Items As IEnumerable(Of T)) As List(Of ResultatInfo)
        Return ProprietesValeurs.GetResultat(Of T)(Items)
    End Function
End Class
