﻿Option Strict On
Option Explicit On
Option Compare Text

Imports System
Imports System.Collections.Generic
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.Net.Session
Imports System.Web.UI
Imports System.Drawing
Imports Virtualia.Structure.Formation
Imports System.Threading.Tasks
Imports System.IO
Imports System.Threading
Imports AjaxControlToolkit

Public Class PopupResultatPerso
    Inherits UserControl
    Implements IControlBase

    Private WsCtrlGestion As CtrlBilanPerso
    Private WsPopup As ModalPopupExtender

    Private WsCouleurTitre As Color = VisuHelper.ConvertiCouleur("#124545")
    Private WsCouleurPoliceTitre As Color = Color.Snow

    Private WsCouleurValeur As Color = Color.Snow
    Private WsCouleurPoliceValeur As Color = VisuHelper.ConvertiCouleur("#101010")

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Private Property VCache As CacheResultatPerso
        Get
            If Not (ViewState.KeyExiste("VCache")) Then
                ViewState.AjouteValeur("VCache", New CacheResultatPerso())
            End If
            Return ViewState.GetValeur(Of CacheResultatPerso)("VCache")
        End Get
        Set(value As CacheResultatPerso)
            ViewState.AjouteValeur("VCache", value)
        End Set
    End Property

    Public WriteOnly Property Popup As ModalPopupExtender
        Set(ByVal value As ModalPopupExtender)
            WsPopup = value
        End Set
    End Property

    Public WriteOnly Property CtrlGestion As CtrlBilanPerso
        Set(value As CtrlBilanPerso)
            WsCtrlGestion = value
        End Set
    End Property

    Public Property NbItemParPage As Integer
        Private Get
            If Not (ViewState.KeyExiste("NbItemParPage")) Then
                ViewState.AjouteValeur("NbItemParPage", 100)
            End If
            Return DirectCast(ViewState("NbItemParPage"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("NbItemParPage", value)
        End Set
    End Property

    Private Property NumPageCourante As Integer
        Get
            If Not (ViewState.KeyExiste("NumPageCourante")) Then
                ViewState.AjouteValeur("NumPageCourante", 0)
            End If
            Return DirectCast(ViewState("NumPageCourante"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("NumPageCourante", value)
        End Set
    End Property

    Private Property NbPage As Integer
        Get
            If Not (ViewState.KeyExiste("NbPage")) Then
                ViewState.AjouteValeur("NbPage", -1)
            End If
            Return DirectCast(ViewState("NbPage"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("NbPage", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        BtnQuitter.Act = Sub(btn)
                             NumPageCourante = 0
                             NbPage = -1
                             WsPopup.Hide()
                         End Sub
    End Sub

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        ConstruitResultat()
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        lblContexte.Text = "Année : " & WsCtrlGestion.AnneeSel

        Dim cr As CacheResultatPerso = TryCast(items, CacheResultatPerso)
        If cr Is Nothing Then
            VCache = New CacheResultatPerso()
            Return
        End If
        VCache = cr

        ConstruitResultat()
    End Sub

    Private Sub ConstruitResultat()
        Label2.Text = VCache.NomAgent

        lblTitre.Text = VCache.Titre

        ConstruitEntete()
        ConstruitDonnees()
    End Sub

    Private Sub ConstruitEntete()
        TBEntete.Rows.Clear()
        If VCache.ProprietesValeurs.Count <= 0 Then
            Exit Sub
        End If
        Dim Rangee As TableRow = New TableRow()


        VCache.ProprietesValeurs.GetEntete().ForEach(Sub(pv)
                                                         Rangee.Cells.Add(ConstruitCelluleValeur(pv.TitreColonne, "" & pv.Taille & "%", HorizontalAlign.Center, WsCouleurTitre, WsCouleurPoliceTitre))
                                                     End Sub)
        'Cellule pour le scrollbar
        Rangee.Cells.Add(ConstruitCelluleValeur("", "15px", HorizontalAlign.Center, WsCouleurTitre, WsCouleurPoliceTitre))

        TBEntete.Rows.Add(Rangee)
    End Sub

    Private Sub ConstruitDonnees()
        If VCache.ProprietesValeurs.Count <= 0 Then
            Return
        End If
        TB_Donnees.Rows.Clear()
        ConstruitGrille()
    End Sub

    Private Function ConstruitCelluleValeur(valeur As String, taille As String, hAlign As HorizontalAlign, CouleurFond As Color, CouleurPolice As Color) As TableCell
        Dim CRetour As TableCell = New TableCell()

        CRetour.BackColor = CouleurFond
        CRetour.Width = New Unit(taille)

        Dim lbl As Label = New Label()
        lbl.ForeColor = CouleurPolice

        Select Case hAlign
            Case HorizontalAlign.Center
                lbl.Style.Add(HtmlTextWriterStyle.TextAlign, "center")
            Case HorizontalAlign.Left
                lbl.Style.Add(HtmlTextWriterStyle.TextAlign, "left")
                lbl.Style.Add(HtmlTextWriterStyle.MarginLeft, "2px")
            Case HorizontalAlign.Right
                lbl.Style.Add(HtmlTextWriterStyle.TextAlign, "right")
                lbl.Style.Add(HtmlTextWriterStyle.MarginRight, "2px")
        End Select

        lbl.Text = valeur

        If taille.EndsWith("px") Then
            lbl.Width = New Unit(taille)
        Else
            lbl.Width = New Unit("100%")
        End If

        CRetour.Controls.Add(lbl)

        Return CRetour
    End Function

    Private Function GetResultat() As List(Of ResultatInfo)
        Return VCache.GetResultat(Of StatistiqueFormationInfo)(WsCtrlGestion.Statistiques)
    End Function

    Private Sub ConstruitGrille()
        Dim lstresultat As List(Of ResultatInfo) = GetResultat()

        LblToTGene.Text = CStr(lstresultat.Count)
        RempliPagination(lstresultat.Count)
        If lstresultat.Count <= 0 Then
            Return
        End If
        If NbPage > 0 Then
            lstresultat = (From it In lstresultat Select it).Skip(NumPageCourante * NbItemParPage).Take(NbItemParPage).ToList()
        End If
        Dim lst As List(Of PropValeurInfo) = VCache.ProprietesValeurs.GetEntete()
        If lst.Count <= 0 Then
            Exit Sub
        End If

        Dim Rangee As TableRow
        Dim idx As Integer = 0
        Dim ssplit As String()

        For Each s In (From iri In lstresultat Order By iri.Clef Select iri.Clef).ToList()
            Rangee = New TableRow()
            idx = 0

            ssplit = s.Split("|"c)

            For Each v In ssplit
                Rangee.Cells.Add(ConstruitCelluleValeur(v, "" & lst(idx).Taille & "%", lst(idx).HAlignment, WsCouleurValeur, WsCouleurPoliceValeur))
                idx = idx + 1
            Next

            TB_Donnees.Rows.Add(Rangee)
        Next
    End Sub

    Private Sub RempliPagination(ByVal NbItemTot As Integer)
        If NbPage >= 0 Then
            Exit Sub
        End If
        If NbItemTot = 0 Then
            NbPage = 0
            R_Page.Visible = False
            Exit Sub
        End If
        If NbItemTot <= NbItemParPage Then
            NbPage = 0
            R_Page.Visible = False
            Exit Sub
        End If
        R_Page.Visible = True
        NbPage = Convert.ToInt32(Math.Round(Math.Truncate(NbItemTot / NbItemParPage), 0))

        If (NbItemTot Mod NbItemParPage) > 0 Then
            NbPage = NbPage + 1
        End If
        lblIndicPage.Text = "sur " & NbPage & " pages"

        BtnPgPrec.Enabled = False
        BtnPgSuiv.Enabled = True
        RempliCboPage()

    End Sub

    Private Sub RempliCboPage()
        CboPage.Items.Clear()
        For index = 0 To NbPage - 1
            CboPage.Items.Add(New ListItem("Page " & (index + 1), "" & index))
        Next
        CboPage.Items(0).Selected = True
    End Sub

    Protected Sub BtnPgPrec_Click(sender As Object, e As ImageClickEventArgs)
        NumPageCourante = NumPageCourante - 1
        CboPage.SelectedIndex = NumPageCourante

        BtnPgPrec.Enabled = (NumPageCourante <> 0)
        BtnPgSuiv.Enabled = (NumPageCourante < (NbPage - 1))

        ConstruitResultat()
    End Sub

    Protected Sub CboPage_SelectedIndexChanged(sender As Object, e As EventArgs)
        NumPageCourante = Integer.Parse(CboPage.SelectedItem.Value)

        BtnPgPrec.Enabled = (NumPageCourante <> 0)
        BtnPgSuiv.Enabled = (NumPageCourante < (NbPage - 1))

        ConstruitResultat()
    End Sub

    Protected Sub BtnPgSuiv_Click(sender As Object, e As ImageClickEventArgs)
        NumPageCourante = NumPageCourante + 1
        CboPage.SelectedIndex = NumPageCourante

        BtnPgPrec.Enabled = (NumPageCourante <> 0)
        BtnPgSuiv.Enabled = (NumPageCourante < (NbPage - 1))

        ConstruitResultat()
    End Sub

End Class