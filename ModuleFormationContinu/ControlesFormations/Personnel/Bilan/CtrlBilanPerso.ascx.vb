﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.OutilsVisu.Formation

Public Class CtrlBilanPerso
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public Property Statistiques As Virtualia.Metier.Formation.StatistiqueFormationInfoCollection
        Get
            If (Not ViewState.KeyExiste("Statistiques")) Then
                ViewState.AjouteValeur("Statistiques", New Virtualia.Metier.Formation.StatistiqueFormationInfoCollection())
            End If
            Return ViewState.GetValeur(Of Virtualia.Metier.Formation.StatistiqueFormationInfoCollection)("Statistiques")
        End Get
        Private Set(value As Virtualia.Metier.Formation.StatistiqueFormationInfoCollection)
            ViewState.AjouteValeur("Statistiques", value)
        End Set
    End Property

    Public Property AnneeSel As Integer
        Get
            If Not (ViewState.KeyExiste("AnneeSel")) Then
                ViewState.AjouteValeur("AnneeSel", 0)
            End If
            Return DirectCast(ViewState("AnneeSel"), Integer)
        End Get
        Private Set(value As Integer)
            ViewState.AjouteValeur("AnneeSel", value)
        End Set
    End Property

    Public Property Identifiant As Integer
        Get
            If Not ViewState.KeyExiste("Ide") Then
                ViewState.AjouteValeur("Ide", 0)
            End If
            Return DirectCast(ViewState("Ide"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("Ide", value)
        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        CtrlPopupResultat.CtrlGestion = Me
        CtrlPopupResultat.NbItemParPage = 200
        CtrlPopupResultat.Popup = PopupPopupResultat
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If ListeAnnees.Items.Count = 0 Then
            Call Initialiser()
        End If
    End Sub

    Private Sub Initialiser()
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur

        Statistiques.Clear()
        AnneeSel = 0
        ListeAnnees.Items.Clear()
        ListeAnnees.Items.Add(New ListItem("Sélectionner", "0") With {.Selected = True})

        CadreBilan.Visible = False
        lblTitre.Text = "Bilan individuel de formation"

        Dim Its As List(Of ListItem) = Requeteur.ListeAnneesPourFiltre(Identifiant, 3)
        If Its Is Nothing OrElse Its.Count <= 0 Then
            Return
        End If
        ListeAnnees.Items.AddRange(Its.ToArray())
    End Sub

    Private Sub ChargerTableau()
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If

        CadreBilan.Visible = True

        Tot_Nb_Action_Formation.Text = CStr(Statistiques.NB_ActionFormation)
        Tot_Nb_Heure.Text = WebFct.ViRhDates.CalcHeure(CStr(Statistiques.Durees_Global_H * 60), "", 2)
        Tot_Nb_Jour.Text = WebFct.ViRhFonction.MontantEdite(Statistiques.Durees_Global_J, 2, True)
        Duree_Moyenne_Action_Formation_h.Text = WebFct.ViRhDates.CalcHeure(CStr(Statistiques.Duree_Moyenne_Action_Formation_H * 60), "", 2)
        Duree_Moyenne_Action_Formation_j.Text = WebFct.ViRhFonction.MontantEdite(Statistiques.Duree_Moyenne_Action_Formation_J, 1, True)
        Cout_Tot.Text = WebFct.ViRhFonction.MontantEdite(Statistiques.Couts_Global, 2, True)
        Cout_Pedagogique.Text = WebFct.ViRhFonction.MontantEdite(Statistiques.Couts_Pedagogiques, 2, True)
        Cout_Frais_Mission.Text = WebFct.ViRhFonction.MontantEdite(Statistiques.Couts_Formations, 2, True)
        Cout_Moyen_Action_Formation.Text = WebFct.ViRhFonction.MontantEdite(Statistiques.Cout_Moyen_Action_Formation, 2, True)

    End Sub

    Private Sub MontreResultat(ByVal CacheRes As CacheResultatPerso)
        CtrlPopupResultat.Charge(CacheRes)
        PopupPopupResultat.Show()
    End Sub

    Protected Async Sub LstAnnee_SelectedIndexChanged(sender As Object, e As EventArgs)
        AnneeSel = 0
        CadreBilan.Visible = False
        Statistiques.Clear()
        If ListeAnnees.SelectedIndex <= 0 Then
            Return
        End If
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        AnneeSel = Integer.Parse(ListeAnnees.Text)
        Dim LstStats As List(Of Virtualia.Metier.Formation.StatistiqueFormationInfo) = New List(Of Virtualia.Metier.Formation.StatistiqueFormationInfo)()

        Await System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                               LstStats = Requeteur.UneFormationContinue(Integer.Parse(ListeAnnees.SelectedItem.Text), Identifiant)
                                                           End Sub)
        If LstStats.Count <= 0 Then
            Return
        End If
        Statistiques.AddRange(LstStats)
        Call ChargerTableau()
    End Sub

    Protected Sub Tot_Nb_Action_Formation_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim CacheRes As CacheResultatPerso = New CacheResultatPerso()
        CacheRes.Titre = "Actions de formation"
        CacheRes.NomAgent = lblTitre.Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.ValeurPredicat = "True"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Intitule_Stage"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 50
        pv.TitreColonne = "Stage"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Intitule_Session"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 50
        pv.TitreColonne = "Session"
        CacheRes.ProprietesValeurs.Add(pv)

        Call MontreResultat(CacheRes)
    End Sub

    Protected Sub Tot_Nb_Heure_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim CacheRes As CacheResultatPerso = New CacheResultatPerso()
        CacheRes.Titre = "Nombre d'heures"
        CacheRes.NomAgent = lblTitre.Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 60
        pv.TitreColonne = "Stage / Session"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Dates"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Duree_Heure"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Durée (h)"
        pv.Taille = 15
        CacheRes.ProprietesValeurs.Add(pv)

        Call MontreResultat(CacheRes)
    End Sub

    Protected Sub Tot_Nb_Jour_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim CacheRes As CacheResultatPerso = New CacheResultatPerso()
        CacheRes.Titre = "Nombre de jours"
        CacheRes.NomAgent = lblTitre.Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 60
        pv.TitreColonne = "Stage / Session"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Dates"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Duree_Jour"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Durée (j)"
        pv.Taille = 15
        CacheRes.ProprietesValeurs.Add(pv)

        Call MontreResultat(CacheRes)
    End Sub

    Protected Sub Cout_Tot_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim CacheRes As CacheResultatPerso = New CacheResultatPerso()
        CacheRes.Titre = "Total coûts"
        CacheRes.NomAgent = lblTitre.Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 60
        pv.TitreColonne = "Stage / Session"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Dates"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Couts"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Coûts"
        pv.Taille = 15
        CacheRes.ProprietesValeurs.Add(pv)

        Call MontreResultat(CacheRes)
    End Sub

    Protected Sub Cout_Pedagogique_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim CacheRes As CacheResultatPerso = New CacheResultatPerso()
        CacheRes.Titre = "Coûts pédagogiques"
        CacheRes.NomAgent = lblTitre.Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 60
        pv.TitreColonne = "Stage / Session"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Dates"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Cout_Pedagogiques"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Coûts"
        pv.Taille = 15
        CacheRes.ProprietesValeurs.Add(pv)

        Call MontreResultat(CacheRes)
    End Sub

    Protected Sub Cout_Frais_Mission_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim CacheRes As CacheResultatPerso = New CacheResultatPerso()
        CacheRes.Titre = "Frais de mission"
        CacheRes.NomAgent = lblTitre.Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 60
        pv.TitreColonne = "Stage / Session"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Dates"
        CacheRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Cout_Formations"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Coûts"
        pv.Taille = 15
        CacheRes.ProprietesValeurs.Add(pv)

        Call MontreResultat(CacheRes)
    End Sub

    Private Sub CmdExcel_Click(sender As Object, e As EventArgs) Handles CmdExcel.Click
        Dim FichierCsv As String
        Dim FluxTeleChargement As Byte()
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim Chaine As System.Text.StringBuilder

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        FichierCsv = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirRepertoireTemporaire
        FichierCsv &= "\" & WebFct.PointeurUtilisateur.V_NomdeConnexion & ".csv"

        FicStream = New System.IO.FileStream(FichierCsv, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        Chaine = New System.Text.StringBuilder
        Chaine.Append("Intitulé" & VI.PointVirgule & "Résultat")
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Nb actions de formation" & VI.PointVirgule & CStr(Statistiques.NB_ActionFormation))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Nb heures de formation" & VI.PointVirgule & WebFct.ViRhDates.CalcHeure(CStr(Statistiques.Durees_Global_H * 60), "", 2))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Nb jours de formation" & VI.PointVirgule & WebFct.ViRhFonction.MontantEdite(Statistiques.Durees_Global_J, 2, True))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Durée moyenne action de formation en heures" & VI.PointVirgule & WebFct.ViRhDates.CalcHeure(CStr(Statistiques.Duree_Moyenne_Action_Formation_H * 60), "", 2))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Durée moyenne action de formation en jours" & VI.PointVirgule & WebFct.ViRhFonction.MontantEdite(Statistiques.Duree_Moyenne_Action_Formation_J, 1, True))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Total coûts" & VI.PointVirgule & WebFct.ViRhFonction.MontantEdite(Statistiques.Couts_Global, 2, True))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Coûts pédagogiques" & VI.PointVirgule & WebFct.ViRhFonction.MontantEdite(Statistiques.Couts_Pedagogiques, 2, True))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Coûts frais de mission" & VI.PointVirgule & WebFct.ViRhFonction.MontantEdite(Statistiques.Couts_Formations, 2, True))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()
        Chaine.Append("Coût moyen par action de formation" & VI.PointVirgule & WebFct.ViRhFonction.MontantEdite(Statistiques.Cout_Moyen_Action_Formation, 2, True))
        FicWriter.WriteLine(Chaine.ToString)

        FicWriter.Flush()
        FicWriter.Close()

        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(FichierCsv)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & WebFct.PointeurUtilisateur.V_NomdeConnexion & ".csv" & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub
End Class