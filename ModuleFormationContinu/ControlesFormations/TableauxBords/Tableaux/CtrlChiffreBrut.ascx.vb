﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Structure.Formation
Public Class CtrlChiffreBrut
    Inherits UserControl
    Implements IControlBase
    Implements ICtrlExport
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Private WsCtrlGestion As CtrlGestionTableauBord

    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        EtiEffectif.Text = "Effectif au 31/12/" & WsCtrlGestion.VCache.Annee

        Dim stats As Virtualia.Metier.Formation.StatistiqueFormationInfoCollection = WsCtrlGestion.VCache.Statistiques

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Tot_Nb_Action_Formation.Text = WebFct.ViRhFonction.MontantEdite(stats.NB_ActionFormation, 0, True)
        Tot_Nb_Benef.Text = WebFct.ViRhFonction.MontantEdite(stats.NB_Beneficiaire, 0, True)
        Tot_Nb_Stagiaire.Text = WebFct.ViRhFonction.MontantEdite(stats.NB_Stagiaire, 0, True)
        Tot_Nb_Effectif.Text = WebFct.ViRhFonction.MontantEdite(stats.Effectif, 0, True)

        Tot_Nb_Heure.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Durees_Global_H * 60), "", 2)
        Tot_Nb_Jour.Text = WebFct.ViRhFonction.MontantEdite(stats.Durees_Global_J, 2, True)

        Duree_Moyenne_Action_Formation_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Action_Formation_H * 60), "", 2)
        Duree_Moyenne_Beneficiaire_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Beneficiaire_H * 60), "", 2)
        Duree_Moyenne_Stagiaire_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Stagiaire_H * 60), "", 2)
        Duree_Moyenne_Effectif_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Effectif_H * 60), "", 2)

        Duree_Moyenne_Action_Formation_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Action_Formation_J, 1, True)
        Duree_Moyenne_Beneficiaire_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Beneficiaire_J, 1, True)
        Duree_Moyenne_Stagiaire_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Stagiaire_J, 1, True)
        Duree_Moyenne_Effectif_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Effectif_J, 1, True)

        Cout_Tot.Text = WebFct.ViRhFonction.MontantEdite(stats.Couts_Global, 2, True)
        Cout_Pedagogique.Text = WebFct.ViRhFonction.MontantEdite(stats.Couts_Pedagogiques, 2, True)
        Cout_Frais_Mission.Text = WebFct.ViRhFonction.MontantEdite(stats.Couts_Formations, 2, True)
        Cout_Masse_Salarial.Text = ""

        Cout_Moyen_Action_Formation.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Action_Formation, 2, True)
        Cout_Moyen_Beneficiaire.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Beneficiaire, 2, True)
        Cout_Moyen_Stagiaires.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Stagiaire, 2, True)
        Cout_Moyen_Effectif.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Effectif, 2, True)

        If stats.Effectif > 0 Then
            Pourcent_Effectif_Forme.Text = Math.Round((stats.NB_Beneficiaire / stats.Effectif) * 100, 2) & " %"
        Else
            Pourcent_Effectif_Forme.Text = ""
        End If
        If stats.Effectif > 0 Then
            Pourcent_Effectif_Non_Forme.Text = Math.Round((stats.NB_Non_Forme / stats.Effectif) * 100, 2) & " %"
        Else
            Pourcent_Effectif_Non_Forme.Text = ""
        End If

        VisuHelper.GetControlSaisie(Of Button)(CadreChiffres).ForEach(Sub(bt)
                                                                          bt.Enabled = (bt.Text <> "")
                                                                      End Sub)
    End Sub

    Public Function ToValeurs(extension As TypeExport) As List(Of String)
        Dim LstResultat As List(Of String) = New List(Of String)
        Dim contenu As ContenuExport

        For Each Rangee As TableRow In CadreChiffres.Rows
            contenu = New ContenuExport(extension)
            For Each Cellule As TableCell In Rangee.Cells
                For Each Controle As System.Web.UI.Control In Cellule.Controls
                    If TypeOf Controle Is Label Then
                        contenu.Add(CType(Controle, Label).Text)
                    ElseIf TypeOf Controle Is Button Then
                        contenu.Add(CType(Controle, Button).Text)
                    End If
                Next
            Next
            LstResultat.Add(contenu.ToString())
        Next
        Return LstResultat
    End Function

    Public Function GetExport() As ExportPdfInfo Implements ICtrlExport.GetExport
        Dim SHtml As String = Me.GetSourceHtml()
        Dim SContexte As String = ""

        SContexte = "Année : " & WsCtrlGestion.VCache.Annee & "; "
        If WsCtrlGestion.VCache.Plan = "" Then
            SContexte = SContexte & "Plan de formation : Tous; "
        Else
            SContexte = SContexte & "Plan de formation : " & WsCtrlGestion.VCache.Plan & "; "
        End If
        If WsCtrlGestion.VCache.Etablissement = "" Then
            SContexte = SContexte & "Etablissement : Tous"
        Else
            SContexte = SContexte & "Etablissement : " & WsCtrlGestion.VCache.Etablissement
        End If

        Dim FormatContexte As String = "<tr><td align=""center""><span id=""Principal_CtrlGestion_CtrlGestTableau_CtrlVue_CtrlChiffresBruts_EtiContexte"" style=""display:inline-block;color:#B0E0D7;background-color:#124545;border-color:#B0E0D7;border-width:2px;border-style:solid;width:600px;font-style: oblique; text-indent: 5px; text-align: center;"">{0}</span></td></tr>"""
        Dim ssplit As String() = SHtml.Split(New String() {"</tr>"}, StringSplitOptions.None)

        SHtml = ""
        Dim SiPremier As Boolean = True

        For Each Chaine In ssplit
            If SHtml = "" Then
                SHtml &= Chaine
            Else
                SHtml &= "</tr>" & Chaine
            End If
            If SiPremier = True Then
                SiPremier = False
                SHtml &= String.Format(FormatContexte, Server.HtmlEncode(scontexte))
            End If
        Next
        Dim CRetour As ExportPdfInfo = New ExportPdfInfo() With {.NomFichier = "ChiffresBruts", .ContenuHtml = SHtml}
        If CRetour.ContenuHtml = "" Then
            CRetour.EstOk = False
        End If
        Return CRetour
    End Function

    Public Sub BloqueOuNonControles(ByVal SiEnabled As Boolean)
        Tot_Nb_Action_Formation.Enabled = SiEnabled
        Tot_Nb_Benef.Enabled = SiEnabled
        Tot_Nb_Stagiaire.Enabled = SiEnabled
        Tot_Nb_Effectif.Enabled = SiEnabled

        Tot_Nb_Heure.Enabled = SiEnabled
        Tot_Nb_Jour.Enabled = SiEnabled

        Duree_Moyenne_Action_Formation_h.Enabled = SiEnabled
        Duree_Moyenne_Beneficiaire_h.Enabled = SiEnabled
        Duree_Moyenne_Stagiaire_h.Enabled = SiEnabled
        Duree_Moyenne_Effectif_h.Enabled = SiEnabled

        Duree_Moyenne_Action_Formation_j.Enabled = SiEnabled
        Duree_Moyenne_Beneficiaire_j.Enabled = SiEnabled
        Duree_Moyenne_Stagiaire_j.Enabled = SiEnabled
        Duree_Moyenne_Effectif_j.Enabled = SiEnabled

        Cout_Tot.Enabled = SiEnabled
        Cout_Pedagogique.Enabled = SiEnabled
        Cout_Frais_Mission.Enabled = SiEnabled
        Cout_Masse_Salarial.Enabled = SiEnabled

        Cout_Moyen_Action_Formation.Enabled = SiEnabled
        Cout_Moyen_Beneficiaire.Enabled = SiEnabled
        Cout_Moyen_Stagiaires.Enabled = SiEnabled
        Cout_Moyen_Effectif.Enabled = SiEnabled

        Pourcent_Effectif_Forme.Enabled = SiEnabled
        Pourcent_Effectif_Non_Forme.Enabled = SiEnabled
    End Sub

    Protected Sub Tot_Nb_Action_Formation_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Exit Sub
        End If

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Actions de formation"
        cr.EstSurStat = True
        cr.NomFichierExport = "Actions_de_formation"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Intitule_Stage"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 50
        pv.TitreColonne = "Stage"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Intitule_Session"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 50
        pv.TitreColonne = "Session"
        cr.ProprietesValeurs.Add(pv)

        WsCtrlGestion.MontreResultat(cr)

    End Sub

    Protected Sub Tot_Nb_Effectif_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Effectif"
        cr.EstSurStat = False
        cr.NomFichierExport = "Effectif"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Etablissement"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 40
        pv.TitreColonne = "Etablissement"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Date_Naissance"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TDate
        pv.Taille = 10
        pv.TitreColonne = "Nais."
        cr.ProprietesValeurs.Add(pv)

        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Tot_Nb_Benef_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If
        Dim cr As CacheResultat = GestionListeResultat.GetListePVI_Beneficiaire()
        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Tot_Nb_Stagiaire_Click(sender As Object, e As EventArgs)
        If (DirectCast(sender, Button).Text = "") Then
            Return
        End If
        Dim cr As CacheResultat = GestionListeResultat.GetListePVI_Stagiaire()
        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Tot_Nb_Heure_Click(sender As Object, e As EventArgs)
        If (DirectCast(sender, Button).Text = "") Then
            Return
        End If
        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Nombre d'heures"
        cr.EstSurStat = True
        cr.NomFichierExport = "Nombre_Heure"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 35
        pv.TitreColonne = "Stage / Session"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 15
        pv.TitreColonne = "Dates"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Duree_Heure"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Durée (h)"
        pv.Taille = 10
        cr.ProprietesValeurs.Add(pv)

        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Tot_Nb_Jour_Click(sender As Object, e As EventArgs)
        If (DirectCast(sender, Button).Text = "") Then
            Return
        End If
        Dim cr As CacheResultat = GestionListeResultat.GetListePVI_Jour()
        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Cout_Tot_Click(sender As Object, e As EventArgs)
        If (DirectCast(sender, Button).Text = "") Then
            Return
        End If
        Dim cr As CacheResultat = GestionListeResultat.GetListePVI_Cout()
        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Cout_Pedagogique_Click(sender As Object, e As EventArgs)
        If DirectCast(sender, Button).Text = "" Then
            Return
        End If

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Coûts pédagogiques"
        cr.EstSurStat = True
        cr.NomFichierExport = "Couts_Pedagogiques"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 35
        pv.TitreColonne = "Stage / Session"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 15
        pv.TitreColonne = "Dates"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Cout_Pedagogiques"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Coûts"
        pv.Taille = 10
        cr.ProprietesValeurs.Add(pv)

        WsCtrlGestion.MontreResultat(cr)
    End Sub

    Protected Sub Cout_Frais_Mission_Click(sender As Object, e As EventArgs)
        If (DirectCast(sender, Button).Text = "") Then
            Return
        End If

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Frais de mission"
        cr.EstSurStat = True
        cr.NomFichierExport = "Frais_Mission"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 35
        pv.TitreColonne = "Stage / Session"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 15
        pv.TitreColonne = "Dates"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Cout_Formations"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Coûts"
        pv.Taille = 10
        cr.ProprietesValeurs.Add(pv)

        WsCtrlGestion.MontreResultat(cr)
    End Sub

End Class