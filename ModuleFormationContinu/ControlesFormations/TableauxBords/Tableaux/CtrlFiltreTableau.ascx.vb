﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports System.Threading.Tasks
Public Class CtrlFiltreTableau
    Inherits UserControl
    Private Const CouleurEnable As String = "#124545"
    Private Const NbAnneeChargement As Integer = 3
    Private WsCtrlGestion As CtrlGestionTableauBord
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property FenetreMere As FrmTableauxBord
        Get
            Return DirectCast(Page, FrmTableauxBord)
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
        End Set
    End Property

    Private Property IndexTraitement As Integer
        Get
            If (Not ViewState.KeyExiste("IndexTraitement")) Then
                ViewState.AjouteValeur("IndexTraitement", 9999)
            End If
            Return DirectCast(ViewState("IndexTraitement"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexTraitement", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        If IsPostBack = False Then
            Call ChargeCboAnnee()
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)
        If TimerAttente.Enabled = False Then
            Exit Sub
        End If
        Dim IndiceI As Integer = FiltrerAsync().Result
        IndexTraitement = IndiceI
        If IndexTraitement = 5 Then
            TimerAttente.Enabled = False
            WsCtrlGestion.AfficheStatistiques()
            Exit Sub
        End If
        If IndexTraitement > 5 Then
            TimerAttente.Enabled = False
        End If
    End Sub

    Public Sub Charge()
        ListeAnnee.Focus()
        WsCtrlGestion.VCache.Statistiques.PoidsHeure = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).ParametreFormation.Poids_Heure
        ListeAnnee.SelectedIndex = 0
        VideEcran()
    End Sub

    Private Sub VideEcran()
        WsCtrlGestion.VCache.Vider()

        ChargeCbo(ListePlan, Nothing)
        ChargeCbo(ListeEtab, Nothing)

        GereEnabled(ListePlan, EtiPlan, False)
        GereEnabled(ListeEtab, EtiEtab, False)

        CmdExecuter.Enabled = False

        ListePlan.SelectedIndex = 0
        ListeEtab.SelectedIndex = 0
    End Sub

    Private Sub ChargeCbo(ByVal ListeCombo As DropDownList, ByVal LstValeurs As List(Of ListItem))
        ListeCombo.Items.Clear()
        ListeCombo.Items.Add(GetItemVide())
        If LstValeurs Is Nothing Then
            Return
        End If
        LstValeurs.ForEach(Sub(it)
                               ListeCombo.Items.Add(it)
                           End Sub)
    End Sub

    Private Function GetItemVide() As ListItem
        Return New ListItem("Tous...", "0")
    End Function

    Private Sub GereEnabled(ByVal ListeCombo As DropDownList, ByVal Etiquette As Label, ByVal SiEnabled As Boolean)
        ListeCombo.Enabled = SiEnabled
        Dim Couleur As System.Drawing.Color = VisuHelper.ConvertiCouleur(CouleurEnable)
        If SiEnabled = True Then
            Couleur = VisuHelper.ConvertiCouleur(CouleurEnable)
        Else
            Couleur = System.Drawing.Color.Gray
        End If
        Etiquette.BackColor = Couleur
    End Sub

    Private Sub ChargeCboAnnee()
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        ListeAnnee.Items.Add(New ListItem("Sélectionner", "0"))
        Dim LstValeurs As List(Of ListItem) = Requeteur.ListeAnneesPourFiltre(NbAnneeChargement)
        LstValeurs.ForEach(Sub(it)
                               ListeAnnee.Items.Add(it)
                           End Sub)
    End Sub

    Private Async Function FiltrerAsync() As Task(Of Integer)
        Dim functrait As Func(Of Integer) = Function()
                                                Return Filtrer()
                                            End Function
        Return Await Task.FromResult(Of Integer)(functrait())
    End Function

    Private Function Filtrer() As Integer
        Dim CRetour As Integer = 0
        Select Case IndexTraitement
            Case 0
                If TraiteEffectif() <= 0 Then
                    GereEnabled(ListeAnnee, EtiAnnee, True)
                    GereEnabled(ListeEtab, EtiEtab, True)
                    GereEnabled(ListePlan, EtiPlan, True)
                    CmdExecuter.Enabled = True
                    Return 999
                End If
                CRetour = 1
            Case 1
                If TraiteChargement() <= 0 Then
                    GereEnabled(ListeAnnee, EtiAnnee, True)
                    GereEnabled(ListeEtab, EtiEtab, True)
                    GereEnabled(ListePlan, EtiPlan, True)
                    CmdExecuter.Enabled = True
                    Return 999
                End If
                CRetour = 2
            Case 2
                'chargement des categories
                TraiteCategorie()
                CRetour = 3
            Case 3
                'chargement des statuts
                TraiteStatut()
                CRetour = 4
            Case 4
                'chargement des niveauxs
                TraiteNiveaux()
                GereEnabled(ListeAnnee, EtiAnnee, True)
                GereEnabled(ListeEtab, EtiEtab, True)
                GereEnabled(ListePlan, EtiPlan, True)
                CRetour = 5
        End Select
        Return CRetour
    End Function

    Private Function TraiteEffectif() As Integer
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Dim LstResultat As List(Of Virtualia.Metier.Formation.StatistiqueEffectifInfo)

        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        LstResultat = Requeteur.ObtenirEffectifComplet(WsCtrlGestion.VCache.Annee, WsCtrlGestion.VCache.Etablissement)
        If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
            WsCtrlGestion.VCache.Statistiques.Effectif = 0
            Return 0
        End If
        LstResultat.ForEach(Sub(it)
                                WsCtrlGestion.VCache.Effectifs.Add(it)
                            End Sub)
        WsCtrlGestion.VCache.Statistiques.Effectif = LstResultat.Count
        Return LstResultat.Count
    End Function

    Private Function TraiteChargement() As Integer
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Dim LstResultat As List(Of Virtualia.Metier.Formation.StatistiqueFormationInfo)

        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        LstResultat = Requeteur.LesFormationContinues(WsCtrlGestion.VCache.Annee, WsCtrlGestion.VCache.Plan, WsCtrlGestion.VCache.Etablissement)
        If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
            WsCtrlGestion.VCache.Statistiques.Effectif = 0
            Return 0
        End If
        LstResultat.ForEach(Sub(it)
                                WsCtrlGestion.VCache.Statistiques.Add(it)
                            End Sub)
        Return LstResultat.Count
    End Function

    Private Sub TraiteCategorie()
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        Call Requeteur.AffecteValeurCategorie(WsCtrlGestion.VCache.Annee, WsCtrlGestion.VCache.Plan, WsCtrlGestion.VCache.Statistiques)
    End Sub

    Private Sub TraiteStatut()
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        Call Requeteur.AffecteValeurStatut(WsCtrlGestion.VCache.Annee, WsCtrlGestion.VCache.Plan, WsCtrlGestion.VCache.Statistiques)
    End Sub

    Private Sub TraiteNiveaux()
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
        Requeteur.AffecteValeurNiveau(WsCtrlGestion.VCache.Annee, WsCtrlGestion.VCache.Plan, WsCtrlGestion.VCache.Statistiques)
    End Sub

    Protected Async Sub cboAnnee_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
        Requeteur = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur

        VideEcran()
        If ListeAnnee.SelectedItem.Value = "0" Then
            ListeAnnee.Focus()
            Exit Sub
        End If
        WsCtrlGestion.VCache.Annee = Integer.Parse(ListeAnnee.SelectedItem.Value)

        Await Task.Factory.StartNew(Sub()
                                        ChargeCbo(ListeEtab, Requeteur.ListeEtablissementPourFiltre(WsCtrlGestion.VCache.Annee))
                                    End Sub)
        Await Task.Factory.StartNew(Sub()
                                        ChargeCbo(ListePlan, Requeteur.ListePlanPourFiltre(WsCtrlGestion.VCache.Annee))
                                    End Sub)

        GereEnabled(ListeEtab, EtiEtab, True)
        GereEnabled(ListePlan, EtiPlan, True)

        CmdExecuter.Enabled = True
        CmdExecuter.Focus()
    End Sub

    Protected Sub CboEtab_SelectedIndexChanged(sender As Object, e As EventArgs)
        CmdExecuter.Enabled = True
        CmdExecuter.Focus()
        If ListeEtab.SelectedItem.Value = "0" Then
            WsCtrlGestion.VCache.Etablissement = ""
        Else
            WsCtrlGestion.VCache.Etablissement = ListeEtab.SelectedItem.Value
        End If
        WsCtrlGestion.VCache.Statistiques.Clear()
        WsCtrlGestion.VCache.Effectifs.Clear()
        WsCtrlGestion.VCache.EstRecharge = True

    End Sub

    Protected Sub CboPlan_SelectedIndexChanged(sender As Object, e As EventArgs)
        CmdExecuter.Enabled = True
        CmdExecuter.Focus()
        If ListePlan.SelectedItem.Value = "0" Then
            WsCtrlGestion.VCache.Plan = ""
        Else
            WsCtrlGestion.VCache.Plan = ListePlan.SelectedItem.Value
        End If
        WsCtrlGestion.VCache.Statistiques.Clear()
        WsCtrlGestion.VCache.Effectifs.Clear()
        WsCtrlGestion.VCache.EstRecharge = True

    End Sub

    Private Sub CmdExecuter_Click(sender As Object, e As EventArgs) Handles CmdExecuter.Click
        IndexTraitement = 0
        CmdExecuter.Enabled = False
        Call GereEnabled(ListeAnnee, EtiAnnee, False)
        Call GereEnabled(ListeEtab, EtiEtab, False)
        Call GereEnabled(ListePlan, EtiPlan, False)
        TimerAttente.Enabled = True
    End Sub
End Class