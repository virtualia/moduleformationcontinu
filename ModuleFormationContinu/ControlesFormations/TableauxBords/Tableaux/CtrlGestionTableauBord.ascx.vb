﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Net.Session
Imports System.IO
Public Class CtrlGestionTableauBord
    Inherits UserControl
    Implements IControlBase

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Public ReadOnly Property VCache As DonneeStatistiqueFormationInfo
        Get
            Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            Return DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, LDObjetSession).DonneeState
        End Get
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)

        CtrlFiltre.CtrlGestion = Me
        CtrlVue.CtrlGestion = Me
        CtrlPopupResultat.CtrlGestion = Me
        CtrlPopupResultat.NbItemParPage = 200
        CtrlPopupResultat.Popup = PopupPopupResultat
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        VCache.Vider()

        CtrlFiltre.Charge()
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Public Sub AfficheStatistiques()
        CtrlVue.Charge()
        MultiOnglets.ActiveViewIndex = 1
    End Sub

    Public Sub GereRetour()
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Public Sub MontreResultat(ByVal CacheRes As CacheResultat)
        CtrlPopupResultat.Charge(CacheRes)
        PopupPopupResultat.Show()
    End Sub

    Public Sub GereExport(ByVal NomFichier As String, ByVal Extension As String, ByVal LstValeurs As List(Of String))
        Dim NomFicComplet As String
        Dim FluxExport As Byte()

        NomFicComplet = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirRepertoireTemporaire
        NomFicComplet &= "\" & Session.SessionID & "." & Extension

        Using FicStream As FileStream = New FileStream(NomFicComplet, IO.FileMode.Create, IO.FileAccess.Write)
            Using FicWriter As StreamWriter = New StreamWriter(FicStream, Encoding.GetEncoding(1252))
                For Each Ligne In LstValeurs
                    FicWriter.WriteLine(Ligne)
                Next
                FicWriter.Flush()
                FicWriter.Close()
            End Using
            FicStream.Close()
        End Using
        FluxExport = My.Computer.FileSystem.ReadAllBytes(NomFicComplet)
        If FluxExport IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "." & Extension & "; size=" & FluxExport.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxExport)
            response.Flush()
            response.End()
        End If
    End Sub

    Public Sub GereExportPdf(ByVal CtExp As ICtrlExport, Optional ByVal SiPaysage As Boolean = True)
        Dim FichierHtml As String
        Dim Util As Virtualia.Systeme.Outils.Utilitaires
        Dim FluxHtml As String
        Dim FluxPDF As Byte()

        If CtExp Is Nothing Then
            Return
        End If
        Dim epi As ExportPdfInfo = CtExp.GetExport()
        If epi.EstOk = False Then
            'ERREUR
            Return
        End If
        FichierHtml = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirRepertoireTemporaire
        FichierHtml &= "\" & Session.SessionID & ".html"

        Util = New Virtualia.Systeme.Outils.Utilitaires
        Call Util.SourceHtmlEditee(FichierHtml, epi.ContenuHtml)
        Call Util.TransformeFicHtml(FichierHtml)

        FluxHtml = My.Computer.FileSystem.ReadAllText(FichierHtml, System.Text.Encoding.UTF8)
        FluxPDF = DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase).SrvOutils.ConversionPDF(FluxHtml, SiPaysage)
        If FluxPDF IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & epi.NomFichier & ".pdf" & "; size=" & FluxPDF.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxPDF)
            response.Flush()
            response.End()
        End If
    End Sub

End Class