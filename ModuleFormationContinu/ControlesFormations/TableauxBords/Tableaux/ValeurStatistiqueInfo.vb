﻿
Public Class CleeValeurStatistiqueInfo
    Public Property Clee As String = ""
    Public Property ValeurDuPredicat As ValeurStatistiqueInfo = Nothing
End Class

Public Class ValeurStatistiqueInfo

    Public Property NbBenef As Integer = 0
    Public Property PourcentBenef As Double = 0

    Public Property NbStag As Integer = 0
    Public Property PourcentStag As Double = 0

    Public Property Duree As Double = 0
    Public Property PourcentDuree As Double = 0

    Public Property Cout As Double = 0
    Public Property PourcentCout As Double = 0

End Class
