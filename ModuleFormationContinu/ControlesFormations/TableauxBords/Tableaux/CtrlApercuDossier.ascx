﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlApercuDossier.ascx.vb" Inherits="Virtualia.Net.CtrlApercuDossier" %>

<%@ Register Src="~/ControlesFormations/TableauxBords/ExportPdf/CtrlDossierExport.ascx" TagName="CTL_APERCU" TagPrefix="Formation" %>
<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:Table ID="TbGenerale" runat="server" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" >
     <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Width="1050px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px">
                        <asp:ImageButton ID="BtnRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" ToolTip="Retour vers le tableau de bord" OnClick="BtnRetour_Click"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" Text="Apercu du dossier" Height="22px" Width="400px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="100px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Right">
            <asp:UpdatePanel ID="UpdatePanelExport" runat="server">
                <ContentTemplate>
                    <Generic:BTN_GENE ID="BtnPdf" runat="server" Text="Fichier PDF" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="BtnPdf" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Formation:CTL_APERCU ID="CtrlApercu" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>
</asp:Table>
