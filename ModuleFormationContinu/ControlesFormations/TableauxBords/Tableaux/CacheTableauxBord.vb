﻿Imports Virtualia.Metier.Formation

Public Interface ICtrlGestionDonneeStatistiquesFormation
    ReadOnly Property VCache As DonneeStatistiqueFormationInfo
End Interface

<Serializable>
Public Class DonneeStatistiqueFormationInfo
    Public Const KeyState As String = "CacheTableauxBord"

    Public Property Annee As Integer = 0
    Public Property Etablissement As String = ""
    Public Property Plan As String = ""
    Public Property EstRecharge As Boolean = True

    Public Property Statistiques As StatistiqueFormationInfoCollection = New StatistiqueFormationInfoCollection()
    Public Property Effectifs As StatistiqueEffectifInfoCollection = New StatistiqueEffectifInfoCollection()

    Public Sub Vider()
        Annee = 0
        Etablissement = ""
        Plan = ""
        EstRecharge = True

        Statistiques.Clear()
        Effectifs.Clear()
    End Sub
End Class
