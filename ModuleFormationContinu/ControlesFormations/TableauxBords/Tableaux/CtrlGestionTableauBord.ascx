﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlGestionTableauBord.ascx.vb" Inherits="Virtualia.Net.CtrlGestionTableauBord" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlFiltreTableau.ascx" TagName="CTL_FILTRE" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/CtrlVueTableauBord.ascx" TagName="CTL_VUE" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/Tableaux/PopupResultat.ascx" TagName="CTL_POPUPRESULTAT" TagPrefix="Formation" %>

<asp:Table ID="CadreControle" runat="server" HorizontalAlign="Center" Width="1150px" Font-Names="Trebuchet MS"
    Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7">
    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="Tableaux de bord" Width="700px" Height="22px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadrePatience" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="40px">
                        <asp:UpdateProgress ID="UpdateProgressAttente" runat="server">
                            <ProgressTemplate>
                                <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
                                    <asp:TableRow>
                                        <asp:TableCell Width="30px" Height="30px" HorizontalAlign="Right">
                                            <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                        </asp:TableCell>
                                        <asp:TableCell Width="10px" Height="10px" HorizontalAlign="Center" />
                                        <asp:TableCell HorizontalAlign="Left">
                                            <asp:Label ID="EtiAttente" runat="server" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="center">
            <asp:UpdatePanel ID="UpdateProgressVues" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                        <asp:View ID="Vue1" runat="server">
                            <Formation:CTL_FILTRE ID="CtrlFiltre" runat="server" />
                        </asp:View>
                        <asp:View ID="Vue2" runat="server">
                            <Formation:CTL_VUE ID="CtrlVue" runat="server" />
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <ajaxToolkit:ModalPopupExtender ID="PopupPopupResultat" runat="server" TargetControlID="HidenPopupResultat" PopupControlID="PanelResultatPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelResultatPopup" runat="server">
                <Formation:CTL_POPUPRESULTAT ID="CtrlPopupResultat" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HidenPopupResultat" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
  