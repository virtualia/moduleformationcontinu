﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlGraphique.ascx.vb" Inherits="Virtualia.Net.CtrlGraphique" %>


<%--<asp:UpdatePanel ID="eeee" runat="server">
    <ContentTemplate>--%>

<asp:Table ID="TB1" runat="server" Visible="true" Width="1005px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" ColumnSpan="2" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
            <asp:Table ID="TB2" runat="server" Width="700px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px" />
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" Text="Graphiques" Height="22px" Width="600px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="100px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" ColumnSpan="2" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:Table ID="Table1" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" >
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label8" runat="server" Text="Répartition par" Width="120px" Height="16px" BackColor="Transparent" BorderColor="Transparent" BorderStyle="None" BorderWidth="2px" ForeColor="#124545" Style="font-style: oblique; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="1px" />
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="BtnSEXE" runat="server" Text="sexe" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="BtnCATEGORIE" runat="server" Text="catégorie" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Button ID="BtnSTATUT" runat="server" Text="statut" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Button ID="BtnDOMAINE" runat="server" Text="domaine" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Button ID="BtnTHEME" runat="server" Text="thème" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Button ID="BtnTYPEACTION" runat="server" Text="type d'action" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Button ID="BtnNIVEAU" runat="server" Text="niveau 1" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Button ID="BtnNIVEAU2" runat="server" Text="niveau 2" Width="120px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; cursor:pointer" OnClick="BtnVue_Click" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>

        <asp:TableCell VerticalAlign="Top">

            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <asp:Table ID="TB_SEXE" runat="server" HorizontalAlign="Center" Width="980px">
                        <asp:TableRow>
                            <asp:TableCell BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                                <asp:Table ID="TB5" runat="server" >
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Center">
                                            <asp:Chart ID="Sexe_NB" runat="server" Palette="SeaGreen" BackColor="WhiteSmoke" Height="250px" Width="250px" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title2" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 10pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series Name="Default" ChartType="Doughnut" BorderColor="180, 26, 59, 105" />
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle Rotation="10" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                        <asp:TableCell ID="Cell_Sexe_Cout" HorizontalAlign="Center">
                                            <asp:Chart ID="Sexe_Cout" runat="server" Palette="SeaGreen" BackColor="WhiteSmoke" Height="250px" Width="250px" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title2" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 10pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series Name="Default" ChartType="Doughnut" BorderColor="180, 26, 59, 105" />
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle Rotation="10" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                        <asp:TableCell ID="Cell_Sexe_Jour" HorizontalAlign="Center">
                                            <asp:Chart ID="Sexe_Jour" runat="server" Palette="SeaGreen" BackColor="WhiteSmoke" Height="250px" Width="250px" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title2" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 10pt, style=Bold" ShadowOffset="3" Text="Nombre de jours" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series Name="Default" ChartType="Doughnut" BorderColor="180, 26, 59, 105" />
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle Rotation="10" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Center">
                                <asp:Chart ID="Sexe_Effectif" runat="server" Palette="SeaGreen" BackColor="WhiteSmoke" Height="180px" Width="180px" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title2" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 10pt, style=Bold" ShadowOffset="3" Text="Effectif au 31/12" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series Name="Default" ChartType="Doughnut" BorderColor="180, 26, 59, 105" />
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle Rotation="10" />
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View2" runat="server">
                    <asp:Table ID="TB_CATEGORIE" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Chart ID="Categorie_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires / Effectif au 31/12" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Effectif" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Categorie_Cout" HorizontalAlign="Center">
                                <asp:Chart ID="Categorie_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Categorie_Jour" HorizontalAlign="Center">
                                <asp:Chart ID="Categorie_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View3" runat="server">
                    <asp:Table ID="TB_STATUT" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Chart ID="Statut_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires / Effectif au 31/12" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Effectif" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Statut_Cout" HorizontalAlign="Center">
                                <asp:Chart ID="Statut_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Statut_Jour" HorizontalAlign="Center">
                                <asp:Chart ID="Statut_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View4" runat="server">
                    <asp:Table ID="TB_DOMAINE" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Chart ID="Domaine_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>

                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Domaine_Cout" HorizontalAlign="Center">
                                <asp:Chart ID="Domaine_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Domaine_Jour" HorizontalAlign="Center">
                                <asp:Chart ID="Domaine_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View5" runat="server">
                    <asp:Table ID="TB_THEME" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Chart ID="Theme_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>

                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="false" WallWidth="0" IsClustered="false" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Theme_Cout" HorizontalAlign="Center">
                                <asp:Chart ID="Theme_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Theme_Jour" HorizontalAlign="Center">
                                <asp:Chart ID="Theme_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View6" runat="server">
                    <asp:Table ID="TB_TYPEACTION" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Chart ID="TypeAction_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_TypeAction_Cout" HorizontalAlign="Center">
                                <asp:Chart ID="TypeAction_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_TypeAction_Jour" HorizontalAlign="Center">
                                <asp:Chart ID="TypeAction_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View7" runat="server">
                    <asp:Table ID="TB_NIVEAU" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Chart ID="Niveau1_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires / Effectif au 31/12" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Effectif" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Niveau1_Cout" HorizontalAlign="Center">
                                <asp:Chart ID="Niveau1_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell ID="Cell_Niveau1_Jour" HorizontalAlign="Center">
                                <asp:Chart ID="Niveau1_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                    <Titles>
                                        <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                            <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                            <AxisY LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX LineColor="64, 64, 64, 64">
                                                <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

                <asp:View ID="View8" runat="server">
                    <asp:Table ID="Table3" runat="server" HorizontalAlign="Center" Width="980px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                        <asp:TableRow>
                            <asp:TableCell ID="CellCboNiveau1" HorizontalAlign="Center">
                                <asp:Table ID="Table2" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right">
                                            <asp:Label ID="Label9" runat="server" Text="Voir la répartition du niveau 2 par le niveau 1 " Height="20px" Width="300px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="CboNiveau1" runat="server" Height="20px" Width="400px" BackColor="#EEECFD" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" AutoPostBack="true" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table ID="TB_NIVEAU2" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Center">
                                            <asp:Chart ID="Niveau2_NB" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires / Effectif au 31/12" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series ChartArea="ChartArea1" Name="Effectif" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                                    </asp:Series>
                                                    <asp:Series ChartArea="ChartArea1" Name="Bénéficiaires" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 65, 140, 240">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                                        <AxisY LineColor="64, 64, 64, 64">
                                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisY>
                                                        <AxisX LineColor="64, 64, 64, 64">
                                                            <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell ID="Cell_Niveau2_Cout" HorizontalAlign="Center">
                                            <asp:Chart ID="Niveau2_Cout" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Coûts" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series ChartArea="ChartArea1" Name="Coûts" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                                        <AxisY LineColor="64, 64, 64, 64">
                                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisY>
                                                        <AxisX LineColor="64, 64, 64, 64">
                                                            <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell ID="Cell_Niveau2_Jour" HorizontalAlign="Center">
                                            <asp:Chart ID="Niveau2_Jour" runat="server" Width="900px" Height="600px" Palette="SeaGreen" BackColor="WhiteSmoke" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title1" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 14.25pt, style=Bold" ShadowOffset="3" Text="Jours" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series ChartArea="ChartArea1" Name="Jours" ChartType="Column" BorderColor="180, 26, 59, 105" Color="220, 252, 180, 65">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle PointGapDepth="0" Rotation="10" Enable3D="True" Inclination="15" IsRightAngleAxes="False" WallWidth="0" IsClustered="False" />
                                                        <AxisY LineColor="64, 64, 64, 64">
                                                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisY>
                                                        <AxisX LineColor="64, 64, 64, 64">
                                                            <LabelStyle Font="Trebuchet MS, 9pt" Interval="1" />
                                                            <MajorGrid LineColor="64, 64, 64, 64" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>

            </asp:MultiView>

        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
