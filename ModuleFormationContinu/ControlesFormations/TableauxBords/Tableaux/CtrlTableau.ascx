﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlTableau.ascx.vb" Inherits="Virtualia.Net.CtrlTableau" %>

<asp:Table ID="TbGenerale" runat="server" Width="1005px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="Tableaux de synthèse" Height="22px" Width="500px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="RowDetail" Visible="false">

        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TbDetail" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                <asp:TableRow>
                    <asp:TableCell Height="2px" />
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="tb4" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Label ID="EtiDate" runat="server" Text="Du XX/XX/XXXX au XX/XX/XXXX" Height="20px" Width="200px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="tb5" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Label ID="EtiPlan" runat="server" Text="Plan XXXXXXXXXXXXXXXXXXXXXXXXXXXX" Height="20px" Width="600px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="tb6" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Label ID="EtiEtab" runat="server" Text="Etablissement XXXXXXXXXXXXXXXXXXXXXXXXXXXX" Height="20px" Width="600px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreDonnees" runat="server" CellPadding="0" CellSpacing="0" ForeColor="Black" Font-Size="90%">

                <%--<asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label3" runat="server" Text="Nb bénéficiaires" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label1" runat="server" Text="%" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label4" runat="server" Text="Nb stagiaires" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label5" runat="server" Text="%" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label6" runat="server" Text="Durée en jours" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label7" runat="server" Text="%" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label8" runat="server" Text="Coûts" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label9" runat="server" Text="%" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label19" runat="server" Text="SEXE" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label22" runat="server" Text="Hommes" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label11" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label12" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label13" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label14" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label15" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label16" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label17" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label18" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label23" runat="server" Text="Femmes" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label24" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label25" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label26" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label27" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label28" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label29" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label30" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label31" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label34" runat="server" Text="CATEGORIE" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label35" runat="server" Text="XXXX" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label36" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label37" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label38" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label39" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label40" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label41" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label42" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label43" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label46" runat="server" Text="STATUT" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label47" runat="server" Text="XXXX" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label48" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label49" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label50" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label51" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label52" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label53" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label54" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label55" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label58" runat="server" Text="NIVEAU 1 ET NIVEAU 2" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label59" runat="server" Text="XXXX" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label60" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label61" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label62" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label63" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label64" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label65" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label66" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label67" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label70" runat="server" Text="DOMAINE" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label71" runat="server" Text="XXXX" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label72" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label73" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label74" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label75" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label76" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label77" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label78" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label79" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>


                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label82" runat="server" Text="THEME" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label83" runat="server" Text="XXXX" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label84" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label85" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label86" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label87" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label88" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label89" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label90" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label91" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow BackColor="#80DFD5" Font-Bold="True">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" Height="30px">
                        <asp:Label ID="Label94" runat="server" Text="TYPE" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" />
                </asp:TableRow>
                <asp:TableRow BackColor="Snow" Font-Bold="false" Font-Italic="true">
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px" >
                        <asp:Label ID="Label95" runat="server" Text="XXXX" Font-Italic="false" Width="80px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label96" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label97" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label98" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label99" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label100" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label101" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label102" runat="server" Text="" Width="130px" BorderStyle="None" BorderWidth="0px" Style="text-align: left; margin-left: 5px" />
                    </asp:TableCell>
                    <asp:TableCell BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        <asp:Label ID="Label103" runat="server" Text="" Width="50px" BorderStyle="None" BorderWidth="0px" Style="text-align: center; margin-left: 5px" />
                    </asp:TableCell>
                </asp:TableRow>--%>

            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
