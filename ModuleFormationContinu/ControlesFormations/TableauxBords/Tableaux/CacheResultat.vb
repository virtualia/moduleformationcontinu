﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Metier.Formation
Imports System.Reflection
Imports Virtualia.Structure.Formation

<Serializable>
Public Class CacheResultat
    Public Property Largeur As Unit = New Unit("95%")
    Public Property PourExport As Boolean = False
    Public Property EstSurStat As Boolean = True
    Public Property Titre As String = ""
    Public Property ProprietesValeurs As PropValeurInfoCollection = New PropValeurInfoCollection()
    Public Property NomFichierExport As String = ""

    Public Function GetResultat(Of T)(Items As IEnumerable(Of T)) As List(Of ResultatInfo)
        Return ProprietesValeurs.GetResultat(Of T)(Items)
    End Function

End Class

Public Interface IResultatInfo
    Property Clef As String
End Interface

Public Class ResultatInfo
    Implements IResultatInfo
    Public Property Clef As String = "" Implements IResultatInfo.Clef
End Class

<Serializable>
Public Class PropValeurInfo
    Inherits DetailExportInfo
    Public Enum TypeOperateur
        Egal
        Superieur
        SuperieurEgal
        Inferieur
        InferieurEgal
        Different
    End Enum

    Public Property ValeurPredicat As String = ""
    Public Property TypeOpe As TypeOperateur = TypeOperateur.Egal
    Public Property Propriete As String = ""
    Private Property PropType As PropertyInfo = Nothing

    Public Property EstEntete As Boolean = True
    Public Property EstPredicat As Boolean = True
    Public Property EstClef As Boolean = True
    Public Property EstTri As Boolean = False
    Public Property EstPourExport As Boolean = False

    Public Sub InitPropType(Of T)()
        PropType = (From p In GetType(T).GetProperties() Where p.Name.ToLower() = Propriete.ToLower() Select p).FirstOrDefault()
    End Sub

    Public Function GetValeur(ByVal item As Object) As String
        If PropType Is Nothing Then
            Throw New Exception("La propriete """ & Propriete & """ n'existe pas")
        End If
        Dim RhFct As Virtualia.Systeme.Fonctions.Generales = New Virtualia.Systeme.Fonctions.Generales
        Dim obj As Object = PropType.GetValue(item, Nothing)
        Dim CRetour As String = ""
        If obj Is Nothing Then
            Return CRetour
        End If
        Select Case TypeVal
            Case TypeValeur.TChaine
                CRetour = obj.ToString()
            Case TypeValeur.TIdentifiant
                CRetour = "" & Convert.ToInt32(obj)
            Case TypeValeur.TEntier
                CRetour = RhFct.MontantEdite(obj.ToString, 0)
            Case TypeValeur.TDate
                CRetour = Convert.ToDateTime(obj).ToShortDateString
            Case TypeValeur.TDouble1
                CRetour = RhFct.MontantEdite(obj.ToString, 1)
            Case TypeValeur.TDouble2
                CRetour = RhFct.MontantEdite(obj.ToString, 2)
        End Select
        Return CRetour
    End Function

    Public Function Evalue(Of T)(Obj As T) As Boolean
        If Not (EstPredicat) Then
            Return True
        End If
        Dim CRetour As Boolean = True
        Select Case TypeVal
            Case TypeValeur.TChaine
                CRetour = EvalueChaine(Of T)(Obj)
            Case TypeValeur.TDate
                CRetour = EvalueDate(Of T)(Obj)
            Case TypeValeur.TEntier
                CRetour = EvalueNumerique(Of T)(Obj, 0)
            Case TypeValeur.TDouble1
                CRetour = EvalueNumerique(Of T)(Obj, 1)
            Case TypeValeur.TDouble2
                CRetour = EvalueNumerique(Of T)(Obj, 2)
        End Select
        Return CRetour
    End Function

    Private Function EvalueChaine(Of T)(Obj As T) As Boolean
        If Not (EstPredicat) Then
            Return True
        End If
        Dim val As String = GetValeur(Obj).Trim().ToLower()
        Dim spred As String = ValeurPredicat.Trim().ToLower()

        Select Case TypeOpe
            Case TypeOperateur.Different
                Return val <> spred
            Case TypeOperateur.Egal
                Return val = spred
            Case TypeOperateur.Inferieur
                Return String.Compare(val, spred) < 0
            Case TypeOperateur.InferieurEgal
                Return String.Compare(val, spred) <= 0
            Case TypeOperateur.Superieur
                Return String.Compare(val, spred) > 0
            Case TypeOperateur.SuperieurEgal
                Return String.Compare(val, spred) >= 0
        End Select
        Return False
    End Function

    Private Function EvalueDate(Of T)(Obj As T) As Boolean
        If EstPredicat = False Then
            Return True
        End If
        Dim RhDates As Virtualia.Systeme.Fonctions.CalculDates = New Virtualia.Systeme.Fonctions.CalculDates
        Dim val As DateTime? = RhDates.DateTypee(GetValeur(Obj))
        Dim spred As DateTime? = RhDates.DateTypee(ValeurPredicat)
        Dim test As String = ""
        If (val.HasValue) Then
            test = test & "1"
        Else
            test = test & "0"
        End If
        If (spred.HasValue) Then
            test = test & "1"
        Else
            test = test & "0"
        End If
        Select Case test
            Case "00"
                Select Case TypeOpe
                    Case TypeOperateur.Different
                        Return False
                    Case TypeOperateur.Egal
                        Return False
                    Case TypeOperateur.Inferieur
                        Return False
                    Case TypeOperateur.InferieurEgal
                        Return True
                    Case TypeOperateur.Superieur
                        Return False
                    Case TypeOperateur.SuperieurEgal
                        Return True
                End Select
            Case "10"
                Select Case TypeOpe
                    Case TypeOperateur.Different
                        Return True
                    Case TypeOperateur.Egal
                        Return False
                    Case TypeOperateur.Inferieur
                        Return False
                    Case TypeOperateur.InferieurEgal
                        Return False
                    Case TypeOperateur.Superieur
                        Return True
                    Case TypeOperateur.SuperieurEgal
                        Return True
                End Select
            Case "01"
                Select Case TypeOpe
                    Case TypeOperateur.Different
                        Return True
                    Case TypeOperateur.Egal
                        Return False
                    Case TypeOperateur.Inferieur
                        Return True
                    Case TypeOperateur.InferieurEgal
                        Return True
                    Case TypeOperateur.Superieur
                        Return False
                    Case TypeOperateur.SuperieurEgal
                        Return False
                End Select
            Case "11"
                Select Case TypeOpe
                    Case TypeOperateur.Different
                        Return val.Value <> spred.Value
                    Case TypeOperateur.Egal
                        Return val.Value = spred.Value
                    Case TypeOperateur.Inferieur
                        Return DateTime.Compare(val.Value, spred.Value) < 0
                    Case TypeOperateur.InferieurEgal
                        Return DateTime.Compare(val.Value, spred.Value) <= 0
                    Case TypeOperateur.Superieur
                        Return DateTime.Compare(val.Value, spred.Value) > 0
                    Case TypeOperateur.SuperieurEgal
                        Return DateTime.Compare(val.Value, spred.Value) >= 0
                End Select
        End Select
        Return False
    End Function

    Private Function EvalueNumerique(Of T)(Obj As T, nbdec As Integer) As Boolean
        If EstPredicat = False Then
            Return True
        End If
        Dim RhFct As Virtualia.Systeme.Fonctions.Generales = New Virtualia.Systeme.Fonctions.Generales
        Dim val As Double = RhFct.ConversionDouble(GetValeur(Obj).Trim(), nbdec)
        Dim spred As Double = RhFct.ConversionDouble(ValeurPredicat.Trim(), nbdec)

        Select Case TypeOpe
            Case TypeOperateur.Different
                Return val <> spred
            Case TypeOperateur.Egal
                Return val = spred
            Case TypeOperateur.Inferieur
                Return val < spred
            Case TypeOperateur.InferieurEgal
                Return val <= spred
            Case TypeOperateur.Superieur
                Return val > spred
            Case TypeOperateur.SuperieurEgal
                Return val >= spred
        End Select
        Return False
    End Function
End Class

<Serializable>
Public Class PropValeurInfoCollection
    Inherits List(Of PropValeurInfo)
    Public Sub New()

    End Sub

    Public Sub New(items As IEnumerable(Of PropValeurInfo))
        MyBase.New(items)
    End Sub

    Public Function GetResultat(Of T)(items As IEnumerable(Of T)) As List(Of ResultatInfo)
        If items Is Nothing Then
            Return Nothing
        End If
        ForEach(Sub(pv)
                    pv.InitPropType(Of T)()
                End Sub)
        Dim LstResultat As List(Of ResultatInfo) = (From s In ( _
                                                        From it In items.ToList() _
                                                        Where Evalue(Of T)(it)
                                                        Order By GetTri(Of T)(it)
                                                        Select GetSelection(Of T)(it)
                                                        ).Distinct()
                                            Select New ResultatInfo() With {.Clef = s}).ToList()
        Return LstResultat
    End Function

    Public Function GetExport(Of T)(items As IEnumerable(Of T), extension As TypeExport) As List(Of String)
        If items Is Nothing Then
            Return Nothing
        End If
        ForEach(Sub(pv)
                    pv.InitPropType(Of T)()
                End Sub)
        Dim LstResultat As List(Of String) = New List(Of String)()
        LstResultat.Add(GetEnteteExport(extension))
        LstResultat.AddRange((From it In items.ToList() Where Evalue(Of T)(it) Order By GetTri(Of T)(it) Select GetExport(Of T)(it, extension)).Distinct().ToList())
        Return LstResultat
    End Function

    Public Function GetEntete() As List(Of PropValeurInfo)
        Return (From it In Me Where it.EstEntete Select it).ToList()
    End Function

    Private Function Evalue(Of T)(obj As T) As Boolean
        Dim lstCol As List(Of PropValeurInfo) = (From it In Me Where it.EstPredicat Select it).ToList()
        For Each p In lstCol
            If Not (p.Evalue(Of T)(obj)) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function GetClef(Of T)(item As T) As String
        Dim CRetour As String = ""
        Dim sep As String = ""
        Dim lstCol As List(Of PropValeurInfo) = (From it In Me Where it.EstClef Select it).ToList()
        For Each p In lstCol
            CRetour &= sep & p.GetValeur(item)
            sep = "|"
        Next
        Return CRetour
    End Function

    Private Function GetSelection(Of T)(item As T) As String
        Dim CRetour As String = ""
        Dim sep As String = ""
        Dim lstCol As List(Of PropValeurInfo) = (From it In Me Where it.EstEntete Select it).ToList()
        For Each p In lstCol
            CRetour &= sep & p.GetValeur(item)
            sep = "|"
        Next
        Return CRetour
    End Function

    Private Function GetExport(Of T)(item As T, extension As TypeExport) As String
        Dim lstCol As List(Of PropValeurInfo) = (From it In Me Where it.EstPourExport Select it).ToList()
        Dim cont As ContenuExport = New ContenuExport(extension)
        For Each p In lstCol
            cont.Add(p.GetValeur(item))
        Next
        Return cont.ToString()
    End Function

    Private Function GetEnteteExport(extension As TypeExport) As String
        Dim cont As ContenuExport = New ContenuExport(extension)
        For Each ent In (From it In Me Where it.EstPourExport Select it.TitreColonne)
            cont.Add(ent)
        Next
        Return cont.ToString()
    End Function

    Private Function GetTri(Of T)(item As T) As String
        Dim CRetour As String = ""
        Dim sep As String = ""

        Dim lstCol As List(Of PropValeurInfo) = (From it In Me Where it.EstTri Select it).ToList()
        For Each p In lstCol
            CRetour &= sep & p.GetValeur(item)
            sep = "|"
        Next
        Return CRetour
    End Function
End Class

