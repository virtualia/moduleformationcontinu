﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.Net.Session
Imports System.Drawing
Imports System.Web.UI.DataVisualization.Charting
Imports System.IO

Public Class CtrlGraphique
    Inherits UserControl
    Implements IControlBase
    Implements ICtrlExport
    Private WsCtrlGestion As CtrlGestionTableauBord
    Private WsCouleur_Benef As Color = VisuHelper.ConvertiCouleur("#4CCBC1") 'BackColor Control Gestion
    Private WsCouleur_Effectif As Color = VisuHelper.ConvertiCouleur("#2A716B") 'BorderColor LABEL
    Private WsCouleur_Cout As Color = VisuHelper.ConvertiCouleur("#08625B") 'BackColor LABEL
    Private WsCouleur_Jour As Color = VisuHelper.ConvertiCouleur("#19968D") 'BackColor Control graphique
    Private WsCouleur_Btn_Normal As Color = VisuHelper.ConvertiCouleur("#124545")
    Private WsCouleur_Btn_Sel As Color = VisuHelper.ConvertiCouleur("#19968D")
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Public Property Repartition_Courante As String
        Get
            If (Not ViewState.KeyExiste("Repartition_Courante")) Then
                ViewState.AjouteValeur("Repartition_Courante", "SEXE")
            End If
            Return DirectCast(ViewState("Repartition_Courante"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("Repartition_Courante", value)
        End Set
    End Property

    Public Property ChargeOnLoad As Boolean
        Get
            If (Not ViewState.KeyExiste("ChargeOnLoad")) Then
                ViewState.AjouteValeur("ChargeOnLoad", False)
            End If
            Return DirectCast(ViewState("ChargeOnLoad"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("ChargeOnLoad", value)
        End Set
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        If Not (ChargeOnLoad) Then
            Return
        End If
        If (WsCtrlGestion Is Nothing OrElse WsCtrlGestion.VCache Is Nothing OrElse WsCtrlGestion.VCache.Statistiques.Count <= 0) Then
            Return
        End If
        ChargeGraphique()
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        ChargeCboNiveau1()
        Repartition_Courante = "SEXE"
        ChargeOnLoad = True
        OnLoad(Nothing)
    End Sub

    Private Sub ChargeGraphique()
        BtnSelec(BtnSEXE, False)
        BtnSelec(BtnCATEGORIE, False)
        BtnSelec(BtnSTATUT, False)
        BtnSelec(BtnDOMAINE, False)
        BtnSelec(BtnTHEME, False)
        BtnSelec(BtnTYPEACTION, False)
        BtnSelec(BtnNIVEAU, False)
        BtnSelec(BtnNIVEAU2, False)

        Select Case (Repartition_Courante)
            Case "SEXE"
                MultiOnglets.ActiveViewIndex = 0
                BtnSelec(BtnSEXE, True)
                ChargeSexe()
            Case "CATEGORIE"
                MultiOnglets.ActiveViewIndex = 1
                BtnSelec(BtnCATEGORIE, True)
                ChargeCategorie()
            Case "STATUT"
                MultiOnglets.ActiveViewIndex = 2
                BtnSelec(BtnSTATUT, True)
                ChargeStatut()
            Case "DOMAINE"
                MultiOnglets.ActiveViewIndex = 3
                BtnDOMAINE.BackColor = WsCouleur_Btn_Sel
                ChargeDomaine()
            Case "THEME"
                MultiOnglets.ActiveViewIndex = 4
                BtnSelec(BtnTHEME, True)
                ChargeTheme()
            Case "TYPEACTION"
                MultiOnglets.ActiveViewIndex = 5
                BtnSelec(BtnTYPEACTION, True)
                ChargeTypeAction()
            Case "NIVEAU"
                MultiOnglets.ActiveViewIndex = 6
                BtnSelec(BtnNIVEAU, True)
                ChargeNiveau1()
            Case "NIVEAU2"
                MultiOnglets.ActiveViewIndex = 7
                BtnSelec(BtnNIVEAU2, True)
                ChargeNiveau2()
        End Select
    End Sub

    Private Sub ChargeSexe()
        Cell_Sexe_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_Sexe_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        Sexe_NB.Series(0)("PieDrawingStyle") = "Concave"
        Sexe_Cout.Series(0)("PieDrawingStyle") = "Concave"
        Sexe_Jour.Series(0)("PieDrawingStyle") = "Concave"
        Sexe_Effectif.Series(0)("PieDrawingStyle") = "Concave"

        Sexe_NB.Series(0).ChartType = SeriesChartType.Doughnut
        Sexe_Cout.Series(0).ChartType = SeriesChartType.Doughnut
        Sexe_Jour.Series(0).ChartType = SeriesChartType.Doughnut
        Sexe_Effectif.Series(0).ChartType = SeriesChartType.Doughnut

        Dim lstdtps_NB As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_Cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_Jour As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_eff As List(Of DataPoint) = New List(Of DataPoint)()

        Dim func_pred_homme As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                                Return sfi.Sexe.ToLower() = "masculin"
                                                                            End Function

        GetDataPoint_Benef("Hommes", func_pred_homme, lstdtps_NB)
        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            GetDataPoint_Cout("Hommes", func_pred_homme, lstdtps_Cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            GetDataPoint_Jour("Hommes", func_pred_homme, lstdtps_Jour)
        End If
        Dim func_pred_femme As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                                Return sfi.Sexe.ToLower() <> "masculin"
                                                                            End Function

        GetDataPoint_Benef("Femmes", func_pred_femme, lstdtps_NB)
        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            GetDataPoint_Cout("Femmes", func_pred_femme, lstdtps_Cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            GetDataPoint_Jour("Femmes", func_pred_femme, lstdtps_Jour)
        End If

        Dim func_pred_eff_homme As Func(Of StatistiqueEffectifInfo, Boolean) = Function(sfi)
                                                                                   Return sfi.Sexe.ToLower() = "masculin"
                                                                               End Function

        GetDataPoint_Effectif("Hommes", func_pred_eff_homme, lstdtps_eff)

        Dim func_pred_eff_femme As Func(Of StatistiqueEffectifInfo, Boolean) = Function(sfi)
                                                                                   Return sfi.Sexe.ToLower() <> "masculin"
                                                                               End Function
        GetDataPoint_Effectif("Femmes", func_pred_eff_femme, lstdtps_eff)


        RempliSerie(Sexe_NB, 0, lstdtps_NB)
        RempliSerie(Sexe_Effectif, 0, lstdtps_eff)

        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            RempliSerie(Sexe_Cout, 0, lstdtps_Cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            RempliSerie(Sexe_Jour, 0, lstdtps_Jour)
        End If
    End Sub

    Private Sub ChargeCategorie()
        Cell_Categorie_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_Categorie_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        Categorie_NB.Series(0)("DrawingStyle") = "Cylinder"
        Categorie_NB.Series(1)("DrawingStyle") = "Cylinder"
        Categorie_Cout.Series(0)("DrawingStyle") = "Cylinder"
        Categorie_Jour.Series(0)("DrawingStyle") = "Cylinder"

        Categorie_NB.Series(0).Color = WsCouleur_Effectif
        Categorie_NB.Series(1).Color = WsCouleur_Benef
        Categorie_Cout.Series(0).Color = WsCouleur_Cout
        Categorie_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_NB As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_Cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_Jour As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_eff As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee Order By sfi.Categorie Select sfi.Categorie).Distinct().ToList()

        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""

        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Categorie = valeur_pred
                                                                      End Function
        Dim func_pred_effectif As Func(Of StatistiqueEffectifInfo, Boolean) = Function(sfi)
                                                                                  Return sfi.Categorie = valeur_pred
                                                                              End Function
        lst.ForEach(Sub(v)

                        valeur_pred = v

                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If

                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_NB, False)
                        GetDataPoint_Effectif("Effectif", lblaxe, func_pred_effectif, lstdtps_eff, False)

                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_Cout)
                        End If

                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Jours) Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_Jour)
                        End If

                    End Sub)

        RempliSerie(Categorie_NB, 0, lstdtps_eff)
        RempliSerie(Categorie_NB, 1, lstdtps_NB)

        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            RempliSerie(Categorie_Cout, 0, lstdtps_Cout)
        End If

        If (WsCtrlGestion.VCache.Statistiques.A_Des_Jours) Then
            RempliSerie(Categorie_Jour, 0, lstdtps_Jour)
        End If
    End Sub

    Private Sub ChargeStatut()
        Cell_Statut_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_Statut_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        Statut_NB.Series(0)("DrawingStyle") = "Cylinder"
        Statut_NB.Series(1)("DrawingStyle") = "Cylinder"
        Statut_Cout.Series(0)("DrawingStyle") = "Cylinder"
        Statut_Jour.Series(0)("DrawingStyle") = "Cylinder"

        Statut_NB.Series(0).Color = WsCouleur_Effectif
        Statut_NB.Series(1).Color = WsCouleur_Benef
        Statut_Cout.Series(0).Color = WsCouleur_Cout
        Statut_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_NB As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_Cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_Jour As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_eff As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee Order By sfi.Statut Select sfi.Statut).Distinct().ToList()

        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""

        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Statut = valeur_pred
                                                                      End Function
        Dim func_pred_effectif As Func(Of StatistiqueEffectifInfo, Boolean) = Function(sfi)
                                                                                  Return sfi.Statut = valeur_pred
                                                                              End Function

        lst.ForEach(Sub(v)
                        valeur_pred = v
                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If
                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_NB, False)
                        GetDataPoint_Effectif("Effectif", lblaxe, func_pred_effectif, lstdtps_eff, False)

                        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_Cout)
                        End If
                        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_Jour)
                        End If

                    End Sub)

        RempliSerie(Statut_NB, 0, lstdtps_eff)
        RempliSerie(Statut_NB, 1, lstdtps_NB)

        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            RempliSerie(Statut_Cout, 0, lstdtps_Cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            RempliSerie(Statut_Jour, 0, lstdtps_Jour)
        End If
    End Sub

    Private Sub ChargeDomaine()
        Cell_Domaine_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_Domaine_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        Domaine_NB.Series(0)("DrawingStyle") = "Cylinder"
        Domaine_Cout.Series(0)("DrawingStyle") = "Cylinder"
        Domaine_Jour.Series(0)("DrawingStyle") = "Cylinder"

        Domaine_NB.Series(0).Color = WsCouleur_Benef
        Domaine_Cout.Series(0).Color = WsCouleur_Cout
        Domaine_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_benef As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_jour As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee _
                Order By sfi.Domaine Select sfi.Domaine).Distinct().ToList()
        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""
        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Domaine = valeur_pred
                                                                      End Function
        lst.ForEach(Sub(v)
                        valeur_pred = v
                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If
                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_benef)
                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_cout)
                        End If

                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Jours) Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_jour)
                        End If
                    End Sub)

        RempliSerie(Domaine_NB, 0, lstdtps_benef)

        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            RempliSerie(Domaine_Cout, 0, lstdtps_cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            RempliSerie(Domaine_Jour, 0, lstdtps_jour)
        End If
    End Sub

    Private Sub ChargeTheme()
        Cell_Theme_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_Theme_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        Theme_NB.Series(0)("DrawingStyle") = "Cylinder"
        Theme_Cout.Series(0)("DrawingStyle") = "Cylinder"
        Theme_Jour.Series(0)("DrawingStyle") = "Cylinder"

        Theme_NB.Series(0).Color = WsCouleur_Benef
        Theme_Cout.Series(0).Color = WsCouleur_Cout
        Theme_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_benef As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_jour As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee _
                Order By sfi.Theme Select sfi.Theme).Distinct().ToList()

        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""
        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Theme = valeur_pred
                                                                      End Function
        lst.ForEach(Sub(v)
                        valeur_pred = v
                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If
                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_benef)
                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_cout)
                        End If
                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Jours) Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_jour)
                        End If
                    End Sub)

        RempliSerie(Theme_NB, 0, lstdtps_benef)

        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            RempliSerie(Theme_Cout, 0, lstdtps_cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            RempliSerie(Theme_Jour, 0, lstdtps_jour)
        End If
    End Sub

    Private Sub ChargeTypeAction()
        Cell_TypeAction_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_TypeAction_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        TypeAction_NB.Series(0)("DrawingStyle") = "Cylinder"
        TypeAction_Cout.Series(0)("DrawingStyle") = "Cylinder"
        TypeAction_Jour.Series(0)("DrawingStyle") = "Cylinder"

        TypeAction_NB.Series(0).Color = WsCouleur_Benef
        TypeAction_Cout.Series(0).Color = WsCouleur_Cout
        TypeAction_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_benef As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_jour As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee _
                Order By sfi.Type_Action Select sfi.Type_Action).Distinct().ToList()
        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""
        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Type_Action = valeur_pred
                                                                      End Function
        lst.ForEach(Sub(v)
                        valeur_pred = v
                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If

                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_benef)

                        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_cout)
                        End If
                        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_jour)
                        End If
                    End Sub)

        RempliSerie(TypeAction_NB, 0, lstdtps_benef)

        If WsCtrlGestion.VCache.Statistiques.A_Des_Couts = True Then
            RempliSerie(TypeAction_Cout, 0, lstdtps_cout)
        End If
        If WsCtrlGestion.VCache.Statistiques.A_Des_Jours = True Then
            RempliSerie(TypeAction_Jour, 0, lstdtps_jour)
        End If
    End Sub

    Private Sub ChargeNiveau1()
        Cell_Niveau1_Cout.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Couts
        Cell_Niveau1_Jour.Visible = WsCtrlGestion.VCache.Statistiques.A_Des_Jours

        Niveau1_NB.Series(0)("DrawingStyle") = "Cylinder"
        Niveau1_NB.Series(1)("DrawingStyle") = "Cylinder"
        Niveau1_Cout.Series(0)("DrawingStyle") = "Cylinder"
        Niveau1_Jour.Series(0)("DrawingStyle") = "Cylinder"

        Niveau1_NB.Series(0).Color = WsCouleur_Effectif
        Niveau1_NB.Series(1).Color = WsCouleur_Benef
        Niveau1_Cout.Series(0).Color = WsCouleur_Cout
        Niveau1_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_benef As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_jour As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_eff As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee _
                Order By sfi.Niveau1 Select sfi.Niveau1).Distinct().ToList()
        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""
        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Niveau1 = valeur_pred
                                                                      End Function
        Dim func_pred_effectif As Func(Of StatistiqueEffectifInfo, Boolean) = Function(sfi)
                                                                                  Return sfi.Niveau1 = valeur_pred
                                                                              End Function

        Dim avectooltip As Boolean = (lst.Count >= 15)

        lst.ForEach(Sub(v)
                        valeur_pred = v
                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If

                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_benef, False)
                        GetDataPoint_Effectif("Effectif", lblaxe, func_pred_effectif, lstdtps_eff, False)

                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_cout)
                        End If

                        If (WsCtrlGestion.VCache.Statistiques.A_Des_Jours) Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_jour)
                        End If

                    End Sub)

        RempliSerie(Niveau1_NB, 0, lstdtps_eff)
        RempliSerie(Niveau1_NB, 1, lstdtps_benef)

        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            RempliSerie(Niveau1_Cout, 0, lstdtps_cout)
        End If

        If (WsCtrlGestion.VCache.Statistiques.A_Des_Jours) Then
            RempliSerie(Niveau1_Jour, 0, lstdtps_jour)
        End If

    End Sub

    Private Sub ChargeNiveau2()
        TB_NIVEAU2.Visible = False
        If (CboNiveau1.SelectedItem Is Nothing) OrElse (CboNiveau1.SelectedItem.Value = "0") Then
            Return
        End If

        TB_NIVEAU2.Visible = True

        Dim adescout As Boolean = ((From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee And sfi.Niveau1 = CboNiveau1.SelectedItem.Value Select sfi.Couts).Sum() <> 0)
        Dim adesjours As Boolean = ((From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee And sfi.Niveau1 = CboNiveau1.SelectedItem.Value Select sfi.Duree_Heure).Sum() <> 0)

        Cell_Niveau2_Cout.Visible = adescout
        Cell_Niveau2_Jour.Visible = adesjours

        Niveau2_NB.Series(0)("DrawingStyle") = "Cylinder"
        Niveau2_NB.Series(1)("DrawingStyle") = "Cylinder"
        Niveau2_Cout.Series(0)("DrawingStyle") = "Cylinder"
        Niveau2_Jour.Series(0)("DrawingStyle") = "Cylinder"

        Niveau2_NB.Series(0).Color = WsCouleur_Effectif
        Niveau2_NB.Series(1).Color = WsCouleur_Benef
        Niveau2_Cout.Series(0).Color = WsCouleur_Cout
        Niveau2_Jour.Series(0).Color = WsCouleur_Jour

        Dim lstdtps_benef As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_cout As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_jour As List(Of DataPoint) = New List(Of DataPoint)()
        Dim lstdtps_eff As List(Of DataPoint) = New List(Of DataPoint)()

        Dim lst As List(Of String) = (From sfi In WsCtrlGestion.VCache.Statistiques Where sfi.EstFormationValidee And sfi.Niveau1 = CboNiveau1.SelectedItem.Value And sfi.Niveau2 <> "" Order By sfi.Niveau2 Select sfi.Niveau2).Distinct().ToList()

        Dim valeur_pred As String = ""
        Dim lblaxe As String = ""

        Dim func_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(sfi)
                                                                          Return sfi.Niveau1 = CboNiveau1.SelectedItem.Value AndAlso sfi.Niveau2 = valeur_pred
                                                                      End Function
        Dim func_pred_effectif As Func(Of StatistiqueEffectifInfo, Boolean) = Function(sfi)
                                                                                  Return sfi.Niveau1 = CboNiveau1.SelectedItem.Value AndAlso sfi.Niveau2 = valeur_pred
                                                                              End Function
        lst.ForEach(Sub(v)

                        valeur_pred = v

                        lblaxe = v.Trim()
                        If (v.Trim() = "") Then
                            lblaxe = "Non renseigné"
                        End If

                        GetDataPoint_Benef("Bénéficiaires", lblaxe, func_pred, lstdtps_benef, False)
                        GetDataPoint_Effectif("Effectif", lblaxe, func_pred_effectif, lstdtps_eff, False)

                        If (adescout) Then
                            GetDataPoint_Cout("Coûts", lblaxe, func_pred, lstdtps_cout)
                        End If

                        If (adesjours) Then
                            GetDataPoint_Jour("Jours", lblaxe, func_pred, lstdtps_jour)
                        End If

                    End Sub)

        RempliSerie(Niveau2_NB, 0, lstdtps_eff)
        RempliSerie(Niveau2_NB, 1, lstdtps_benef)

        If (adescout) Then
            RempliSerie(Niveau2_Cout, 0, lstdtps_cout)
        End If

        If (adesjours) Then
            RempliSerie(Niveau2_Jour, 0, lstdtps_jour)
        End If
    End Sub

    Private Sub ChargeCboNiveau1()
        CboNiveau1.Items.Clear()
        Dim query = From sfi In WsCtrlGestion.VCache.Statistiques _
                                      Where sfi.EstFormationValidee And sfi.Niveau1 <> "" _
                                      Order By sfi.Niveau1 _
                                      Group sfi.Niveau2 By Key = sfi.Niveau1 Into Group _
                                      Select Niv1 = Key, GRP = Group

        Dim lstit As List(Of ListItem) = New List(Of ListItem)()

        For Each el In query
            If (el.GRP.Distinct().Count() = 1 AndAlso el.GRP.Distinct().First() = "") Then
                Continue For
            End If
            lstit.Add(New ListItem(el.Niv1, el.Niv1))
        Next

        If lstit.Count = 0 Then
            BtnNIVEAU2.Visible = False
            Return
        End If

        BtnNIVEAU2.Visible = True

        If (lstit.Count = 1) Then
            CboNiveau1.Items.Add(lstit(0))
            Return
        End If

        CboNiveau1.Items.Add(New ListItem("Sélectionner...", "0") With {.Selected = True})

        lstit.ForEach(Sub(it)
                          CboNiveau1.Items.Add(it)
                      End Sub)
    End Sub

    Private Sub GetDataPoint_Benef(legend As String, func_pred As Func(Of StatistiqueFormationInfo, Boolean), lstdatas As List(Of DataPoint))
        GetDataPoint_Benef(legend, "", func_pred, lstdatas)
    End Sub

    Private Sub GetDataPoint_Benef(ByVal Legende As String, ByVal AxeVal As String, ByVal Func_pred As Func(Of StatistiqueFormationInfo, Boolean), _
                                   ByVal LstDatas As List(Of DataPoint), Optional SiIgnore_0 As Boolean = True)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim nb As Integer = WsCtrlGestion.VCache.Statistiques.GetValeurBenef(Func_pred)
        Dim db As Double = Convert.ToDouble(nb)
        Dim lb As String = WebFct.ViRhFonction.MontantEdite(db, 0)

        If SiIgnore_0 = True AndAlso nb = 0 Then
            Exit Sub
        End If

        Dim dtp As DataPoint = New DataPoint()
        dtp.LegendText = Legende
        dtp.Label = lb
        dtp.YValues = New Double() {nb}

        dtp.Font = New Font("Trebuchet MS", 9, FontStyle.Bold)

        If AxeVal.Trim() <> "" Then
            dtp.AxisLabel = AxeVal
            dtp.ToolTip = AxeVal & vbCrLf & lb & " bénéficiaire"
            If (nb > 1) Then
                dtp.ToolTip = dtp.ToolTip & "s"
            End If
        End If
        lstdatas.Add(dtp)
    End Sub

    Private Sub GetDataPoint_Stagiaire(legend As String, func_pred As Func(Of StatistiqueFormationInfo, Boolean), lstdatas As List(Of DataPoint))
        GetDataPoint_Stagiaire(legend, "", func_pred, lstdatas)
    End Sub

    Private Sub GetDataPoint_Stagiaire(legend As String, AxeVal As String, func_pred As Func(Of StatistiqueFormationInfo, Boolean), lstdatas As List(Of DataPoint), Optional ignore_0 As Boolean = True)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim nb As Integer = WsCtrlGestion.VCache.Statistiques.GetValeurStagiaire(func_pred)
        Dim db As Double = Convert.ToDouble(nb)
        Dim lb As String = WebFct.ViRhFonction.MontantEdite(db, 0)

        If ignore_0 AndAlso nb = 0 Then
            Return
        End If

        Dim dtp As DataPoint = New DataPoint()
        dtp.LegendText = legend
        dtp.Label = lb
        dtp.YValues = New Double() {nb}

        dtp.Font = New Font("Trebuchet MS", 10, FontStyle.Bold)

        If (AxeVal.Trim() <> "") Then
            dtp.AxisLabel = AxeVal
            dtp.ToolTip = AxeVal & vbCrLf & lb & " stagiaire"
            If (nb > 1) Then
                dtp.ToolTip = dtp.ToolTip & "s"
            End If
        End If

        lstdatas.Add(dtp)

    End Sub

    Private Sub GetDataPoint_Jour(legend As String, func_pred As Func(Of StatistiqueFormationInfo, Boolean), lstdatas As List(Of DataPoint))
        GetDataPoint_Jour(legend, "", func_pred, lstdatas)
    End Sub

    Private Sub GetDataPoint_Jour(ByVal Legende As String, ByVal AxeVal As String, ByVal Func_Pred As Func(Of StatistiqueFormationInfo, Boolean), _
                                  ByVal LstDatas As List(Of DataPoint), Optional SiIgnore_0 As Boolean = True)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim nb As Double = WsCtrlGestion.VCache.Statistiques.GetValeurDuree_Jour(func_pred)
        Dim lb As String = WebFct.ViRhFonction.MontantEdite(nb, 2)

        If SiIgnore_0 = True AndAlso nb = 0 Then
            Return
        End If

        Dim dtp As DataPoint = New DataPoint()
        dtp.LegendText = Legende
        dtp.Label = lb
        dtp.YValues = New Double() {nb}

        dtp.Font = New Font("Trebuchet MS", 9, FontStyle.Bold)

        If (AxeVal.Trim() <> "") Then
            dtp.AxisLabel = AxeVal
            dtp.ToolTip = AxeVal & vbCrLf & lb & " jour"
            If (nb > 1) Then
                dtp.ToolTip = dtp.ToolTip & "s"
            End If
        End If
        lstdatas.Add(dtp)
    End Sub

    Private Sub GetDataPoint_Cout(legend As String, func_pred As Func(Of StatistiqueFormationInfo, Boolean), lstdatas As List(Of DataPoint))
        GetDataPoint_Cout(legend, "", func_pred, lstdatas)
    End Sub

    Private Sub GetDataPoint_Cout(legend As String, AxeVal As String, func_pred As Func(Of StatistiqueFormationInfo, Boolean), lstdatas As List(Of DataPoint), Optional ignore_0 As Boolean = True)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim nb As Double = WsCtrlGestion.VCache.Statistiques.GetValeurCout(func_pred)
        Dim lb As String = WebFct.ViRhFonction.MontantEdite(nb, 2)

        If (ignore_0 AndAlso nb = 0) Then
            Return
        End If

        Dim dtp As DataPoint = New DataPoint()
        dtp.LegendText = legend
        dtp.Label = lb
        dtp.YValues = New Double() {nb}

        dtp.Font = New Font("Trebuchet MS", 9, FontStyle.Bold)

        If (AxeVal.Trim() <> "") Then
            dtp.AxisLabel = AxeVal
            dtp.ToolTip = AxeVal & vbCrLf & lb
        End If
        lstdatas.Add(dtp)
    End Sub

    Private Sub GetDataPoint_Effectif(legend As String, func_pred As Func(Of StatistiqueEffectifInfo, Boolean), lstdatas As List(Of DataPoint))
        GetDataPoint_Effectif(legend, "", func_pred, lstdatas)
    End Sub

    Private Sub GetDataPoint_Effectif(legend As String, AxeVal As String, func_pred As Func(Of StatistiqueEffectifInfo, Boolean), lstdatas As List(Of DataPoint), Optional ignore_0 As Boolean = True)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim nb As Integer = WsCtrlGestion.VCache.Effectifs.GetValeurPred(func_pred)
        Dim db As Double = Convert.ToDouble(nb)
        Dim lb As String = WebFct.ViRhFonction.MontantEdite(db, 0)

        If ignore_0 AndAlso nb = 0 Then
            Return
        End If

        Dim dtp As DataPoint = New DataPoint()
        dtp.LegendText = legend
        dtp.Label = lb
        dtp.YValues = New Double() {nb}

        dtp.Font = New Font("Trebuchet MS", 9, FontStyle.Bold)

        If (AxeVal.Trim() <> "") Then
            dtp.AxisLabel = AxeVal
            dtp.ToolTip = AxeVal & vbCrLf & lb & " collaborateur"
            If (nb > 1) Then
                dtp.ToolTip = dtp.ToolTip & "s"
            End If
        End If
        lstdatas.Add(dtp)
    End Sub

    Private Sub RempliSerie(grp As Chart, numserie As Integer, dtps As List(Of DataPoint))
        Dim quetooltip As Boolean = (dtps.Count >= 15)

        grp.Series(numserie).Points.Clear()
        dtps.ForEach(Sub(dtp)
                         If (quetooltip) Then
                             dtp.Label = ""
                         Else
                             dtp.ToolTip = ""
                         End If
                         grp.Series(numserie).Points.Add(dtp)
                     End Sub)
    End Sub

    Private Sub BtnSelec(btn As Button, selectionne As Boolean)
        If (selectionne) Then
            btn.BorderStyle = BorderStyle.Inset
            btn.BackColor = WsCouleur_Btn_Sel
            Exit Sub
        End If
        btn.BorderStyle = BorderStyle.None
        btn.BackColor = WsCouleur_Btn_Normal
    End Sub

    Public Function GetExport() As ExportPdfInfo Implements ICtrlExport.GetExport
        Dim sw As StringWriter = New StringWriter()
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)

        Dim CRetour As ExportPdfInfo = New ExportPdfInfo()
        CRetour.NomFichier = "Graphique_" & Repartition_Courante

        Dim tb As Table = Nothing

        Try
            Select Case (Repartition_Courante)
                Case "SEXE"
                    tb = TB_SEXE
                Case "CATEGORIE"
                    tb = TB_CATEGORIE
                Case "STATUT"
                    tb = TB_STATUT
                Case "DOMAINE"
                    tb = TB_DOMAINE
                Case "THEME"
                    tb = TB_THEME
                Case "TYPEACTION"
                    tb = TB_TYPEACTION
                Case "NIVEAU"
                    tb = TB_NIVEAU
                Case "NIVEAU2"
                    tb = TB_NIVEAU2
            End Select
            tb.RenderControl(htw)
            CRetour.ContenuHtml = htw.InnerWriter.ToString()
        Catch ex As Exception
            CRetour.EstOk = False
        End Try
        Return CRetour
    End Function

    Public Function GetImagesGraphique() As List(Of ImageExportInfo)
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()
        Dim og As LDObjetGlobal = DirectCast((DirectCast(Page, FrmTableauxBord)).VObjetGlobal, LDObjetGlobal)
        Dim Cheminphysique As String = og.CheminIMG
        Dim CheminUrl As String = og.SiteIMG
        If (Not CheminUrl.EndsWith("/")) Then
            CheminUrl = CheminUrl & "/"
        End If
        Cheminphysique &= Session.SessionID
        If Cheminphysique = "" Then
            Return LstResultat
        End If
        Try
            My.Computer.FileSystem.DeleteDirectory(Cheminphysique, FileIO.DeleteDirectoryOption.DeleteAllContents)
        Catch ex As Exception
            Exit Try
        End Try
        Try
            My.Computer.FileSystem.CreateDirectory(Cheminphysique)
        Catch ex As Exception
            Return LstResultat
        End Try
        Cheminphysique &= "\"
        CheminUrl &= Session.SessionID & "/"

        LstResultat.AddRange(CreeImagesSexe(Cheminphysique, CheminUrl))
        LstResultat.AddRange(CreeImagesCategorie(Cheminphysique, CheminUrl))
        LstResultat.AddRange(CreeImagesStatut(Cheminphysique, CheminUrl))
        LstResultat.AddRange(CreeImagesDomaine(Cheminphysique, CheminUrl))
        LstResultat.AddRange(CreeImagesTheme(Cheminphysique, CheminUrl))
        LstResultat.AddRange(CreeImagesTypeAction(Cheminphysique, CheminUrl))
        LstResultat.AddRange(CreeImagesNiveau1(Cheminphysique, CheminUrl))

        Return LstResultat
    End Function

    Private Function CreeImagesSexe(ByVal Cheminphysique As String, ByVal CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeSexe()
        iei = New ImageExportInfo() With {.Groupe = "SEXE", .TypeCalcul = "NB"}
        Sexe_NB.SaveImage(Cheminphysique & "PicSexeNB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicSexeNB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "SEXE", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            Sexe_Cout.SaveImage(Cheminphysique & "PicSexeCout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicSexeCout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "SEXE", .TypeCalcul = "JOUR"}
        Sexe_Jour.SaveImage(Cheminphysique & "PicSexeJour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicSexeJour.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "SEXE", .TypeCalcul = "EFFECTIF"}
        Sexe_Effectif.SaveImage(Cheminphysique & "PicSexeEffectif.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicSexeEffectif.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Private Function CreeImagesCategorie(Cheminphysique As String, CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeCategorie()

        iei = New ImageExportInfo() With {.Groupe = "CATEGORIE", .TypeCalcul = "NB"}
        Categorie_Cout.SaveImage(Cheminphysique & "PicCategorieNB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicCategorieNB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "CATEGORIE", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            Categorie_Cout.SaveImage(Cheminphysique & "PicCategorieCout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicCategorieCout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "CATEGORIE", .TypeCalcul = "JOUR"}
        Categorie_Jour.SaveImage(Cheminphysique & "PicCategorieJour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicCategorieJour.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Private Function CreeImagesStatut(Cheminphysique As String, CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeStatut()

        iei = New ImageExportInfo() With {.Groupe = "STATUT", .TypeCalcul = "NB"}
        Statut_Cout.SaveImage(Cheminphysique & "PicStatutNB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicStatutNB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "STATUT", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            Statut_Cout.SaveImage(Cheminphysique & "PicStatutCout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicStatutCout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "STATUT", .TypeCalcul = "JOUR"}
        Statut_Jour.SaveImage(Cheminphysique & "PicStatutJour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicStatutJour.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Private Function CreeImagesDomaine(Cheminphysique As String, CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeDomaine()

        iei = New ImageExportInfo() With {.Groupe = "DOMAINE", .TypeCalcul = "NB"}
        Domaine_Cout.SaveImage(Cheminphysique & "PicDomaineNB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicDomaineNB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "DOMAINE", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            Domaine_Cout.SaveImage(Cheminphysique & "PicDomaineCout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicDomaineCout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "DOMAINE", .TypeCalcul = "JOUR"}
        Domaine_Jour.SaveImage(Cheminphysique & "PicDomaineJour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicDomaineJour.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Private Function CreeImagesTheme(Cheminphysique As String, CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeTheme()

        iei = New ImageExportInfo() With {.Groupe = "THEME", .TypeCalcul = "NB"}
        Theme_Cout.SaveImage(Cheminphysique & "PicThemeNB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicThemeNB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "THEME", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            Theme_Cout.SaveImage(Cheminphysique & "PicThemeCout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicThemeCout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "THEME", .TypeCalcul = "JOUR"}
        Theme_Jour.SaveImage(Cheminphysique & "PicThemeJour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicThemeJour.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Private Function CreeImagesTypeAction(Cheminphysique As String, CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeTypeAction()

        iei = New ImageExportInfo() With {.Groupe = "TYPEACTION", .TypeCalcul = "NB"}
        TypeAction_Cout.SaveImage(Cheminphysique & "PicTypeActionNB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicTypeActionNB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "TYPEACTION", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            TypeAction_Cout.SaveImage(Cheminphysique & "PicTypeActionCout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicTypeActionCout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "TYPEACTION", .TypeCalcul = "JOUR"}
        TypeAction_Jour.SaveImage(Cheminphysique & "PicTypeActionJour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicTypeActionJour.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Private Function CreeImagesNiveau1(Cheminphysique As String, CheminUrl As String) As List(Of ImageExportInfo)
        Dim iei As ImageExportInfo
        Dim LstResultat As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()

        ChargeNiveau1()

        iei = New ImageExportInfo() With {.Groupe = "NIVEAU1", .TypeCalcul = "NB"}
        Niveau1_Cout.SaveImage(Cheminphysique & "PicNiveau1NB.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicNiveau1NB.jpg"
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "NIVEAU1", .TypeCalcul = "COUT"}
        If (WsCtrlGestion.VCache.Statistiques.A_Des_Couts) Then
            Niveau1_Cout.SaveImage(Cheminphysique & "PicNiveau1Cout.jpg", ChartImageFormat.Jpeg)
            iei.UrlImg = CheminUrl & "PicNiveau1Cout.jpg"
        End If
        LstResultat.Add(iei)

        iei = New ImageExportInfo() With {.Groupe = "NIVEAU1", .TypeCalcul = "JOUR"}
        Niveau1_Jour.SaveImage(Cheminphysique & "PicNiveau1Jour.jpg", ChartImageFormat.Jpeg)
        iei.UrlImg = CheminUrl & "PicNiveau1Jour.jpg"
        LstResultat.Add(iei)

        Return LstResultat
    End Function

    Protected Sub BtnVue_Click(sender As Object, e As EventArgs)
        If (Repartition_Courante = DirectCast(sender, Control).ID.Replace("Btn", "")) Then
            Return
        End If
        Repartition_Courante = DirectCast(sender, Control).ID.Replace("Btn", "")
        OnLoad(Nothing)
    End Sub
End Class