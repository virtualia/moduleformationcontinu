﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Structure.Formation
Public Class CtrlVueTableauBord
    Inherits UserControl
    Private Const NBItemParPage As Integer = 200
    Private Const C_COURANT As Integer = 0
    Private Const C_STAT As Integer = 1
    Private Const C_EFFECTIF As Integer = 2
    Private WsCtrlGestion As CtrlGestionTableauBord

    Private ReadOnly Property FenetreMere As FrmTableauxBord
        Get
            Return DirectCast(Page, FrmTableauxBord)
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
            CtrlChiffresBruts.CtrlGestion = WsCtrlGestion
            CtrlTableauSynthese.CtrlGestion = WsCtrlGestion
            CtrlGraph.CtrlGestion = WsCtrlGestion
            CtrlApercu.CtrlGestion = WsCtrlGestion
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        CboAction.SelectedIndex = 0
        CtrlApercu.CtrlGestionVue = Me
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        CboAction.SelectedIndex = 0
    End Sub

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
    End Sub

    Public Sub Charge()
        TbVueAction.Visible = True
        RowDonneeFiltre.Visible = True

        EtiAnnee.Text = "Année : " & WsCtrlGestion.VCache.Annee
        If WsCtrlGestion.VCache.Plan = "" Then
            EtiPlan.Text = "Plan de formation : Tous"
        Else
            EtiPlan.Text = "Plan de formation : " & WsCtrlGestion.VCache.Plan
        End If
        If WsCtrlGestion.VCache.Etablissement = "" Then
            EtiEtab.Text = "Etablissement : Tous"
        Else
            EtiEtab.Text = "Etablissement : " & WsCtrlGestion.VCache.Etablissement
        End If
        cboVuPar.SelectedIndex = 0
        ChargeComboAction()

        CtrlChiffresBruts.Charge(Nothing)
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Private Async Sub GereExport(ByVal TypeExp As TypeExport, ByVal TypeDonnee As Integer)
        Dim LstValeurs As List(Of String) = New List(Of String)()
        Dim NomFichier As String = ""

        Select Case TypeDonnee
            Case C_COURANT 'Vue courante
                Select Case MultiOnglets.ActiveViewIndex
                    Case 0
                        Await System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                               LstValeurs = CtrlChiffresBruts.ToValeurs(TypeExp)
                                                                           End Sub)
                        NomFichier = "ChiffresGlobaux"
                    Case 1
                        Await System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                               LstValeurs = CtrlTableauSynthese.ToValeurs(TypeExp)
                                                                           End Sub)
                        NomFichier = "ChiffreSynthese"
                End Select
            Case C_STAT 'Donnees
                Await System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                       LstValeurs = WsCtrlGestion.VCache.Statistiques.ToValeurs(TypeExp)
                                                                   End Sub)
                NomFichier = "DonneeStat"
            Case C_EFFECTIF
                Await System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                       LstValeurs = WsCtrlGestion.VCache.Effectifs.ToValeurs(TypeExp)
                                                                   End Sub)
                NomFichier = "DonneeEffectif"
        End Select

        WsCtrlGestion.GereExport(NomFichier, ContenuExport.GetExtensionFichier(TypeExp), LstValeurs)

    End Sub

    Private Sub ConstitueDossierExportPdf()
        Dim InfoExport As ExportDossierInfo = New ExportDossierInfo()
        InfoExport.Annee = "" & WsCtrlGestion.VCache.Annee
        If WsCtrlGestion.VCache.Plan = "" Then
            InfoExport.PlanFormation = "Tous"
        Else
            InfoExport.PlanFormation = WsCtrlGestion.VCache.Plan
        End If
        If WsCtrlGestion.VCache.Etablissement = "" Then
            InfoExport.Etablissement = "Tous"
        Else
            InfoExport.Etablissement = WsCtrlGestion.VCache.Etablissement
        End If

        Dim Liste_Taches As List(Of System.Threading.Tasks.Task) = New List(Of System.Threading.Tasks.Task)()

        Liste_Taches.Add(System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                          InfoExport.SHtmlTableaux = CtrlTableauSynthese.GetExportPagine()
                                                                      End Sub))

        Liste_Taches.Add(System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                                          InfoExport.ImagesGraphiques = CtrlGraph.GetImagesGraphique()
                                                                      End Sub))

        System.Threading.Tasks.Task.WaitAll(Liste_Taches.ToArray())

        CtrlApercu.Charge(InfoExport)

        TbVueAction.Visible = False
        RowDonneeFiltre.Visible = False

        MultiOnglets.ActiveViewIndex = 3

    End Sub

    Public Sub GereRetourApercu()
        TbVueAction.Visible = True
        RowDonneeFiltre.Visible = True
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Public Sub ReinitComboAction()
        CboAction.SelectedIndex = 0
    End Sub

    Private Sub ChargeComboAction()
        CboAction.Items.Clear()
        CboAction.Items.Add("Selectionner...")
        If cboVuPar.SelectedItem.Value <> "2" Then
            CboAction.Items.Add("Exporter la vue courante vers Excel")
            'CboAction.Items.Add("Exporter la vue courante au format TXT")
            'CboAction.Items.Add("Exporter la vue courante au format PDF")
        End If
        CboAction.Items.Add("Exporter les données sources des statistiques vers Excel")
        'CboAction.Items.Add("Exporter les données sources des statistiques au format TXT")
        CboAction.Items.Add("Exporter les données sources de l'effectif vers Excel")
        'CboAction.Items.Add("Exporter les données sources de l'effectif au format TXT")
        CboAction.Items.Add("Afficher l'aperçu du bilan complet")

        CboAction.SelectedIndex = 0

    End Sub

    Protected Sub BtnRetour_Click(sender As Object, e As ImageClickEventArgs)
        WsCtrlGestion.GereRetour()
    End Sub

    Protected Sub cboVuPar_SelectedIndexChanged(sender As Object, e As EventArgs)
        ChargeComboAction()
        Dim Idx As Integer = Integer.Parse(cboVuPar.SelectedItem.Value)
        If MultiOnglets.ActiveViewIndex = idx Then
            Exit Sub
        End If
        If Idx < 2 Then
            cboVuPar.Focus()
        Else
            CtrlGraph.Focus()
        End If
        CtrlTableauSynthese.ChargeOnLoad = False
        CtrlGraph.ChargeOnLoad = False

        Select Case Idx
            Case 0
                MultiOnglets.ActiveViewIndex = 0
            Case 1
                MultiOnglets.ActiveViewIndex = 1
                CtrlTableauSynthese.Charge(False)
            Case 2
                MultiOnglets.ActiveViewIndex = 2
                CtrlGraph.Charge(False)
        End Select

    End Sub

    Protected Sub CboAction_SelectedIndexChanged(sender As Object, e As EventArgs)
        If CboAction.SelectedIndex = 0 Then
            Exit Sub
        End If
        Dim ExportSelectionne As String = CboAction.SelectedItem.Text

        CboAction.SelectedIndex = 0
        Select Case ExportSelectionne
            Case "Exporter la vue courante vers Excel"
                If MultiOnglets.ActiveViewIndex = 2 Then
                    Exit Sub
                End If
                GereExport(TypeExport.CSV, C_COURANT)
            Case "Exporter la vue courante au format TXT"
                If MultiOnglets.ActiveViewIndex = 2 Then
                    Exit Sub
                End If
                GereExport(TypeExport.TXT, C_COURANT)
            'Case "Exporter la vue courante au format PDF"
            '    Select Case MultiOnglets.ActiveViewIndex
            '        Case 0
            '            WsCtrlGestion.GereExportPdf(CtrlChiffresBruts)
            '        Case 1
            '            WsCtrlGestion.GereExportPdf(CtrlTableauSynthese)
            '        Case Else
            '            Exit Sub
            '    End Select
            Case "Exporter les données sources des statistiques vers Excel"
                Call GereExport(TypeExport.CSV, C_STAT)
            Case "Exporter les données sources des statistiques au format TXT"
                Call GereExport(TypeExport.TXT, C_STAT)
            Case "Exporter les données sources de l'effectif vers Excel"
                Call GereExport(TypeExport.CSV, C_EFFECTIF)
            Case "Exporter les données sources de l'effectif au format TXT"
                Call GereExport(TypeExport.TXT, C_EFFECTIF)
            Case "Afficher l'aperçu du bilan complet"
                Call ConstitueDossierExportPdf()
        End Select
    End Sub

End Class