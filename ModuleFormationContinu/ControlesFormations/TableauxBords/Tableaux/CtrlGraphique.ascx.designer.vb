﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtrlGraphique

    '''<summary>
    '''Contrôle TB1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TB2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle Table1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Table1 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Label8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label8 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle BtnSEXE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnSEXE As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnCATEGORIE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnCATEGORIE As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnSTATUT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnSTATUT As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnDOMAINE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnDOMAINE As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnTHEME.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnTHEME As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnTYPEACTION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnTYPEACTION As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnNIVEAU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnNIVEAU As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle BtnNIVEAU2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnNIVEAU2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle MultiOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiOnglets As Global.System.Web.UI.WebControls.MultiView

    '''<summary>
    '''Contrôle View1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View1 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_SEXE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_SEXE As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TB5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB5 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Sexe_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Sexe_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Sexe_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Sexe_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Sexe_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Sexe_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Sexe_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Sexe_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Sexe_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Sexe_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Sexe_Effectif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Sexe_Effectif As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View2 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_CATEGORIE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_CATEGORIE As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Categorie_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Categorie_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Categorie_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Categorie_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Categorie_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Categorie_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Categorie_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Categorie_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Categorie_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Categorie_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View3 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_STATUT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_STATUT As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Statut_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Statut_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Statut_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Statut_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Statut_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Statut_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Statut_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Statut_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Statut_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Statut_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View4 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_DOMAINE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_DOMAINE As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Domaine_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Domaine_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Domaine_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Domaine_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Domaine_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Domaine_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Domaine_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Domaine_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Domaine_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Domaine_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View5 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_THEME.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_THEME As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Theme_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Theme_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Theme_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Theme_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Theme_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Theme_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Theme_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Theme_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Theme_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Theme_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View6 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_TYPEACTION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_TYPEACTION As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle TypeAction_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TypeAction_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_TypeAction_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_TypeAction_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TypeAction_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TypeAction_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_TypeAction_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_TypeAction_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle TypeAction_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TypeAction_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View7 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle TB_NIVEAU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_NIVEAU As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Niveau1_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Niveau1_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Niveau1_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Niveau1_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Niveau1_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Niveau1_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Niveau1_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Niveau1_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Niveau1_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Niveau1_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle View8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents View8 As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''Contrôle Table3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Table3 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CellCboNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCboNiveau1 As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Table2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Table2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Label9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Label9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CboNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CboNiveau1 As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Contrôle TB_NIVEAU2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TB_NIVEAU2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Niveau2_NB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Niveau2_NB As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Niveau2_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Niveau2_Cout As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Niveau2_Cout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Niveau2_Cout As Global.System.Web.UI.DataVisualization.Charting.Chart

    '''<summary>
    '''Contrôle Cell_Niveau2_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell_Niveau2_Jour As Global.System.Web.UI.WebControls.TableCell

    '''<summary>
    '''Contrôle Niveau2_Jour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Niveau2_Jour As Global.System.Web.UI.DataVisualization.Charting.Chart
End Class
