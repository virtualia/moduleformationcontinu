﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class GestionListeResultat

    Public Shared Function GetListePVI_Beneficiaire() As CacheResultat

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Bénéficiaires"
        cr.EstSurStat = True

        cr.NomFichierExport = "Beneficiaires"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 45
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 45
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Date_Naissance"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TDate
        pv.Taille = 10
        pv.TitreColonne = "Nais."
        cr.ProprietesValeurs.Add(pv)

        Return cr

    End Function

    Public Shared Function GetListePVI_Stagiaire() As CacheResultat

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Stagiaires"
        cr.EstSurStat = True
        cr.NomFichierExport = "Stagiaires"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 40
        pv.TitreColonne = "Action de formation"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 25
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Date_Naissance"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TDate
        pv.Taille = 10
        pv.TitreColonne = "Nais."
        cr.ProprietesValeurs.Add(pv)

        'cr.Largeur = New Unit("1300px")

        Return cr

    End Function

    Public Shared Function GetListePVI_Jour() As CacheResultat

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Nombre de jours"
        cr.EstSurStat = True
        cr.NomFichierExport = "Nombre_Jour"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 35
        pv.TitreColonne = "Stage / Session"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 15
        pv.TitreColonne = "Dates"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Duree_Jour"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Durée (j)"
        pv.Taille = 10
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        Return cr

    End Function

    Public Shared Function GetListePVI_Cout() As CacheResultat

        Dim cr As CacheResultat = New CacheResultat()
        cr.Titre = "Total coûts "
        cr.EstSurStat = True
        cr.NomFichierExport = "Couts_Total"

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.TitreColonne = "Identifiant Interne"
        pv.TypeVal = PropValeurInfo.TypeValeur.TIdentifiant
        pv.EstEntete = False
        pv.EstTri = False
        pv.EstPredicat = False
        pv.EstClef = False
        pv.EstPourExport = True
        pv.Propriete = "Ide_Stat"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "EstFormationValidee"
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        pv.ValeurPredicat = "True"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "ActionFormationExport"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 35
        pv.TitreColonne = "Stage / Session"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "DuAu"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = False
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Center
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 15
        pv.TitreColonne = "Dates"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Nom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Nom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Prenom"
        pv.EstEntete = True
        pv.EstPredicat = False
        pv.EstTri = True
        pv.EstPourExport = True
        pv.HAlignment = HorizontalAlign.Left
        pv.TypeVal = PropValeurInfo.TypeValeur.TChaine
        pv.Taille = 20
        pv.TitreColonne = "Prénom"
        cr.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.Propriete = "Couts"
        pv.EstEntete = True
        pv.EstPredicat = True
        pv.EstTri = False
        pv.EstPourExport = True
        pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        pv.HAlignment = HorizontalAlign.Right
        pv.TypeVal = PropValeurInfo.TypeValeur.TDouble2
        pv.ValeurPredicat = "0"
        pv.TitreColonne = "Coûts"
        pv.Taille = 10
        cr.ProprietesValeurs.Add(pv)

        Return cr

    End Function
End Class
