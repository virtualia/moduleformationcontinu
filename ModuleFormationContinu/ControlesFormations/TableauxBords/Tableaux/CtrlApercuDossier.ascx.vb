﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Structure.Formation

Public Class CtrlApercuDossier
    Inherits UserControl
    Implements ICtrlExport

    Private WsCtrlGestion As CtrlGestionTableauBord
    Private WsCtrlGestionVue As CtrlVueTableauBord

    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
        End Set
    End Property

    Public WriteOnly Property CtrlGestionVue As CtrlVueTableauBord
        Set(value As CtrlVueTableauBord)
            WsCtrlGestionVue = value
        End Set
    End Property

    Public Sub Charge(Infos As ExportDossierInfo)
        CtrlApercu.Charge(Infos, WsCtrlGestion)
    End Sub

    Protected Sub BtnRetour_Click(sender As Object, e As ImageClickEventArgs)
        CtrlApercu.Vider()
        WsCtrlGestionVue.GereRetourApercu()
    End Sub

    Public Function GetExport() As ExportPdfInfo Implements ICtrlExport.GetExport
        Dim CRetour As ExportPdfInfo = New ExportPdfInfo() With {.NomFichier = "Dossier", .ContenuHtml = CtrlApercu.GetSourceHtml()}
        If (CRetour.ContenuHtml = "") Then
            CRetour.EstOk = False
        End If
        Return CRetour
    End Function

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        BtnPdf.Act = Sub(btn)
                         WsCtrlGestion.GereExportPdf(Me)
                     End Sub
    End Sub
End Class