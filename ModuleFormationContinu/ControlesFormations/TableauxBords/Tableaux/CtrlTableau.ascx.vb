﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing
Imports Virtualia.Structure.Formation
Imports System.IO

Public Class CtrlTableau
    Inherits UserControl
    Implements IControlBase
    Implements ICtrlExport

    Private WsCtrlGestion As CtrlGestionTableauBord
    Private WsNumLigne As Integer = 0

    Private Const CWIDTH_DONNEE As String = "130px"
    Private Const CWIDTH_ENTETE As String = "220px"
    Private Const CWIDTH_ENTETE1 As String = "20px"
    Private Const CWIDTH_ENTETE2 As String = "200px"
    Private Const CWIDTH_POURCENT As String = "50px"

    Private Const CLIB_SEXE As String = "SEXE"
    Private Const CLIB_CATEGORIE As String = "CATEGORIE"
    Private Const CLIB_STATUT As String = "STATUT"
    Private Const CLIB_NIVEAU As String = CLIB_NIVEAU1 & " ET " & CLIB_NIVEAU2
    Private Const CLIB_NIVEAU1 As String = "NIVEAU 1"
    Private Const CLIB_NIVEAU2 As String = "NIVEAU 2"
    Private Const CLIB_DOMAINE As String = "DOMAINE"
    Private Const CLIB_THEME As String = "THEME"
    Private Const CLIB_TYPE As String = "TYPE"

    Private Const CLIB_ETOILE As String = "*"
    Private Const CLIB_VIDE As String = "(Non renseigné)"

    Private Const C_BENEF As String = "BENEF"
    Private Const C_STAG As String = "STAG"
    Private Const C_DUREE As String = "DUREE"
    Private Const C_COUT As String = "COUT"

    Private WsCoul_Titre As Color = VisuHelper.ConvertiCouleur("#19968D")
    Private WsCoul_Donnee_impaire As Color = Color.Snow
    Private WsCoul_Donnee_paire As Color = VisuHelper.ConvertiCouleur("#E5E5E5")
    Private WsCoul_Donnee_regroupement As Color = VisuHelper.ConvertiCouleur("#C1C1C1")

    Private WsLstBtn As List(Of Button) = New List(Of Button)()

    Private Const CNB_ENREG As Integer = 27

    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Public Property ChargeOnLoad As Boolean
        Get
            If (Not ViewState.KeyExiste("ChargeOnLoad")) Then
                ViewState.AjouteValeur("ChargeOnLoad", False)
            End If
            Return DirectCast(ViewState("ChargeOnLoad"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("ChargeOnLoad", value)
        End Set
    End Property

    Private ReadOnly Property Stats As StatistiqueFormationInfoCollection
        Get
            Return WsCtrlGestion.VCache.Statistiques
        End Get
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        If Not (ChargeOnLoad) Then
            Return
        End If
        If (WsCtrlGestion Is Nothing OrElse WsCtrlGestion.VCache Is Nothing OrElse WsCtrlGestion.VCache.Statistiques.Count <= 0) Then
            Return
        End If
        ConstruitLignesTableau()
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        ConstruitLignesTableau()
        ChargeOnLoad = True
    End Sub

    Public Function ToValeurs(extension As TypeExport) As List(Of String)
        Dim LstResultat As List(Of String) = New List(Of String)()
        Dim LstCellules As List(Of TableCell)
        Dim valct As String
        Dim SiEstSurDonneeNiveau As Boolean = False
        Dim SiPremier As Boolean = True
        Dim libniveau1 As String = ""
        Dim cont As ContenuExport

        For Each Rangee As TableRow In CadreDonnees.Rows
            If Rangee.Cells.Count = 2 Then
                'On est sur une row de titre
                SiEstSurDonneeNiveau = DirectCast(Rangee.Cells(0).Controls(0), Label).Text = CLIB_NIVEAU
            End If
            LstCellules = VisuHelper.GetControlSaisie(Of TableCell)(DirectCast(Rangee, Control))
            SiPremier = True

            cont = New ContenuExport(extension)

            LstCellules.ForEach(Sub(cell)
                                    If cell.Controls.Count <= 0 Then
                                        For index = 0 To cell.ColumnSpan - 1
                                            cont.Add("")
                                        Next
                                        Return
                                    End If
                                    If cell.ColumnSpan > 1 Then
                                        For index = 0 To cell.ColumnSpan - 1
                                            If index = 0 Then
                                                valct = GetTextControl(cell.Controls(0))
                                                If SiEstSurDonneeNiveau = True AndAlso SiPremier = True Then
                                                    libniveau1 = valct
                                                    SiPremier = False
                                                End If
                                                cont.Add(valct)
                                                Continue For
                                            End If
                                            cont.Add("")
                                        Next
                                        Return
                                    End If
                                    valct = GetTextControl(cell.Controls(0))
                                    If SiEstSurDonneeNiveau = True AndAlso valct.Trim() = CLIB_ETOILE Then
                                        cont.Add(libniveau1)
                                    Else
                                        cont.Add(valct)
                                    End If
                                End Sub)
            LstResultat.Add(cont.ToString())
        Next
        Return LstResultat
    End Function

    Private Function GetTextControl(ct As Control) As String
        Dim Etiquette As Label = TryCast(ct, Label)
        If Etiquette IsNot Nothing Then
            Return Etiquette.Text
        End If
        Dim Btn As Button = TryCast(ct, Button)
        If Btn IsNot Nothing Then
            Return Btn.Text
        End If
        Return ""
    End Function

    Private Sub ConstruitLignesTableau()
        Dim lstrows As DetailRowCollection = GetCollectionRowInfo()
        DetailRowCollection.RempliTable(CadreDonnees, ConstruitLigneEntete(), lstrows)
        AffectClick()
    End Sub

    Private Function GetCollectionRowInfo() As DetailRowCollection
        Dim lstrows As DetailRowCollection = New DetailRowCollection()

        lstrows.AddRange(ConstruitLigneDonneeSexe())
        lstrows.AddRange(ConstruitLigneDonneeCategorie())
        lstrows.AddRange(ConstruitLigneDonneeStatuts())
        lstrows.AddRange(ConstruitLigneDonneeNiveau())
        lstrows.AddRange(ConstruitLigneDonneeDomaines())
        lstrows.AddRange(ConstruitLigneDonneeTheme())
        lstrows.AddRange(ConstruitLigneDonneeTypeAction())
        Return lstrows
    End Function

    Private Function ConstruitLigneEntete() As TableRow
        Dim row As TableRow = ConstruitRow(WsCoul_Titre, Color.Snow, True, False)

        row.Cells.Add(ConstruitCellVide(2))
        row.Cells.Add(ConstruitCellValeur(0, "Nb bénéficiaires", CWIDTH_DONNEE, HorizontalAlign.Left, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(1, "%", CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(2, "Nb stagiaires", CWIDTH_DONNEE, HorizontalAlign.Left, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(3, "%", CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(4, "Durée en jours", CWIDTH_DONNEE, HorizontalAlign.Left, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(5, "%", CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(6, "Coûts", CWIDTH_DONNEE, HorizontalAlign.Left, False, 1, True))
        row.Cells.Add(ConstruitCellValeur(7, "%", CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, True))

        WsNumLigne += 1

        Return row
    End Function

    Private Function ConstruitLigneDonneeSexe() As List(Of DetailRow)
        Dim f_sel As Func(Of String, CleeValeurStatistiqueInfo) = Function(Clef)
                                                                      Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                      If Clef.ToLower() = "masculin" Then
                                                                          CRetour.Clee = "Hommes"
                                                                      Else
                                                                          CRetour.Clee = "Femmes"
                                                                      End If
                                                                      Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                     Return Sfi.Sexe.ToLower() = Clef.ToLower()
                                                                                                                                 End Function
                                                                      CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)
                                                                      Return CRetour
                                                                  End Function

        Dim qr = From s In (From st In Stats Where st.EstFormationValidee Select st.Sexe).Distinct() Select f_sel(s)
        Dim NbBenef_Tot As Integer = (From it In qr Select it.ValeurDuPredicat.NbBenef).Sum()
        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_SEXE, NbBenef_Tot, Stats.NB_Stagiaire)
        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim cpt As Integer = 0
        For Each odyn In qr
            Rangee = ConstruitLigneDonnee(CLIB_SEXE, odyn.Clee, odyn.ValeurDuPredicat, (cpt Mod 2 = 0))
            lstdr.Add(New DetailRow() With {.RT = Rangee})
            cpt += 1
        Next
        Return lstdr
    End Function

    Private Function ConstruitLigneDonneeCategorie() As List(Of DetailRow)
        Dim f_sel As Func(Of String, CleeValeurStatistiqueInfo) = Function(Clef)
                                                                      Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                      CRetour.Clee = Clef
                                                                      Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                     Return Sfi.Categorie = Clef
                                                                                                                                 End Function
                                                                      CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)
                                                                      Return CRetour
                                                                  End Function

        Dim qr = From s In (From st In Stats Where st.EstFormationValidee Select st.Categorie).Distinct() Order By s Select f_sel(s)
        Dim NbBenef_Tot As Integer = (From it In qr Select it.ValeurDuPredicat.NbBenef).Sum()
        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_CATEGORIE, NbBenef_Tot, Stats.NB_Stagiaire)

        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim Cpt As Integer = 0

        For Each odyn In qr
            odyn.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn.ValeurDuPredicat.NbBenef, NbBenef_Tot)
            Rangee = ConstruitLigneDonnee(CLIB_CATEGORIE, odyn.Clee, odyn.ValeurDuPredicat, (Cpt Mod 2 = 0))
            lstdr.Add(New DetailRow() With {.RT = Rangee})

            Cpt += 1
        Next

        Return lstdr

    End Function

    Private Function ConstruitLigneDonneeStatuts() As List(Of DetailRow)
        Dim f_sel As Func(Of String, CleeValeurStatistiqueInfo) = Function(Clef)
                                                                      Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                      CRetour.Clee = Clef
                                                                      Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                     Return Sfi.Statut = Clef
                                                                                                                                 End Function
                                                                      CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)
                                                                      Return CRetour
                                                                  End Function

        Dim qr = From s In (From st In Stats Where st.EstFormationValidee Select st.Statut).Distinct() Order By s Select f_sel(s)
        Dim NbBenef_Tot As Integer = (From it In qr Select it.ValeurDuPredicat.NbBenef).Sum()
        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_STATUT, NbBenef_Tot, Stats.NB_Stagiaire)

        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim cpt As Integer = 0

        For Each odyn In qr

            odyn.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn.ValeurDuPredicat.NbBenef, NbBenef_Tot)
            Rangee = ConstruitLigneDonnee(CLIB_STATUT, odyn.Clee, odyn.ValeurDuPredicat, (cpt Mod 2 = 0))
            lstdr.Add(New DetailRow() With {.RT = Rangee})
            cpt = cpt + 1
        Next
        Return lstdr

    End Function

    Private Function ConstruitLigneDonneeNiveau() As List(Of DetailRow)
        Dim f_sel_Niv1 As Func(Of String, CleeValeurStatistiqueInfo) = Function(Clef)
                                                                           Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                           CRetour.Clee = Clef
                                                                           Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                          Return Sfi.Niveau1 = Clef
                                                                                                                                      End Function
                                                                           CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)

                                                                           Return CRetour
                                                                       End Function

        Dim qr_Niv1 = From s In (From st In Stats Where st.EstFormationValidee Select st.Niveau1).Distinct() Order By s Select f_sel_Niv1(s)
        Dim NbBenef_Tot_Niv1 As Integer = (From it In qr_Niv1 Select it.ValeurDuPredicat.NbBenef).Sum()
        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_NIVEAU, NbBenef_Tot_Niv1, Stats.NB_Stagiaire)

        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim f_sel_Niv2 As Func(Of String, String, CleeValeurStatistiqueInfo) = Function(niv1, niv2)
                                                                                   Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                                   CRetour.Clee = niv2
                                                                                   Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                                  Return Sfi.Niveau1 = niv1 AndAlso Sfi.Niveau2 = niv2
                                                                                                                                              End Function
                                                                                   CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)

                                                                                   Return CRetour
                                                                               End Function

        Dim Cpt As Integer = 0

        For Each odyn_Niv1 In qr_Niv1
            odyn_Niv1.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn_Niv1.ValeurDuPredicat.NbBenef, NbBenef_Tot_Niv1)
            Rangee = ConstruitLigneDonnee(CLIB_NIVEAU1, odyn_Niv1.Clee, odyn_Niv1.ValeurDuPredicat, True, True, WsCoul_Donnee_regroupement)
            lstdr.Add(New DetailRow() With {.SiEstLigneRegroupement = True, .RT = Rangee})
            If odyn_Niv1.Clee.Trim() = "" Then
                Continue For
            End If
            Cpt = 0

            Dim qr_Niv2 = From s In (From st In Stats Where st.EstFormationValidee AndAlso st.Niveau1 = odyn_Niv1.Clee Select st.Niveau2).Distinct() Order By s Select f_sel_Niv2(odyn_Niv1.Clee, s)

            For Each odyn_Niv2 In qr_Niv2
                odyn_Niv2.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn_Niv2.ValeurDuPredicat.NbBenef, NbBenef_Tot_Niv1)
                Rangee = ConstruitLigneDonnee(CLIB_NIVEAU2, odyn_Niv2.Clee, odyn_Niv2.ValeurDuPredicat, ((Cpt Mod 2) = 0), False)
                lstdr.Add(New DetailRow() With {.RT = Rangee})
                cpt = cpt + 1
            Next
        Next
        Return lstdr
    End Function

    Private Function ConstruitLigneDonneeDomaines() As List(Of DetailRow)
        Dim f_sel As Func(Of String, CleeValeurStatistiqueInfo) = Function(clee)
                                                                      Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                      CRetour.Clee = clee
                                                                      Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                     Return Sfi.Domaine = clee
                                                                                                                                 End Function
                                                                      CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)
                                                                      Return CRetour
                                                                  End Function

        Dim qr = From s In (From st In Stats Where st.EstFormationValidee Select st.Domaine).Distinct() Order By s Select f_sel(s)
        Dim NbBenef_Tot As Integer = (From it In qr Select it.ValeurDuPredicat.NbBenef).Sum()
        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_DOMAINE, NbBenef_Tot, Stats.NB_Stagiaire)

        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim Cpt As Integer = 0

        For Each odyn In qr
            odyn.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn.ValeurDuPredicat.NbBenef, NbBenef_Tot)
            Rangee = ConstruitLigneDonnee(CLIB_DOMAINE, odyn.Clee, odyn.ValeurDuPredicat, (Cpt Mod 2 = 0))
            lstdr.Add(New DetailRow() With {.RT = Rangee})

            cpt = cpt + 1
        Next
        Return lstdr
    End Function

    Private Function ConstruitLigneDonneeTheme() As List(Of DetailRow)
        Dim f_sel As Func(Of String, CleeValeurStatistiqueInfo) = Function(clee)
                                                                      Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                      CRetour.Clee = clee
                                                                      Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                     Return Sfi.Theme = clee
                                                                                                                                 End Function
                                                                      CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)
                                                                      Return CRetour
                                                                  End Function

        Dim qr = From s In (From st In Stats Where st.EstFormationValidee Select st.Theme).Distinct() Order By s Select f_sel(s)
        Dim NbBenef_Tot As Integer = (From it In qr Select it.ValeurDuPredicat.NbBenef).Sum()

        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_THEME, NbBenef_Tot, Stats.NB_Stagiaire)

        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim Cpt As Integer = 0

        For Each odyn In qr
            odyn.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn.ValeurDuPredicat.NbBenef, NbBenef_Tot)
            Rangee = ConstruitLigneDonnee(CLIB_THEME, odyn.Clee, odyn.ValeurDuPredicat, (Cpt Mod 2 = 0))
            lstdr.Add(New DetailRow() With {.RT = Rangee})
            cpt = cpt + 1
        Next
        Return lstdr
    End Function

    Private Function ConstruitLigneDonneeTypeAction() As List(Of DetailRow)
        Dim f_sel As Func(Of String, CleeValeurStatistiqueInfo) = Function(clee)
                                                                      Dim CRetour As CleeValeurStatistiqueInfo = New CleeValeurStatistiqueInfo()
                                                                      CRetour.Clee = clee
                                                                      Dim f_pred As Func(Of StatistiqueFormationInfo, Boolean) = Function(Sfi)
                                                                                                                                     Return Sfi.Type_Action = clee
                                                                                                                                 End Function
                                                                      CRetour.ValeurDuPredicat = GetValeurDuPredicat(f_pred)
                                                                      Return CRetour
                                                                  End Function

        Dim qr = From s In (From st In Stats Where st.EstFormationValidee Select st.Type_Action).Distinct() Order By s Select f_sel(s)
        Dim NbBenef_Tot As Integer = (From it In qr Select it.ValeurDuPredicat.NbBenef).Sum()

        Dim Rangee As TableRow = ConstruitLigneEnteteDonneeValeur(CLIB_TYPE, NbBenef_Tot, Stats.NB_Stagiaire)

        Dim lstdr As List(Of DetailRow) = New List(Of DetailRow)()
        lstdr.Add(New DetailRow() With {.SiEstLigneEnteteGroupe = True, .RT = Rangee})

        Dim Cpt As Integer = 0

        For Each odyn In qr

            odyn.ValeurDuPredicat.PourcentBenef = StatistiqueFormationInfoCollection.GetPourcent(odyn.ValeurDuPredicat.NbBenef, NbBenef_Tot)
            Rangee = ConstruitLigneDonnee(CLIB_TYPE, odyn.Clee, odyn.ValeurDuPredicat, (Cpt Mod 2 = 0))
            lstdr.Add(New DetailRow() With {.RT = Rangee})
            cpt += 1
        Next
        Return lstdr
    End Function

    Private Sub ConstruitLigneEnteteDonnee(TitreLigne As String)
        Dim Rangee As TableRow = ConstruitRow(WsCoul_Titre, Color.Snow, True, False)
        Rangee.Cells.Add(ConstruitCellValeur(0, TitreLigne, CWIDTH_ENTETE, HorizontalAlign.Left, False, 2, True))
        Rangee.Cells.Add(ConstruitCellVide(8))
        CadreDonnees.Rows.Add(Rangee)
        WsNumLigne += 1
    End Sub

    Private Function ConstruitLigneEnteteDonneeValeur(ByVal TitreLigne As String, ByVal NbBenefs As Integer, ByVal NbStagiaires As Integer) As TableRow
        Dim Rangee As TableRow = ConstruitRow(WsCoul_Titre, Color.Snow, True, False)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Rangee.Cells.Add(ConstruitCellValeur(0, TitreLigne, CWIDTH_ENTETE, HorizontalAlign.Left, False, 2, True))
        'NbBenef
        Rangee.Cells.Add(ConstruitCellValeur(1, WebFct.ViRhFonction.MontantEdite(NbBenefs, 0), CWIDTH_DONNEE, HorizontalAlign.Center, False, 1, True))
        Rangee.Cells.Add(ConstruitCellValeur(2, "", CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, True))
        'nbstagiaire
        Rangee.Cells.Add(ConstruitCellValeur(3, WebFct.ViRhFonction.MontantEdite(NbStagiaires, 0), CWIDTH_DONNEE, HorizontalAlign.Center, False, 1, True))
        Rangee.Cells.Add(ConstruitCellVide(5))
        WsNumLigne += 1

        Return Rangee
    End Function

    Friend Function ConstruitRow(ByVal Couleur As Color, ByVal CouleurPolice As Color, ByVal FontBold As Boolean, ByVal FontItalic As Boolean) As TableRow
        Dim CRetour As TableRow = New TableRow() With {.BackColor = couleur, .ForeColor = CouleurPolice}
        If FontBold = True Then
            CRetour.Font.Bold = True
        End If
        If fontitalic = True Then
            CRetour.Font.Italic = True
        End If
        Return CRetour
    End Function

    Private Function ConstruitCellVide(ByVal nbspan As Integer, Optional ByVal SiAvecBorder As Boolean = True) As TableCell
        Dim CRetour As TableCell = New TableCell() With {.BorderColor = Color.Black, .BorderWidth = New Unit("1px")}
        If SiAvecBorder = True Then
            CRetour.BorderStyle = BorderStyle.NotSet
        Else
            CRetour.BorderStyle = BorderStyle.None
        End If
        If nbspan > 1 Then
            CRetour.ColumnSpan = nbspan
        End If
        Return CRetour
    End Function

    Private Function ConstruitCellValeur(ByVal NumTitre As Integer, ByVal EtiText As String, ByVal EtiWidth As String, _
                                         ByVal AlignementText As HorizontalAlign, ByVal SINonItalic As Boolean, ByVal NbSpan As Integer, ByVal SiAvecBorder As Boolean) As TableCell

        Dim CRetour As TableCell = ConstruitCellVide(NbSpan, SiAvecBorder)
        Dim Eti As Label = ConstruitLabel("lbl_" & WsNumLigne & "_" & NumTitre, EtiWidth, AlignementText, SINonItalic)
        Eti.Text = EtiText
        CRetour.Controls.Add(Eti)
        Return CRetour
    End Function

    Private Function ConstruitLabel(ByVal IdEti As String, ByVal EtiWidth As String, ByVal AlignementText As HorizontalAlign, ByVal SiNonItalic As Boolean) As Label
        Dim Eti As Label = New Label() With {.Width = New Unit(EtiWidth), .BorderStyle = BorderStyle.None, .BorderWidth = New Unit("0px")}

        If SiNonItalic = True Then
            Eti.Font.Italic = False
        End If
        Select Case AlignementText
            Case HorizontalAlign.Left
                Eti.Style.Add(HtmlTextWriterStyle.MarginLeft, "2px")
                Eti.Style.Add(HtmlTextWriterStyle.TextAlign, "left")
            Case HorizontalAlign.Right
                Eti.Style.Add(HtmlTextWriterStyle.MarginRight, "2px")
                Eti.Style.Add(HtmlTextWriterStyle.TextAlign, "right")
            Case HorizontalAlign.Center
                Eti.Style.Add(HtmlTextWriterStyle.TextAlign, "center")
        End Select

        Return Eti
    End Function

    Private Function ConstruitLigneDonnee(ByVal LibGroupe As String, ByVal LibEntete As String, ByVal Valeur As ValeurStatistiqueInfo, ByVal SiEstPaire As Boolean, _
                                          Optional ByVal SiDoubleCellule As Boolean = True, Optional ByVal CouleurLigne As Color? = Nothing) As TableRow
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim Couleur As Color = WsCoul_Donnee_impaire
        If SiEstPaire = True Then
            Couleur = WsCoul_Donnee_paire
        End If
        If CouleurLigne IsNot Nothing Then
            Couleur = CouleurLigne.Value
        End If
        Dim Rangee As TableRow = ConstruitRow(Couleur, Color.Black, False, True)
        Dim ColSpan As Integer = 2
        Dim Taille As String = CWIDTH_ENTETE

        If SiDoubleCellule = False Then
            ColSpan = 1
            Taille = CWIDTH_ENTETE2
            Rangee.Cells.Add(ConstruitCellValeur(0, CLIB_ETOILE, CWIDTH_ENTETE1, HorizontalAlign.Center, True, 1, False))
        End If
        If LibEntete.Trim() = "" Then
            Rangee.Cells.Add(ConstruitCellValeur(1, CLIB_VIDE, Taille, HorizontalAlign.Left, True, ColSpan, True))
        Else
            Rangee.Cells.Add(ConstruitCellValeur(1, LibEntete, Taille, HorizontalAlign.Left, True, ColSpan, True))
        End If

        Dim MontantColonne As String = WebFct.ViRhFonction.MontantEdite(Valeur.NbBenef, 0, True)
        If MontantColonne = "" Then
            Rangee.Cells.Add(ConstruitCellValeur(2, "", CWIDTH_DONNEE, HorizontalAlign.Center, False, 1, False))
        Else
            Rangee.Cells.Add(ConstruitCellButton(2, LibGroupe, C_BENEF, MontantColonne, CWIDTH_DONNEE, HorizontalAlign.Center))
        End If
        Rangee.Cells.Add(ConstruitCellValeur(3, WebFct.ViRhFonction.MontantEdite(Valeur.PourcentBenef, 2, True), CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, False))

        MontantColonne = WebFct.ViRhFonction.MontantEdite(Valeur.NbStag, 0, True)
        If MontantColonne = "" Then
            Rangee.Cells.Add(ConstruitCellValeur(4, "", CWIDTH_DONNEE, HorizontalAlign.Center, False, 1, False))
        Else
            Rangee.Cells.Add(ConstruitCellButton(4, LibGroupe, C_STAG, MontantColonne, CWIDTH_DONNEE, HorizontalAlign.Center))
        End If
        Rangee.Cells.Add(ConstruitCellValeur(5, WebFct.ViRhFonction.MontantEdite(Valeur.PourcentStag, 2, True), CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, False))

        MontantColonne = WebFct.ViRhFonction.MontantEdite(Valeur.Duree, 2, True)
        If WebFct.ViRhFonction.ConversionDouble(MontantColonne) = 0 Then
            Rangee.Cells.Add(ConstruitCellValeur(6, "", CWIDTH_DONNEE, HorizontalAlign.Center, False, 1, False))
        Else
            Rangee.Cells.Add(ConstruitCellButton(6, LibGroupe, C_DUREE, MontantColonne, CWIDTH_DONNEE, HorizontalAlign.Center))
        End If
        Rangee.Cells.Add(ConstruitCellValeur(7, WebFct.ViRhFonction.MontantEdite(Valeur.PourcentDuree, 2, True), CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, False))

        MontantColonne = WebFct.ViRhFonction.MontantEdite(Valeur.Cout, 2, True)
        If WebFct.ViRhFonction.ConversionDouble(MontantColonne) = 0 Then
            Rangee.Cells.Add(ConstruitCellValeur(8, "", CWIDTH_DONNEE, HorizontalAlign.Center, False, 1, False))
        Else
            Rangee.Cells.Add(ConstruitCellButton(8, LibGroupe, C_COUT, MontantColonne, CWIDTH_DONNEE, HorizontalAlign.Center))
        End If
        Rangee.Cells.Add(ConstruitCellValeur(9, WebFct.ViRhFonction.MontantEdite(Valeur.PourcentCout, 2, True), CWIDTH_POURCENT, HorizontalAlign.Center, False, 1, False))

        WsNumLigne += 1

        Return Rangee

    End Function

    Private Function GetValeurDuPredicat(func_pred As Func(Of StatistiqueFormationInfo, Boolean)) As ValeurStatistiqueInfo
        Dim CRetour As ValeurStatistiqueInfo = New ValeurStatistiqueInfo()

        CRetour.NbBenef = Stats.GetValeurBenef(func_pred)
        CRetour.PourcentBenef = 0
        If Stats.NB_Beneficiaire > 0 Then
            CRetour.PourcentBenef = Math.Round((CRetour.NbBenef / Stats.NB_Beneficiaire) * 100, 2)
        End If

        CRetour.NbStag = Stats.GetValeurStagiaire(func_pred)
        CRetour.PourcentStag = 0
        If Stats.NB_Stagiaire > 0 Then
            CRetour.PourcentStag = Math.Round((CRetour.NbStag / Stats.NB_Stagiaire) * 100, 2)
        End If

        CRetour.Duree = Stats.GetValeurDuree_Jour(func_pred)
        CRetour.PourcentDuree = 0
        If Stats.Durees_Global_J > 0 Then
            CRetour.PourcentDuree = Math.Round((CRetour.Duree / Stats.Durees_Global_J) * 100, 2)
        End If

        CRetour.Cout = Stats.GetValeurCout(func_pred)
        CRetour.PourcentCout = 0
        If Stats.Couts_Global > 0 Then
            CRetour.PourcentCout = Math.Round((CRetour.Cout / Stats.Couts_Global) * 100, 2)
        End If

        Return CRetour
    End Function

    Private Function ConstruitCellButton(ByVal Numtitre As Integer, ByVal LibGroupe As String, ByVal TypeDonnee As String, ByVal Valeur As String, _
                                         ByVal WidthDonnee As String, ByVal HAlign As HorizontalAlign) As TableCell
        Dim CRetour As TableCell = ConstruitCellVide(1, False)
        CRetour.Width = New Unit(WidthDonnee)

        CRetour.HorizontalAlign = hAlign

        Dim Bouton As Button = New Button()

        Bouton.ID = LibGroupe & "_" & TypeDonnee & "_" & WsNumLigne & "_" & Numtitre
        Bouton.BackColor = Color.Transparent
        Bouton.BorderStyle = BorderStyle.None
        Bouton.Text = Valeur
        Bouton.Font.Italic = True
        Bouton.Style.Add(HtmlTextWriterStyle.Cursor, "pointer")

        CRetour.Controls.Add(Bouton)
        WsLstBtn.Add(Bouton)

        Return CRetour

    End Function

    Private Sub AffectClick()
        If WsLstBtn.Count <= 0 Then
            Exit Sub
        End If
        WsLstBtn.ForEach(Sub(Btn)
                             Btn.Enabled = (Btn.Text <> "")
                             AddHandler Btn.Click, Sub(s, e)
                                                       Dim Bouton As Button = DirectCast(s, Button)
                                                       Dim Ssplit As String() = DirectCast(s, Button).ID.Split("_"c)
                                                       Dim CRes As CacheResultat = GetCacheResultat(Ssplit(1))

                                                       Select Case Ssplit(0)
                                                           Case CLIB_CATEGORIE
                                                               ClickCategorie(Bouton, CRes)
                                                           Case CLIB_DOMAINE
                                                               ClickDomaine(Bouton, CRes)
                                                           Case CLIB_SEXE
                                                               ClickSexe(Bouton, CRes)
                                                           Case CLIB_STATUT
                                                               ClickStatut(Bouton, CRes)
                                                           Case CLIB_THEME
                                                               ClickTheme(Bouton, CRes)
                                                           Case CLIB_TYPE
                                                               ClickType(Bouton, CRes)
                                                           Case CLIB_NIVEAU1
                                                               ClickNiveau1(Bouton, CRes)
                                                           Case CLIB_NIVEAU2
                                                               ClickNiveau2(Bouton, CRes)
                                                       End Select
                                                   End Sub
                         End Sub)

    End Sub

    Private Sub ClickNiveau1(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Niveau_1"

        Dim ValeurPred As String = GetValeurFiltre(Bouton)
        Dim pv As PropValeurInfo = New PropValeurInfo()

        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Niveau1"
        pv.EstPourExport = True
        pv.TitreColonne = "Niveau 1"
        pv.ValeurPredicat = ValeurPred
        CRes.ProprietesValeurs.Add(pv)

        If ValeurPred = "" Then
            CRes.Titre = CRes.Titre & " - Niveau 1 : " & CLIB_VIDE
        Else
            CRes.Titre = CRes.Titre & " - Niveau 1 : " & ValeurPred
        End If
        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Sub ClickNiveau2(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Niveau_2"

        Dim Rangee As TableRow = DirectCast(Bouton.Parent.Parent, TableRow)
        Dim Cadre As Table = DirectCast(Rangee.Parent, Table)
        Dim LstToutesRangees As List(Of TableRow) = New List(Of TableRow)()

        For Each rt As TableRow In Cadre.Rows
            LstToutesRangees.Add(rt)
        Next
        Dim idxr As Integer = LstToutesRangees.IndexOf(Rangee)
        Dim rniv1 As TableRow = (From rtmp In LstToutesRangees Where rtmp.Cells(0).ColumnSpan = 2 And LstToutesRangees.IndexOf(rtmp) < idxr _
                                                                        Order By LstToutesRangees.IndexOf(rtmp) Descending Select rtmp).First()

        Dim valeurniv1 As String = DirectCast(rniv1.Cells(0).Controls(0), Label).Text
        Dim valeurniv2 As String = DirectCast(Rangee.Cells(1).Controls(0), Label).Text

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Niveau1"
        If (valeurniv1 = CLIB_VIDE) Then
            pv.ValeurPredicat = ""
        Else
            pv.ValeurPredicat = valeurniv1
        End If
        pv.EstPourExport = True
        pv.TitreColonne = "Niveau 1"
        CRes.ProprietesValeurs.Add(pv)

        pv = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Niveau2"
        If valeurniv2 = CLIB_VIDE Then
            pv.ValeurPredicat = ""
        Else
            pv.ValeurPredicat = valeurniv2
        End If
        pv.EstPourExport = True
        pv.TitreColonne = "Niveau 2"
        CRes.ProprietesValeurs.Add(pv)

        CRes.Titre = CRes.Titre & " - Niveau 1 / Niveau 2 : " & valeurniv1 & " / " & valeurniv2

        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Sub ClickType(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Type"

        Dim ValeurPred As String = GetValeurFiltre(Bouton)
        Dim pv As PropValeurInfo = New PropValeurInfo()

        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Type_Action"
        pv.ValeurPredicat = ValeurPred
        pv.EstPourExport = True
        pv.TitreColonne = "Type Action"
        CRes.ProprietesValeurs.Add(pv)

        If ValeurPred = "" Then
            CRes.Titre = CRes.Titre & " - Type : " & CLIB_VIDE
        Else
            CRes.Titre = CRes.Titre & " - Type : " & ValeurPred
        End If

        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Sub ClickTheme(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Theme"

        Dim ValeurPred As String = GetValeurFiltre(Bouton)

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Theme"
        pv.ValeurPredicat = ValeurPred
        pv.EstPourExport = True
        pv.TitreColonne = "Theme"
        CRes.ProprietesValeurs.Add(pv)

        If ValeurPred = "" Then
            CRes.Titre = CRes.Titre & " - Thème : " & CLIB_VIDE
        Else
            CRes.Titre = CRes.Titre & " - Thème : " & ValeurPred
        End If

        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Sub ClickStatut(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Statut"

        Dim valeurpred As String = GetValeurFiltre(Bouton)

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Statut"
        pv.ValeurPredicat = valeurpred
        pv.EstPourExport = True
        pv.TitreColonne = "Statut"
        CRes.ProprietesValeurs.Add(pv)

        If (valeurpred = "") Then
            CRes.Titre = CRes.Titre & " - Statut : " & CLIB_VIDE
        Else
            CRes.Titre = CRes.Titre & " - Statut : " & valeurpred
        End If

        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Sub ClickSexe(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Sexe"

        Dim valeurpred As String = GetValeurFiltre(Bouton)

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Sexe"
        If (valeurpred.ToLower() = "hommes") Then
            pv.ValeurPredicat = "masculin"
            pv.TypeOpe = PropValeurInfo.TypeOperateur.Egal
        Else
            pv.ValeurPredicat = "masculin"
            pv.TypeOpe = PropValeurInfo.TypeOperateur.Different
        End If
        pv.EstPourExport = True
        pv.TitreColonne = "Sexe"
        CRes.ProprietesValeurs.Add(pv)

        CRes.Titre = CRes.Titre & " - " & valeurpred

        WsCtrlGestion.MontreResultat(CRes)

    End Sub

    Private Sub ClickDomaine(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Domaine"

        Dim valeurpred As String = GetValeurFiltre(Bouton)

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Domaine"
        pv.ValeurPredicat = valeurpred
        pv.EstPourExport = True
        pv.TitreColonne = "Domaine"
        CRes.ProprietesValeurs.Add(pv)

        If valeurpred = "" Then
            CRes.Titre = CRes.Titre & " - Domaine : " & CLIB_VIDE
        Else
            CRes.Titre = CRes.Titre & " - Domaine : " & valeurpred
        End If

        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Sub ClickCategorie(ByVal Bouton As Button, ByVal CRes As CacheResultat)
        CRes.NomFichierExport = "Categorie"

        Dim valeurpred As String = GetValeurFiltre(Bouton)

        Dim pv As PropValeurInfo = New PropValeurInfo()
        pv.EstEntete = False
        pv.EstPredicat = True
        pv.Propriete = "Categorie"
        pv.ValeurPredicat = valeurpred
        pv.EstPourExport = True
        pv.TitreColonne = "Categorie"
        CRes.ProprietesValeurs.Add(pv)

        If valeurpred = "" Then
            CRes.Titre = CRes.Titre & " - Catégorie : " & CLIB_VIDE
        Else
            CRes.Titre = CRes.Titre & " - Catégorie : " & valeurpred
        End If

        WsCtrlGestion.MontreResultat(CRes)
    End Sub

    Private Function GetValeurFiltre(ByVal Bouton As Button) As String
        Dim Rangee As TableRow = DirectCast(Bouton.Parent.Parent, TableRow)
        Dim lblentete As Label = DirectCast(Rangee.Cells(0).Controls(0), Label)
        If lblentete.Text = CLIB_VIDE Then
            Return ""
        End If
        Return lblentete.Text
    End Function

    Private Function GetCacheResultat(ByVal TypeDonnee As String) As CacheResultat
        Dim CRes As CacheResultat = Nothing
        Select Case TypeDonnee
            Case C_BENEF
                CRes = GestionListeResultat.GetListePVI_Beneficiaire()
            Case C_COUT
                CRes = GestionListeResultat.GetListePVI_Cout()
            Case C_DUREE
                CRes = GestionListeResultat.GetListePVI_Jour()
            Case C_STAG
                CRes = GestionListeResultat.GetListePVI_Stagiaire()
        End Select
        Return CRes
    End Function

    Public Function GetExport() As ExportPdfInfo Implements ICtrlExport.GetExport
        Dim SWriter As StringWriter = New StringWriter()
        Dim HTextWriter As HtmlTextWriter = New HtmlTextWriter(SWriter)

        Dim CRetour As ExportPdfInfo = New ExportPdfInfo()
        CRetour.NomFichier = "ChiffresSyntheses"

        Try
            RenderControl(HTextWriter)
            CRetour.ContenuHtml = HTextWriter.InnerWriter.ToString()
        Catch ex As Exception
            CRetour.EstOk = False
        End Try

        Dim SContexte As String = ""
        SContexte = "Année : " & WsCtrlGestion.VCache.Annee & "; "

        If WsCtrlGestion.VCache.Plan = "" Then
            SContexte = SContexte & "Plan de formation : Tous; "
        Else
            SContexte = SContexte & "Plan de formation : " & WsCtrlGestion.VCache.Plan & "; "
        End If
        If WsCtrlGestion.VCache.Etablissement = "" Then
            SContexte = SContexte & "Etablissement : Tous"
        Else
            SContexte = SContexte & "Etablissement : " & WsCtrlGestion.VCache.Etablissement
        End If

        Dim FormatContexte As String = "<tr><td align=""center""><span id=""PrincipalWsCtrlGestion_CtrlGestTableau_CtrlVue_CtrlChiffresBruts_EtiContexte"" style=""display:inline-block;color:#B0E0D7;background-color:#124545;border-color:#B0E0D7;border-width:2px;border-style:solid;width:600px;font-style: oblique; text-indent: 5px; text-align: center;"">{0}</span></td></tr>"""
        Dim Ssplit As String() = CRetour.ContenuHtml.Split(New String() {"</tr>"}, StringSplitOptions.None)
        Dim Contenu_Html As String = ""
        Dim SiPremier As Boolean = True

        For Each Chaine In Ssplit
            If Contenu_Html = "" Then
                Contenu_Html &= Chaine
            Else
                Contenu_Html &= "</tr>" & Chaine
            End If
            If SiPremier = True Then
                SiPremier = False
                Contenu_Html &= String.Format(FormatContexte, Server.HtmlEncode(SContexte))
            End If
        Next
        CRetour.ContenuHtml = Contenu_Html
        Return CRetour
    End Function

    Public Function GetExportPagine() As List(Of String)
        Dim LstCodeHtml As List(Of String) = New List(Of String)()
        Dim LstRangees As DetailRowCollection = GetCollectionRowInfo()
        Dim SiContinuer = True
        Dim Nb_Skip As Integer = 0
        Dim Conteneur As Table
        Dim SiPremier As Boolean = True

        While SiContinuer = True
            Conteneur = New Table()
            If SiPremier = True Then
                SiContinuer = LstRangees.Pagine(Me, Nb_Skip, CNB_ENREG - 5, Conteneur, ConstruitLigneEntete())
                SiPremier = False
            Else
                SiContinuer = LstRangees.Pagine(Me, Nb_Skip, CNB_ENREG, Conteneur, ConstruitLigneEntete())
            End If
            LstCodeHtml.Add(Conteneur.GetSourceHtml())
        End While
        Return LstCodeHtml
    End Function

    Public Class DetailRowCollection
        Inherits List(Of DetailRow)

        Public Shared Sub RempliTable(ByVal Cadre As Table, ByVal RangeeEntete As TableRow, ByVal Rangees As IEnumerable(Of DetailRow))
            Cadre.Rows.Clear()
            If RangeeEntete IsNot Nothing Then
                Cadre.Rows.Add(RangeeEntete)
            End If
            Rangees.ToList().ForEach(Sub(dr)
                                         Cadre.Rows.Add(dr.RT)
                                     End Sub)
        End Sub

        Public Function Pagine(ByVal Ctrl As CtrlTableau, ByRef Nb_Skip As Integer, ByVal Nb_Take As Integer, ByRef Conteneur As Table, _
                               ByVal RangeeEntete As TableRow) As Boolean

            Dim LstDetails As List(Of DetailRow) = Me.Skip(Nb_Skip).Take(Nb_Take + 1).ToList()

            Conteneur.Width = Unit.Pixel(750)
            Conteneur.CellPadding = 0
            Conteneur.CellSpacing = 0
            Conteneur.ForeColor = Color.Black
            Conteneur.Font.Size = FontUnit.Parse("90%")
            Conteneur.Rows.Add(RangeeEntete)

            Dim LstRowsTmp As List(Of DetailRow) = New List(Of DetailRow)()

            Dim Cpt As Integer = 0
            Dim Idx As Integer = 0

            For Each dr In LstDetails
                If Cpt = 0 Then
                    If dr.EstLigneDonnee = True Then
                        'On recherche la ligne de regroupement directement avant
                        Idx = Me.IndexOf(dr)

                        Dim DREntete = (From it In Me Where it.SiEstLigneEnteteGroupe And Me.IndexOf(it) < Idx Order By Me.IndexOf(it) Descending Select INDEX = Me.IndexOf(it), R = it.RT).First()

                        Conteneur.Rows.Add(DetailRow.CloneRow(DREntete.R))

                        'On recherche une ligne de regroupment si elle existe
                        Dim DRGroup = (From it In Me Where it.SiEstLigneRegroupement And Me.IndexOf(it) < Idx Order By Me.IndexOf(it) Descending Select INDEX = Me.IndexOf(it), R = it.RT).FirstOrDefault()
                        If (Not (DRGroup Is Nothing) AndAlso (DRGroup.INDEX > DREntete.INDEX)) Then
                            Conteneur.Rows.Add(DetailRow.CloneRow(DRGroup.R))
                        End If

                        Cpt += 1
                        LstRowsTmp.Add(dr)
                        Continue For
                    End If

                    If dr.SiEstLigneRegroupement = True Then
                        Idx = Me.IndexOf(dr)

                        Dim DREntete = (From it In Me Where it.SiEstLigneEnteteGroupe And Me.IndexOf(it) < Idx Order By Me.IndexOf(it) Descending Select INDEX = Me.IndexOf(it), R = it.RT).First()
                        Conteneur.Rows.Add(DetailRow.CloneRow(DREntete.R))

                        Cpt += 1
                        LstRowsTmp.Add(dr)
                        Continue For

                    End If
                End If
                Cpt += 1
                LstRowsTmp.Add(dr)
            Next
            If LstRowsTmp.Count <= Nb_Take Then
                'Il n'y a pas de page suivante
                Nb_Skip = 0
                For Each dr In LstRowsTmp
                    Conteneur.Rows.Add(dr.RT)
                Next
                Return False
            End If
            'il y a forcement une page suivante

            If LstRowsTmp(Nb_Take - 1).SiEstLigneEnteteGroupe = True Then
                'On ne la prend pas
                Nb_Skip = Nb_Skip + Nb_Take - 1
                For Each dr In LstRowsTmp.Take(Nb_Take - 1)
                    Conteneur.Rows.Add(dr.RT)
                Next
                Return True
            End If

            If LstRowsTmp(Nb_Take - 1).SiEstLigneRegroupement = True Then
                Idx = Me.IndexOf(LstRowsTmp(Nb_Take - 1))

                'On recherche la ligne d'entete directement avant dans la collection
                Dim idxEntete As Integer = (From it In Me Where it.SiEstLigneEnteteGroupe And Me.IndexOf(it) < Idx Order By Me.IndexOf(it) Descending Select Me.IndexOf(it)).First()

                'Est ce que l'index est directement inferieure a l'index de la row
                If (Idx = idxEntete + 1) Then
                    'On est dans le cas, ligne d'entete Niveau, ligne de regroupement niveau 1
                    Nb_Skip = Nb_Skip + Nb_Take - 2
                    For Each dr In LstRowsTmp.Take(Nb_Take - 2)
                        Conteneur.Rows.Add(dr.RT)
                    Next
                    Return True
                End If

                'La ligne directement avant est une ligne de donnees
                'On ne met pas la ligne de regroupement
                Nb_Skip = Nb_Skip + Nb_Take - 1
                For Each dr In LstRowsTmp.Take(Nb_Take - 1)
                    Conteneur.Rows.Add(dr.RT)
                Next
                Return True

            End If

            'On est forcement sur une ligne de donnees
            Nb_Skip = Nb_Skip + Nb_Take
            For Each dr In LstRowsTmp.Take(Nb_Take)
                Conteneur.Rows.Add(dr.RT)
            Next

            Return True

        End Function
    End Class

    Public Class DetailRow
        Public Property SiEstLigneEnteteGroupe As Boolean = False
        Public Property SiEstLigneRegroupement As Boolean = False

        Public Property RT As TableRow

        Public ReadOnly Property EstLigneDonnee As Boolean
            Get
                Return Not SiEstLigneEnteteGroupe And Not SiEstLigneRegroupement
            End Get
        End Property


        Public Shared Function CloneRow(R As TableRow) As TableRow
            Dim tb As TableRow = New TableRow()

            tb.BackColor = R.BackColor
            tb.ForeColor = R.ForeColor
            tb.Font.Bold = R.Font.Bold
            tb.Font.Italic = R.Font.Italic

            Dim ccl As TableCell
            Dim lblcl As Label
            Dim btncl As Button

            Dim lbl As Label
            Dim btn As Button
            Dim vst As String

            For Each c As TableCell In R.Cells
                ccl = New TableCell() With {.BorderColor = Color.Black, .BorderWidth = New Unit("1px")}
                ccl.BorderStyle = c.BorderStyle
                ccl.ColumnSpan = c.ColumnSpan
                ccl.HorizontalAlign = c.HorizontalAlign

                If (c.HasControls()) Then

                    lbl = TryCast(c.Controls(0), Label)

                    If Not (lbl Is Nothing) Then
                        lblcl = New Label()
                        lblcl.Width = lbl.Width
                        lblcl.BorderStyle = BorderStyle.None
                        lblcl.BorderWidth = Unit.Pixel(0)
                        lblcl.Text = lbl.Text

                        'For Each k As String In lbl.Style.Keys
                        '    lblcl.Style.Add(k, lbl.Style(k))
                        'Next


                        vst = lbl.Style(HtmlTextWriterStyle.TextAlign)
                        If Not (String.IsNullOrEmpty(vst)) Then
                            lblcl.Style.Add(HtmlTextWriterStyle.TextAlign, vst)
                        End If
                        vst = lbl.Style(HtmlTextWriterStyle.MarginLeft)
                        If Not (String.IsNullOrEmpty(vst)) Then
                            lblcl.Style.Add(HtmlTextWriterStyle.MarginLeft, vst)
                        End If
                        vst = lbl.Style(HtmlTextWriterStyle.MarginRight)
                        If Not (String.IsNullOrEmpty(vst)) Then
                            lblcl.Style.Add(HtmlTextWriterStyle.MarginRight, vst)
                        End If

                        ccl.Controls.Add(lblcl)

                        tb.Cells.Add(ccl)
                        Continue For
                    End If

                    btn = TryCast(c.Controls(0), Button)
                    If Not (btn Is Nothing) Then

                        btncl = New Button()
                        btncl.BackColor = Color.Transparent
                        btncl.BorderStyle = BorderStyle.None
                        btncl.Text = btn.Text

                        ccl.Controls.Add(btncl)

                        tb.Cells.Add(ccl)
                        Continue For
                    End If

                End If

                tb.Cells.Add(ccl)
            Next

            Return tb

        End Function
    End Class

End Class