﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlExportGraphique1
    Inherits System.Web.UI.UserControl

    Public Sub Charge(ByVal Numpage As Integer, ByVal ImagesGraphiques As List(Of ImageExportInfo))
        lblNumPage.Text = "Virtualia.net Page " & Numpage
        Dim Query = From ig In (From i In ImagesGraphiques _
                                    Where i.Groupe = "STATUT" OrElse i.Groupe = "DOMAINE" _
                                    Select i) _
                    Group ig By key = ig.Groupe Into Group _
                    Select CLEF = key, IMGS = Group

        For Each odyn In Query
            Select Case odyn.CLEF
                Case "STATUT"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_Statut_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               Statut_Cout.Visible = False
                                                               Return
                                                           End If
                                                           Statut_Cout.Visible = True
                                                           IMG_Statut_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_Statut_Jour.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)

                Case "DOMAINE"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_Domaine_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               Domaine_Cout.Visible = False
                                                               Return
                                                           End If
                                                           Domaine_Cout.Visible = True
                                                           IMG_Domaine_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_Domaine_Jour.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)
            End Select
        Next
    End Sub
End Class