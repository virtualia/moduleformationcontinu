﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlExportEntete.ascx.vb" Inherits="Virtualia.Net.CtrlExportEntete" %>

<asp:Table ID="TB1" runat="server" Width="768px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="White">

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="BILAN DE FORMATION" Height="22px" Width="600px" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="table1" runat="server" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="lblAnnee" runat="server" Text="Année : 2010" Height="22px" Width="600px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: left; margin-left: 10px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="lblplan" runat="server" Text="Plan de formation : tous" Height="22px" Width="600px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: left; margin-left: 10px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="lblEtab" runat="server" Text="Etablissement : tous" Height="22px" Width="600px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: left; margin-left: 10px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" >
            <asp:Label ID="EtiChiffre" runat="server" Text="Totaux" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Bottom">
        <asp:TableCell HorizontalAlign="Right">
            <asp:Label ID="EtiNoPage" runat="server" Text="Virtualia.net Page 1" Width="500px" BorderStyle="None" ForeColor="LightGray" Style="font-style: oblique; text-indent: 5px; text-align: right; margin-right: 10px" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
