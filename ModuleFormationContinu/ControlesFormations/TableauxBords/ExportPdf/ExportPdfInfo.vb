﻿
Public Interface ICtrlExport
    Function GetExport() As ExportPdfInfo
End Interface

Public Class ExportPdfInfo
    Public Property EstOk As Boolean = True
    Public Property NomFichier As String = ""
    Public Property ContenuHtml As String = ""
End Class
