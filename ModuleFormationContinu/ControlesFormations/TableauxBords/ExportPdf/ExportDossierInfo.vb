﻿Imports System

<Serializable>
Public Class ExportDossierInfo
    Public Property Annee As String = ""
    Public Property PlanFormation As String = "Tous"
    Public Property Etablissement As String = "Tous"

    Public Property SHtmlChiffresBruts As String = ""
    Public Property SHtmlTableaux As List(Of String) = New List(Of String)()

    Public Property ImagesGraphiques As List(Of ImageExportInfo) = New List(Of ImageExportInfo)()
End Class

<Serializable>
Public Class ImageExportInfo
    Public Property Groupe As String = ""
    Public Property TypeCalcul As String = ""
    Public Property UrlImg As String = ""
End Class


