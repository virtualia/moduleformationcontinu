﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlExportGraphique.ascx.vb" Inherits="Virtualia.Net.CtrlExportGraphique" %>

<asp:Table ID="TB1" runat="server" Width="768px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="White">

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="GRAPHIQUES" Height="22px" Width="600px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TB_SEXE" runat="server" HorizontalAlign="Center" Width="980px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="Label1" runat="server" Text="Répartition par sexe" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                        <asp:Table ID="TB5" runat="server">
                            <asp:TableRow>
                                <asp:TableCell ID="Sexe_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Sexe_NB" runat="server" Height="250px" Width="250px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                </asp:TableCell>
                                <asp:TableCell ID="Sexe_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Sexe_Cout" runat="server" Height="250px" Width="250px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                </asp:TableCell>
                                <asp:TableCell ID="Sexe_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Sexe_Jour" runat="server" Height="250px" Width="250px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell ID="Sexe_Effectif" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black" HorizontalAlign="Center">
                        <asp:Image ID="IMG_Sexe_Effectif" runat="server" Height="180px" Width="180px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="TB_CATEGORIE" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="Label2" runat="server" Text="Répartition par catégorie" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table1" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell ID="Categorie_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Categorie_NB" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                                </asp:TableCell>
                                <asp:TableCell ID="Categorie_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Categorie_Cout" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                               </asp:TableCell>
                               <asp:TableCell ID="Categorie_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Categorie_Jour" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg" />
                               </asp:TableCell>
                            </asp:TableRow>

                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Bottom">
        <asp:TableCell HorizontalAlign="Right">
            <asp:Label ID="EtiNumPage" runat="server" Text="Virtualia.net Page " Width="500px" BackColor="White" BorderStyle="None" ForeColor="LightGray" Style="font-style: oblique; text-indent: 5px; text-align: right; margin-right: 10px" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
