﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Structure.Formation
Imports System.Threading.Tasks

Public Class CtrlDossierExport
    Inherits UserControl
    Implements ICtrlExport

    Private Property VCache As ExportDossierInfo
        Get
            If Not (ViewState.KeyExiste("VCache")) Then
                ViewState.AjouteValeur("VCache", New ExportDossierInfo())
            End If
            Return ViewState.GetValeur(Of ExportDossierInfo)("VCache")
        End Get
        Set(value As ExportDossierInfo)
            ViewState.AjouteValeur("VCache", value)
        End Set
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        If VCache.Annee = "" Then
            Return
        End If
        Dim LstTaches As List(Of Task) = New List(Of Task)()
        LstTaches.Add(Task.Factory.StartNew(Sub()
                                                CtrlEntete.Charge(VCache)
                                            End Sub))

        LstTaches.Add(Task.Factory.StartNew(Sub()
                                                CadreExportTableau.Rows.Clear()
                                                Dim Numpage As Integer = 1
                                                Dim Rangee As TableRow
                                                Dim Cellule As TableCell
                                                Dim ctdon As CtrlExportDonnee

                                                For Each shtml In VCache.SHtmlTableaux
                                                    Cellule = New TableCell() With {.HorizontalAlign = HorizontalAlign.Center}

                                                    ctdon = DirectCast(LoadControl("~/ControlesFormations/TableauxBords/ExportPdf/CtrlExportDonnee.ascx"), CtrlExportDonnee)
                                                    ctdon.Charge(Numpage + 1, shtml, (Numpage = 1))
                                                    Cellule.Controls.Add(ctdon)

                                                    Rangee = New TableRow()
                                                    Rangee.Cells.Add(Cellule)

                                                    CadreExportTableau.Rows.Add(Rangee)

                                                    Numpage += 1

                                                    Rangee = New TableRow()
                                                    Cellule = New WebControls.TableCell() With {.Height = Unit.Pixel(15)}
                                                    Rangee.Cells.Add(Cellule)

                                                    CadreExportTableau.Rows.Add(Rangee)

                                                Next

                                                CtrlGraph.Charge(Numpage + 1, VCache.ImagesGraphiques)
                                                CtrlGraph1.Charge(Numpage + 2, VCache.ImagesGraphiques)
                                                CtrlGraph2.Charge(Numpage + 3, VCache.ImagesGraphiques)

                                            End Sub))

        Task.WaitAll(LstTaches.ToArray())

    End Sub

    Public Sub Charge(ByVal Infos As ExportDossierInfo, ByVal CtlGestion As CtrlGestionTableauBord)
        VCache = Infos
        CtrlBrut.CtrlGestion = CtlGestion
        OnLoad(EventArgs.Empty)
    End Sub

    Public Sub Vider()
        CadreExportTableau.Rows.Clear()
    End Sub

    Public Function GetExport() As ExportPdfInfo Implements ICtrlExport.GetExport
        Dim CRetour As ExportPdfInfo = New ExportPdfInfo() With {.NomFichier = "TableauDeBords", .ContenuHtml = TbGenerale.GetSourceHtml()}
        If CRetour.ContenuHtml = "" Then
            CRetour.EstOk = False
        End If
        Return CRetour
    End Function
End Class