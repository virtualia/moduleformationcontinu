﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlDossierExport.ascx.vb" Inherits="Virtualia.Net.CtrlDossierExport" %>

<%@ Register Src="~/ControlesFormations/TableauxBords/ExportPdf/CtrlExportEntete.ascx" TagName="CTL_ENTETE" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/ExportPdf/CtrlExportBrut.ascx" TagName="CTL_BRUT" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/ExportPdf/CtrlExportGraphique.ascx" TagName="CTL_GRAPHIQUE" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/ExportPdf/CtrlExportGraphique1.ascx" TagName="CTL_GRAPHIQUE1" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/TableauxBords/ExportPdf/CtrlExportGraphique2.ascx" TagName="CTL_GRAPHIQUE2" TagPrefix="Formation" %>

<asp:Table ID="TbGenerale" runat="server" Width="1005px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Formation:CTL_ENTETE ID="CtrlEntete" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Formation:CTL_BRUT ID="CtrlBrut" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreExportTableau" runat="server">
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Formation:CTL_GRAPHIQUE ID="CtrlGraph" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Formation:CTL_GRAPHIQUE1 ID="CtrlGraph1" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <Formation:CTL_GRAPHIQUE2 ID="CtrlGraph2" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>


