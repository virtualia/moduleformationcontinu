﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Structure.Formation
Public Class CtrlExportDonnee
    Inherits UserControl
    Public Sub Charge(ByVal Numpage As Integer, ByVal Shtml As String, ByVal SiAvecTitre As Boolean)
        rTitre.Visible = SiAvecTitre
        Dim ct As Control = Me.InstancieControl(Shtml)
        CellDonnee.Controls.Clear()
        CellDonnee.Controls.Add(ct)
        EtiNumPage.Text = "Virtualia.net Page " & Numpage
    End Sub

End Class