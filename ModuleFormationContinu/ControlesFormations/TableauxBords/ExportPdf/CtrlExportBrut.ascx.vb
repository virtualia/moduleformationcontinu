﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlExportBrut
    Inherits System.Web.UI.UserControl
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtrlGestion As CtrlGestionTableauBord

    Public WriteOnly Property CtrlGestion As CtrlGestionTableauBord
        Set(value As CtrlGestionTableauBord)
            WsCtrlGestion = value
            EtiEffectif.Text = "Effectif au 31/12/" & WsCtrlGestion.VCache.Annee

            Dim stats As Virtualia.Metier.Formation.StatistiqueFormationInfoCollection = WsCtrlGestion.VCache.Statistiques

            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Tot_Nb_Action_Formation.Text = WebFct.ViRhFonction.MontantEdite(stats.NB_ActionFormation, 0, True)
            Tot_Nb_Benef.Text = WebFct.ViRhFonction.MontantEdite(stats.NB_Beneficiaire, 0, True)
            Tot_Nb_Stagiaire.Text = WebFct.ViRhFonction.MontantEdite(stats.NB_Stagiaire, 0, True)
            Tot_Nb_Effectif.Text = WebFct.ViRhFonction.MontantEdite(stats.Effectif, 0, True)

            Tot_Nb_Heure.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Durees_Global_H * 60), "", 2)
            Tot_Nb_Jour.Text = WebFct.ViRhFonction.MontantEdite(stats.Durees_Global_J, 2, True)

            Duree_Moyenne_Action_Formation_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Action_Formation_H * 60), "", 2)
            Duree_Moyenne_Beneficiaire_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Beneficiaire_H * 60), "", 2)
            Duree_Moyenne_Stagiaire_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Stagiaire_H * 60), "", 2)
            Duree_Moyenne_Effectif_h.Text = WebFct.ViRhDates.CalcHeure(CStr(stats.Duree_Moyenne_Effectif_H * 60), "", 2)

            Duree_Moyenne_Action_Formation_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Action_Formation_J, 1, True)
            Duree_Moyenne_Beneficiaire_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Beneficiaire_J, 1, True)
            Duree_Moyenne_Stagiaire_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Stagiaire_J, 1, True)
            Duree_Moyenne_Effectif_j.Text = WebFct.ViRhFonction.MontantEdite(stats.Duree_Moyenne_Effectif_J, 1, True)

            Cout_Tot.Text = WebFct.ViRhFonction.MontantEdite(stats.Couts_Global, 2, True)
            Cout_Pedagogique.Text = WebFct.ViRhFonction.MontantEdite(stats.Couts_Pedagogiques, 2, True)
            Cout_Frais_Mission.Text = WebFct.ViRhFonction.MontantEdite(stats.Couts_Formations, 2, True)
            Cout_Masse_Salarial.Text = ""

            Cout_Moyen_Action_Formation.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Action_Formation, 2, True)
            Cout_Moyen_Beneficiaire.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Beneficiaire, 2, True)
            Cout_Moyen_Stagiaires.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Stagiaire, 2, True)
            Cout_Moyen_Effectif.Text = WebFct.ViRhFonction.MontantEdite(stats.Cout_Moyen_Effectif, 2, True)

            If stats.Effectif > 0 Then
                Pourcent_Effectif_Forme.Text = Math.Round((stats.NB_Beneficiaire / stats.Effectif) * 100, 2) & " %"
            Else
                Pourcent_Effectif_Forme.Text = ""
            End If
            If stats.Effectif > 0 Then
                Pourcent_Effectif_Non_Forme.Text = Math.Round((stats.NB_Non_Forme / stats.Effectif) * 100, 2) & " %"
            Else
                Pourcent_Effectif_Non_Forme.Text = ""
            End If
        End Set
    End Property

    Public Function GetSourceHtml() As String
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        Return WebFct.VirSourceHtml(CadreChiffres)
    End Function

End Class