﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlExportGraphique
    Inherits UserControl

    Public Sub Charge(ByVal Numpage As Integer, ByVal ImagesGraphiques As List(Of ImageExportInfo))
        EtiNumPage.Text = "Virtualia.net Page " & Numpage
        Dim Query = From ig In (From i In ImagesGraphiques _
                                    Where i.Groupe = "SEXE" OrElse i.Groupe = "CATEGORIE" _
                                    Select i) _
                    Group ig By key = ig.Groupe Into Group _
                    Select CLEF = key, IMGS = Group

        For Each odyn In Query
            Select Case odyn.CLEF
                Case "SEXE"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_Sexe_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               Sexe_Cout.Visible = False
                                                               Return
                                                           End If
                                                           Sexe_Cout.Visible = True
                                                           IMG_Sexe_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_Sexe_Jour.ImageUrl = ig.UrlImg
                                                       Case "EFFECTIF"
                                                           IMG_Sexe_Effectif.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)

                Case "CATEGORIE"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_Categorie_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               Categorie_Cout.Visible = False
                                                               Return
                                                           End If
                                                           Categorie_Cout.Visible = True
                                                           IMG_Categorie_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_Categorie_Jour.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)
            End Select
        Next
    End Sub
End Class