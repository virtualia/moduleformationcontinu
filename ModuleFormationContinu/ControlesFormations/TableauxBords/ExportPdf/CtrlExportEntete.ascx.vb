﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlExportEntete
    Inherits UserControl

    Public Sub Charge(ByVal Infos As ExportDossierInfo)
        lblAnnee.Text = "Année : " & Infos.Annee
        lblplan.Text = "Plan de formation : " & Infos.PlanFormation
        lblEtab.Text = "Etablissement : " & Infos.Etablissement
    End Sub

End Class