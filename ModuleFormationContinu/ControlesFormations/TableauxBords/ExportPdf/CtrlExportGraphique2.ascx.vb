﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlExportGraphique2
    Inherits System.Web.UI.UserControl

    Public Sub Charge(ByVal Numpage As Integer, ByVal ImagesGraphiques As List(Of ImageExportInfo))
        lblNumPage.Text = "Virtualia.net Page " & Numpage

        Dim Query = From ig In (From i In ImagesGraphiques _
                                    Where i.Groupe = "THEME" OrElse i.Groupe = "TYPEACTION" OrElse i.Groupe = "NIVEAU1" _
                                    Select i) _
                    Group ig By key = ig.Groupe Into Group _
                    Select CLEF = key, IMGS = Group

        For Each odyn In Query
            Select Case odyn.CLEF
                Case "THEME"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_Theme_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               Theme_Cout.Visible = False
                                                               Return
                                                           End If
                                                           Theme_Cout.Visible = True
                                                           IMG_Theme_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_Theme_Jour.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)

                Case "TYPEACTION"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_TypeAction_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               TypeAction_Cout.Visible = False
                                                               Return
                                                           End If
                                                           TypeAction_Cout.Visible = True
                                                           IMG_TypeAction_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_TypeAction_Jour.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)

                Case "NIVEAU1"
                    odyn.IMGS.ToList().ForEach(Sub(ig)
                                                   Select Case ig.TypeCalcul
                                                       Case "NB"
                                                           IMG_Niveau1_NB.ImageUrl = ig.UrlImg
                                                       Case "COUT"
                                                           If (ig.UrlImg = "") Then
                                                               Niveau1_Cout.Visible = False
                                                               Return
                                                           End If
                                                           Niveau1_Cout.Visible = True
                                                           IMG_Niveau1_Cout.ImageUrl = ig.UrlImg
                                                       Case "JOUR"
                                                           IMG_Niveau1_Jour.ImageUrl = ig.UrlImg
                                                   End Select
                                               End Sub)
            End Select
        Next
    End Sub

End Class