﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Systeme.Evenements

Public Class CtrlDetailStage
    Inherits UserControl
    Implements IControlBase
    '**RP
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    '**
    <Serializable>
    Public Class CacheDetailStage
        Public Const KeyState As String = "CacheDetailStage"
        Public Property ItemStage As CacheSelectionArmoire
        Public Property EstNouveauStage As Boolean = False

        Public Sub Sauve(vs As StateBag)
            vs.AjouteValeur(KeyState, Me)
        End Sub
    End Class

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private Sub ActItemRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        If e Is Nothing Then
            Exit Sub
        End If
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("STAGE", 0, "", "")
        ReponseRetour(Evenement)
    End Sub

    Public ReadOnly Property V_Cache As CacheDetailStage
        Get
            If Not (ViewState.KeyExiste(CacheDetailStage.KeyState)) Then
                ViewState.AjouteValeur(CacheDetailStage.KeyState, New CacheDetailStage())
            End If
            Return ViewState.GetValeur(Of CacheDetailStage)(CacheDetailStage.KeyState)
        End Get
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Private ReadOnly Property Act_ClickMenu As System.Action(Of CacheClickMenu)
        Get
            Return Sub(clk As CacheClickMenu)
                       If V_Cache.ItemStage.Ide <= 0 Then
                           If CInt(clk.Value) > 0 And CtrlFormation.V_Identifiant = 0 Then
                               Return
                           Else
                               V_Cache.ItemStage.Ide = CtrlFormation.V_Identifiant
                               V_Cache.Sauve(ViewState)
                           End If
                       End If
                       Select Case clk.Value
                           Case "0"
                               MultiOnglets.ActiveViewIndex = 0
                               CtrlFormation.V_Identifiant = V_Cache.ItemStage.Ide
                           Case "1"
                               MultiOnglets.ActiveViewIndex = 1
                               CtrlFormation.V_Identifiant = 0
                               CtrlGestStage.Charge(V_Cache.ItemStage)
                           Case "2"
                               MultiOnglets.ActiveViewIndex = 2
                               CtrlListInsc.Charge(V_Cache.ItemStage)
                           Case "3"
                               MultiOnglets.ActiveViewIndex = 3
                               CtrlMultiInsc.Charge(V_Cache.ItemStage)
                           Case "4"
                               MultiOnglets.ActiveViewIndex = 4
                               CtrlValidation.V_Identifiant = V_Cache.ItemStage.Ide
                       End Select
                   End Sub
        End Get
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        If IsPostBack = False Then
            Call InitMenus()
            Exit Sub
        End If

        CtrlMenus.Act_ClickMenu = Act_ClickMenu
        CtrlMenus.EndInit()

        CtrlGestStage.ControllerMultiVue = DirectCast(Page, FrmPlanDeFormation)
        CtrlGestStage.CtrlDetail = Me

        CtrlListInsc.ControllerMultiVue = DirectCast(Page, FrmPlanDeFormation)
        CtrlListInsc.Gestion = DirectCast(Page, FrmPlanDeFormation).MoteurInscription

        CtrlMultiInsc.ControllerMultiVue = DirectCast(Page, FrmPlanDeFormation)
        CtrlMultiInsc.Gestion = DirectCast(Page, FrmPlanDeFormation).MoteurInscription

        CtrlFormation.Act_Retour = AddressOf ActItemRetour
    End Sub

    Public Sub Charge(ByVal Items As Object) Implements IControlBase.Charge
        CellMenus.Visible = True
        Dim ccm As CacheClickMenu = New CacheClickMenu()
        ccm.TypeClick = TypeClickMenu.VUE
        ccm.TypeSource = TypeSourceMenu.V_Menu

        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        V_Cache.ItemStage = DirectCast(Items, CacheSelectionArmoire)
        V_Cache.Sauve(ViewState)

        If V_Cache.ItemStage.Ide <= 0 Then
            CtrlMenus.SelectionneBouton("0")
            MultiOnglets.ActiveViewIndex = 0
            CtrlFormation.V_Identifiant = 0 'V_Cache.Item.Ide
            Return
        End If
        '**RP ***
        If WebFct.PointeurUtilisateur.Etablissement <> "" Then
            Dim Ensemble As Virtualia.Metier.Formation.EnsembledeStages
            Dim Dossier As Virtualia.Metier.Formation.DossierStage
            Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsembleFOR
            Ensemble.Identifiant = V_Cache.ItemStage.Ide
            Dossier = Ensemble.ItemDossier(V_Cache.ItemStage.Ide)
            If Dossier.Etablissement_Gestionnaire <> WebFct.PointeurUtilisateur.Etablissement Then
                CtrlMenus.SupprimerMenu("1")
                CtrlMenus.SupprimerMenu("3")
                CtrlFormation.V_SiEnLectureSeule = True
                V_Cache.ItemStage.SiEnLecture = True
                V_Cache.Sauve(ViewState)
            ElseIf CtrlMenus.NombreMenus < 3 Then
                Call InitMenus()
                CtrlFormation.V_SiEnLectureSeule = False
                V_Cache.ItemStage.SiEnLecture = False
                V_Cache.Sauve(ViewState)
            End If
        End If
        '**
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).SiAccesAuModuleSeul = True Then
            ccm.Value = "4"
            CtrlMenus.SelectionneBouton("4")
        ElseIf Virtualia.Metier.Formation.FormationHelper.GetForInscriptionsDeStage(V_Cache.ItemStage.Ide).Count > 0 Then
            ccm.Value = "2"
            CtrlMenus.SelectionneBouton("2")
        Else
            ccm.Value = "0"
            CtrlMenus.SelectionneBouton("0")
        End If
        Act_ClickMenu()(ccm)
    End Sub

    Private Sub InitMenus()
        Dim LstMenus As List(Of MenuItem) = New List(Of MenuItem)

        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        LstMenus.Add(New MenuItem() With {.Text = "Détails du stage", .ToolTip = "Visualisation du stage", .Value = "0"})
        LstMenus.Add(New MenuItem() With {.Text = "Procédure d'inscription", .ToolTip = "Assistant d'inscription", .Value = "3"})
        LstMenus.Add(New MenuItem() With {.Text = "Tableau des inscriptions", .ToolTip = "Situation des inscriptions", .Value = "2"})

        Try
            If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).SiAccesAuModuleSeul = True Then
                LstMenus.Add(New MenuItem() With {.Text = "Validation des demandes", .ToolTip = "Validation des demandes de stage ayant reçu un avis favorable du supérieur hiérachique", .Value = "4"})
            Else
                LstMenus.Add(New MenuItem() With {.Text = "Gestion du stage", .ToolTip = "Utilitaires de gestion du stage", .Value = "1"})
            End If
        Catch ex As Exception
            Exit Try
        End Try

        CtrlMenus.Charge(LstMenus)
    End Sub

    Public Sub CacheOuMontreMenu(estvisible As Boolean)
        CellMenus.Visible = estvisible
    End Sub
    '** RP
    Private Sub CtrlRP_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles CtrlFormation.ValeurRetour, CtrlValidation.ValeurRetour
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("STAGE", 0, "", "")
        ReponseRetour(Evenement)
    End Sub
    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        MultiOnglets.SetActiveView(VueValidation)
    End Sub
    Private Sub CtrlValidation_MessageDialogue(sender As Object, e As MessageSaisieEventArgs) Handles CtrlValidation.MessageDialogue
        MsgVirtualia.AfficherMessage = e
        MultiOnglets.SetActiveView(VueMessage)
    End Sub
    Private Sub CtrlList_Dossier_Click(sender As Object, e As DossierClickEventArgs) Handles CtrlListInsc.Dossier_Click, CtrlMultiInsc.Dossier_Click
        If CtrlFormation.V_SiEnLectureSeule = True Then
            Exit Sub
        End If
        CtrlIndividuel.V_Identifiant = e.Identifiant
        CtrlIndividuel.V_Emetteur = e.Valeur
        CellMenus.Visible = False
        MultiOnglets.SetActiveView(VueDossier)
    End Sub
    Private Sub CtrlIndividuel_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles CtrlIndividuel.ValeurRetour
        CellMenus.Visible = True
        Select Case e.Emetteur
            Case "Tableau"
                MultiOnglets.SetActiveView(VueGestionInscription)
            Case Else
                MultiOnglets.SetActiveView(VueGestionMultiInsc)
        End Select
    End Sub

    '**
End Class