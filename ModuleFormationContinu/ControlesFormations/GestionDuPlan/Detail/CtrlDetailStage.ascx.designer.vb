﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtrlDetailStage
    
    '''<summary>
    '''Contrôle CadreVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreVues As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellMenus.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMenus As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreMenus.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreMenus As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CtrlMenus.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlMenus As Global.Virtualia.Net.MenuBoutons
    
    '''<summary>
    '''Contrôle ConteneurVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ConteneurVues As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle MultiOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiOnglets As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle VueDetailStage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDetailStage As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlFormation As Global.Virtualia.Net.CtlPvueFOR
    
    '''<summary>
    '''Contrôle VueGestionStage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueGestionStage As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlGestStage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlGestStage As Global.Virtualia.Net.CtrlGestionStage
    
    '''<summary>
    '''Contrôle VueGestionInscription.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueGestionInscription As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlListInsc.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlListInsc As Global.Virtualia.Net.CtrlGestionListeInscription
    
    '''<summary>
    '''Contrôle VueGestionMultiInsc.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueGestionMultiInsc As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlMultiInsc.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlMultiInsc As Global.Virtualia.Net.CtrlGestionMultiInscription
    
    '''<summary>
    '''Contrôle VueValidation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueValidation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlValidation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlValidation As Global.Virtualia.Net.CtlValidationDemandes
    
    '''<summary>
    '''Contrôle VueMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMessage As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle MsgVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgVirtualia As Global.Virtualia.Net.Controles_VMessage
    
    '''<summary>
    '''Contrôle VueDossier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDossier As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlIndividuel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlIndividuel As Global.Virtualia.Net.CtlSituationW
End Class
