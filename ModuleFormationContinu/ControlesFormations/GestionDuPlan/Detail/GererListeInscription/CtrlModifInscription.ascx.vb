﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Virtualia.Structure.Formation
Imports System.Drawing

Public Class CtrlModifInscription
    Inherits UserControl
    Implements IControlBase
    Private WsCtrlGestion As CtrlGestionListeInscription
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '**
    Private ReadOnly Property ObjGlob As Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase
        Get
            Return DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase)
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionListeInscription
        Set(value As CtrlGestionListeInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Private Property Index As Integer
        Get
            If (Not ViewState.KeyExiste("Index")) Then
                ViewState.AjouteValeur("Index", -1)
            End If
            Return DirectCast(ViewState("Index"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("Index", value)
        End Set
    End Property

    Private Property IndexCourant As Integer
        Get
            If (Not ViewState.KeyExiste("IndexCourant")) Then
                ViewState.AjouteValeur("IndexCourant", -1)
            End If
            Return DirectCast(ViewState("IndexCourant"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexCourant", value)
        End Set
    End Property

    Private Property TypeModif As String
        Get
            If (Not ViewState.KeyExiste("TypeModif")) Then
                ViewState.AjouteValeur("TypeModif", "")
            End If

            Return DirectCast(ViewState("TypeModif"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("TypeModif", value)
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Private ReadOnly Property Moteur As GestionInscription
        Get
            Return DirectCast(Page, FrmPlanDeFormation).MoteurInscription
        End Get
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        AddHandler txtValeur.ValeurChange, Sub(s, ev)
                                               BtnTrait.Enabled = (ev.Valeur.Trim() <> "")
                                           End Sub

        BtnTrait.Act = Sub(btn)
                           TbMessage.Visible = True
                           BtnTrait.Enabled = False
                           cboValeur.Enabled = False
                           txtValeur.V_SiEnLectureSeule = True

                           If Not (VerifieSiActionPossible()) Then
                               TxtStatut.Text = "Modification impossible"
                               txtMsg.Text = "Cette modification entraine une répartition des coûts incohérente"
                               Return
                           End If
                           TimerTraitement.Enabled = True

                           DirectCast(Page.Master, VirtualiaMain).SessionCourante.ReinitListeArbo()
                       End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        If Not (TimerTraitement.Enabled) Then
            Return
        End If
        If IndexCourant = Index Then
            Return
        End If
        Dim NbTotal As Integer = WsCtrlGestion.DonneesInscription.Selection.Count
        Dim sep As String = ""
        Dim CRetour As RetAsync = Nothing
        If Index > 0 Then
            sep = vbCrLf
        End If
        IndexCourant = Index
        TxtStatut.Text = "Traitement " & (Index + 1) & "/" & NbTotal

        Try
            CRetour = TraiteModificationAsync(Index).Result
        Catch ex As Exception
            Exit Sub
        End Try
        txtMsg.Text = txtMsg.Text & sep & CRetour.Retour

        Index = CRetour.Index
        If Index >= NbTotal Then
            TxtStatut.Text = "Traitement terminé"
            WsCtrlGestion.Actualiser = True
            TimerTraitement.Enabled = False
            Index = 0
            IndexCourant = -1
        End If
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        Index = 0
        IndexCourant = -1
        txtMsg.Text = ""
        TxtStatut.Text = ""
        BtnTrait.Enabled = False
        TbMessage.Visible = False
        cboValeur.Enabled = True
        txtValeur.V_SiEnLectureSeule = False

        TbMotif.Visible = False

        Dim itemsel As ListItem = DirectCast(items, ListItem)
        TypeModif = itemsel.Value

        lblTraitement.Text = itemsel.Text

        If TypeModif.StartsWith("CBO_") Then
            RTxt.Visible = False
            RCbo.Visible = True
            ChargeCbo()
            cboValeur.SelectedIndex = 0
            Return
        End If

        If TypeModif.StartsWith("OUINON_") Then
            RTxt.Visible = False
            RCbo.Visible = True
            ChargeCboOuiNon()
            cboValeur.SelectedIndex = 0
            Return
        End If

        If TypeModif.StartsWith("NB_HEURE") Then
            lblSuffixe.Text = "h"
            txtValeur.V_Nature = 2
            txtValeur.V_Format = 22
        End If

        If TypeModif.StartsWith("COUT_") Then
            lblSuffixe.Text = "€"
            txtValeur.V_Nature = 2
            txtValeur.V_Format = 20
        End If

        If TypeModif.StartsWith("DTE_") Then
            lblSuffixe.Text = ""
            txtValeur.V_Nature = 1
            txtValeur.V_Format = 0
        End If

        RTxt.Visible = True
        RCbo.Visible = False

        txtValeur.DonText = ""

    End Sub

    Private Sub ChargeCbo()
        cboValeur.Items.Clear()

        Dim donneeref As String = ""
        Dim donneerefannex As String = ""
        Select Case TypeModif
            Case "CBO_SUIVI"
                donneeref = "Avis"
                donneerefannex = "Motif de non participation au stage"
            Case "CBO_MOTIF"
                donneeref = "Motif de non participation au stage"
            Case "CBO_PRIORITE"
                donneeref = "Ordre de priorité à l'inscription"
            Case "CBO_PRESENCE"
                donneeref = "Suivi présence au stage"
            Case "CBO_ACTION"
                donneeref = "Action de formation"
            Case "CBO_CONTEXTE"
                'donneeref = "Contexte de formation"
                donneeref = "Contexte du stage"
            Case "CBO_CURSUS"
                donneeref = "Cursus de formation"
            Case "CBO_FILIERE"
                donneeref = "Filière de formation"
            Case "CBO_MODULE"
                donneeref = "Module de formation"
            Case "CBO_PRIORITE"
                donneeref = "Ordre de priorité à l'inscription"
        End Select

        cboValeur.Items.Add(New ListItem("Sélectionner", "###RIEN###"))

        If TypeModif <> "CBO_SUIVI" Then
            cboValeur.Items.Add(New ListItem("Vider la valeur """ & donneeref & """", "###VIDER###"))
        End If

        Dim mot As GestionRefBase
        Dim items As List(Of Virtualia.TablesObjet.ShemaREF.TAB_LISTE)

        mot = ObjGlob.GetMoteurListeRef(donneeref)
        items = mot.Source
        If (items Is Nothing OrElse items.Count <= 0) Then
            Return
        End If

        For Each s In (From it In items Order By it.Valeur Select it.Valeur).ToList()
            cboValeur.Items.Add(New ListItem(s))
        Next

        If (donneerefannex <> "") Then
            mot = ObjGlob.GetMoteurListeRef(donneerefannex)
            items = mot.Source

            Label1.Text = "Motif de non participation"

            cboMotif.Items.Clear()
            cboMotif.Items.Add(New ListItem("Choisir ..."))

            If (items.Count <= 0) Then
                Return
            End If

            For Each s In (From it In items Order By it.Valeur Select it.Valeur).ToList()
                cboMotif.Items.Add(New ListItem(s))
            Next

        End If
    End Sub

    Private Sub ChargeCboOuiNon()
        cboValeur.Items.Clear()
        cboValeur.Items.Add(New ListItem("Choisir ..."))

        cboValeur.Items.Add("Oui")
        cboValeur.Items.Add("Non")

        If TypeModif = "OUINON_DIF" Then
            Label1.Text = "DIF Hors temps de travail"

            cboMotif.Items.Clear()
            cboMotif.Items.Add(New ListItem("Choisir ..."))

            cboMotif.Items.Add("Oui")
            cboMotif.Items.Add("Non")
        End If
    End Sub

    Private Async Function TraiteModificationAsync(ByVal IndexDonnee As Integer) As Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(IData)
                                                               Return TraiteModification(IndexDonnee)
                                                           End Function

        Return Await Task.FromResult(Of RetAsync)(FuncTraitement(IndexDonnee))
    End Function

    Private Function TraiteModification(ByVal IndexDonnee As Integer) As RetAsync
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim CRetour As RetAsync = New RetAsync()
        CRetour.Index = IndexDonnee + 1
        Dim ElementVerif As ItemInscriptionInfo = WsCtrlGestion.DonneesInscription.Selection(IndexDonnee)

        Moteur.ReinitStructureSeulement()

        Dim StructureInscription As StructureFormationInscription = TryCast(Moteur.GetStructure(WsCtrlGestion.IdeStage), StructureFormationInscription)

        If StructureInscription Is Nothing Then
            CRetour.Index = Integer.MaxValue
            CRetour.Retour = "Le stage n'existe pas ou a été supprimé par un autre utilisateur"
            Return CRetour
        End If
        Dim FicheOLD As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS
        FicheOLD = (From fi In StructureInscription.Inscriptions.GetFichesTypees()
                    Where
                                                fi.Nom_stagiaire = ElementVerif.Nom And
                                                fi.Prenom_stagiaire = ElementVerif.Prenom And
                                                fi.Datenaissance_stagiaire = ElementVerif.DateDeNaissance And
                                                fi.Rang_inscription = ElementVerif.Rang And
                                                fi.Rang_Session = ElementVerif.Rang_Session
                    Select fi).FirstOrDefault()
        If FicheOLD Is Nothing Then
            FicheOLD = (From fi In StructureInscription.Inscriptions.GetFichesTypees()
                        Where
                                                    fi.Nom_stagiaire = ElementVerif.Nom And
                                                    fi.Prenom_stagiaire = ElementVerif.Prenom And
                                                    fi.Datenaissance_stagiaire = ElementVerif.DateDeNaissance And
                                                    fi.Rang_inscription = ElementVerif.Rang
                        Select fi).FirstOrDefault()
        End If
        CRetour.Retour = ElementVerif.NomPrenom & vbCrLf
        If FicheOLD Is Nothing Then
            CRetour.Retour = CRetour.Retour & vbTab & "L'inscription a été supprimée."
            Return CRetour
        End If
        CRetour.Retour = CRetour.Retour & vbTab

        Dim OldClef As String = FicheOLD.GetClef()
        Dim FicheNEW As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS = VirFicheExtension.CloneVirFiche(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)(FicheOLD)
        Dim db As Double = 0
        Dim SiaFaire As Boolean = False
        Dim SiaVider As Boolean = False
        '***** RP ***********************************************************
        Dim FicheDIF As Virtualia.TablesObjet.ShemaPER.PER_DIF = Nothing
        Dim Ensemble As EnsembleDossiers = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsemblePER
        Dim Dossier As DossierFormation
        Dim IdeDossier As Integer
        '********************************************************************

        If TypeModif.StartsWith("CBO_") Then
            SiaVider = cboValeur.SelectedItem.Value = "###VIDER###"
        End If

        Select Case TypeModif
            Case "CBO_SUIVI"
                FicheNEW.Suivi_inscription = cboValeur.SelectedItem.Text
                If TbMotif.Visible = True Then
                    If cboMotif.SelectedIndex > 0 Then
                        FicheNEW.Motif_nonparticipation = cboMotif.SelectedItem.Text
                    End If
                Else
                    FicheNEW.Motif_nonparticipation = ""
                End If
                SiaFaire = SuiviADesChangements(FicheOLD, FicheNEW)

                If SiaFaire = True Then
                    If FicheNEW.Datedebut_formation.Trim() <> "" Then
                        '***** RP  DIF **********************************************************************
                        FicheDIF = Nothing
                        IdeDossier = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirIdentifiantPER(FicheNEW.Nom_stagiaire, FicheNEW.Prenom_stagiaire, FicheNEW.Datenaissance_stagiaire)
                        If IdeDossier > 0 Then
                            Ensemble.Identifiant = IdeDossier
                            Dossier = Ensemble.ItemDossier(IdeDossier)
                            If Dossier IsNot Nothing Then
                                Call Dossier.VerifierEtMajDIF(FicheNEW.Datedebut_formation)
                            End If
                            FicheDIF = Dossier.Fiche_DIF(FicheNEW.Datedebut_formation)
                        End If
                        '*************************************************************************************
                    End If
                End If

            Case "CBO_MOTIF"
                If SiaVider = True Then
                    FicheNEW.Motif_nonparticipation = ""
                Else
                    If (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                        CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le motif de non participation ne peut pas être renseigné"
                        Return CRetour
                    End If

                    FicheNEW.Motif_nonparticipation = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Motif_nonparticipation <> FicheNEW.Motif_nonparticipation)

            Case "CBO_PRIORITE"
                If SiaVider = True Then
                    FicheNEW.Ordrepriorite_inscription = ""
                Else
                    FicheNEW.Ordrepriorite_inscription = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Ordrepriorite_inscription <> FicheNEW.Ordrepriorite_inscription)

            Case "CBO_PRESENCE"
                If SiaVider = True Then
                    FicheNEW.Suivipresence = ""
                Else
                    FicheNEW.Suivipresence = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Suivipresence <> FicheNEW.Suivipresence)

            Case "CBO_ACTION"
                If SiaVider = True Then
                    FicheNEW.Action_formation = ""
                Else
                    FicheNEW.Action_formation = cboValeur.SelectedItem.Text
                End If
                If (FicheOLD.Action_formation <> FicheNEW.Action_formation) Then
                    FicheNEW.Inscriptionpersonnalisee = "Oui"
                    SiaFaire = True
                End If

            Case "NB_HEURE"
                If Not (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                    CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le nombre d'heures ne doit pas être renseigné"
                    Return CRetour
                End If
                FicheNEW.Nbheures_formation = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText, 1)

                If FicheOLD.Nbheures_formation <> FicheNEW.Nbheures_formation Then
                    If FicheNEW.Datedebut_formation.Trim() <> "" AndAlso
                        FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription) AndAlso (FicheNEW.DIF.ToLower() = "oui") Then

                        '***** RP  DIF **********************************************************
                        FicheDIF = Nothing
                        IdeDossier = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirIdentifiantPER(FicheNEW.Nom_stagiaire, FicheNEW.Prenom_stagiaire, FicheNEW.Datenaissance_stagiaire)
                        If IdeDossier > 0 Then
                            Ensemble.Identifiant = IdeDossier
                            Dossier = Ensemble.ItemDossier(IdeDossier)
                            If Dossier IsNot Nothing Then
                                Call Dossier.VerifierEtMajDIF(FicheNEW.Datedebut_formation)
                            End If
                            FicheDIF = Dossier.Fiche_DIF(FicheNEW.Datedebut_formation)
                            If FicheDIF Is Nothing Then
                                CRetour.Retour = CRetour.Retour & "Il n'y a pas de fiche DIF pour ce stagiaire valable à la date du " & FicheNEW.Datedebut_formation
                                Return CRetour
                            End If
                        End If
                        If FicheDIF Is Nothing Then
                            CRetour.Retour = CRetour.Retour & "Il n'y a pas de fiche DIF pour ce stagiaire valable à la date du "
                            Return CRetour
                        ElseIf FicheNEW.Nbheures_formation > WebFct.ViRhFonction.ConversionDouble(WebFct.ViRhDates.CalcHeure(FicheDIF.Solde_DIF, "", 1)) Then
                            CRetour.Retour = "La durée dépasse le solde DIF autorisé"
                            Return CRetour
                        End If
                        '*************************************************************************************
                    End If
                    FicheNEW.Inscriptionpersonnalisee = "Oui"
                    SiaFaire = True
                End If

            Case "NB_HEURE_DEP"
                FicheNEW.Nbheures_deplacement = Integer.Parse(txtValeur.DonText)
                If FicheOLD.Nbheures_deplacement <> FicheNEW.Nbheures_deplacement Then
                    SiaFaire = True
                End If

            Case "COUT_FORFAITAIRE"
                If Not (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                    CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le coût annexe ne peut pas être renseigné"
                    Return CRetour
                End If
                FicheNEW.Coutforfaitaire = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                If FicheOLD.Coutforfaitaire <> FicheNEW.Coutforfaitaire Then
                    FicheNEW.Inscriptionpersonnalisee = "Oui"
                    SiaFaire = True
                End If

            Case "COUT_PEDAGOGIQUE"
                If Not (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                    CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le coût pédagogique ne peut pas être renseigné"
                    Return CRetour
                End If

                FicheNEW.Coutpedagogique = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                If FicheOLD.Coutpedagogique <> FicheNEW.Coutpedagogique Then
                    FicheNEW.Inscriptionpersonnalisee = "Oui"
                    SiaFaire = True
                End If

            Case "COUT_DEPLACEMENT"
                If Not (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                    CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le coût de déplacement ne peut pas être renseigné"
                    Return CRetour
                End If
                FicheNEW.Coutdeplacement = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                SiaFaire = (FicheOLD.Coutdeplacement <> FicheNEW.Coutdeplacement)

            Case "COUT_RESTAURATION"
                If Not (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                    CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le coût de restauration ne peut pas être renseigné"
                    Return CRetour
                End If
                FicheNEW.Coutrestauration = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                SiaFaire = (FicheOLD.Coutrestauration <> FicheNEW.Coutrestauration)

            Case "COUT_HEBERGEMENT"
                If Not (FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription)) Then
                    CRetour.Retour = CRetour.Retour & "Pour le suivi de l'inscription """ & FicheNEW.Suivi_inscription & """, le coût d'hébergement ne peut pas être renseigné"
                    Return CRetour
                End If

                FicheNEW.Couthebergement = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                SiaFaire = (FicheOLD.Couthebergement <> FicheNEW.Couthebergement)

            Case "CBO_CONTEXTE"
                If SiaVider = True Then
                    FicheNEW.Contexte_formation = ""
                Else
                    FicheNEW.Contexte_formation = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Contexte_formation <> FicheNEW.Contexte_formation)

            Case "CBO_CURSUS"
                If SiaVider = True Then
                    FicheNEW.Cursus_formation = ""
                Else
                    FicheNEW.Cursus_formation = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Cursus_formation <> FicheNEW.Cursus_formation)

            Case "CBO_FILIERE"
                If SiaVider = True Then
                    FicheNEW.Filiere_formation = ""
                Else
                    FicheNEW.Filiere_formation = cboValeur.SelectedItem.Text
                End If

                If FicheOLD.Filiere_formation <> FicheNEW.Filiere_formation Then
                    SiaFaire = True
                    FicheNEW.Inscriptionpersonnalisee = "Oui"
                End If

            Case "CBO_MODULE"
                If SiaVider = True Then
                    FicheNEW.Module_formation = ""
                Else
                    FicheNEW.Module_formation = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Module_formation <> FicheNEW.Module_formation)

            Case "CBO_PRIORITE"
                If SiaVider = True Then
                    FicheNEW.Ordrepriorite_inscription = ""
                Else
                    FicheNEW.Ordrepriorite_inscription = cboValeur.SelectedItem.Text
                End If
                SiaFaire = (FicheOLD.Ordrepriorite_inscription <> FicheNEW.Ordrepriorite_inscription)

            Case "DTE_INSC"
                If txtValeur.DonText.Trim() = "" Then
                    CRetour.Retour = CRetour.Retour & "La date saisie n'est pas correcte"
                    Return CRetour
                End If
                FicheNEW.Date_inscription = txtValeur.DonText
                SiaFaire = (FicheOLD.Date_inscription <> FicheNEW.Date_inscription)

            Case "OUINON_HEBER"
                FicheNEW.Demandehebergement = cboValeur.SelectedItem.Text
                SiaFaire = (FicheOLD.Demandehebergement.ToLower() <> FicheNEW.Demandehebergement.ToLower())

            Case "OUINON_DIF"
                FicheNEW.DIF = cboValeur.SelectedItem.Text
                If cboValeur.SelectedItem.Text.ToLower() = "non" Then
                    FicheNEW.DIF_horstravail = "Non"
                End If
                If cboMotif.SelectedIndex <> 0 Then
                    FicheNEW.DIF_horstravail = cboMotif.SelectedItem.Text
                End If
                If FicheNEW.Datedebut_formation.Trim() <> "" Then
                    '***** RP  DIF **********************************************************
                    FicheDIF = Nothing
                    IdeDossier = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirIdentifiantPER(FicheNEW.Nom_stagiaire, FicheNEW.Prenom_stagiaire, FicheNEW.Datenaissance_stagiaire)
                    If IdeDossier > 0 Then
                        Ensemble.Identifiant = IdeDossier
                        Dossier = Ensemble.ItemDossier(IdeDossier)
                        If Dossier IsNot Nothing Then
                            Call Dossier.VerifierEtMajDIF(FicheNEW.Datedebut_formation)
                        End If
                        FicheDIF = Dossier.Fiche_DIF(FicheNEW.Datedebut_formation)
                        If FicheDIF Is Nothing Then
                            CRetour.Retour = CRetour.Retour & "Il n'y a pas de fiche DIF pour ce stagiaire valable à la date du " & FicheNEW.Datedebut_formation
                            Return CRetour
                        End If
                    End If
                    '*************************************************************************************
                    If FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription) = True _
                        AndAlso FicheNEW.DIF.ToLower() = "oui" Then
                        If FormationHelper.DureeDIFOK(FicheDIF, FicheNEW.Nbheures_formation) = False Then
                            CRetour.Retour = "La durée dépasse le solde DIF autorisé"
                            Return CRetour
                        End If
                    End If
                End If
                SiaFaire = (FicheOLD.DIF.ToLower() <> FicheNEW.DIF.ToLower()) OrElse (FicheOLD.DIF_horstravail.ToLower() <> FicheNEW.DIF_horstravail.ToLower())

            Case "OUINON_DIF_HORS"
                If FicheNEW.DIF.ToLower() = "non" AndAlso cboValeur.SelectedIndex = 1 Then
                    CRetour.Retour = CRetour.Retour & "Il n'y a pas de DIF pour cette inscription, la valeur saisie est incohérente"
                    Return CRetour
                End If
                If FicheNEW.Datedebut_formation.Trim() <> "" _
                    AndAlso FormationHelper.EstValeurDeComptabilisationEtDeValidation(FicheNEW.Suivi_inscription) _
                    AndAlso (FicheOLD.DIF.ToLower() = "oui") Then

                    '***** RP  DIF **********************************************************
                    FicheDIF = Nothing
                    IdeDossier = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirIdentifiantPER(FicheNEW.Nom_stagiaire, FicheNEW.Prenom_stagiaire, FicheNEW.Datenaissance_stagiaire)
                    If IdeDossier > 0 Then
                        Ensemble.Identifiant = IdeDossier
                        Dossier = Ensemble.ItemDossier(IdeDossier)
                        If Dossier IsNot Nothing Then
                            Call Dossier.VerifierEtMajDIF(FicheNEW.Datedebut_formation)
                        End If
                        FicheDIF = Dossier.Fiche_DIF(FicheNEW.Datedebut_formation)
                        If FicheDIF Is Nothing Then
                            CRetour.Retour = CRetour.Retour & "Il n'y a pas de fiche DIF pour ce stagiaire valable à la date du " & FicheNEW.Datedebut_formation
                            Return CRetour
                        End If
                    End If
                    '*************************************************************************************
                End If
                FicheNEW.DIF_horstravail = cboValeur.SelectedItem.Text
                SiaFaire = (FicheOLD.DIF_horstravail.ToLower() <> FicheNEW.DIF_horstravail.ToLower())

        End Select

        CRetour.Retour = CRetour.Retour & "Modification "

        If SiaFaire = False Then
            CRetour.Retour = CRetour.Retour & "OK"
            Return CRetour
        End If
        Dim LstResultat As RetourCRUD = Moteur.Modifie(OldClef, FicheNEW)
        If LstResultat.Index < 0 Then
            CRetour.Retour = CRetour.Retour & LstResultat.Message
        Else
            CRetour.Retour = CRetour.Retour & "OK"
            WsCtrlGestion.Actualiser = True
        End If

        If FicheDIF Is Nothing Then
            Return CRetour
        End If
        '***** RP **********************************************************
        Ensemble.Identifiant = FicheDIF.Ide_Dossier
        Dossier = Ensemble.ItemDossier(FicheDIF.Ide_Dossier)
        If Dossier IsNot Nothing Then
            Call Dossier.VerifierEtMajDIF(FicheDIF.Date_de_Valeur)
        End If
        '*************************************************************************************
        Return CRetour
    End Function

    Private Function SuiviADesChangements(ByVal fianc As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS, ByVal finew As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS) As Boolean
        Return Not ((fianc.Suivi_inscription = finew.Suivi_inscription) AndAlso (fianc.Motif_nonparticipation = finew.Motif_nonparticipation))
    End Function

    Private Function VerifieSiActionPossible() As Boolean
        If TypeModif <> "COUT_FORFAITAIRE" AndAlso TypeModif <> "COUT_PEDAGOGIQUE" Then
            Return True
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim LstCouts As List(Of Virtualia.TablesObjet.ShemaREF.FOR_COUTS) =
            WebFct.PointeurGlobal.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.FOR_COUTS)(WebFct.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WebFct.PointeurGlobal.VirNomUtilisateur, 9, 6, WsCtrlGestion.IdeStage, False))

        If LstCouts Is Nothing Then
            Return True
        End If
        Moteur.ReinitStructureSeulement()
        Dim StructForInscr As StructureFormationInscription = TryCast(Moteur.GetStructure(WsCtrlGestion.IdeStage), StructureFormationInscription)
        If StructForInscr Is Nothing Then
            Return False
        End If

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim Qr = From fi In StructForInscr.Inscriptions.GetFichesTypees() Join sel In WsCtrlGestion.DonneesInscription.Selection _
                On fi.Nom_stagiaire Equals sel.Nom And fi.Prenom_stagiaire Equals sel.Prenom And fi.Datenaissance_stagiaire _
                Equals sel.DateDeNaissance And fi.Rang_inscription Equals sel.Rang Select DirectCast(fi, Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)

        Dim inscsamods As List(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS) = New List(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)()
        Dim NEWFiche As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS
        Dim vdb As Double = 0

        Dim lstvaleurcompta As List(Of String) = FormationHelper.GetValeursDeComptabilisation()

        For Each fi In Qr
            NEWFiche = Nothing

            Select Case TypeModif
                Case "COUT_FORFAITAIRE"
                    If Not (lstvaleurcompta.Contains(fi.Suivi_inscription.ToLower())) Then
                        Continue For
                    End If
                    vdb = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                    If (fi.Coutforfaitaire = vdb) Then
                        Continue For
                    End If
                    NEWFiche = VirFicheExtension.CloneVirFiche(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)(fi)
                    NEWFiche.Coutforfaitaire = vdb

                Case "COUT_PEDAGOGIQUE"
                    If Not (lstvaleurcompta.Contains(fi.Suivi_inscription.ToLower())) Then
                        Continue For
                    End If
                    vdb = WebFct.ViRhFonction.ConversionDouble(txtValeur.DonText)
                    If (fi.Coutpedagogique = vdb) Then
                        Continue For
                    End If
                    NEWFiche = VirFicheExtension.CloneVirFiche(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)(fi)
                    NEWFiche.Coutpedagogique = vdb
            End Select
            If NEWFiche IsNot Nothing Then
                NEWFiche.Inscriptionpersonnalisee = "Oui"
                inscsamods.Add(NEWFiche)
            End If

        Next
        If inscsamods.Count <= 0 Then
            Return True
        End If

        Return FormationHelper.EstCalculCoutPossible(WsCtrlGestion.IdeStage, inscsamods, LstCouts)

    End Function

    Protected Sub cboValeur_SelectedIndexChanged(sender As Object, e As EventArgs)
        If TypeModif = "CBO_SUIVI" Then

            cboMotif.SelectedIndex = 0
            TbMotif.Visible = False
            BtnTrait.Enabled = False

            If cboValeur.SelectedIndex <= 0 Then
                Return
            End If

            Dim SiEstValeurdeCompta As Boolean = (FormationHelper.EstValeurDeComptabilisationEtDeValidation(cboValeur.SelectedItem.Text))
            BtnTrait.Enabled = SiEstValeurdeCompta
            TbMotif.Visible = Not SiEstValeurdeCompta

            Return
        End If

        If TypeModif = "OUINON_DIF" Then
            If cboValeur.SelectedIndex = 0 OrElse cboValeur.SelectedIndex = 2 Then
                cboMotif.SelectedIndex = 0
                TbMotif.Visible = False
                BtnTrait.Enabled = (cboValeur.SelectedIndex = 2)
                Return
            End If

            cboMotif.SelectedIndex = 0
            TbMotif.Visible = True
            BtnTrait.Enabled = True

            Return

        End If

        BtnTrait.Enabled = (cboValeur.SelectedIndex > 0)
    End Sub

    Protected Sub cboMotif_SelectedIndexChanged(sender As Object, e As EventArgs)
        BtnTrait.Enabled = (cboMotif.SelectedIndex > 0)
    End Sub

End Class