﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Structure.Formation
Imports Virtualia.Metier.Formation
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Public Class CtrlGestionListeInscription
    'Gestion de l'approche par session
    Private WsForceChargementSession As Boolean = False
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Public ReadOnly Property DonneesGridSession As DonneeForSessionInscriptionCollection
        Get
            If Not (ViewState.KeyExiste(DonneeForSessionInscriptionCollection.KeyState)) Then
                ViewState.AjouteValeur(DonneeForSessionInscriptionCollection.KeyState, New DonneeForSessionInscriptionCollection())
            End If
            Return ViewState.GetValeur(Of DonneeForSessionInscriptionCollection)(DonneeForSessionInscriptionCollection.KeyState)
        End Get
    End Property

    Private Property VCacheCollectionSession As CacheGestCollectionFiche
        Get
            If Not (ViewState.KeyExiste("VCacheCollectionSession")) Then
                ViewState.AjouteValeur("VCacheCollectionSession", New CacheGestCollectionFiche())
            End If
            Return ViewState.GetValeur(Of CacheGestCollectionFiche)("VCacheCollectionSession")
        End Get
        Set(value As CacheGestCollectionFiche)
            ViewState.AjouteValeur("VCacheCollectionSession", value)
        End Set
    End Property

    Private ReadOnly Property ActSelectDansDataGridSession As Action(Of String)
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return Sub(Clef)
                       cboAction.SelectedIndex = 0
                       cboAction.Enabled = False
                       If WsForceChargementSession = False Then
                           If Clef.Trim() = "" OrElse VCacheCollectionSession.ClefCourante = Clef Then
                               Return
                           End If
                       End If

                       VCacheCollectionSession.ClefCourante = Clef
                       VCacheCollectionSession.Sauve(ViewState)

                       ListeGrilleSession.ClefCourante = Clef

                       Dim dfsi As DonneeForSessionInscription = (From d In DonneesGridSession Where d.CleeSelection = Clef Select d).First()
                       Dim dtedeb As String = "NULLE"
                       Dim dtefin As String = "NULLE"
                       Dim rangsession As Integer = 0

                       If dfsi.DateDebut <> DateTime.MinValue Then
                           dtedeb = dfsi.DateDebut.ToShortDateString
                           dtefin = dfsi.DateFin.ToShortDateString
                           rangsession = dfsi.RangSession
                       End If

                       LblTitreSelection.Text = dfsi.LibelleCellule

                       Dim lstfis As List(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)

                       If Clef.ToString().EndsWith("#SANS#") = True Then
                           MoteurInscription.ReinitStructureSeulement()

                           Dim structf As StructureFormationInscription = TryCast(MoteurInscription.GetStructure(IdeStage), StructureFormationInscription)
                           Dim lstsession As List(Of Virtualia.TablesObjet.ShemaREF.FOR_SESSION) = FormationHelper.GetSessionDeStage(structf.Identification.Ide_Dossier)

                           Dim QueryInsc = From elem In ( _
                                                            From fi In structf.Inscriptions.GetFichesTypees() Where Not (fi.Datedebut_formation Is Nothing) AndAlso fi.Datedebut_formation.Trim() <> "" _
                                                            Group Join fs In lstsession On WebFct.ViRhDates.DateTypee(fi.Datedebut_formation) _
                                                            Equals WebFct.ViRhDates.DateTypee(fs.DateSession) And WebFct.ViRhDates.DateTypee(fi.Datefin_formation) _
                                                            Equals WebFct.ViRhDates.DateTypee(fs.DatedeFin_Session) And fi.Rang_Session Equals fs.Rang Into Group _
                                                            From ss In Group.DefaultIfEmpty() _
                                                            Select FORINSC = fi, _
                                                                   SESSION = ss _
                                                        ) _
                                            Group elem By SESSION = elem.SESSION Into INSCS = Group _
                                            Select SESSION, INSCS

                           lstfis = New List(Of Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS)()

                           For Each Elem In (From it In QueryInsc Where it.SESSION Is Nothing Select it).First().INSCS
                               lstfis.Add(DirectCast(Elem.FORINSC, Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS))
                           Next
                       Else
                           lstfis = FormationHelper.GetToutesLesForInscriptionDeSession(dfsi.Ide, dtedeb, dtefin, rangsession, False)
                       End If

                       DonneesInscription.Clear()
                       DonneesInscription.AddRange(New ItemInscriptionInfoCollection((From fi In lstfis Order By fi.Nom_stagiaire, fi.Prenom_stagiaire Select fi).ToList()))
                       DonneesInscription.Sauve(ViewState)

                       Select Case MultiOnglets.ActiveViewIndex
                           Case 0
                               CtrlListe1.Charge(Nothing)
                           Case 1
                               CtrlListe2.Charge(Nothing)
                           Case 8
                               CtrlListe3.Charge(Nothing)
                       End Select

                   End Sub
        End Get
    End Property

    Private Sub ChargeApprocheSession()
        MultiVueGrille.ActiveViewIndex = 0
        btnVue.Text = "Par stagiaire"

        If (MultiOnglets.ActiveViewIndex <> 0 AndAlso MultiOnglets.ActiveViewIndex <> 1 AndAlso MultiOnglets.ActiveViewIndex <> 8) Then
            MultiOnglets.ActiveViewIndex = 0
            CellDataGrid.Visible = True
            tbMenus.Visible = True
            TbEdition.Visible = True
            CellTotaux.Visible = True
            btnVue.Visible = True

            NumVueAnc = 0
        End If

        TbEdition.Visible = True

        cboAction.SelectedIndex = 0
        cboAction.Enabled = False

        VCacheCollectionSession.ClefCourante = ""
        VCacheCollectionSession.Sauve(ViewState)

        CtrlListe1.ViderDetail(True)
        CtrlListe2.ViderDetail(True)
        CtrlListe3.ViderDetail(True)

        ChargeCacheSession(True)

    End Sub

    Private Sub InitDataGridSession()

        If Not (IsPostBack) Then
            ListeGrilleSession.Captions = New List(Of String)()

            ListeGrilleSession.Captions.Add("Aucune session")
            ListeGrilleSession.Captions.Add("Une session")
            ListeGrilleSession.Captions.Add("sessions")

            ListeGrilleSession.Colonnes = New DonneeColonneCollection()

            Dim dc As DonneeColonne
            dc = New DonneeColonne()
            dc.Alignement = HorizontalAlign.Center
            dc.Libelle = "Session"
            dc.Propriete = "LibelleCellule"
            dc.TypeDonnee = TypeData.Chaine
            dc.Alignement = HorizontalAlign.Left
            ListeGrilleSession.Colonnes.Add(dc)

            dc = New DonneeColonne()
            dc.Alignement = HorizontalAlign.Center
            dc.Libelle = "Nombre d'inscrits"
            dc.Propriete = "NbInscTotGrid"
            dc.TypeDonnee = TypeData.Chaine
            ListeGrilleSession.Colonnes.Add(dc)

            dc = New DonneeColonne()
            dc.Alignement = HorizontalAlign.Center
            dc.Libelle = "Nombre d'heures"
            dc.Propriete = "NbHeureTot"
            dc.TypeDonnee = TypeData.Chaine
            ListeGrilleSession.Colonnes.Add(dc)

            dc = New DonneeColonne()
            dc.Alignement = HorizontalAlign.Right
            dc.Libelle = "Coûts pédagogiques"
            dc.Propriete = "CoutTotChaine"
            dc.TypeDonnee = TypeData.Chaine
            ListeGrilleSession.Colonnes.Add(dc)
        End If

        ListeGrilleSession.Act_SelectDataDansDataGrid = ActSelectDansDataGridSession
        ListeGrilleSession.EndInit()

    End Sub

    Private Sub ChargeGrilleSession(chargement As Boolean)
        If DonneesGridSession.Count <= 0 Then
            ListeGrilleSession.Charge(Nothing)
            Exit Sub
        End If

        Dim donnees As List(Of ForSessionInscription) = New List(Of ForSessionInscription)()

        DonneesGridSession.ForEach(Sub(d)
                                       donnees.Add(New ForSessionInscription(d))
                                   End Sub)

        VCacheCollectionSession.Datas.AddRange(donnees)
        VCacheCollectionSession.Sauve(ViewState)

        ListeGrilleSession.ClefCourante = ""
        ListeGrilleSession.Charge(donnees)

        If (chargement) Then
            ActSelectDansDataGridSession()(ListeGrilleSession.PremiereClef)
            Return
        End If

        WsForceChargementSession = True

        If ListeGrilleSession.ListClefs.Contains(VCacheCollectionSession.ClefCourante) Then
            ActSelectDansDataGridSession()(VCacheCollectionSession.ClefCourante)
            Return
        End If

        ActSelectDansDataGridSession()(ListeGrilleSession.PremiereClef)

    End Sub

    Private Sub ChargeCacheSession(chargement As Boolean)
        DonneesGridSession.Vider(ViewState)
        DonneesInscription.Clear()
        DonneesInscription.Sauve(ViewState)

        MoteurInscription.ReinitStructureSeulement()

        Dim strucf As StructureFormationInscription = TryCast(MoteurInscription.GetStructure(IdeStage), StructureFormationInscription)

        If strucf IsNot Nothing AndAlso strucf.Inscriptions.Count > 0 Then

            Dim lsttmp As List(Of DonneeForSessionInscription) = DonneeForSessionInscriptionCollection.ConstruitListe(MoteurInscription, strucf, DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).ParametreFormation)
            DonneesGridSession.AddRange(lsttmp)
            DonneesGridSession.Sauve(ViewState)
        End If
        ChargeTotaux(strucf)

        ChargeGrilleSession(chargement)
    End Sub

End Class
