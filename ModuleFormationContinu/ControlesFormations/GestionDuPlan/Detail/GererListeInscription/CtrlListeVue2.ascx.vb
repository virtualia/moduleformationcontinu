﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing

Public Class CtrlListeVue2
    Inherits UserControl
    Implements IControlBase

    Private WsCtrlGestion As CtrlGestionListeInscription
    Private WsCouleurLigne As Color = VisuHelper.ConvertiCouleur("#E2E2E2")
    Private WsCouleurLigne_InscPerson As Color = VisuHelper.ConvertiCouleur("#B0E0D7")
    Private WsCouleurBouton As Color = Color.LightGray

    Private WsLstBtn As List(Of Button) = New List(Of Button)()
    Private WsLstChk As List(Of CheckBox) = New List(Of CheckBox)()
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Public WriteOnly Property CtrlGestion As CtrlGestionListeInscription
        Set(value As CtrlGestionListeInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Private Property NbTotJour As Double
        Get
            If Not (ViewState.KeyExiste("NbTotJour")) Then
                ViewState.AjouteValeur("NbTotJour", Double.Parse("0"))
            End If
            Return DirectCast(ViewState("NbTotJour"), Double)
        End Get
        Set(value As Double)
            ViewState.AjouteValeur("NbTotJour", value)
        End Set
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        ConstruitControles()
    End Sub

#Region "Methodes"
    Public Sub Charge(items As Object) Implements IControlBase.Charge
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        NbTotJour = 0
        ChkSelection.Checked = False

        ConstruitControles()

        LblTotDossier.Text = WsCtrlGestion.GetTotal("NB")
        LblTotHeure.Text = WsCtrlGestion.GetTotal("NBHEURE")
        LblTotJour.Text = WebFct.ViRhFonction.MontantEdite(NbTotJour, 1)
        LblTotForfaitaire.Text = WsCtrlGestion.GetTotal("C_FORFAIT")
        LblTotPedagogique.Text = WsCtrlGestion.GetTotal("C_PEDA")
        LblTotDeplacement.Text = WsCtrlGestion.GetTotal("C_DEPLA")
        LblTotRestauration.Text = WsCtrlGestion.GetTotal("C_RESTAU")
        LblTotHebergement.Text = WsCtrlGestion.GetTotal("C_HEBERG")

    End Sub

    Private Sub ConstruitControles()
        WsLstBtn = New List(Of Button)()
        WsLstChk = New List(Of CheckBox)()

        ViderDetail()

        For Each iti In WsCtrlGestion.DonneesInscription
            ConstruitLigneDetail(iti)
        Next
        If WsCtrlGestion.V_SiEnLecture = True Then
            For Each Ck As CheckBox In WsLstChk
                Ck.Enabled = False
            Next
            For Each Bt As Button In WsLstBtn
                Bt.Enabled = False
            Next
        Else
            WsLstChk.ForEach(Sub(ck As CheckBox)
                                 AddHandler ck.CheckedChanged, AddressOf ChkClick
                             End Sub)
            WsLstBtn.ForEach(Sub(bt As Button)
                                 AddHandler bt.Click, AddressOf BtnClick
                             End Sub)
        End If
    End Sub

    Private Sub ConstruitLigneDetail(iti As ItemInscriptionInfo)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim param As ParametreFormationInfo = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).ParametreFormation
        Dim Rangee As TableRow = New TableRow()
        Rangee.ID = "RDetail" & iti.Index

        If Not (iti.EstPersonnalise) Then
            Rangee.BackColor = WsCouleurLigne
        Else
            Rangee.BackColor = WsCouleurLigne_InscPerson
        End If

        Rangee.Cells.Add(ConstructionCell.ConstruitCellChk(iti, WsLstChk))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellBouton(iti, WsLstBtn, WsCouleurBouton))

        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "NomPrenom", "40%", "370px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "NbHeure", "10%", "80px", HorizontalAlign.Center))

        Dim nbj As Double = 0
        If (iti.NbHeure > 0) Then
            nbj = param.CalculNbJour(iti.NbHeure)
            NbTotJour = NbTotJour + nbj
        End If
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabelValeur(WebFct.ViRhFonction.MontantEdite(nbj, 1), iti.Index, "10%", "80px", HorizontalAlign.Center))

        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Cout_Pedagogique", "10%", "80px", HorizontalAlign.Right))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Cout_Forfaitaire", "10%", "80px", HorizontalAlign.Right))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Cout_Deplacement", "10%", "80px", HorizontalAlign.Right))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Cout_Restauration", "10%", "80px", HorizontalAlign.Right))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Cout_Hebergement", "10%", "80px", HorizontalAlign.Right))

        TbDetail.Rows.Add(Rangee)

    End Sub

    Public Sub ViderDetail(Optional ViderTotal As Boolean = False)
        TbDetail.Rows.Clear()
        If Not (ViderTotal) Then
            Return
        End If

        LblTotDossier.Text = ""
        LblTotHeure.Text = ""
        LblTotForfaitaire.Text = ""
        LblTotPedagogique.Text = ""
        LblTotDeplacement.Text = ""
        LblTotRestauration.Text = ""
        LblTotHebergement.Text = ""
        LblTotDossier.Text = ""

    End Sub

    Private Sub ChkClick(s As Object, ev As EventArgs)
        ConstructionCell.EvenChk(DirectCast(s, CheckBox), WsCtrlGestion)

        ChkSelection.Checked = WsCtrlGestion.DonneesInscription.Count = WsLstChk.Where(Function(ck)
                                                                                           Return ck.Checked
                                                                                       End Function).Count()
    End Sub

    Private Sub BtnClick(s As Object, ev As EventArgs)
        ConstructionCell.EvenBtn(DirectCast(s, Button), WsCtrlGestion)
    End Sub

    Protected Sub ChkSelection_CheckedChanged(sender As Object, e As EventArgs)
        Dim sel As Boolean = ChkSelection.Checked
        If WsLstChk.Count > 0 Then
            For Each Chk In WsLstChk
                Chk.Checked = sel
                ChkClick(Chk, Nothing)
            Next
        End If
    End Sub
#End Region

End Class