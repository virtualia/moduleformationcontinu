﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.TablesObjet.ShemaREF

Partial Public Class CtrlGestionListeInscription
    'Gestion de l'approche par stagiaire
    Private WsForceChargementStagiaire As Boolean = False

    Public ReadOnly Property DonneesGridStagiaire As DonneeForStagiaireInscriptionCollection
        Get
            If Not (ViewState.KeyExiste(DonneeForStagiaireInscriptionCollection.KeyState)) Then
                ViewState.AjouteValeur(DonneeForStagiaireInscriptionCollection.KeyState, New DonneeForStagiaireInscriptionCollection())
            End If
            Return ViewState.GetValeur(Of DonneeForStagiaireInscriptionCollection)(DonneeForStagiaireInscriptionCollection.KeyState)
        End Get
    End Property

    Private Property VCacheCollectionStagiaire As CacheGestCollectionFiche
        Get
            If Not (ViewState.KeyExiste("VCacheCollectionAgent")) Then
                ViewState.AjouteValeur("VCacheCollectionAgent", New CacheGestCollectionFiche())
            End If
            Return ViewState.GetValeur(Of CacheGestCollectionFiche)("VCacheCollectionAgent")
        End Get
        Set(value As CacheGestCollectionFiche)
            ViewState.AjouteValeur("VCacheCollectionAgent", value)
        End Set
    End Property

    Private ReadOnly Property ActSelectDansDataGridStagiaire As Action(Of String)
        Get
            Return Sub(Clef)
                       cboAction.SelectedIndex = 0
                       cboAction.Enabled = False
                       Dim dfsi As DonneeForStagiaireInscription = (From d In DonneesGridStagiaire Where d.CleeSelection = Clef Select d).FirstOrDefault()
                       If Not (dfsi Is Nothing) Then
                           LblTitreSelection.Text = "Inscription de " & dfsi.NomPrenom
                       End If
                       If (Not WsForceChargementStagiaire) Then
                           If Clef.Trim() = "" OrElse VCacheCollectionStagiaire.ClefCourante = Clef Then
                               Return
                           End If
                       End If
                       VCacheCollectionStagiaire.ClefCourante = Clef
                       VCacheCollectionStagiaire.Sauve(ViewState)

                       ListeGrilleAgt.ClefCourante = Clef
                       Dim lstfis As List(Of FOR_INSCRIPTIONS) = FormationHelper.GetToutesLesForInscriptionDeStagiaire(IdeStage, dfsi.Nom, dfsi.Prenom, dfsi.DateNaissance)

                       DonneesInscription.Clear()
                       DonneesInscription.AddRange(New ItemInscriptionInfoCollection((From fi In lstfis Order By fi.Date_Valeur_ToDate Select fi).ToList()))
                       DonneesInscription.Sauve(ViewState)

                       Select Case MultiOnglets.ActiveViewIndex
                           Case 2
                               CtrlListeAgt1.Charge(Nothing)
                           Case 3
                               CtrlListeAgt2.Charge(Nothing)
                           Case 9
                               CtrlListeAgt3.Charge(Nothing)
                       End Select

                   End Sub
        End Get
    End Property

    Private Sub ChargeApprocheStagiaire()
        MultiVueGrille.ActiveViewIndex = 1
        btnVue.Text = "Par session"
        If (MultiOnglets.ActiveViewIndex <> 2 AndAlso MultiOnglets.ActiveViewIndex <> 3 AndAlso MultiOnglets.ActiveViewIndex <> 9) Then
            MultiOnglets.ActiveViewIndex = 2
            CellDataGrid.Visible = True
            tbMenus.Visible = True
            TbEdition.Visible = True
            CellTotaux.Visible = True
            btnVue.Visible = True

            NumVueAnc = 2
        End If
        TbEdition.Visible = False
        cboAction.SelectedIndex = 0
        cboAction.Enabled = False

        VCacheCollectionStagiaire.ClefCourante = ""
        VCacheCollectionStagiaire.Sauve(ViewState)

        CtrlListeAgt1.ViderDetail(True)
        CtrlListeAgt2.ViderDetail()
        CtrlListeAgt2.ViderDetail(True)

        ChargeCacheStagiaire(True)
    End Sub

    Private Sub InitDataGridStagiaire()
        If Not (IsPostBack) Then
            ListeGrilleAgt.Captions = New List(Of String)()
            ListeGrilleAgt.Captions.Add("Aucun stagiaire")
            ListeGrilleAgt.Captions.Add("Un stagiaire")
            ListeGrilleAgt.Captions.Add("stagiaires")

            ListeGrilleAgt.Colonnes = New DonneeColonneCollection()

            Dim dc As DonneeColonne
            dc = New DonneeColonne()
            dc.Alignement = HorizontalAlign.Left
            dc.Libelle = "Stagiaire"
            dc.Propriete = "NomPrenom"
            dc.TypeDonnee = TypeData.Chaine
            ListeGrilleAgt.Colonnes.Add(dc)

            dc = New DonneeColonne()
            dc.Alignement = HorizontalAlign.Center
            dc.Libelle = "Nombre d'inscriptions"
            dc.Propriete = "NbInscription"
            dc.TypeDonnee = TypeData.Chaine
            ListeGrilleAgt.Colonnes.Add(dc)
        End If

        ListeGrilleAgt.Act_SelectDataDansDataGrid = ActSelectDansDataGridStagiaire
        ListeGrilleAgt.EndInit()

    End Sub

    Private Sub ChargeCacheStagiaire(ByVal chargement As Boolean)
        DonneesGridStagiaire.Vider(ViewState)
        DonneesInscription.Clear()
        DonneesInscription.Sauve(ViewState)

        MoteurInscription.ReinitStructureSeulement()

        Dim strucf As StructureFormationInscription = TryCast(MoteurInscription.GetStructure(IdeStage), StructureFormationInscription)

        If (Not (strucf Is Nothing) AndAlso strucf.Inscriptions.Count > 0) Then
            Dim lsttmp As List(Of DonneeForStagiaireInscription) = DonneeForStagiaireInscriptionCollection.ConstruitListe(MoteurInscription, strucf.Inscriptions.GetFichesTypees())
            DonneesGridStagiaire.AddRange(lsttmp)
            DonneesGridStagiaire.Sauve(ViewState)
        End If

        ChargeTotaux(strucf)

        ChargeGrilleStagiaire(chargement)
    End Sub

    Private Sub ChargeGrilleStagiaire(ByVal chargement As Boolean)
        If (DonneesGridStagiaire.Count <= 0) Then
            ListeGrilleAgt.Charge(Nothing)
            Return
        End If

        Dim donnees As List(Of ForStagiaireInscription) = New List(Of ForStagiaireInscription)()
        DonneesGridStagiaire.ForEach(Sub(d)
                                         donnees.Add(New ForStagiaireInscription(d))
                                     End Sub)

        VCacheCollectionStagiaire.Datas.AddRange(donnees)
        VCacheCollectionStagiaire.Sauve(ViewState)

        ListeGrilleAgt.ClefCourante = ""
        ListeGrilleAgt.Charge(donnees)

        If (chargement) Then
            ActSelectDansDataGridStagiaire()(ListeGrilleAgt.PremiereClef)
            Return
        End If

        WsForceChargementStagiaire = True

        If (ListeGrilleAgt.ListClefs.Contains(VCacheCollectionStagiaire.ClefCourante)) Then
            ActSelectDansDataGridStagiaire()(VCacheCollectionStagiaire.ClefCourante)
            Return
        End If

        ActSelectDansDataGridStagiaire()(ListeGrilleAgt.PremiereClef)

    End Sub

End Class
