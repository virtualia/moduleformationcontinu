﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing

Public Class CtrlListeVue3
    Inherits UserControl
    Implements IControlBase

    Private WsCouleurLigne As Color = VisuHelper.ConvertiCouleur("#E2E2E2")
    Private WsCouleurLigne_InscPerson As Color = VisuHelper.ConvertiCouleur("#B0E0D7")
    Private WsCouleurBouton As Color = Color.LightGray

    Private WslstBtn As List(Of Button) = New List(Of Button)()
    Private WslstChk As List(Of CheckBox) = New List(Of CheckBox)()

    Private WsCtrlGestion As CtrlGestionListeInscription

    Public WriteOnly Property CtrlGestion As CtrlGestionListeInscription
        Set(value As CtrlGestionListeInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        ConstruitControles()
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        ChkSelection.Checked = False
        ConstruitControles()
        LblTotDossier.Text = WsCtrlGestion.GetTotal("NB")
    End Sub

    Private Sub ConstruitControles()
        If WsCtrlGestion Is Nothing Then
            Return
        End If
        WslstBtn = New List(Of Button)()
        WslstChk = New List(Of CheckBox)()

        ViderDetail()

        For Each iti In WsCtrlGestion.DonneesInscription
            ConstruitLigneDetail(iti)
        Next
        If WsCtrlGestion.V_SiEnLecture = True Then
            For Each Ck As CheckBox In WslstChk
                Ck.Enabled = False
            Next
            For Each Bt As Button In WslstBtn
                Bt.Enabled = False
            Next
        Else
            WslstChk.ForEach(Sub(ck As CheckBox)
                                 AddHandler ck.CheckedChanged, AddressOf ChkClick
                             End Sub)
            WslstBtn.ForEach(Sub(bt As Button)
                                 AddHandler bt.Click, AddressOf BtnClick
                             End Sub)
        End If
    End Sub

    Private Sub ConstruitLigneDetail(iti As ItemInscriptionInfo)
        Dim Rangee As TableRow = New TableRow()
        Rangee.ID = "RDetail" & iti.Index

        If Not (iti.EstPersonnalise) Then
            Rangee.BackColor = WsCouleurLigne
        Else
            Rangee.BackColor = WsCouleurLigne_InscPerson
        End If

        Rangee.Cells.Add(ConstructionCell.ConstruitCellChk(iti, WslstChk))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellBouton(iti, WslstBtn, WsCouleurBouton))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "NomPrenom", "25%", "230px"))

        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Contexte", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Cursus", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Date_Insc", "10%", "80px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Demande_Heber", "10%", "80px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Priorite", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Presence", "20%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Filiere", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Module_Formation", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Nb_Heure_Dep", "10%", "80px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Organisme", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Organisme_Autres", "10%", "215px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Organisme_Peda", "10%", "215px"))


        TbDetail.Rows.Add(Rangee)

    End Sub

    Public Sub ViderDetail(Optional ViderTotal As Boolean = False)
        Dim lstrows As List(Of TableRow) = (From r In TbDetail.Rows Where DirectCast(r, TableRow).ID <> "RDetail" Select DirectCast(r, TableRow)).ToList()
        For Each Rangee In lstrows
            TbDetail.Rows.Remove(Rangee)
        Next
        If Not (ViderTotal) Then
            Return
        End If
        LblTotDossier.Text = ""
    End Sub

    Private Sub ChkClick(s As Object, ev As EventArgs)
        ConstructionCell.EvenChk(DirectCast(s, CheckBox), WsCtrlGestion)
        ChkSelection.Checked = WsCtrlGestion.DonneesInscription.Count = WslstChk.Where(Function(ck)
                                                                                           Return ck.Checked
                                                                                       End Function).Count()
    End Sub

    Private Sub BtnClick(s As Object, ev As EventArgs)
        ConstructionCell.EvenBtn(DirectCast(s, Button), WsCtrlGestion)
    End Sub

    Protected Sub ChkSelection_CheckedChanged(sender As Object, e As EventArgs)
        Dim SiSel As Boolean = ChkSelection.Checked
        If WslstChk.Count > 0 Then
            For Each Chk In WslstChk
                Chk.Checked = SiSel
                ChkClick(Chk, Nothing)
            Next
        End If
    End Sub

End Class