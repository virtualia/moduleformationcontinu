﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.Net.Session
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Virtualia.Structure.Formation
Imports Virtualia.TablesObjet.ShemaREF
Imports System.Drawing

Public Class CtrlSuppressionInscriptions
    Inherits UserControl
    Implements IControlBase

    Private WsCtrlGestion As CtrlGestionListeInscription

    Public WriteOnly Property CtrlGestion As CtrlGestionListeInscription
        Set(value As CtrlGestionListeInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Private Property Index As Integer
        Get
            If (Not ViewState.KeyExiste("Index")) Then
                ViewState.AjouteValeur("Index", -1)
            End If

            Return DirectCast(ViewState("Index"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("Index", value)
        End Set
    End Property

    Private Property IndexCourant As Integer
        Get
            If (Not ViewState.KeyExiste("IndexCourant")) Then
                ViewState.AjouteValeur("IndexCourant", -1)
            End If

            Return DirectCast(ViewState("IndexCourant"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexCourant", value)
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)
        If TimerTraitement.Enabled = True Then
            DirectCast(Page.Master, VirtualiaMain).SessionCourante.ReinitListeArbo()
            ExecuteSuppression()
        End If
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        Index = 0
        IndexCourant = -1
        txtMsg.Text = ""
        TxtStatut.Text = ""
        TimerTraitement.Enabled = True
    End Sub

    Private Sub ExecuteSuppression()
        Dim NbTot As Integer = WsCtrlGestion.DonneesInscription.Selection.Count
        If (IndexCourant = Index) Then
            Return
        End If
        Dim sep As String = ""
        If (Index > 0) Then
            sep = vbCrLf
        End If
        IndexCourant = Index
        TxtStatut.Text = "Suppression " & Index + 1 & "/" & NbTot

        Dim CRetour As RetAsync = TraiteSuppressionAsync(Index).Result
        txtMsg.Text = txtMsg.Text & sep & CRetour.Retour
        Index = CRetour.Index
        If Index >= NbTot Then
            If NbTot > 0 Then
                TxtStatut.Text = "Suppressions terminées"
            Else
                TxtStatut.Text = "Suppression terminée"
            End If
            WsCtrlGestion.Actualiser = True
            TimerTraitement.Enabled = False
            Index = 0
            IndexCourant = -1
        End If
    End Sub

    Private Async Function TraiteSuppressionAsync(indexdonnee As Integer) As Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(IdxDonnee)
                                                               Return TraiteSuppression(indexdonnee)
                                                           End Function
        Return Await Task.FromResult(Of RetAsync)(FuncTraitement(indexdonnee))
    End Function

    Private Function TraiteSuppression(IdxDonnee As Integer) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()
        CRetour.Index = IdxDonnee + 1

        Dim itemasuppr As ItemInscriptionInfo = WsCtrlGestion.DonneesInscription.Selection(IdxDonnee)
        Dim moteur As GestionInscription = DirectCast(Page, FrmPlanDeFormation).MoteurInscription

        Dim structf As StructureFormationInscription = Nothing
        moteur.ReinitStructureSeulement()
        structf = TryCast(moteur.GetStructure(WsCtrlGestion.IdeStage), StructureFormationInscription)

        If (structf Is Nothing) Then
            CRetour.Index = Integer.MaxValue
            CRetour.Retour = "Le stage n'existe pas ou a été supprimé par un autre utilisateur"
            Return CRetour
        End If

        Dim fi As ForInscriptionInfo = (From f In structf.Inscriptions.GetFichesTypees() _
                                        Where f.Nom_stagiaire = itemasuppr.Nom _
                                              And f.Prenom_stagiaire = itemasuppr.Prenom _
                                              And f.Datenaissance_stagiaire = itemasuppr.DateDeNaissance _
                                              And f.Rang_inscription = itemasuppr.Rang
                                        Select f).FirstOrDefault()

        If fi Is Nothing Then
            CRetour.Retour = itemasuppr.NomPrenom & vbCrLf & vbTab & "Inscription supprimée"
            Return CRetour
        End If

        Dim CRetourserv As RetourCRUD = moteur.Supprime(fi)

        CRetour.Retour = itemasuppr.NomPrenom & vbCrLf & vbTab & "Inscription "
        If CRetourserv.Index < 0 Then
            CRetour.Retour &= "Erreur"
        Else
            CRetour.Retour &= "supprimée"
        End If
        Return CRetour
    End Function

End Class