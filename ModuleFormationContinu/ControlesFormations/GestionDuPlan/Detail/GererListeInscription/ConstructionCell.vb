﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Metier.Formation
Imports System.Drawing

Public Class ConstructionCell

    Public Shared Function ConstruitCellChk(iti As ItemInscriptionInfo, LstChk As List(Of CheckBox)) As TableCell

        Dim cellchk As TableCell = New TableCell() With {.Width = New Unit("65px"), .HorizontalAlign = HorizontalAlign.Center, .VerticalAlign = VerticalAlign.Middle}
        Dim chkcell As CheckBox = New CheckBox() With {.Width = New Unit("65px"), .ID = "CHK_" & iti.Ide & "_" & iti.Rang, .Checked = iti.Selection, .AutoPostBack = True}

        cellchk.Controls.Add(chkcell)

        LstChk.Add(chkcell)

        Return cellchk
    End Function

    Public Shared Function ConstruitCellBouton(iti As ItemInscriptionInfo, lstBtn As List(Of Button), CouleurBouton As Color) As TableCell
        Dim CellTbBtn As TableCell = New TableCell() With {.HorizontalAlign = HorizontalAlign.Center, .VerticalAlign = VerticalAlign.Middle, .Height = New Unit("20px")}

        Dim TbButton As Table = New Table() With {.ID = "TBBtn_" & iti.Index, .Width = New Unit("60px"), .CellPadding = 0, .CellSpacing = 0}
        TbButton.Rows.Add(New TableRow())

        Dim cellbtn As TableCell = New TableCell() With {.VerticalAlign = VerticalAlign.Middle, .HorizontalAlign = HorizontalAlign.Center}
        Dim btn As Button = New Button() With {.ID = "Btn_" & iti.Ide & "_" & iti.Rang, .Width = New Unit("20px"), .Height = New Unit("16px"), .BackColor = CouleurBouton, .BorderStyle = BorderStyle.NotSet, .ToolTip = "Accéder au dossier de la personne"}
        btn.Style.Add(HtmlTextWriterStyle.Cursor, "pointer")
        lstBtn.Add(btn)

        cellbtn.Controls.Add(btn)

        TbButton.Rows(0).Cells.Add(cellbtn)

        CellTbBtn.Controls.Add(TbButton)

        Return CellTbBtn
    End Function

    Public Shared Function ConstruitCellLabel(iti As ItemInscriptionInfo, Propriete As String, WCell As String, wLabel As String, Optional alignement As HorizontalAlign = HorizontalAlign.Left) As TableCell

        Dim cell As TableCell = New TableCell() With {.HorizontalAlign = HorizontalAlign.Center, .VerticalAlign = VerticalAlign.Middle, .Height = New Unit("20px"), .Width = New Unit(WCell)}

        Dim lbl As Label = New Label() With {.ID = "Lbl_" & Propriete & "_" & iti.Index, .Text = iti.GetValeur(Propriete), .Width = New Unit(wLabel), .ToolTip = If(((Propriete = "NomPrenom" OrElse Propriete = "DtesSession") AndAlso iti.EstPersonnalise), "Inscription personnalisée", "")}

        Dim valstyle As String = "left"
        Select Case alignement
            Case HorizontalAlign.Center
                valstyle = "center"
            Case HorizontalAlign.Left
                valstyle = "left"
            Case HorizontalAlign.Right
                valstyle = "right"
        End Select

        lbl.Style.Add(HtmlTextWriterStyle.TextAlign, valstyle)

        cell.Controls.Add(lbl)

        Return cell
    End Function

    Public Shared Function ConstruitCellLabelValeur(valeur As String, index As Integer, WCell As String, wLabel As String, Optional alignement As HorizontalAlign = HorizontalAlign.Left) As TableCell

        Dim cell As TableCell = New TableCell() With {.HorizontalAlign = HorizontalAlign.Center, .VerticalAlign = VerticalAlign.Middle, .Height = New Unit("20px"), .Width = New Unit(WCell)}

        Dim lbl As Label = New Label() With {.ID = "Lbl_aaaaa_" & index, .Text = valeur, .Width = New Unit(wLabel)}

        Dim valstyle As String = "left"
        Select Case alignement
            Case HorizontalAlign.Center
                valstyle = "center"
            Case HorizontalAlign.Left
                valstyle = "left"
            Case HorizontalAlign.Right
                valstyle = "right"
        End Select

        lbl.Style.Add(HtmlTextWriterStyle.TextAlign, valstyle)

        cell.Controls.Add(lbl)

        Return cell

    End Function

    Public Shared Sub EvenChk(ByVal ck As CheckBox, ByVal ctrlgest As CtrlGestionListeInscription)
        Dim Clef As String = ck.ID.Substring(4)
        ctrlgest.GereSelection(Clef, ck.Checked)
    End Sub

    Public Shared Sub EvenBtn(ByVal btn As Button, ByVal ctrlgest As CtrlGestionListeInscription)
        Dim Clef As String = btn.ID.Substring(4)
        ctrlgest.AfficherDossier(Clef)
    End Sub
End Class
