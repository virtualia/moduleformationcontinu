﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlModifInscription.ascx.vb" Inherits="Virtualia.Net.CtrlModifInscription" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Width="600px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
    
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow Height="40px">
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Table ID="Table1" runat="server" Width="400px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                <asp:TableRow >
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Label ID="lblTraitement" runat="server"  Width="300px" Text="XXXXXXX" ForeColor="White" BorderStyle="None" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="tb" runat="server" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px" Width="500px">
                <asp:TableRow ID="RCbo">
                    <asp:TableCell>
                        <asp:Table ID="tbbb" runat="server">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Middle" Width="500px" HorizontalAlign="Center">
                                    <asp:DropDownList ID="cboValeur" runat="server" Width="480px" AutoPostBack="true" ForeColor="#124545" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="cboValeur_SelectedIndexChanged" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Table ID="TbMotif" runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Center">
                                                        <asp:Label ID="Label1" runat="server" Text="Motif de non participation" Width="320px" BackColor="Transparent" BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-align: center;" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell VerticalAlign="Middle" Width="500px" HorizontalAlign="Center">
                                                        <asp:DropDownList ID="cboMotif" runat="server" Width="480px" AutoPostBack="true" ForeColor="#124545" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" OnSelectedIndexChanged="cboMotif_SelectedIndexChanged" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="cboValeur" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="RTxt">
                    <asp:TableCell VerticalAlign="Middle" Width="500px" HorizontalAlign="Center">
                        <asp:Table ID="tb1" runat="server" Width="120px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="txtValeur" runat="server" V_Nature="2" V_Format="22" V_SiDonneeDico="false" V_SiAutoPostBack="true"
                                        EtiVisible="false" DonWidth="80px" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:Label ID="lblSuffixe" runat="server" Text="h" Height="20px" Width="30px" BackColor="Transparent" 
                                        BorderStyle="none" BorderWidth="0px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" 
                                        Font-Size="Small" Style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 1px; text-align: left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:UpdatePanel ID="upd2" runat="server">
                            <ContentTemplate>
                                <Generic:BTN_GENE ID="BtnTrait" runat="server" Text="OK" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboValeur" />
                                <asp:AsyncPostBackTrigger ControlID="cboMotif" />
                                <asp:AsyncPostBackTrigger ControlID="txtValeur" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:UpdatePanel ID="UpPnlMessage" runat="server">
                <ContentTemplate>
                    <asp:Timer ID="TimerTraitement" runat="server" Interval="50" Enabled="false" />
                    <asp:Table ID="TbMessage" runat="server" Visible="true" Width="700px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                        <asp:TableRow>
                            <asp:TableCell Height="2px" />
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:TextBox ID="TxtStatut" runat="server" Height="20px" Width="680px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:TextBox ID="txtMsg" runat="server" Height="400px" Width="680px" TextMode="MultiLine" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="50px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false">
                                <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                    <ProgressTemplate>
                                        <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                                </asp:TableCell>
                                                <asp:TableCell Width="10px" Height="10px" />
                                                <asp:TableCell>
                                                    <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="BtnTrait" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
