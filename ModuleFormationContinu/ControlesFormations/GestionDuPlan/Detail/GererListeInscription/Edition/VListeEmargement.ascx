﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VListeEmargement.ascx.vb" Inherits="Virtualia.Net.VListeEmargement" %>

<style type="text/css">
.EtiquetteLeft
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:20px;
        margin-top:2px;
        text-align:left;
        text-indent:0px;
        width:450px;
    }
.EtiquetteRight
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:20px;
        margin-top:2px;
        text-align:right;
        text-indent:0px;
        width:450px;
    }
.CellCalendrier
    {
        background-color:white;
        border-color:black;
        border-style:solid;
        border-width:1px;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        height:30px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:180px;
    }
.EtiCalendrier
    {
        background-color:transparent;
        border-color:black;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:22px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:180px;
    }
.CellStagiaire
    {
        background-color:white;
        border-color:black;
        border-style:none;
        border-left-style:solid;
        border-width:1px;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        height:30px;
        margin-bottom:0px;
        margin-left:0px;
        margin-right:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
        width:180px;
    }
</style>

<asp:Table ID="CadreEmargement" runat="server" Width="750px" HorizontalAlign="Center">
     <asp:TableRow>
        <asp:TableCell Height="60px" HorizontalAlign="Center">
            <asp:Label ID="EtiConvocation" runat="server" Text="FEUILLE D'EMARGEMENT" Width="400px" 
                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Larger" Font-Italic="False"
                BorderStyle="None" style="text-align:center" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell>
            <asp:Table ID="CadreStage" runat="server" Width="740px" Height="60px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiFormation" runat="server" Text="" CssClass="EtiquetteLeft" Font-Size="Medium" 
                            Font-Bold="true" style="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDateDebut" runat="server" CssClass="EtiquetteLeft" Text=""/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiIntervenant" runat="server" CssClass="EtiquetteLeft" Text=""/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="200px" VerticalAlign="Top" HorizontalAlign="Center">
            <asp:Table ID="CadreCalendrier" runat="server" Width="740px" CellPadding="0" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell CssClass="CellCalendrier" Width="380px">
                        <asp:Label ID="EtiStagiaire" runat="server" Text="STAGIAIRE" CssClass="EtiCalendrier" Width="380px" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Width="180px">
                        <asp:Label ID="EtiMatin" runat="server" Text="MATIN" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                    <asp:TableCell CssClass="CellCalendrier" Width="178px">
                        <asp:Label ID="EtiAM" runat="server" Text="APRES-MIDI" CssClass="EtiCalendrier" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_01" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_01" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_01" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_01" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_01" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_01" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_01" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_01" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_01" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_01" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_02" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_02" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_02" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_02" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_02" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_02" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_02" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_02" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_02" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_02" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_03" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_03" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_03" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_03" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_03" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_03" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_03" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_03" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_03" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_03" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_04" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_04" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_04" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_04" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_04" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_04" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_04" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_04" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_04" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_04" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_05" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_05" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_05" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_05" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_05" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_05" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_05" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_05" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_05" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_05" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_06" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_06" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_06" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_06" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_06" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_06" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_06" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_06" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_06" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_06" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_07" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_07" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_07" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_07" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_07" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_07" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_07" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_07" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_07" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_07" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_08" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_08" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_08" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_08" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_08" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_08" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_08" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_08" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_08" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_08" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_09" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_09" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_09" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_09" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_09" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_09" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_09" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_09" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_09" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_09" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="3">
                        <asp:Table ID="CadreStagiaire_10" runat="server" Width="740px" Height="70px" CellPadding="0" CellSpacing="0" BorderStyle="Solid"
                             BorderColor="Black" BorderWidth="1px">
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L1_NomPrenomStagiaire_10" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_Matin_10" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L1_AM_10" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L2_Niveau1_10" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_Matin_10" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L2_AM_10" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="380px">
                                    <asp:Label ID="L3_Niveau2_10" runat="server" Text="" CssClass="EtiCalendrier" Width="380px" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_Matin_10" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                                <asp:TableCell CssClass="CellStagiaire">
                                    <asp:Label ID="L3_AM_10" runat="server" Text="" CssClass="EtiCalendrier" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow> 
</asp:Table>

