﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtrlAttestations

    '''<summary>
    '''Contrôle Cadre_Edition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cadre_Edition As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Attestation_001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_001 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_002.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_002 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_003.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_003 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_004.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_004 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_005.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_005 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_006.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_006 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_007.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_007 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_008.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_008 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_009.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_009 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_010.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_010 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_011.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_011 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_012.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_012 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_013.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_013 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_014.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_014 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_015.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_015 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_016.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_016 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_017.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_017 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_018.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_018 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_019.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_019 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_020.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_020 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_021.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_021 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_022.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_022 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_023.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_023 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_024.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_024 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_025.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_025 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_026.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_026 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_027.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_027 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_028.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_028 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_029.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_029 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_030.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_030 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_031.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_031 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_032.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_032 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_033.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_033 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_034.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_034 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_035.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_035 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_036.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_036 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_037.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_037 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_038.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_038 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_039.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_039 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_040.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_040 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_041.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_041 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_042.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_042 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_043.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_043 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_044.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_044 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_045.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_045 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_046.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_046 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_047.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_047 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_048.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_048 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_049.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_049 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_050.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_050 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_051.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_051 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_052.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_052 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_053.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_053 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_054.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_054 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_055.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_055 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_056.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_056 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_057.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_057 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_058.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_058 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_059.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_059 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_060.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_060 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_061.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_061 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_062.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_062 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_063.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_063 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_064.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_064 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_065.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_065 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_066.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_066 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_067.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_067 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_068.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_068 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_069.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_069 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_070.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_070 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_071.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_071 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_072.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_072 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_073.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_073 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_074.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_074 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_075.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_075 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_076.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_076 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_077.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_077 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_078.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_078 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_079.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_079 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_080.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_080 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_081.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_081 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_082.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_082 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_083.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_083 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_084.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_084 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_085.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_085 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_086.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_086 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_087.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_087 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_088.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_088 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_089.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_089 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_090.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_090 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_091.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_091 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_092.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_092 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_093.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_093 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_094.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_094 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_095.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_095 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_096.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_096 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_097.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_097 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_098.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_098 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_099.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_099 As Global.Virtualia.Net.VAttestation

    '''<summary>
    '''Contrôle Attestation_100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Attestation_100 As Global.Virtualia.Net.VAttestation
End Class
