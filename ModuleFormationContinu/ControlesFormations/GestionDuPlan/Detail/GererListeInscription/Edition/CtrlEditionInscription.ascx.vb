﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.Evenements

Public Class CtrlEditionInscription
    Inherits UserControl
    Implements IControlBase
    Private WsCtrlGestion As CtrlGestionListeInscription
    '** RP 
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Private Property NomFichier As String
        Get
            If Not (ViewState.KeyExiste("NomFichier")) Then
                ViewState.AjouteValeur("NomFichier", "")
            End If
            Return DirectCast(ViewState("NomFichier"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("NomFichier", value)
        End Set
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionListeInscription
        Set(value As CtrlGestionListeInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
    End Sub

    Public Sub Charge(ByVal Items As Object) Implements IControlBase.Charge
        CmdPDF.Enabled = True
        CadreCommandes.Visible = True
        EtiTitre.Visible = True
        CadreApercu.BackColor = Drawing.Color.White
        Select Case Items.ToString()
            Case "La liste d'émargement"
                NomFichier = "ListeEmargement"
                EtiTitre.Text = "Aperçu de la liste d'émargement"
                Emargement.DonneesEdition = CreerDonneesEdition()
                MultiVues.SetActiveView(VueEmargements)
            Case "Les attestations de stage"
                NomFichier = "Attestation"
                EtiTitre.Text = "Aperçu des attestations de stage"
                Attestation.DonneesEdition = CreerDonneesEdition()
                MultiVues.SetActiveView(VueAttestations)
            Case "Les convocations"
                NomFichier = "Convocation"
                EtiTitre.Text = "Aperçu des convocations"
                Convocation.DonneesEdition = CreerDonneesEdition()
                MultiVues.SetActiveView(VueConvocations)
            Case "Les convocations par email"
                CadreCommandes.Visible = False
                EtiTitre.Visible = False
                CadreApercu.BackColor = Drawing.Color.Transparent
                NewConvocation.V_DonneesEdition = CreerDonneesEdition()
                MultiVues.SetActiveView(VueEmailConvocation)
        End Select
        OnLoad(EventArgs.Empty)
    End Sub

    Private Function CreerDonneesEdition() As DonneeEditionInfo
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim LstDatas As DonneeEditionInfo = New DonneeEditionInfo()
        Dim StructFInscr As StructureFormationInscription = TryCast(WsCtrlGestion.MoteurInscription.GetStructure(WsCtrlGestion.IdeStage), StructureFormationInscription)
        If StructFInscr Is Nothing Then
            Return Nothing
        End If
        Dim LstValeursComptaValides As List(Of String) = FormationHelper.GetvaleurDeComptaEtDeValidation()

        LstDatas.Stage.Intitule = StructFInscr.Identification.Intitule

        Dim SiPremier As Boolean = True
        Dim Stagiaire As DonneeStagiaireInfo
        Dim LstValeurs As List(Of String)
        Dim IdeDossier As Integer
        Dim LDGlobales As Virtualia.Net.Session.LDObjetGlobal = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)

        For Each it In WsCtrlGestion.DonneesInscription.Where(Function(Element)
                                                                  Return LstValeursComptaValides.Contains(Element.Suivi.ToLower()) And Element.Selection = True
                                                              End Function).OrderBy(Function(Element)
                                                                                        Return Element.NomPrenom
                                                                                    End Function)

            IdeDossier = LDGlobales.VirIdentifiantPER(it.Nom, it.Prenom, it.DateDeNaissance)
            If IdeDossier <= 0 Then
                Continue For
            End If
            If SiPremier = True Then
                SiPremier = False
                LstDatas.FicheInscription = it
                LstDatas.Stage.DateDeb = it.DateDebSession
                LstDatas.Stage.DateFin = it.DateFinSession
                '** RP
                Dim FicheIDENT As FOR_IDENTIFICATION = FormationHelper.GetIdentificationDeStage(WsCtrlGestion.IdeStage)
                If FicheIDENT IsNot Nothing Then
                    LstDatas.Stage.Domaine = FicheIDENT.Domaine
                End If
                '**
                Dim FicheREF As FOR_SESSION = FormationHelper.GetSessionDeInscription(WsCtrlGestion.IdeStage,
                                                WebFct.ViRhDates.DateTypee(it.DateDebSession), WebFct.ViRhDates.DateTypee(it.DateFinSession), it.Rang_Session)

                If FicheREF IsNot Nothing Then
                    LstDatas.Organisme.Organisme = If(FicheREF.OrganismePrestataire.Trim() <> "", FicheREF.OrganismePrestataire.Trim(),
                                                          StructFInscr.Identification.Organisme)

                    LstDatas.Stage.H_AM_Deb = FicheREF.HeureDebut_Matin
                    LstDatas.Stage.H_AM_Fin = FicheREF.HeureFin_Matin
                    LstDatas.Stage.H_PM_Deb = FicheREF.HeureDebut_ApresMidi
                    LstDatas.Stage.H_PM_Fin = FicheREF.HeureFin_ApresMidi
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                    LstDatas.Stage.NbHeure = WebFct.ViRhFonction.MontantEdite(FicheREF.DureeSession_Heures, 2)

                    LstDatas.LieuFormation.Stage = FicheREF.LieuFormation
                    LstDatas.LieuFormation.Session = FicheREF.LieuSession
                    LstDatas.LieuFormation.Adr = FicheREF.AdresseduStage
                    LstDatas.LieuFormation.CP = If(FicheREF.CodePostal.Trim() = "0", "", FicheREF.CodePostal.Trim())
                    LstDatas.LieuFormation.Ville = FicheREF.VilleAdresse
                End If
            End If

            Stagiaire = New DonneeStagiaireInfo() With {.Identifiant = IdeDossier, .Nom = it.Nom, .Prenom = it.Prenom}

            Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
            Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
            LstValeurs = Requeteur.Affectation_Un_Agent(IdeDossier, it.DateDebSession)
            If LstValeurs IsNot Nothing AndAlso LstValeurs.Count > 0 Then
                Stagiaire.Niveau1 = LstValeurs(0)
                Stagiaire.Service = GetService(LstValeurs)
                Stagiaire.EmploiExerce = LstValeurs(6)

                Stagiaire.RueCollectivite = LstValeurs(7)
                Stagiaire.CPCollectivite = LstValeurs(8)
                Stagiaire.BureauDistribCollectivite = LstValeurs(9)
                Stagiaire.Tel = LstValeurs(10)
                Stagiaire.Fax = LstValeurs(11)
            End If

            LstDatas.ListeStagiaires.Add(Stagiaire)
        Next
        Return LstDatas
    End Function

    Private Function GetService(ByVal LstValeurs As List(Of String)) As String
        Dim IndiceI As Integer
        If LstValeurs.Count <= 0 Then
            Return ""
        End If
        Dim LstTmp As List(Of String) = LstValeurs.Skip(1).Take(5).ToList()

        For IndiceI = LstTmp.Count - 1 To 0 Step -1
            If LstTmp(IndiceI).Trim() <> "" Then
                Return LstTmp(IndiceI).Trim()
            End If
        Next
        Return ""
    End Function

    Private Sub CmdPDF_Click(sender As Object, e As System.EventArgs) Handles CmdPDF.Click
        Dim FichierHtml As String
        Dim Util As Virtualia.Systeme.Outils.Utilitaires
        Dim FluxHtml As String
        Dim FluxPDF As Byte()

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        FichierHtml = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirRepertoireTemporaire
        FichierHtml &= "\" & Session.SessionID & ".html"

        If DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition Is Nothing Then
            Exit Sub
        End If
        Util = New Virtualia.Systeme.Outils.Utilitaires
        Select Case NomFichier
            Case "Attestation"
                Call Util.SourceHtmlEditee(FichierHtml, WebFct.VirSourceHtml(Me.VueAttestations))
            Case "Convocation"
                Call Util.SourceHtmlEditee(FichierHtml, WebFct.VirSourceHtml(Me.VueConvocations))
            Case "ListeEmargement"
                Call Util.SourceHtmlEditee(FichierHtml, WebFct.VirSourceHtml(Me.VueEmargements))
        End Select
        FluxHtml = My.Computer.FileSystem.ReadAllText(FichierHtml, System.Text.Encoding.UTF8)
        FluxPDF = DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase).SrvOutils.ConversionPDF(FluxHtml, False)
        If FluxPDF IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & ".pdf" & "; size=" & FluxPDF.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxPDF)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        MultiVues.SetActiveView(VueEmailConvocation)
    End Sub

    Private Sub NewConvocation_MessageDialogue(sender As Object, e As MessageSaisieEventArgs) Handles NewConvocation.MessageDialogue
        MsgVirtualia.AfficherMessage = e
        MultiVues.SetActiveView(VueMessage)
    End Sub
End Class