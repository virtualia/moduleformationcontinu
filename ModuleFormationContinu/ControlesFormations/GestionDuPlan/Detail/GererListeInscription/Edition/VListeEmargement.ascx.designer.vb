﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VListeEmargement

    '''<summary>
    '''Contrôle CadreEmargement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEmargement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiConvocation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiConvocation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStage As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiFormation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiDateDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiIntervenant.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiIntervenant As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreCalendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCalendrier As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiStagiaire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiStagiaire As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_01 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_01 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_02 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_02 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_03 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_03 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_04 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_04 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_05 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_05 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_06 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_06 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_07 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_07 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_08 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_08 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_09 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_09 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStagiaire_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStagiaire_10 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle L1_NomPrenomStagiaire_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_NomPrenomStagiaire_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_Matin_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_Matin_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_AM_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_AM_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Niveau1_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Niveau1_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_Matin_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_Matin_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L2_AM_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L2_AM_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Niveau2_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Niveau2_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_Matin_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_Matin_10 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L3_AM_10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L3_AM_10 As Global.System.Web.UI.WebControls.Label
End Class
