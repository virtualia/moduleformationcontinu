﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtrlEmargements

    '''<summary>
    '''Contrôle Cadre_Edition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cadre_Edition As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Emargement_001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_001 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_002.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_002 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_003.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_003 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_004.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_004 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_005.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_005 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_006.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_006 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_007.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_007 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_008.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_008 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_009.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_009 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_010.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_010 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_011.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_011 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_012.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_012 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_013.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_013 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_014.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_014 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_015.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_015 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_016.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_016 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_017.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_017 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_018.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_018 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_019.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_019 As Global.Virtualia.Net.VListeEmargement

    '''<summary>
    '''Contrôle Emargement_020.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Emargement_020 As Global.Virtualia.Net.VListeEmargement
End Class
