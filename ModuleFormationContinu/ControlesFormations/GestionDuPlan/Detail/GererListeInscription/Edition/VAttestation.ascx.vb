﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VAttestation
    Inherits System.Web.UI.UserControl

    Public WriteOnly Property Fiche(ByVal Index As Integer) As DonneeEditionInfo
        Set(value As DonneeEditionInfo)
            EtiTitre.Text = "Formation continue et concours"
            EtiEtablissement.Text = value.ListeStagiaires.Item(Index).Niveau1
            EtiEtaNoEtNomRue.Text = value.ListeStagiaires.Item(Index).RueCollectivite
            EtiEtaVille.Text = value.ListeStagiaires.Item(Index).CpVilleCollectivite
            EtiEtaTelephone.Text = value.ListeStagiaires.Item(Index).STel

            EtiDateEdition.Text = "Le " & Now.ToLongDateString
            EtiIndNomPrenom.Text = value.ListeStagiaires.Item(Index).QualNomPrenom
            EtiIndNiveau1.Text = value.ListeStagiaires.Item(Index).Niveau1
            EtiIndNiveau2.Text = value.ListeStagiaires.Item(Index).Service

            EtiStagiaire.Text = value.ListeStagiaires.Item(Index).QualNomPrenom
            EtiFormation.Text = value.Stage.Intitule
            EtiDates.Text = "du " & value.Stage.DateDeb & " au " & value.Stage.DateFin

            EtiLieuFormation.Text = value.LieuFormation.Stage
            EtiAdresseFormation.Text = value.LieuFormation.Adr
            EtiVilleFormation.Text = value.LieuFormation.CP & Strings.Space(1) & value.LieuFormation.Ville

        End Set
    End Property

End Class