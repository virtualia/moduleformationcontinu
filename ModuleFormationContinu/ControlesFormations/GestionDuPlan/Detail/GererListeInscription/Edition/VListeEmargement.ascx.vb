﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VListeEmargement
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public WriteOnly Property Fiche(ByVal NoPage As Integer, ByVal DateValeur As String) As DonneeEditionInfo
        Set(value As DonneeEditionInfo)
            Dim Ctl As Control
            Dim VirCadreStagiaire As System.Web.UI.WebControls.Table
            Dim IndiceI As Integer

            EtiFormation.Text = value.Stage.Intitule & " Session du " & DateValeur
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            IndiceI = 0
            Do
                Ctl = WebFct.VirWebControle(Me.CadreEmargement, "CadreStagiaire_", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirCadreStagiaire = CType(Ctl, System.Web.UI.WebControls.Table)
                VirCadreStagiaire.Visible = False
                If IndiceI + (NoPage * 10) < value.ListeStagiaires.Count Then
                    VirCadreStagiaire.Visible = True

                    Ctl = WebFct.VirWebControle(VirCadreStagiaire, "L1_NomPrenomStagiaire_", 0)
                    If Ctl IsNot Nothing Then
                        CType(Ctl, System.Web.UI.WebControls.Label).Text = value.ListeStagiaires.Item(IndiceI + (NoPage * 10)).QualNomPrenom
                    End If
                    Ctl = WebFct.VirWebControle(VirCadreStagiaire, "L2_Niveau1_", 0)
                    If Ctl IsNot Nothing Then
                        CType(Ctl, System.Web.UI.WebControls.Label).Text = value.ListeStagiaires.Item(IndiceI + (NoPage * 10)).Niveau1
                    End If
                    Ctl = WebFct.VirWebControle(VirCadreStagiaire, "L3_Niveau2_", 0)
                    If Ctl IsNot Nothing Then
                        CType(Ctl, System.Web.UI.WebControls.Label).Text = value.ListeStagiaires.Item(IndiceI + (NoPage * 10)).Service
                    End If
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

End Class