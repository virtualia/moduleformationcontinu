﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlEmargements
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public Property DonneesEdition As Virtualia.Net.DonneeEditionInfo
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition
        End Get
        Set(value As Virtualia.Net.DonneeEditionInfo)
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition = value
        End Set
    End Property

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Call CreerLesElements()
    End Sub

    Private Sub CreerLesElements()
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.VListeEmargement
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim NoPage = 0
        Dim LstDates As List(Of String)
        Dim DateW As Date

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If DonneesEdition.ListeStagiaires Is Nothing Then
            Exit Sub
        End If
        LstDates = New List(Of String)
        DateW = CDate(DonneesEdition.Stage.DateDeb)
        Do
            LstDates.Add(FormatDateTime(DateW, DateFormat.ShortDate))
            DateW = CDate(LstDates.Item(LstDates.Count - 1)).AddDays(1)
            If DateW > CDate(DonneesEdition.Stage.DateFin) Then
                Exit Do
            End If
        Loop
        IndiceK = 0
        For Each DteValeur In LstDates
            IndiceI = IndiceK
            NoPage = 0
            Do
                Ctl = WebFct.VirWebControle(Me.Cadre_Edition, "Emargement_", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.VListeEmargement)
                VirControle.Visible = False
                If (NoPage * 10) < DonneesEdition.ListeStagiaires.Count Then
                    VirControle.Fiche(NoPage, DteValeur) = DonneesEdition
                    VirControle.Visible = True
                    NoPage += 1
                    IndiceK += 1
                End If
                IndiceI += 1
            Loop
        Next
    End Sub
End Class