﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtrlConvocations

    '''<summary>
    '''Contrôle Cadre_Edition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cadre_Edition As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Convocation_001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_001 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_002.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_002 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_003.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_003 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_004.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_004 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_005.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_005 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_006.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_006 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_007.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_007 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_008.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_008 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_009.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_009 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_010.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_010 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_011.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_011 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_012.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_012 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_013.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_013 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_014.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_014 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_015.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_015 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_016.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_016 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_017.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_017 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_018.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_018 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_019.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_019 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_020.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_020 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_021.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_021 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_022.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_022 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_023.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_023 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_024.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_024 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_025.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_025 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_026.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_026 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_027.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_027 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_028.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_028 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_029.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_029 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_030.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_030 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_031.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_031 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_032.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_032 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_033.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_033 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_034.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_034 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_035.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_035 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_036.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_036 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_037.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_037 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_038.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_038 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_039.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_039 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_040.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_040 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_041.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_041 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_042.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_042 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_043.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_043 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_044.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_044 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_045.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_045 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_046.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_046 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_047.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_047 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_048.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_048 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_049.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_049 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_050.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_050 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_051.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_051 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_052.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_052 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_053.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_053 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_054.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_054 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_055.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_055 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_056.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_056 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_057.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_057 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_058.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_058 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_059.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_059 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_060.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_060 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_061.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_061 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_062.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_062 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_063.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_063 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_064.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_064 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_065.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_065 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_066.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_066 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_067.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_067 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_068.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_068 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_069.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_069 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_070.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_070 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_071.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_071 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_072.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_072 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_073.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_073 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_074.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_074 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_075.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_075 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_076.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_076 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_077.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_077 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_078.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_078 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_079.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_079 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_080.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_080 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_081.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_081 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_082.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_082 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_083.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_083 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_084.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_084 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_085.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_085 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_086.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_086 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_087.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_087 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_088.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_088 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_089.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_089 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_090.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_090 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_091.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_091 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_092.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_092 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_093.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_093 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_094.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_094 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_095.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_095 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_096.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_096 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_097.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_097 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_098.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_098 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_099.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_099 As Global.Virtualia.Net.VConvocation

    '''<summary>
    '''Contrôle Convocation_100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Convocation_100 As Global.Virtualia.Net.VConvocation
End Class
