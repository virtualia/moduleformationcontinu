﻿Option Strict On
Option Explicit On
Option Compare Text

Public Enum EtenduStage
    MATINEE
    APRESMIDI
    JOURNEE
End Enum

Public Class DonneeEditionInfo
    Public Property FicheInscription As Virtualia.Metier.Formation.ItemInscriptionInfo
    Public Property Stage As DonneeStageInfo = New DonneeStageInfo()
    Public Property LieuFormation As DonneeLieuInfo = New DonneeLieuInfo()
    Public Property Organisme As DonneeOrganismeInfo = New DonneeOrganismeInfo()
    Public Property ListeStagiaires As List(Of DonneeStagiaireInfo) = New List(Of DonneeStagiaireInfo)()
    Public Property Responsable As String = ""
End Class

Public Class DonneeStageInfo
    Public Property Intitule As String = ""
    Public Property Domaine As String = ""
    Public Property DateDeb As String = ""
    Public Property DateFin As String = ""

    Public Property H_AM_Deb As String = ""
    Public Property H_AM_Fin As String = ""
    Public Property H_PM_Deb As String = ""
    Public Property H_PM_Fin As String = ""
    Public Property NbHeure As String = ""

    Public ReadOnly Property Etendu As EtenduStage
        Get
            Dim test As String = ""

            test = test & If(H_AM_Deb = "", "0", "1")
            test = test & If(H_AM_Fin = "", "0", "1")
            test = test & If(H_PM_Deb = "", "0", "1")
            test = test & If(H_PM_Fin = "", "0", "1")

            Dim CRetour As EtenduStage = EtenduStage.JOURNEE

            Select Case test
                Case "0000"
                    CRetour = EtenduStage.JOURNEE
                Case "1100"
                    CRetour = EtenduStage.MATINEE
                Case "0011"
                    CRetour = EtenduStage.APRESMIDI
                Case "1001"
                    CRetour = EtenduStage.JOURNEE
                Case "1111"
                    CRetour = EtenduStage.JOURNEE
            End Select

            Return CRetour
        End Get
    End Property

    Public ReadOnly Property LibDates As String
        Get
            If DateDeb = DateFin Then
                Return "le " & DateDeb
            End If
            Return "du " & DateDeb & " au " & DateFin
        End Get
    End Property
End Class

Public Class DonneeOrganismeInfo
    Public Property Organisme As String = ""
    Public Property Formateur As String = ""

    Public ReadOnly Property OrgaFormateur As String
        Get
            Dim CRetour As String = ""
            Dim Sep As String = ""
            If Organisme.Trim() <> "" Then
                CRetour = Organisme.Trim()
                Sep = ", "
            End If
            If Formateur.Trim() <> "" Then
                CRetour &= Sep & Formateur.Trim()
            End If
            Return CRetour.Trim()
        End Get
    End Property
End Class

Public Class DonneeLieuInfo
    Public Property Stage As String = ""
    Public Property Adr As String = ""
    Public Property CP As String = ""
    Public Property Ville As String = ""
    Public Property Session As String = ""

    Public ReadOnly Property SLieu As String
        Get
            Dim CRetour As String = ""
            Dim Sep As String = ""
            If Stage.Trim() <> "" Then
                CRetour &= Sep & Stage
                Sep = ", "
            End If
            If Session.Trim() <> "" Then
                CRetour &= Sep & Session
                Sep = ", "
            End If
            If (CP & Strings.Space(1) & Ville).Trim() <> "" Then
                CRetour &= Sep & (CP & Strings.Space(1) & Ville).Trim()
            End If
            Return CRetour
        End Get
    End Property
End Class

Public Class DonneeStagiaireBaseInfo
    Public Property Identifiant As Integer
    Public Property Sexe As String = ""
    Public Property Nom As String = ""
    Public Property Prenom As String = ""
    Public Property Etablissement As String = ""
    Public Property Niveau1 As String = ""
    Public Property Service As String = ""
    Public Property EmploiExerce As String = ""
    Public Property Grade As String = ""
    Public Property EMail As String = ""
    Public Property Fiche_Inscription As Virtualia.Metier.Formation.ItemInscriptionInfo

    Public ReadOnly Property NomPrenom As String
        Get
            Return Nom & Strings.Space(1) & Prenom
        End Get
    End Property
End Class

Public Class DonneeStagiaireInfo
    Inherits DonneeStagiaireBaseInfo

    Public Property Qualite As String = ""
    Public Property RueCollectivite As String = ""
    Public Property CPCollectivite As String = ""
    Public Property BureauDistribCollectivite As String = ""
    Public Property Tel As String = ""
    Public Property Fax As String = ""

    Public ReadOnly Property CpVilleCollectivite As String
        Get
            Return (CPCollectivite & Strings.Space(1) & BureauDistribCollectivite).Trim()
        End Get
    End Property

    Public ReadOnly Property VilleCollectivite As String
        Get
            If BureauDistribCollectivite.Trim() = "" Then
                Return ""
            End If
            If Not (BureauDistribCollectivite.ToLower().Contains(" cedex")) Then
                Return BureauDistribCollectivite
            End If
            Dim Idx As Integer = BureauDistribCollectivite.Trim().ToLower().IndexOf(" cedex")
            Return BureauDistribCollectivite.Trim().Substring(0, Idx).Trim()
        End Get
    End Property

    Public ReadOnly Property QualNomPrenom As String
        Get
            Return Qualite & Strings.Space(1) & Nom & Strings.Space(1) & Prenom
        End Get
    End Property

    Public ReadOnly Property STel As String
        Get
            If Tel.Trim() <> "" Then
                Return "Tel : " & Tel
            End If
            Return ""
        End Get
    End Property

    Public ReadOnly Property SFax As String
        Get
            If Fax.Trim() <> "" Then
                Return "Fax : " & Fax
            End If
            Return ""
        End Get
    End Property
End Class

