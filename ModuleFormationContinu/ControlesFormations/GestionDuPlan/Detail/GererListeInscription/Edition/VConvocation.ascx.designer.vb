﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VConvocation

    '''<summary>
    '''Contrôle CadreConvocation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreConvocation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEtablissement As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiEtablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiEtablissement As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiEtaNoEtNomRue.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiEtaNoEtNomRue As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiEtaVille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiEtaVille As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiEtaTelephone.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiEtaTelephone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreIndividu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreIndividu As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiDateEdition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateEdition As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiIndNomPrenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiIndNomPrenom As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiIndNiveau1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiIndNiveau1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiIndNiveau2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiIndNiveau2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiConvocation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiConvocation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreStage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreStage As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiLibelle1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLibelle1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiFormation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiLibelle2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLibelle2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreCalendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCalendrier As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiDateDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiDateFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiHeureDebutMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHeureDebutMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiHeureFinMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHeureFinMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiHeureDebutAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHeureDebutAM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiHeureFinAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHeureFinAM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_DonDateDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_DonDateDebut As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_DonDateFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_DonDateFin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_DonHeureDebutMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_DonHeureDebutMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_DonHeureFinMatin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_DonHeureFinMatin As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_DonHeureDebutAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_DonHeureDebutAM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle L1_DonHeureFinAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents L1_DonHeureFinAM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreLieuFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLieuFormation As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiLibelle3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLibelle3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiLieuFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLieuFormation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiAdresseFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAdresseFormation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiVilleFormation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiVilleFormation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadrePolitesse.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrePolitesse As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiLibelle4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLibelle4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiLibelle5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLibelle5 As Global.System.Web.UI.WebControls.Label
End Class
