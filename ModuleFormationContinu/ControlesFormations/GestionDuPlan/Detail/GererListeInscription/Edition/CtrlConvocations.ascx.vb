﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class CtrlConvocations
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public Property DonneesEdition As Virtualia.Net.DonneeEditionInfo
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition
        End Get
        Set(value As Virtualia.Net.DonneeEditionInfo)
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).DonneesEdition = value
        End Set
    End Property

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Call CreerLesElements()
    End Sub

    Private Sub CreerLesElements()
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.VConvocation
        Dim IndiceI As Integer

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        IndiceI = 0
        Do
            Ctl = WebFct.VirWebControle(Me.Cadre_Edition, "Convocation_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Virtualia.Net.VConvocation)
            VirControle.Visible = False
            If DonneesEdition.ListeStagiaires IsNot Nothing AndAlso IndiceI < DonneesEdition.ListeStagiaires.Count Then
                VirControle.Fiche(IndiceI) = DonneesEdition
                VirControle.Visible = True
            End If
            IndiceI += 1
        Loop
    End Sub
End Class