﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing

Partial Public Class CtrlGestionListeInscription
    Inherits GestMultiVueBase
    '**RP
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
    Public Event Dossier_Click As Dossier_ClickEventHandler
    '**
    Private Const WsCouleur_Insc As String = "#0E5F5C"
    Private Const WsCouleur_Cout As String = "#137A75"
    Private Const WsCouleur_Info As String = "#19968F"
    Private WsCouleur_Btn_Insc As Color = VisuHelper.ConvertiCouleur(WsCouleur_Insc)
    Private WsCouleur_Btn_Cout As Color = VisuHelper.ConvertiCouleur(WsCouleur_Cout)
    Private WsCouleur_Btn_Info As Color = VisuHelper.ConvertiCouleur(WsCouleur_Info)
    Private WsCouleur_Btn_Sel As Color = VisuHelper.ConvertiCouleur("#19968D")

    Protected Sub VDossier_Click(ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
        RaiseEvent Dossier_Click(Me, e)
    End Sub

    Public ReadOnly Property DonneesInscription As ItemInscriptionInfoCollection
        Get
            If Not (ViewState.KeyExiste(ItemInscriptionInfoCollection.KeyState)) Then
                ViewState.AjouteValeur(ItemInscriptionInfoCollection.KeyState, New ItemInscriptionInfoCollection())
            End If
            Return ViewState.GetValeur(Of ItemInscriptionInfoCollection)(ItemInscriptionInfoCollection.KeyState)
        End Get
    End Property

    Public ReadOnly Property MoteurInscription As GestionInscription
        Get
            Return DirectCast(Gestion, GestionInscription)
        End Get
    End Property

    Private Property NumVueAnc As Integer
        Get
            If Not (ViewState.KeyExiste("NumVueAnc")) Then
                ViewState.AjouteValeur("NumVueAnc", 0)
            End If
            Return DirectCast(ViewState("NumVueAnc"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("NumVueAnc", value)
        End Set
    End Property

    Public Property IdeStage As Integer
        Get
            If Not (ViewState.KeyExiste("IdeStage")) Then
                ViewState.AjouteValeur("IdeStage", 0)
            End If
            Return DirectCast(ViewState("IdeStage"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IdeStage", value)
        End Set
    End Property

    Public Property V_SiEnLecture As Boolean
        Get
            If Not (ViewState.KeyExiste("Lecture")) Then
                ViewState.AjouteValeur("Lecture", False)
            End If
            Return DirectCast(ViewState("Lecture"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("Lecture", value)
        End Set
    End Property

    Private Property EstApprocheSession As Boolean
        Get
            If Not (ViewState.KeyExiste("EstApprocheSession")) Then
                ViewState.AjouteValeur("EstApprocheSession", True)
            End If
            Return DirectCast(ViewState("EstApprocheSession"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("EstApprocheSession", value)
        End Set
    End Property

    Public Property VueCoutInsc As String
        Get
            If (Not ViewState.KeyExiste("VueCoutInsc")) Then
                ViewState.AjouteValeur("VueCoutInsc", "INSC")
            End If

            Return DirectCast(ViewState("VueCoutInsc"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("VueCoutInsc", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        btnVue.Act = Sub(btn)
                         Dim numvuegrille As Integer = 0
                         Dim numvueLst As Integer = 0

                         If EstApprocheSession Then
                             Select Case NumVueAnc
                                 Case 0
                                     NumVueAnc = 2
                                 Case 1
                                     NumVueAnc = 3
                                 Case 8
                                     NumVueAnc = 9
                             End Select

                             EstApprocheSession = False
                             MultiOnglets.ActiveViewIndex = NumVueAnc

                             ChargeApprocheStagiaire()
                             Return
                         End If

                         Select Case NumVueAnc
                             Case 2
                                 NumVueAnc = 0
                             Case 3
                                 NumVueAnc = 1
                             Case 9
                                 NumVueAnc = 8
                         End Select

                         EstApprocheSession = True
                         MultiOnglets.ActiveViewIndex = NumVueAnc

                         ChargeApprocheSession()
                     End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)
        Dim Couleur As String = ""
        For index = 2 To cboAction.Items.Count - 1
            If index >= 2 AndAlso index <= 6 Then
                Couleur = WsCouleur_Insc
            End If
            If index >= 7 AndAlso index <= 12 Then
                Couleur = WsCouleur_Cout
            End If
            If index >= 13 Then
                Couleur = WsCouleur_Info
            End If
            cboAction.Items(index).Attributes.Add("style", "background-color:" & Couleur & "; color:white")
        Next
    End Sub

    Public Overrides Sub Charge(source As Object)
        LblTitreSelection.Text = ""
        IdeStage = DirectCast(source, CacheSelectionArmoire).Ide
        V_SiEnLecture = DirectCast(source, CacheSelectionArmoire).SiEnLecture

        If DirectCast(source, CacheSelectionArmoire).Reference <> "" Then
            lblTitre.Text = "Inscriptions - " & DirectCast(source, CacheSelectionArmoire).Libelle & " (" & DirectCast(source, CacheSelectionArmoire).Reference & ")"
        Else
            lblTitre.Text = "Inscriptions - " & DirectCast(source, CacheSelectionArmoire).Libelle
        End If
        BtnSelec(BtnINSC, (VueCoutInsc = "INSC"))
        BtnSelec(BtnCOUT, (VueCoutInsc = "COUT"))
        BtnSelec(BtnINFO, (VueCoutInsc = "INFO"))
        '** RP **
        If DirectCast(source, CacheSelectionArmoire).SiEnLecture = True Then
            cboAction.Enabled = False
            LstChoixEdition.Enabled = False
        End If
        '**
        If EstApprocheSession = True Then
            ChargeApprocheSession()
            Exit Sub
        End If
        ChargeApprocheStagiaire()
    End Sub

    Protected Overrides Sub InitComposants()
        KeyMultiVue = "LISTINSC"

        CmdRetour = CommandeRetour

        CadreMenu = tbMenus

        CtrlListeRef = Referentiel
        CtrlListeRef.ControleurListeRef = Me

        PopupMessage = PopupMsg
        CtrlMessage = CtlMsg
        CtrlMessage.Act_RetourMessage = Act_RetourMessage

        DicoVue = New ItemDicoVueCollection(MultiOnglets)

        Dim itdico As ItemDicoVue

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Liste1"
        itdico.IDVue = "Vue1"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Liste2"
        itdico.IDVue = "Vue2"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "ListeAgt1"
        itdico.IDVue = "VueAgt1"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "ListeAgt1"
        itdico.IDVue = "VueAgt2"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "ForInscription"
        itdico.IDVue = "VueForInsc"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Suppression"
        itdico.Controls.Add(CtrlSuppression)
        itdico.IDVue = "VueSuppr"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Modification"
        itdico.Controls.Add(CtrlModification)
        itdico.IDVue = "VueModif"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "EDITION"
        itdico.LibelleMenu = "Edition"
        itdico.Controls.Add(CtrlGestionEdition)
        itdico.IDVue = "VueEdition"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Controls.Add(Referentiel)
        itdico.IDVue = "VueSysRef"
        itdico.TypeDeVue = TypeVue.VueListeRef
        DicoVue.Add(itdico)

        InitDataGridSession()
        InitDataGridStagiaire()

        CtrlListe1.CtrlGestion = Me
        CtrlListe2.CtrlGestion = Me
        CtrlListe3.CtrlGestion = Me

        CtrlListeAgt1.CtrlGestion = Me
        CtrlListeAgt2.CtrlGestion = Me
        CtrlListeAgt3.CtrlGestion = Me

        CtrlSuppression.CtrlGestion = Me
        CtrlModification.CtrlGestion = Me

        CtrlGestionEdition.CtrlGestion = Me

        If IsPostBack = False Then
            ChargeCboAction()
            ChargeCboEdition()
        End If
    End Sub

    Public Sub GereSelection(ByVal Clef As String, ByVal selection As Boolean)
        Dim ssplit As String() = Clef.Split("_"c)
        Dim it As ItemInscriptionInfo = (From iti In DonneesInscription Where iti.Ide = Integer.Parse(ssplit(0)) And iti.Rang = Integer.Parse(ssplit(1)) Select iti).First()
        it.Selection = selection
        DonneesInscription.Sauve(ViewState)

        If V_SiEnLecture = False Then
            cboAction.Enabled = (DonneesInscription.Selection.Count > 0)
        End If
    End Sub

    Protected Overrides Sub ClickMenu(ByVal ccache As CacheClickMenu)
        CadreTelecharger.Visible = False
        CmdTelecharger.Text = ""
        If ccache.TypeClick = TypeClickMenu.RETOUR Then
            MyBase.ClickMenu(ccache)
            Return
        End If
        If Not (EstApprocheSession) Then
            If ccache.Value = "0" Then
                ccache.Value = "2"
            End If
            If ccache.Value = "1" Then
                ccache.Value = "3"
            End If
            If ccache.Value = "8" Then
                ccache.Value = "9"
            End If
        End If
        If ccache.TypeClick <> TypeClickMenu.RETOUR Then
            NumVueAnc = Integer.Parse(ccache.Value)
        End If
        MyBase.ClickMenu(ccache)
        Select Case MultiOnglets.ActiveViewIndex
            Case 0
                CtrlListe1.Charge(Nothing)
            Case 1
                CtrlListe2.Charge(Nothing)
            Case 8
                CtrlListe3.Charge(Nothing)
            Case 2
                CtrlListeAgt1.Charge(Nothing)
            Case 3
                CtrlListeAgt2.Charge(Nothing)
            Case 9
                CtrlListeAgt3.Charge(Nothing)
        End Select
    End Sub

    Protected Overrides Sub Retour()
        CadreTelecharger.Visible = False
        CmdTelecharger.Text = ""
        Select Case MultiOnglets.ActiveViewIndex
            Case 4, 5, 6, 10, 11
                cboAction.SelectedIndex = 0

                Dim ccm As CacheClickMenu = New CacheClickMenu()
                ccm.TypeClick = TypeClickMenu.VUE
                ccm.TypeSource = TypeSourceMenu.V_Menu
                ccm.Value = "" & NumVueAnc

                CellDataGrid.Visible = True
                tbMenus.Visible = True
                TbEdition.Visible = True
                CellTotaux.Visible = True
                btnVue.Visible = True

                If Actualiser = True Then
                    cboAction.Enabled = False
                    Actualiser = False
                    If EstApprocheSession = True Then
                        ChargeCacheSession(False)
                    Else
                        ChargeCacheStagiaire(False)
                    End If
                End If
                ClickMenu(ccm)
                Exit Sub
        End Select
        MyBase.Retour()
    End Sub

    Protected Overrides Sub RetourMessage(ccache As CacheRetourMessage)
        If MultiOnglets.ActiveViewIndex = 4 Then
            CtrlGestInscription.GereRetourMessage(ccache)
            Exit Sub
        End If
        If ccache.TypeMessage = TypePageMessage.CONFIRMSUPPRESSION Then
            If ccache.Commande = TypeCommandeMessage.OUI Then
                'On affiche l'onglet de suppression qui se lancera tout de suite
                GereSuppression()
            End If
            Exit Sub
        End If
        MyBase.RetourMessage(ccache)
    End Sub

    Public Sub GereDetail()
        Dim it As ItemInscriptionInfo = (From iti In DonneesInscription Where iti.Selection = True Select iti).First()
        If it Is Nothing Then
            Exit Sub
        End If
        Dim ccm As CacheClickMenu = New CacheClickMenu()
        ccm.TypeClick = TypeClickMenu.VUE
        ccm.TypeSource = TypeSourceMenu.V_Menu
        ccm.Value = "4"
        CtrlGestInscription.Identifiant(it.Rang) = it.Ide

        NumVueAnc = MultiOnglets.ActiveViewIndex

        MyBase.ClickMenu(ccm)

        CellDataGrid.Visible = False
        tbMenus.Visible = False
        TbEdition.Visible = False
        CellTotaux.Visible = False
        btnVue.Visible = False
    End Sub

    Private Sub GereSuppression()
        NumVueAnc = MultiOnglets.ActiveViewIndex

        MultiOnglets.ActiveViewIndex = 5

        CellDataGrid.Visible = False
        tbMenus.Visible = False
        TbEdition.Visible = False
        CellTotaux.Visible = False
        btnVue.Visible = False

        CtrlSuppression.Charge(Nothing)
    End Sub

    Private Sub GereModif()
        NumVueAnc = MultiOnglets.ActiveViewIndex

        MultiOnglets.ActiveViewIndex = 6

        CellDataGrid.Visible = False
        tbMenus.Visible = False
        TbEdition.Visible = False
        CellTotaux.Visible = False
        btnVue.Visible = False

        CtrlModification.Charge(cboAction.SelectedItem)
    End Sub

    Private Sub ChargeCboEdition()
        LstChoixEdition.Items.Clear()
        LstChoixEdition.Items.Add("Sélectionner ...")
        LstChoixEdition.Items.Add("Export des données vers Excel")
        LstChoixEdition.Items.Add("Les convocations")
        LstChoixEdition.Items.Add("Les convocations par email")
        LstChoixEdition.Items.Add("La liste d'émargement")
        LstChoixEdition.Items.Add("Les attestations de stage")
        LstChoixEdition.Items.Add("Les adresses électroniques")
    End Sub

    Private Sub ChargeCboAction()
        Dim it As ListItem
        it = New ListItem("Choisir ...", "RIEN")
        it.Selected = True
        cboAction.Items.Add(it)

        it = New ListItem("Consulter le détail de l'inscription", "LECTURE")
        cboAction.Items.Add(it)
        it = New ListItem("Supprimer les inscriptions", "SUPPR")
        cboAction.Items.Add(it)

        it = New ListItem("Modifier le suivi", "CBO_SUIVI")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le motif de non participation", "CBO_MOTIF")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier l'action de formation", "CBO_ACTION")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier la prise en compte dans le DIF", "OUINON_DIF")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier la prise en compte dans le DIF hors temps de travail", "OUINON_DIF_HORS")
        cboAction.Items.Add(it)

        it = New ListItem("Modifier le nombre d'heures", "NB_HEURE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le coût pédagogique", "COUT_PEDAGOGIQUE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le coût annexe", "COUT_FORFAITAIRE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le coût du déplacement", "COUT_DEPLACEMENT")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le coût de la restauration", "COUT_RESTAURATION")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le coût de l'hébergement", "COUT_HEBERGEMENT")
        cboAction.Items.Add(it)

        it = New ListItem("Modifier le contexte", "CBO_CONTEXTE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier la demande d'hébergement", "OUINON_HEBER")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier la priorité", "CBO_PRIORITE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier la présence au stage", "CBO_PRESENCE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier la filière", "CBO_FILIERE")
        cboAction.Items.Add(it)
        it = New ListItem("Modifier le nombre d'heures de déplacement", "NB_HEURE_DEP")
        cboAction.Items.Add(it)
    End Sub

    Private Sub ChargeTotaux(ByVal strucf As StructureFormationInscription)
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        LblTotDossier.Text = ""
        LblTotHeure.Text = ""
        LblTotJour.Text = ""
        LblTotForfaitaire.Text = ""
        LblTotPedagogique.Text = ""
        LblTotDeplacement.Text = ""
        LblTotRestauration.Text = ""
        LblTotHebergement.Text = ""

        If (strucf Is Nothing) OrElse strucf.Inscriptions.Count <= 0 Then
            Return
        End If
        Dim lstitems As List(Of ForInscriptionInfo) = strucf.Inscriptions.GetFichesTypees()
        LblTotDossier.Text = "" & lstitems.Count()
        Dim Total As Double = 0

        Total = (From it In lstitems Select it.Nbheures_formation).Sum()
        If Total > 0 Then
            LblTotHeure.Text = WebFct.ViRhFonction.MontantEdite(Total, 1)
        End If

        Total = (From it In lstitems Select DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).ParametreFormation.CalculNbJour(it.Nbheures_formation)).Sum()
        If Total > 0 Then
            LblTotJour.Text = WebFct.ViRhFonction.MontantEdite(Total, 1)
        End If

        Total = (From it In lstitems Select it.Coutforfaitaire).Sum()
        If Total > 0 Then
            LblTotForfaitaire.Text = WebFct.ViRhFonction.MontantEdite(Total, 2)
        End If

        Total = (From it In lstitems Select it.Coutpedagogique).Sum()
        If Total > 0 Then
            LblTotPedagogique.Text = WebFct.ViRhFonction.MontantEdite(Total, 2)
        End If

        Total = (From it In lstitems Select it.Coutdeplacement).Sum()
        If Total > 0 Then
            LblTotDeplacement.Text = WebFct.ViRhFonction.MontantEdite(Total, 2)
        End If

        Total = (From it In lstitems Select it.Coutrestauration).Sum()
        If Total > 0 Then
            LblTotRestauration.Text = WebFct.ViRhFonction.MontantEdite(Total, 2)
        End If

        Total = (From it In lstitems Select it.Couthebergement).Sum()
        If Total > 0 Then
            LblTotHebergement.Text = WebFct.ViRhFonction.MontantEdite(Total, 2)
        End If
    End Sub

    Private Sub AfficheMessageSuppression()
        Dim msg As String = ""
        Dim nb As Integer = DonneesInscription.Selection.Count
        If nb = 1 Then
            msg = "Voulez-vous supprimer cette inscription ?"
        Else
            msg = "Voulez-vous supprimer ces " & nb & " inscriptions ?"
        End If
        Dim cam As CacheAppelMessage = CacheAppelMessage.GenereMessageSuppression("Suppression", msg)

        AfficheMessage(cam)
    End Sub

    Public Function GetTotal(ByVal TypeValeur As String) As String
        If TypeValeur = "NB" Then
            Return DonneesInscription.GetTotal(TypeValeur, EstApprocheSession)
        End If
        Return DonneesInscription.GetTotal(TypeValeur)
    End Function

    Private Sub BtnSelec(btn As Button, selectionne As Boolean)
        btn.BorderStyle = If(selectionne, BorderStyle.Inset, BorderStyle.Outset)
    End Sub

    Protected Sub cboAction_SelectedIndexChanged(sender As Object, e As EventArgs)
        CadreTelecharger.Visible = False
        CmdTelecharger.Text = ""
        If cboAction.SelectedItem.Value = "RIEN" Then
            Exit Sub
        End If
        If cboAction.SelectedItem.Value = "SUPPR" Then
            AfficheMessageSuppression()
            Exit Sub
        End If
        If cboAction.SelectedItem.Value = "LECTURE" Then
            Call GereDetail()
            Exit Sub
        End If
        Select Case cboAction.SelectedItem.Value
            Case "CBO_SUIVI", "CBO_MOTIF", "CBO_ACTION", "OUINON_DIF", "OUINON_DIF_HORS"
                BtnTab_Click(BtnINSC, Nothing)
            Case "NB_HEURE", "COUT_PEDAGOGIQUE", "COUT_FORFAITAIRE", "COUT_DEPLACEMENT", "COUT_RESTAURATION", "COUT_HEBERGEMENT"
                BtnTab_Click(BtnCOUT, Nothing)
            Case "CBO_CONTEXTE", "CBO_CURSUS", "DTE_INSC", "OUINON_HEBER", "CBO_PRIORITE", "CBO_PRESENCE", "CBO_FILIERE", "CBO_MODULE", "NB_HEURE_DEP", "CBO_ORGA", "CBO_ORGA_COUT_PEDA", "CBO_ORGA_COUT_AUTRE"
                BtnTab_Click(BtnINFO, Nothing)
        End Select
        If cboAction.SelectedItem.Value.StartsWith("CBO_ORGA") Then
            Exit Sub
        End If
        GereModif()
    End Sub

    Protected Sub BtnTab_Click(sender As Object, e As EventArgs)
        CadreTelecharger.Visible = False
        CmdTelecharger.Text = ""
        If VueCoutInsc = DirectCast(sender, Control).ID.Replace("Btn", "") Then
            Return
        End If
        VueCoutInsc = DirectCast(sender, Control).ID.Replace("Btn", "")

        BtnSelec(BtnINSC, (VueCoutInsc = "INSC"))
        BtnSelec(BtnCOUT, (VueCoutInsc = "COUT"))
        BtnSelec(BtnINFO, (VueCoutInsc = "INFO"))

        Dim ccm As CacheClickMenu = New CacheClickMenu()
        ccm.TypeClick = TypeClickMenu.VUE
        ccm.TypeSource = TypeSourceMenu.V_Menu

        Dim idm As String = ""
        Select Case VueCoutInsc
            Case "INSC"
                ccm.Value = "0"
            Case "COUT"
                ccm.Value = "1"
            Case "INFO"
                ccm.Value = "8"
        End Select
        ClickMenu(ccm)
    End Sub

    '**RP
    Private Sub CtrlGestInscription_MessageDialogue(sender As Object, e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) Handles CtrlGestInscription.MessageDialogue
        Dim Cam As CacheAppelMessage
        Cam = CacheAppelMessage.GenereMessageSuppression(e.TitreMessage, e.ContenuMessage)
        Call AfficheMessage(Cam)
    End Sub

    Public Sub AfficherDossier(ByVal Clef As String)
        Dim TableauData(0) As String
        Dim Inscription As ItemInscriptionInfo
        Dim IdePER As Integer

        CadreTelecharger.Visible = False
        CmdTelecharger.Text = ""

        TableauData = Clef.Split("_"c)
        Inscription = (From iti In DonneesInscription Where iti.Ide = Integer.Parse(TableauData(0)) And iti.Rang = Integer.Parse(TableauData(1)) Select iti).First()
        If Inscription Is Nothing Then
            Exit Sub
        End If
        IdePER = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).VirIdentifiantPER(Inscription.Nom, Inscription.Prenom, Inscription.DateDeNaissance)
        If IdePER > 0 Then
            VDossier_Click(New Virtualia.Systeme.Evenements.DossierClickEventArgs(IdePER, "Tableau"))
        End If
    End Sub

    Private Sub ChoixEdition_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstChoixEdition.SelectedIndexChanged
        Dim LstValeursComptavalides As List(Of String) = FormationHelper.GetvaleurDeComptaEtDeValidation()
        Dim SelEdition As String = LstChoixEdition.SelectedItem.Text

        LstChoixEdition.SelectedIndex = 0
        LstChoixEdition.SelectedItem.Text = LstChoixEdition.Items.Item(0).Text
        If ((From it In DonneesInscription Where it.DateDebSession = "" Select 1).Sum() > 0) Then
            Dim cam As CacheAppelMessage = CacheAppelMessage.GenereMessageInformatif("Editions", "Aucune édition possible sur des pré-inscriptions.")
            AfficheMessage(cam)
            Exit Sub
        End If

        If ((From it In DonneesInscription Where it.DateDebSession <> "" AndAlso LstValeursComptavalides.Contains(it.Suivi.ToLower()) Select 1).Sum() <= 0) Then
            Dim cam As CacheAppelMessage = CacheAppelMessage.GenereMessageInformatif("Editions", "Aucune inscription comptabilisable ou validée à éditer.")
            AfficheMessage(cam)
            Exit Sub
        End If

        Select Case SelEdition
            Case "Export des données vers Excel"
                Call CreerFichierCsv("Tout")
            Case "Les adresses électroniques"
                Call CreerFichierCsv("Email")
            Case Else
                CadreTelecharger.Visible = False
                CmdTelecharger.Text = ""
                NumVueAnc = MultiOnglets.ActiveViewIndex
                CellDataGrid.Visible = False
                tbMenus.Visible = False
                TbEdition.Visible = False
                CellTotaux.Visible = False
                btnVue.Visible = False
                MultiOnglets.ActiveViewIndex = 10
                CtrlGestionEdition.Charge(SelEdition)
        End Select

    End Sub

    Private Sub CreerFichierCsv(ByVal Nature As String)
        Dim FichierCsv As String
        Dim NomExport As String = ""
        Dim DatasExtraites As DonneeEditionInfo
        Dim VirLdGlobal As Virtualia.Net.Session.LDObjetGlobal = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        FichierCsv = VirLdGlobal.VirRepertoireTemporaire
        Select Case Nature
            Case "Email"
                NomExport = "Email_" & WebFct.PointeurUtilisateur.V_NomdeConnexion & ".csv"
                FichierCsv &= "\" & NomExport
                CmdTelecharger.Text = "Obtenir le ficher des adresses électroniques"
            Case "Tout"
                NomExport = "Inscrits_" & WebFct.PointeurUtilisateur.V_NomdeConnexion & ".csv"
                FichierCsv &= "\" & NomExport
                CmdTelecharger.Text = "Obtenir le fichier des inscriptions"
        End Select

        DatasExtraites = CreerDonneesEdition()
        If DatasExtraites Is Nothing OrElse DatasExtraites.ListeStagiaires.Count = 0 Then
            Exit Sub
        End If
        For Each Stagiaire In DatasExtraites.ListeStagiaires
            Stagiaire.Grade = VirLdGlobal.VirSelectionUneDonnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, 1, Stagiaire.Identifiant)
            Stagiaire.EMail = VirLdGlobal.VirSelectionUneDonnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, 7, Stagiaire.Identifiant)
        Next
        Select Case Nature
            Case "Email"
                Call EcrireFichierEMail(FichierCsv, DatasExtraites)
            Case "Tout"
                Call EcrireFichierCsv(FichierCsv, DatasExtraites)
        End Select
        CadreTelecharger.Visible = True
        CmdTelecharger.CommandArgument = Nature
    End Sub

    Private Sub EcrireFichierCsv(ByVal NomFichier As String, ByVal Donnees As DonneeEditionInfo)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim Chaine As System.Text.StringBuilder
        Dim NBCalc As Double
        FicStream = New System.IO.FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        Chaine = New System.Text.StringBuilder
        Chaine.Append("Du" & VI.PointVirgule)
        Chaine.Append("Au" & VI.PointVirgule)
        Chaine.Append("Lieu" & VI.PointVirgule)
        Chaine.Append("Stagiaire" & VI.PointVirgule)
        Chaine.Append("Grade" & VI.PointVirgule)
        Chaine.Append("Niveau 1" & VI.PointVirgule)
        Chaine.Append("Niveau 2" & VI.PointVirgule)
        Chaine.Append("Emploi Exerce" & VI.PointVirgule)
        Chaine.Append("Adresse électronique" & VI.PointVirgule)
        Chaine.Append("Inscrit le" & VI.PointVirgule)
        Chaine.Append("Priorité" & VI.PointVirgule)
        Chaine.Append("Suivi" & VI.PointVirgule)
        Chaine.Append("Session" & VI.PointVirgule)
        Chaine.Append("Module" & VI.PointVirgule)
        Chaine.Append("Cursus" & VI.PointVirgule)
        Chaine.Append("Non participation" & VI.PointVirgule)
        Chaine.Append("Présence" & VI.PointVirgule)
        Chaine.Append("Nbr. heures" & VI.PointVirgule)
        Chaine.Append("Nbr. jours" & VI.PointVirgule)
        Chaine.Append("Domaine" & VI.PointVirgule)
        Chaine.Append("Action" & VI.PointVirgule)
        Chaine.Append("Organisme" & VI.PointVirgule)
        Chaine.Append("Coût pédagogique" & VI.PointVirgule)
        Chaine.Append("Coût annexe" & VI.PointVirgule)
        Chaine.Append("Coût déplacement" & VI.PointVirgule)
        Chaine.Append("Coût restauration" & VI.PointVirgule)
        Chaine.Append("Coût hébergement" & VI.PointVirgule)
        Chaine.Append("Organisme payeur pédagogie" & VI.PointVirgule)
        Chaine.Append("Organisme payeur autres coûts" & VI.PointVirgule)
        Chaine.Append("Nbr. heures de déplacement" & VI.PointVirgule)
        Chaine.Append("Demande d'hébergement")

        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()

        For Each Stagiaire In Donnees.ListeStagiaires
            Chaine.Append(Donnees.Stage.DateDeb & VI.PointVirgule)
            Chaine.Append(Donnees.Stage.DateFin & VI.PointVirgule)
            Chaine.Append(Donnees.LieuFormation.SLieu & VI.PointVirgule)
            Chaine.Append(Stagiaire.NomPrenom & VI.PointVirgule)
            Chaine.Append(Stagiaire.Grade & VI.PointVirgule)
            Chaine.Append(Stagiaire.Niveau1 & VI.PointVirgule)
            Chaine.Append(Stagiaire.Service & VI.PointVirgule)
            Chaine.Append(Stagiaire.EmploiExerce & VI.PointVirgule)
            Chaine.Append(Stagiaire.EMail & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Date_Insc & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Priorite & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Suivi & VI.PointVirgule)
            Chaine.Append(Donnees.Stage.Intitule & VI.PointVirgule)
            Chaine.Append(Donnees.FicheInscription.Module_Formation & VI.PointVirgule)
            Chaine.Append(Donnees.FicheInscription.Cursus & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Motif & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Presence & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.NbHeure & VI.PointVirgule)
            NBCalc = DirectCast(Page, FrmPlanDeFormation).ParamFormation.CalculNbJour(Stagiaire.Fiche_Inscription.NbHeure)
            Chaine.Append(WebFct.ViRhFonction.MontantEdite(Str(NBCalc), 1) & VI.PointVirgule)
            Chaine.Append(Donnees.Stage.Domaine & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Action & VI.PointVirgule)
            Chaine.Append(Donnees.Organisme.Organisme & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Cout_Pedagogique & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Cout_Forfaitaire & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Cout_Deplacement & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Cout_Restauration & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Cout_Hebergement & VI.PointVirgule)
            Chaine.Append(Donnees.FicheInscription.Organisme_Peda & VI.PointVirgule)
            Chaine.Append(Donnees.FicheInscription.Organisme_Autres & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Nb_Heure_Dep & VI.PointVirgule)
            Chaine.Append(Stagiaire.Fiche_Inscription.Demande_Heber)

            FicWriter.WriteLine(Chaine.ToString)
            Chaine.Clear()
        Next
        FicWriter.Flush()
        FicWriter.Close()
    End Sub

    Private Sub EcrireFichierEMail(ByVal NomFichier As String, ByVal Donnees As DonneeEditionInfo)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim Chaine As System.Text.StringBuilder

        FicStream = New System.IO.FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        Chaine = New System.Text.StringBuilder
        Chaine.Append("Stagiaire" & VI.PointVirgule)
        Chaine.Append("Adresse électronique" & VI.PointVirgule)
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()

        For Each Stagiaire In Donnees.ListeStagiaires
            Chaine.Append(Stagiaire.NomPrenom & VI.PointVirgule)
            Chaine.Append(Stagiaire.EMail & VI.PointVirgule)
            FicWriter.WriteLine(Chaine.ToString)
            Chaine.Clear()
        Next
        FicWriter.Flush()
        FicWriter.Close()
    End Sub

    Private Function CreerDonneesEdition() As DonneeEditionInfo
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim LstDatas As DonneeEditionInfo = New DonneeEditionInfo()
        Dim StructFInscr As StructureFormationInscription = TryCast(MoteurInscription.GetStructure(IdeStage), StructureFormationInscription)
        If StructFInscr Is Nothing Then
            Return Nothing
        End If
        Dim LstValeursComptaValides As List(Of String) = FormationHelper.GetvaleurDeComptaEtDeValidation()

        LstDatas.Stage.Intitule = StructFInscr.Identification.Intitule

        Dim SiPremier As Boolean = True
        Dim Stagiaire As DonneeStagiaireInfo
        Dim LstValeurs As List(Of String)
        Dim IdeDossier As Integer
        Dim LDGlobales As Virtualia.Net.Session.LDObjetGlobal = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)

        For Each it In DonneesInscription.Where(Function(Element)
                                                    Return LstValeursComptaValides.Contains(Element.Suivi.ToLower()) And Element.Selection = True
                                                End Function).OrderBy(Function(Element)
                                                                          Return Element.NomPrenom
                                                                      End Function)

            IdeDossier = LDGlobales.VirIdentifiantPER(it.Nom, it.Prenom, it.DateDeNaissance)
            If IdeDossier <= 0 Then
                Continue For
            End If
            If SiPremier = True Then
                SiPremier = False
                LstDatas.FicheInscription = it
                LstDatas.Stage.DateDeb = it.DateDebSession
                LstDatas.Stage.DateFin = it.DateFinSession
                '** RP
                Dim FicheIDENT As Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION = FormationHelper.GetIdentificationDeStage(IdeStage)
                If FicheIDENT IsNot Nothing Then
                    LstDatas.Stage.Domaine = FicheIDENT.Domaine
                End If
                '**
                Dim FicheREF As Virtualia.TablesObjet.ShemaREF.FOR_SESSION = FormationHelper.GetSessionDeInscription(IdeStage,
                                                WebFct.ViRhDates.DateTypee(it.DateDebSession), WebFct.ViRhDates.DateTypee(it.DateFinSession), it.Rang_Session)

                If FicheREF IsNot Nothing Then
                    LstDatas.Organisme.Organisme = If(FicheREF.OrganismePrestataire.Trim() <> "", FicheREF.OrganismePrestataire.Trim(),
                                                          StructFInscr.Identification.Organisme)

                    LstDatas.Stage.H_AM_Deb = FicheREF.HeureDebut_Matin
                    LstDatas.Stage.H_AM_Fin = FicheREF.HeureFin_Matin
                    LstDatas.Stage.H_PM_Deb = FicheREF.HeureDebut_ApresMidi
                    LstDatas.Stage.H_PM_Fin = FicheREF.HeureFin_ApresMidi
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                    LstDatas.Stage.NbHeure = WebFct.ViRhFonction.MontantEdite(FicheREF.DureeSession_Heures, 2)

                    LstDatas.LieuFormation.Stage = FicheREF.LieuFormation
                    LstDatas.LieuFormation.Session = FicheREF.LieuSession
                    LstDatas.LieuFormation.Adr = FicheREF.AdresseduStage
                    LstDatas.LieuFormation.CP = If(FicheREF.CodePostal.Trim() = "0", "", FicheREF.CodePostal.Trim())
                    LstDatas.LieuFormation.Ville = FicheREF.VilleAdresse
                End If
            End If

            Stagiaire = New DonneeStagiaireInfo() With {.Identifiant = IdeDossier, .Nom = it.Nom, .Prenom = it.Prenom}
            Stagiaire.Fiche_Inscription = it

            Dim Requeteur As Virtualia.Metier.Formation.RequeteurFormation
            Requeteur = DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).VirRequeteur
            LstValeurs = Requeteur.Affectation_Un_Agent(IdeDossier, it.DateDebSession)
            If LstValeurs IsNot Nothing AndAlso LstValeurs.Count > 0 Then
                Stagiaire.Niveau1 = LstValeurs(0)
                Stagiaire.Service = GetService(LstValeurs)
                Stagiaire.EmploiExerce = LstValeurs(6)

                Stagiaire.RueCollectivite = LstValeurs(7)
                Stagiaire.CPCollectivite = LstValeurs(8)
                Stagiaire.BureauDistribCollectivite = LstValeurs(9)
                Stagiaire.Tel = LstValeurs(10)
                Stagiaire.Fax = LstValeurs(11)
            End If

            LstDatas.ListeStagiaires.Add(Stagiaire)
        Next
        Return LstDatas
    End Function
    Private Function GetService(ByVal LstValeurs As List(Of String)) As String
        Dim IndiceI As Integer
        If LstValeurs.Count <= 0 Then
            Return ""
        End If
        Dim LstTmp As List(Of String) = LstValeurs.Skip(1).Take(5).ToList()

        For IndiceI = LstTmp.Count - 1 To 0 Step -1
            If LstTmp(IndiceI).Trim() <> "" Then
                Return LstTmp(IndiceI).Trim()
            End If
        Next
        Return ""
    End Function

    Protected Sub CmdTelecharger_Commande(sender As Object, e As CommandEventArgs)
        Dim FluxTeleChargement As Byte()
        Dim FichierCsv As String
        Dim NomExport As String = ""
        Dim VirLdGlobal As Virtualia.Net.Session.LDObjetGlobal = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        FichierCsv = VirLdGlobal.VirRepertoireTemporaire
        Select Case CmdTelecharger.CommandArgument
            Case "Email"
                NomExport = "Email_" & WebFct.PointeurUtilisateur.V_NomdeConnexion & ".csv"
                FichierCsv &= "\" & NomExport
            Case "Tout"
                NomExport = "Inscrits_" & WebFct.PointeurUtilisateur.V_NomdeConnexion & ".csv"
                FichierCsv &= "\" & NomExport
        End Select
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(FichierCsv)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomExport & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub
End Class