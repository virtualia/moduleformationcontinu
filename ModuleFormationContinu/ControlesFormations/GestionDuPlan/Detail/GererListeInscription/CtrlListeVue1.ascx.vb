﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing

Public Class CtrlListeVue1
    Inherits UserControl
    Implements IControlBase
    Private WsCouleurLigne As Color = VisuHelper.ConvertiCouleur("#E2E2E2")
    Private WsCouleurLigne_InscPerson As Color = VisuHelper.ConvertiCouleur("#B0E0D7")
    Private WsCouleurBouton As Color = Color.LightGray
    Private WsListeBtn As List(Of Button) = New List(Of Button)()
    Private WsListeChk As List(Of CheckBox) = New List(Of CheckBox)()

    Private WsCtrlGestion As CtrlGestionListeInscription

    Public WriteOnly Property CtrlGestion As CtrlGestionListeInscription
        Set(value As CtrlGestionListeInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        ConstruitControles()
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        ChkSelection.Checked = False
        ConstruitControles()
        LblTotDossier.Text = WsCtrlGestion.GetTotal("NB")
    End Sub

    Private Sub ConstruitControles()
        WsListeBtn = New List(Of Button)()
        WsListeChk = New List(Of CheckBox)()

        ViderDetail()
        For Each iti In WsCtrlGestion.DonneesInscription
            ConstruitLigneDetail(iti)
        Next
        If WsCtrlGestion.V_SiEnLecture = True Then
            For Each Ck As CheckBox In WsListeChk
                Ck.Enabled = False
            Next
            For Each Bt As Button In WsListeBtn
                Bt.Enabled = False
            Next
        Else
            WsListeChk.ForEach(Sub(ck As CheckBox)
                                   AddHandler ck.CheckedChanged, AddressOf ChkClick
                               End Sub)
            WsListeBtn.ForEach(Sub(bt As Button)
                                   AddHandler bt.Click, AddressOf BtnClick
                               End Sub)
        End If
    End Sub

    Public Sub ViderDetail(Optional Vidertotal As Boolean = False)
        TbDetail.Rows.Clear()
        If Not (Vidertotal) Then
            Return
        End If
        LblTotDossier.Text = ""
    End Sub

    Private Sub ConstruitLigneDetail(iti As ItemInscriptionInfo)
        Dim Rangee As TableRow = New TableRow()
        Rangee.ID = "RDetail" & iti.Index

        If Not (iti.EstPersonnalise) Then
            Rangee.BackColor = WsCouleurLigne
        Else
            Rangee.BackColor = WsCouleurLigne_InscPerson
        End If
        Rangee.Cells.Add(ConstructionCell.ConstruitCellChk(iti, WsListeChk))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellBouton(iti, WsListeBtn, WsCouleurBouton))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "NomPrenom", "25%", "230px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Suivi", "10%", "150px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Motif", "15%", "150px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "Action", "20%", "235px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "DIF", "10%", "80px"))
        Rangee.Cells.Add(ConstructionCell.ConstruitCellLabel(iti, "DIF_HT", "20%", "80px"))

        TbDetail.Rows.Add(Rangee)

    End Sub

    Private Sub ChkClick(s As Object, ev As EventArgs)
        ConstructionCell.EvenChk(DirectCast(s, CheckBox), WsCtrlGestion)
        ChkSelection.Checked = WsCtrlGestion.DonneesInscription.Count = WsListeChk.Where(Function(ck)
                                                                                             Return ck.Checked
                                                                                         End Function).Count()
    End Sub

    Private Sub BtnClick(s As Object, ev As EventArgs)
        ConstructionCell.EvenBtn(DirectCast(s, Button), WsCtrlGestion)
    End Sub

    Protected Sub ChkSelection_CheckedChanged(sender As Object, e As EventArgs)
        Dim SiSelect As Boolean = ChkSelection.Checked
        If WsListeChk.Count > 0 Then
            For Each Chk In WsListeChk
                Chk.Checked = SiSelect
                ChkClick(Chk, Nothing)
            Next
        End If
    End Sub

End Class