﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlSuppression.ascx.vb" Inherits="Virtualia.Net.CtrlSuppression" %>

<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererStage/PopupConfirmSupp.ascx" TagName="CTL_CONFIRM" TagPrefix="Formation" %>
<%@ Register Src="~/ControleGeneric/BoutonSupprimer.ascx" TagName="BTN_SUPPR" TagPrefix="Generic" %>

<asp:Table ID="CadreControle" runat="server" Width="590px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow Height="40px">
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:UpdatePanel ID="pnltrait" runat="server">
                <ContentTemplate>
                    <asp:Table ID="Table1" runat="server" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="Label2" runat="server" Width="400px" Text="Virtualia va supprimer ce stage" ForeColor="White" BorderStyle="None" Style="text-align: left" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="Label1" runat="server" Width="400px" Text="Cette action entrainera la suppression de toutes les données de ce stage." ForeColor="White" BorderStyle="None" Style="text-align: left" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="2px"/>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="Label4" runat="server" Width="350px" Text="Virtualia vérifie l'existence d'inscriptions." ForeColor="White" BorderStyle="None" Style="text-align: left" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="Label5" runat="server" Width="350px" Text="S'il en existe, la suppression ne sera pas effectuée." ForeColor="White" BorderStyle="None" Style="text-align: left" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <Generic:BTN_SUPPR ID="BtnSuppr" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
            <asp:UpdatePanel ID="UpPnlMessage" runat="server">
                <ContentTemplate>
                    <asp:Timer ID="TimerAttente" runat="server" Interval="50" Enabled="false" />
                    <asp:Table ID="TbMessage" runat="server" Visible="true" Width="580px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                        <asp:TableRow>
                            <asp:TableCell Height="5px" />
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:TextBox ID="TxtStatut" runat="server" Height="20px" Width="540px" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:TextBox ID="txtMsg" runat="server" Height="340px" Width="540px" TextMode="MultiLine" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="50px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false">
                                <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                    <ProgressTemplate>
                                        <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                                </asp:TableCell>
                                                <asp:TableCell Width="10px" Height="10px" />
                                                <asp:TableCell>
                                                    <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="Traitement en cours, veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:TableCell>
                        </asp:TableRow>

                    </asp:Table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="BtnSuppr" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
