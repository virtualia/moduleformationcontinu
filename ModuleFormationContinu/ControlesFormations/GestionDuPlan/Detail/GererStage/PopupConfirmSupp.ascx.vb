﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu.Formation
Imports System.Web.UI
Imports AjaxControlToolkit
Imports Virtualia.ObjetBaseStructure.Formation

Public Class PopupConfirmSupp
    Inherits UserControl

    Private WsPopup As ModalPopupExtender
    Private WsAct_Retour As Action(Of Boolean)

    Public WriteOnly Property Popup As ModalPopupExtender
        Set(ByVal value As ModalPopupExtender)
            WsPopup = value
        End Set
    End Property

    Public WriteOnly Property Act_Retour As Action(Of Boolean)
        Set(value As Action(Of Boolean))
            WsAct_Retour = value
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)

        txtMsg.Text = "La confirmation de cette suppression entrainera la suppression de :" & vbCrLf & _
                       vbTab & "- sa définition " & vbCrLf & _
                       vbTab & "- sa caractéristique " & vbCrLf & _
                       vbTab & "- son descriptif " & vbCrLf & _
                       "etc ..." & vbCrLf & vbCrLf & _
                       "Un contrôle sur l'existence d'inscription sera effectué" & vbCrLf & _
                       vbTab & "S'il en existe le stage ne sera pas supprimé"

        btnNon.Act = AddressOf btn_Click
        btnOui.Act = AddressOf btn_Click
    End Sub

    Protected Sub btn_Click(btn As BoutonBase)
        WsAct_Retour(btn.ID.ToLower().Contains("oui"))
        WsPopup.Hide()
    End Sub
End Class