﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Web.UI
Imports Virtualia.Structure.Formation

Public Class CtrlVerification
    Inherits UserControl
    Implements IControlBase
    Private WsCtrlGestion As CtrlGestionStage

    <Serializable>
    Public Class CacheCtrlVerification
        Public Const KeyState As String = "CacheCtrlVerification"
        Public Property LstSessions As List(Of LibPlanStageSessionInfo) = New List(Of LibPlanStageSessionInfo)()

        Public Sub Sauve(vs As StateBag)
            vs.AjouteValeur(KeyState, Me)
        End Sub

        Public Sub Vider(vs As StateBag)
            LstSessions.Clear()
            Sauve(vs)
        End Sub
    End Class

    Public WriteOnly Property CtrlGestion As CtrlGestionStage
        Set(value As CtrlGestionStage)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Private ReadOnly Property V_Cache As CacheCtrlVerification
        Get
            If Not (ViewState.KeyExiste(CacheCtrlVerification.KeyState)) Then
                ViewState.AjouteValeur(CacheCtrlVerification.KeyState, New CacheCtrlVerification())
            End If
            Return ViewState.GetValeur(Of CacheCtrlVerification)(CacheCtrlVerification.KeyState)
        End Get
    End Property

    Private Property NumeroTraitement As Integer
        Get
            If ViewState.KeyExiste("NoTraitement") = False Then
                ViewState.AjouteValeur("NoTraitement", 0)
            End If
            Return DirectCast(ViewState("NoTraitement"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("NoTraitement", value)
        End Set
    End Property

    Private Property IndexOperation As Integer
        Get
            If (Not ViewState.KeyExiste("IndexOperation")) Then
                ViewState.AjouteValeur("IndexOperation", -1)
            End If
            Return DirectCast(ViewState("IndexOperation"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexOperation", value)
        End Set
    End Property

    Private Property IndexOperationCourante As Integer
        Get
            If (Not ViewState.KeyExiste("IndexOperationCourante")) Then
                ViewState.AjouteValeur("IndexOperationCourante", -1)
            End If
            Return DirectCast(ViewState("IndexOperationCourante"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexOperationCourante", value)
        End Set
    End Property

    Private Property Index As Integer
        Get
            If (Not ViewState.KeyExiste("Index")) Then
                ViewState.AjouteValeur("Index", -1)
            End If
            Return DirectCast(ViewState("Index"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("Index", value)
        End Set
    End Property

    Private Property IndexCourant As Integer
        Get
            If (Not ViewState.KeyExiste("IndexCourant")) Then
                ViewState.AjouteValeur("IndexCourant", -1)
            End If
            Return DirectCast(ViewState("IndexCourant"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexCourant", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)

        BtnVerif.Act = Sub(btn)
                           IndexOperation = 0
                           IndexOperationCourante = -1
                           Index = 0
                           IndexCourant = -1
                           BtnVerif.Enabled = False
                           CellBtn.Visible = False
                           TbMessage.Visible = True

                           If BtnVerif.Text.StartsWith("Rechercher") Then
                               V_Cache.Vider(ViewState)
                               TxtStatut.Text = "Recherche des sessions"
                               TbMessage.Visible = True
                               txtMsg.Text = ""

                               HorlogeTraitement.Enabled = True
                               NumeroTraitement = 1
                               Exit Sub
                           End If

                           TxtStatut.Text = "Correction"
                           txtMsg.Text = ""
                           HorlogeTraitement.Enabled = True
                           NumeroTraitement = 2
                       End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)
        If HorlogeTraitement.Enabled = True Then
            Select Case NumeroTraitement
                Case 1
                    Call ExecuterVerification()
                Case 2
                    DirectCast(Page.Master, VirtualiaMain).SessionCourante.ReinitListeArbo()
                    Call ExecuterCorrection()
            End Select
        End If
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        V_Cache.Vider(ViewState)
        BtnVerif.Enabled = True
        CellBtn.Visible = True
        BtnVerif.Text = "Rechercher"
        EtiExplic1.Text = "Virtualia vérifie la cohérence entre le tableau des inscriptions et les dossiers du personnel."
        TbMessage.Visible = False
        txtMsg.Text = ""
        TxtStatut.Text = ""
    End Sub

    Private Async Function TraiteRechercheAsync(indexTraitement As Integer, indexdonnee As Integer) As System.Threading.Tasks.Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, Integer, RetAsync) = Function(idxtrait, IdxDonnee)
                                                                        Return TraiteRecherche(idxtrait, indexdonnee)
                                                                    End Function

        Return Await System.Threading.Tasks.Task.FromResult(Of RetAsync)(FuncTraitement(indexTraitement, indexdonnee))
    End Function

    Private Function TraiteRecherche(ByVal IndexTrait As Integer, ByVal IdxDonnee As Integer) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()

        Select Case IndexTrait
            Case 0
                'On charge les for_inscription
                Dim Ident As Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION = FormationHelper.GetIdentificationDeStage(WsCtrlGestion.IdeStage)
                If Ident Is Nothing Then
                    CRetour.Index = 2
                    CRetour.Retour = "Ce stage a été supprimé par un autre utilisateur"
                    CRetour.TraitementSuivant = "Recherche terminée"
                    Return CRetour
                End If
                V_Cache.LstSessions = FormationHelper.GetLibPlanStageSessionInfo(Ident.Ide_Dossier, Ident.PlandeFormation, Ident.Intitule)
                If V_Cache.LstSessions.Count <= 0 Then
                    CRetour.Index = 2
                    CRetour.Retour = "Aucune session à verifier sur ce stage"
                    CRetour.TraitementSuivant = "Recherche terminée"
                    Return CRetour
                End If
                V_Cache.Sauve(ViewState)
                CRetour.Index = 1
                CRetour.TraitementSuivant = "Recherche des anomalies"
                If V_Cache.LstSessions.Count = 1 Then
                    CRetour.Retour = "Une session chargée"
                Else
                    CRetour.Retour = "" & V_Cache.LstSessions.Count & " sessions chargées"
                End If

            Case 1
                'On cherche les anomalies
                Dim PlanSessions As LibPlanStageSessionInfo = V_Cache.LstSessions(IdxDonnee)
                PlanSessions.ADesAnomalies = FormationHelper.GetSessionEnAnomalie(PlanSessions)

                V_Cache.Sauve(ViewState)

                CRetour.Index = IdxDonnee + 1
                CRetour.Retour = "Session du " & PlanSessions.DateDeb & " au " & PlanSessions.DateFin
                If PlanSessions.Horaires.Trim() <> "" Then
                    CRetour.Retour = CRetour.Retour & vbCrLf & vbTab & PlanSessions.Horaires
                Else
                    CRetour.Retour = CRetour.Retour & vbCrLf & vbTab & "journée complète"
                End If
                If PlanSessions.ADesAnomalies = True Then
                    CRetour.Retour = CRetour.Retour & vbTab & vbTab & vbTab & "En Anomalie"
                Else
                    CRetour.Retour = CRetour.Retour & vbTab & vbTab & vbTab & "OK"
                End If
                CRetour.TraitementSuivant = "Recherche des anomalies " & (IdxDonnee + 1) & "/" & V_Cache.LstSessions.Count
        End Select
        Return CRetour
    End Function

    Private Sub ExecuterVerification()
        Dim CRetour As RetAsync

        Select Case IndexOperation
            Case 0
                'Operation de chargement
                If IndexOperationCourante = IndexOperation Then
                    Exit Sub
                End If
                IndexOperationCourante = IndexOperation
                If IndexOperation = 0 Then
                    txtMsg.Text = "Virtualia effectue les recherches..."
                    TxtStatut.Text = "Recherche"
                End If
                CRetour = TraiteRechercheAsync(IndexOperation, 0).Result
                IndexOperation = CRetour.Index
                If IndexOperation = 2 Then
                    HorlogeTraitement.Enabled = False
                    NumeroTraitement = 0
                    If CRetour.TraitementSuivant = "Recherche terminée" Then
                        WsCtrlGestion.Actualiser = (CRetour.Retour = "Ce stage a été supprimé par un autre utilisateur")
                    End If
                    txtMsg.Text = CRetour.Retour
                    Exit Sub
                End If

            Case 1
                If Index = 0 Then
                    txtMsg.Text = "Virtualia effectue les recherches..."
                End If
                'Operation de recherche d'anomailes
                If IndexCourant = Index Then
                    Exit Sub
                End If
                IndexCourant = Index
                CRetour = TraiteRechercheAsync(IndexOperation, Index).Result
                Index = CRetour.Index
                If CRetour.Retour.ToLower().EndsWith(" anomalie") Then
                    txtMsg.Text = txtMsg.Text & vbCrLf & "   " & CRetour.Retour
                End If
                If Index = V_Cache.LstSessions.Count Then
                    Dim NbAno As Integer = (From pss In V_Cache.LstSessions Where pss.ADesAnomalies Select pss).Count()
                    If NbAno > 0 Then
                        BtnVerif.Enabled = True
                        CellBtn.Visible = True

                        BtnVerif.Text = "Corriger"
                        EtiExplic1.Text = "Virtualia va rétablir la cohérence entre le tableau des inscriptions et les dossiers du personnel."
                        TxtStatut.Text = "Incohérences détectées"
                        HorlogeTraitement.Enabled = False
                        NumeroTraitement = 0
                        Exit Sub
                    End If
                    txtMsg.Text = ""
                    TxtStatut.Text = "Aucune incohérence détectée"
                    HorlogeTraitement.Enabled = False
                    NumeroTraitement = 0
                End If
        End Select
    End Sub

    Private Async Function TraiteCorrectionAsync(index As Integer, lstPssEnAnomalie As List(Of LibPlanStageSessionInfo)) As System.Threading.Tasks.Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, List(Of LibPlanStageSessionInfo), RetAsync) = Function(idx, lst)
                                                                                                 Return TraiterCorrection(idx, lst)
                                                                                             End Function
        Return Await System.Threading.Tasks.Task.FromResult(Of RetAsync)(FuncTraitement(index, lstPssEnAnomalie))
    End Function

    Private Function TraiterCorrection(idx As Integer, lstPssEnAnomalie As List(Of LibPlanStageSessionInfo)) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()
        Dim MoteurLD As GestionFormation = DirectCast(Page, FrmPlanDeFormation).MoteurFormation
        Dim pss As LibPlanStageSessionInfo = lstPssEnAnomalie(idx)

        CRetour.Index = idx + 1
        CRetour.TraitementSuivant = "Correction " & (idx + 1) & " / " & lstPssEnAnomalie.Count

        Dim CRetourCrea As String = FormationHelper.CorrigeInscriptionSansPerSession(pss, DirectCast(Page, FrmPlanDeFormation).ParamFormation)
        If CRetourCrea = "" Then
            Return CRetour
        End If
        If pss.DateDeb = "" Then
            CRetour.Retour = vbCrLf & "session récurrente"
        Else
            CRetour.Retour = vbCrLf & "session du " & pss.DateDeb & " au " & pss.DateFin
            If (pss.Horaires = "") Then
                CRetour.Retour = CRetour.Retour & " journée complète"
            Else
                CRetour.Retour = CRetour.Retour & " " & pss.Horaires
            End If
        End If
        CRetour.Retour &= vbCrLf & CRetourCrea
        Return CRetour
    End Function

    Private Sub ExecuterCorrection()
        If IndexCourant = Index Then
            Return
        End If
        If Index = 0 Then
            TxtStatut.Text = "Correction"
            txtMsg.Text = "Virtualia corrige les incohérences..." & vbCrLf
        End If
        IndexCourant = Index

        Dim lstPssEnAnomalie As List(Of LibPlanStageSessionInfo) = (From it In V_Cache.LstSessions Where it.ADesAnomalies Select it).ToList()
        Dim CRetourAsync As RetAsync = TraiteCorrectionAsync(Index, lstPssEnAnomalie).Result

        Index = CRetourAsync.Index
        txtMsg.Text = txtMsg.Text & vbCrLf & "  " & CRetourAsync.Retour
        If CRetourAsync.Index = lstPssEnAnomalie.Count Then
            HorlogeTraitement.Enabled = False
            NumeroTraitement = 0
            TxtStatut.Text = "Correction terminée"
        End If
    End Sub

End Class