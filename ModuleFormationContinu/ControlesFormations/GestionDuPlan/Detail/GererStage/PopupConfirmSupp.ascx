﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PopupConfirmSupp.ascx.vb" Inherits="Virtualia.Net.PopupConfirmSupp" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
        <asp:Table ID="tb" runat="server" Width="680px" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">

            <asp:TableRow>
                <asp:TableCell Height="5px" />
            </asp:TableRow>

            <asp:TableRow Height="40px">
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Table ID="Table1" runat="server" Width="200px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:Label ID="Label2" runat="server" Height="18px" Width="120px" Text="Confirmation" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="5px" />
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Table ID="Table2" runat="server" Width="600px" Height="200px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:TextBox ID="txtMsg" runat="server" Height="100%" Width="100%" TextMode="MultiLine" BorderStyle="None" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="5px" />
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Table ID="Table3" runat="server" Width="600px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="25px">
                                <asp:Label ID="Label1" runat="server" Height="22px" Width="400px" Text="Confirmez-vous la suppression ?" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                            <asp:TableCell Width="50px" HorizontalAlign="Left" VerticalAlign="Middle">
                                <Generic:BTN_GENE ID="btnOui" runat="server" Text="Oui"  />
                            </asp:TableCell>
                            <asp:TableCell Width="50px" HorizontalAlign="Left" VerticalAlign="Middle">
                                <Generic:BTN_GENE ID="btnNon" runat="server" Text="Non"  />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="5px" />
            </asp:TableRow>
        </asp:Table>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnOui" />
        <asp:PostBackTrigger ControlID="btnNon" />
    </Triggers>
</asp:UpdatePanel>

