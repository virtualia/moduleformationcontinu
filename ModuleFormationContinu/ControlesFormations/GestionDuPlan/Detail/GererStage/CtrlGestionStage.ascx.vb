﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation

Public Class CtrlGestionStage
    Inherits GestMultiVueBase

    Private WsCtrlDetail As CtrlDetailStage

    Public WriteOnly Property CtrlDetail As CtrlDetailStage
        Set(value As CtrlDetailStage)
            WsCtrlDetail = value
        End Set
    End Property

    Public Property IdeStage As Integer
        Get
            If Not (ViewState.KeyExiste("IdeStage")) Then
                ViewState.AjouteValeur("IdeStage", 0)
            End If

            Return DirectCast(ViewState("IdeStage"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IdeStage", value)
        End Set
    End Property

    Public Property IntituleStage As String
        Get
            If Not (ViewState.KeyExiste("IntituleStage")) Then
                ViewState.AjouteValeur("IntituleStage", "")
            End If

            Return DirectCast(ViewState("IntituleStage"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("IntituleStage", value)
        End Set
    End Property

    Public Property ReferenceStage As String
        Get
            If Not (ViewState.KeyExiste("RefStage")) Then
                ViewState.AjouteValeur("RefStage", "")
            End If

            Return DirectCast(ViewState("RefStage"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("RefStage", value)
        End Set
    End Property

    Protected Overrides Sub InitComposants()

        KeyMultiVue = "GESTIONSTAGE"

        Menus = CtrlMenus
        CmdRetour = CommandeRetour

        CadreMenu = CellMenuArbre

        DicoVue = New ItemDicoVueCollection(MultiOnglets)

        Dim itdico As ItemDicoVue

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Vérification de la cohérence des inscriptions"
        itdico.IDVue = "VueVerif"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Duplication d'un stage"
        itdico.IDVue = "VueIdentification"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Suppression d'un stage"
        itdico.IDVue = "VueSuppression"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        CtrlSuppression.CtrlGestion = Me
        CtrlVerif.CtrlGestion = Me
        CtrlDuplic.CtrlGestion = Me

    End Sub

    Public Overrides Sub Charge(source As Object)

        CtrlMenus.ReInit(True)
        CellMenuArbre.Visible = True

        IntituleStage = DirectCast(source, CacheSelectionArmoire).Libelle
        IdeStage = DirectCast(source, CacheSelectionArmoire).Ide
        ReferenceStage = DirectCast(source, CacheSelectionArmoire).Reference
        If ReferenceStage <> "" Then
            lblTitre.Text = "Gestion - " & IntituleStage & " (" & ReferenceStage & ")"
        Else
            lblTitre.Text = "Gestion - " & IntituleStage
        End If

    End Sub

    Protected Overrides Sub ClickMenu(ccache As CacheClickMenu)
        MyBase.ClickMenu(ccache)

        If (ccache.TypeClick = TypeClickMenu.RETOUR) Then
            Return
        End If

        If (ccache.Value.Split("|"c)(1).StartsWith("Suppression")) Then
            CtrlSuppression.Charge(Nothing)
            Return
        End If

        If (ccache.Value.Split("|"c)(1).StartsWith("Vérification")) Then
            CtrlVerif.Charge(Nothing)
            Return
        End If

        If (ccache.Value.Split("|"c)(1).StartsWith("Duplication")) Then
            CtrlDuplic.Charge(Nothing)
            Return
        End If

    End Sub

    Public Sub CacheOuMontreMenu(estvisible As Boolean)
        WsCtrlDetail.CacheOuMontreMenu(estvisible)
        CellMenuArbre.Visible = estvisible
    End Sub

    Protected Overrides Sub Retour()
        MyBase.Retour()
    End Sub

End Class