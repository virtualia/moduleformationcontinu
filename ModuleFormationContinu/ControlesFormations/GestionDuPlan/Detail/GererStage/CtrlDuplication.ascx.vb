﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports System.Web.UI
Imports System.Threading.Tasks
Imports Virtualia.Structure.Formation

Public Class CtrlDuplication
    Inherits UserControl
    Implements IControlBase
    Private WsCtrlGestion As CtrlGestionStage

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionStage
        Set(value As CtrlGestionStage)
            WsCtrlGestion = value
        End Set
    End Property

    Private ReadOnly Property MoteurFormation As Virtualia.Metier.Formation.GestionFormation
        Get
            Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            Return DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).Gestion_Plandeformation
        End Get
    End Property

    Private Property IndexOperation As Integer
        Get
            If (Not ViewState.KeyExiste("IndexOperation")) Then
                ViewState.AjouteValeur("IndexOperation", -1)
            End If

            Return DirectCast(ViewState("IndexOperation"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexOperation", value)
        End Set
    End Property

    Private Property IndexOperationCourante As Integer
        Get
            If (Not ViewState.KeyExiste("IndexOperationCourante")) Then
                ViewState.AjouteValeur("IndexOperationCourante", -1)
            End If

            Return DirectCast(ViewState("IndexOperationCourante"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexOperationCourante", value)
        End Set
    End Property

    Private Property IdeNouv As Integer
        Get
            If (Not ViewState.KeyExiste("IdeNouv")) Then
                ViewState.AjouteValeur("IdeNouv", -1)
            End If

            Return DirectCast(ViewState("IdeNouv"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IdeNouv", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        BtnTrait.Act = Sub(btn)
                           CboPlanCible.Enabled = False
                           txtNouvelleValeur.Enabled = False
                           BtnTrait.Enabled = False

                           TbMessage.Visible = True
                           IndexOperation = 0
                           IndexOperationCourante = -1

                           IdeNouv = -1

                           TxtStatut.Text = ""

                           TimerTraitement.Enabled = True
                       End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        If TimerTraitement.Enabled = False Then
            Return
        End If
        DirectCast(Page.Master, VirtualiaMain).SessionCourante.ReinitListeArbo()
        Dim Sep As String = ""
        If IndexOperationCourante = IndexOperation Then
            Return
        End If
        IndexOperationCourante = IndexOperation
        If IndexOperation = 0 Then
            TxtStatut.Text = "Vérification de l'unicité du stage"
        Else
            Sep = vbCrLf & vbCrLf
        End If

        Dim CRetour As RetAsync = TraiteDuplicationAsync(IndexOperation).Result

        IndexOperation = CRetour.Index
        TxtStatut.Text = CRetour.TraitementSuivant
        txtMsg.Text = CRetour.Retour

        If IndexOperation > 1 Then
            TimerTraitement.Enabled = False
            If CRetour.Retour = "Ce nouveau stage existe déjà" Then
                BtnTrait.Enabled = True
                txtNouvelleValeur.Enabled = True
                CboPlanCible.Enabled = True
                Return
            End If
            If CRetour.Retour = "Le stage source n'existe plus" Then
                WsCtrlGestion.Actualiser = True
                Return
            End If
            WsCtrlGestion.Actualiser = (CRetour.TraitementSuivant = "Duplication effectuée")
        End If
    End Sub

    Public Sub Charge(ByVal items As Object) Implements IControlBase.Charge
        If CboPlanCible.Items.Count <= 0 Then
            ChargeCboPlan()
        End If
        BtnTrait.Enabled = False
        txtNouvelleValeur.Text = WsCtrlGestion.IntituleStage
        CboPlanCible.Enabled = True
        txtNouvelleValeur.Enabled = True

        TbMessage.Visible = False
        TxtStatut.Text = ""
        txtMsg.Text = ""
    End Sub

    Private Sub ChargeCboPlan()
        CboPlanCible.Items.Clear()
        CboPlanCible.Items.Add(New ListItem("Sélectionner....", "0000"))
        MoteurFormation.GetListPlanFiltre().ForEach(Sub(pf)
                                                        CboPlanCible.Items.Add(New ListItem(pf, pf))
                                                    End Sub)
    End Sub

    Private Async Function TraiteDuplicationAsync(ByVal IndexTraitement As Integer) As Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(idxtrait)
                                                               Return TraiteDuplication(idxtrait)
                                                           End Function

        Return Await Task.FromResult(Of RetAsync)(FuncTraitement(IndexTraitement))
    End Function

    Private Function TraiteDuplication(ByVal IdxTrait As Integer) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()

        Select Case IdxTrait
            Case 0
                'On teste l'unicite 
                Dim LDGlobales As Virtualia.Net.Session.LDObjetGlobal = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)

                Dim idents As List(Of Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION)
                Dim LstParamSel As List(Of KeyValuePair(Of Integer, String)) = New List(Of KeyValuePair(Of Integer, String))
                LstParamSel.Add(New KeyValuePair(Of Integer, String)(1, txtNouvelleValeur.Text.Trim))
                idents = LDGlobales.ObjetGlobalModule.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION)(LDGlobales.VirSelectionFiltree(9, 1, 0, LstParamSel))

                If idents IsNot Nothing AndAlso idents.Count > 0 Then
                    CRetour.TraitementSuivant = "Duplication impossible"
                    CRetour.Index = 2
                    CRetour.Retour = "Ce nouveau stage existe déjà"
                    Return CRetour
                End If
                CRetour.TraitementSuivant = "Duplication du stage"
                CRetour.Index = 1
                CRetour.Retour = "Vérification de l'unicité" & vbTab & "OK"

            Case 1
                'On duplique 
                MoteurFormation.ReinitStructure()
                Dim st As Virtualia.Metier.Formation.StructureStage = TryCast(MoteurFormation.GetStructure(WsCtrlGestion.IdeStage), Virtualia.Metier.Formation.StructureStage)
                If st Is Nothing Then
                    CRetour.TraitementSuivant = "Duplication impossible"
                    CRetour.Index = 2
                    CRetour.Retour = "Le stage source n'existe plus"
                    Return CRetour
                End If
                st.Identification.Ide_Dossier = 0
                st.Identification.PlandeFormation = CboPlanCible.SelectedItem.Text
                st.Identification.Intitule = txtNouvelleValeur.Text.Trim()
                st.Identification.DateSaisie = DateTime.Now.ToShortDateString
                If st.Caracteristique IsNot Nothing Then
                    st.Caracteristique.Ide_Dossier = 0
                End If
                st.Evaluation = Nothing
                st.Factures.Clear()
                'st.Intervenants.Clear()
                st.Sessions.Clear()
                If st.Couts.Count > 0 Then
                    Dim ctnouv As Virtualia.TablesObjet.ShemaREF.FOR_COUTS = (From c In st.Couts.GetToutesLesFiches()
                                                                              Order By DirectCast(c, Virtualia.TablesObjet.ShemaREF.FOR_COUTS).Date_Valeur_ToDate
                                       Descending Select DirectCast(c, Virtualia.TablesObjet.ShemaREF.FOR_COUTS)).First()
                    ctnouv.Ide_Dossier = 0
                    ctnouv.Date_de_Valeur = "01/01/" & DateTime.Now.Year

                    st.Couts.Clear()
                    st.Couts.Add(ctnouv)
                End If

                Dim CRetourserv As Virtualia.Structure.Formation.RetourCRUD = MoteurFormation.AjouteStructure(st)

                If CRetourserv.Index <= 0 Then
                    CRetour.TraitementSuivant = "Duplication impossible"
                    CRetour.Index = 2
                    CRetour.Retour = txtNouvelleValeur.Text & vbTab & "ERREUR"
                    Return CRetour
                End If

                CRetour.TraitementSuivant = "Duplication effectuée"
                CRetour.Index = 2
                CRetour.Retour = txtNouvelleValeur.Text & vbTab & "OK"

        End Select

        Return CRetour
    End Function

    Protected Sub txtNouvelleValeur_TextChanged(sender As Object, e As EventArgs)
        BtnTrait.Enabled = CboPlanCible.SelectedIndex > 0 AndAlso txtNouvelleValeur.Text.ToLower().Trim() <> "" _
            AndAlso txtNouvelleValeur.Text.ToLower().Trim() <> WsCtrlGestion.IntituleStage.ToLower().Trim()
    End Sub

    Protected Sub CboPlanCible_SelectedIndexChanged(sender As Object, e As EventArgs)
        BtnTrait.Enabled = CboPlanCible.SelectedIndex > 0 AndAlso txtNouvelleValeur.Text.ToLower().Trim() <> "" _
                            AndAlso txtNouvelleValeur.Text.ToLower().Trim() <> WsCtrlGestion.IntituleStage.ToLower().Trim()
    End Sub

End Class