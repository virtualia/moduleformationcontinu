﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Threading.Tasks
Imports Virtualia.Structure.Formation

Public Class CtrlSuppression
    Inherits UserControl
    Implements IControlBase

    Private WsCtrlGestion As CtrlGestionStage

    Public WriteOnly Property CtrlGestion As CtrlGestionStage
        Set(value As CtrlGestionStage)
            WsCtrlGestion = value
        End Set
    End Property

    Private Property IndexOperation As Integer
        Get
            If (Not ViewState.KeyExiste("IndexOperation")) Then
                ViewState.AjouteValeur("IndexOperation", -1)
            End If

            Return DirectCast(ViewState("IndexOperation"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexOperation", value)
        End Set
    End Property

    Private Property IndexOperationCourante As Integer
        Get
            If (Not ViewState.KeyExiste("IndexOperationCourante")) Then
                ViewState.AjouteValeur("IndexOperationCourante", -1)
            End If

            Return DirectCast(ViewState("IndexOperationCourante"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexOperationCourante", value)
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        BtnSuppr.Act = Sub(btn)
                           IndexOperation = 0
                           IndexOperationCourante = -1

                           BtnSuppr.Enabled = False
                           TbMessage.Visible = True

                           DirectCast(Page.Master, VirtualiaMain).SessionCourante.ReinitListeArbo()

                           TimerAttente.Enabled = True
                       End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)

        If Not (TimerAttente.Enabled) Then
            Return
        End If

        If (IndexOperationCourante = IndexOperation) Then
            Return
        End If

        IndexOperationCourante = IndexOperation

        If (IndexOperation = 0) Then
            TxtStatut.Text = "Existence d'inscription"
        End If

        Dim CRetour As RetAsync = TraiteSuppressionAsync(IndexOperation).Result

        IndexOperation = CRetour.Index
        TxtStatut.Text = CRetour.TraitementSuivant
        txtMsg.Text = CRetour.Retour

        If (IndexOperation > 1) Then
            TimerAttente.Enabled = False

            If (CRetour.TraitementSuivant = "Suppression impossible") Then
                Return
            End If

            Table1.Visible = False
            WsCtrlGestion.CacheOuMontreMenu(False)
            WsCtrlGestion.Actualiser = (CRetour.TraitementSuivant <> "Suppression impossible")
        End If

    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        Table1.Visible = True
        BtnSuppr.Enabled = True
        TbMessage.Visible = False
        txtMsg.Text = ""
        TxtStatut.Text = ""
    End Sub

    Private Async Function TraiteSuppressionAsync(index As Integer) As Task(Of RetAsync)

        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(idx)
                                                               Return TraiteSuppression(idx)
                                                           End Function

        Return Await Task.FromResult(Of RetAsync)(FuncTraitement(index))
    End Function

    Private Function TraiteSuppression(idx As Integer) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()
        Dim mot As GestionFormation = DirectCast(Page, FrmPlanDeFormation).MoteurFormation

        Select Case idx
            Case 0
                'On verifie l'existence d'inscription
                Dim nbinsc As Integer = FormationHelper.GetForInscriptionsDeStage(WsCtrlGestion.IdeStage).Count

                If (nbinsc <= 0) Then
                    CRetour.Index = idx + 1
                    CRetour.TraitementSuivant = "Suppression du stage"
                    CRetour.Retour = "Aucune inscription sur ce stage"
                    Return CRetour
                End If

                CRetour.Index = 2
                CRetour.TraitementSuivant = "Suppression impossible"

                If (nbinsc = 1) Then
                    CRetour.Retour = "Il y a une inscription sur ce stage"
                Else
                    CRetour.Retour = "Il y a " & nbinsc & " inscriptions sur ce stage"
                End If
            Case 1
                CRetour.Index = 2
                'On supprime
                Dim sts As StructureStage = TryCast(mot.GetStructure(WsCtrlGestion.IdeStage), StructureStage)
                If sts Is Nothing Then
                    CRetour.TraitementSuivant = "Suppression impossible"
                    CRetour.Retour = "Le stage """ & sts.Identification.Intitule & """ a été supprimé par un autre utilisateur"
                    Return CRetour
                End If
                Dim CRetoursup As RetourCRUD = mot.SupprimeDossier(WsCtrlGestion.IdeStage)
                If CRetoursup.Index <= 0 Then
                    CRetour.TraitementSuivant = "Suppression impossible"
                    CRetour.Retour = "Erreur lors de la suppression de """ & sts.Identification.Intitule & """"
                    Return CRetour
                End If
                CRetour.TraitementSuivant = "Suppression effectuée"
                CRetour.Retour = ""
        End Select

        Return CRetour
    End Function

End Class