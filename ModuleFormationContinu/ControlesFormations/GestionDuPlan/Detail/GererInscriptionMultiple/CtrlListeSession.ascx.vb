﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing

Public Class CtrlListeSession
    Inherits UserControl

    Private WsCtrlGestion As CtrlGestionMultiInscription
    Private WsCtrlChoix As CtrlChoixSession
    Private WsLstChk As List(Of CheckBox) = New List(Of CheckBox)()
    Private WsCouleurLigne As Color = VisuHelper.ConvertiCouleur("#E2E2E2")
    Private WsCouleurBouton As Color = Color.LightGray
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Public WriteOnly Property CtrlGestion As CtrlGestionMultiInscription
        Set(value As CtrlGestionMultiInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public WriteOnly Property CtrlChoix As CtrlChoixSession
        Set(value As CtrlChoixSession)
            WsCtrlChoix = value
        End Set
    End Property

    Private Property DateMinSelection As DateTime
        Get
            If Not (ViewState.KeyExiste("DateMinSelection")) Then
                ViewState.AjouteValeur("DateMinSelection", DateTime.Now)
            End If
            Return DirectCast(ViewState("DateMinSelection"), DateTime)
        End Get
        Set(value As DateTime)
            ViewState.AjouteValeur("DateMinSelection", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        Dim lstbtn As List(Of Button) = VisuHelper.GetControlSaisie(Of Button)(TbBoutonsCalendrier)

        lstbtn.ForEach(Sub(bt)
                           AddHandler bt.Click, Sub(s, ev)
                                                    Dim dteaajouter As DateTime = New DateTime(Integer.Parse(CboAnnee.SelectedItem.Text), (CboMois.SelectedIndex + 1), Integer.Parse(DirectCast(s, Button).Text))

                                                    If Not (WsCtrlGestion.VCache.SessionRecurrentes.Contains(dteaajouter.ToShortDateString())) Then
                                                        WsCtrlGestion.VCache.SessionRecurrentes.Add(dteaajouter.ToShortDateString)
                                                        WsCtrlGestion.SauveViewState()
                                                    End If
                                                    ChargeListeReccurente()
                                                End Sub
                       End Sub)

        BtnSuppr.Act = Sub(btn)
                           If CtrlArmoireRecurrente.ItemSelectionne Is Nothing Then
                               Return
                           End If
                           Dim dtesel As String = CtrlArmoireRecurrente.ItemSelectionne.Libelle.Replace("Le lundi ", "").Replace("Le mardi ", "").Replace("Le mercredi ", "").Replace("Le jeudi ", "").Replace("Le vendredi ", "").Replace("Le samedi ", "").Replace("Le dimanche ", "")

                           WsCtrlGestion.VCache.SessionRecurrentes.Remove(dtesel)
                           WsCtrlGestion.SauveViewState()
                           ChargeListeReccurente()
                       End Sub

        BtnGeneSessionRecc.Act = Sub(btn)
                                     If TBreccurente.Visible Then
                                         Return
                                     End If
                                     TBreccurente.Visible = True
                                     ConstruitCalendrier()
                                 End Sub

        If IsPostBack = True Then
            Exit Sub
        End If

        CtrlArmoireRecurrente.Charge(Nothing)

        CboAnnee.Items.Add("" & DateTime.Now.Year)
        CboAnnee.Items.Add("" & (DateTime.Now.Year + 1))
        CboAnnee.SelectedIndex = 0

        CboMois.Items.Add("Janvier")
        CboMois.Items.Add("Février")
        CboMois.Items.Add("Mars")
        CboMois.Items.Add("Avril")
        CboMois.Items.Add("Mai")
        CboMois.Items.Add("Juin")
        CboMois.Items.Add("Juillet")
        CboMois.Items.Add("Août")
        CboMois.Items.Add("Septembre")
        CboMois.Items.Add("Octobre")
        CboMois.Items.Add("Novembre")
        CboMois.Items.Add("Décembre")

        CboMois.SelectedIndex = (DateTime.Now.Month - 1)

    End Sub

    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        ConstruitControles()
    End Sub

    Public Sub Charge()
        WsCtrlGestion.VCache.SessionsSelectionnees(0).EstSelectionne = True
        WsCtrlGestion.SauveViewState()
        ConstruitControles()
        InitRecurrente()
    End Sub

    Public Sub ViderDetail()
        TbDetail.Rows.Clear()
    End Sub

    Private Sub ConstruitControles()
        WsLstChk = New List(Of CheckBox)()
        ViderDetail()

        Dim qr = From ss In WsCtrlGestion.VCache.SessionsSelectionnees Where WsCtrlGestion.VCache.SessionsSelectionnees.IndexOf(ss) > 0 Select ss
        Dim idx As Integer = 0
        For Each ss In qr
            idx = idx + 1
            ConstruitLigneDetail(ss, idx)
        Next

        WsLstChk.ForEach(Sub(ck As CheckBox)
                             AddHandler ck.CheckedChanged, AddressOf ChkClick
                         End Sub)
    End Sub

    Private Sub ConstruitLigneDetail(ByVal ss As StageSessionInfo, ByVal idx As Integer)
        Dim Rangee As TableRow = New TableRow()
        Rangee.ID = "RDetail" & idx

        Rangee.Cells.Add(ConstructionCellSession.ConstruitCellChk(ss, idx, WsLstChk))
        Rangee.Cells.Add(ConstructionCellSession.ConstruitCellLabel(ss, idx, ss.Dates, "100%", "100%"))
       
        TbDetail.Rows.Add(Rangee)
    End Sub

    Private Sub GereChevauchementAutre(ByVal idxselectionne As Integer)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim StageSession As StageSessionInfo

        Dim linkQuery = From Info In WsCtrlGestion.VCache.SessionsSelectionnees Where Info.DateDebut <> "" _
                AndAlso WsCtrlGestion.VCache.SessionsSelectionnees.IndexOf(Info) <> idxselectionne _
                AndAlso Info.EstSelectionne Select Index = WsCtrlGestion.VCache.SessionsSelectionnees.IndexOf(Info), SessionSel = Info

        If linkQuery.Count() <= 0 Then
            WsCtrlGestion.VCache.SessionsSelectionnees(idxselectionne).EstSelectionne = True
            WsCtrlGestion.SauveViewState()
            WsCtrlChoix.GereBoutonEtape()
            Exit Sub
        End If

        StageSession = WsCtrlGestion.VCache.SessionsSelectionnees(idxselectionne)

        Dim lstindexchevauchement As List(Of Integer) = (From sdyn In linkQuery _
                    Where WebFct.ViRhDates.SiChevauchementDates(StageSession.DateDebut, StageSession.DateFin, sdyn.SessionSel.DateDebut, _
                                    sdyn.SessionSel.DateFin) Select sdyn.Index).ToList()

        If lstindexchevauchement.Count <= 0 Then
            WsCtrlGestion.VCache.SessionsSelectionnees(idxselectionne).EstSelectionne = True
            WsCtrlGestion.SauveViewState()
            WsCtrlChoix.GereBoutonEtape()
            Exit Sub
        End If

        WsCtrlGestion.AfficheAnomalieSession(idxselectionne, lstindexchevauchement)

    End Sub

    Public Sub SelectOuPasChk(idx As Integer, coche As Boolean)
        WsLstChk(idx - 1).Checked = coche
    End Sub

    Private Sub InitRecurrente()
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        WsCtrlGestion.VCache.ModelSessionRecurente = New StageSessionInfo()
        WsCtrlGestion.SauveViewState()

        CboAnnee.SelectedIndex = 0
        CboMois.SelectedIndex = (DateTime.Now.Month - 1)

        CtrlArmoireRecurrente.Charge(Nothing)

        TBreccurente.Visible = False
        BtnGeneSessionRecc.Visible = False

        '** Note RP les tests LD sont à partir du Deuxième dans la liste car liste préinitialisée avec Pré-inscriptions **
        '** On choisit la présence d'une session obligatoirement conmme session référente
        If WsCtrlGestion.VCache.SessionsSelectionnees.Count > 2 Then
            Return
        End If
        If WsCtrlGestion.VCache.SessionsSelectionnees.Count = 1 Then
            'BtnGeneSessionRecc.Visible = True
            'DateMinSelection = WebFct.ViRhDates.DateTypee("01/01/" & DateTime.Now.Year)
            'WsCtrlGestion.VCache.ModelSessionRecurente.Intitule = WsCtrlGestion.VCache.SessionsSelectionnees(0).Intitule
            'WsCtrlGestion.VCache.ModelSessionRecurente.NbHeure = WsCtrlGestion.VCache.SessionsSelectionnees(0).NbHeure
            'WsCtrlGestion.VCache.ModelSessionRecurente.RangSession = Integer.MinValue
            'Label14.Text = "Nb Heures : " & WebFct.ViRhFonction.MontantEdite(WsCtrlGestion.VCache.SessionsSelectionnees(0).NbHeure, 1)
            'Label22.Text = "Nb Jours : " & WebFct.ViRhFonction.MontantEdite(DirectCast(Page, FrmPlanDeFormation).ParamFormation.CalculNbJour(WsCtrlGestion.VCache.SessionsSelectionnees(0).NbHeure), 1)
            Return
        End If
        If WsCtrlGestion.VCache.SessionsSelectionnees(1).DateDebut <> WsCtrlGestion.VCache.SessionsSelectionnees(1).DateFin Then
            Return
        End If

        BtnGeneSessionRecc.Visible = True

        DateMinSelection = WebFct.ViRhDates.DateTypee(WsCtrlGestion.VCache.SessionsSelectionnees(1).DateDebut)

        WsCtrlGestion.VCache.ModelSessionRecurente.Ide = WsCtrlGestion.VCache.SessionsSelectionnees(1).Ide
        WsCtrlGestion.VCache.ModelSessionRecurente.Intitule = WsCtrlGestion.VCache.SessionsSelectionnees(1).Intitule
        WsCtrlGestion.VCache.ModelSessionRecurente.IntituleSession = WsCtrlGestion.VCache.SessionsSelectionnees(1).IntituleSession

        WsCtrlGestion.VCache.ModelSessionRecurente.DateDebut = WsCtrlGestion.VCache.SessionsSelectionnees(1).DateDebut
        WsCtrlGestion.VCache.ModelSessionRecurente.DateFin = WsCtrlGestion.VCache.SessionsSelectionnees(1).DateFin

        WsCtrlGestion.VCache.ModelSessionRecurente.Hdeb_am = WsCtrlGestion.VCache.SessionsSelectionnees(1).Hdeb_am
        WsCtrlGestion.VCache.ModelSessionRecurente.Hdeb_pm = WsCtrlGestion.VCache.SessionsSelectionnees(1).Hdeb_pm
        WsCtrlGestion.VCache.ModelSessionRecurente.HFin_am = WsCtrlGestion.VCache.SessionsSelectionnees(1).HFin_am
        WsCtrlGestion.VCache.ModelSessionRecurente.HFin_pm = WsCtrlGestion.VCache.SessionsSelectionnees(1).HFin_pm
        WsCtrlGestion.VCache.ModelSessionRecurente.NbHeure = WsCtrlGestion.VCache.SessionsSelectionnees(1).NbHeure

        WsCtrlGestion.VCache.ModelSessionRecurente.Ref_Session = WsCtrlGestion.VCache.SessionsSelectionnees(1).Ref_Session
        WsCtrlGestion.VCache.ModelSessionRecurente.RangSession = WsCtrlGestion.VCache.SessionsSelectionnees(1).RangSession

        Label14.Text = "Nb Heures : " & WebFct.ViRhFonction.MontantEdite(WsCtrlGestion.VCache.SessionsSelectionnees(1).NbHeure, 1)
        Label22.Text = "Nb Jours : " & WebFct.ViRhFonction.MontantEdite(DirectCast(Page, FrmPlanDeFormation).ParamFormation.CalculNbJour(WsCtrlGestion.VCache.SessionsSelectionnees(1).NbHeure), 1)

    End Sub

    Private Sub ConstruitCalendrier()

        Dim lstbtn As List(Of Button) = VisuHelper.GetControlSaisie(Of Button)(TbBoutonsCalendrier)
        For Each bt In lstbtn
            bt.Enabled = True
            bt.BackColor = Color.LightGray
            bt.ForeColor = Color.Black
            bt.Visible = False
        Next

        R1.Visible = False
        R2.Visible = False
        R3.Visible = False
        R4.Visible = False
        R5.Visible = False
        R6.Visible = False


        Dim dtemax As DateTime
        If CboMois.SelectedIndex = 11 Then
            dtemax = New DateTime(Integer.Parse(CboAnnee.SelectedItem.Text) + 1, 1, 1)
        Else
            dtemax = New DateTime(Integer.Parse(CboAnnee.SelectedItem.Text), CboMois.SelectedIndex + 2, 1)
        End If

        Dim dtetmp As DateTime = New DateTime(Integer.Parse(CboAnnee.SelectedItem.Text), (CboMois.SelectedIndex + 1), 1)

        Dim dadd As Integer = 0
        Select Case dtetmp.DayOfWeek
            Case DayOfWeek.Monday
                dadd = 0
            Case DayOfWeek.Tuesday
                dadd = -1
            Case DayOfWeek.Wednesday
                dadd = -2
            Case DayOfWeek.Thursday
                dadd = -3
            Case DayOfWeek.Friday
                dadd = -4
            Case DayOfWeek.Saturday
                dadd = -5
            Case DayOfWeek.Sunday
                dadd = -6
        End Select

        dtetmp = dtetmp.AddDays(dadd)

        Dim continuer As Boolean = True
        Dim NumR As Integer = 0
        Dim Btn As Button = Nothing

        Dim lstdteferie As List(Of String) = GetLstJourFerie()

        While continuer

            Select Case dtetmp.DayOfWeek
                Case DayOfWeek.Monday
                    NumR = NumR + 1
                    Select Case NumR
                        Case 1
                            R1.Visible = True
                        Case 2
                            R2.Visible = True
                        Case 3
                            R3.Visible = True
                        Case 4
                            R4.Visible = True
                        Case 5
                            R5.Visible = True
                        Case 6
                            R6.Visible = True
                    End Select
                    Btn = (From bt In lstbtn Where bt.ID.EndsWith("_" & NumR & "_lu") Select bt).First()
                Case DayOfWeek.Tuesday
                    Btn = (From bt In lstbtn Where bt.ID.EndsWith("_" & NumR & "_ma") Select bt).First()
                Case DayOfWeek.Wednesday
                    Btn = (From bt In lstbtn Where bt.ID.EndsWith("_" & NumR & "_me") Select bt).First()
                Case DayOfWeek.Thursday
                    Btn = (From bt In lstbtn Where bt.ID.EndsWith("_" & NumR & "_je") Select bt).First()
                Case DayOfWeek.Friday
                    Btn = (From bt In lstbtn Where bt.ID.EndsWith("_" & NumR & "_ve") Select bt).First()
                Case DayOfWeek.Saturday, DayOfWeek.Sunday
                    Btn = Nothing
            End Select

            If Btn Is Nothing Then
                dtetmp = dtetmp.AddDays(2)
                continuer = DateTime.Compare(dtetmp, dtemax) < 0
                Continue While
            End If

            If Not ("" & dtetmp.Year = CboAnnee.SelectedItem.Text AndAlso dtetmp.Month = (CboMois.SelectedIndex + 1)) Then
                dtetmp = dtetmp.AddDays(1)
                continuer = DateTime.Compare(dtetmp, dtemax) < 0
                Continue While
            End If

            Btn.Visible = True
            Btn.Text = "" & dtetmp.Day

            If lstdteferie.Contains(dtetmp.ToShortDateString) Then
                Btn.BackColor = Color.Gray
                Btn.ForeColor = Color.Snow
                Btn.Enabled = False
            End If

            If (DateTime.Compare(dtetmp, DateMinSelection) < 0) Then
                Btn.BackColor = Color.Snow
                Btn.ForeColor = Color.Gray
                Btn.Enabled = False
            End If

            dtetmp = dtetmp.AddDays(1)

            continuer = DateTime.Compare(dtetmp, dtemax) < 0
        End While

    End Sub

    Private Function GetLstJourFerie() As List(Of String)
        Dim jfs As String = Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase.RhDate.ListeDesJoursFeries("" & CboAnnee.SelectedItem.Text, (CboMois.SelectedIndex + 1))
        If jfs.Trim() = "" Then
            Return New List(Of String)()
        End If
        Dim LstResultat As List(Of String) = New List(Of String)()

        For Each sd As String In jfs.Split("~"c)
            If (sd.Trim() = "") Then
                Exit For
            End If
            LstResultat.Add(sd.Trim())
        Next
        Return LstResultat
    End Function

    Private Sub ChargeListeReccurente()
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        WsCtrlGestion.VCache.SessionsSelectionnees.ForEach(Sub(it)
                                                               it.EstSelectionne = False
                                                           End Sub)
        WsCtrlGestion.SauveViewState()

        For Each ck In WsLstChk
            ck.Checked = False
        Next

        WsCtrlChoix.GereBoutonEtape()

        CtrlArmoireRecurrente.Charge(Nothing)

        Dim lstitem As List(Of ElementArmoire) = New List(Of ElementArmoire)()
        Dim idx As Integer = 1
        Dim funclib As Func(Of DateTime, String) = Function(dt)
                                                       Dim CRetour As String = "Le "
                                                       CRetour &= ""
                                                       Select Case (dt.DayOfWeek)
                                                           Case DayOfWeek.Monday
                                                               CRetour &= "lundi "
                                                           Case DayOfWeek.Tuesday
                                                               CRetour &= "mardi "
                                                           Case DayOfWeek.Wednesday
                                                               CRetour &= "mercredi "
                                                           Case DayOfWeek.Thursday
                                                               CRetour &= "jeudi "
                                                           Case DayOfWeek.Friday
                                                               CRetour &= "vendredi "
                                                           Case DayOfWeek.Saturday
                                                               CRetour &= "samedi "
                                                           Case DayOfWeek.Sunday
                                                               CRetour &= "dimanche "
                                                       End Select
                                                       Return CRetour & dt.ToShortDateString
                                                   End Function


        For Each it In (From Chaine In WsCtrlGestion.VCache.SessionRecurrentes Order By WebFct.ViRhDates.DateTypee(Chaine) Select WebFct.ViRhDates.DateTypee(Chaine))
            lstitem.Add(New ElementArmoire(idx, funclib(it), ConstanteFormation.IconeSession))
            idx = idx + 1
        Next
        CtrlArmoireRecurrente.Charge(lstitem)

    End Sub

    Private Sub ChkClick(s As Object, ev As EventArgs)
        Dim chk As CheckBox = DirectCast(s, CheckBox)
        Dim idx As Integer = Integer.Parse(chk.ID.Split("_"c)(1))

        If chk.Checked = True Then
            WsCtrlGestion.VCache.SessionsSelectionnees(0).EstSelectionne = False
            WsCtrlGestion.SauveViewState()
        End If

        If chk.Checked = False Then
            WsCtrlGestion.VCache.SessionsSelectionnees(idx).EstSelectionne = False
            WsCtrlGestion.SauveViewState()
            WsCtrlChoix.GereBoutonEtape()
            Exit Sub
        End If

        WsCtrlGestion.VCache.SessionRecurrentes.Clear()
        CtrlArmoireRecurrente.Charge(Nothing)

        TBreccurente.Visible = False

        GereChevauchementAutre(idx)

    End Sub

    Protected Sub CboAnnee_SelectedIndexChanged(sender As Object, e As EventArgs)
        ConstruitCalendrier()
    End Sub

    Protected Sub CboMois_SelectedIndexChanged(sender As Object, e As EventArgs)
        ConstruitCalendrier()
    End Sub

End Class