﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlGestionMultiInscription.ascx.vb" Inherits="Virtualia.Net.CtrlGestionMultiInscription" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/ControlesGeneriques/V_Message.ascx" TagName="VMessage" TagPrefix="Generic" %>

<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlChoixSession.ascx" TagName="CTL_ETAPE1" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlChoixStagiaire.ascx" TagName="CTL_ETAPE2" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlModalite.ascx" TagName="CTL_ETAPE3" TagPrefix="Formation" %>
<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/CtrlConfirmation.ascx" TagName="CTL_ETAPE4" TagPrefix="Formation" %>

<%@ Register Src="~/ControlesFormations/GestionDuPlan/Detail/GererInscriptionMultiple/PopupDetailEtape.ascx" TagName="CTL_DETAIL" TagPrefix="Formation" %>

<asp:Table ID="CadreVues" runat="server" Width="1150px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Tb1" runat="server" Width="1150px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="200px">
                        <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" ImageAlign="Middle" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Button ID="BtnTitre" runat="server" Text="Procédure d'inscription - XXXXXXXXXXXXXXX" Height="22px" Width="900px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" ToolTip="Cliquer pour avoir des renseignements sur l'étape courante ..." Style="font-style: oblique; text-align: center;" OnClick="BtnTitre_Click"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="200px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" ColumnSpan="2">
            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                <asp:View ID="Vue1" runat="server">
                    <Formation:CTL_ETAPE1 ID="CtrlEtape1" runat="server" />
                </asp:View>
                <asp:View ID="Vue2" runat="server">
                    <Formation:CTL_ETAPE2 ID="CtrlEtape2" runat="server" />
                </asp:View>
                <asp:View ID="Vue3" runat="server">
                    <Formation:CTL_ETAPE3 ID="CtrlEtape3" runat="server" />
                </asp:View>
                <asp:View ID="Vue4" runat="server">
                    <Formation:CTL_ETAPE4 ID="CtrlEtape4" runat="server" />
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <ajaxToolkit:ModalPopupExtender ID="PopupDetailEtape" runat="server" TargetControlID="HidenPopupDetailEtape" PopupControlID="PanelDetailEtapePopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelDetailEtapePopup" runat="server">
                <Formation:CTL_DETAIL ID="CtrlDetail" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HidenPopupDetailEtape" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HidenPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Generic:VMessage ID="CtlMsg" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HidenPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
