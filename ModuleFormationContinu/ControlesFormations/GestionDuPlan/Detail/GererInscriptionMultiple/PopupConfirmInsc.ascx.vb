﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu.Formation
Imports System.Web.UI
Imports AjaxControlToolkit

Public Class PopupConfirmInsc
    Inherits UserControl

    Private WsPopup As ModalPopupExtender
    Private WsAct_Retour As Action(Of String)

    Public WriteOnly Property Popup As ModalPopupExtender
        Set(ByVal value As ModalPopupExtender)
            WsPopup = value
        End Set
    End Property

    Public WriteOnly Property Act_Retour As Action(Of String)
        Set(value As Action(Of String))
            WsAct_Retour = value
        End Set
    End Property

    Public Property NbAnomalie As Integer
        Set(value As Integer)
            ViewState.AjouteValeur("NbAnomalie", value)
            GenereMessage()
        End Set
        Private Get
            If Not (ViewState.KeyExiste("NbAnomalie")) Then
                ViewState.AjouteValeur("NbAnomalie", 0)
            End If
            Return DirectCast(ViewState("NbAnomalie"), Integer)
        End Get
    End Property

    Public Property NbValide As Integer
        Set(value As Integer)
            ViewState.AjouteValeur("NbValide", value)
            GenereMessage()
        End Set
        Private Get
            If Not (ViewState.KeyExiste("NbValide")) Then
                ViewState.AjouteValeur("NbValide", 0)
            End If
            Return DirectCast(ViewState("NbValide"), Integer)
        End Get
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        btnOuiToute.Act = Sub(btn)
                              WsAct_Retour("TOUTE")
                          End Sub
        btnOuiValide.Act = Sub(btn)
                               WsAct_Retour("VALIDE")
                           End Sub
        btnAnnuler.Act = Sub(btn)
                             WsPopup.Hide()
                         End Sub
    End Sub

    Private Sub GenereMessage()
        Dim msg As String = ""

        RowBtnValide.Visible = True

        If (NbValide > 0) Then
            If (NbValide = 1) Then
                msg = "Il y a une inscription valide" & vbCrLf
            Else
                msg = "Il y a " & NbValide & " inscriptions valides" & vbCrLf
            End If
        Else
            RowBtnValide.Visible = False
        End If

        If (NbAnomalie = 1) Then
            msg = msg & "Il y a une inscription en anomalie" & vbCrLf & vbCrLf
        Else
            msg = msg & "Il y a " & NbAnomalie & " inscriptions en anomalie" & vbCrLf & vbCrLf
        End If

        msg = msg & "Précisez l'action que vous voulez effectuer."

        txtMsg.Text = msg

    End Sub

End Class