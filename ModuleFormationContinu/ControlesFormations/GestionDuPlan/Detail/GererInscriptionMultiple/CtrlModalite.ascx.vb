﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports System.Drawing
Imports Virtualia.Systeme.Evenements

Public Class CtrlModalite
    Inherits UserControl
    Implements IEtape
    Private WsCtrlGestion As CtrlGestionMultiInscription
    Private WsEtapeSuivante As IEtape
    Private WsEtapePrecedente As IEtape
    Private WsDerniereEtape As IEtape

    Public ReadOnly Property NomControl As String Implements IEtape.NomControl
        Get
            Return "ETAPE_3"
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionMultiInscription
        Set(value As CtrlGestionMultiInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NumEtape As Integer Implements IEtape.NumEtape
        Get
            Return 2
        End Get
    End Property

    Public WriteOnly Property Rafraichir As Boolean Implements IEtape.Rafraichir
        Set(value As Boolean)
            ViewState.AjouteValeur("Rafraichir", value)
        End Set
    End Property

    Public WriteOnly Property EtapeSuivante As IEtape Implements IEtape.EtapeSuiv
        Set(value As IEtape)
            WsEtapeSuivante = value
        End Set
    End Property

    Public WriteOnly Property EtapePrecedente As IEtape Implements IEtape.EtapePrec
        Set(value As IEtape)
            WsEtapePrecedente = value
        End Set
    End Property

    Public WriteOnly Property DerniereEtape As IEtape Implements IEtape.DerniereEtape
        Set(value As IEtape)
            WsDerniereEtape = value
        End Set
    End Property


    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
        End If
    End Sub

    Public Sub Charge(items As Object) Implements IEtape.Charge
        If Not (DirectCast(ViewState("Rafraichir"), Boolean)) Then
            Exit Sub
        End If
        Rafraichir = False
        MultiOnglets.ActiveViewIndex = 0
        WsCtrlGestion.VCache.DoneeModalite.Vider()
        WsCtrlGestion.SauveViewState()
        CommandeSuiv.Visible = False

        Call ChargeFenetre()
    End Sub

    Private Sub ChargeFenetre()
        TxtDateInsc.DonText = WsCtrlGestion.VCache.DoneeModalite.DateInscription
        TxtDateInsc.DonBackColor = Color.White

        BtnSuivi.DonText = WsCtrlGestion.VCache.DoneeModalite.Suivi
        BtnSuivi.DonBackColor = Color.White

        If BtnSuivi.DonText = "" Then
            BtnMotif.DonBackColor = Color.Gray
            BtnMotif.V_SiEnLectureSeule = True
        Else
            If (FormationHelper.EstValeurDeComptabilisationEtDeValidation(WsCtrlGestion.VCache.DoneeModalite.Suivi)) Then
                BtnMotif.DonBackColor = Color.Gray
                BtnMotif.V_SiEnLectureSeule = True
            Else
                BtnMotif.DonText = WsCtrlGestion.VCache.DoneeModalite.Motif
                BtnMotif.DonBackColor = Color.White
            End If
        End If

        BtnPresence.DonText = WsCtrlGestion.VCache.DoneeModalite.Presence
        BtnPresence.DonBackColor = Color.White
        BtnPriorite.DonText = WsCtrlGestion.VCache.DoneeModalite.Priorite
        BtnPriorite.DonBackColor = Color.White
        BtnFiliere.DonText = WsCtrlGestion.VCache.DoneeModalite.Filiere
        BtnFiliere.DonBackColor = Color.White

        txtCommentaire.DonText = WsCtrlGestion.VCache.DoneeModalite.Commentaire
        txtCommentaire.DonBackColor = Color.White

    End Sub

    Protected Sub CommandeSuiv_Click(sender As Object, e As ImageClickEventArgs)
        WsCtrlGestion.GereEtapes(WsEtapeSuivante.NumEtape)
    End Sub

    Protected Sub CommandePrec_Click(sender As Object, e As ImageClickEventArgs)
        WsCtrlGestion.GereEtapes(WsEtapePrecedente.NumEtape)
    End Sub

    Private Sub DonTab_AppelTable(sender As Object, e As AppelTableEventArgs) Handles BtnFiliere.AppelTable, BtnMotif.AppelTable, BtnPresence.AppelTable,
            BtnPriorite.AppelTable, BtnSuivi.AppelTable

        If (DirectCast(sender, UserControl).ID = "BtnMotif" AndAlso FormationHelper.EstValeurDeComptabilisationEtDeValidation(WsCtrlGestion.VCache.DoneeModalite.Suivi)) Then
            Exit Sub
        End If
        TbEntete.Visible = False
        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Private Sub EtiDonnee_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles TxtDateInsc.ValeurChange, txtCommentaire.ValeurChange
        If (DirectCast(sender, Controles_VCoupleEtiDonnee).ID = "txtCommentaire") Then
            If WsCtrlGestion.VCache.DoneeModalite.Commentaire = e.Valeur Then
                Exit Sub
            End If
            WsCtrlGestion.VCache.DoneeModalite.Commentaire = e.Valeur
            WsDerniereEtape.Rafraichir = True
        End If

        If (DirectCast(sender, Controles_VCoupleEtiDonnee).ID = "TxtDateInsc") Then
            If WsCtrlGestion.VCache.DoneeModalite.DateInscription = e.Valeur Then
                Return
            End If
            WsCtrlGestion.VCache.DoneeModalite.DateInscription = e.Valeur
            WsDerniereEtape.Rafraichir = True
        End If
        WsCtrlGestion.SauveViewState()
        DirectCast(sender, Controles_VCoupleEtiDonnee).DonBackColor = VisuHelper.CouleurMaj()
    End Sub

    Private Sub Referentiel_ValeurSelectionnee(sender As Object, e As ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        TbEntete.Visible = True
        Select Case e.ControleAppelant
            Case "BtnSuivi"
                If BtnSuivi.DonText <> e.Valeur Then
                    BtnSuivi.DonBackColor = VisuHelper.CouleurMaj()
                    BtnSuivi.DonText = e.Valeur
                    WsCtrlGestion.VCache.DoneeModalite.Suivi = e.Valeur

                    If FormationHelper.EstValeurDeComptabilisationEtDeValidation(WsCtrlGestion.VCache.DoneeModalite.Suivi) Then
                        CommandeSuiv.Visible = True
                        If BtnMotif.DonText.Trim() <> "" Then
                            BtnMotif.DonText = ""
                            WsCtrlGestion.VCache.DoneeModalite.Motif = ""
                        End If
                        BtnMotif.DonBackColor = Color.Gray
                        BtnMotif.V_SiEnLectureSeule = True
                    ElseIf BtnMotif.DonText = "" And e.Valeur <> "" Then
                        CommandeSuiv.Visible = False
                        BtnMotif.DonBackColor = Color.White
                        BtnMotif.V_SiEnLectureSeule = False
                    End If
                End If
                WsDerniereEtape.Rafraichir = True
            Case "BtnMotif"
                If BtnMotif.DonText <> e.Valeur Then
                    BtnMotif.DonBackColor = VisuHelper.CouleurMaj()
                    BtnMotif.DonText = e.Valeur
                    WsCtrlGestion.VCache.DoneeModalite.Motif = e.Valeur
                    CommandeSuiv.Visible = (e.Valeur <> "")
                    WsDerniereEtape.Rafraichir = True
                End If
            Case "BtnPresence"
                If BtnPresence.DonText <> e.Valeur Then
                    BtnPresence.DonBackColor = VisuHelper.CouleurMaj()
                    BtnPresence.DonText = e.Valeur
                    WsCtrlGestion.VCache.DoneeModalite.Presence = e.Valeur
                    WsDerniereEtape.Rafraichir = True
                End If
            Case "BtnPriorite"
                If BtnPriorite.DonText <> e.Valeur Then
                    BtnPriorite.DonBackColor = VisuHelper.CouleurMaj()
                    BtnPriorite.DonText = e.Valeur
                    WsCtrlGestion.VCache.DoneeModalite.Priorite = e.Valeur
                    WsDerniereEtape.Rafraichir = True
                End If
            Case "BtnFiliere"
                If BtnFiliere.DonText <> e.Valeur Then
                    BtnFiliere.DonBackColor = VisuHelper.CouleurMaj()
                    BtnFiliere.DonText = e.Valeur
                    WsCtrlGestion.VCache.DoneeModalite.Filiere = e.Valeur
                    WsDerniereEtape.Rafraichir = True
                End If
        End Select
        WsCtrlGestion.SauveViewState()
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Private Sub Referentiel_RetourEventHandler(sender As Object, e As EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
        TbEntete.Visible = True
        MultiOnglets.ActiveViewIndex = 0
    End Sub
End Class