﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtrlGestionMultiInscription
    
    '''<summary>
    '''Contrôle CadreVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreVues As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Tb1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Tb1 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle BtnTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents BtnTitre As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle MultiOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiOnglets As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle Vue1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vue1 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlEtape1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlEtape1 As Global.Virtualia.Net.CtrlChoixSession
    
    '''<summary>
    '''Contrôle Vue2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vue2 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlEtape2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlEtape2 As Global.Virtualia.Net.CtrlChoixStagiaire
    
    '''<summary>
    '''Contrôle Vue3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vue3 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlEtape3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlEtape3 As Global.Virtualia.Net.CtrlModalite
    
    '''<summary>
    '''Contrôle Vue4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vue4 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CtrlEtape4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlEtape4 As Global.Virtualia.Net.CtrlConfirmation
    
    '''<summary>
    '''Contrôle PopupDetailEtape.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupDetailEtape As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelDetailEtapePopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelDetailEtapePopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CtrlDetail.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtrlDetail As Global.Virtualia.Net.PopupDetailEtape
    
    '''<summary>
    '''Contrôle HidenPopupDetailEtape.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HidenPopupDetailEtape As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle PopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupMsg As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelMsgPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelMsgPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CtlMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtlMsg As Global.Virtualia.Net.V_Message
    
    '''<summary>
    '''Contrôle HidenPopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HidenPopupMsg As Global.System.Web.UI.WebControls.HiddenField
End Class
