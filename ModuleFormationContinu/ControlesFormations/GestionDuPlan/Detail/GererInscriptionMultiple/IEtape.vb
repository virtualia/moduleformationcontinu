﻿Imports Virtualia.OutilsVisu.Formation

Public Interface IEtape
    Inherits IControlBase

    ReadOnly Property NumEtape As Integer
    WriteOnly Property Rafraichir As Boolean


    WriteOnly Property EtapePrec As IEtape
    WriteOnly Property EtapeSuiv As IEtape
    WriteOnly Property DerniereEtape As IEtape

End Interface
