﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.TablesObjet.ShemaREF

Public Class CtrlGestionMultiInscription
    Inherits GestMultiVueBase
    '** RP
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
    Public Event Dossier_Click As Dossier_ClickEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Protected Sub VDossier_Click(ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
        RaiseEvent Dossier_Click(Me, e)
    End Sub
    '***
    <Serializable>
    Public Class ModaliteInfo
        Public Property DateInscription As String

        Public Property Filiere As String
        Public Property Suivi As String
        Public Property Motif As String
        Public Property Presence As String
        Public Property Priorite As String
        Public Property Commentaire As String

        Public ReadOnly Property ToolTip As String
            Get
                Dim CRetour As String = ""
                Dim sep As String = ""

                CRetour = "Suivi de l'inscription : " & Suivi
                If (Priorite.Trim() <> "") Then
                    CRetour &= vbCrLf & vbTab & "Priorité : " & Priorite
                End If
                If (Filiere.Trim() <> "") Then
                    CRetour &= vbCrLf & vbTab & "Filière : " & Filiere
                End If
                If (Presence.Trim() <> "") Then
                    CRetour &= vbCrLf & vbTab & "Suivi de la présence : " & Presence
                End If
                If (Motif.Trim() <> "") Then
                    CRetour &= vbCrLf & vbTab & "Motif de non participation : " & Motif
                End If

                Return CRetour
            End Get
        End Property

        Public Sub New()
            Vider()
        End Sub

        Public Sub Vider()
            Dim dtejour As String = Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase.RhDate.DateduJour()

            DateInscription = dtejour

            Filiere = ""
            Suivi = ""
            Motif = ""
            Presence = ""
            Priorite = ""
            Commentaire = ""
        End Sub
    End Class

    <Serializable>
    Public Class SelectionSessionInfo
        Public Property IndexSel As Integer = -1
        Public Property ListIndexSessionAnomalie As List(Of Integer) = New List(Of Integer)()

        Public Sub Vider()
            IndexSel = -1
            ListIndexSessionAnomalie.Clear()
        End Sub
    End Class

    <Serializable>
    Public Class CacheMultiInsc
        Public Const KeyState As String = "CacheMultiInsc"

        Public Property SessionsSelectionnees As List(Of StageSessionInfo) = New List(Of StageSessionInfo)()
        Public Property AgentsSelectionnes As List(Of ElementArmoire) = New List(Of ElementArmoire)
        Public Property DoneeModalite As ModaliteInfo = New ModaliteInfo()
        Public Property SelectSession As SelectionSessionInfo = New SelectionSessionInfo()
        Public Property ModelSessionRecurente As StageSessionInfo = New StageSessionInfo()
        Public Property SessionRecurrentes As List(Of String) = New List(Of String)()

        Public Sub Vider()
            SessionsSelectionnees.Clear()
            AgentsSelectionnes.Clear()
            DoneeModalite.Vider()
            SelectSession.Vider()

            ModelSessionRecurente = New StageSessionInfo()
            SessionRecurrentes.Clear()
        End Sub

        Public Sub Sauve(vs As StateBag)
            vs.AjouteValeur(KeyState, Me)
        End Sub
    End Class

    Public ReadOnly Property VCache As CacheMultiInsc
        Get
            If Not (ViewState.KeyExiste(CacheMultiInsc.KeyState)) Then
                ViewState.AjouteValeur(CacheMultiInsc.KeyState, New CacheMultiInsc())
            End If
            Return ViewState.GetValeur(Of CacheMultiInsc)(CacheMultiInsc.KeyState)
        End Get
    End Property

    Public Property IdeStage As Integer
        Get
            If Not (ViewState.KeyExiste("IdeStage")) Then
                ViewState.AjouteValeur("IdeStage", 0)
            End If
            Return DirectCast(ViewState("IdeStage"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IdeStage", value)
        End Set
    End Property

    Public Property IntituleStage As String
        Get
            If Not (ViewState.KeyExiste("IntituleStage")) Then
                ViewState.AjouteValeur("IntituleStage", "")
            End If
            Return DirectCast(ViewState("IntituleStage"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("IntituleStage", value)
        End Set
    End Property

    Public Property ReferenceStage As String
        Get
            If Not (ViewState.KeyExiste("RefStage")) Then
                ViewState.AjouteValeur("RefStage", "")
            End If
            Return DirectCast(ViewState("RefStage"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("RefStage", value)
        End Set
    End Property

    Public Property EtapeCourante As Integer
        Get
            If Not (ViewState.KeyExiste("EtapeCourante")) Then
                ViewState.AjouteValeur("EtapeCourante", -1)
            End If
            Return DirectCast(ViewState("EtapeCourante"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("EtapeCourante", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        CtrlDetail.Popup = PopupDetailEtape
        CtlMsg.Act_RetourMessage = Sub(retmsg As CacheRetourMessage)
                                       If retmsg.Commande = TypeCommandeMessage.OUI Then
                                           'On deselectionne les autres 
                                           VCache.SelectSession.ListIndexSessionAnomalie.ForEach(Sub(idx)
                                                                                                     VCache.SessionsSelectionnees(idx).EstSelectionne = False
                                                                                                 End Sub)
                                           VCache.SessionsSelectionnees(VCache.SelectSession.IndexSel).EstSelectionne = True
                                           CtrlEtape1.GereCoche()
                                           Return
                                       End If
                                       CtrlEtape1.GereCoche()
                                   End Sub
    End Sub

    Public Overrides Sub Charge(source As Object)
        MultiOnglets.ActiveViewIndex = 0
        IntituleStage = DirectCast(source, CacheSelectionArmoire).Libelle
        IdeStage = DirectCast(source, CacheSelectionArmoire).Ide
        ReferenceStage = DirectCast(source, CacheSelectionArmoire).Reference
        If ReferenceStage <> "" Then
            BtnTitre.Text = "Procédure d'inscription - " & IntituleStage & " (" & ReferenceStage & ")"
        Else
            BtnTitre.Text = "Procédure d'inscription - " & IntituleStage
        End If
        VCache.Vider()
        ChargeCache()
        EtapeCourante = -1

        CtrlEtape1.Rafraichir = True
        CtrlEtape2.Rafraichir = True
        CtrlEtape3.Rafraichir = True
        CtrlEtape4.Rafraichir = True

        GereEtapes(0)
    End Sub

    Protected Overrides Sub InitComposants()
        KeyMultiVue = "MULTIINSC"

        CmdRetour = CommandeRetour

        DicoVue = New ItemDicoVueCollection(MultiOnglets)

        Dim itdico As ItemDicoVue

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Vue1"
        itdico.IDVue = "Vue1"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Vue2"
        itdico.IDVue = "Vue2"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Vue3"
        itdico.IDVue = "Vue3"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        itdico = New ItemDicoVue(DicoVue)
        itdico.Groupe = "Gestion"
        itdico.LibelleMenu = "Vue4"
        itdico.IDVue = "Vue4"
        itdico.TypeDeVue = TypeVue.VueAutre
        DicoVue.Add(itdico)

        CtrlEtape1.EtapePrec = Nothing
        CtrlEtape1.EtapeSuivante = CtrlEtape2
        CtrlEtape1.DerniereEtape = CtrlEtape4
        CtrlEtape1.CtrlGestion = Me

        CtrlEtape2.EtapePrecedente = CtrlEtape1
        CtrlEtape2.EtapeSuivante = CtrlEtape3
        CtrlEtape2.DerniereEtape = CtrlEtape4
        CtrlEtape2.CtrlGestion = Me

        CtrlEtape3.EtapePrecedente = CtrlEtape2
        CtrlEtape3.EtapeSuivante = CtrlEtape4
        CtrlEtape3.DerniereEtape = CtrlEtape4
        CtrlEtape3.CtrlGestion = Me

        CtrlEtape4.EtapePrecedente = CtrlEtape3
        CtrlEtape4.EtapeSuivante = Nothing
        CtrlEtape4.DerniereEtape = Nothing
        CtrlEtape4.CtrlGestion = Me

    End Sub

    Protected Overrides Sub ClickMenu(ByVal ccache As CacheClickMenu)
        MyBase.ClickMenu(ccache)
        If ccache.TypeClick = TypeClickMenu.RETOUR Then
            Exit Sub
        End If
        If EtapeCourante < 0 Then
            EtapeCourante = 0
            CtrlEtape1.Rafraichir = True
            CtrlEtape1.Charge(Nothing)
            Return
        End If
        Select Case ccache.Value
            Case "0"
                CtrlEtape1.Charge(Nothing)
                EtapeCourante = 0
            Case "1"
                CtrlEtape2.Charge(Nothing)
                EtapeCourante = 1
            Case "2"
                CtrlEtape3.Charge(Nothing)
                EtapeCourante = 2
            Case "3"
                CtrlEtape4.Charge(Nothing)
                EtapeCourante = 3
        End Select
    End Sub

    Public Sub GereEtapes(Source As Integer)
        Dim cclk As CacheClickMenu = New CacheClickMenu()
        cclk.TypeClick = TypeClickMenu.VUE
        cclk.Value = "" & Source
        ClickMenu(cclk)
    End Sub

    Private Sub ChargeCache()
        Dim MoteurInscLD As GestionInscription = DirectCast(Page, FrmPlanDeFormation).MoteurInscription
        Dim StructureInscLD As StructureFormationInscription = TryCast(MoteurInscLD.GetStructure(IdeStage), StructureFormationInscription)

        If StructureInscLD Is Nothing Then
            Exit Sub
        End If
        Dim LstInscriptions As List(Of ForInscriptionInfo) = Nothing

        If StructureInscLD.Inscriptions IsNot Nothing Then
            LstInscriptions = StructureInscLD.Inscriptions.GetFichesTypees()
        End If
        Dim LstSessionsTemp As List(Of FOR_SESSION) = FormationHelper.GetSessionDeStage(IdeStage)
        Dim LstTemp As List(Of StageSessionInfo) = New List(Of StageSessionInfo)()

        LstTemp.AddRange(StageSessionInfo.ConstruitListe(StructureInscLD.Identification, Nothing, Nothing))

        If (Not (LstSessionsTemp Is Nothing) AndAlso LstSessionsTemp.Count > 0) Then
            LstTemp.AddRange(StageSessionInfo.ConstruitListe(StructureInscLD.Identification, LstSessionsTemp, LstInscriptions))
        End If
        VCache.SessionsSelectionnees.AddRange(LstTemp)
        VCache.Sauve(ViewState)
    End Sub

    Public Sub SauveViewState()
        VCache.Sauve(ViewState)
    End Sub

    Public Function GetArboResultat() As List(Of ArboResultatInfo)
        Dim LstResultat As List(Of ArboResultatInfo) = New List(Of ArboResultatInfo)()
        Dim Resultat As ArboResultatInfo
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        For Each oss In (From ss In VCache.SessionsSelectionnees Where ss.EstSelectionne Select ss).ToList()
            For Each oag In (From ag In VCache.AgentsSelectionnes Order By ag.Libelle Select ag).ToList()

                Resultat = New ArboResultatInfo With _
                                { _
                                    .Stage = IntituleStage, _
                                    .DateDebut = oss.DateDebut, _
                                    .DateFin = oss.DateFin, _
                                    .Stagiaire = oag.Libelle, _
                                    .DetInscription = VCache.DoneeModalite.ToolTip, _
                                    .IdeAgt = oag.Valeur, _
                                    .Rangsession = oss.RangSession, _
                                    .HDeb_AM = oss.Hdeb_am, _
                                    .HFin_AM = oss.HFin_am, _
                                    .HDeb_PM = oss.Hdeb_pm, _
                                    .HFin_PM = oss.HFin_pm _
                                }
                LstResultat.Add(Resultat)
            Next
        Next

        If LstResultat.Count <= 0 Then
            For Each dsr In (From Valeur In VCache.SessionRecurrentes Order By WebFct.ViRhDates.DateTypee(Valeur) Select Valeur)
                For Each ElementSess In (From ElementAgt In VCache.AgentsSelectionnes Order By ElementAgt.Libelle Select ElementAgt).ToList()
                    Resultat = New ArboResultatInfo With _
                                { _
                                    .Stage = IntituleStage, _
                                    .DateDebut = dsr, _
                                    .DateFin = dsr, _
                                    .Stagiaire = ElementSess.Libelle, _
                                    .IdeAgt = ElementSess.Valeur, _
                                    .Rangsession = 9999, _
                                    .HDeb_AM = VCache.ModelSessionRecurente.Hdeb_am, _
                                    .HFin_AM = VCache.ModelSessionRecurente.HFin_am, _
                                    .HDeb_PM = VCache.ModelSessionRecurente.Hdeb_pm, _
                                    .HFin_PM = VCache.ModelSessionRecurente.HFin_pm _
                                }
                    LstResultat.Add(Resultat)
                Next
            Next
        End If

        Return (From it In LstResultat Order By it.Stage, it.Stagiaire Select it).ToList()
    End Function

    Protected Overrides Sub Retour()
        If MultiOnglets.ActiveViewIndex = 3 Then
            Actualiser = CtrlEtape4.SiADesChangements
        End If
        MyBase.Retour()
    End Sub

    Public Sub AfficheAnomalieSession(idxsel As Integer, listindex As List(Of Integer))
        Dim CAMessage As CacheAppelMessage = New CacheAppelMessage()

        CAMessage.TypeMessage = TypePageMessage.SAISIE
        CAMessage.Titre = "Chevauchement de selection"
        CAMessage.Boutons = New List(Of BoutonMessage)()
        CAMessage.Boutons.Add(New BoutonMessage() With {.Libelle = "Oui", .TypeCommande = TypeCommandeMessage.OUI})
        CAMessage.Boutons.Add(New BoutonMessage() With {.Libelle = "Non", .TypeCommande = TypeCommandeMessage.NON})

        CAMessage.Messages = New List(Of String)()

        Dim MsgTemp As String = "La " & StageSessionInfo.GetDescription(VCache.SessionsSelectionnees(idxsel)) & " chevauche "

        If listindex.Count > 1 Then
            MsgTemp = MsgTemp & "les sessions suivantes : "
        Else
            MsgTemp = MsgTemp & "la session suivante : "
        End If
        CAMessage.Messages.Add(MsgTemp)

        VCache.SelectSession.Vider()
        VCache.SelectSession.IndexSel = idxsel
        VCache.Sauve(ViewState)

        listindex.ForEach(Sub(idx)
                              VCache.SelectSession.ListIndexSessionAnomalie.Add(idx)
                              CAMessage.Messages.Add(vbTab & "- " & StageSessionInfo.GetDescription(VCache.SessionsSelectionnees(idx)))
                          End Sub)

        VCache.Sauve(ViewState)

        CAMessage.Messages.Add("")
        CAMessage.Messages.Add("Voulez-vous continuer ?")
        CAMessage.IDControlSaisie = "ANOSESSIONS"

        CtlMsg.Charge(CAMessage)
        PopupMsg.Show()

    End Sub

    Protected Sub BtnTitre_Click(sender As Object, e As EventArgs)
        CtrlDetail.EtapeCourante = EtapeCourante
        PopupDetailEtape.Show()
    End Sub

    Public Sub AfficherDossier(ByVal IdePER As Integer)
        If IdePER > 0 Then
            VDossier_Click(New Virtualia.Systeme.Evenements.DossierClickEventArgs(IdePER, "Procedure"))
        End If
    End Sub
End Class