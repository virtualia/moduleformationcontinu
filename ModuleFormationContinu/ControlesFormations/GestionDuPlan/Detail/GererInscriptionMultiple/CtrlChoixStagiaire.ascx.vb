﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.Net.Session

Public Class CtrlChoixStagiaire
    Inherits UserControl
    Implements IEtape
    Private WsCtrlGestion As CtrlGestionMultiInscription
    Private WsEtapeSuivante As IEtape
    Private WsDerniereEtape As IEtape
    Private WsEtapePrecedente As IEtape
    '** RP
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property
    '***
    Public ReadOnly Property NomControl As String Implements IEtape.NomControl
        Get
            Return "ETAPE_2"
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionMultiInscription
        Set(value As CtrlGestionMultiInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NumEtape As Integer Implements IEtape.NumEtape
        Get
            Return 1
        End Get
    End Property

    Public WriteOnly Property Rafraichir As Boolean Implements IEtape.Rafraichir
        Set(value As Boolean)
            ViewState.AjouteValeur("Rafraichir", value)
        End Set
    End Property

    Public WriteOnly Property EtapeSuivante As IEtape Implements IEtape.EtapeSuiv
        Set(value As IEtape)
            WsEtapeSuivante = value
        End Set
    End Property

    Public WriteOnly Property EtapePrecedente As IEtape Implements IEtape.EtapePrec
        Set(value As IEtape)
            WsEtapePrecedente = value
        End Set
    End Property

    Public WriteOnly Property DerniereEtape As IEtape Implements IEtape.DerniereEtape
        Set(value As IEtape)
            WsDerniereEtape = value
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        CtrlArmoireStagiaire.Act_SelectArmoire = Sub(c As CacheSelectionArmoire)
                                                     CtrlArmoireCible.Deselectionne()
                                                     If ((From it In WsCtrlGestion.VCache.AgentsSelectionnes Where it.Valeur = c.Ide Select it).Count() <= 0) Then
                                                         Dim lsttmp As List(Of ElementArmoire) = WsCtrlGestion.VCache.AgentsSelectionnes
                                                         Dim elem As ElementArmoire = DirectCast(c.Element, ElementArmoire)

                                                         If elem.Libelle.Contains("(") Then
                                                             Dim ssplit As String() = elem.Libelle.Split("("c)

                                                             Dim elemnew As ElementArmoire = New ElementArmoire(elem.Valeur, ssplit(0).Trim(), elem.UrlImage)
                                                             lsttmp.Add(elemnew)
                                                         Else
                                                             lsttmp.Add(DirectCast(c.Element, ElementArmoire))
                                                         End If

                                                         WsCtrlGestion.VCache.AgentsSelectionnes = (From it In lsttmp Order By it.Libelle Select it).ToList()
                                                         WsCtrlGestion.SauveViewState()

                                                         CtrlArmoireCible.Charge(WsCtrlGestion.VCache.AgentsSelectionnes)

                                                         WsDerniereEtape.Rafraichir = True
                                                     End If
                                                     CommandeSuiv.Visible = (WsCtrlGestion.VCache.AgentsSelectionnes.Count > 0)
                                                 End Sub

        BtnSupprimer.Act = Sub(btn)
                               If CtrlArmoireCible.ItemSelectionne Is Nothing Then
                                   Return
                               End If

                               Dim itasuppr As ElementArmoire = (From it In WsCtrlGestion.VCache.AgentsSelectionnes Where it.Valeur = CtrlArmoireCible.ItemSelectionne.Valeur Select it).First()
                               WsCtrlGestion.VCache.AgentsSelectionnes.Remove(itasuppr)
                               WsCtrlGestion.SauveViewState()

                               CommandeSuiv.Visible = (WsCtrlGestion.VCache.AgentsSelectionnes.Count > 0)

                               CtrlArmoireCible.Charge(WsCtrlGestion.VCache.AgentsSelectionnes)

                               WsDerniereEtape.Rafraichir = True
                           End Sub

    End Sub

    Public Sub Charge(items As Object) Implements IEtape.Charge
        ChargeCboSource()
        CboSource.SelectedIndex = 0
        ChargeArmoireSource()
        If Not (DirectCast(ViewState("Rafraichir"), Boolean)) Then
            Exit Sub
        End If
        Rafraichir = False
        CommandeSuiv.Visible = False

        CtrlArmoireCible.Charge(Nothing)

        WsCtrlGestion.VCache.AgentsSelectionnes.Clear()
        WsCtrlGestion.SauveViewState()
    End Sub

    Private Sub ChargeArmoireSource()
        CtrlArmoireStagiaire.Charge(Nothing)
        Dim datas As List(Of ElementArmoire) = GetDatas()
        CtrlArmoireStagiaire.Charge(datas)
    End Sub

    Private Function GetDatas() As List(Of ElementArmoire)
        Dim LstDonnees As List(Of ElementArmoire) = Nothing
        Dim LstAgents As List(Of IdentitePersonne) = Nothing
        Dim IndexSession As Integer = 0

        Select Case CboSource.SelectedItem.Value
            Case "NOMINATIVE"
                Dim ObjSession As LDObjetSession = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)
                If ObjSession Is Nothing Then
                    Return LstDonnees
                End If
                LstAgents = ObjSession.Lecteur_Agent.LstArmoireAgent
            Case "ACTIVITE"
                Dim ObjSession As LDObjetSession = DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)
                If ObjSession Is Nothing Then
                    Return LstDonnees
                End If
                LstAgents = ObjSession.Lecteur_Agent.LstArmoireAgentEnActivite
            Case "PREINSC"
                LstAgents = FormationHelper.GetAgentPreInscrit(WsCtrlGestion.IdeStage)
        End Select

        If CboSource.SelectedItem.Value.StartsWith("ANNUL") Then
            IndexSession = Integer.Parse(CboSource.SelectedItem.Value.Split("#"c)(1))
            LstAgents = FormationHelper.GetAgentAnnulle(WsCtrlGestion.IdeStage, GetSessions()(IndexSession))
        End If

        If CboSource.SelectedItem.Value.StartsWith("INSC") Then
            IndexSession = Integer.Parse(CboSource.SelectedItem.Value.Split("#"c)(1))
            LstAgents = FormationHelper.GetAgentInscrit(WsCtrlGestion.IdeStage, GetSessions()(IndexSession))
        End If

        '** RP
        If CboSource.SelectedItem.Value.StartsWith("DEMANDES") Then
            Dim LstDemandes As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
            Dim LstIdeAgents As List(Of Integer)
            Dim LstInscrits As List(Of IdentitePersonne) = Nothing
            Dim Inscrit As IdentitePersonne
            Dim LstSessions As List(Of StageSessionInfo)
            LstDemandes = ListeDesDemandesdeFormation(WsCtrlGestion.IntituleStage, "", True)
            If LstDemandes IsNot Nothing AndAlso LstDemandes.Count > 0 Then
                LstIdeAgents = New List(Of Integer)
                For Each Demande In LstDemandes
                    If Demande.Date_Session = "" Then
                        LstIdeAgents.Add(Demande.Ide_Dossier)
                    End If
                Next
                If LstIdeAgents.Count > 0 Then
                    LstAgents = ListeAgentsViaIntranet(LstIdeAgents)
                    LstSessions = GetSessions()
                    If LstSessions IsNot Nothing AndAlso LstSessions.Count > 0 Then
                        For Each StageSession In LstSessions
                            LstInscrits = FormationHelper.GetAgentInscrit(WsCtrlGestion.IdeStage, StageSession)
                            If LstInscrits IsNot Nothing AndAlso LstInscrits.Count > 0 Then
                                For Each Agent In LstAgents
                                    Inscrit = (From Inscription In LstInscrits Select Inscription Where Inscription.Ide = Agent.Ide).FirstOrDefault
                                    If Inscrit IsNot Nothing Then
                                        Agent.Nom = Inscrit.Nom
                                        Agent.Prenom = Inscrit.Prenom
                                    End If
                                Next
                            End If
                        Next
                    End If
                End If
            End If
        End If

        If CboSource.SelectedItem.Value.StartsWith("INTRANET") Then
            Dim DateSessionDemande As String = Strings.Right(CboSource.SelectedItem.Value, 10)
            Dim LstDemandes As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
            Dim LstIdeAgents As List(Of Integer)
            Dim LstInscrits As List(Of IdentitePersonne) = Nothing
            Dim Inscrit As IdentitePersonne
            Dim LstSessions As List(Of StageSessionInfo)
            LstDemandes = ListeDesDemandesdeFormation(WsCtrlGestion.IntituleStage, DateSessionDemande)
            If LstDemandes IsNot Nothing AndAlso LstDemandes.Count > 0 Then
                LstIdeAgents = New List(Of Integer)
                For Each Demande In LstDemandes
                    LstIdeAgents.Add(Demande.Ide_Dossier)
                Next
                LstAgents = ListeAgentsViaIntranet(LstIdeAgents)
                LstSessions = GetSessions()
                If LstSessions IsNot Nothing AndAlso LstSessions.Count > 0 Then
                    For IndexSession = 0 To LstSessions.Count - 1
                        If LstSessions.Item(IndexSession).DateDebut = DateSessionDemande Then
                            Exit For
                        End If
                    Next IndexSession
                    LstInscrits = FormationHelper.GetAgentInscrit(WsCtrlGestion.IdeStage, LstSessions.Item(IndexSession))
                    If LstInscrits IsNot Nothing AndAlso LstInscrits.Count > 0 Then
                        For Each Agent In LstAgents
                            Inscrit = (From Inscription In LstInscrits Select Inscription Where Inscription.Ide = Agent.Ide).FirstOrDefault
                            If Inscrit IsNot Nothing Then
                                Agent.Nom = Inscrit.Nom
                                Agent.Prenom = Inscrit.Prenom
                            End If
                        Next
                    End If
                End If
            End If
        End If
        '**
        If LstAgents Is Nothing OrElse LstAgents.Count = 0 Then
            Return LstDonnees
        End If

        LstDonnees = (From Agent In LstAgents Order By Agent.Nom, Agent.Prenom Select New ElementArmoire(Agent.Ide, Agent.Nom & " " & Agent.Prenom,
                                                                                   "~/Images/Armoire/GrisFonceFermer16.bmp")).ToList()
        If LstDonnees.Count <= 0 Then
            Return LstDonnees
        End If
        If LstDonnees.Count > 50 Then
            CtrlArmoireStagiaire.LettreDefaut = ValeurDefaut.A
        Else
            CtrlArmoireStagiaire.LettreDefaut = ValeurDefaut._TOUS
        End If
        Return LstDonnees
    End Function

    Private Sub ChargeCboSource()
        CboSource.Items.Clear()
        CboSource.Items.Add(New ListItem() With {.Text = "la liste nominative du personnel en activité ce jour", .Value = "ACTIVITE"})
        CboSource.Items.Add(New ListItem() With {.Text = "la liste nominative du personnel", .Value = "NOMINATIVE"})
        CboSource.Items.Add(New ListItem() With {.Text = "la liste des personnes ayant demandé via l'intranet le stage sans préciser de session", .Value = "DEMANDES"})

        Dim DateDerniereSel As DateTime = GetDateDerniereSelection()
        If DateDerniereSel = DateTime.MinValue Then
            Return
        End If
        CboSource.Items.Add(New ListItem() With {.Text = "la liste des pré-inscrits", .Value = "PREINSC"})

        Dim LstSessions As List(Of StageSessionInfo) = GetSessions()
        Dim NoDansListe As Integer = 0
        If LstSessions IsNot Nothing AndAlso LstSessions.Count > 0 Then
            LstSessions.ForEach(Sub(ss)
                                    CboSource.Items.Add(New ListItem() With {.Text = "la liste des stagiaires à la session" & If((Not (ss.IntituleSession Is Nothing) AndAlso ss.IntituleSession.Trim() <> ""), " """ & ss.IntituleSession.Trim() & """", "") & " du " & ss.DateDebut & " au " & ss.DateFin, .Value = "INSC#" & NoDansListe})
                                    NoDansListe += 1
                                End Sub)
        End If

        '** Ajout RP
        If lstsessions IsNot Nothing AndAlso lstsessions.Count > 0 Then
            Dim LstDemandes As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
            LstDemandes = ListeDesDemandesdeFormation(lstsessions.Item(0).Intitule)
            NoDansListe = 0
            If LstDemandes IsNot Nothing AndAlso LstDemandes.Count > 0 Then
                For Each Demande In LstDemandes
                    For Each SessiondeFormation In LstSessions
                        If Demande.Date_Session = SessiondeFormation.DateDebut Then
                            CboSource.Items.Add(New ListItem() With {.Text = "la liste des personnes ayant demandé via l'intranet la session du " & Demande.Date_Session, .Value = "INTRANET#" & Demande.Date_Session})
                            NoDansListe += 1
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
        '**

        Dim lstSessionAnterieures As List(Of StageSessionInfo) = GetSessionAnterieureADerniereSelection()
        NoDansListe = 0
        If lstSessionAnterieures IsNot Nothing AndAlso lstSessionAnterieures.Count > 0 Then
            For Each ss In lstSessionAnterieures
                CboSource.Items.Add(New ListItem() With {.Text = "la liste des annulations/reports (session du " & ss.DateDebut & " au " & ss.DateFin & ")", .Value = "ANNUL#" & NoDansListe})
                NoDansListe += 1
            Next
        End If

    End Sub

    '** RP
    Private Function ListeDesDemandesdeFormation(ByVal IntituleStage As String, Optional ByVal DateSession As String = "",
                                                 Optional ByVal SiSansDate As Boolean = False) As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)
        Dim LstRequete As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim LstResultat As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
        Dim InfoSel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
        Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)()
        Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim Rupture As String = ""

        ObjetStructure.SiForcerClauseDistinct = False
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.SiHistoriquedeSituation = True
        ObjetStructure.SiSelectMin_PlutotQue_Max = True
        ObjetStructure.SiPasdeTriSurIdeDossier = False
        If DateSession = "" Then
            ObjetStructure.SiPasdeTriSurIdeDossier = True
        End If

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 1)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
        InfoSel.Valeurs_AComparer = (New String() {IntituleStage}).ToList()
        LstSel.Add(InfoSel)

        If SiSansDate = False Then
            InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 2)
            If DateSession <> "" Then
                InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
                InfoSel.Valeurs_AComparer = (New String() {DateSession}).ToList()
            Else
                InfoSel.Operateur_Comparaison = VI.Operateurs.Inclu
                InfoSel.Valeurs_AComparer = (New String() {"01/01/" & (DateTime.Now.Year - 1), "31/12/" & DateTime.Now.Year + 1}).ToList()
            End If
            LstSel.Add(InfoSel)
        End If

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 14)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Egalite
        InfoSel.Valeurs_AComparer = (New String() {"Avis favorable"}).ToList()
        LstSel.Add(InfoSel)

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaDemandeFormation, 5)
        InfoSel.Operateur_Comparaison = VI.Operateurs.Difference
        InfoSel.Valeurs_AComparer = (New String() {"Refus"}).ToList()
        LstSel.Add(InfoSel)

        For Each Info In V_WebFonction.PointeurGlobal.VirListeInfosDico
            If Info.PointdeVue = VI.PointdeVue.PVueApplicatif And Info.Objet = VI.ObjetPer.ObaDemandeFormation Then
                If DateSession <> "" Then
                    LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 0))
                Else
                    Select Case Info.Information
                        Case 2
                            LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 1))
                        Case Else
                            LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaDemandeFormation, Info.Information, 0))
                    End Select
                End If
            End If
        Next

        Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(V_WebFonction.PointeurGlobal.VirModele, V_WebFonction.PointeurGlobal.VirInstanceBd)
        Constructeur.StructureRequete = ObjetStructure
        Constructeur.ListeInfosASelectionner = LstSel
        Constructeur.ListeInfosAExtraire = LstExt

        LstRequete = V_WebFonction.PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDemandeFormation,
                                                                                  Constructeur.OrdreSqlDynamique)
        If LstRequete Is Nothing Then
            Return Nothing
        End If
        LstResultat = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION In LstRequete
            If DateSession = "" And SiSansDate = False Then
                If Fiche.Date_Session <> Rupture Then
                    LstResultat.Add(Fiche)
                End If
                Rupture = Fiche.Date_Session
            Else
                If CStr(Fiche.Ide_Dossier) <> Rupture Then
                    LstResultat.Add(Fiche)
                End If
                Rupture = Fiche.Ide_Dossier.ToString
            End If
        Next
        Return V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION)(LstResultat)
    End Function
    '** RP
    Private Function ListeAgentsViaIntranet(ByVal LstIde As List(Of Integer)) As List(Of Virtualia.Metier.Formation.IdentitePersonne)
        Dim LstResultat As List(Of ServiceServeur.VirRequeteType)
        Dim LstAgents As List(Of Virtualia.Metier.Formation.IdentitePersonne) = Nothing
        Dim AgentDemandeur As Virtualia.Metier.Formation.IdentitePersonne
        Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
        Dim InfoSel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
        Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction) = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)()
        Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim Rupture As String = ""

        InfoSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaCivil, 2)
        LstSel.Add(InfoSel)
        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 0))
        LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 0))
        ObjetStructure.SiForcerClauseDistinct = False
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.SiHistoriquedeSituation = False
        ObjetStructure.SiPasdeTriSurIdeDossier = False

        Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(V_WebFonction.PointeurGlobal.VirModele, V_WebFonction.PointeurGlobal.VirInstanceBd)
        Constructeur.StructureRequete = ObjetStructure
        Constructeur.ListeInfosASelectionner = LstSel
        Constructeur.ListeInfosAExtraire = LstExt
        Constructeur.PreselectiondIdentifiants = LstIde

        LstResultat = V_WebFonction.PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(V_WebFonction.PointeurGlobal.VirNomUtilisateur,
                                                     VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDemandeFormation, Constructeur.OrdreSqlDynamique)
        If LstResultat Is Nothing Then
            Return Nothing
        End If
        LstAgents = New List(Of Virtualia.Metier.Formation.IdentitePersonne)
        For Each Resultat In LstResultat
            AgentDemandeur = New Virtualia.Metier.Formation.IdentitePersonne
            AgentDemandeur.Ide = Resultat.Ide_Dossier
            AgentDemandeur.Nom = Resultat.Valeurs(0)
            AgentDemandeur.Prenom = Resultat.Valeurs(1)
            LstAgents.Add(AgentDemandeur)
        Next
        Return LstAgents
    End Function
    '***********
    Private Function GetSessionAnterieureADerniereSelection() As List(Of StageSessionInfo)
        Dim dtedersel As DateTime = GetDateDerniereSelection()
        If dtedersel = DateTime.MinValue Then
            Return New List(Of StageSessionInfo)()
        End If
        If WsCtrlGestion.VCache.SessionsSelectionnees.Count = 1 Then
            Return New List(Of StageSessionInfo)()
        End If
        Dim LstResultat As List(Of StageSessionInfo) = (From ss In WsCtrlGestion.VCache.SessionsSelectionnees
                                                        Where V_WebFonction.ViRhDates.DateTypee(ss.DateDebut) <> DateTime.MinValue _
                                                        And DateTime.Compare(V_WebFonction.ViRhDates.DateTypee(ss.DateDebut), dtedersel) < 0 Select ss).ToList()

        Return LstResultat
    End Function

    Private Function GetSessions() As List(Of StageSessionInfo)
        Return (From ss In WsCtrlGestion.VCache.SessionsSelectionnees Where V_WebFonction.ViRhDates.DateTypee(ss.DateDebut) <> DateTime.MinValue Select ss).ToList()
    End Function

    Private Function GetDateDerniereSelection() As DateTime
        Dim ssi As StageSessionInfo = (From ss In WsCtrlGestion.VCache.SessionsSelectionnees
                                       Where ss.EstSelectionne Order By V_WebFonction.ViRhDates.DateTypee(ss.DateDebut) Descending
                                       Select ss).FirstOrDefault()

        If ssi IsNot Nothing Then
            Return V_WebFonction.ViRhDates.DateTypee(ssi.DateDebut)
        End If

        Return (From Chaine In WsCtrlGestion.VCache.SessionRecurrentes Order By V_WebFonction.ViRhDates.DateTypee(Chaine) Descending Select V_WebFonction.ViRhDates.DateTypee(Chaine)).First()

    End Function

    Protected Sub CommandeSuiv_Click(sender As Object, e As ImageClickEventArgs)
        WsCtrlGestion.GereEtapes(WsEtapeSuivante.NumEtape)
    End Sub

    Protected Sub CommandePrec_Click(sender As Object, e As ImageClickEventArgs)
        WsCtrlGestion.GereEtapes(WsEtapePrecedente.NumEtape)
    End Sub

    Protected Sub CboSource_SelectedIndexChanged(sender As Object, e As EventArgs)
        ChargeArmoireSource()
    End Sub
    '** RP
    Private Sub CtrlArmoireStagiaire_PreRender(sender As Object, e As EventArgs) Handles CtrlArmoireStagiaire.PreRender
        If CtrlArmoireStagiaire.ItemSelectionne Is Nothing Then
            EtiNomSelection.Text = ""
            CadreSelection.Visible = False
            CellDemandes.Visible = False
            CellHisto.Visible = False
            Exit Sub
        End If
        Dim LstColonnes As List(Of String)
        Dim Ide As Integer = CtrlArmoireStagiaire.ItemSelectionne.Valeur
        Dim LstInfos As List(Of String)
        Dim Ensemble As Virtualia.Metier.Formation.EnsembleDossiers
        Dim Dossier As Virtualia.Metier.Formation.DossierFormation
        Dim LstIntranet As List(Of Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION) = Nothing
        Dim LstSessions As List(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION) = Nothing

        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsemblePER
        Dossier = Ensemble.ItemDossier(Ide)
        If Dossier Is Nothing Then
            Ensemble.Identifiant = Ide
            Dossier = Ensemble.ItemDossier(Ide)
        End If
        If Dossier Is Nothing Then
            EtiNomSelection.Text = ""
            CadreSelection.Visible = False
            CellDemandes.Visible = False
            CellHisto.Visible = False
            Exit Sub
        End If
        EtiNomSelection.Text = Dossier.Nom & Strings.Space(1) & Dossier.Prenom & " (Ide V : " & Dossier.Identifiant & ")"
        CadreSelection.Visible = True

        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune situation")
        LstLibels.Add("Une situation")
        LstLibels.Add("situations")

        LstIntranet = Dossier.ListeDesDemandesFavorables("01/01/" & (DateTime.Now.Year - 2))
        If LstIntranet IsNot Nothing Then
            CellDemandes.Visible = True
            ListeDemandes.V_LibelCaption = LstLibels
            LstColonnes = New List(Of String)
            LstColonnes.Add("Stage")
            LstColonnes.Add("Date de la session")
            LstColonnes.Add("Au titre du DIF")
            LstColonnes.Add("Date de la demande")
            LstColonnes.Add("Suite")
            LstColonnes.Add("Clef")
            ListeDemandes.V_LibelColonne = LstColonnes
            LstInfos = New List(Of String)
            Ide = 0
            EtiDEmande.Text = "DEMANDES DE FORMATIONS AVEC AVIS FAVORABLE DEPUIS LE " & "01/01/" & (DateTime.Now.Year - 2)
            For Each Demande In LstIntranet
                Ide += 1
                LstInfos.Add(Demande.Objet_Demande & VI.Tild & Demande.Date_Session & VI.Tild & Demande.SiDIF & VI.Tild & Demande.Date_Demande & VI.Tild & Demande.SuiteDonnee & VI.Tild & Ide)
            Next
            For Ide = 1 To 3
                ListeDemandes.Centrage_Colonne(Ide) = 1
            Next Ide
            ListeDemandes.V_Liste = LstInfos
        End If
        LstSessions = Dossier.ListeDesFormations("01/01/" & DateTime.Now.Year - 2, "31/12/" & DateTime.Now.Year + 1)
        If LstSessions IsNot Nothing Then
            CellHisto.Visible = True
            ListeHisto.V_LibelCaption = LstLibels
            LstColonnes = New List(Of String)
            LstColonnes.Add("Stage")
            LstColonnes.Add("Date de la session")
            LstColonnes.Add("Date de fin")
            LstColonnes.Add("Suite donnée")
            LstColonnes.Add("Clef")
            ListeHisto.V_LibelColonne = LstColonnes
            Dim Libel As String
            LstInfos = New List(Of String)
            Ide = 0
            EtiHisto.Text = "FORMATIONS EFFECTUEES DEPUIS LE " & "01/01/" & DateTime.Now.Year - 2
            For Each Formation In LstSessions
                Libel = Formation.Intitule_de_la_session
                If Libel = "" Then
                    Libel = Formation.Intitule
                End If
                Ide += 1
                LstInfos.Add(Libel & VI.Tild & Formation.Date_de_Valeur & VI.Tild & Formation.Date_de_Fin & VI.Tild & Formation.Suite_donnee & VI.Tild & Ide)
            Next
            For Ide = 1 To 3
                ListeHisto.Centrage_Colonne(Ide) = 1
            Next Ide
            ListeHisto.V_Liste = LstInfos
        End If

    End Sub

    Private Sub CmdDossier_Click(sender As Object, e As ImageClickEventArgs) Handles CmdDossier.Click
        If WsCtrlGestion IsNot Nothing And IsNumeric(CtrlArmoireStagiaire.ItemSelectionne.Valeur) Then
            Call WsCtrlGestion.AfficherDossier(CInt(CtrlArmoireStagiaire.ItemSelectionne.Valeur))
        End If
    End Sub
    '**
End Class