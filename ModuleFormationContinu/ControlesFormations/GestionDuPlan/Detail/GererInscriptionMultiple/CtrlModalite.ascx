﻿<%@ Control Language="vb" AutoEventWireup="true" CodeBehind="CtrlModalite.ascx.vb" Inherits="Virtualia.Net.CtrlModalite" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>

<asp:Table ID="TbGenerale" runat="server" Visible="true" Width="1100px" Height="500px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:Table ID="TbEntete" runat="server" Width="1000px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px">
                        <asp:ImageButton ID="CommandePrec" runat="server" Width="32px" Height="16px" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ImageUrl="~/Images/Boutons/PagePrecedente.bmp" ImageAlign="Middle" ToolTip="ETAPE 2 - Sélection des stagiaires" OnClick="CommandePrec_Click" />
                    </asp:TableCell>

                    <asp:TableCell HorizontalAlign="Center" Width="800px">
                        <asp:Label ID="EtiTitre" runat="server" Text="ETAPE 3 - Modalités des inscriptions" Height="22px" Width="700px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="100px">
                        <asp:ImageButton ID="CommandeSuiv" runat="server" Width="32px" Height="16px" BorderColor="#124545" BorderStyle="Notset" BorderWidth="1px" ImageUrl="~/Images/Boutons/PageSuivante.bmp" ImageAlign="Middle" ToolTip="ETAPE 4 - Confirmation des inscriptions" Visible="true" OnClick="CommandeSuiv_Click" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0" >
                <asp:View ID="VueSaisie" runat="server">
                    <asp:Table ID="CadreDon" runat="server" Width="600px" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" Height="10px" />
                        </asp:TableRow>
                        <asp:TableRow Height="50px">
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="Label6" runat="server" Height="20px" Width="350px" Text="Caractéristiques d'inscription" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Height="2px" />
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Date Inscription--%>
                                <Virtualia:VCoupleEtiDonnee ID="TxtDateInsc" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="2" V_SiDonneeDico="true" 
                                    DonWidth="100px" EtiWidth="250px" DonTabIndex="1" V_SiAutoPostBack="true" EtiStyle="font-style: normal; text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Height="5px" />
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Suivi de l'inscription--%>
                                <Virtualia:VDuoEtiquetteCommande ID="BtnSuivi" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="17" V_SiDonneeDico="true" 
                                    DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiStyle="font-style: normal; text-align: left;" EtiText="Suivi de l'inscription" DonTooltip="Saisie obligatoire pour passer à l'étape suivante" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Motif de non participation--%>
                                <Virtualia:VDuoEtiquetteCommande ID="BtnMotif" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="18" V_SiDonneeDico="true" 
                                    DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiStyle="font-style: normal; text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" />
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Suivi de la présence au stage--%>
                                <Virtualia:VDuoEtiquetteCommande ID="BtnPresence" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="19" V_SiDonneeDico="true" 
                                    DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiStyle="font-style: normal; text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Ordre de priorité à l'inscription--%>
                                <Virtualia:VDuoEtiquetteCommande ID="BtnPriorite" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="20" V_SiDonneeDico="true" 
                                    DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiStyle="font-style: normal; text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Filière--%>
                                <Virtualia:VDuoEtiquetteCommande ID="BtnFiliere" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="34" V_SiDonneeDico="true" 
                                    DonTabIndex="7" DonWidth="400px" EtiWidth="250px" EtiStyle="font-style: normal; text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <%--Commentaire--%>
                                <Virtualia:VCoupleEtiDonnee ID="txtCommentaire" runat="server" V_PointdeVue="9" V_Objet="8" V_Information="22" V_SiDonneeDico="true" 
                                    DonWidth="394px" DonTabIndex="3" EtiText="Commentaire" EtiWidth="250px" V_SiAutoPostBack="true" EtiStyle="font-style: normal; text-align: left;" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
</asp:Table>
