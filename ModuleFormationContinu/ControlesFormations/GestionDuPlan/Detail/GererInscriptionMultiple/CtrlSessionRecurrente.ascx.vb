﻿Option Strict On
Option Explicit On
Option Compare Text

Public Class CtrlSessionRecurrente
    Inherits System.Web.UI.UserControl
    Private WsMultiOnglet As MultiView

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        BtnRetour.Act = Sub(btn)
                            WsMultiOnglet.ActiveViewIndex = 0
                        End Sub
    End Sub

End Class