﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.Net.Session
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Virtualia.Structure.Formation
Imports Virtualia.TablesObjet.ShemaREF
Imports System.Drawing

Public Class CtrlChoixSession
    Inherits UserControl
    Implements IEtape
    Private WsCtrlGestion As CtrlGestionMultiInscription
    Private WsEtapeSuivante As IEtape
    Private WsDerniereEtape As IEtape

    Public ReadOnly Property NomControl As String Implements IEtape.NomControl
        Get
            Return "ETAPE_1"
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionMultiInscription
        Set(value As CtrlGestionMultiInscription)
            WsCtrlGestion = value
            ctrlLstSession.CtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NumEtape As Integer Implements IEtape.NumEtape
        Get
            Return 0
        End Get
    End Property

    Public WriteOnly Property Rafraichir As Boolean Implements IEtape.Rafraichir
        Set(value As Boolean)
            ViewState.AjouteValeur("Rafraichir", value)
        End Set
    End Property

    Public WriteOnly Property EtapeSuivante As IEtape Implements IEtape.EtapeSuiv
        Set(value As IEtape)
            WsEtapeSuivante = value
        End Set
    End Property

    Public WriteOnly Property EtapePrec As IEtape Implements IEtape.EtapePrec
        Set(value As IEtape)

        End Set
    End Property

    Public WriteOnly Property DerniereEtape As IEtape Implements IEtape.DerniereEtape
        Set(value As IEtape)
            WsDerniereEtape = value
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        ctrlLstSession.CtrlChoix = Me
    End Sub

    Public Sub Charge(items As Object) Implements IEtape.Charge

        If Not (DirectCast(ViewState("Rafraichir"), Boolean)) Then
            Return
        End If
        Rafraichir = False

        ctrlLstSession.Charge()
    End Sub

    Public Sub GereBoutonEtape()
        WsDerniereEtape.Rafraichir = True
    End Sub

    Public Sub GereCoche()
        Dim query = From it In WsCtrlGestion.VCache.SessionsSelectionnees _
                    Where WsCtrlGestion.VCache.SessionsSelectionnees.IndexOf(it) > 0 _
                    Select Index = WsCtrlGestion.VCache.SessionsSelectionnees.IndexOf(it), Coche = it.EstSelectionne
        query.ToList().ForEach(Sub(idyn)
                                   ctrlLstSession.SelectOuPasChk(idyn.Index, idyn.Coche)
                               End Sub)
        GereBoutonEtape()
    End Sub

    Protected Sub CommandeSuiv_Click(sender As Object, e As ImageClickEventArgs)
        If (WsCtrlGestion.VCache.SessionRecurrentes.Count <= 0 AndAlso (From it In WsCtrlGestion.VCache.SessionsSelectionnees Where it.EstSelectionne Select it).Count() <= 0) Then
            WsCtrlGestion.VCache.SessionsSelectionnees(0).EstSelectionne = True
            WsCtrlGestion.SauveViewState()
        End If
        WsCtrlGestion.GereEtapes(WsEtapeSuivante.NumEtape)
    End Sub

End Class