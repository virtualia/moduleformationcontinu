﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.OutilsVisu.Formation
Imports AjaxControlToolkit

Public Class PopupDetailEtape
    Inherits UserControl

    Private WsPopup As ModalPopupExtender

    Public WriteOnly Property Popup As ModalPopupExtender
        Set(ByVal value As ModalPopupExtender)
            WsPopup = value
        End Set
    End Property

    Public Property EtapeCourante As Integer
        Private Get
            If Not (ViewState.KeyExiste("EtapeCourante")) Then
                ViewState.AjouteValeur("EtapeCourante", -1)
            End If

            Return DirectCast(ViewState("EtapeCourante"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("EtapeCourante", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        btnOK.Act = Sub(btn)
                        WsPopup.Hide()
                    End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)

        Select Case EtapeCourante
            Case 0
                TbMsg.Height = New Unit("220px")

                LblTitre.Text = "ETAPE 1 - Sélection des sessions"

                txtMsg.Text = "A cette étape, " & vbCrLf & vbCrLf & _
                              "Vous sélectionnez les sessions sur lesquelles vous voulez inscrire des stagiaires." & vbCrLf & vbCrLf & _
                              vbTab & "Pour selectionner des sessions, vous devez cliquer sur l'une des sessions affichées dans la liste" & vbCrLf & _
                              vbTab & vbTab & "Si elle n'est pas sélectionnée, elle le sera" & vbCrLf & _
                              vbTab & vbTab & "Si elle l'est, elle ne le sera plus." & vbCrLf & vbCrLf & _
                              "Le bouton ""étape suivante"" s'affichera dés lors qu'au moins une session a été cliquée et sélectionnée" & vbCrLf & _
                              vbTab & "Vous pourrez alors passer à ""ETAPE 2 - Sélection des stagiaires"""
            Case 1
                TbMsg.Height = New Unit("300px")

                LblTitre.Text = "ETAPE 2 - Sélection des stagiaires"

                txtMsg.Text = "A cette étape, " & vbCrLf & vbCrLf & _
                              "Vous sélectionnez les stagiaires que vous voulez inscrire." & vbCrLf & vbCrLf & _
                              vbTab & "Pour selectionner un stagiaire, vous devez cliquer sur l'un des noms proposés dans la liste de gauche" & vbCrLf & _
                              vbTab & vbTab & "Le stagiaire passera dans la liste ""Stagiaires sélectionnés""" & vbCrLf & vbCrLf & _
                              vbTab & "Pour supprimer un stagiaire de la sélection" & vbCrLf & _
                              vbTab & vbTab & "Vous devez cliquer sur son nom dans la liste ""Stagiaires sélectionnés""" & vbCrLf & _
                              vbTab & vbTab & "Cliquer sur le bouton ""Supprimer""" & vbCrLf & vbCrLf & _
                              "Le bouton ""étape suivante"" s'affichera dés lors qu'au moins un stagiaire est selectionné" & vbCrLf & _
                              vbTab & "Vous pourrez alors passer à ""ETAPE 3 - Modalités des inscriptions"""
            Case 2
                TbMsg.Height = New Unit("170px")

                LblTitre.Text = "ETAPE 3 - Modalités des inscriptions"

                txtMsg.Text = "A cette étape, " & vbCrLf & vbCrLf & _
                              "Vous indiquez les modalités d'inscription." & vbCrLf & vbCrLf & _
                              vbTab & "Seule l'information ""Suivi de l'inscription"" permet d'afficher le bouton ""étape suivante""" & vbCrLf & _
                              vbTab & vbTab & "Les autres informations étant facultatives" & vbCrLf & vbCrLf & _
                              vbTab & "Vous pourrez alors passer à ""ETAPE 4 - Confirmation des inscriptions"""
            Case 3
                TbMsg.Height = New Unit("450px")

                LblTitre.Text = "ETAPE 4 - Confirmation des inscriptions"

                txtMsg.Text = "A cette étape, " & vbCrLf & vbCrLf & _
                              "Vous confirmez les inscriptions." & vbCrLf & vbCrLf & _
                              "Cliquer sur le bouton ""Confirmer"" lance le traitement" & vbCrLf & vbCrLf & _
                              vbTab & "Une fois le traitement lancé, le bouton ""étape précédente"" disparait" & vbCrLf & _
                              vbTab & "Une premiere action consiste à rechercher les inscriptions des stagiaire pour chaque session sélectionnée" & vbCrLf & _
                              vbTab & vbTab & """Chargée"" signifie que la recherche est terminée pour le stagiaire" & vbCrLf & _
                              vbTab & "S'il existe des inscriptions, une deuxième action recherche d'eventuels chevauchements de dates et d'horaires." & vbCrLf & _
                              vbTab & vbTab & """OK"" signifie que le stagiaire peut être inscrit sur la session" & vbCrLf & _
                              vbTab & vbTab & """Chevauchement"" signifie que le stagiaire est inscrit sur une autre session" & vbCrLf & _
                              vbTab & vbTab & """Chevauchement d'horaire"" signifie que le stagiaire est inscrit sur une session aux mêmes dates sur des horaires se chevauchant" & vbCrLf & _
                              vbTab & "Sinon l'action d'inscription se lance" & vbCrLf & _
                              vbTab & vbTab & """OK"" signifie que le stagiaire a bien été inscrit sur la session" & vbCrLf & _
                              vbTab & vbTab & """Erreur"" signifie que l'inscription n'a pas été faite, cependant le traitement continu" & vbCrLf & vbCrLf & _
                              "La procédure d'inscriptions est terminée"

        End Select
    End Sub

End Class