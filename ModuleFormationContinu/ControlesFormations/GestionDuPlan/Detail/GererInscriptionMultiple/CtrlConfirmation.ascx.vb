﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Metier.Formation
Imports Virtualia.Structure.Formation

Public Class CtrlConfirmation
    Inherits UserControl
    Implements IEtape
    Private WsCtrlGestion As CtrlGestionMultiInscription
    Private WsEtapePrecedente As IEtape
    Private WsDerniereEtape As IEtape
    Private Const C_LIB_MEMESTAGE As String = "Déjà inscrit à ce stage"
    Private Const C_LIB_NBMAX As String = "Déjà inscrit à {0} sessions"
    Private Const C_NB_CHEVAUCH As Integer = 2
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***

    <Serializable>
    Public Class IndexATraiterCollection
        Inherits List(Of Integer)

        Public Const KeyState As String = "IndexATraiterCollection"

        Public Sub Vider(vs As StateBag)
            Me.Clear()
            Sauve(vs)
        End Sub

        Public Sub Sauve(vs As StateBag)
            vs.AjouteValeur(KeyState, Me)
        End Sub
    End Class

    Public ReadOnly Property NomControl As String Implements IEtape.NomControl
        Get
            Return "ETAPE_4"
        End Get
    End Property

    Public Property SiTouteInscription As Boolean
        Set(value As Boolean)
            ViewState.AjouteValeur("TouteInscription", value)
        End Set
        Private Get
            If Not (ViewState.KeyExiste("TouteInscription")) Then
                ViewState.AjouteValeur("TouteInscription", True)
            End If
            Return DirectCast(ViewState("TouteInscription"), Boolean)
        End Get
    End Property

    Public WriteOnly Property CtrlGestion As CtrlGestionMultiInscription
        Set(value As CtrlGestionMultiInscription)
            WsCtrlGestion = value
        End Set
    End Property

    Public ReadOnly Property NumEtape As Integer Implements IEtape.NumEtape
        Get
            Return 3
        End Get
    End Property

    Public WriteOnly Property Rafraichir As Boolean Implements IEtape.Rafraichir
        Set(value As Boolean)
            ViewState.AjouteValeur("Rafraichir", value)
        End Set
    End Property

    Public Property SiADesChangements As Boolean
        Get
            If Not (ViewState.KeyExiste("ADesChangements")) Then
                ViewState.AjouteValeur("ADesChangements", False)
            End If
            Return DirectCast(ViewState("ADesChangements"), Boolean)
        End Get
        Private Set(value As Boolean)
            ViewState.AjouteValeur("ADesChangements", value)
        End Set
    End Property

    Private Property SiLancerCalculArbre As Boolean
        Get
            If Not (ViewState.KeyExiste("LanceCalculArbre")) Then
                ViewState.AjouteValeur("LanceCalculArbre", False)
            End If
            Return DirectCast(ViewState("LanceCalculArbre"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("LanceCalculArbre", value)
        End Set
    End Property

    Public WriteOnly Property EtapeSuivante As IEtape Implements IEtape.EtapeSuiv
        Set(value As IEtape)
        End Set
    End Property

    Public WriteOnly Property EtapePrecedente As IEtape Implements IEtape.EtapePrec
        Set(value As IEtape)
            WsEtapePrecedente = value
        End Set
    End Property

    Public WriteOnly Property DerniereEtape As IEtape Implements IEtape.DerniereEtape
        Set(value As IEtape)
            WsDerniereEtape = value
        End Set
    End Property

    Private ReadOnly Property IndexATraiter As IndexATraiterCollection
        Get
            If (Not ViewState.KeyExiste(IndexATraiterCollection.KeyState)) Then
                ViewState.AjouteValeur(IndexATraiterCollection.KeyState, New IndexATraiterCollection())
            End If
            Return DirectCast(ViewState(IndexATraiterCollection.KeyState), IndexATraiterCollection)
        End Get
    End Property

    Private Property NumeroTraitement As Integer
        Get
            If Me.ViewState.KeyExiste("NoTraitement") = False Then
                Me.ViewState.AjouteValeur("NoTraitement", 0)
            End If
            Return DirectCast(Me.ViewState("NoTraitement"), Integer)
        End Get
        Set(value As Integer)
            Me.ViewState.AjouteValeur("NoTraitement", value)
        End Set
    End Property

    Private Property Index As Integer
        Get
            If (Not ViewState.KeyExiste("Index")) Then
                ViewState.AjouteValeur("Index", -1)
            End If
            Return DirectCast(ViewState("Index"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("Index", value)
        End Set
    End Property

    Private Property IndexCourant As Integer
        Get
            If (Not ViewState.KeyExiste("IndexCourant")) Then
                ViewState.AjouteValeur("IndexCourant", -1)
            End If
            Return DirectCast(ViewState("IndexCourant"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("IndexCourant", value)
        End Set
    End Property

    Private Property RangInscription As Integer
        Get
            If (Not ViewState.KeyExiste("RangInscription")) Then
                ViewState.AjouteValeur("RangInscription", -1)
            End If
            Return DirectCast(ViewState("RangInscription"), Integer)
        End Get
        Set(value As Integer)
            ViewState.AjouteValeur("RangInscription", value)
        End Set
    End Property

    Private ReadOnly Property InscriptionAVerifier As VerifInscriptionInfoCollection
        Get
            If Not (ViewState.KeyExiste(VerifInscriptionInfoCollection.KeyState)) Then
                ViewState.AjouteValeur(VerifInscriptionInfoCollection.KeyState, New VerifInscriptionInfoCollection())
            End If
            Return ViewState.GetValeur(Of VerifInscriptionInfoCollection)(VerifInscriptionInfoCollection.KeyState)
        End Get
    End Property

    Private Property DonneesArbre As List(Of ArboResultatInfo)
        Get
            If Not (ViewState.KeyExiste("DonneesArbre")) Then
                ViewState.AjouteValeur("DonneesArbre", New List(Of ArboResultatInfo)())
            End If
            Return ViewState.GetValeur(Of List(Of ArboResultatInfo))("DonneesArbre")
        End Get
        Set(value As List(Of ArboResultatInfo))
            ViewState.AjouteValeur("DonneesArbre", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        InitCtrlArbre()
        InitMethodeArbre()
        CtrlArbreResultat.EndInit()

        CtrlConfirm.Popup = PopupMsg
        CtrlConfirm.Act_Retour = Sub(action)
                                     SiTouteInscription = (action = "TOUTE")
                                     Call LancerTraitement()
                                 End Sub

        BtnTrait.Act = Sub(btn)
                           Dim LstSessionStage As List(Of StageSessionInfo) = WsCtrlGestion.VCache.SessionsSelectionnees.Where(Function(it)
                                                                                                                                   Return it.EstSelectionne
                                                                                                                               End Function).ToList()

                           If LstSessionStage.Count = 1 AndAlso LstSessionStage(0).DateDebut = "" Then
                               SiTouteInscription = False
                               Index = 0
                               IndexCourant = -1
                               Call LancerTraitement()
                               Return
                           End If

                           Dim NbAnos As Integer = (From it In DonneesArbre Where it.Anomalie <> "" AndAlso Not it.Anomalie.StartsWith(C_LIB_MEMESTAGE) Select it).Count()
                           If NbAnos > 0 Then
                               CtrlConfirm.NbAnomalie = NbAnos
                               CtrlConfirm.NbValide = (From it In DonneesArbre Where it.Anomalie = "" Select it).Count()
                               PopupMsg.Show()
                               Return
                           End If

                           Index = 0
                           IndexCourant = -1

                           Call LancerTraitement()
                       End Sub
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)
        If HorlogeTraitement.Enabled = True Then
            Select Case NumeroTraitement
                Case 1 'Chargement
                    Call ExecuterChargement()
                Case 2 'Vérification
                    Call ExecuterVerif()
                Case 3 'Inscription
                    PopupMsg.Hide()
                    CommandePrec.Visible = False
                    Call ExecuterInscription()
            End Select
        End If
    End Sub

    Public Sub Charge(items As Object) Implements IEtape.Charge
        SiLancerCalculArbre = False
        SiADesChangements = False
        If Not (DirectCast(ViewState("Rafraichir"), Boolean)) Then
            Return
        End If

        Rafraichir = False
        SiTouteInscription = True

        BtnTrait.Enabled = False
        TbMessage.Visible = True
        TxtStatut.Text = ""
        txtMsg.Text = ""
        CommandePrec.Visible = True

        Index = 0
        IndexCourant = -1
        IndexATraiter.Vider(ViewState)
        InscriptionAVerifier.Vider(ViewState)

        TxtNbTot.Text = ""
        TxtNbTrait.Text = ""
        TxtNbAnomalie.Text = ""

        DonneesArbre = WsCtrlGestion.GetArboResultat()

        TxtNbTot.Text = "Total : " & DonneesArbre.Count
        CtrlArbreResultat.Charge()
        HorlogeTraitement.Enabled = True
        NumeroTraitement = 1
    End Sub

    Private Sub InitCtrlArbre()
        If IsPostBack = True Then
            Exit Sub
        End If
        CtrlArbreResultat.Titre = "Récapitulatif"

        Dim LstItem As ListItemArmoireCollection = New ListItemArmoireCollection()

        Dim CRetour As ItemArmoire = New ItemArmoire()
        CRetour.Key = "Resultat"
        CRetour.Libelle = "Résultat"
        CRetour.AfficheFitre = False

        Dim ListProps As List(Of String) = New List(Of String)()
        ListProps.Add("Stage")
        ListProps.Add("Session")
        ListProps.Add("Stagiaire")
        ListProps.Add("Anomalie")
        CRetour.Proprietes = ListProps

        LstItem.Add(CRetour)

        CtrlArbreResultat.ArmoireItems = LstItem

        Dim lststyletmp As ListStyleTreeNodeCollection = New ListStyleTreeNodeCollection()
        lststyletmp.Add(New StyleTreeNode("Stage", ConstanteFormation.IconeStage) With {.Expanded = True})
        lststyletmp.Add(New StyleTreeNode("Session", ConstanteFormation.IconeSession) With {.Expanded = True})
        lststyletmp.Add(New StyleTreeNode("Stagiaire", "~/Images/Armoire/GrisFonceFermer16.bmp") With {.Expanded = True, .PropToolTip = "DetInscription"})
        lststyletmp.Add(New StyleTreeNode("Anomalie", ConstanteFormation.IconeAnomalie) With {.Expanded = False, .AfficherFeuilleVide = False})
        CtrlArbreResultat.StyleNoeuds = lststyletmp
    End Sub

    Private Sub InitMethodeArbre()
        CtrlArbreResultat.Func_CalculNbTotal = Function(Donnees, key, predicat)
                                                   If SiLancerCalculArbre = False Then
                                                       Return ""
                                                   End If
                                                   Dim DonneesArbre As List(Of ArboResultatInfo) = DirectCast(Donnees, List(Of ArboResultatInfo))
                                                   Dim nbtot As Integer = (From it In DonneesArbre Select "" & it.IdeAgt & "|" & it.Stage & "|" & it.Session).Distinct().Count
                                                   Dim nbano As Integer = (From it In DonneesArbre Where it.Anomalie <> "" Select 1).Sum()

                                                   Return If( _
                                                                    nbano = 0, _
                                                                    "aucune anomalie sur " & nbtot & If(nbtot = 1, " inscription", " inscriptions"), _
                                                                    ( _
                                                                        If( _
                                                                            nbano = 1, _
                                                                            "une anomalie sur " & nbtot & If(nbtot = 1, " inscription", " inscriptions"), _
                                                                            "" & nbano & " anomalies sur " & nbtot & If(nbtot = 1, " inscription", " inscriptions") _
                                                                            ) _
                                                                    ) _
                                                                )
                                               End Function
        CtrlArbreResultat.Func_DataSource = Function()
                                                Return DonneesArbre
                                            End Function

        CtrlArbreResultat.Act_NoeudChange = Nothing
        CtrlArbreResultat.Act_ItemArmoireChange = Nothing
    End Sub

    Private Sub ExecuterChargement()
        If IndexCourant = Index Then
            Exit Sub
        End If
        Dim Separateur As String = vbCrLf
        If Index > 0 Then
            Separateur = vbCrLf
        End If
        If Index = 0 Then
            txtMsg.Text = "Virtualia effectue des contrôles ..." & vbCrLf & vbCrLf
        End If
        IndexCourant = Index
        TxtStatut.Text = "Contrôles " & (Index + 1) & "/" & DonneesArbre.Count

        Dim CRetour As RetAsync = TraiteChargementAsync(Index).Result
        txtMsg.Text = txtMsg.Text & Separateur & CRetour.Retour
        Index = CRetour.Index

        If Index >= DonneesArbre.Count Then
            HorlogeTraitement.Enabled = False
            NumeroTraitement = 0
            Index = 0
            IndexCourant = -1

            If InscriptionAVerifier IsNot Nothing AndAlso InscriptionAVerifier.Count > 0 Then
                HorlogeTraitement.Enabled = True
                NumeroTraitement = 2
                Exit Sub
            End If
            'Il n'y a pas d'inscription a verifier, on peut inscrire tout le monde
            Dim IndiceI As Integer = 0
            IndexATraiter.Vider(ViewState)
            DonneesArbre.ForEach(Sub(el)
                                     IndexATraiter.Add(IndiceI)
                                     IndiceI += 1
                                 End Sub)
            IndexATraiter.Sauve(ViewState)
            BtnTrait.Enabled = True
        End If
    End Sub

    Private Async Function TraiteChargementAsync(indexdonnee As Integer) As System.Threading.Tasks.Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(IdxDonnee)
                                                               Return TraiteChargement(indexdonnee)
                                                           End Function

        Return Await System.Threading.Tasks.Task.FromResult(Of RetAsync)(FuncTraitement(indexdonnee))
    End Function

    Private Function TraiteChargement(ByVal IdxDonnee As Integer) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()
        CRetour.Index = IdxDonnee + 1

        Dim ItemAVerifier As ArboResultatInfo = DonneesArbre(IdxDonnee)
        If ItemAVerifier.DateDebut = "" Then
            Dim VerifPre As VerifInscriptionInfo = FormationHelper.GetPreInscriptionDeStagiaire(ItemAVerifier.IdeAgt, WsCtrlGestion.IdeStage)
            If VerifPre IsNot Nothing Then
                InscriptionAVerifier.Add(VerifPre)
            End If
            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            Return CRetour
        End If

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim DateDebut As String = "01/01/" & WebFct.ViRhDates.DateTypee(ItemAVerifier.DateFin).Year
        'On recherche les FOR_INSCRIPTIONS
        Dim LstVerifAgent As List(Of VerifInscriptionInfo) = FormationHelper.GetInscriptionDeStagiaire(ItemAVerifier.IdeAgt, DateDebut, ItemAVerifier.DateFin)

        LstVerifAgent.ForEach(Sub(vf)
                                  InscriptionAVerifier.Add(vf)
                              End Sub)
        CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
        Return CRetour
    End Function

    Private Sub ExecuterVerif()
        If IndexCourant = Index Then
            Exit Sub
        End If
        Dim Separateur As String = ""
        If Index > 0 Then
            Separateur = vbCrLf
        End If
        If Index = 0 Then
            IndexATraiter.Vider(ViewState)
            txtMsg.Text = "Virtualia effectue des contrôles ..." & vbCrLf & vbCrLf
        End If
        IndexCourant = Index
        TxtStatut.Text = "Contrôles " & (Index + 1) & "/" & DonneesArbre.Count

        Dim CRetour As RetAsync = TraiteVeriftAsync(Index).Result

        TxtNbTrait.Text = "A traiter : " & IndexATraiter.Count
        TxtNbAnomalie.Text = "En anomalie : " & (From it In DonneesArbre Where it.Anomalie <> "" AndAlso Not it.Anomalie.StartsWith(C_LIB_MEMESTAGE) Select it).Count()

        Index = CRetour.Index

        If Index >= DonneesArbre.Count Then
            HorlogeTraitement.Enabled = False
            NumeroTraitement = 0
            Index = 0
            IndexCourant = -1
            txtMsg.Text = ""
            SiLancerCalculArbre = True

            CtrlArbreResultat.Charge()

            BtnTrait.Enabled = True  '(IndexATraiter.Count > 0)
        End If
    End Sub

    Private Async Function TraiteVeriftAsync(indexdonnee As Integer) As System.Threading.Tasks.Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(IdxDonnee)
                                                               Return TraiteVerif(indexdonnee)
                                                           End Function

        Return Await System.Threading.Tasks.Task.FromResult(Of RetAsync)(FuncTraitement(indexdonnee))
    End Function

    Private Function TraiteVerif(ByVal IdxDonnee As Integer) As RetAsync
        Dim CRetour As RetAsync = New RetAsync()
        CRetour.Index = IdxDonnee + 1

        'On recherche dans la liste des inscriptions pour l'agent si l'une des autres inscriptions liées ne chevaucheraient pas
        Dim ItemAVerifier As ArboResultatInfo = DonneesArbre(IdxDonnee)

        If ItemAVerifier.DateDebut.Trim = "" Then
            If (From it In InscriptionAVerifier Where it.IdeAgt = ItemAVerifier.IdeAgt And it.DateDeb.Trim = "" Select it).Count() > 0 Then
                CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
                DonneesArbre(IdxDonnee).Anomalie = "Déjà pré-inscrit"
            Else
                CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
                IndexATraiter.Add(IdxDonnee)
                IndexATraiter.Sauve(ViewState)
            End If
            Return CRetour
        End If

        'On verifie si l'agent n'est pas absent ce jour là
        Dim VerifAgent As String = FormationHelper.VerifieAbsenceAgent(ItemAVerifier.IdeAgt, ItemAVerifier.DateDebut, ItemAVerifier.DateFin, ItemAVerifier.HDeb, ItemAVerifier.HFin)
        If VerifAgent <> "OK" Then
            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            DonneesArbre(IdxDonnee).Anomalie = VerifAgent
            Return CRetour
        End If
        '** Ajout RP on vérifie si il n'est pas déjà inscrit à une autre session le même jour
        Dim Ensemble As Virtualia.Metier.Formation.EnsembleDossiers
        Ensemble = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsemblePER
        VerifAgent = Ensemble.Controler_Inscription_Stagiaire(ItemAVerifier.IdeAgt, ItemAVerifier.DateDebut, ItemAVerifier.DateFin, ItemAVerifier.HDeb, ItemAVerifier.HFin)
        If VerifAgent <> "OK" Then
            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            DonneesArbre(IdxDonnee).Anomalie = VerifAgent 'Absent - déjà inscrit à la session
            Return CRetour
        End If
        '**
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim autresinscs As List(Of VerifInscriptionInfo) = (From it In InscriptionAVerifier _
                                                            Where it.IdeAgt = ItemAVerifier.IdeAgt _
                                                                  And ( _
                                                                          it.DateDeb.Trim() <> "" _
                                                                          AndAlso WebFct.ViRhDates.SiChevauchementHoraire(it.DateDeb, it.DateFin, ItemAVerifier.DateDebut, ItemAVerifier.DateFin) _
                                                                      ) _
                                                            Select it).ToList()

        If autresinscs.Count <= 0 Then
            IndexATraiter.Add(IdxDonnee)
            IndexATraiter.Sauve(ViewState)
            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            Return CRetour
        End If

        Dim testsel As String = ItemAVerifier.GetTestHoraire()
        Dim testautre As String = ""
        Dim chevauchement As String = ""
        Dim libelle As String = ""

        For Each it In autresinscs.Where(Function(ai)
                                             Return ai.IdeStage = WsCtrlGestion.IdeStage
                                         End Function)

            libelle = C_LIB_MEMESTAGE & " " & it.DateDeb & " au " & it.DateFin & " "

            testautre = it.GetTestHoraire()

            Select Case testautre
                Case "0000"
                    libelle = libelle & " journée complète"
                Case "1100"
                    libelle = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_AM
                Case "0011"
                    libelle = libelle & " de " & it.HeureDeb_PM & " à " & it.HeureFin_PM
                Case "1111"
                    libelle = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                Case "1001"
                    libelle = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
            End Select

            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            DonneesArbre(IdxDonnee).Anomalie = libelle
            Return CRetour
        Next

        Dim qrautres = autresinscs.Where(Function(ai)
                                             Return ai.IdeStage <> WsCtrlGestion.IdeStage
                                         End Function)

        If qrautres.Count = C_NB_CHEVAUCH Then
            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            DonneesArbre(IdxDonnee).Anomalie = String.Format(C_LIB_NBMAX, "" & C_NB_CHEVAUCH)
            Return CRetour
        End If

        For Each it In qrautres
            libelle = "Chevauchements avec l'inscription " & If(it.libstage = "", " du ", " au stage """ & it.libstage & """ du ") & it.DateDeb & " au " & it.DateFin & " "

            testautre = it.GetTestHoraire()

            If (testautre = "0000") OrElse (testsel = "0000") Then
                IndexATraiter.Add(IdxDonnee)
                IndexATraiter.Sauve(ViewState)
                chevauchement = libelle & "journée complète"
                Exit For
            End If

            Select Case testsel
                Case "1100"
                    Select Case testautre
                        Case "1100"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_AM
                                Exit For
                            End If
                        Case "0011"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_PM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1111"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1001"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                    End Select
                Case "0011"
                    Select Case testautre
                        Case "1100"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_AM
                                Exit For
                            End If
                        Case "0011"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_PM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1111"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1001"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_PM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                    End Select
                Case "1111"
                    Select Case testautre
                        Case "1100"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_AM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_AM
                                Exit For
                            End If
                        Case "0011"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_PM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_PM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1111"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1001"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_AM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_PM, ItemAVerifier.HDeb_PM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                    End Select
                Case "1001"
                    Select Case testautre
                        Case "1100"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_AM
                                Exit For
                            End If
                        Case "0011"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_PM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1111"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_AM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_PM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                        Case "1001"
                            If WebFct.ViRhDates.SiChevauchementHoraire(it.HeureDeb_AM, it.HeureFin_PM, ItemAVerifier.HDeb_AM, ItemAVerifier.HFin_PM) = True Then
                                chevauchement = libelle & " de " & it.HeureDeb_AM & " à " & it.HeureFin_PM
                                Exit For
                            End If
                    End Select
            End Select
        Next

        If chevauchement <> "" Then
            'Il y a des chevauchement d'horaire
            CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session
            DonneesArbre(IdxDonnee).Anomalie = chevauchement
            Return CRetour
        End If

        'il y a bien chevauchement mais pas des horaires, l'inscription est possible
        IndexATraiter.Add(IdxDonnee)
        IndexATraiter.Sauve(ViewState)
        CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session

        Return CRetour

    End Function

    Private Function EstLaMemeInscription(itemaverif As ArboResultatInfo, it As VerifInscriptionInfo) As Boolean
        Return WsCtrlGestion.IdeStage = it.IdeStage AndAlso itemaverif.DateDebut = it.DateDeb AndAlso itemaverif.DateFin = it.DateFin AndAlso itemaverif.HDeb_AM = it.HeureDeb_AM AndAlso itemaverif.HDeb_PM = it.HeureDeb_PM AndAlso itemaverif.HFin_AM = it.HeureFin_AM AndAlso itemaverif.HFin_PM = it.HeureFin_PM
    End Function

    Private Sub ExecuterInscription()
        If IndexCourant = Index Then
            DirectCast(Page.Master, VirtualiaMain).SessionCourante.ReinitListeArbo()
            Exit Sub
        End If
        Dim Separateur As String = ""
        If Index > 0 Then
            Separateur = vbCrLf
        Else
            TxtNbAnomalie.Text = ""
            TxtNbTrait.Text = ""
        End If
        If Index = 0 Then
            RangInscription = -1
            DirectCast(Page, FrmPlanDeFormation).MoteurInscription.ReinitStructureSeulement()
            txtMsg.Text = "Virtualia procède aux inscriptions ..." & vbCrLf & vbCrLf
        End If
        IndexCourant = Index
        If SiTouteInscription = True Then
            TxtStatut.Text = "Inscription " & (Index + 1) & "/" & DonneesArbre.Count
        Else
            TxtStatut.Text = "Inscription " & (Index + 1) & "/" & IndexATraiter.Count
        End If
        Dim CRetour As RetAsync
        CRetour = TraiteInscriptiontAsync(Index).Result
        If CRetour.Retour = "" Then
            Index = CRetour.Index
            Exit Sub
        End If
        txtMsg.Text = txtMsg.Text & Separateur & CRetour.Retour

        Dim SSplit As String()
        If CRetour.Retour.Contains(" Erreur") Then
            If TxtNbTrait.Text = "" Then
                TxtNbTrait.Text = "Inscrit : 0"
            End If
            If TxtNbAnomalie.Text = "" Then
                TxtNbAnomalie.Text = "Erreur : 1"
            Else
                SSplit = TxtNbAnomalie.Text.Split(":"c)
                TxtNbAnomalie.Text = "Erreurs : " & Integer.Parse(SSplit(1)) + 1
            End If
        Else
            If TxtNbAnomalie.Text = "" Then
                TxtNbAnomalie.Text = "Erreur : 0"
            End If
            If TxtNbTrait.Text = "" Then
                TxtNbTrait.Text = "Inscrit : 1"
            Else
                SSplit = TxtNbTrait.Text.Split(":"c)
                TxtNbTrait.Text = "Inscrits : " & Integer.Parse(SSplit(1)) + 1
            End If
        End If
        Index = CRetour.Index
        Dim SiArreter As Boolean = False

        If SiTouteInscription = True Then
            SiArreter = (Index >= DonneesArbre.Count)
        Else
            SiArreter = (Index >= IndexATraiter.Count)
        End If
        If SiArreter = True Then
            TxtStatut.Text = "Traitement terminé"

            txtMsg.Text = txtMsg.Text & vbCrLf & vbCrLf & "Virtualia a fini les inscriptions" & vbCrLf
            txtMsg.Text = txtMsg.Text & vbCrLf & vbTab & "-" & TxtNbTrait.Text & vbCrLf
            txtMsg.Text = txtMsg.Text & vbCrLf & vbTab & "-" & TxtNbAnomalie.Text & vbCrLf

            HorlogeTraitement.Enabled = False
            NumeroTraitement = 0
            Index = 0
            IndexCourant = -1
            IndexATraiter.Vider(ViewState)

            CtrlArbreResultat.Charge()
        End If
    End Sub

    Private Async Function TraiteInscriptiontAsync(indexdonnee As Integer) As System.Threading.Tasks.Task(Of RetAsync)
        Dim FuncTraitement As Func(Of Integer, RetAsync) = Function(IdxDonnee)
                                                               Return TraiteInscription(indexdonnee)
                                                           End Function
        Return Await System.Threading.Tasks.Task.FromResult(Of RetAsync)(FuncTraitement(indexdonnee))
    End Function

    Private Function TraiteInscription(ByVal IdxDonnee As Integer) As RetAsync
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        Dim CRetour As RetAsync = New RetAsync() With {.Index = IdxDonnee + 1, .Retour = ""}
        Dim ItemAVerifier As ArboResultatInfo
        If SiTouteInscription = True Then
            ItemAVerifier = DonneesArbre(Index)
        Else
            ItemAVerifier = DonneesArbre(IndexATraiter(IdxDonnee))
        End If
        If ItemAVerifier.Anomalie.StartsWith(C_LIB_MEMESTAGE) = True Then
            Return CRetour
        End If
        Dim MoteurLD As GestionInscription = DirectCast(Page, FrmPlanDeFormation).MoteurInscription
        Dim StructfLD As StructureFormationInscription = Nothing
        MoteurLD.ReinitStructureSeulement()
        StructfLD = TryCast(MoteurLD.GetStructure(WsCtrlGestion.IdeStage), StructureFormationInscription)

        If StructfLD Is Nothing Then
            CRetour.Index = Integer.MaxValue
            CRetour.Retour = "Le stage n'existe pas ou a été supprimé par un autre utilisateur"
            Return CRetour
        End If
        If RangInscription <= 0 Then
            If StructfLD.Inscriptions Is Nothing OrElse StructfLD.Inscriptions.Count <= 0 Then
                RangInscription = 1
            Else
                RangInscription = (From fi In StructfLD.Inscriptions.GetFichesTypees() Select fi.Rang_inscription).Max() + 1
            End If
        End If

        Dim ForSession As Virtualia.TablesObjet.ShemaREF.FOR_SESSION = Nothing
        If ItemAVerifier.DateDebut.Trim() <> "" Then
            If ItemAVerifier.Rangsession <> 9999 Then
                ForSession = FormationHelper.GetSessionDeInscription(
                                                                    StructfLD.Identification.Ide_Dossier,
                                                                    WebFct.ViRhDates.DateTypee(ItemAVerifier.DateDebut),
                                                                    WebFct.ViRhDates.DateTypee(ItemAVerifier.DateFin),
                                                                    ItemAVerifier.Rangsession
                                                                 )
            Else
                'On est sur des sessions recurrentes
                If WsCtrlGestion.VCache.ModelSessionRecurente.RangSession <> Integer.MinValue Then
                    ForSession = FormationHelper.GetSessionDeInscription(
                                                                    StructfLD.Identification.Ide_Dossier,
                                                                    WebFct.ViRhDates.DateTypee(WsCtrlGestion.VCache.ModelSessionRecurente.DateDebut),
                                                                    WebFct.ViRhDates.DateTypee(WsCtrlGestion.VCache.ModelSessionRecurente.DateFin),
                                                                    WsCtrlGestion.VCache.ModelSessionRecurente.RangSession
                                                                 )
                End If
            End If
        End If

        Dim Agent As AgentForInscriptionInfo = FormationHelper.GetAgentForInscription(ItemAVerifier.IdeAgt)
        If Agent Is Nothing Then
            CRetour.Retour = ItemAVerifier.Stagiaire & " n'existe pas ou a été supprimé"
            Return CRetour
        End If
        Dim InscNouvelle As ForInscriptionInfo = New ForInscriptionInfo()
        InscNouvelle.IdeStage = WsCtrlGestion.IdeStage
        InscNouvelle.LibStage = WsCtrlGestion.IntituleStage

        InscNouvelle.Rang_inscription = RangInscription + Index

        InscNouvelle.Nom_stagiaire = Agent.Nom
        InscNouvelle.Prenom_stagiaire = Agent.Prenom
        InscNouvelle.Datenaissance_stagiaire = Agent.DateNaiss
        InscNouvelle.Etablissement_stagiaire = Agent.Etablissement
        If Agent.Appartenance = True Then
            InscNouvelle.Appartenanceaupersonnel = "Oui"
        Else
            InscNouvelle.Appartenanceaupersonnel = "Non"
        End If
        InscNouvelle.Direction_stagiaire = Agent.Direction
        InscNouvelle.Service_stagiaire = Agent.Service
        InscNouvelle.DIF = "Non"
        InscNouvelle.DIF_horstravail = "Non"

        Dim ForCaracteristique As Virtualia.TablesObjet.ShemaREF.FOR_CARACTERISTIC = FormationHelper.GetCaracteristiqueDeStage(StructfLD.Identification.Ide_Dossier)

        If ForCaracteristique IsNot Nothing Then
            InscNouvelle.Action_formation = ForCaracteristique.Actionformation
            InscNouvelle.Contexte_formation = ForCaracteristique.Contexte
        End If

        InscNouvelle.Organisme_formation = StructfLD.Identification.Organisme
        InscNouvelle.Cursus_formation = StructfLD.Identification.Cursus
        InscNouvelle.Module_formation = StructfLD.Identification.ModuledeFormation
        InscNouvelle.Rubrique_formation = StructfLD.Identification.Rubrique

        InscNouvelle.Nbheures_formation = StructfLD.Identification.DureeHoraire
        InscNouvelle.Rang_Session = ItemAVerifier.Rangsession

        If ForSession IsNot Nothing Then
            InscNouvelle.Datedebut_formation = ItemAVerifier.DateDebut
            InscNouvelle.Datefin_formation = ItemAVerifier.DateFin
            InscNouvelle.Reference_session = ForSession.ReferenceSession
            InscNouvelle.Nbheures_formation = WebFct.ViRhFonction.ConversionDouble("" & ForSession.DureeSession_Heures, 2)

            If Not (ForSession.CursusSession Is Nothing OrElse ForSession.CursusSession.Trim() = "") Then
                InscNouvelle.Cursus_formation = ForSession.CursusSession
            End If
            If Not (ForSession.ModuleSession Is Nothing OrElse ForSession.ModuleSession.Trim() = "") Then
                InscNouvelle.Module_formation = ForSession.ModuleSession
            End If
            If Not (ForSession.OrganismePrestataire Is Nothing OrElse ForSession.OrganismePrestataire.Trim() = "") Then
                InscNouvelle.Organisme_formation = ForSession.OrganismePrestataire
            End If
        Else
            If ItemAVerifier.Rangsession = 9999 Then
                InscNouvelle.Datedebut_formation = ItemAVerifier.DateDebut
                InscNouvelle.Datefin_formation = ItemAVerifier.DateFin
            End If
        End If

        InscNouvelle.Date_inscription = WsCtrlGestion.VCache.DoneeModalite.DateInscription
        InscNouvelle.Filiere_formation = WsCtrlGestion.VCache.DoneeModalite.Filiere
        InscNouvelle.Suivi_inscription = WsCtrlGestion.VCache.DoneeModalite.Suivi
        InscNouvelle.Suivipresence = WsCtrlGestion.VCache.DoneeModalite.Presence
        InscNouvelle.Commentaire = WsCtrlGestion.VCache.DoneeModalite.Commentaire
        InscNouvelle.Motif_nonparticipation = WsCtrlGestion.VCache.DoneeModalite.Motif
        InscNouvelle.Ordrepriorite_inscription = WsCtrlGestion.VCache.DoneeModalite.Priorite

        Dim CRetourserv As RetourCRUD
        Dim PreInscr As Virtualia.TablesObjet.ShemaREF.FOR_INSCRIPTIONS = FormationHelper.GetPreInscriptionDeStagiaire(WsCtrlGestion.IdeStage,
                                                                InscNouvelle.Nom_stagiaire, InscNouvelle.Prenom_stagiaire, InscNouvelle.Datenaissance_stagiaire)

        If PreInscr IsNot Nothing Then
            CRetourserv = MoteurLD.Supprime(PreInscr)
            If CRetourserv.Index <= 0 Then
                CRetour.Retour = CRetour.Retour & "Erreur sur la suppression de la préinscription"
                DonneesArbre(IndexATraiter(IdxDonnee)).Anomalie = "Erreur sur la suppression de la préinscription"

                Return CRetour
            End If
            If PreInscr.Datedebut_formation <> "" Then
                '***** RP Decembre2014 DIF ***********************************
                Dim Ensemble As EnsembleDossiers = DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal).EnsemblePER
                Dim Dossier As DossierFormation
                Ensemble.Identifiant = Agent.Ide
                Dossier = Ensemble.ItemDossier(Agent.Ide)
                If Dossier IsNot Nothing Then
                    Call Dossier.VerifierEtMajDIF(PreInscr.Datedebut_formation)
                End If
                '********************************************************
            End If
        End If
        CRetourserv = MoteurLD.Ajoute(WsCtrlGestion.IdeStage, InscNouvelle)

        CRetour.Retour = ItemAVerifier.Stagiaire & vbCrLf & vbTab & ItemAVerifier.Session & vbTab
        If CRetourserv.Index < 0 Then
            CRetour.Retour = CRetour.Retour & "Erreur sur le serveur"
            DonneesArbre(IndexATraiter(IdxDonnee)).Anomalie = "Erreur sur le serveur lors de l'inscription"
        Else
            CRetour.Retour = CRetour.Retour & "Inscrit"
            SiADesChangements = True
        End If
        Return CRetour
    End Function

    Private Sub LancerTraitement()
        BtnTrait.Enabled = False
        TbMessage.Visible = True
        HorlogeTraitement.Enabled = True
        NumeroTraitement = 3
        TxtStatut.Text = "Inscription"
    End Sub

    Protected Sub CommandePrec_Click(sender As Object, e As ImageClickEventArgs)
        WsCtrlGestion.GereEtapes(WsEtapePrecedente.NumEtape)
    End Sub

End Class