﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlSessionRecurrente.ascx.vb" Inherits="Virtualia.Net.CtrlSessionRecurrente" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonNouveau.ascx" TagName="BTN_NOUVEAU" TagPrefix="Generic" %>
<%@ Register Src="~/ControleGeneric/BoutonSupprimer.ascx" TagName="BTN_SUPPR" TagPrefix="Generic" %>

<asp:Table ID="Tb2" runat="server" Width="550px" BackColor="#A8BBB8">
    <asp:TableRow Height="50px">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="Label6" runat="server" Height="20px" Width="350px" Text="Sessions recurrentes" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Label ID="lblTitre" runat="server" Height="22px" Width="500px" Text="XXXXXXXX" BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="NotSet" BorderWidth="1px" ForeColor="White" Style="margin-top: 2px; margin-left: 0px; margin-bottom: 0px; font-style: oblique; text-indent: 5px; text-align: center;" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="2px" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Panel ID="PanelTree" runat="server" ScrollBars="Auto" Height="200px" Width="500px" Style="margin-top: 0px; vertical-align: top; text-align: left" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="Snow">
                <asp:CheckBoxList ID="ListeSession" runat="server" AutoPostBack="true" Width="100%" BackColor="Transparent" Style="height: auto" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table1" runat="server" Width="500px">
                <asp:TableRow>
                    <asp:TableCell Width="300px" HorizontalAlign="Left">
                        <Generic:BTN_GENE ID="BtnRetour" runat="server" Text="Retour"  />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                        <Generic:BTN_SUPPR ID="btnSuppr" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Middle">
                        <Generic:BTN_NOUVEAU ID="btnNouveau" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
