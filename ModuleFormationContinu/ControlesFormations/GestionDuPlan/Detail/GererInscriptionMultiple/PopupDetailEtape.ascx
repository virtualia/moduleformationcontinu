﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PopupDetailEtape.ascx.vb" Inherits="Virtualia.Net.PopupDetailEtape" %>

<%@ Register Src="~/ControleGeneric/BoutonGeneral.ascx" TagName="BTN_GENE" TagPrefix="Generic" %>

<asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
        <asp:Table ID="tb" runat="server" Width="680px" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">

            <asp:TableRow>
                <asp:TableCell Height="5px" />
            </asp:TableRow>

            <asp:TableRow Height="40px">
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Table ID="Table1" runat="server" Width="200px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:Label ID="lbl" runat="server" Height="18px" Width="500px" Text="Détail de l'assistant" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:Label ID="LblTitre" runat="server" Height="18px" Width="500px" Text="XXXXXX" ForeColor="White" BorderStyle="None" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="5px" />
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Table ID="TbMsg" runat="server" Width="600px" Height="150px" BorderStyle="Solid" BackColor="#6C9690" BorderColor="#B0E0D7" BorderWidth="1px">
                        <asp:TableRow Height="25px">
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                                <asp:TextBox ID="txtMsg" runat="server" Height="100%" Width="100%" TextMode="MultiLine" BorderStyle="None" ForeColor="White" BackColor="#6C9690" Font-Italic="true" ReadOnly="true" Font-Bold="True" Style="text-align: left;  " />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Table ID="Table3" runat="server" Width="600px" BorderStyle="Solid" BackColor="Transparent" BorderColor="Transparent" BorderWidth="1px">
                        <asp:TableRow>
                            <asp:TableCell Width="600px" HorizontalAlign="Right" VerticalAlign="Middle">
                                <Generic:BTN_GENE ID="btnOK" runat="server" Text="OK"  />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell Height="2px" />
            </asp:TableRow>
        </asp:Table>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnOK" />
    </Triggers>
</asp:UpdatePanel>