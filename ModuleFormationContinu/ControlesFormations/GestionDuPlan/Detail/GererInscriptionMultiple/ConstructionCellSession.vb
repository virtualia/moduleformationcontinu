﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.Metier.Formation
Imports System.Drawing
Imports System.IO

Public Class ConstructionCellSession
    Public Shared Function ConstruitCellChk(ss As StageSessionInfo, idx As Integer, LstChk As List(Of CheckBox)) As TableCell

        Dim cellchk As TableCell = New TableCell() With {.Width = New Unit("65px"), .HorizontalAlign = HorizontalAlign.Center}
        Dim chkcell As CheckBox = New CheckBox() With {.Width = New Unit("65px"), .ID = "CHK_" & idx, .Checked = ss.EstSelectionne, .AutoPostBack = True}

        cellchk.Controls.Add(chkcell)

        LstChk.Add(chkcell)

        Return cellchk
    End Function

    Public Shared Function ConstruitCellBouton(ss As StageSessionInfo, idx As Integer, lstBtn As List(Of Button), CouleurBouton As Color) As TableCell

        Dim CellTbBtn As TableCell = New TableCell() With {.HorizontalAlign = HorizontalAlign.Center, .VerticalAlign = VerticalAlign.Middle, .Height = New Unit("20px")}

        Dim TbButton As Table = New Table() With {.ID = "TBBtn_" & idx, .Width = New Unit("60px"), .CellPadding = 0, .CellSpacing = 0}
        TbButton.Rows.Add(New TableRow())

        Dim cellbtn As TableCell = New TableCell() With {.VerticalAlign = VerticalAlign.Middle, .HorizontalAlign = HorizontalAlign.Center}
        Dim btn As Button = New Button() With {.ID = "Btn_" & idx, .Width = New Unit("30px"), .Height = New Unit("20px"), .BackColor = CouleurBouton, .BorderStyle = BorderStyle.NotSet}

        If (ss.Dates.StartsWith("Session")) Then
            btn.ToolTip = "Cliquer pour générer des sessions récurrentes sur ce modèle"
            btn.Style.Add(HtmlTextWriterStyle.Cursor, "pointer")
            lstBtn.Add(btn)
        Else
            btn.Enabled = False
        End If

        cellbtn.Controls.Add(btn)

        TbButton.Rows(0).Cells.Add(cellbtn)

        CellTbBtn.Controls.Add(TbButton)

        Return CellTbBtn
    End Function

    Public Shared Function ConstruitCellLabel(ss As StageSessionInfo, idx As Integer, libelle As String, WCell As String, wLabel As String, Optional alignement As HorizontalAlign = HorizontalAlign.Left) As TableCell

        Dim cell As TableCell = New TableCell() With {.HorizontalAlign = HorizontalAlign.Center, .VerticalAlign = VerticalAlign.Middle, .Height = New Unit("20px"), .Width = New Unit(WCell)}

        Dim lbl As Label = New Label() With {.ID = "Lbl_" & "_" & idx, .Text = libelle, .Width = New Unit(wLabel)}

        Dim valstyle As String = "left"
        Select Case alignement
            Case HorizontalAlign.Center
                valstyle = "center"
            Case HorizontalAlign.Left
                valstyle = "left"
            Case HorizontalAlign.Right
                valstyle = "right"
        End Select

        lbl.Style.Add(HtmlTextWriterStyle.TextAlign, valstyle)

        cell.Controls.Add(lbl)

        Return cell

    End Function
End Class
