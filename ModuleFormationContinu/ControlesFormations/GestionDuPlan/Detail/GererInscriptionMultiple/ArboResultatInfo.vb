﻿Imports Virtualia.Structure.Formation

<Serializable>
Public Class ArboResultatInfo
    Implements IDateBorne

    Public Property IdeAgt As Integer = 0
    Public Property Stage As String = ""

    Public Property Stagiaire As String = ""
    Public Property DetInscription As String = ""
    Public Property DateDebut As String = "" Implements IDateBorne.DateDebut
    Public Property DateFin As String = "" Implements IDateBorne.DateFin

    Public Property HDeb_AM As String = ""
    Public Property HFin_AM As String = ""
    Public Property HDeb_PM As String = ""
    Public Property HFin_PM As String = ""

    Public Property Anomalie As String = ""
    Public Property Rangsession As Integer = -1

    Public ReadOnly Property Session As String
        Get
            Dim RhDates As Virtualia.Systeme.Fonctions.CalculDates = New Virtualia.Systeme.Fonctions.CalculDates
            If DateDebut.Trim() = "" Then
                Return "Pré-inscription"
            End If
            Dim CRetour As String = ""
            If Rangsession = 9999 Then
                CRetour = RhDates.JourEnClair(RhDates.DateTypee(DateDebut).DayOfWeek) & Strings.Space(1) & DateDebut
            Else
                CRetour = "Session du " & DateDebut & " au " & DateFin
            End If
            If HDeb.Trim() <> "" Then
                CRetour &= " de " & HDeb & " à " & HFin
            End If
            Return CRetour
        End Get
    End Property

    Public ReadOnly Property HDeb As String
        Get
            If HDeb_AM.Trim() <> "" Then
                Return HDeb_AM
            End If
            Return HDeb_PM
        End Get
    End Property

    Public ReadOnly Property HFin As String
        Get
            If HFin_PM.Trim() <> "" Then
                Return HFin_PM
            End If
            Return HFin_AM
        End Get
    End Property

    Public Function GetTestHoraire() As String
        Dim test As String = ""

        If HDeb_AM.Trim() <> "" Then
            test = test & "1"
        Else
            test = test & "0"
        End If
        If HFin_AM.Trim() <> "" Then
            test = test & "1"
        Else
            test = test & "0"
        End If
        If HDeb_PM.Trim() <> "" Then
            test = test & "1"
        Else
            test = test & "0"
        End If
        If HFin_PM.Trim() <> "" Then
            test = test & "1"
        Else
            test = test & "0"
        End If

        Return test
    End Function
End Class
