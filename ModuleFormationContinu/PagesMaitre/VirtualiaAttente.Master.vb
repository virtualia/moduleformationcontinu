﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation

Public Class VirtualiaAttente
    Inherits VirMasterPage

    Private Sub HorlogeVirtuelle_Tick(sender As Object, e As EventArgs) Handles HorlogeVirtuelle.Tick
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        EtiDateJour.Text = ObjetGlobalBase.RhDate.ClairDate(ObjetGlobalBase.RhDate.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
        EtiTitreModule.Text = ""
    End Sub

End Class