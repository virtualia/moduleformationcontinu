﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation

Public Class VirtualiaMain
    Inherits VirMasterPage
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Public WriteOnly Property V_SiCadreCommandeVisible As Boolean
        Set(value As Boolean)
            CadreCommandes.Visible = value
        End Set
    End Property

    Public WriteOnly Property V_Info1 As String
        Set(value As String)
            EtiInfoN1.Text = value
        End Set
    End Property

    Public WriteOnly Property V_Info2 As String
        Set(value As String)
            If value = "(Tous)" Then
                EtiInfoN2.Text = ""
            Else
                EtiInfoN2.Text = value
            End If
        End Set
    End Property

    Public WriteOnly Property V_Info3 As String
        Set(value As String)
            If value = "(Tous)" Then
                EtiInfoN3.Text = ""
            Else
                EtiInfoN3.Text = value
            End If
        End Set
    End Property

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        EtiDateJour.Text = Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase.RhDate.ClairDate(Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase.RhDate.DateduJour, True)
        EtiTitre.Text = "Module de gestion du plan de formation"
        EtiUtilisateur.Text = WebFct.PointeurGlobal.NomPrenomUtilisateur(Session.SessionID)
        CmdAccueil.PostBackUrl = WebFct.PointeurGlobal.UrlDestination(Session.SessionID, 0, "") 'Retour à l'accueil Virtualia.Net

        Dim Chaine As String = "~/Fenetres/Commun/FrmAttente.aspx"
        CmdPlan.PostBackUrl = Chaine & "?Index=7&Lien=~/Fenetres/Module/FrmPlanDeFormation.aspx"
        CmdArmoire.PostBackUrl = Chaine & "?Index=7&Lien=~/Fenetres/Module/FrmPersonnel.aspx"
        CmdTableau.PostBackUrl = Chaine & "?Index=7&Lien=~/Fenetres/Module/FrmTableauxBord.aspx"
        CmdAdministration.PostBackUrl = Chaine & "?Index=7&Lien=~/Fenetres/Module/FrmAdminFormation.aspx"

        Dim LDSessionModule As Virtualia.Net.Session.LDObjetSession
        Try
            LDSessionModule = TryCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)
            If LDSessionModule.SiAccesAuModuleSeul = True Then
                CmdAdministration.Visible = False
            Else
                CmdAdministration.Visible = True
            End If
        Catch ex As Exception
            CmdAdministration.Visible = False
        End Try
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

    Private Sub HorlogeVirtuelle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles HorlogeVirtuelle.Tick
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        End If
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

    Public Overrides WriteOnly Property Titre As String
        Set(value As String)
            EtiTitre.Text = value
        End Set
    End Property

    Public ReadOnly Property SessionCourante As Virtualia.Net.Session.LDObjetSession
        Get
            Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
            Return TryCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)
        End Get
    End Property

End Class