TOTAUX	Nb actions de formation	Nb b�n�ficiaires	Nb stagiaires	Effectif au 31/12/2013

DUREES	Nb heures de formation	Nb jours de formation		
		
	Dur�e moyenne action de formation	Dur�e moyenne par personne form�e	Dur�e moyenne par stagiaire	Dur�e moyenne par personne � l'effectif
en heures	122 h 26 mn	30 h 01 mn	8 h 53 mn	20 h 34 mn
en jours	20,7	5,1	1,5	3,5
COUTS	Total co�ts	Co�ts p�dagogiques	Co�ts frais de mission	Co�ts masse salariale
	
	Co�t moyen par action de formation	Co�t moyen par personne form�e	Co�t moyen par stagiaire	Co�t moyen par personne � l'effectif
	93,33	22,89	6,77	15,68
EFFORT DE FORMATION	Pourcentage de l'effectif form�	Pourcentage de l'effectif non form�		
	68,52 %	31,48 %		
