﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBase.Formation
Imports Virtualia.OutilsVisu.Formation

Public Class V_Tuiles
    Inherits ObjetConteneurTuilesBase

    Protected Overrides ReadOnly Property V_CadreTuiles As Table
        Get
            Return Me.CadreTuiles
        End Get
    End Property
    Protected Overrides ReadOnly Property V_PanelTuiles As Panel
        Get
            Return Me.PanelTuiles
        End Get
    End Property
    Protected Overrides ReadOnly Property V_RowTuiles As TableRow
        Get
            Return Me.RowTuiles
        End Get
    End Property

    Protected Overrides Function Construit_VCoupleIconeEtiBase(cacheTuile As CacheTuile) As ObjetTuileIconeBase

        Dim TuileIcone As V_TuileIcone = DirectCast(LoadControl("~/ControlesGeneriques/V_TuileIcone.ascx"), V_TuileIcone)
        Return TuileIcone
    End Function
End Class