﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation

Public Class V_Armoire
    Inherits UserControl
    Implements IControlArmoire

    Private WsControllerArmoire As IControllerArmoire
    Private WsAct_NbSelectionChange As Action(Of Integer)

    Public WriteOnly Property ControllerArmoire As IControllerArmoire Implements IControlArmoire.ControllerArmoire
        Set(value As IControllerArmoire)
            WsControllerArmoire = value
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Public WriteOnly Property BackColor As String
        Set(value As String)
            CadreArmoire.BackColor = VisuHelper.ConvertiCouleur(value)
        End Set
    End Property

    Public Overloads WriteOnly Property Width As Unit
        Set(value As Unit)
            EtiInfo.Width = value
            Table1.Width = value
            CadreFiltre.Width = value
            PanelTree.Width = value
            CadreStatut.Width = value
        End Set
    End Property

    Public Overloads WriteOnly Property Height As Unit
        Set(value As Unit)
            PanelTree.Height = value
        End Set
    End Property

    Public Property SiSelection As Boolean
        Private Get
            Return LstArmoire.ShowCheckBoxes = TreeNodeTypes.All
        End Get
        Set(value As Boolean)

            If (value) Then
                LstArmoire.ShowCheckBoxes = TreeNodeTypes.All
                CelluleSelection.Visible = True
                Return
            End If

            LstArmoire.ShowCheckBoxes = TreeNodeTypes.None
            CelluleSelection.Visible = False
        End Set
    End Property

    Public Property SiFiltre As Boolean
        Private Get
            Return CelluleFiltre.Visible
        End Get
        Set(value As Boolean)
            CelluleFiltre.Visible = value
            CelluleLettre.Visible = value
        End Set
    End Property

    Public ReadOnly Property Selection As List(Of IElementArmoire)
        Get
            Dim LstResultat As List(Of IElementArmoire) = New List(Of IElementArmoire)()
            If LstArmoire.Nodes.Count <= 0 Then
                Return LstResultat
            End If

            Dim predicat As Func(Of TreeNode, Boolean) = Function(tn)
                                                             Return True
                                                         End Function

            If SiSelection Then
                predicat = Function(tn)
                               Return tn.Checked
                           End Function
            End If

            Dim noeudssel As List(Of Integer) = (From tn As TreeNode In LstArmoire.Nodes Where predicat(tn) Select Integer.Parse(tn.Value)).ToList()

            LstResultat = (From it In Donnees Join i In noeudssel On it.Valeur Equals i Select it).ToList()

            Return LstResultat
        End Get
    End Property

    Public Property CocheParDefaut As Boolean
        Private Get

            If Not (ViewState.KeyExiste("COCHEDEFAUT")) Then
                Return False
            End If

            Return DirectCast(ViewState("COCHEDEFAUT"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("COCHEDEFAUT", value)

            Dim s As String = ""
            If (value) Then
                s = "tout"
            Else
                s = "rien"
            End If

            For Each it As ListItem In cboselection.Items
                it.Selected = (it.Value.ToLower() = s)
            Next
        End Set
    End Property

    Private Property Donnees As ElementArmoireCollection
        Get
            If Not (ViewState.KeyExiste(ElementArmoireCollection.KeyState)) Then
                ViewState.AjouteValeur(ElementArmoireCollection.KeyState, New ElementArmoireCollection())
            End If

            Return ViewState.GetValeur(Of ElementArmoireCollection)(ElementArmoireCollection.KeyState)

        End Get
        Set(ByVal value As ElementArmoireCollection)
            ViewState.AjouteValeur(ElementArmoireCollection.KeyState, value)
        End Set
    End Property

    Private Property DonneesFiltrees As ElementArmoireCollection
        Get
            If Not (ViewState.KeyExiste("DonneesFiltrees")) Then
                ViewState.AjouteValeur("DonneesFiltrees", New ElementArmoireCollection())
            End If

            Return ViewState.GetValeur(Of ElementArmoireCollection)("DonneesFiltrees")

        End Get
        Set(ByVal value As ElementArmoireCollection)
            ViewState.AjouteValeur("DonneesFiltrees", value)
        End Set
    End Property

    Private Property Rafraichir As Boolean
        Get
            If Not (ViewState.KeyExiste("Rafraichir")) Then
                Return False
            End If

            Return DirectCast(ViewState("Rafraichir"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            If Not (ViewState.KeyExiste("Rafraichir")) Then
                ViewState.AjouteValeur("Rafraichir", value)
                Return
            End If
            ViewState.AjouteValeur("Rafraichir", value)
        End Set
    End Property

    Public WriteOnly Property Act_NbSelectionChange As Action(Of Integer)
        Set(value As Action(Of Integer))
            WsAct_NbSelectionChange = value
        End Set
    End Property

    Public ReadOnly Property NbDatas As Integer
        Get
            Return Donnees.Count
        End Get
    End Property

    Public Property LettreDefaut As ValeurDefaut
        Set(value As ValeurDefaut)
            ViewState.AjouteValeur("LettreDefaut", value)
        End Set
        Private Get
            If Not (ViewState.KeyExiste("LettreDefaut")) Then
                ViewState.AjouteValeur("LettreDefaut", ValeurDefaut._TOUS)
            End If

            Return DirectCast(ViewState("LettreDefaut"), ValeurDefaut)
        End Get
    End Property

    Public WriteOnly Property Titre As String
        Set(ByVal value As String)
            EtiInfo.Text = value
        End Set
    End Property

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)

        If Not (Rafraichir) Then
            Return
        End If

        Rafraichir = False

        LstArmoire.Nodes.Clear()

        Dim tr As TreeNode
        Dim cpt As Integer = 0

        DonneesFiltrees.ForEach(Sub(it)
                                    cpt = cpt + 1
                                    tr = it.ToTreeNode()
                                    LstArmoire.Nodes.Add(tr)

                                    If ((LstArmoire.ShowCheckBoxes = TreeNodeTypes.All) And CocheParDefaut) Then
                                        tr.Checked = True
                                    End If

                                End Sub)

        GereSelectionChange(cpt)

        lblNbStatut.Text = "" & cpt & " / " & Donnees.Count
    End Sub

    Protected Sub LstArmoire_SelectedChanged(sender As Object, e As EventArgs)
        If (WsControllerArmoire Is Nothing OrElse WsControllerArmoire.Act_SelectArmoire Is Nothing) Then
            Return
        End If

        Dim tn As TreeNode = DirectCast(sender, TreeView).SelectedNode
        Dim act As Action(Of CacheSelectionArmoire) = WsControllerArmoire.Act_SelectArmoire
        Dim objSelection As CacheSelectionArmoire = New CacheSelectionArmoire()

        If Not (tn Is Nothing) Then
            objSelection.Ide = Integer.Parse(tn.Value)
            objSelection.Libelle = tn.Text
            objSelection.Element = (From it In Donnees Where it.Valeur = Integer.Parse(tn.Value) Select it).First()
            act(objSelection)
            Return
        End If

        objSelection.Ide = 0
        objSelection.Libelle = ""
        objSelection.Element = Nothing
        act(objSelection)

    End Sub

    Protected Sub ButtonLettre_Click(sender As Object, e As ImageClickEventArgs)

        If (Donnees.Count <= 0) Then
            Return
        End If

        Dim Button As ImageButton = DirectCast(sender, ImageButton)
        Dim idbutton As String = Button.ID
        Dim lettre As String = GetLettreButton(idbutton)

        If (HButtonLettre.Value = idbutton) Then
            Return
        End If

        txtRecherche.Text = ""

        If (HButtonLettre.Value.Trim() <> "") Then
            If (HButtonLettre.Value <> "TXT") Then
                SelOuNon_BoutonLettre(HButtonLettre.Value, GetLettreButton(HButtonLettre.Value))
            Else
                SelOuNon_BoutonLettre("ButtonArobase", GetLettreButton("ButtonArobase"))
            End If
        End If

        SelOuNon_BoutonLettre(idbutton, lettre & "_sel")
        HButtonLettre.Value = idbutton

        Dim i As Integer = 0

        ChargeItems(Function(it As IElementArmoire)
                        If (lettre = "Arobase") Then
                            Return True
                        End If

                        If (lettre = "0") Then
                            Return Integer.TryParse(it.Libelle.Trim().Substring(0, 1), i)
                        End If

                        Return it.Libelle.ToLower().StartsWith(lettre.ToLower())
                    End Function)
    End Sub

    Protected Sub txtRecherche_TextChanged(sender As Object, e As EventArgs)

        If (Donnees.Count <= 0) Then
            Return
        End If

        If (HButtonLettre.Value <> "TXT") Then
            SelOuNon_BoutonLettre(HButtonLettre.Value, GetLettreButton(HButtonLettre.Value))
            SelOuNon_BoutonLettre("ButtonArobase", GetLettreButton("ButtonArobase") & "_sel")
            HButtonLettre.Value = "TXT"
        End If

        If (txtRecherche.Text = "") Then
            HButtonLettre.Value = "ButtonArobase"
            ChargeItems(Nothing)
            Return
        End If

        ChargeItems(Function(it As IElementArmoire)
                        Return it.Libelle.ToLower().Contains(txtRecherche.Text.ToLower())
                    End Function)
    End Sub

    Protected Sub cboselection_SelectedIndexChanged(sender As Object, e As EventArgs)

        'If (LstArmoire.Nodes.Count <= 0) Then
        '    GereSelectionChange(0)
        '    Return
        'End If

        Dim nb As Integer = 0

        For Each tn As TreeNode In LstArmoire.Nodes
            Select Case DirectCast(sender, DropDownList).SelectedItem.Value
                Case "TOUT"
                    If Not (tn.Checked) Then
                        tn.Checked = True
                        nb += 1
                    End If
                Case "RIEN"
                    If (tn.Checked) Then
                        tn.Checked = False
                    End If
                Case "INV"
                    tn.Checked = Not (tn.Checked)
                    If (tn.Checked) Then
                        nb += 1
                    End If
            End Select
        Next

        DirectCast(sender, DropDownList).SelectedIndex = 0

        GereSelectionChange(nb)
    End Sub

    Public Sub EndInit() Implements IControlArmoire.EndInit
        If WsControllerArmoire Is Nothing Then
            Return
        End If
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        txtRecherche.Text = ""

        Dim lettrebouton As String = GereSelectionBouton()

        Donnees = New ElementArmoireCollection()

        Dim elems As IEnumerable = TryCast(items, IEnumerable)

        Dim itemarmoire As IElementArmoire
        If Not (elems Is Nothing) Then
            For Each elem As Object In elems

                itemarmoire = TryCast(elem, IElementArmoire)

                If (itemarmoire Is Nothing) Then
                    Exit For
                End If

                Donnees.Add(itemarmoire)
            Next
        End If

        If (lettrebouton <> "Arobase") Then
            ChargeItems(Function(it As IElementArmoire)
                            Return it.Libelle.ToLower().StartsWith(lettrebouton.ToLower())
                        End Function)
            Return
        End If

        ChargeItems(Nothing)
    End Sub

    Private Sub ChargeItems(predicat As Func(Of IElementArmoire, Boolean))
        DonneesFiltrees = Donnees.ToDonneesFiltrees(predicat)

        Rafraichir = True
    End Sub

    Private Sub SelOuNon_BoutonLettre(id As String, TypeImage As String)

        Dim bouton As ImageButton = VisuHelper.GetImageBoutonLettre(RowLettres, id)

        If (bouton Is Nothing) Then
            bouton = VisuHelper.GetImageBoutonLettre(RowLettres, id)
        End If

        bouton.ImageUrl = "~/Images/Lettres/" & TypeImage & ".bmp"

    End Sub

    Private Function GetLettreButton(IDButton As String)
        Return (IDButton.ToLower().Split(New String() {"button"}, StringSplitOptions.RemoveEmptyEntries))(0).ToUpper()
    End Function

    Protected Sub GereSelectionChange(nb As Integer)
        Dim controllerselection As IControllerArmoireSelection = TryCast(WsControllerArmoire, IControllerArmoireSelection)

        If (controllerselection Is Nothing OrElse controllerselection.Act_NbSelectionChange Is Nothing) Then
            Return
        End If
        Dim act As Action(Of Integer) = controllerselection.Act_NbSelectionChange
        act(nb)
    End Sub

    Private Function GereSelectionBouton() As String
        If HButtonLettre.Value.Trim() <> "" And HButtonLettre.Value <> "TXT" Then
            SelOuNon_BoutonLettre(HButtonLettre.Value, GetLettreButton(HButtonLettre.Value))
        End If
        Dim lettrebouton As String = [Enum].GetName(GetType(ValeurDefaut), LettreDefaut)
        If lettrebouton = "_TOUS" Then
            lettrebouton = "Arobase"
        End If
        SelOuNon_BoutonLettre("Button" & lettrebouton, GetLettreButton("Button" & lettrebouton) & "_sel")
        HButtonLettre.Value = "Button" & lettrebouton

        Return lettrebouton
    End Function

    Public Sub Reinit()
        txtRecherche.Text = ""

        Dim lettrebouton = GereSelectionBouton()
        Dim bouton As ImageButton = VisuHelper.GetImageBoutonLettre(RowLettres, "Button" & lettrebouton)

        'Pour forcer le chargement de la liste
        HButtonLettre.Value = ""
        ButtonLettre_Click(bouton, Nothing)

        cboselection.SelectedIndex = 2
        cboselection_SelectedIndexChanged(cboselection, Nothing)
    End Sub

    Public Sub Deselectionne()
        If Not (LstArmoire.SelectedNode Is Nothing) Then
            LstArmoire.SelectedNode.Selected = False
        End If
    End Sub

End Class