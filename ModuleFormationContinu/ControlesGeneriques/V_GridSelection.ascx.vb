﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Public Class V_GridSelection
    Inherits ControlDataGrid

    Protected Overrides ReadOnly Property P_CadreGrid As Table
        Get
            Return Me.CadreGrid
        End Get
    End Property
    Protected Overrides ReadOnly Property P_GridDonnee As GridView
        Get
            Return Me.GridDonnee
        End Get
    End Property

End Class