﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VCalendrier.ascx.vb" Inherits="Virtualia.Net.VCalendrier" %>

<asp:Table ID="TabMatinMidi" runat="server" HorizontalAlign="Center" BackColor="#053E3C">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
            <asp:Label ID="lbl" runat="server" Text="Sélectionner une date" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" Width="250px" ForeColor="#142425"/>
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
            <asp:DropDownList ID="CboAnnee" runat="server" AutoPostBack="true"/>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="cellMatin">
            <asp:RadioButton ID="RadioMatin" runat="server" Text="Matin" Visible="true" AutoPostBack="false" GroupName="GrpEtendu" ForeColor="#142425" Height="18px" Width="150px" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-align: right; vertical-align: middle; text-align: center" />
        </asp:TableCell>
        <asp:TableCell ID="cellAprem">
            <asp:RadioButton ID="RadioMidi" runat="server" Text="Après-midi" Visible="true" AutoPostBack="false" GroupName="GrpEtendu" ForeColor="#142425" Height="18px" Width="150px" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-align: right; vertical-align: middle; text-align: center" />
        </asp:TableCell>
        <asp:TableCell ID="cellJour">
            <asp:RadioButton ID="RadioJournee" runat="server" Text="Journée complète" Visible="true" AutoPostBack="false" GroupName="GrpEtendu" ForeColor="#142425" Height="18px" Width="150px" BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="text-align: right; vertical-align: middle; text-align: center" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
            <asp:UpdatePanel ID="UP" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Calendar ID="Calendrier" runat="server" DayHeaderStyle-BorderColor="#CAEBE4" SelectedDayStyle-BackColor="#2D8780" ForeColor="White" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="CboAnnee" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
            <asp:Button ID="ButtonOK" runat="server" Text="OK" OnClick="Btn_Click"  Width="90px" BackColor="#B0E0D7"/>
            <asp:Button ID="ButtonAnnul" runat="server" Text="Annuler" OnClick="Btn_Click" Width="90px" BackColor="#B0E0D7"/>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>



