﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="V_GridSelection.ascx.vb" Inherits="Virtualia.Net.V_GridSelection" %>

<asp:Table ID="CadreGrid" runat="server" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="True">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">
            <asp:GridView ID="GridDonnee" runat="server" Caption="Virtualia" CaptionAlign="Top" CellPadding="2" BackColor="#B0E0D7" ForeColor="#142425" Height="40px" HorizontalAlign="Left" Width="1000px" AlternatingRowStyle-BorderStyle="None" BorderStyle="Inset" AutoGenerateColumns="False" AutoGenerateSelectButton="True"  >
                <%--OnPageIndexChanging="GridDonnee_PageIndexChanging" OnPageIndexChanged="GridDonnee_PageIndexChanged" OnSelectedIndexChanged="GridDonnee_SelectedIndexChanged"--%>
                <RowStyle BackColor="#DFFAF3" Font-Names="Trebuchet MS" Font-Italic="false" Font-Bold="false" Font-Size="Small" Height="18px" HorizontalAlign="Left" VerticalAlign="Middle" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#225C59" ForeColor="White" Font-Bold="False" Font-Italic="True" Font-Names="Trebuchet MS" Font-Size="Small" HorizontalAlign="Center" BorderStyle="Outset" BorderWidth="1px" Height="20px" VerticalAlign="Middle" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
