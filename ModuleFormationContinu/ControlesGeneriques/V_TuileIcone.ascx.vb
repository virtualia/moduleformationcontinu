﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBase.Formation

Public Class V_TuileIcone
    Inherits ObjetTuileIconeBase

    Protected Overrides ReadOnly Property V_CmdChoix As ImageButton
        Get
            Return Me.CmdChoix
        End Get
    End Property
    Protected Overrides ReadOnly Property V_Etiquette As Label
        Get
            Return Me.Etiquette
        End Get
    End Property
    Protected Overrides ReadOnly Property V_VIcone As Table
        Get
            Return Me.VIcone
        End Get
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)

        AddHandler CmdChoix.Click, AddressOf Me.CmdChoix_Click
    End Sub
End Class