﻿Imports Virtualia.OutilsVisu.Formation
Public Class VCalendrier
    Inherits UserControl
    Private WsAct_Selectionne As Action(Of ValueCalendrier)

    Public WriteOnly Property Act_Selectionne As Action(Of ValueCalendrier)
        Set(ByVal value As Action(Of ValueCalendrier))
            WsAct_Selectionne = value
        End Set
    End Property

    Public WriteOnly Property DateDefaut As DateTime
        Set(ByVal value As DateTime)
            VCache.DateDefaut = New DateTime(value.Year, value.Month, value.Day)
        End Set
    End Property

    Public WriteOnly Property TypeJourneeDefaut As TypeSelectionJournee
        Set(ByVal value As TypeSelectionJournee)
            VCache.TypeJournee = value
        End Set
    End Property

    Private WriteOnly Property AnneeDefaut As Integer
        Set(ByVal value As Integer)
            For Each it As ListItem In CboAnnee.Items
                it.Selected = (it.Text = VCache.Annee)
            Next
        End Set
    End Property

    Public WriteOnly Property AnneeDebut As Integer
        Set(ByVal value As Integer)
            VCache.AnneeDeb = value
        End Set
    End Property

    Public WriteOnly Property AnneeFin As Integer
        Set(ByVal value As Integer)
            VCache.AnneeFin = value
        End Set
    End Property

    Private ReadOnly Property VCache As CacheCalendrier
        Get
            If Me.ViewState.KeyExiste(CacheCalendrier.KeyState) = False Then
                Me.ViewState.AjouteValeur(CacheCalendrier.KeyState, New CacheCalendrier())
            End If
            Return Me.ViewState.GetValeur(Of CacheCalendrier)(CacheCalendrier.KeyState)
        End Get
    End Property

    Public WriteOnly Property SelectionVisible As Boolean
        Set(value As Boolean)
            ViewState.AjouteValeur("SelectionVisible", value)
        End Set
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        If Not (IsPostBack) Then
            CacheCalendrier.GetAnnee(VCache.AnneeDeb, VCache.AnneeFin).ForEach(Sub(an)
                                                                                   Me.CboAnnee.Items.Add("" & an)
                                                                               End Sub)
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        If VCache.Reset = True Then
            CboAnnee.SelectedIndex = -1
            For Each it As ListItem In CboAnnee.Items
                it.Selected = (it.Text = VCache.DateDefaut.Year.ToString())
            Next
            TypeJourneeToRadio(VCache.TypeJournee)

            Me.Calendrier.VisibleDate = VCache.DateDefaut
            Me.Calendrier.SelectedDate = VCache.DateDefaut

            VCache.Reset = False

            Me.ViewState.AjouteValeur(CacheCalendrier.KeyState, VCache)
        End If

        Dim cellvisible As Boolean = True
        If ViewState.KeyExiste("SelectionVisible") = True Then
            cellvisible = DirectCast(ViewState("SelectionVisible"), Boolean)
        End If

        cellAprem.Visible = cellvisible
        cellJour.Visible = cellvisible
        cellMatin.Visible = cellvisible

    End Sub

    Private Function GetTypeJournee() As TypeSelectionJournee
        Dim CRetour As TypeSelectionJournee = TypeSelectionJournee.RIEN

        If RadioJournee.Checked = True Then
            CRetour = TypeSelectionJournee.JOUR
        End If
        If RadioMatin.Checked = True Then
            CRetour = TypeSelectionJournee.MATIN
        End If
        If RadioMidi.Checked = True Then
            CRetour = TypeSelectionJournee.MIDI
        End If
        Return CRetour
    End Function

    Private Sub TypeJourneeToRadio(valeur As TypeSelectionJournee)
        Select Case valeur
            Case TypeSelectionJournee.JOUR
                Me.ViewState("TYPE") = "JOUR"
                RadioJournee.Checked = True
                RadioMatin.Checked = False
                RadioMidi.Checked = False
                Return
            Case TypeSelectionJournee.MATIN
                Me.ViewState("TYPE") = "MATIN"
                RadioJournee.Checked = False
                RadioMatin.Checked = True
                RadioMidi.Checked = False
                Return
            Case TypeSelectionJournee.MIDI
                Me.ViewState("TYPE") = "MIDI"
                RadioJournee.Checked = False
                RadioMatin.Checked = False
                RadioMidi.Checked = True
                Return
            Case TypeSelectionJournee.RIEN
                Me.ViewState("TYPE") = "RIEN"
                RadioJournee.Checked = False
                RadioMatin.Checked = False
                RadioMidi.Checked = False
                Return
        End Select
    End Sub

    Public Sub Clear()
        VCache.Reset = True
    End Sub

    Protected Sub Btn_Click(sender As Object, e As EventArgs)
        If WsAct_Selectionne Is Nothing Then
            Return
        End If
        Dim act As Action(Of ValueCalendrier) = WsAct_Selectionne
        Dim Valeur As ValueCalendrier = New ValueCalendrier()

        Valeur.Annuler = Not (DirectCast(sender, Control).ID.Contains("OK"))
        If Valeur.Annuler = True Then
            act(Valeur)
            Return
        End If

        Valeur.DateSelectionnee = Me.Calendrier.SelectedDate
        Valeur.TypeJournee = GetTypeJournee()
        act(Valeur)

    End Sub

    Protected Sub CboAnnee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboAnnee.SelectedIndexChanged
        If CboAnnee.SelectedIndex <= 0 Then
            Return
        End If
        Dim dte As DateTime = New DateTime(Integer.Parse(CboAnnee.SelectedItem.Text), 1, 1)
        Me.Calendrier.SelectedDate = dte
        Me.Calendrier.VisibleDate = dte
    End Sub

End Class





