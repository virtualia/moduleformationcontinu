﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation

Public Class V_Message
    Inherits UserControl
    Implements IMessage

    Public ReadOnly Property NomControl As String Implements IMessage.NomControl
        Get
            Return Me.ID
        End Get
    End Property

    Private WsAct_RetourMessage As Action(Of CacheRetourMessage)

    Public WriteOnly Property Act_RetourMessage As Action(Of CacheRetourMessage) Implements IMessage.Act_RetourMessage
        Set(value As Action(Of CacheRetourMessage))
            WsAct_RetourMessage = value
        End Set
    End Property

    Public Sub Charge(cache As Object) Implements IMessage.Charge
        Dim cachetmp As CacheAppelMessage = DirectCast(cache, CacheAppelMessage)
        If (cachetmp Is Nothing) Then
            Return
        End If
        CmdCancel.Visible = False
        CmdOui.Visible = False
        CmdNon.Visible = False
        EtiMsg.Text = ""
        EtiTitre.Text = ""

        For Each bt As BoutonMessage In cachetmp.Boutons
            Select Case bt.TypeCommande
                Case TypeCommandeMessage.CANCEL
                    CmdCancel.Visible = True
                    CmdCancel.Text = IIf(bt.Libelle.Trim() = "", "Annuler", bt.Libelle.Trim()).ToString()
                Case TypeCommandeMessage.OUI
                    CmdOui.Visible = True
                    CmdOui.Text = IIf(bt.Libelle.Trim() = "", "Oui", bt.Libelle.Trim()).ToString()
                Case TypeCommandeMessage.NON
                    CmdNon.Visible = True
                    CmdNon.Text = IIf(bt.Libelle.Trim() = "", "Non", bt.Libelle.Trim()).ToString()
            End Select
        Next

        EtiTitre.Text = cachetmp.Titre

        Dim sep As String = ""
        cachetmp.Messages.ForEach(Sub(msg)
                                      EtiMsg.Text += sep + msg
                                      sep = vbCrLf
                                  End Sub)

        Me.ViewState.AjouteValeur(CacheAppelMessage.KeyState, cachetmp)
    End Sub

    Protected Sub Cmd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdOui.Click, CmdNon.Click, CmdCancel.Click
        GereRetour(DirectCast(sender, Button).Text)
    End Sub

    Private Sub GereRetour(NomBouton As String)
        If WsAct_RetourMessage Is Nothing Then
            Return
        End If
        Dim cacheappel As CacheAppelMessage = Me.ViewState.GetValeur(Of CacheAppelMessage)(CacheAppelMessage.KeyState)

        Dim cacheretour As CacheRetourMessage = New CacheRetourMessage()
        If (cacheappel Is Nothing) Then
            cacheretour.Commande = TypeCommandeMessage._AUCUN
            WsAct_RetourMessage(cacheretour)
            Return
        End If

        Dim tpcmd As TypeCommandeMessage = (From bt In cacheappel.Boutons Where bt.Libelle = NomBouton Select bt.TypeCommande).First()

        cacheretour.IDControlSaisie = cacheappel.IDControlSaisie
        cacheretour.NomVueSource = cacheappel.NomVueSource
        cacheretour.Commande = tpcmd
        cacheretour.TypeMessage = cacheappel.TypeMessage
        cacheretour.NumObjet = cacheappel.NumObjet

        ViewState.Remove(CacheAppelMessage.KeyState)

        WsAct_RetourMessage(cacheretour)
    End Sub

End Class