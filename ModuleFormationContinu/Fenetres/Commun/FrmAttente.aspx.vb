﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Virtualia.ObjetBaseStructure.Formation
Imports Virtualia.OutilsVisu.Formation

Public Class FrmAttente
    Inherits Page

    <Serializable>
    Public Class CacheAttente
        Public Const KeyState As String = "VAttente"
        Public Property Index As Integer
        Public Property UrlCible As String
        Public Property Parametre As String

        Public Sub Sauve(vs As StateBag)
            vs.AjouteValeur(KeyState, Me)
        End Sub

        Public Sub New()
            Index = 0
            UrlCible = ""
            Parametre = ""
        End Sub
    End Class

    Private ReadOnly Property V_Cache As CacheAttente
        Get
            If Not (ViewState.KeyExiste(CacheAttente.KeyState)) Then
                ViewState.AjouteValeur(CacheAttente.KeyState, New CacheAttente())
            End If
            Return ViewState.GetValeur(Of CacheAttente)(CacheAttente.KeyState)
        End Get
    End Property

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        Dim NumLogo As String = Server.HtmlDecode(Request.QueryString("Index"))
        Dim LienCible As String = Server.HtmlDecode(Request.QueryString("Lien"))
        Dim Param As String = Server.HtmlDecode(Request.QueryString("Param"))

        If LienCible = "" Then
            Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, "")) 'Retour à l'accueil Virtualia.Net
        End If

        If IsNumeric(NumLogo) Then
            V_Cache.Index = Integer.Parse(NumLogo)
            V_Cache.Sauve(ViewState)
        Else
            NumLogo = "0"
        End If

        CadreUpdateAttente.BackColor = Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase.Charte.CouleurTuile(V_Cache.Index)

        If LienCible.ToLower().StartsWith("http") Then
            V_Cache.UrlCible = LienCible
            V_Cache.Sauve(ViewState)
            Return
        End If

        If Param = "" Then
            V_Cache.UrlCible = LienCible
        Else
            V_Cache.UrlCible = LienCible & "?Param=" & Param
        End If

        V_Cache.Sauve(ViewState)

    End Sub

    Private Sub TimerAttente_Tick(sender As Object, e As EventArgs) Handles TimerAttente.Tick
        If V_Cache.UrlCible = "" Then
            Return
        End If
        Dim ChaineHttp As String = V_Cache.UrlCible
        V_Cache.UrlCible = ""
        V_Cache.Sauve(ViewState)
        Response.Redirect(ChaineHttp)
    End Sub

End Class