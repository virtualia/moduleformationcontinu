﻿Option Explicit On
Option Strict On
Option Compare Text
Public Class FrmTableauxBord
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public ReadOnly Property VObjetGlobal As Virtualia.Net.Session.LDObjetGlobal
        Get
            Return DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)
        End Get
    End Property

    Public ReadOnly Property ObjSession As Virtualia.Net.Session.LDObjetSession
        Get
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            Return DirectCast(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)
        End Get
    End Property

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)

        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Dim Msg As String = "Erreur lors de l'identification. Session invalide."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Return
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Master.V_Info2 = ""
            Master.V_Info3 = ""
            Dim Msg As String = "Erreur lors de l'identification. Pas de connexion utilisateur."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
        End If
    End Sub

End Class