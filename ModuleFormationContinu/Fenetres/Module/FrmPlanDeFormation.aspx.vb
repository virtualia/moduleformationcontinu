﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
Imports Virtualia.Systeme.Evenements

Public Class FrmPlanDeFormation
    Inherits Page
    Implements IControlBase
    Implements IControllerMultiVue

    <Serializable>
    Public Class CacheFrmPlan
        Public Const KeyState As String = "CacheFrmPlan"
        Public Property IdeStageCourant As Integer = 0
        Public Property ItemArmoirs As Virtualia.OutilsVisu.Formation.ListItemArmoireCollection = New Virtualia.OutilsVisu.Formation.ListItemArmoireCollection()
        Public Property KeyArmoireSelectionnee As String = ""
        Public Property EstNouveauStage As Boolean = False

        Public Sub Sauve(ByVal vs As StateBag)
            vs.AjouteValeur(KeyState, Me)
        End Sub
    End Class
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private ReadOnly Property VCache As CacheFrmPlan
        Get
            If Not (ViewState.KeyExiste(CacheFrmPlan.KeyState)) Then
                ViewState.AjouteValeur(CacheFrmPlan.KeyState, New CacheFrmPlan())
            End If
            Return ViewState.GetValeur(Of CacheFrmPlan)(CacheFrmPlan.KeyState)
        End Get
    End Property

    Public ReadOnly Property MoteurFormation As Virtualia.Metier.Formation.GestionFormation
        Get
            Dim gest As Virtualia.Metier.Formation.GestionFormation =
                DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).Gestion_Plandeformation
            gest.ParametreFormation = ParamFormation
            Return gest
        End Get
    End Property

    Public ReadOnly Property ParamFormation As Virtualia.Metier.Formation.ParametreFormationInfo
        Get
            Return VObjetGlobal.ParametreFormation
        End Get
    End Property

    Public ReadOnly Property MoteurInscription As Virtualia.Metier.Formation.GestionInscription
        Get
            Dim gest As Virtualia.Metier.Formation.GestionInscription =
                DirectCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession).Gestion_Inscription
            gest.ParametreFormation = ParamFormation
            Return gest
        End Get
    End Property

    Public ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property
    Public ReadOnly Property VObjetGlobal As Virtualia.Net.Session.LDObjetGlobal
        Get
            Return DirectCast(DirectCast(Me.Application("VirGlobales2"), Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase), Virtualia.Net.Session.LDObjetGlobal)
        End Get
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Public ReadOnly Property Act_Retour As Action(Of String, Boolean) Implements IControllerMultiVue.Act_Retour
        Get
            Return Sub(KeyVue, recharger)
                       Select Case KeyVue
                           Case "STAGE"
                               If VCache.EstNouveauStage = True Then
                                   If recharger = True Then
                                       Dim id As Integer? = MoteurFormation.IdeCourant
                                       If id IsNot Nothing Then
                                           MoteurFormation.GetStructure(Integer.Parse(MoteurFormation.IdeCourant.ToString()))
                                       End If
                                       VCache.KeyArmoireSelectionnee = "STAGE_TOUS"
                                       VCache.Sauve(ViewState)
                                   Else
                                       VCache.EstNouveauStage = False
                                       VCache.Sauve(ViewState)
                                   End If
                               End If
                               Charge(recharger)
                               Return
                           Case "GESTIONSTAGE"
                               Charge(recharger)
                               Return
                           Case "LISTINSC"
                               Charge(False)
                               If (recharger) Then
                                   MoteurInscription.Reinitialiser()
                               End If
                               Return
                           Case "MULTIINSC"
                               If (recharger) Then
                                   'On a créé de nouvelles inscriptions, on affiche directement les inscriptions
                                   MoteurInscription.Reinitialiser()
                                   Dim Ide As Integer = VCache.IdeStageCourant
                                   VCache.IdeStageCourant = 0
                                   VCache.Sauve(ViewState)
                                   If Ide = 0 Then
                                       Charge(False)
                                       Return
                                   End If
                                   ActSelection(Ide)
                                   Return
                               End If
                               Charge(False)
                               Return
                       End Select
                   End Sub
        End Get
    End Property

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
        BtnNouveau.Act = Sub(btn)
                             ActSelection(-1)
                         End Sub
        If V_WebFonction.VerifierCookie(Me, Session.SessionID) = False Then
            Dim Msg As String = "Erreur lors de l'identification. Session invalide."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Return
        End If
        If V_WebFonction.PointeurUtilisateur Is Nothing Then
            Master.V_Info2 = ""
            Master.V_Info3 = ""
            Dim Msg As String = "Erreur lors de l'identification. Pas de connexion utilisateur."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Return
        End If
        Call RemplirItemArmoires()

        InitCtrlArbre()
        InitMethodeArbre()
        CtrlArbre.EndInit()

        InitMethodeCalendrier()
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        MyBase.OnPreRender(e)
        If IsPostBack = False Then
            Charge(Nothing)
        End If
    End Sub

    Public Sub Charge(ByVal items As Object) Implements IControlBase.Charge
        RCmdNouv.Visible = True
        If items Is Nothing Then
            If System.Configuration.ConfigurationManager.AppSettings("SiOuvrirSurNonTraitees") = "Oui" Then
                ActItemArmoireChange("STAGE_DEMANDES_VALIDEES")
                MultiOnglets.ActiveViewIndex = 0
                Exit Sub
            End If
            Dim LDSessionModule As Virtualia.Net.Session.LDObjetSession
            LDSessionModule = TryCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)
            If LDSessionModule.SiAccesAuModuleSeul = True Then
                ActItemArmoireChange("demandes")
            Else
                ActItemArmoireChange("calendrier")
            End If
            Exit Sub
        End If
        If DirectCast(items, Boolean) = True Then
            ActItemArmoireChange(VCache.KeyArmoireSelectionnee)
            If (VCache.EstNouveauStage) Then
                Master.SessionCourante.ReinitListeArbo()
                VCache.EstNouveauStage = False
                VCache.Sauve(ViewState)
            End If
            If MoteurFormation.IdeCourant > 0 Then
                CtrlArbre.Selectionne(DirectCast(MoteurFormation.GetStructure(Integer.Parse(MoteurFormation.IdeCourant.ToString())),
                                      Virtualia.Metier.Formation.StructureStage).Identification.Intitule, False)
            End If
            Exit Sub
        End If
        If VCache.KeyArmoireSelectionnee.ToLower() = "calendrier" Then
            MultiOnglets.ActiveViewIndex = 1
            Exit Sub
        ElseIf VCache.KeyArmoireSelectionnee.ToLower() = "demandes" Then
            MultiOnglets.ActiveViewIndex = 3
            Exit Sub
        End If
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Private Sub InitCtrlArbre()
        If IsPostBack = True Then
            Return
        End If
        Dim Lstmcboarmoire As Virtualia.OutilsVisu.Formation.ListItemArmoireCollection = VCache.ItemArmoirs

        CtrlArbre.ArmoireItems = Lstmcboarmoire

        Dim LstStyleTmp As Virtualia.OutilsVisu.Formation.ListStyleTreeNodeCollection = New Virtualia.OutilsVisu.Formation.ListStyleTreeNodeCollection()

        LstStyleTmp.Add(New StyleTreeNode("Etablissement", Virtualia.Metier.Formation.ConstanteFormation.IconeEtablissement) With {.PopulateOnDemand = True})
        LstStyleTmp.Add(New StyleTreeNode("PlandeFormation", Virtualia.Metier.Formation.ConstanteFormation.IconePlan) With {.PopulateOnDemand = True})
        LstStyleTmp.Add(New StyleTreeNode("Domaine", Virtualia.Metier.Formation.ConstanteFormation.IconeDomaine) With {.PopulateOnDemand = True})
        LstStyleTmp.Add(New StyleTreeNode("Theme", Virtualia.Metier.Formation.ConstanteFormation.IconeTheme) With {.PopulateOnDemand = True})
        LstStyleTmp.Add(New StyleTreeNode("Sous_Theme", Virtualia.Metier.Formation.ConstanteFormation.IconeSousTheme) With {.PopulateOnDemand = True})
        LstStyleTmp.Add((New StyleTreeNode("Stage", Virtualia.Metier.Formation.ConstanteFormation.IconeStage) With {.AvecIde = True, .AfficherFeuilleVide = False, .PopulateOnDemand = True}))
        LstStyleTmp.Add(New StyleTreeNode("LibSession", Virtualia.Metier.Formation.ConstanteFormation.IconeSession) With {.AvecIde = True, .AfficherFeuilleVide = False})

        CtrlArbre.StyleNoeuds = LstStyleTmp
        CtrlArbre.ProprieteDeRecherche = "Stage"
    End Sub

    Private Sub InitMethodeArbre()
        Dim ListeDyna As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)

        CtrlArbre.Func_DataSource = Function()
                                        Select Case VCache.KeyArmoireSelectionnee
                                            Case "PLAN_DERNIERE_N_ANNEE", "ETAB_PLAN_TOUS"
                                                If Master.SessionCourante.ListeArboPlan_ANS Is Nothing Then
                                                    Master.SessionCourante.ListeArboPlan_ANS = Virtualia.Metier.Formation.FormationHelper.GetStageAns(3, V_WebFonction.PointeurUtilisateur.Etablissement)
                                                End If
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return Master.SessionCourante.ListeArboPlan_ANS.OrderBy(Of String)(Function(el)
                                                                                                                           Return el.PlandeFormation & "~" & el.Domaine & "~" & el.Theme & "~" & el.SousTheme & "~" & el.Stage & "~" & el.IntituleSession
                                                                                                                       End Function).ToList()
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In Master.SessionCourante.ListeArboPlan_ANS Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes.OrderBy(Of String)(Function(el)
                                                                                         Return el.PlandeFormation & "~" & el.Domaine & "~" & el.Theme & "~" & el.SousTheme & "~" & el.Stage & "~" & el.IntituleSession
                                                                                     End Function).ToList()
                                                End If

                                            Case "PLAN_TOUS"
                                                If Master.SessionCourante.ListeArboPlan_TOUS Is Nothing Then
                                                    Master.SessionCourante.ListeArboPlan_TOUS = Virtualia.Metier.Formation.FormationHelper.GetStageAns(-1, V_WebFonction.PointeurUtilisateur.Etablissement)
                                                End If
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return Master.SessionCourante.ListeArboPlan_TOUS.OrderBy(Of String)(Function(el)
                                                                                                                            Return el.PlandeFormation & "~" & el.Domaine & "~" & el.Theme & "~" & el.SousTheme & "~" & el.Stage & "~" & el.IntituleSession
                                                                                                                        End Function).ToList()
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In Master.SessionCourante.ListeArboPlan_TOUS Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes.OrderBy(Of String)(Function(el)
                                                                                         Return el.PlandeFormation & "~" & el.Domaine & "~" & el.Theme & "~" & el.SousTheme & "~" & el.Stage & "~" & el.IntituleSession
                                                                                     End Function).ToList()
                                                End If

                                            Case "STAGE_TOUS"
                                                If Master.SessionCourante.ListeArboPlan_TOUS Is Nothing Then
                                                    Master.SessionCourante.ListeArboPlan_TOUS = Virtualia.Metier.Formation.FormationHelper.GetStageAns(-1, V_WebFonction.PointeurUtilisateur.Etablissement)
                                                End If
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return Master.SessionCourante.ListeArboPlan_TOUS.OrderBy(Of String)(Function(el)
                                                                                                                            Return el.Stage & "~" & el.IntituleSession
                                                                                                                        End Function).ToList()
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In Master.SessionCourante.ListeArboPlan_TOUS Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes.OrderBy(Of String)(Function(el)
                                                                                         Return el.Stage & "~" & el.IntituleSession
                                                                                     End Function).ToList()
                                                End If

                                            Case "PLAN_STAGE_SESSION_A_VENIR"
                                                ListeDyna = Virtualia.Metier.Formation.FormationHelper.GetStageAvecSessionAVenir(V_WebFonction.PointeurUtilisateur.Etablissement)
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return ListeDyna
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In ListeDyna Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes
                                                End If

                                            Case "PLAN_STAGE_SANS_INSCRIT"
                                                ListeDyna = Virtualia.Metier.Formation.FormationHelper.GetStageSansInscription(V_WebFonction.PointeurUtilisateur.Etablissement)
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return ListeDyna
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In ListeDyna Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes
                                                End If

                                            Case "PLAN_STAGE_SANS_SESSION"
                                                ListeDyna = Virtualia.Metier.Formation.FormationHelper.GetStageSansSession(V_WebFonction.PointeurUtilisateur.Etablissement)
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return ListeDyna.OrderBy(Of String)(Function(el)
                                                                                            Return el.PlandeFormation & "~" & el.Domaine & "~" & el.Theme & "~" & el.Stage
                                                                                        End Function).ToList()
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In ListeDyna Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes.OrderBy(Of String)(Function(el)
                                                                                         Return el.PlandeFormation & "~" & el.Domaine & "~" & el.Theme & "~" & el.Stage
                                                                                     End Function).ToList()
                                                End If

                                            Case "PLAN_STAGE_SANS_CONVOCATION" 'Ajout RP
                                                ListeDyna = Virtualia.Metier.Formation.FormationHelper.GetStageSansConvocation(V_WebFonction.PointeurUtilisateur.Etablissement)
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return ListeDyna
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In ListeDyna Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes
                                                End If

                                            Case "STAGE_DEMANDES_VALIDEES" 'Ajout RP
                                                Dim LDSessionModule As Virtualia.Net.Session.LDObjetSession
                                                LDSessionModule = TryCast(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.LDObjetSession)

                                                ListeDyna = Virtualia.Metier.Formation.FormationHelper.GetStageDemandesValidees(V_WebFonction.PointeurUtilisateur.Etablissement, LDSessionModule.SiAccesAuModuleSeul)
                                                If Master.SessionCourante.FiltreSurPlan = "" Then
                                                    Return ListeDyna
                                                Else
                                                    Dim LstRes As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo)
                                                    LstRes = (From Element In ListeDyna Where Element.PlandeFormation = Master.SessionCourante.FiltreSurPlan).ToList
                                                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                                                        Return Nothing
                                                    End If
                                                    Return LstRes.OrderBy(Of String)(Function(el)
                                                                                         Return el.Stage & "~" & el.IntituleSession
                                                                                     End Function).ToList()
                                                End If

                                        End Select
                                        Return Nothing

                                    End Function

        CtrlArbre.Func_CalculNbTotal = Function(donnees, Key, predicat)

                                           Dim lst As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo) = DirectCast(donnees, List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo))
                                           Dim nbtot As Integer = (From it In lst Select it.Stage).Distinct().Count()
                                           If (predicat Is Nothing) Then
                                               Return "" & nbtot
                                           End If
                                           Return "" & (From it In lst Where predicat(it) Select it.Stage).Distinct().Count() & " / " & nbtot
                                       End Function

        CtrlArbre.Act_NoeudChange = Sub(tns, key)
                                        Dim Depth_Max As Integer = (From it In VCache.ItemArmoirs Where it.Key = key Select (it.Proprietes.Count - 1)).First()
                                        Dim Depth_Selection As Integer = 0
                                        Dim Noeud As TreeNode = tns(0)

                                        Noeud.Selected = False
                                        Depth_Selection = Depth_Max - 1
                                        If Noeud.Depth >= Depth_Selection Then
                                            Noeud.Selected = False
                                            Try
                                                ActSelection(Integer.Parse(Noeud.Value.Split("|"c).Last()))
                                            Catch ex As Exception
                                                Exit Try
                                            End Try
                                            Exit Sub
                                        End If
                                        CtrlArbre.CollapseAutreNoeud(Noeud)
                                        If Noeud.Expanded = False Then
                                            Noeud.Expand()
                                        Else
                                            Noeud.CollapseAll()
                                        End If
                                    End Sub

        CtrlArbre.Act_ItemArmoireChange = AddressOf ActItemArmoireChange
        CtrlArbre.Act_ItemPlanChange = AddressOf ActItemPlanChange
    End Sub

    Private Sub RemplirItemArmoires()
        If VCache.ItemArmoirs.Count > 0 Then
            Exit Sub
        End If
        Dim LstItem As Virtualia.OutilsVisu.Formation.ListItemArmoireCollection = New Virtualia.OutilsVisu.Formation.ListItemArmoireCollection()
        Dim Armoire As Virtualia.OutilsVisu.Formation.ItemArmoire
        Dim itarmCalendrier As Virtualia.OutilsVisu.Formation.ItemArmoire

        '** P
        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "STAGE_DEMANDES_VALIDEES", .Libelle = "Liste des stages pour lesquels les demandes de formation validées ne sont pas traitées"}
        Armoire.FiltreDefaut = ValeurDefaut._TOUS
        Armoire.AfficheFitre = True
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)
        '**
        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "PLAN_DERNIERE_N_ANNEE", .Libelle = "Liste organisée par plan, domaine, thème des stages des 3 dernières années"}
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Domaine")
        Armoire.Proprietes.Add("Theme")
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "PLAN_TOUS", .Libelle = "Liste organisée par plan, domaine, thème de tous les stages"}
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Domaine")
        Armoire.Proprietes.Add("Theme")
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "ETAB_PLAN_TOUS", .Libelle = "Liste organisée par établissement, plan, domaine, thème des stages des 3 dernières années"}
        Armoire.Proprietes.Add("Etablissement")
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Domaine")
        Armoire.Proprietes.Add("Theme")
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "STAGE_TOUS", .Libelle = "Liste alphabétique de tous les stages"}
        Armoire.FiltreDefaut = ValeurDefaut.A
        Armoire.AfficheFitre = True
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "PLAN_STAGE_SESSION_A_VENIR", .Libelle = "Liste organisée par plan des stages ayant des sessions à venir"}
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        Armoire = New ItemArmoire() With {.Key = "PLAN_STAGE_SANS_SESSION", .Libelle = "Liste organisée par plan, domaine, thème des stages sans session"}
        Armoire.FiltreDefaut = ValeurDefaut._TOUS
        Armoire.AfficheFitre = True
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Domaine")
        Armoire.Proprietes.Add("Theme")
        Armoire.Proprietes.Add("Stage")
        LstItem.Add(Armoire)

        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "PLAN_STAGE_SANS_INSCRIT", .Libelle = "Liste organisée par plan des stages sans inscrit"}
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        Armoire = New Virtualia.OutilsVisu.Formation.ItemArmoire() With {.Key = "PLAN_STAGE_SANS_CONVOCATION", .Libelle = "Liste organisée par plan des stages pour lesquels les inscrits n'ont pas été convoqués"}
        Armoire.Proprietes.Add("PlandeFormation")
        Armoire.Proprietes.Add("Stage")
        Armoire.Proprietes.Add("LibSession")
        LstItem.Add(Armoire)

        '** RP
        itarmCalendrier = New ItemArmoire() With {.Key = "DEMANDES", .Libelle = "Le calendrier des demandes de formation"}
        LstItem.Add(itarmCalendrier)
        '**
        itarmCalendrier = New ItemArmoire() With {.Key = "CALENDRIER", .Libelle = "Le calendrier des sessions"}
        LstItem.Add(itarmCalendrier)

        VCache.ItemArmoirs = LstItem

        VCache.Sauve(ViewState)

    End Sub

    Private Sub InitMethodeCalendrier()
        '**RP
        CtrlPlanning.ArmoireChange = AddressOf ActItemArmoireChange
        CtrlPlanning.IdeChange = AddressOf ActSelection
        CtrlDemandes.ArmoireChange = AddressOf ActItemArmoireChange
        CtrlDemandes.IdeChange = AddressOf ActSelection
    End Sub
    '** RP
    Private Sub ActItemPlanChange(ByVal key As String)
        Master.SessionCourante.FiltreSurPlan = key
        CtrlArbre.SelectionneTypeArmoire(VCache.KeyArmoireSelectionnee)
    End Sub
    '*****
    Private Sub ActItemArmoireChange(ByVal key As String)
        Dim ActiveVue As Integer = 0

        VCache.KeyArmoireSelectionnee = key.ToLower()
        VCache.Sauve(ViewState)
        Select Case key.ToLower
            Case "calendrier"
                ActiveVue = 1
            Case "demandes"
                ActiveVue = 3
            Case Else
                CtrlArbre.SelectionneTypeArmoire(key)
        End Select
        If ActiveVue <> MultiOnglets.ActiveViewIndex Then
            MultiOnglets.ActiveViewIndex = ActiveVue
        End If
    End Sub

    Private Sub ActSelection(ByVal Ide As Integer)
        RCmdNouv.Visible = False

        Dim csa As CacheSelectionArmoire

        If Ide < 0 Then
            VCache.EstNouveauStage = True
            csa = New CacheSelectionArmoire() With {.Ide = -1, .Libelle = "", .Reference = ""}
            CtrlDetail.Charge(csa)
            MultiOnglets.ActiveViewIndex = 2
            Return
        End If

        Dim stf As Virtualia.Metier.Formation.StructureStage = TryCast(MoteurFormation.GetStructure(Ide), Virtualia.Metier.Formation.StructureStage)
        If stf Is Nothing Then
            Charge(Nothing)
            Return
        End If
        VCache.IdeStageCourant = Ide
        VCache.Sauve(ViewState)

        csa = New CacheSelectionArmoire() With {.Ide = stf.Identification.Ide_Dossier, .Libelle = stf.Identification.Intitule, .Reference = stf.Identification.ReferenceduStage}
        CtrlDetail.Charge(csa)
        MultiOnglets.ActiveViewIndex = 2

    End Sub
    '** RP
    Private Sub CtrlDetail_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles CtrlDetail.ValeurRetour
        If VCache.EstNouveauStage = True Then
            VCache.EstNouveauStage = False
            VCache.Sauve(ViewState)
        End If
        Call Charge(False)
    End Sub
End Class