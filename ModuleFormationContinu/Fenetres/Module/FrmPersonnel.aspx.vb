﻿Option Explicit On
Option Strict On
Option Compare Text

Public Class FrmPersonnel
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Dim Msg As String = "Erreur lors de l'identification. Session invalide."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Return
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Master.V_Info2 = ""
            Master.V_Info3 = ""
            Dim Msg As String = "Erreur lors de l'identification. Pas de connexion utilisateur."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Return
        End If
    End Sub

    Private Sub CtrlIndividuel_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles CtrlIndividuel.ValeurRetour
        MultiOnglets.ActiveViewIndex = 0
    End Sub

    Private Sub CtrlArmoire_Dossier_Click(sender As Object, e As Systeme.Evenements.DossierClickEventArgs) Handles CtrlArmoire.Dossier_Click
        CtrlIndividuel.V_Identifiant = e.Identifiant
        MultiOnglets.ActiveViewIndex = 1
    End Sub
End Class