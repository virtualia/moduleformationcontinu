﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmPersonnel.aspx.vb" Inherits="Virtualia.Net.FrmPersonnel" MasterPageFile="~/PagesMaitre/VirtualiaMain.Master" MaintainScrollPositionOnPostback="True" Culture="auto:fr-FR" UICulture="auto:fr-FR" Async="true" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.Master" %>
<%@ Register Src="~/ControlArmoireAgent/VArmoirePER.ascx" TagName="CTL_ARMOIRE" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/PER/CtlSituationW.ascx" TagName="VGestion" TagPrefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreVues" runat="server" Width="1140px" HorizontalAlign="Center" BackColor="#5E9598" CellSpacing="2" Style="margin-top:1px">
        <asp:TableRow>
            <asp:TableCell ID="ConteneurVues" Width="1140px" VerticalAlign="Top" HorizontalAlign="Center">
                <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueSelection" runat="server">
                        <asp:Table ID="TbSeslection" runat="server" Width="1150px" BackColor="#137A76" HorizontalAlign="Center" BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:CTL_ARMOIRE ID="CtrlArmoire" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:View>
                    <asp:View ID="VueGestionIndiv" runat="server">
                        <Virtualia:VGestion ID="CtrlIndividuel" runat="server" />
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
