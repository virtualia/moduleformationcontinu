﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmAdminFormation
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Dim Msg As String = "Erreur lors de l'identification. Session invalide."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Master.V_Info2 = ""
            Master.V_Info3 = ""
            Dim Msg As String = "Erreur lors de l'identification. Pas de connexion utilisateur."
            Response.Redirect("~/Fenetres/Commun/FrmErreur.aspx?Msg=" & Msg)
            Exit Sub
        End If
        MajReferentiel.Act_Selection = AddressOf ActItemSelection
    End Sub

    Private Sub ActItemSelection(ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)
        If e Is Nothing Then
            Exit Sub
        End If
        Dim SiEnable As Boolean = True
        Dim PtdeVue As Integer = e.PointdeVue
        CellRetour.Visible = True
        If e.Code = "Supp" Then
            If PtdeVue = VI.PointdeVue.PVueEntreprise Then
                Call SuppressionOrganisme(e.Valeur)
            End If
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        If WebFct.PointeurUtilisateur.Etablissement <> "" Then
            SiEnable = False
        End If
        Select Case PtdeVue
            Case VI.PointdeVue.PVueGeneral
                MajTabGen.V_NomTable = e.NomdelaTable
                MajTabGen.SiCommandesVisible = SiEnable
                MultiVues.SetActiveView(VueTabGen)
            Case VI.PointdeVue.PVueEntreprise
                MajOrganisme.Identifiant = e.Identifiant
                MultiVues.SetActiveView(VueOrganisme)
        End Select
    End Sub

    Private Sub MajREF_RetourEventHandler(sender As Object, e As System.EventArgs) Handles MajOrganisme.RetourEventHandler
        CellRetour.Visible = False
        MajReferentiel.Initialiser()
        MultiVues.SetActiveView(VueReferentiel)
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles CommandeRetour.Click
        CellRetour.Visible = False
        MajReferentiel.Initialiser()
        MultiVues.SetActiveView(VueReferentiel)
    End Sub

    Private Sub SuppressionOrganisme(ByVal Denomination As String)
        Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)
        Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure
        Dim ConstructeurOrga As Virtualia.Systeme.Sgbd.Sql.SqlInterne
        Dim Sel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
        Dim EvtMsg As Virtualia.Systeme.Evenements.MessageSaisieEventArgs

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        ConstructeurOrga = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WebFct.PointeurGlobal.VirModele, WebFct.PointeurGlobal.VirInstanceBd)
        LstExt = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)

        LstSel = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
        Sel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaFormation, 1)
        Sel.Operateur_Comparaison = VI.Operateurs.Egalite
        Sel.Valeurs_AComparer = (New String() {Denomination}).ToList()
        LstSel.Add(Sel)

        ObjetStructure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.Date_Fin = "31/12/" & Now.Year + 1
        ObjetStructure.SiForcerClauseDistinct = True
        ObjetStructure.SiPasdeTriSurIdeDossier = True
        ObjetStructure.SiPasdeControleDatedeFin = True

        ConstructeurOrga.StructureRequete = ObjetStructure
        ConstructeurOrga.ListeInfosAExtraire = LstExt
        ConstructeurOrga.ListeInfosASelectionner = LstSel

        Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) =
            WebFct.PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WebFct.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaFormation, ConstructeurOrga.OrdreSqlDynamique)
        If LstResultat IsNot Nothing AndAlso LstResultat.Count > 0 Then
            EvtMsg = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Organisme", "OK", "Suppression impossible car cet organisme est utilisé.")
            MsgVirtualia.AfficherMessage = EvtMsg
            MultiVues.SetActiveView(VueMessage)
            Exit Sub
        End If
        EvtMsg = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Suppression", 1, Denomination, "Oui;Non", "Suppression d'un organisme", "Confirmez-vous la suppression de " & Denomination & " ?")
        MsgVirtualia.AfficherMessage = EvtMsg
        MultiVues.SetActiveView(VueMessage)
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        If e.Emetteur <> "Suppression" Then
            MultiVues.SetActiveView(VueReferentiel)
            Exit Sub
        End If
        If e.ReponseMsg <> "Oui" Then
            MultiVues.SetActiveView(VueReferentiel)
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        Dim Ide As Integer
        Dim LstRes As List(Of ServiceServeur.VirRequeteType)
        Dim SiOk As Boolean
        LstRes = WebFct.PointeurGlobal.SelectionDynamique(VI.PointdeVue.PVueEntreprise, 1, 1, VI.Operateurs.ET, VI.Operateurs.Egalite, e.ChaineDatas, "", "")
        If LstRes Is Nothing OrElse LstRes.Count = 0 Then
            MultiVues.SetActiveView(VueReferentiel)
            Exit Sub
        End If
        Ide = LstRes.Item(0).Ide_Dossier
        Dim LstFicheREF As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        LstFicheREF = WebFct.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WebFct.PointeurGlobal.VirNomUtilisateur,
                                                                           VI.PointdeVue.PVueEntreprise, 2, Ide, False)
        If LstFicheREF IsNot Nothing Then
            For Each Fiche In LstFicheREF
                SiOk = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurGlobal.VirNomUtilisateur,
                                                       VI.PointdeVue.PVueEntreprise, 2, Ide, "S",
                                                       CType(Fiche, Virtualia.TablesObjet.ShemaREF.ENT_INTERLOCUTEUR).ContenuTable, "")
            Next
        End If
        LstFicheREF = WebFct.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(WebFct.PointeurGlobal.VirNomUtilisateur,
                                                                           VI.PointdeVue.PVueEntreprise, 1, Ide, False)
        If LstFicheREF IsNot Nothing Then
            For Each Fiche In LstFicheREF
                SiOk = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurGlobal.VirNomUtilisateur,
                                                       VI.PointdeVue.PVueEntreprise, 1, Ide, "S",
                                                       CType(Fiche, Virtualia.TablesObjet.ShemaREF.ENT_ORGANISME).ContenuTable, "")
            Next
        End If
        MajReferentiel.Initialiser()
        MultiVues.SetActiveView(VueReferentiel)

    End Sub
End Class