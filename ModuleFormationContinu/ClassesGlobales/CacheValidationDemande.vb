﻿Option Strict On
Option Explicit On
Option Compare Text
<Serializable>
Public Class CacheValidationDemande
    Private WsIdeFOR As Integer
    Private WsListeIdePER As List(Of Integer) = New List(Of Integer)
    Private WsTypeListe As Integer = 0 '0= Toutes 1=Les demandes accordées 2=Les demandes refusées
    Private WsDateSession As String = ""
    Private WsSuiteDonnee As String = "Accord" 'Accord, Refus
    Private WsCocheAll As Boolean = False

    Public Property IdentifiantStage As Integer
        Get
            Return WsIdeFOR
        End Get
        Set(value As Integer)
            WsIdeFOR = value
        End Set
    End Property

    Public Property ListeIdentifiantsPER As List(Of Integer)
        Get
            Return WsListeIdePER
        End Get
        Set(value As List(Of Integer))
            WsListeIdePER = value
        End Set
    End Property

    Public Property Type_Liste As Integer
        Get
            Return WsTypeListe
        End Get
        Set(value As Integer)
            WsTypeListe = value
        End Set
    End Property

    Public Property DateSession As String
        Get
            Return WsDateSession
        End Get
        Set(value As String)
            WsDateSession = value
            If value = "09/09/1999" Then
                WsDateSession = ""
            End If
        End Set
    End Property

    Public Property SuiteDonnee As String
        Get
            Return WsSuiteDonnee
        End Get
        Set(value As String)
            WsSuiteDonnee = value
        End Set
    End Property

    Public Property SiCocheAllCochee As Boolean
        Get
            Return WsCocheAll
        End Get
        Set(value As Boolean)
            WsCocheAll = value
        End Set
    End Property

End Class
