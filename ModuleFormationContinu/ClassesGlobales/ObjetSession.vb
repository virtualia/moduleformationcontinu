﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class LDObjetSession
        '** RP *************************************
        Private WsParent As Virtualia.Net.Session.ObjetSession
        Private WsSiAccesModuleSeul As Boolean = False 'Si dans le menu Outils la personne a uniquement accés au module formation
        Private WsRequeteur As Virtualia.Metier.Formation.RequeteurFormation = Nothing
        Private WsFiltreSurPlan As String = ""
        '******************
        Private WsGestionOrganisme As Virtualia.Metier.Formation.GestionOrganisme
        Private WsGestionPlandeformation As Virtualia.Metier.Formation.GestionFormation
        Private WsGestionInscription As Virtualia.Metier.Formation.GestionInscription
        Private WsGestionDifs As Virtualia.Metier.Formation.GestionDif
        Private WsGestionInscriptionIndividuelle As Virtualia.Metier.Formation.GestionDonneeIndividuelle

        Private WsLecteurAgent As Virtualia.Metier.Formation.SelectionAgent
        Private WsDonneeState As DonneeStatistiqueFormationInfo = New DonneeStatistiqueFormationInfo()
        Private WsDonneesEdition As Virtualia.Net.DonneeEditionInfo

        Public Property ListValeurExport As List(Of String) = New List(Of String)()

        Public Property ListeArboPlan_TOUS As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo) = Nothing
        Public Property ListeArboPlan_ANS As List(Of Virtualia.Metier.Formation.PlanFormationArborescenceInfo) = Nothing

        Public ReadOnly Property Gestion_Plandeformation As Virtualia.Metier.Formation.GestionFormation
            Get
                Return WsGestionPlandeformation
            End Get
        End Property

        Public ReadOnly Property Gestion_Inscription As Virtualia.Metier.Formation.GestionInscription
            Get
                Return WsGestionInscription
            End Get
        End Property

        Public ReadOnly Property Gestion_InscriptionIndividuelle As Virtualia.Metier.Formation.GestionDonneeIndividuelle
            Get
                Return WsGestionInscriptionIndividuelle
            End Get
        End Property

        Public ReadOnly Property Gestion_Difs As Virtualia.Metier.Formation.GestionDif
            Get
                Return WsGestionDifs
            End Get
        End Property

        Public ReadOnly Property GestionOrganisme As Virtualia.Metier.Formation.GestionOrganisme
            Get
                Return WsGestionOrganisme
            End Get
        End Property

        Public ReadOnly Property Lecteur_Agent As Virtualia.Metier.Formation.SelectionAgent
            Get
                If WsLecteurAgent Is Nothing Then
                    WsLecteurAgent = New Virtualia.Metier.Formation.SelectionAgent(WsParent.Etablissement, WsParent.FiltreV3)
                End If
                Return WsLecteurAgent
            End Get
        End Property

        Public ReadOnly Property DonneeState As DonneeStatistiqueFormationInfo
            Get
                Return WsDonneeState
            End Get
        End Property

        Public Property DonneesEdition As Virtualia.Net.DonneeEditionInfo
            Get
                Return WsDonneesEdition
            End Get
            Set(value As Virtualia.Net.DonneeEditionInfo)
                WsDonneesEdition = value
            End Set
        End Property

        '** RP **********
        Public ReadOnly Property VParent As Virtualia.Net.Session.ObjetSession
            Get
                Return WsParent
            End Get
        End Property

        Public Property SiAccesAuModuleSeul As Boolean
            Get
                Return WsSiAccesModuleSeul
            End Get
            Set(value As Boolean)
                WsSiAccesModuleSeul = value
            End Set
        End Property

        Public ReadOnly Property VirRequeteur As Virtualia.Metier.Formation.RequeteurFormation
            Get
                If WsRequeteur Is Nothing Then
                    WsRequeteur = New Virtualia.Metier.Formation.RequeteurFormation(WsParent.V_IDSession, WsParent.Etablissement, WsParent.FiltreV3)
                End If
                Return WsRequeteur
            End Get
        End Property

        Public Property FiltreSurPlan As String
            Get
                Return WsFiltreSurPlan
            End Get
            Set(value As String)
                WsFiltreSurPlan = value
            End Set
        End Property

        Public Sub InitialiserListesLD()
            ListeArboPlan_TOUS = Nothing
            ListeArboPlan_ANS = Nothing
        End Sub

        '******************
        Public Sub New(ByVal VObjetGlobal As Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase,
                       PointeurSession As Virtualia.Net.Session.ObjetSession)

            WsParent = PointeurSession

            '********************************************
            Dim LDGlobales As LDObjetGlobal = DirectCast(VObjetGlobal, LDObjetGlobal)
            WsGestionPlandeformation = New Virtualia.Metier.Formation.GestionFormation(WsParent.V_NomdUtilisateurSgbd, VObjetGlobal.NumBdd, "")
            WsGestionInscription = New Virtualia.Metier.Formation.GestionInscription(WsParent.V_NomdUtilisateurSgbd, VObjetGlobal.NumBdd, "")
            WsGestionInscriptionIndividuelle = New Virtualia.Metier.Formation.GestionDonneeIndividuelle(WsParent.V_NomdUtilisateurSgbd, VObjetGlobal.NumBdd, "")
            WsGestionDifs = New Virtualia.Metier.Formation.GestionDif(WsParent.V_NomdUtilisateurSgbd, VObjetGlobal.NumBdd, "")
            WsGestionOrganisme = New Virtualia.Metier.Formation.GestionOrganisme(WsParent.V_NomdUtilisateurSgbd, VObjetGlobal.NumBdd, "")

        End Sub

        Public Sub Reinitialiser(ByVal ObjetAreinitialiser As String)

            Gestion_Plandeformation.Reinitialiser()

            Gestion_Inscription.Reinitialiser()
            Gestion_InscriptionIndividuelle.Reinitialiser()
            Gestion_Difs.Reinitialiser()
            GestionOrganisme.Reinitialiser()

            DonneeState.Vider()
            ListValeurExport.Clear()

            ReinitListeArbo()
            WsRequeteur = Nothing
        End Sub

        Public Sub ReinitListeArbo()
            ListeArboPlan_TOUS = Nothing
            ListeArboPlan_ANS = Nothing
        End Sub

    End Class
End Namespace

