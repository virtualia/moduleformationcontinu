﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.Metier.Formation
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class LDObjetGlobal
        Inherits Virtualia.ObjetBaseStructure.Formation.ObjetGlobalBase
        '**RP
        Private WsObjetGlobalModule As Virtualia.Net.Session.ObjetGlobalBase
        Private WsEnsemblePER As Virtualia.Metier.Formation.EnsembleDossiers
        Private WsEnsembleFOR As Virtualia.Metier.Formation.EnsembledeStages
        '**
        Private WsParametreFormation As ParametreFormationInfo = Nothing

        '** RP Décembre 2014
        Public ReadOnly Property ObjetGlobalModule As Virtualia.Net.Session.ObjetGlobalBase
            Get
                Return WsObjetGlobalModule
            End Get
        End Property

        Public ReadOnly Property EnsembleFOR As Virtualia.Metier.Formation.EnsembledeStages
            Get
                If WsEnsembleFOR Is Nothing Then
                    WsEnsembleFOR = New Virtualia.Metier.Formation.EnsembledeStages(WsObjetGlobalModule.VirInstanceBd, WsObjetGlobalModule.VirNomUtilisateur)
                End If
                Return WsEnsembleFOR
            End Get
        End Property

        Public ReadOnly Property EnsemblePER As Virtualia.Metier.Formation.EnsembleDossiers
            Get
                If WsEnsemblePER Is Nothing Then
                    WsEnsemblePER = New Virtualia.Metier.Formation.EnsembleDossiers(WsObjetGlobalModule.VirInstanceBd, WsObjetGlobalModule.VirNomUtilisateur)
                End If
                Return WsEnsemblePER
            End Get
        End Property

        Public ReadOnly Property VirRepertoireTemporaire As String
            Get
                Return WsObjetGlobalModule.VirRepertoireTemporaire
            End Get
        End Property

        Public ReadOnly Property VirConfiguration As Virtualia.Systeme.Configuration.FichierConfig
            Get
                Return WsObjetGlobalModule.VirConfiguration
            End Get
        End Property

        Public ReadOnly Property VirIdentifiantPER(ByVal Nom As String, ByVal Prenom As String, ByVal DateNaissance As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim IndiceI As Integer = 0
                Dim LimiteObjet As Integer
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsObjetGlobalModule.VirModele, WsObjetGlobalModule.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.IdentifiantDossier = 0
                Constructeur.PreselectiondIdentifiants = Nothing

                LimiteObjet = 17
                Constructeur.NoInfoSelection(IndiceI, VI.ObjetPer.ObaCivil) = 2
                Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = Nom
                For IndiceI = 1 To LimiteObjet
                    Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaCivil, 0) = IndiceI
                Next IndiceI
                LstFiches = WsObjetGlobalModule.VirServiceServeur.RequeteSql_ToFiches(WsObjetGlobalModule.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, Constructeur.OrdreSqlDynamique)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return 0
                End If
                If LstFiches.Count = 1 Then
                    Return LstFiches.Item(0).Ide_Dossier
                End If
                For Each Fiche In LstFiches
                    If CType(Fiche, Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).Prenom = Prenom And CType(Fiche, Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).Date_de_naissance = DateNaissance Then
                        Return Fiche.Ide_Dossier
                    End If
                Next
                For Each Fiche In LstFiches
                    If CType(Fiche, Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).Prenom = Prenom Then
                        Return Fiche.Ide_Dossier
                    End If
                Next
                Return 0
            End Get
        End Property

        Public Function VirSelectionFiltree(ByVal PtDevue As Integer, ByVal NoObjet As Integer, ByVal Ide As Integer,
                                              ByVal LstFiltres As List(Of KeyValuePair(Of Integer, String))) As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim IndiceI As Integer = 0
            Dim LimiteObjet As Integer
            Dim Idebut As Integer = 0

            If LstFiltres Is Nothing OrElse LstFiltres.Count = 0 Then
                Return Nothing
            End If
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsObjetGlobalModule.VirModele, WsObjetGlobalModule.VirInstanceBd)
            Constructeur.NombredeRequetes(PtDevue, "01/01/1950", "31/12/" & (DateTime.Now.Year + 50), VI.Operateurs.ET) = LstFiltres.Count
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = True
            Constructeur.IdentifiantDossier = Ide
            Constructeur.PreselectiondIdentifiants = Nothing

            For Each VirObjet In WsObjetGlobalModule.VirListeObjetsDico
                If VirObjet.PointdeVue = PtDevue And VirObjet.Objet = NoObjet Then
                    LimiteObjet = VirObjet.PointeurModele.LimiteMaximum
                    Select Case VirObjet.PointeurModele.VNature
                        Case VI.TypeObjet.ObjetSimple, VI.TypeObjet.ObjetMemo, VI.TypeObjet.ObjetTableau
                            Idebut = 1
                    End Select
                    If NoObjet = VI.ObjetPer.ObaGrade Then
                        LimiteObjet -= 2
                    End If
                    Exit For
                End If
            Next
            If LimiteObjet = 0 Then
                Return Nothing
            End If
            IndiceI = 0
            For Each Element In LstFiltres
                Constructeur.NoInfoSelection(IndiceI, NoObjet) = Element.Key
                Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = Element.Value
                IndiceI += 1
            Next
            For IndiceI = Idebut To LimiteObjet
                Constructeur.InfoExtraite(IndiceI, NoObjet, 0) = IndiceI
            Next IndiceI
            Return WsObjetGlobalModule.VirServiceServeur.RequeteSql_ToFiches(WsObjetGlobalModule.VirNomUtilisateur, PtDevue, NoObjet, Constructeur.OrdreSqlDynamique)

        End Function

        Public Function VirSelectionUneDonnee(ByVal PtDevue As Integer, ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal Ide As Integer) As String
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsObjetGlobalModule.VirModele, WsObjetGlobalModule.VirInstanceBd)
            Constructeur.NombredeRequetes(PtDevue, "", RhDate.DateduJour, VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.IdentifiantDossier = Ide
            Constructeur.PreselectiondIdentifiants = Nothing

            Constructeur.NoInfoSelection(0, NoObjet) = NoInfo
            Constructeur.InfoExtraite(0, NoObjet, 0) = NoInfo

            LstRes = WsObjetGlobalModule.VirServiceServeur.RequeteSql_ToListeType(WsObjetGlobalModule.VirNomUtilisateur, PtDevue, NoObjet, Constructeur.OrdreSqlDynamique)
            If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                Return ""
            End If
            Return LstRes.Item(0).Valeurs(0)
        End Function

        '********************************
        Public Sub New(UtilBdd As String, NumBD As Integer)
            MyBase.New(UtilBdd, NumBD)

            WsObjetGlobalModule = New Virtualia.Net.Session.ObjetGlobalBase(UtilBdd, NumBD)
            WsObjetGlobalModule.VirNomModule = "Formation"

            WsEnsembleFOR = New Virtualia.Metier.Formation.EnsembledeStages(WsObjetGlobalModule.VirInstanceBd, WsObjetGlobalModule.VirNomUtilisateur)
        End Sub

        Public ReadOnly Property ParametreFormation As ParametreFormationInfo
            Get
                If WsParametreFormation IsNot Nothing Then
                    Return WsParametreFormation
                End If
                WsParametreFormation = New ParametreFormationInfo()
                If VirConfiguration IsNot Nothing Then
                    WsParametreFormation.Poids_Heure = CInt(VirConfiguration.Parametres_Formation.Poids_Journee_Heure)
                    'WsParametreFormation.Poids_Jour = CInt(VirConfiguration.Parametres_Formation.Poids_Journee_Jour)
                    'WsParametreFormation.NbDem = CInt(VirConfiguration.Parametres_Formation.Nombre_Demande_En_Cours)
                    'WsParametreFormation.NbPlan = CInt(VirConfiguration.Parametres_Formation.Nombre_Plan_En_Cours)
                    'WsParametreFormation.NbInsc = CInt(VirConfiguration.Parametres_Formation.Nombre_Inscription_En_Cours)
                    'WsParametreFormation.ArrondiDif = CInt(VirConfiguration.Parametres_Formation.Arrondi_DIF)
                End If
                If WsParametreFormation.Poids_Heure = 0 Then
                    WsParametreFormation.Poids_Heure = 7
                End If
                If WsParametreFormation.Poids_Jour = 0 Then
                    WsParametreFormation.Poids_Jour = 1
                End If
                If WsParametreFormation.NbDem = 0 Then
                    WsParametreFormation.NbDem = 20
                End If
                If WsParametreFormation.NbPlan = 0 Then
                    WsParametreFormation.NbPlan = 3
                End If
                If WsParametreFormation.NbInsc = 0 Then
                    WsParametreFormation.NbInsc = 200
                End If
                If WsParametreFormation.ArrondiDif = 0 Then
                    WsParametreFormation.ArrondiDif = 0.5
                End If
                Return WsParametreFormation
            End Get
        End Property

        Public Function CalculerPoidsJournee(ByVal NombreHeures As String) As String
            Dim Nbheures As Double = WsObjetGlobalModule.VirRhFonction.ConversionDouble(NombreHeures)
            If Nbheures = 0 Then
                Return ""
            End If
            Dim ValDemiJournee As Double = Math.Round((ParametreFormation.Poids_Heure / 2), 1)
            Dim NbDemiJourneeComplete As Integer = 0
            Dim DureeRestante As Double = 0
            Try
                NbDemiJourneeComplete = Convert.ToInt32(Math.Truncate(Nbheures / ValDemiJournee))
            Catch ex As Exception
                NbDemiJourneeComplete = 0
            End Try
            If Nbheures < ValDemiJournee Then
                If ParametreFormation.Poids_Jour <> 0 Then
                    'On ne proratise pas
                    NbDemiJourneeComplete = NbDemiJourneeComplete + 1
                    Return CStr(Math.Round(NbDemiJourneeComplete / 2, 1))
                End If
            End If
            DureeRestante = Nbheures - (NbDemiJourneeComplete * ValDemiJournee)
            If DureeRestante = 0 Then
                Return CStr(Math.Round(NbDemiJourneeComplete / 2, 1))
            End If
            'Si plus grand qu'une demi journee, proratisation obligatoire
            Dim Cpl As Double = DureeRestante / ParametreFormation.Poids_Heure
            Return CStr(Math.Round(((NbDemiJourneeComplete / 2) + Cpl), 1))
        End Function

        Public ReadOnly Property SiteIMG As String
            Get
                Return ConfigurationManager.AppSettings("RepertoireVirtuelGraphiques")
            End Get
        End Property

        Public ReadOnly Property CheminIMG As String
            Get
                Return ConfigurationManager.AppSettings("RepertoirePhysiqueGraphiques")
            End Get
        End Property

    End Class
End Namespace
