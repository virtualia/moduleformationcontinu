﻿Option Strict On
Option Explicit On
Option Compare Text

Imports System.Web.SessionState
Imports Virtualia.Systeme
Imports Virtualia.Net.WebService
Imports System.Configuration
Imports Virtualia.Net.Session
Imports System.IO
Imports System.Web

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application est démarrée
        Dim ObjGlobalTest As LDObjetGlobal = New LDObjetGlobal(ConfigurationManager.AppSettings("NomUtilisateur"), Integer.Parse(ConfigurationManager.AppSettings("NumeroDatabase")))
        Try
            Select Case ObjGlobalTest.Srv.OuvertureSessionWcf(ConfigurationManager.AppSettings("NomUtilisateur"), ConfigurationManager.AppSettings("Password"), Integer.Parse(ConfigurationManager.AppSettings("NumeroDatabase")))
                Case Constantes.CnxUtilisateur.CnxOK, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxDejaConnecte, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxReprise, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxVirtualia
                    Application.Add("Database", CInt(ConfigurationManager.AppSettings("NumeroDatabase")))
                Case Else
                    EcrireLog("---- Erreur à la connexion au WCF ServiceServeur ---")
                    Dispose()
                    Return
            End Select
        Catch ex As Exception
            EcrireLog("---- Erreur le WCF ServiceServeur ne répond pas ---" & Strings.Space(1) & ex.Message)
            Dispose()
            Return
        End Try

        Try
            Application.Add("VirGlobales2", ObjGlobalTest)
        Catch ex As Exception
            EcrireLog("---- Erreur au Chargement de l'application ---" & Strings.Space(1) & ex.Message)
            Dispose()
            Return
        End Try
        '**RP Décembre 2014 
        Try
            Application.Add("VirGlobales", ObjGlobalTest.ObjetGlobalModule)
        Catch ex As Exception
            EcrireLog("---- Erreur au Chargement de l'application ---" & Strings.Space(1) & ex.Message)
            Dispose()
            Return
        End Try
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session est démarrée
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche au début de chaque demande
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lors d'une tentative d'authentification de l'utilisation
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsqu'une erreur se produit
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session se termine
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application se termine
        GC.Collect()
        Try
            GC.WaitForPendingFinalizers()
        Catch ex As Exception
            Return
        End Try
    End Sub

    Private Sub EcrireLog(ByVal Msg As String)

        Dim SysFicLog As String = "WebVersion4.log"

        Dim NomRep As String = ConfigurationManager.AppSettings("RepertoireVirtualia")
        Dim NomLog As String = Constantes.DossierVirtualiaService("Logs") & SysFicLog

        Using FicStream As FileStream = New FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            Using FicWriter As StreamWriter = New StreamWriter(FicStream, Encoding.UTF8)
                FicWriter.WriteLine(Format(System.DateTime.Now, "g") & " " & Msg)
                FicWriter.Flush()
                FicWriter.Close()
            End Using
        End Using
    End Sub
End Class